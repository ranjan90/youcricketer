<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>

<div class="page-container"> 
		<?php  include('common/user-left-panel.php');?>
      <!-- END SIDEBAR -->
      
      <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
		<div class="page-content">
		
		<div class="white-box">
            <div class="row">
              <div class="col-sm-12">
                <h2> Deleted Messages </h2>
              </div>
            </div>
			
			<?php 
				if(isset($_POST['action']) && $_POST['action'] == 'multi'){ 
					foreach(array_keys($_POST['ids']) as $ids){
						$record = get_record_on_id('messages', $ids);
						if($record['from_user_id'] == $_SESSION['ycdc_dbuid']){
							$key = 'p_delete_by_from';
						}else if($record['to_user_id'] == $_SESSION['ycdc_dbuid']){
							$key = 'p_delete_by_to';
						}
						mysqli_query($conn,"update messages set $key = '4' where id = '$ids' ");
					}
					echo '<div id="success" class="alert alert-success">Message deleted successfully ... !</div>';
				}
			?>
            
            <form method="post" action="" enctype="multipart/form-data" class="form-horizontal" id="multiple">
              <input name="action" value="multi" type="hidden">
              <div class="form-group">
                <div class="col-sm-4">
                  <a href="#" class="text-success checked" id="checkAll" style="display:inline-block; margin:15px 5px;"><i class="fa fa-check-square"></i> Check All</a>
                  <a href="#" class="text-danger checked" id="uncheckAll" style="display:inline-block; margin:15px 5px;"><i class="fa fa-check-square-o"></i> Uncheck All</a>
                </div>
                <div class="col-sm-4">
                  <select name="dd_status" class="form-control">
                     <option value="" selected="selected">Select One</option>
					<option value="<?=DELETED_STATUS?>">Delete Messages</option>
                  </select>
                </div>
                <div class="col-sm-4">
                  <button class="btn orange hvr-float-shadow" type="submit"> <i class="fa fa-times"></i> Delete Selected Message </button>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="table-responsive">
                    <table class="table table-bordered table-condensed table-hover table-striped" id="inbox">
                      <thead>
                        <tr>
                          <th><!--<input name="Select All" type="checkbox">--></th>
                          <th> From </th>
                          <th> Subject </th>
                          <th> Date </th>
                          <th> Action </th>
				        </tr>
                      </thead>
                      <tbody>
						<?php 	$rs_msg = mysqli_query($conn,"select * from messages where (from_user_id = '".$_SESSION['ycdc_dbuid']."' or to_user_id = '".$_SESSION['ycdc_dbuid']."' ) 
			    						and (p_delete_by_to = '1' or p_delete_by_from = '1') ORDER BY message_date DESC "); 
						if(mysqli_num_rows($rs_msg) > 0){
						while($row_msg = mysqli_fetch_assoc($rs_msg)){ 
				    	$thread =  $row_msg['thread_id'];	
						?>
                        <tr title="Click for detail" class="<?=($row_msg['status'] == '0')?'selected':'';?>">
                          <td><input type="checkbox" name="ids[<?=$row_msg['id']?>]"></td>
                          <td><?=get_combo('users','f_name',$row_msg['from_user_id'],'','text')."&nbsp;"?><? echo get_combo('users','last_name',$row_msg['from_user_id'],'','text');?></td>
                          <td style="cursor:pointer;" onclick="window.location='<?=WWW?>inbox-message-<?=$row_msg['id']?>-<?=$row_msg['thread_id']?>.html'"><?=truncate_string($row_msg['subject'], 200)?></td>
                          <td><?=date_converter($row_msg['message_date'])." ".date("H:i:s",strtotime($row_msg['message_date']))?></td>
                          <td><a href="<?=WWW?>inbox-delete-<?=$row_msg['id']?>.html" title="Delete Message" onclick="return confirm('Are you sure to delete Message');" class="text-danger"><i class="fa fa-times"></i> </a></td>
                        </tr>
                        <?php  } ?>
						<?php }else{ ?>
							<tr><td colspan="3">No Records</td></tr>
						<?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </form>
          </div>
		
		
		</div>
	</div>
</div>		

<script>
$(document).ready(function(){
	
	$('a#checkAll').click(function(){
		$('form#multiple input[type=checkbox]').attr('checked', 'checked').prop('checked', true);
	});
	$('a#uncheckAll').click(function(){
		$('form#multiple input[type=checkbox]').removeAttr('checked', 'checked').prop('checked', false);
	});
});
</script>
<style>.selected{background-color:#ccc !important;}</style>

<?php include('common/footer.php'); ?>