<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? }
 ?>
		<?php $company_type_editable = true;
		$company_type_info = get_record_on_id('company_types', $row_comp['company_type_id']);

		// remove after updating permalinks
		/*$sql = "Select * from companies ";
		$rs = mysqli_query($conn,$sql);
		while($row_data = mysqli_fetch_assoc($rs)){
			$company_name = trim($row_data['company_name']);
			$company_id = $row_data['id'];
			$company_permalink = '';
					if(!empty($row_data['company_name'])){
						if(strpos($company_name,' ') === false){
							$company_permalink = strtolower($company_name).'-'.$company_id;
						}else{
							$name_arr = explode(' ',$company_name);
							for($i=0;$i<count($name_arr);$i++){
								if(preg_match('/^([a-z]+)$/i',$name_arr[$i]))
									$company_permalink.=substr($name_arr[$i],0,1);
							}
							$company_permalink = strtolower($company_permalink).'-'.$company_id;
						}
						$sql = "Update companies SET company_permalink = '$company_permalink' Where id = ".$company_id;
						mysqli_query($conn,$sql);
					}
		}*/

		/*if(isset($row_comp) && in_array($row_comp['company_type_id'],array(1,2,3,35)) ){
			$sql = "Select c.id,t.id as tournament_id, lc.id as club_id, lw.id as umpire_id, lv.id as venue_id
			from companies as c left join tournaments as t on c.id=t.league_id
			left join league_clubs as lc on c.id=lc.league_id
			left join league_umpires as lw on c.id=lw.league_id
			left join league_venues as lv on c.id=lv.league_id Where c.id = ".$row_comp['id'].' LIMIT 1';
			$rs = mysqli_query($conn,$sql);
			$league_data = mysqli_fetch_assoc($rs);
			if(empty($row_comp['by_laws_file']) && empty($league_data['tournament_id']) && empty($league_data['club_id']) && empty($league_data['umpire_id']) && empty($league_data['venue_id']) ){
				$company_type_editable = true;
			}else{
				$company_type_info = get_record_on_id('company_types', $row_comp['company_type_id']);
				$company_type_editable = false;
			}
		}

		if(isset($row_comp) && in_array($row_comp['company_type_id'],array(4)) ){
			$sql = "Select member_id from club_members Where club_id = ".$row_comp['id'].' LIMIT 1';
			$rs = mysqli_query($conn,$sql);
			if(mysqli_num_rows($rs)){
				$company_type_editable = false;
				$company_type_info = get_record_on_id('company_types', $row_comp['company_type_id']);
			}else{
				$company_type_editable = true;
			}
		}*/
		?>

		<? 	if(isset($_POST) && !empty($_POST)){

			if($_POST['is_newsletter'] == 'on'){
					$isNews = '1';
				}else{
					$isNews = '0';
				}

				//$set[] 		= "date_of_birth = '".$_POST['year'].'-'.$_POST['month'].'-'.$_POST['day']."'";
				$set[] = "date_of_birth = '".$_POST['birthday']."'";
				$set[] 		= "gender = '".$_POST['gender']."'";
				$set[] 		= "f_name = '".$_POST['f_name']."'";
				$set[] = "is_newsletter = '".$isNews."'";
				$set[] 		= "last_name = '".$_POST['last_name']."'";
				$set[] 		= "country_id = '".$_POST['country_id']."'";
				$set[] 		= "timezone = '".$_POST['timezone']."'";


			//pr($set);exit;
				/*
				$rsCheck  = mysqli_fetch_assoc(mysqli_query($conn,"select * from users where yc_email = '".$_POST['yc_email']."'"));
				if(isset($_POST['yc_email']) && mysqli_num_rows($rsCheck) == 0){
					$set[] = "yc_email = '".$_POST['yc_email']."'";
				}else{
					echo '<div id="error">Email Letters already exists ... !</div>';
				}
				*/
				$rsCheck  = mysqli_fetch_assoc(mysqli_query($conn,"select * from users where permalink = '".$_POST['permalink']."'"));
				if(isset($_POST['permalink'])){
					if(mysqli_num_rows($rsCheck) == 0){
						$set[] = "permalink = '".$_POST['permalink']."'";
					}
				}

				$query  = "update users set ".implode(',', $set)." where id = '".$row_user['id']."'";
				//echo $query;exit;

				if(mysqli_query($conn,$query)){
					$update[] =  "company_name = '".$_POST['company_name']."'";
					if(isset($_POST['name'])) $update[] =  "company_type_id = '".$_POST['name']."'";
					$company_name = trim($_POST['company_name']);
					$company_id = $row_comp['id'];

					if(!empty($_POST['company_name']) && $_SESSION['company_name'] != trim($_POST['company_name']) ){
						if(strpos($company_name,' ') === false){
							$company_permalink = strtolower($company_name);
						}else{
							$name_arr = explode(' ',$company_name);
							for($i=0;$i<count($name_arr);$i++){
								if(preg_match('/^([a-z]+)$/i',$name_arr[$i]))
									$company_permalink.=substr($name_arr[$i],0,1);
							}
							$company_permalink = strtolower($company_permalink);
						}
						$sql = "Select id from companies Where company_permalink = '$company_permalink' AND id != ".$row_comp['id'];
						$rs_result = mysqli_query($conn,$sql);
						if(mysqli_num_rows($rs_result) > 0){
							$sql = "Select id,company_permalink from companies Where company_permalink LIKE '{$company_permalink}-%' AND id != ".$row_comp['id']." ORDER BY id DESC LIMIT 1 ";
							$rs_result = mysqli_query($conn,$sql);
							if(mysqli_num_rows($rs_result) == 0){
								$number = 1;
							}else{
								$row_permalink = mysqli_fetch_assoc($rs_result);
								$number = substr($row_permalink['company_permalink'],strpos($row_permalink['company_permalink'],'-')+1)+1;
							}

							$company_permalink = $company_permalink.'-'.$number;
						}
						$update[] =  "company_permalink = '".$company_permalink."'";
						$_SESSION['company_name'] = $_POST['company_name'];
					}

					$sql = "Update companies set ".implode(',', $update)." Where user_id=".$row_user['id'];
					mysqli_query($conn,$sql);
					echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
				    ?>
	                <script>
	                	window.location.href = "<?=WWW?>account-information-company.html";
	                </script>
	                <?
				}else{
					echo '<div id="error" class="alert alert-danger">Information cannot be updated ... !</div>';
				}
			}

		?>

<div class="page-container">
	<?php include('common/user-left-panel.php');?>

	<!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">

          <div class="white-box">

			<div class="row">
              <div class="col-sm-12">
                <h2> Administrator & Organization Information </h2>
              </div>
            </div>

            <form method="post" action="" enctype="multipart/form-data" id="edit-account-frm" class="form-horizontal">
              <input name="action" value="update" type="hidden">
              <div class="form-group">
                <label class="col-sm-3 control-label"> First Name: </label>
                <div class="col-sm-3">
                  <input value="<?=$row_user['f_name']?>" name="f_name" maxlength="30" class="form-control validate[required]" type="text">
                </div>
                <label class="col-sm-3 control-label"> Last Name: </label>
                <div class="col-sm-3">
                  <input value="<?=$row_user['last_name']?>" maxlength="30" name="last_name" class="form-control" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label"> Date of Birth: </label>
                <div class="col-sm-3">
                  <input name="birthday" value="<?php echo $row_user['date_of_birth']; ?>" autocomplete="off" class="form-control datepicker validate[required]" maxlength="30" type="text">
                  <span class="help-block"><a href="#" onClick="javascript:void(0)" data-toggle="modal" data-target="#birthView">Why do we need your birthday?</a></span>
                </div>
                <label class="col-sm-3 control-label"> Gender: </label>
                <div class="col-sm-3">
                  <?=get_gender_combo($row_user['gender'],'');?>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label"> Country of Residence: </label>
                <div class="col-sm-3">
                  <?=get_combo('countries','name', $row_user['country_id'],'country_id');?>
                </div>

				 <label class="col-sm-3 control-label"> Timezone: </label>
                <div class="col-sm-3">
                  <?php echo get_combo('timezones', 'title', $row_user['timezone'],'timezone')?>
                </div>

              </div>
              <div class="form-group">
				<label class="col-sm-3 control-label"> Title: </label>
                <div class="col-sm-3">
                  <input value="<?php if(isset($_POST['company_name'])) echo $_POST['company_name'];else echo $row_comp['company_name']; ?>" name="company_name" id="company_name" placeholder="Type Club/League/Company Name" maxlength="60"  class="form-control validate[required]" type="text">
                </div>

                <label class="col-sm-3 control-label"> Email Address: </label>
                <div class="col-sm-3">
                  <div class="text" style="padding-top:7px;"><?php echo $row_user['email']; ?></div>
                </div>

              </div>
              <div class="form-group">

				<label class="col-sm-3 control-label"> Registered On: </label>
                <div class="col-sm-3">
                  <div class="text" style="padding-top:7px;"><?=date_converter($row_user['create_date'])?></div>
                </div>
				<label class="col-sm-3 control-label"> Registered As: </label>
                <div class="col-sm-3" style="padding-top:7px;">
					<?php if($row_comp['tournament_data_added'] == 'no'): ?>
						<?=get_combo('company_types','name',$row_comp['company_type_id'],'')?>
					<?php else: ?>
						<div class="text"><?php echo $company_type_info['name']; ?></div>
					<?php endif; ?>
                </div>

              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label"> YC Profile Permalink: </label>
                <div class="col-sm-3" style="padding-top:7px;">
					<?=WWW?><?php echo $row_comp['company_permalink'];  ?>
                </div>
                <label class="col-sm-3 control-label"> Referral Acceptance Counter: </label>
                <div class="col-sm-3" style="padding-top:3px;">
                  <p class="form-control-static">
					<? 	$query 	= "select * from invitations where from_user_id = '$userid' and status = 'Invitation Accepted'";
								$rs_friends = mysqli_query($conn,$query);
								echo mysqli_num_rows($rs_friends);
					?></p>
                  <span class="help-block"><a href="#" onClick="javascript:void(0)" data-toggle="modal" data-target="#counterView">What is this Counter for?</a></span>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12 text-center">
                  <input class="btn orange hvr-float-shadow" value="Update Information" type="submit">
                  <a class="btn blue hvr-float-shadow" href="dashboard.html">Go Back</a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- END CONTENT-->

</div>


    <div class="modal fade" id="birthView" tabindex="-1" role="dialog" aria-labelledby="birthViewModel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="settingLabel">Why do we need your Birthday </h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-12">
				<?php $row = get_record_on_id('cms',7);?>
                <p><?php echo $row['content']; ?></p>
              </div>
            </div>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.birthModel-Model -->

    <div class="modal fade" id="counterView" tabindex="-1" role="dialog" aria-labelledby="counterViewModel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="settingLabel">What is this counter for ? </h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-12">
                <?php $row = get_record_on_id('cms',14);?>
                <p><?php echo $row['content']; ?></p>
              </div>
            </div>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.counterView-Model -->

<script type="text/javascript">
$(document).ready(function(){
	$('.datepicker').datepicker({'format':'yyyy-mm-dd'});
	$('#edit-account-frm').validationEngine();
});
</script>

<?php include('common/footer.php'); ?>
