<?php include_once('includes/configuration.php');
$page = 'club-members.html';
$selected_country = getGeoLocationCountry();

$ppage = intval($_GET["page"]);
if($ppage<=0) $ppage = 1;

$league_info = array();
$user_info  = array();
$club_members = array();

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);
	if($user_info['is_company'] == 1){
		$sql = "select * from companies where user_id = $user_id and company_type_id = 2 and status=1";
		$rs_company = mysqli_query($conn,$sql);
		if(mysqli_num_rows($rs_company)){
			$league_info = mysqli_fetch_assoc($rs_company);
		}
	}
}

if(isset($_POST['clubs_del_submit']) && !empty($_POST['clubs_del_submit'])){
	$clubs = $_POST['club_ids'];
	if(!empty($clubs)){
		for($i=0;$i<count($clubs);$i++){
			$sql = "DELETE FROM league_clubs WHERE league_id=".$league_info['id']." and club_id=".$clubs[$i] .' LIMIT 1';
			mysqli_query($conn,$sql);
		}
	}

	$_SESSION['league_club_deleted'] = 1;
	header("Location:".WWW.'league-clubs-all-'.$ppage.'.html');
	exit;
	//echo '<script>window.location.href="'.WWW.'club-members-all-'.$ppage.'.html"</script>';
}

if(isset($_SESSION['league_club_deleted']) && $_SESSION['league_club_deleted']==1) {
	$club_deleted = 1;
	unset($_SESSION['league_club_deleted']);
}
?>
<?php include('common/header.php'); ?>

<?php
	$rpp = PRODUCT_LIMIT_FRONT; // results per page

    $query = "select distinct u.*,c.company_name,c.years_in_business,c.id as company_id,c.company_permalink from users u inner join companies as c on u.id=c.user_id inner join league_clubs as l on l.club_id=c.id where u.status = 1 and l.league_id=".$league_info['id'];
	$query_count = "select count(*) as users_count from users u inner join companies as c on u.id=c.user_id inner join league_clubs as l on l.club_id=c.id where u.status = 1 and l.league_id=".$league_info['id'];
	//=======================================
	if(isset($_GET['keywords']) && $_GET['keywords'] != 'Search Here' && $_GET['keywords'] != 'all' ){
		$keywords = str_replace('_',' ',trim($_GET['keywords']));
		$where  .= " and (c.company_name like '%{$keywords}%' ) ";
	}else{
		$keywords = '';
		$where  = '';
	}

	$query.=$where;
	$query_count.=$where;
	$query .= " order by c.company_name ";

	$rs_count   = mysqli_query($conn,$query_count);
	$row_count  = mysqli_fetch_assoc($rs_count);
	$tcount = $row_count['users_count'];


	$tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
	$count = 0;
	$start = ($ppage-1)* $rpp;
	$x = 0;

	$query .= " LIMIT $start,$rpp ";
?>


<div class="page-container">
		<?php include('common/user-left-panel.php');?>
      <!-- END SIDEBAR -->

      <!-- BEGIN CONTENT -->
      <div class="page-content-wrapper">
        <div class="page-content">

          <div class="white-box">
            <div class="row">
              <div class="col-md-12">
                <h2> <?php echo $league_info['company_name'];  ?> - League Clubs </h2>
				<?php if(isset($club_deleted) && $club_deleted == 1): ?>
					<div id="information" class="alert alert-success">Clubs deleted Successfully... !</div>
				<?php  endif; ?>

				<?php if(empty($user_info)): ?>
					<div id="error" class="alert alert-danger">You are not logged... !</div>
				<?php endif; ?>

				<?php if(!empty($user_info) && empty($league_info) ): ?>
					<div id="error" class="alert alert-danger">You are not logged as League Owner.. !</div>
				<?php endif; ?>
              </div>
            </div>

            <div id="pagination-top">
              <div class="row">
                <div class="col-sm-6">
                <?php if($tcount != 0 && $tcount > PRODUCT_LIMIT_FRONT){

					if(!empty($keywords)){
						$reload = "league-clubs-{$keywords}.html?";
					}else{
						$reload = "league-clubs-all.html?";
					}
		        	echo paginate_one($reload, $ppage, $tpages);
		      	?>
		      	<input type="hidden" name="pagination-page" value="league-clubs.html">
		        <?php } ?>
				</div>
                <div class="col-sm-3" id="search-div1">
					<form id="list-search" method="post" action="">
                  <div class="input-group" >
                    <input class="form-control validate[required] input-login" name="txtsearch" id="txtsearch" placeholder="Search Your Clubs" type="text" value="<?=$_POST['txtsearch']?>">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit">Go!</button>
                    </span>
                  </div>
				  </form>
                </div>
              </div>
            </div>

			<form method="post">
            <div id="individual" class="content1">
				<?php
						$rs   = mysqli_query($conn,$query);
						$rcount = mysqli_num_rows(mysqli_query($conn,$query));
						if($rcount == 0){
							echo '<div id="information" class="alert alert-danger">No record found ... !</div>';
						}

						$i= 0 ;
						while($row 	= mysqli_fetch_assoc($rs)){


                		$row_img = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row['id']."' and is_default = '1' "));
			  				$row_country= get_record_on_id('countries', $row['country_id']);
			  				$location 	= $row_country['name'];
			  				$row_vid = mysqli_fetch_assoc(mysqli_query($conn,"select * from videos where entity_id = '".$row['id']."' and entity_type = 'users' and is_default = '1' and file_name != ''"));
		        ?>
              <dl>
                <dt> <a href="<?=WWW?><?php echo $row['company_permalink']; ?>" title="<?=$row['company_name']; ?>"><img src="<?php echo WWW;?><?=($row_img && !empty($row_img['file_name']))?'users/'.$row['id'].'/photos/'.$row_img['file_name']:'images/no-photo.jpg'?>"></a></dt>
                <dd>
                  <div class="details">
                    <h3><a href="<?=WWW?><?php echo $row['company_permalink']; ?>" title="<?=$row['company_name']; ?>"><?=truncate_string($row['company_name'],20)?></a></h3>
                    <p><? echo $location;?><br>
                   <?if($row['user_type_id'] != 1){
						echo get_combo('user_types','name',$row['user_type_id'],'','text');  ?>
						<br />
					<?   if($row['user_type_id'] == 2){ echo $row['type'];} }?>
					<?php if(!empty($league_info)): ?>
						<p><input  name="club_ids[]" id="club_id_<?php echo $i; ?>" value="<?php echo $row['company_id']; ?>" type="checkbox">  Remove from my League </p>
					<?php endif; ?>

                    <a href="<?=WWW?><?php echo $row['company_permalink']; ?>" title="<?=$row['company_name'].' - '.$site_title?>">View Profile</a>
                    <span class="flag"><img title="<?=$row_country['name']?>" alt="<?=$row_country['name']?>" src="<?php echo WWW;?>countries/<?=$row_country['flag']?>"></span>
                  </div>
                  <div class="video"> <?
										$video = $row_vid['file_name'];
											if(!empty($video)){
											if(preg_match('/<iframe(.*)<\/iframe>/', $row_vid['file_name'])){

												preg_match('/src="(.*?)"/',$row_vid['file_name'] , $src);
												$src = $src[1];
												$video = "<iframe width='205' height='130' frameborder='0' allowfullscreen src='$src'></iframe>";

											}else{
												$filename = explode('.',$video);
												$filename1= $filename[0];
												$video = '<video width="220" height="130" controls>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mp4" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.ogg" type=\'video/ogg; codecs="theora, vorbis"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mov" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.webm" type=\'video/webm; codecs="vp8, vorbis"\'>
												</video>';
											}
										}else{
											$video = '<img src="'.WWW.'images/no-video.jpg" >';
										}
										echo $video;?>
					</div>
                </dd>
              </dl>
			  <?php

				$i++;
				$count++;
				$x++;
				}
				?>

				<div class="clearfix"></div>
            </div>
			<div class="clearfix"></div>
						<?php if($rcount!=0){ ?>
						<div class="row">
              <div class="col-sm-12">
                <input name="clubs_del_submit" id="clubs_del_submit" value="Delete Clubs" type="submit" class="btn orange hvr-float-shadow" style="margin:15px 0">
              </div>
            </div>
						<?php }?>
			</form>
<div class="clearfix"></div>
            <div id="pagination-bottom">
              <div class="row">
                <div class="col-sm-6">
                <?php if($tcount != 0 && $tcount > PRODUCT_LIMIT_FRONT){

					if(!empty($keywords)){
						$reload = "league-clubs-{$keywords}.html?";
					}else{
						$reload = "league-clubs-all.html?";
					}
		        	echo paginate_one($reload, $ppage, $tpages);
		      	?>
		      	<input type="hidden" name="pagination-page" value="league-clubs.html">
		        <?php } ?>
				</div>
                <div class="col-sm-3" id="search-div2">
					<form id="list-search" method="post" action="">
                  <div class="input-group">
                    <input class="form-control validate[required] input-login" name="txtsearch" id="txtsearch" placeholder="Search Your Clubs" type="text" value="<?=$_POST['txtsearch']?>">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit">Go!</button>
                    </span>
                  </div>
				  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- END CONTENT-->
    </div><!-- /.container -->

	<script type="text/javascript">
		       $('form#list-search').submit(function(e){
					var parentId = $(this).parent().attr('id');
					var string = $('#'+parentId +' form input[name=txtsearch]').val();
					if(string==""){
						$('form#list-search').attr('action','<?=WWW;?>league-clubs.html');
					}
					if(string != '' && string != 'Search Here'){
						string = string.replace(/[ ]+/g,'_');
						string = string.replace(/[^a-zA-Z0-9_]+/g,'').toLowerCase();
						if(string.length > 0){
							$('form#list-search').attr('action','<?=WWW;?>league-clubs-' + string + '.html');
						}
					}
				});
	</script>

<?php include('common/footer.php'); ?>
