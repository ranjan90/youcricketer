<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	
		<? 	if(isset($_POST) && !empty($_POST)){ 
				$title 	 = $_POST['title'];
				$content = $_POST['content'];
				$state_id =$_POST['state_id'];
				$city_name= $_POST['city_name'];
				$isGlobal = ($_POST['is_global'] == 'on')?'1':'0';
				$from_date = $_POST['start_date'];
				$to_date   = $_POST['end_date'];
				
				$date_from1 = strtotime($from_date);
				$date_to    = strtotime($to_date);
				
				if($date_from1 > $date_to) {
				
					echo "<div id='error' class=\"alert alert-danger\"><b>Error : </b>Please Select Correct Date ... Start Date should be less than End date  ... !</div><br><br>"; 
				}
				
				else {
				
				$start_date = date_converter($_POST['start_date'],'Y-m-d');
				$end_date 	= date_converter($_POST['end_date'],'Y-m-d');
				$date 		= date('Y-m-d H:i:s');
				$user_id 	= $_SESSION['ycdc_dbuid'];
				
				if(!isset($user_id) || empty($user_id)){
					echo '<div id="error" class="alert alert-success">Please login first ... !</div>';
				}else{
					if(!empty($title) && !empty($content)){
						$rs_check = mysqli_query($conn,"select * from events where user_id = '$user_id' and start_date = '$start_date' and end_date = '$end_date' and title = '$title'");
						if(mysqli_num_rows($rs_check) == 0){
							$query = "insert into events (title, content, user_id, start_date, end_date, status, create_date, city_name, state_id, is_global) 
							values ('$title','$content','$user_id','$start_date','$end_date','1','$date','$city_name','$state_id','$isGlobal')";

							if(mysqli_query($conn,$query)){
								$forum_id = mysqli_insert_id($conn);
								
								if(!is_dir('events/'.$forum_id)){
									mkdir('events/'.$forum_id,0777);
								}
								chmod('events/'.$forum_id,0777);

								if(!empty($_FILES['photo']['name'])){
									$filename 	= friendlyURL($title).'.jpg';
									$image 		= new SimpleImage();
									$image->load($_FILES["photo"]["tmp_name"]);
									$image->save('events/'.$forum_id.'/'.$filename);
									chmod('events/'.$forum_id.'/'.$filename,0777);
								
									mysqli_query($conn,"insert into photos (file_name, title, entity_id, entity_type) values ('$filename','$title','$forum_id','events')");
								}
							}

			                echo '<div id="success" class="alert alert-success">Forum Information added successfully ... !</div>';
			                ?>
			                <script>
			                window.location = '<?=WWW?>my-events.html';
			                </script>
			                <?
						}else{
							echo '<div id="error" class="alert alert-danger">Information already exists ... !</div>';
						}
					}else{
						echo '<div id="error" class="alert alert-danger">Please fill all fields ... !</div>';
					}
				}
				
			}
		}
		?>
		

<script src="<?=WWW;?>assets/js/ckeditor/ckeditor.js"></script>

<div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1> Add Event </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <form method="post" action="" enctype="multipart/form-data" class="form-horizontal" id="form-add-event">
            
            <div class="white-box">
              <div class="form-group">
                <div class="col-sm-12 text-center">
                  <h2>Event Information</h2>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Event Title</label>
                <div class="col-sm-9">
					<input type="text" name="title" value="<? if(isset($_POST['title'])){echo $_POST['title'];}?>"class="form-control validate[required]">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Is Global</label>
                <div class="col-sm-9">
                  <input name="is_global" type="checkbox">
                  <span class="help-block">Event will be Regional Event If checked</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Content</label>
                <div class="col-sm-9">
                  <textarea name="content" id="content"  class="ckeditor" class="validate[required]"><?=$row['content']?></textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Start Date</label>
                <div class="col-sm-3">
                  <input name="start_date" value="" class="form-control datepicker validate[required]" type="text" autocomplete="off">
                </div>
                <label class="col-sm-3 control-label">End Date</label>
                <div class="col-sm-3">
                  <input name="end_date" value="" class="form-control datepicker validate[required]" type="text" autocomplete="off">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">State</label>
                <div class="col-sm-3">
                  <select name="state_id" class="form-control validate[required]">
                   <option selected="selected" value=""></option>
					<?php 	$rs_states = mysqli_query($conn,"select * from states where country_id = '".$row_user['country_id']."'"); 
						while($row_state_l = mysqli_fetch_assoc($rs_states)){ ?>
						<option <?=($_POST['state_id'] == $row_state_l['id'])?'selected="selected"':'';?> value="<?=$row_state_l['id']?>"><?=$row_state_l['name']?></option>
					<?php  } ?>
                  </select>
                </div>
                <label class="col-sm-3 control-label">City</label>
                <div class="col-sm-3">
                  <input name="city_name" value="<?php if($city_name){ echo $city_name;} ?>"  class="form-control validate[required]" type="text">
                </div>
              </div>
             
              <div class="form-group">
                <label class="col-sm-3 control-label">Event Photo</label>
                <div class="col-sm-9">
                  <input accept="image/*" name="photo" type="file">
                  <span class="help-block">Max Photo size : 2MB</span>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12 text-center">
                  <input value=" Save " class="btn orange hvr-float-shadow" type="submit">
                  <a id="cancel" class="btn blue hvr-float-shadow" href="<?=WWW?>events.html">Cancel</a>
                </div>
              </div>
            </div>
		  </form>
        </div>
        
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
              <?php include('common/right-panel.php');?>
            </div>
          </div>
          
        </div>
      </div>
    </div><!-- /.container -->
	
<script type="text/javascript">
$(document).ready(function(){
	$('#form-add-event').validationEngine();
	$('.datepicker').datepicker();
});
</script>
	
<?php include('common/footer.php'); ?>