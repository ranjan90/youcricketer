<?php //include_once('includes/configuration.php');
include_once('includes/connection_old.php');

$num_records = 5;
$min_days = 1;
$max_days = 10;
/*	For Individual Users	*/
//$email_template = get_record_on_id('cms', 46);
$email_template = mysql_fetch_assoc(mysql_query('select * from cms where id=46'));
$sql = "SELECT * FROM users WHERE `status` = '2' and activation_reminder_sent = 'no' and is_company = '0' AND DATEDIFF(NOW(),create_date)>=$min_days AND DATEDIFF(NOW(),create_date)<=$max_days ORDER BY DATEDIFF(NOW(),create_date) ASC LIMIT $num_records";
$rs = mysql_query($sql);
$reminder_sent_user_ids = array();
while($row = mysql_fetch_assoc($rs)){
	
	$name = $row['f_name'].' '.$row['m_name'].' '.$row['last_name'];
	$verification_code = $row['verification_code'];
	$email = $row['email'];
	$mail_title		= $email_template['title'];
	$mail_content	= $email_template['content'];
	$mail_content 	= str_replace('{name}',$name, $mail_content);
	$mail_content 	= str_replace('{verification_code}',$verification_code, $mail_content);
	$mail_content 	= str_replace('{verification_link}','<a href="'.WWW.'verify-account.html" title="Verify your account">HERE</a>', $mail_content);
	$mail_content 	= str_replace('{email}',$email, $mail_content);
	$mail_content 	= str_replace('{contact_link}','<a href="'.WWW.'contact-us.html" title="Contact Us">HERE</a>', $mail_content);
	$mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);
						
	$headers = "Content-type:text/html;charset=UTF-8" . "\r\n";
	$headers .= "From: no-reply@youcricketer.com\r\n";			
							
	mail($email,$mail_title,$mail_content,$headers);
	$reminder_sent_user_ids[] = $row['id'];
}

if(!empty($reminder_sent_user_ids)){
	$reminder_sent_user_ids_str = implode(',',$reminder_sent_user_ids);
	$sql = "UPDATE users SET activation_reminder_sent = 'yes' WHERE id IN($reminder_sent_user_ids_str) ";
	mysql_query($sql);
}


/*	For Company Users	*/
//$email_template = get_record_on_id('cms', 47);
$email_template = mysql_fetch_assoc(mysql_query('select * from cms where id=47'));
$sql = "SELECT * FROM users WHERE `status` = '2' and activation_reminder_sent = 'no' and is_company = '1' AND DATEDIFF(NOW(),create_date)>=$min_days AND DATEDIFF(NOW(),create_date)<=$max_days ORDER BY DATEDIFF(NOW(),create_date) ASC LIMIT $num_records";
$rs = mysql_query($sql);
$reminder_sent_user_ids = array();
while($row = mysql_fetch_assoc($rs)){
	
	$name = $row['f_name'].' '.$row['m_name'].' '.$row['last_name'];
	$verification_code = $row['verification_code'];
	$email = $row['email'];
	$mail_title		= $email_template['title'];
	$mail_content	= $email_template['content'];
	$mail_content 	= str_replace('{name}',$name, $mail_content);
	$mail_content 	= str_replace('{verification_code}',$verification_code, $mail_content);
	$mail_content 	= str_replace('{verification_link}','<a href="'.WWW.'verify-account.html" title="Verify your account">HERE</a>', $mail_content);
	$mail_content 	= str_replace('{email}',$email, $mail_content);
	$mail_content 	= str_replace('{contact_link}','<a href="'.WWW.'contact-us.html" title="Contact Us">HERE</a>', $mail_content);
	$mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);
						
	$headers = "Content-type:text/html;charset=UTF-8" . "\r\n";
	$headers .= "From: no-reply@youcricketer.com\r\n";			
							
	mail($email,$mail_title,$mail_content,$headers);
	$reminder_sent_user_ids[] = $row['id'];
}

if(!empty($reminder_sent_user_ids)){
	$reminder_sent_user_ids_str = implode(',',$reminder_sent_user_ids);
	$sql = "UPDATE users SET activation_reminder_sent = 'yes' WHERE id IN($reminder_sent_user_ids_str) ";
	mysql_query($sql);
}
