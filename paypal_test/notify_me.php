
<?php


// STEP 1: read POST data
// Reading POSTed data directly from $_POST causes serialization issues with array data in the POST.
// Instead, read raw POST data from the input stream.
$raw_post_data = file_get_contents('php://input');  send_email($raw_post_data);
$raw_post_array = explode('&', $raw_post_data);
$myPost = array();
foreach ($raw_post_array as $keyval) {
    $keyval = explode ('=', $keyval);
    if (count($keyval) == 2)
        $myPost[$keyval[0]] = urldecode($keyval[1]);
}
// read the IPN message sent from PayPal and prepend 'cmd=_notify-validate'
$req = 'cmd=_notify-validate';
if (function_exists('get_magic_quotes_gpc')) {
    $get_magic_quotes_exists = true;
}
foreach ($myPost as $key => $value) {
    if ($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
        $value = urlencode(stripslashes($value));
    } else {
        $value = urlencode($value);
    }
    $req .= "&$key=$value";
}

// Step 2: POST IPN data back to PayPal to validate
$ch = curl_init('https://ipnpb.sandbox.paypal.com/cgi-bin/webscr');
curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
// In wamp-like environments that do not come bundled with root authority certificates,
// please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set
// the directory path of the certificate as shown below:
// curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
if ( !($res = curl_exec($ch)) ) {
    // error_log("Got " . curl_error($ch) . " when processing IPN data");
    curl_close($ch);
    exit;
}
// inspect IPN validation result and act accordingly
// inspect IPN validation result and act accordingly
if (strcmp ($res, "VERIFIED") == 0) {
    // The IPN is verified, process it:
    // check whether the payment_status is Completed
    // check that txn_id has not been previously processed
    // check that receiver_email is your Primary PayPal email
    // check that payment_amount/payment_currency are correct
    // process the notification
    // assign posted variables to local variables
    $item_name = $_POST['item_name'];
    $item_number = $_POST['item_number'];
    $payment_status = $_POST['payment_status'];
    $payment_amount = $_POST['mc_gross'];
    $payment_currency = $_POST['mc_currency'];
    $txn_id = $_POST['txn_id'];
    $receiver_email = $_POST['receiver_email'];
    $payer_email = $_POST['payer_email'];

    // IPN message values depend upon the type of notification sent.
    // To loop through the &_POST array and print the NV pairs to the screen:
    foreach($_POST as $key => $value) {
        echo $key . " = " . $value . "<br>";
    }
    $_POST['notify_me_result'] = $res;
     send_email($_POST);
} else if (strcmp ($res, "INVALID") == 0) {
    // IPN invalid, log for manual investigation
    echo "The response from IPN was: <b>" .$res ."</b>";
}
curl_close($ch);




/**
 * Send email
 */
function send_email($array)
{
    $to = "jamshaid.ahmed063@gmail.com";
    $email = "sweet.ahmed99@gmail.com";
    $name = "Jamshaid Ahmed";

    $subject = "Testing Paypal IPN";

    $msg = "<table>
            <tr><td>Transaction ID: </td><td>".$array['txn_id']."</td></tr>
            <tr><td>Item: </td><td>".$array['item_name']."</td></tr>
            <tr><td>Amount: </td><td>".$array['mc_gross']."</td></tr>
            <tr><td>Currency: </td><td>".$array['currency']."</td></tr>
            <tr><td>Time: </td><td>".$array['time']."</td></tr>
            <tr><td>Date: </td><td>".$array['date']."</td></tr>
            <tr><td>Timestamp: </td><td>".$array['timestamp']."</td></tr>
            <tr><td>Status: </td><td>".$array['status']."</td></tr>
            <tr><td>First Name: </td><td>".$array['firstname']."</td></tr>
            <tr><td>Last Name: </td><td>".$array['lastname']."</td></tr>
            <tr><td>Email: </td><td>".$array['email']."</td></tr>
            <tr><td>Custom: </td><td>".$array['custom']."</td></tr>
            <tr><td>SLD: </td><td>".$array['sld']."</td></tr>
            </table>";
$msg .= json_encode($array);
    // To send HTML mail, the Content-type header must be set
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

    // Additional headers
    $headers .= "From: $name <$email>" . "\r\n";
    // Mail it to client.
    mail($to, $subject, $msg, $headers);
    return 1;
}


?>