<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? }
 ?>
	<div class="middle">
		<h1>Company Information</h1>
		<?php $company_type_editable = true;
		$company_type_info = get_record_on_id('company_types', $row_comp['company_type_id']);
		
		// remove after updating permalinks
		/*$sql = "Select * from companies ";
		$rs = mysqli_query($conn,$sql);
		while($row_data = mysqli_fetch_assoc($rs)){
			$company_name = trim($row_data['company_name']);
			$company_id = $row_data['id'];
			$company_permalink = '';		
					if(!empty($row_data['company_name'])){
						if(strpos($company_name,' ') === false){
							$company_permalink = strtolower($company_name);
						}else{
							$name_arr = explode(' ',$company_name);
							for($i=0;$i<count($name_arr);$i++){
								if(preg_match('/^([a-z]+)$/i',$name_arr[$i]))
									$company_permalink.=substr($name_arr[$i],0,1);
							}
							$company_permalink = strtolower($company_permalink);
						}
						$sql = "Select id from companies Where company_permalink = '$company_permalink'  ";
						$rs_result = mysqli_query($conn,$sql);
						if(mysqli_num_rows($rs_result) > 0){
							$sql = "Select id,company_permalink from companies Where company_permalink LIKE '{$company_permalink}-%' ORDER BY id DESC LIMIT 1 ";
							$rs_result = mysqli_query($conn,$sql);	
							if(mysqli_num_rows($rs_result) == 0){
								$number = 1;
							}else{
								$row_permalink = mysqli_fetch_assoc($rs_result);
								$number = substr($row_permalink['company_permalink'],strpos($row_permalink['company_permalink'],'-')+1)+1;	
							}
							
							$company_permalink = $company_permalink.'-'.$number;
						}
						$sql = "Update companies SET company_permalink = '$company_permalink' Where id = ".$company_id;
						mysqli_query($conn,$sql);
					}
		}*/
		
		/*if(isset($row_comp) && in_array($row_comp['company_type_id'],array(1,2,3,35)) ){ 
			$sql = "Select c.id,t.id as tournament_id, lc.id as club_id, lw.id as umpire_id, lv.id as venue_id 
			from companies as c left join tournaments as t on c.id=t.league_id 
			left join league_clubs as lc on c.id=lc.league_id 
			left join league_umpires as lw on c.id=lw.league_id 
			left join league_venues as lv on c.id=lv.league_id Where c.id = ".$row_comp['id'].' LIMIT 1';
			$rs = mysqli_query($conn,$sql);
			$league_data = mysqli_fetch_assoc($rs);
			if(empty($row_comp['by_laws_file']) && empty($league_data['tournament_id']) && empty($league_data['club_id']) && empty($league_data['umpire_id']) && empty($league_data['venue_id']) ){
				$company_type_editable = true;
			}else{
				$company_type_info = get_record_on_id('company_types', $row_comp['company_type_id']);
				$company_type_editable = false;
			}
		}
		
		if(isset($row_comp) && in_array($row_comp['company_type_id'],array(4)) ){ 
			$sql = "Select member_id from club_members Where club_id = ".$row_comp['id'].' LIMIT 1';
			$rs = mysqli_query($conn,$sql);
			if(mysqli_num_rows($rs)){
				$company_type_editable = false;
				$company_type_info = get_record_on_id('company_types', $row_comp['company_type_id']);
			}else{
				$company_type_editable = true;
			}
		}*/
		?>

		<? 	if(isset($_POST) && !empty($_POST)){
			
			if($_POST['is_newsletter'] == 'on'){
					$isNews = '1';
				}else{
					$isNews = '0';
				}

				$set[] 		= "date_of_birth = '".$_POST['year'].'-'.$_POST['month'].'-'.$_POST['day']."'";
				$set[] 		= "gender = '".$_POST['gender']."'";
				$set[] 		= "f_name = '".$_POST['f_name']."'";
				$set[] = "is_newsletter = '".$isNews."'";
				$set[] 		= "last_name = '".$_POST['last_name']."'";
				$set[] 		= "country_id = '".$_POST['country_id']."'";
				$set[] 		= "timezone = '".$_POST['timezone']."'";
				
				
			//pr($set);exit;	
				/*
				$rsCheck  = mysqli_fetch_assoc(mysqli_query($conn,"select * from users where yc_email = '".$_POST['yc_email']."'"));
				if(isset($_POST['yc_email']) && mysqli_num_rows($rsCheck) == 0){
					$set[] = "yc_email = '".$_POST['yc_email']."'";
				}else{
					echo '<div id="error">Email Letters already exists ... !</div>';
				}
				*/
				$rsCheck  = mysqli_fetch_assoc(mysqli_query($conn,"select * from users where permalink = '".$_POST['permalink']."'"));
				if(isset($_POST['permalink'])){
					if(mysqli_num_rows($rsCheck) == 0){
						$set[] = "permalink = '".$_POST['permalink']."'";
					}
				}

				$query  = "update users set ".implode(',', $set)." where id = '".$row_user['id']."'";
				//echo $query;exit;
				
				if(mysqli_query($conn,$query)){
					$update[] =  "company_name = '".$_POST['company_name']."'";
					if(isset($_POST['name'])) $update[] =  "company_type_id = '".$_POST['name']."'";
					$company_name = trim($_POST['company_name']);
					$company_id = $row_comp['id'];
					
					if(!empty($_POST['company_name']) && $_SESSION['company_name'] != trim($_POST['company_name']) ){
						if(strpos($company_name,' ') === false){
							$company_permalink = strtolower($company_name);
						}else{
							$name_arr = explode(' ',$company_name);
							for($i=0;$i<count($name_arr);$i++){
								if(preg_match('/^([a-z]+)$/i',$name_arr[$i]))
									$company_permalink.=substr($name_arr[$i],0,1);
							}
							$company_permalink = strtolower($company_permalink);
						}
						$sql = "Select id from companies Where company_permalink = '$company_permalink' AND id != ".$row_comp['id'];
						$rs_result = mysqli_query($conn,$sql);
						if(mysqli_num_rows($rs_result) > 0){
							$sql = "Select id,company_permalink from companies Where company_permalink LIKE '{$company_permalink}-%' AND id != ".$row_comp['id']." ORDER BY id DESC LIMIT 1 ";
							$rs_result = mysqli_query($conn,$sql);	
							if(mysqli_num_rows($rs_result) == 0){
								$number = 1;
							}else{
								$row_permalink = mysqli_fetch_assoc($rs_result);
								$number = substr($row_permalink['company_permalink'],strpos($row_permalink['company_permalink'],'-')+1)+1;	
							}
							
							$company_permalink = $company_permalink.'-'.$number;
						}
						$update[] =  "company_permalink = '".$company_permalink."'";
						$_SESSION['company_name'] = $_POST['company_name'];
					}
					
					$sql = "Update companies set ".implode(',', $update)." Where user_id=".$row_user['id'];
					mysqli_query($conn,$sql);
				    ?>
	                <script>
	                	window.location.href = "<?=WWW?>account-information-company.html";
	                </script>
	                <?
				}else{
					echo '<div id="error">Information cannot be updated ... !</div>'.mysqli_error($conn);
				}
			}

		?>
		<div class="white-box content" id="dashboard">
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
			<form method="post" action="" enctype="multipart/form-data">
				<fieldset>
					
					<h2>Administrator & Company Information</h2>
					<div class="form-box">
						<label>First Name </label>
						<input type="text" style="width: 253px;"value="<?=$row_user['f_name']?>" name="f_name" class="input-login validate[required]">
					</div>
					<div class="form-box">
						<label>Last Name </label>
						<input type="text" style="width: 250px;" value="<?=$row_user['last_name']?>" name="last_name" class="input-login">
					</div>
					<div class="clear"></div>
					<div class="form-box">
							<label>Birthday </label>
							<? $date = explode('-', $row_user['date_of_birth']);?>
							<select style="width:90px;" name="year" class="validate[required]">
								<option value="" selected="selected">Year</option>
								<? for($x = (date('Y')-100); $x < (date('Y')-12); $x++) { ?>
								<option <?=($date[0] == $x)?'selected="selected"':'';?> value="<?=$x?>"><?=$x?></option>
								<? } ?>
							</select>
							<select style="width:90px;" name="month" class="validate[required]">
								<option value="" selected="selected">Month</option>
								<option <?=($date[1] == 1)?'selected="selected"':'';?> value="1">Jan</option>
								<option <?=($date[1] == 2)?'selected="selected"':'';?> value="2">Feb</option>
								<option <?=($date[1] == 3)?'selected="selected"':'';?> value="3">Mar</option>
								<option <?=($date[1] == 4)?'selected="selected"':'';?> value="4">Apr</option>
								<option <?=($date[1] == 5)?'selected="selected"':'';?> value="5">May</option>
								<option <?=($date[1] == 6)?'selected="selected"':'';?> value="6">Jun</option>
								<option <?=($date[1] == 7)?'selected="selected"':'';?> value="7">Jul</option>
								<option <?=($date[1] == 8)?'selected="selected"':'';?> value="8">Aug</option>
								<option <?=($date[1] == 9)?'selected="selected"':'';?> value="9">Sep</option>
								<option <?=($date[1] == 10)?'selected="selected"':'';?> value="10">Oct</option>
								<option <?=($date[1] == 11)?'selected="selected"':'';?> value="11">Nov</option>
								<option <?=($date[1] == 12)?'selected="selected"':'';?> value="12">Dec</option>
							</select>
							<select style="width:85px;" name="day" class="validate[required]">
								<option value="" selected="selected">Day</option>
								<? for($x = 1; $x <= 31; $x++) { ?>
								<option value="<?=$x?>" <?=($x == $date[2])?'selected="selected"':'';?>><?=$x?></option>
								<? } ?>
							</select>
						</div>
						<div class="form-box">
							<label><a class="popup-link" href="#why-do-we-need-your-birthday">Why do we need your birthday?</a></label>
						</div>
						<div class="clear"></div>
						<div class="form-box">
							<label>Gender </label>
							<div class="text" style="margin-right:-5px;"><?=get_gender_combo($row_user['gender']);?></div>
						</div>
						<div class="clear"></div>
						<div class="form-box">
						<label>Country of <br>Residence </label>
						<?=get_combo('countries','name',$row_user['country_id'],'country_id')?>
					</div>
					<div class="clear"></div>
					
					<div class="form-box">
						<label>Title </label>
						<input type="text" style="width: 253px;" value="<?php if(isset($_POST['company_name'])) echo $_POST['company_name'];else echo $row_comp['company_name']; ?>" name="company_name" id="company_name" class="input-login validate[required]" placeholder="Type Club/League/Company Name" maxlength="60">
					</div>
					<div class="clear"></div>
					<div class="form-box" >
						<label>Registered As </label>
						
							<?//=get_combo('user_types','name',$row_user['user_type_id'],'','text')?> 
							<?php if($row_comp['tournament_data_added'] == 'no'): ?>
								<?=get_combo('company_types','name',$row_comp['company_type_id'],'')?>
							<?php else: ?>
								<div class="text"><?php echo $company_type_info['name']; ?></div>
							<?php endif; ?>
						
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Email Address </label>
						<div class="text"><?=$row_user['email']?></div>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Registered On </label>
						<div class="text"><?=date_converter($row_user['create_date'])?></div>
					</div>
					<div class="clear"></div>
					
					<div class="form-box">
						<label>Timezone </label>
						<?=get_combo('timezones','title',$row_user['timezone'],'timezone')?>
					</div>
					<div class="clear"></div>
					<?php /* ?><div class="form-box" style="width:30%;">
							<label>Newsletter Subscription : </label>
							<input type="checkbox" name="is_newsletter" <?php echo ($row_user['is_newsletter'] == '1')?'checked="checked"':'';?>>
					</div><?php */ ?>
						
						<div class="form-box" style="width:100% !important;">
							<label>Refferal Acceptance Counter : </label>
							<div class="text" style="text-align:left;width:74%;">
							<? 	$query 		= "select * from invitations where from_user_id = '$userid' and status = 'Invitation Accepted'";
								$rs_friends = mysqli_query($conn,$query);
								echo mysqli_num_rows($rs_friends);
							?>&nbsp;&nbsp;&nbsp;&nbsp;
							<a class="popup-link" style="font-weight:normal;" href="#what-is-this-counter-for">What is this Counter for?</a>
							</div>
						</div>
						<div class="clear" style="height:8px;"></div>
					<div class="form-box hide">
							<label>YC Email : </label>
							<? if(empty($row_user['yc_email'])){ ?>
								<input type="text" style="width: 253px;"name="yc_email" maxlength="255" class="input-login validate[required]" value="<?=strtolower(str_replace(' ','',$row_comp['company_name']))?>">
							<? }else{ ?>
								<div class="text"><?=$row_user['yc_email']?>@youcricketer.com</div>
							<? } ?>
						</div>
						<div class="form-box hide">
							<? if(empty($row_user['yc_email'])){ ?>
							<div class="left">@youcricketer.com</div>
							<? } ?>
							<div class="left" style="position:relative; top:-10px;" id="yc_email_status">
								<? if(empty($row_user['yc_email'])){ ?>
								<img src="<?=WWW?>images/sucess-icon.png">
								<? } ?>
							</div>
							<? if(empty($row_user['yc_email'])){ ?>
							<input type="hidden" name="yc_email_value">
							<label>Only Characters allowed</label>
							<? } ?>
						</div>
						<div class="clear"></div>
						<?php /* ?><div class="form-box" style="<?=(empty($row_user['permalink']))?'':'width:100%;';?>">
							<label>YC Profile <br>Permalink : </label>
							<? if(empty($row_user['permalink'])){ ?>
								<input type="text" style="width: 253px;" name="permalink" value="<?=strtolower(str_replace(' ','',$row_comp['company_name']))?>" maxlength="255"class="input-login validate[required]">
							<? }else{ ?>
								<div class="text" style="float:left; margin-left:50px;"><?=WWW?>yc<?=($row_user['is_company'] == '0')?'i':'c';?>/<?=$row_user['permalink']?></div>
							<? } ?>
						</div><?php */ ?>
						
						<div class="form-box" style="width:100%;">
							<label>YC Profile <br>Permalink : </label>
								<div class="text" style="float:left; margin-left:50px;"><?=WWW?><?php echo $row_comp['company_permalink'];  ?></div>
						</div>
						
						<div class="form-box" >
							<div class="left" style="position:relative; top:-10px;" id="permalink_status">
								<? if(empty($row_user['permalink'])){ ?>
								<img src="<?=WWW?>images/sucess-icon.png">
								<? } ?>
							</div>
							<? if(empty($row_user['permalink'])){ ?>
							<input type="hidden" name="permalink_value">
							<label>Only Characters allowed</label>
							<? } ?>
						</div>
						
						<div class="clear"></div>
					<div class="form-box">
						<a href="<?=WWW?>dashboard.html" class="submit-login left">Back</a>
					</div>
					<div class="form-box">
						<input type="submit" value=" Update " class="submit-login">
					</div>
				</fieldset>
			</form>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>

<div class="popup hide" id="what-is-this-counter-for" style="width:400px; margin-top:-500px;">
	<h1 style="font-size: 14px;">What is this counter for ? 
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h1>
		<? $row = get_record_on_id('cms',14);?>
		<div>
			<?=$row['content']?>
		    <div class="clear"></div>
		</div>
</div>
<div class="popup hide" id="why-do-we-need-your-birthday" style="width:400px; margin-top: -650px;">
	<h1 style="font-size: 14px;">Why do we need your Birthday 
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h1>
		<? $row = get_record_on_id('cms',7);?>
		<div>
			<?=$row['content']?>
		    <div class="clear"></div>
		</div>
</div>

<script src="<?php echo WWW?>js/jquery-1.8.0.js" type="text/javascript"></script>
<script>
	$(document).ready(function(){
		<? if(empty($row_user['timezone'])){ ?>
		var timezone = jstz.determine();
		$.ajax({type	: 'POST', 
				url		: WWW + 'includes/get-record.php', 
				data	: ({table:'timezones',field:'id',id:timezone.name()}),
				success	: function(msg){
					$('select#timezone').val(msg);	
				}
		});
		<? } ?>
		
		$('a.popup-link').click(function(){
			var link = $(this).attr('href');
			$(link).removeClass('hide');
		});
		
		$('.close-popup').click(function(){
			$('.popup').addClass('hide');
		});
		

	});
</script>
<?php include('common/footer.php'); ?>