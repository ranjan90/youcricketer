<?php include('common/header.php');

$site_key = '6LdcVRETAAAAAD91vVdDOQLo_bhqq-3MUyHBi2nS';
$secret_key = '6LdcVRETAAAAAJgLM7q9C6SqsXHWwt6B2ccePxHo';
 ?>
 <script src='https://www.google.com/recaptcha/api.js'></script>
<? if(isset($_SESSION['ycdc_user_name']) 
	&& !empty($_SESSION['ycdc_user_name'])){ ?>
		<script>
		window.location = '<?=WWW?>dashboard.html';
		</script>
<?  }?>
	<div class="middle">
		<h1>Sign Up as Club/League/Company</h1>

		<? 	if(isset($_POST) && !empty($_POST)){
			$error = ''; 
			
			if(isset($_POST['g-recaptcha-response'])){
				$captcha=$_POST['g-recaptcha-response'];
			}
			if(!$captcha){
				$error = 'Invalid Human Verification';
			}
			$response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret_key&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
			if($response.success==false)
			{
				$error = 'Invalid Human Verification';
			}
			
			$rowEmail = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_POST['email']."' "));
			if(!empty($rowEmail)){
				$error = 'Email Address already exist, please enter different email address to register ... !';
			}
			
			if(empty($error)){
				$company_name 		= $_POST['company_name'];
				$company_type_id	= $_POST['company_type_id'];
				$f_name 			= $_POST['f_name'];
				$m_name 			= $_POST['m_name'];
				$last_name 			= $_POST['last_name'];
				$email 				= $_POST['email'];
				$password 			= $_POST['password'];
				$date_of_birth 		= $_POST['year'].'-'.$_POST['month'].'-'.$_POST['day'];
				$gender 			= $_POST['gender'];
				$epassword 			= md5($password);
				$verification_code 	= getRandomWord(12);
				$date 				= date('Y-m-d H:i:s');
				$status 			= '2';
				$c_country_id 		= $_POST['c_country_id'];
				$u_country_id 		= $_POST['u_country_id'];
				$user_type_id 		= 17;
				$is_company 		= 1;

				$name 	= $_POST['f_name'].' '.$_POST['m_name'].' '.$_POST['last_name'];

				$query  = "insert into users (date_of_birth, gender, f_name, m_name, last_name, user_type_id, country_id, email, password, status, verification_code, create_date, is_company) 
				values ('$date_of_birth','$gender','$f_name','$m_name','$last_name','$user_type_id','$u_country_id','$email','$epassword','2','$verification_code','$date','$is_company');";
				//echo $query;exit;
				
				$row_u  = get_record_on_id('user_types',$user_type_id);
				$row_c  = get_record_on_id('countries', $_POST['country_id']);

				if(mysqli_query($conn,$query)){
					$user_id = mysqli_insert_id($conn);
					
					//permalink code start
					if(!empty($_POST['company_name'])  ){
						if(strpos($company_name,' ') === false){
							$company_permalink = strtolower($company_name);
						}else{
							$name_arr = explode(' ',$company_name);
							for($i=0;$i<count($name_arr);$i++){
								if(preg_match('/^([a-z]+)$/i',$name_arr[$i]))
									$company_permalink.=substr($name_arr[$i],0,1);
							}
							$company_permalink = strtolower($company_permalink);
						}
						$sql = "Select id from companies Where company_permalink = '$company_permalink'  ";
						$rs_result = mysqli_query($conn,$sql);
						if(mysqli_num_rows($rs_result) > 0){
							$sql = "Select id,company_permalink from companies Where company_permalink LIKE '{$company_permalink}-%'  ORDER BY id DESC LIMIT 1 ";
							$rs_result = mysqli_query($conn,$sql);	
							if(mysqli_num_rows($rs_result) == 0){
								$number = 1;
							}else{
								$row_permalink = mysqli_fetch_assoc($rs_result);
								$number = substr($row_permalink['company_permalink'],strpos($row_permalink['company_permalink'],'-')+1)+1;	
							}
							
							$company_permalink = $company_permalink.'-'.$number;
						}
					}
					//permalink code end
					
					$query = "insert into companies (user_id, company_name, country_id, company_type_id,  service_offering, status,company_permalink) 
					values ('$user_id','$company_name','$c_country_id','$company_type_id','$service_offering','2','$company_permalink');";
					mysqli_query($conn,$query);
					
					//echo mysqli_error($conn).$query;exit;
					
					if(isset($_SESSION['ref_invitation'])){ 
						$ref_row	= mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM invitations WHERE id  ='".$_SESSION['ref_invitation']."'"));
						$ref_email 	= $ref_row['to_emails']; 
						$result 	= mysqli_query($conn,"update invitations set accept_status = '1' where id ='".$_SESSION['ref_invitation']."'");
						$from_user_id=$ref_row['from_user_id'];
						mysqli_query($conn,"insert into friendship (from_user_id, to_user_id, create_date) values ('$from_user_id','$user_id','$date'); ");
						unset($_SESSION['ref_invitation']);
					}
					
					if(!is_dir('users/'.$user_id)){
						mkdir('users/'.$user_id,0777);
					}
					
					//sending to user
					$email_template = get_record_on_id('cms', 10);
					$mail_title		= $email_template['title'];
	                $mail_content	= $email_template['content'];
	                $mail_content 	= str_replace('{name}',$name, $mail_content);
	                $mail_content 	= str_replace('{verification_code}',$verification_code, $mail_content);
	                $mail_content 	= str_replace('{verification_link}','<a href="'.WWW.'verify-account.html?id='.$user_id.'" title="Verify your account">HERE</a>', $mail_content);
	                $mail_content 	= str_replace('{email}',$_POST['email'], $mail_content);
	                $mail_content 	= str_replace('{contact_link}','<a href="'.WWW.'contact-us.html" title="Contact Us">HERE</a>', $mail_content);
	                $mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);
						
					$headers = "Content-type:text/html;charset=UTF-8" . "\r\n";
	                $headers .= "From: no-reply@youcricketer.com" ." \r\n";
	                mail($_POST['email'],$mail_title,$mail_content,$headers);

	                //sending to admin
					$email_template = get_record_on_id('cms', 9);
					$mail_title		= $email_template['title'];
	                $mail_content	= $email_template['content'];
	                $mail_content 	= str_replace('{name}',$name, $mail_content);
	                $mail_content 	= str_replace('{user_type_id}',$row_u['name'], $mail_content);
	                $mail_content 	= str_replace('{country_id}',$row_c['name'], $mail_content);
	                $mail_content 	= str_replace('{user_id}',$user_id, $mail_content);
	                $mail_content 	= str_replace('{email}',$_POST['email'], $mail_content);
	                $mail_content   = str_replace('{create_date}',$date, $mail_content);
	                $mail_content   = str_replace('{admin_panel}','<a href="'.WWW.'admin/">ADMIN PANEL</a>', $mail_content);
	                $mail_content 	= str_replace('{contact_link}','<a href="'.WWW.'contact-us.html" title="Contact Us">HERE</a>', $mail_content);
	                $mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);

	                mail(ADMIN_EMAIL,$mail_title,$mail_content,$headers);

	                ?>
	                <script>
	                	window.location.href = "<?=WWW?>sign-up-successful.html?status=success_company";
	                </script>
	                <?
				}else{
					//echo '<div id="error">Email Address already exist, please enter different email address to register ... !</div>';
					echo '<div id="error">Error in Registering User. Try again later ... !</div>';
				}
			}
		}
		?>
		<?php if(!empty($error)): ?><div id="error"> <?php echo $error; ?> </div><?php endif; ?>
		<div class="white-box content">
			<?php include('common/social-login.php');?> 
			<form method="post" action="" enctype="multipart/form-data">
				<? if(isset($_SESSION['ref_invitation']) & !empty($_SESSION['ref_invitation'])){ ?>
					<input type="hidden" name="ref" value="<?=$_SESSION['ref_invitation'];?>">
				<? } ?>
				<fieldset>
					<h2>Account Information</h2>
					<div class="form-box">
						<label>Title </label>
						<input type="text" value="<?php echo $_POST['company_name']; ?>" name="company_name" class="input-login validate[required]" placeholder="Type Club/League/Company Name" maxlength="60">
					</div>
					<div class="form-box">
						<label>Company Type</label>
						<?=get_combo_signup('company_types','name',$_POST['company_type_id'],'company_type_id', '', '','Select Club/League/Company Type')?>
					</div>
					<div class="form-box">
						<label>Country </label>
						<?=get_combo_signup('countries','name',$_POST['c_country_id'],'c_country_id')?>
					</div>
					<div class="clear"></div>
					<h2>Administrator Information</h2>
					<div class="form-box">
						<label>First Name </label>
						<input type="text" name="f_name" value="<?php echo $_POST['f_name']; ?>" class="input-login validate[required]">
					</div>
					<div class="form-box">
						<label>Last Name </label>
						<input type="text" name="last_name" value="<?php echo $_POST['last_name']; ?>" class="input-login">
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Birthday </label>
						<select style="width:95px;" name="year" class="validate[required]">
							<option value="" selected="selected">Year</option>
							<? for($x = (date('Y')-13); $x > (date('Y')-100); $x--) { ?>
							<option  value="<?=$x?>"<?php if($_POST['year'] == $x){?>selected<?php }?>><?=$x?></option>
							<? } ?>
						</select>
						<select style="width:90px;" name="month" class="validate[required]">
							<option value="" selected="selected">Month</option>
							<option value="1" <?php if($_POST['month'] == 1){?>selected<?php }?>>Jan</option>
							<option value="2" <?php if($_POST['month'] == 2){?>selected<?php }?>>Feb</option>
							<option value="3" <?php if($_POST['month'] == 3){?>selected<?php }?>>Mar</option>
							<option value="4" <?php if($_POST['month'] == 4){?>selected<?php }?>>Apr</option>
							<option value="5" <?php if($_POST['month'] == 5){?>selected<?php }?>>May</option>
							<option value="6" <?php if($_POST['month'] == 6){?>selected<?php }?>>Jun</option>
							<option value="7" <?php if($_POST['month'] == 7){?>selected<?php }?>>Jul</option>
							<option value="8" <?php if($_POST['month'] == 8){?>selected<?php }?>>Aug</option>
							<option value="9" <?php if($_POST['month'] == 9){?>selected<?php }?>>Sep</option>
							<option value="10" <?php if($_POST['month'] ==10){?>selected<?php }?>>Oct</option>
							<option value="11" <?php if($_POST['month'] == 11){?>selected<?php }?>>Nov</option>
							<option value="12" <?php if($_POST['month'] == 12){?>selected<?php }?>>Dec</option>
						</select>
						<select style="width:90px;" name="day" class="validate[required]">
							<option value="" selected="selected">Day</option>
							<? for($x = 1; $x <= 31; $x++) { ?>
							<option value="<?=$x?>"<?php if($_POST['day'] == $x){?>selected<?php }?>><?=$x?></option>
							<? } ?>
						</select>
					</div>
					<div class="form-box">
						<label><a class="popup-link" href="#why-do-we-need-your-birthday">Why do we need your birthday?</a></label>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Gender </label>
						<?php if(isset($_POST['gender'])) $sel = $_POST['gender'];else $sel = '1'; ?>
						<?=get_gender_combo($sel)?>
					</div>
					<div class="form-box">
						<label>Country of <br>Residence </label>
						<?=get_combo_signup('countries','name',$_POST['u_country_id'],'u_country_id')?>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Email Address </label>
						<input type="text" name="email" value="<?php echo $_POST['email']; ?>" class="input-login validate[required, custom[email]]">
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Password </label>
						<input type="password" name="password"  value="<?php if($_POST['password']){echo $_POST['password'];}?>" id="password" class="input-login validate[required, custom[password]]">
					</div>
					<div class="form-box">
						<div id="password-result" class="left"></div>
						<p class="right" style="width:180px; font-size:12px;">Min 8 Characters, 1 digit</p>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Confirm <br>Password </label>
						<input type="password" name="confirm_password" value="<?php if($_POST['confirm_password']){echo $_POST['confirm_password'];}?>"class="right input-login validate[required,equals[password]]">
					</div>
					<div class="form-box">
						<div id="confirm-password-result" class="left"></div>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						
						<div class="g-recaptcha" data-sitekey="6LdcVRETAAAAAD91vVdDOQLo_bhqq-3MUyHBi2nS"></div>
					</div>
					<div class="clear"></div>
					<div class="form-box" style="width:100%;">
						<p style="padding:0 10px">
							By clicking Sign Up, you agree to our <a class="no-bg no-padding" target="_blank" href="<?=WWW?>terms-of-use.html" title="Terms of Use - <?=$site_title?>">Terms of Use</a>
							 and that you have read our <a class="no-bg no-padding" target="_blank" href="<?=WWW?>multimedia-policy.html" title="MultiMedia Policy - <?=$site_title?>">MultiMedia Policy</a>, 
							including our <a target="_blank" class="no-bg no-padding" href="<?=WWW?>cookie-use.html" title="Privacy Policy & Cookie Use - <?=$site_title?>">Privacy Policy & Cookie Use</a>.
						</p>
						<input type="submit" value=" Submit " class="submit-login">
					</div>
				</fieldset>
			</form>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>
<div class="popup hide" id="why-do-we-need-your-birthday" style="width:400px; margin-top: -950px;">
	<h1 style="font-size: 14px;">Why do we need your Birthday 
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h1>
		<? $row = get_record_on_id('cms',7);?>
		<div>
			<?=$row['content']?>
		    <div class="clear"></div>
		</div>
</div>
<style>
.white-box.content{position: relative;}
.sign_in_with_services a{position:absolute; top: 0px; right: 0px;}
.input-login, select{height: auto;}
#gender{width: 275px;}
</style>
<script src="<?php echo WWW?>js/jquery-1.8.0.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=WWW?>/js/jquery.validationengine.js"></script>
<script type="text/javascript" src="<?=WWW?>js/validation-engine.js"></script>
<script>
$(document).ready(function(){
	$('form').validationEngine();
	$('.close-popup').click(function(){
		$('.popup').addClass('hide');
	});

	$('a.popup-link').click(function(){
		var link = $(this).attr('href');
		if(link == '#edit-activity'){
			var id = this.id;
			$.ajax({type	: 'POST', 
				url		: WWW + 'includes/get-record.php', 
				data	: ({table:'activity',field:'activity',id:id}),
				success	: function(msg){
					$(link+' textarea').val(msg);
					$(link+' #recordId').val(id);
				}
			});
		}
		$(link).removeClass('hide');
	});

	$('input[name=password]').keyup(function(){
		var password = this.value;
		var score = 0;

		if (password.length >= 8)
	        score++;
	    if (password.length >= 10)
	        score++;
	    if (password.match(/^.*(?=.{8,})(?=.*[a-z]).*$/))
	        score++;

	    if(score == '3'){
	    	$('#password-result').html('Strong Password');
	    	$('#password-result').css('background','green');
	    }else if(score == '2'){
	    	$('#password-result').html('Strong Password');
	    	$('#password-result').css('background','green');
	    }else if(score == '1'){
	    	$('#password-result').html('Strong Password');
	    	$('#password-result').css('background','green');
	    }
	});

	$('input[name=confirm_password]').keyup(function(){
		if($('input[name=confirm_password]').val() === $('input[name=password]').val()){
			$('#confirm-password-result').css('background', 'green');
			$('#confirm-password-result').html('Password Matched');
		}
	});

	$('#recaptcha_response_field').addClass('validate[required]');
	$('select[name=user_type_id]').live('change',function(){
		var id = $('select[name=user_type_id]').val();
		if(id != '2'){
			$('#dependent-player').addClass("hide");
		}else{
			$('#dependent-player').removeClass("hide");
		}
	});


	$('select[name=user_type_id]').live('change',function(){
		var id = $('select[name=user_type_id]').val();
		if(id == '2'){
			$('#match-type').removeClass("hide");
		}else{
			$('#match-type').addClass("hide");
		}
	});

	$('select[name=first_class_player]').live('change',function(){
		var id = $('select[name=first_class_player]').val();
		if(id == '1'){
			$('#espn').removeClass('hide');
			$('#espn input[name=espn]').addClass('validate[required, custom[url]]');
		}else{
			$('#espn').addClass('hide');
			$('#espn input[name=espn]').removeClass('validate[required, custom[url]]');
		}
	});

	$('select[name=day], select[name=month], select[name=year]').change(function(){

		var day 	= $('select[name=day]').val();
		var month 	= $('select[name=month]').val();
		var year 	= $('select[name=year]').val();
		
		if(day 	!= ''
		&& month!= ''
		&& year != ''){
			$.ajax({type: 'POST', 
				url		: WWW + 'includes/check-date.php', 
				data	: ({day:day, month:month, year:year}),
				success	: function(msg){
					var msg1 = msg.split('____');
					if(msg1[0] < 13){
						alert("You are not eligible to register, please follow our guidelines for minimum age requirement.");
						$('select[name=day]').val('');
						$('select[name=month]').val('');
						$('select[name=year]').val('');
					}
					if(msg1[1] == ''){
						alert("Invalid date selected ... !");
						$('select[name=day]').val('');
						$('select[name=month]').val('');
						$('select[name=year]').val('');
					}
				}
			});
		}
	});
});
</script>
<?php include('common/footer.php'); ?>