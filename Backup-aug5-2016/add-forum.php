<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	<div class="middle">
		<h1>Add Forum </h1>
		<? 	if(isset($_POST) && !empty($_POST)){ 
				$title 	= addslashes($_POST['title']);
				$content= addslashes($_POST['content']);
				$category=$_POST['category_id'];
				$date 		= date('Y-m-d H:i:s');
				$user_id 	= $_SESSION['ycdc_dbuid'];
				$admin_id 	= $user_id;
				$isGlobal = ($_POST['is_global'] == 'on')?'1':'0';

				if(!isset($user_id) || empty($user_id)){
					echo '<div id="error">Please login first ... !</div>';
				}else{
					if(!empty($title) && !empty($content)){
						$rs_check = mysqli_query($conn,"select * from forum_topics where user_id = '$user_id' and title = '$title'");
						if(mysqli_num_rows($rs_check) == 0){
							$query = "insert into forum_topics (title, content, user_id, status, create_date, category_id, is_global) 
							values ('$title','$content','$user_id','1','$date','$category_id','$isGlobal')";

							if(mysqli_query($conn,$query)){
								$forum_id = mysqli_insert_id();
								
								if(!is_dir('forum/'.$forum_id)){
									mkdir('forum/'.$forum_id,0777);
								}
								chmod('forum/'.$forum_id,0777);

								if(!empty($_FILES['photo']['name'])){
									$filename 	= friendlyURL($title).'.jpg';
									$image 		= new SimpleImage();
									$image->load($_FILES["photo"]["tmp_name"]);
									$image->save('forum/'.$forum_id.'/'.$filename);
									chmod('forum/'.$forum_id.'/'.$filename,0777);
								
									mysqli_query($conn,"insert into photos (file_name, title, entity_id, entity_type) values ('$filename','$title','$forum_id','forum')");
								}
							}

			                echo '<div id="success">Forum Information added successfully ... !</div>';
			                ?>
			                <script>
			                window.location = '<?=WWW?>my-forum.html';
			                </script>
			                <?
						}else{
							echo '<div id="error">Information already exists ... !</div>';
						}
					}else{
						echo '<div id="error">Please fill all fields ... !</div>';
					}
				}
				
			}
		?>
		<div class="white-box content">
			<form method="post" enctype="multipart/form-data">
				<fieldset>
					<h2>Forum Information</h2>
					<div class="form-box">
						<label>Forum Category</label>
						<?=get_combo('forum_categories','name','','category_id')?>
					</div>
					<div class="clear"></div>
					<div class="form-box" style="width:100%;">
						<label>Is Global</label>
						<input style="float:left; margin-left:70px;" type="checkbox" name="is_global">
						<label>Forum will be Regional Forum If checked</label>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Title</label>
						<input type="text" name="title" style="width:253px;" class="input-login validate[required]">
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Content</label>
						<textarea maxlength="500" name="content" style="width:600px; margin-left:125px; height:100px;" class="validate[required]"></textarea>
					</div>
					<div class="clear"></div>
					<h2>Forum Photo</h2>
					<div class="form-box">
						<label>Forum Photo</label>
						<input type="file" name="photo">
					</div>
					<div class="form-box">
						<label>Max Photo size : 2MB</label>
					</div>
					
					<div class="clear"></div>
					<div class="form-box">
						<input type="submit" value=" Save " class="submit-login">
					</div>
					<div class="form-box">
						<a id="cancel" class="submit-login" href="<?=WWW?>blogs.html">Cancel</a>
					</div>
				</fieldset>
			</form>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>
<style>
#cancel{background: url('<?php echo WWW;?>images/sign-in-button.png') no-repeat !important; color:#fff !important; padding-left:3px; top:0px !important;position:relative; left:-150px;}
</style>
<?php include('common/footer.php'); ?>