<?php include_once('includes/configuration.php');
$page = 'testimonials-about-me.html';
$page_title = 'Testimonials About Me';
if(isset($_GET['action']) && $_GET['action'] == 'delete'){
	$id = $_GET['id'];
	$rs_t = mysqli_query($conn,"select * from testimonials where id = $id and entity_id = '".$_SESSION['ycdc_dbuid']."'");
	if(mysqli_num_rows($rs_t)>0){
		mysqli_query($conn,"DELETE from testimonials where id = '$id' LIMIT 1");
		header("Location:testimonials-about-me.html");
		exit();
	}
}
include('common/header.php'); 
?>
	<div class="middle">
		<h1>
			 Testimonials About Me		
		</h1>
		<div class="white-box content" id="dashboard">
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
			<div id="pagination-top"></div>
			<div class="list">
                <ul>
                	<?  $rpp = PRODUCT_LIMIT_FRONT; // results per page
            			$ppage = intval($_GET["page"]);
      					if($ppage<=0) $ppage = 1;
      					$query = "select * from testimonials where status=1 and entity_id = '".$_SESSION['ycdc_dbuid']."'";
				      	//=======================================
					    if(isset($_GET['keywords']) && $_GET['keywords'] != 'Search Here'){
					        $query  .= "and content like '%".str_replace('-','%',$_GET['keywords'])."%'";
					    }
					    
					    $query .= " order by id desc ";
				      //echo $query;
				      //=======================================
				      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
				        echo '<div id="information">There are no Testimonial for this Individual</div>';
				      }
				      $rs   = mysqli_query($conn,$query);
				      $tcount = mysqli_num_rows($rs);
				      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
				      $count = 0;
				      $i = ($ppage-1)* $rpp;
          			  $x = 0;
          			  //=======================================
      				  while(($count<$rpp) && ($i<$tcount)){
                		mysqli_data_seek($rs,$i);
                		$row_t 	= mysqli_fetch_array($rs);
                		$row_i  = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_t['user_id']."' and is_default = '1'"));
						
                		?>
      					<div class="testi-box">
						<table class="testimonial"><tr><td>  <?php if(!empty($row_i['file_name']) && file_exists('users/'.$row_t['user_id'].'/photos/'.$row_i['file_name'])){?>
							<img style="margin:4px" src="<?=WWW?><?='users/'.$row_t['user_id'].'/photos/'.$row_i['file_name']?>" alt="<?=get_combo('users','concat(f_name," ",last_name)',$row_t['user_id'],'','text')?>" width="40">
							<?php }else{ ?>
								<img  src="<?=WWW?>images/no-photo.png" width="70" height="65">
							<?php } ?></td>
							<td valign="top"><h2><?=get_combo('users','concat(f_name," ",last_name)',$row_t['user_id'],'','text')?> for </h2></td>
							
							<td><?php $row_i  = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_t['entity_id']."' and is_default = '1'")); ?>
							 <?php if($row_i['file_name'] && file_exists('users/'.$row_t['entity_id'].'/photos/'.$row_i['file_name'])){?>
							<img style="margin:4px" src="<?=WWW?><?='users/'.$row_t['entity_id'].'/photos/'.$row_i['file_name']?>" alt="<?=get_combo('users','concat(f_name," ",last_name)',$row_t['entity_id'],'','text')?>" width="40">
							<?php } else { ?>
								<img  src="<?=WWW?>images/no-photo.png" width="70" height="65">
							<?php } ?></td>
							
							<td><h2><?=get_combo('users','concat(f_name," ",last_name)',$row_t['entity_id'],'','text')?> on <?=date_converter($row_t['testimonial_date'],'M d,Y H:i')?></h2></td>
							<td><div class="stars">
								 
	                            <a onclick="return confirm('Are you sure to delete Testimonial');" href="<?=WWW?>testimonials-about-me.html?id=<?=$row_t['id']?>&action=delete"><img src="<?=WWW?>images/icons/delete.png"></a>
	                           
							<? 	$rating = $row_t['rating'];
								for($x = 0; $x < 5; $x++){
									if($x < $rating){
									?><img src="<?=WWW?>images/star-fill-gray.jpg" title="<?=$rating?>/5"><?
									}else{
									?><img src="<?=WWW?>images/star-gray.jpg" title="<?=$rating?>/5"><?
									}
								}
							?>
							</div></td>
						
						</tr>
						<tr><td></td><td colspan="6" style="padding-left:1px;"><p><?=$row_t['content']?></p></td></tr>
						</table>
					</div>
					    <?
					      $i++;
					      $count++;
					      $x++;
					  } 
					  ?>
			</ul>
			</div>
			</div>
			<div id="pagination-bottom">
				<? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_FRONT){ ?>
		      	<?php
		        	$reload = 'testimonials.html?';
		        	echo paginate_one($reload, $ppage, $tpages);
		      	?>
		      	<input type="hidden" name="pagination-page" value="testimonials.html">
		        <? } ?>    
		        <form id="list-search" method="post" action="">
		        	<input type="text" name="txtsearch" placeholder="Search Here" class="validate[required] input-login">
		        	<input type="submit" value="">
		        </form>
		        <script type="text/javascript">
		        $('form#list-search').submit(function(e){
					var parentId = $(this).parent().attr('id');
					var string = $('#'+parentId +' form input[name=txtsearch]').val();
					if(string != '' && string != 'Search Here'){
						string = string.replace(/[^a-zA-Z0-9]+/g,'-').toLowerCase();
						if(string.length > 0){
							$('form#list-search').attr('action','<?=WWW;?>test-' + string + '.html');
						}
					}
				});
		        </script>

  <script src="<?php echo WWW?>js/jquery-1.8.0.js" type="text/javascript"></script>
		 <script type="text/javascript">

$(document).ready(function (){

	$('#dashboard .black-box h2').click(function(){
		$('#dashboard .black-box ul').css('display','none');
		var id = this.id;
		$('#'+id+'-box ul').css('display','block');
	});
});
</script>
		        <div class="clear"></div>
		        
		    </div>  
		    <div class="clear"></div>
		</div>
		
		<div class="clear"></div>
	</div>
<? include('common/login-box.php');?>	
<style>
#login-box{display:none;}
.testimonial td{vertical-align:middle;}
.testi-box .stars {padding:8px;}
</style>
<?php include('common/footer.php'); ?>