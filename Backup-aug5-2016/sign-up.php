<?php include('common/header.php');
$site_key = '6LdcVRETAAAAAD91vVdDOQLo_bhqq-3MUyHBi2nS';
$secret_key = '6LdcVRETAAAAAJgLM7q9C6SqsXHWwt6B2ccePxHo';
 ?>
  <script src='https://www.google.com/recaptcha/api.js'></script>
<?  if(isset($_SESSION['ycdc_user_name']) 
	&& !empty($_SESSION['ycdc_user_name'])){ ?>
		<script>
		window.location = '<?=WWW?>dashboard.html';
		</script>
<?  }?>
	<div class="middle">
		<h1>Individual Sign Up </h1>
          
	<? 	

  /// getting reference email address
if(isset($_SESSION['ref'])){ 
			$ref_row = mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM invitations WHERE id  ='".$_SESSION['ref']."'"));
		    $ref_email = $ref_row['to_emails']; 
 }
	 $captcha_error = '';
    if(isset($_POST) && !empty($_POST)){
			$rowEmail = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_POST['email']."' "));
		
			if(isset($_POST['g-recaptcha-response'])){
				$captcha=$_POST['g-recaptcha-response'];
			}
			if(!$captcha){
				$captcha_error = 'Invalid Human Verification';
			}
			$response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret_key&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
			if($response.success==false)
			{
				$captcha_error = 'Invalid Human Verification';
			}
			unset($_POST['g-recaptcha-response']);
			
			if(empty($_POST['f_name'])){
				echo "<div id='error'>Please Enter your First Name </div>";
			}
			else if(empty($_POST['last_name'])){
				echo "<div id='error'>Please Enter your Last Name</div>";
			}
		

		 else if($_POST['gender'] == 'select'){
				echo "<div id='error'>Pleae select Your Gender</div>";
			}
			else if(empty($_POST['country_id'])){
				echo "<div id='error'>Please your country</div>";
			}
			else if(empty($_POST['email'])){
				echo "<div id='error'>Please Enter your Email</div>";
			}

			else if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
 			 {
  		echo "<div id='error'>Please Enter Valid  Email</div>";
 				 }

			else if(empty($_POST['password'])){
				echo "<div id='error'>Please Enter your Password</div>";
			}
			else if(strlen($_POST['password']) < 8 && !preg_match("/[a-z]/",$_POST['password']) && !preg_match("/[0-9]/",$_POST['password']) ){
					echo "<div id='error'>Password Should be 8 charactors and 1 digit ..!</div>";
				
			}	
			elseif(!empty($captcha_error)){
				echo '<div id="error">'.$captcha_error.'</div>';
			}
			elseif(!empty($rowEmail)){
				echo '<div id="error">Email Address already exist, Please enter different email address to register ... !</div>';
			}else {
				
				
				//////
				$fields_a = array();
				$values_a = array();
				
              $refEmail = $_POST['email'];
				if(!checkdate($_POST['month'], $_POST['day'], $_POST['year'])){
					echo '<div id="error">Please enter valid date ... !</div>';
				}else{
					foreach($_POST as $key=>$value){
						if($key == 'password'){
							$value = md5($_POST[$key]);
						}
						if($key != 'year'
						&& $key != 'month'	
						&& $key != 'day'
						&& $key != 'confirm_password'
						&& $key != 'recaptcha_challenge_field'
						&& $key != 'recaptcha_response_field'
						&& $key != 'ref'
						&& $key != 'timezone'){

							if($key == 'player_type'){
								$fields_a[] = 'type';
								$values_a[] = $value;
							}else if($key == 'batsman_type'){
								$fields_a[] = 'type_of_batsman';
								$values_a[] = $value;
							}else if($key == 'batsman_type'){
								$fields_a[] = 'type_of_batsman';
								$values_a[] = $value;
							}else if($key == 'bowler_type'){
								$fields_a[] = 'type_of_bowler';
								$values_a[] = $value;
							}else if($key == 'bowling_arm'){
								$fields_a[] = 'which_arm_bowler';
								$values_a[] = $value;
							}else if($key == 'bowling_prefer'){
								$fields_a[] = 'bowl_between_overs';
								$values_a[] = $value;
							}else if($key == 'espn'){
								$fields_a[] = 'espncrickinfo';
								$values_a[] = $value;
							}else if($key == 'matchType'){
								$fields_a[] = 'match_types';
								$values_a[] = implode(',', $value);
							}else{
								$fields_a[] = $key;
								$values_a[] = $value;	
							}
						}
					}

					$verification_code 	= getRandomWord(12);
					$date 				= date('Y-m-d H:i:s');
					$fields_a[] = 'status';
					$values_a[] = '2';
					$fields_a[] = 'date_of_birth';
					$values_a[] = $_POST['year'].'-'.$_POST['month'].'-'.$_POST['day'];
					$fields_a[] = 'verification_code';
					$values_a[] = $verification_code;
					$fields_a[] = 'create_date';
					$values_a[] = $date;
					$fields_a[] = 'is_company';
					$values_a[] = '0';

					//check timezone
					$zn 		= "select * from timezones where title like '%".$_POST['timezone']."%'";
					$row_tz 	= mysqli_fetch_assoc(mysqli_query($conn,$zn));
					$timezone_id= $row_tz['id'];
					$fields_a[] = 'timezone';
					$values_a[] = $timezone_id;
					$values_a  = array_map('trim' , $values_a);
					$fields = implode(",",$fields_a);
					$values = implode("','",$values_a);

					$name 	= $_POST['f_name'].' '.$_POST['m_name'].' '.$_POST['last_name'];

					$query  = "insert into users ($fields) values ('$values');";
					//echo $query;exit;
					$row_u  = get_record_on_id('user_types',$_POST['user_type_id']);
					$row_c  = get_record_on_id('countries', $_POST['country_id']);

					if(mysqli_query($conn,$query)){
						$user_id = mysqli_insert_id();
						if(isset($_SESSION['ref_invitation'])){ 
							$ref_row	= mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM invitations WHERE id  ='".$_SESSION['ref_invitation']."'"));
							$ref_email 	= $ref_row['to_emails']; 
							$result 	= mysqli_query($conn,"update invitations set accept_status = '1' where id ='".$_SESSION['ref_invitation']."'");
							$from_user_id=$ref_row['from_user_id'];
							mysqli_query($conn,"insert into friendship (from_user_id, to_user_id, create_date, status) 
								values ('$from_user_id','$user_id','$date', '0'); ");
							unset($_SESSION['ref_invitation']);
						}
						if(!is_dir('users/'.$user_id)){
							mkdir('users/'.$user_id,0777);
						}
						/*
						if(!empty($_FILES['photo']['name'])){
							chmod('users/'.$user_id,0777);
							$filename 	= friendlyURL($name).'.jpg';
							$image 		= new SimpleImage();
							$image->load($_FILES["photo"]["tmp_name"]);
							$image->save('users/'.$user_id.'/'.$filename);
							chmod('users/'.$user_id.'/'.$filename,0777);
							mysqli_query($conn,"insert into photos (file_name, title, entity_id, entity_type, is_default, is_thumb)
							 values ('$filename','$name','$user_id','users','1','0')");

							if(!is_dir('users/'.$user_id.'/thumbnails')){
								mkdir('users/'.$user_id.'/thumbnails',0777);
							}
							chmod('users/'.$user_id.'/thumbnails',0777);
							$image->load($_FILES["photo"]["tmp_name"]);
							$image->resizeToWidth('128');
							$image->save('users/'.$user_id.'/thumbnails/'.$filename);
							chmod('users/'.$user_id.'/thumbnails/'.$filename,0777);
						}
						*/
						//sending to user
						$email_template = get_record_on_id('cms', 1);
						$mail_title		= $email_template['title'];
		                $mail_content	= $email_template['content'];
		                $mail_content 	= str_replace('{name}',$name, $mail_content);
		                $mail_content 	= str_replace('{verification_code}',$verification_code, $mail_content);
		                $mail_content 	= str_replace('{verification_link}','<a href="'.WWW.'verify-account.html" title="Verify your account">HERE</a>', $mail_content);
		                $mail_content 	= str_replace('{email}',$_POST['email'], $mail_content);
		                $mail_content 	= str_replace('{contact_link}','<a href="'.WWW.'contact-us.html" title="Contact Us">HERE</a>', $mail_content);
		                $mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);
						
							$headers = "Content-type:text/html;charset=UTF-8" . "\r\n";
							$headers .= "From: no-reply@youcricketer.com\r\n";			
							
		                mail($_POST['email'],$mail_title,$mail_content,$headers);

		                //sending to admin
						$email_template = get_record_on_id('cms', 8);
						$mail_title		= $email_template['title'];
		                $mail_content	= $email_template['content'];
		                $mail_content 	= str_replace('{name}',$name, $mail_content);
		                $mail_content 	= str_replace('{user_type_id}',$row_u['name'], $mail_content);
		                $mail_content 	= str_replace('{country_id}',$row_c['name'], $mail_content);
		                $mail_content 	= str_replace('{user_id}',$user_id, $mail_content);
		                $mail_content 	= str_replace('{email}',$_POST['email'], $mail_content);
		                $mail_content   = str_replace('{create_date}',$date, $mail_content);
		                $mail_content   = str_replace('{admin_panel}','<a href="'.WWW.'admin/">ADMIN PANEL</a>', $mail_content);
		                $mail_content 	= str_replace('{contact_link}','<a href="'.WWW.'contact-us.html" title="Contact Us">HERE</a>', $mail_content);
		                $mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);

		                mail(ADMIN_EMAIL,$mail_title,$mail_content,$headers);

		                ?>
		                <script>
		                	window.location.href = "<?=WWW?>sign-up-successful.html?status=success_individual";
		                </script>
		                <?
					}else{ 
						/*$rowEmail = mysqli_fetch_assoc(mysqli_query($conn,"select * from users where email = '$email' "));
						if(empty($rowEmail['password']) && $rowEmail['status'] == '1'){
							echo '<div id="error">Email Address already in our system for marketing, Please click <a href="'.WWW.'forgot-password.html">Retrieve Password</a> to retrieve password ... !</div>';
						}
						if(!$rowEmail){
							echo '<div id="error">Email Address already exist, Please enter different email address to register ... !</div>';
						}*/
						echo '<div id="error">Error in Registering User. Try again later ... !</div>';
					}
				}
			}
		}
		?>
<div class="white-box content">
	<?php include('common/social-login.php');?>
			<form method="post" action="" enctype="multipart/form-data">
				<input type="hidden" name="timezone" value="">
				<? if(isset($_SESSION['ref_invitation']) & !empty($_SESSION['ref_invitation'])){ ?>
					<input type="hidden" name="ref" value="<?=$_SESSION['ref_invitation'];?>">
				<? } ?>
				<fieldset>
					<h2>Account Information</h2>
					<div class="form-box">
						<label>First Name </label>
						<input type="text" name="f_name" value="<?php if($_POST['f_name']){echo $_POST['f_name'];}?>" class="input-login validate[required]" maxlength="30" >
					</div>
					<div class="form-box">
						<label>Last Name </label>
						<input type="text" name="last_name" value="<?php if($_POST['last_name']){echo $_POST['last_name'];}?>"  class="input-login validate[required]"  maxlength="30" >
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Birthday </label>
						<select style="width:90px;" name="year" class="validate[required]">
							<option value="" selected="selected">Year</option>
							<? for($x = (date('Y')-13); $x > (date('Y')-100); $x--) { ?>
							<option  value="<?=$x?>"<?php if($_POST['year'] == $x){?>selected<?php }?>><?=$x?></option>
							<? } ?>
						</select>
						<select style="width:93px;" name="month" class="validate[required]">
							<option value="" selected="selected">Month</option>
							<option value="1" <?php if($_POST['month'] == 1){?>selected<?php }?>>Jan</option>
							<option value="2" <?php if($_POST['month'] == 2){?>selected<?php }?>>Feb</option>
							<option value="3"<?php if($_POST['month'] == 3){?>selected<?php }?>>Mar</option>
							<option value="4"<?php if($_POST['month'] == 4){?>selected<?php }?>>Apr</option>
							<option value="5"<?php if($_POST['month'] == 5){?>selected<?php }?>>May</option>
							<option value="6"<?php if($_POST['month'] == 6){?>selected<?php }?>>Jun</option>
							<option value="7"<?php if($_POST['month'] == 7){?>selected<?php }?>>Jul</option>
							<option value="8"<?php if($_POST['month'] == 8){?>selected<?php }?>>Aug</option>
							<option value="9"<?php if($_POST['month'] == 9){?>selected<?php }?>>Sep</option>
							<option value="10"<?php if($_POST['month'] ==10){?>selected<?php }?>>Oct</option>
							<option value="11"<?php if($_POST['month'] == 11){?>selected<?php }?>>Nov</option>
							<option value="12"<?php if($_POST['month'] == 12){?>selected<?php }?>>Dec</option>
						</select>
						<select style="width:93px;" name="day" class="validate[required]">
							<option value="" selected="selected">Day</option>
							<? for($x = 1; $x <= 31; $x++) { ?>
							<option value="<?=$x?>"<?php if($_POST['day'] == $x){?>selected<?php }?>><?=$x?></option>
							<? } ?>
						</select>
					</div>
					<div class="form-box">
						<label><a class="popup-link" href="#why-do-we-need-your-birthday">Why do we need your birthday?</a></label>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Gender </label>
						<?=get_gender_combo()?>
					</div>
					<div class="form-box">
						<label>Country of <br>Residence </label>
						<?=get_combo_signup('countries','name','','country_id')?>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Registering As </label>
						<select name="user_type_id" id="user_type_id" style="width: 277px;" class="validate[required]">
							<option value="">Select One</option>
							<? 	$rs_uts = mysqli_query($conn,"select * from user_types where id  != '1' and status = '1' order by name;");
								while($row_ut = mysqli_fetch_assoc($rs_uts)){
								?><option <?=($row_ut['id'] == '2')?'selected="selected"':'';?> value="<?=$row_ut['id']?>"><?=$row_ut['name']?></option><?
								}
							?>
						</select>
					</div>
					<div class="form-box">
						<label>You can further categorize yourself in your PROFILE</label>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Email Address </label>
						<input type="text" name="email" value="<?php if($_POST['email']){echo $_POST['email'];} if(!empty($ref_email)){echo $ref_email;}?>" class="input-login validate[required, custom[email]]">
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Password </label>
						<input type="password" name="password"  value="<?php if($_POST['password']){echo $_POST['password'];}?>"id="password" class="input-login validate[required, custom[password]]">
					</div>
					<div class="form-box">
						<div id="password-result" class="left"></div>
						<p class="right" style="width:180px; font-size:12px;">Min 8 Characters, 1 digit</p>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Confirm <br>Password </label>
						<input type="password" name="confirm_password" value="<?php if($_POST['confirm_password']){echo $_POST['confirm_password'];}?>"class="right input-login validate[required,equals[password]]">
					</div>
					<div class="form-box">
						<div id="confirm-password-result" class="left"></div>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						
                        <div class="g-recaptcha" data-sitekey="6LdcVRETAAAAAD91vVdDOQLo_bhqq-3MUyHBi2nS"></div>
					</div>
					<div class="clear"></div>
					<div class="form-box" style="width:100%;">
						<p style="padding:0 10px">
							By clicking Sign Up, you agree to our <a class="no-bg no-padding" target="_blank" href="<?=WWW?>terms-of-use.html" title="Terms of Use - <?=$site_title?>">Terms of Use</a>
							 and that you have read our <a class="no-bg no-padding" target="_blank" href="<?=WWW?>multimedia-policy.html" title="MultiMedia Policy - <?=$site_title?>">MultiMedia Policy</a>, 
							including our <a target="_blank" class="no-bg no-padding" href="<?=WWW?>cookie-use.html" title="Privacy Policy & Cookie Use - <?=$site_title?>">Privacy Policy & Cookie Use</a>.
						</p>
						<input type="submit" value=" Submit " class="submit-login">
					</div>
				</fieldset>
			</form>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>
<div class="popup hide" id="why-do-we-need-your-birthday" style="width:500px; margin-top: -850px;">
	<h1 style="font-size: 14px;">Why do we need your Birthday 
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h1>
		<? $row = get_record_on_id('cms',7);?>
		<div>
			<?=$row['content']?>
		    <div class="clear"></div>
		</div>
</div>
<style>
.white-box.content{position: relative;}
.sign_in_with_services a{position:absolute; top: 0px; right: 0px;}
.input-login, select{height: auto;}
#gender{width: 276px;}
</style>
<script src="<?php echo WWW?>js/jquery-1.8.0.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=WWW?>/js/jquery.validationengine.js"></script>
<script type="text/javascript" src="<?=WWW?>js/validation-engine.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('form').validationEngine();
	$('.close-popup').click(function(){
		$('.popup').addClass('hide');
	});

	$('a.popup-link').click(function(){
		var link = $(this).attr('href');
		if(link == '#edit-activity'){
			var id = this.id;
			$.ajax({type	: 'POST', 
				url		: WWW + 'includes/get-record.php', 
				data	: ({table:'activity',field:'activity',id:id}),
				success	: function(msg){
					$(link+' textarea').val(msg);
					$(link+' #recordId').val(id);
				}
			});
		}
		$(link).removeClass('hide');
	});

	$('input[name=password]').keyup(function(){
		var password = this.value;
		var score = 0;

		if (password.length >= 8)
	        score++;
	    if (password.length >= 9)
	        score++;
	    if (password.match(/^.*(?=.{7,})(?=.*[a-z])(?=\S*?[0-9]).*$/))
	        score++;

	    if(score == '3'){
	    	$('#password-result').html('Strong Password');
	    	$('#password-result').css('background','green');
	    }else if(score == '1'){
	    	$('#password-result').html('Not Strong Password');
	    	$('#password-result').css('background','orange');
	    }
	    else if(score == '2'){
	    	$('#password-result').html('Not Strong Password');
	    	$('#password-result').css('background','orange');
	    }
	});

	$('input[name=confirm_password]').keyup(function(){
		if($('input[name=confirm_password]').val() === $('input[name=password]').val()){
			$('#confirm-password-result').css('background', 'green');
			$('#confirm-password-result').html('Password Matched');
		}
	});
/*
	$('select[name=player_type]').live('change',function(){
		var id = $('select[name=player_type]').val();
		if(id == '0'){
			$('#player-type-batsman').removeClass('hide');
			$('#player-type-batsman select[name=batsman_type]').addClass('validate[required]');
			$('#player-type-batsman select[name=batting_order]').addClass('validate[required]');

			$('#player-type-bowler').addClass('hide');
			$('#player-type-bowler select[name=bowler_type]').removeClass('validate[required]');
			$('#player-type-bowler select[name=bowling_arm]').removeClass('validate[required]');
			$('#player-type-bowler select[name=bowling_prefer]').removeClass('validate[required]');
		}else if(id == '1'){
			$('#player-type-batsman').addClass('hide');
			$('#player-type-batsman select[name=batsman_type]').removeClass('validate[required]');
			$('#player-type-batsman select[name=batting_order]').removeClass('validate[required]');

			$('#player-type-bowler').removeClass('hide');
			$('#player-type-bowler select[name=bowler_type]').addClass('validate[required]');
			$('#player-type-bowler select[name=bowling_arm]').addClass('validate[required]');
			$('#player-type-bowler select[name=bowling_prefer]').addClass('validate[required]');
		}else{
			$('#player-type-batsman').addClass('hide');
			$('#player-type-bowler').addClass('hide');
			$('#player-type-batsman select[name=batsman_type]').removeClass('validate[required]');
			$('#player-type-batsman select[name=batting_order]').removeClass('validate[required]');
			$('#player-type-bowler select[name=bowler_type]').removeClass('validate[required]');
			$('#player-type-bowler select[name=bowling_arm]').removeClass('validate[required]');
			$('#player-type-bowler select[name=bowling_prefer]').removeClass('validate[required]');
		}
	});
*/
	$('#recaptcha_response_field').addClass('validate[required]');
	$('select[name=user_type_id]').live('change',function(){
		var id = $('select[name=user_type_id]').val();
		if(id != '2'){
			$('#dependent-player').addClass("hide");
		}else{
			$('#dependent-player').removeClass("hide");
		}
	});


	$('select[name=user_type_id]').live('change',function(){
		var id = $('select[name=user_type_id]').val();
		if(id == '2'){
			$('#match-type').removeClass("hide");
		}else{
			$('#match-type').addClass("hide");
		}
	});

	$('select[name=first_class_player]').live('change',function(){
		var id = $('select[name=first_class_player]').val();
		if(id == '1'){
			$('#espn').removeClass('hide');
			$('#espn input[name=espn]').addClass('validate[required, custom[url]]');
		}else{
			$('#espn').addClass('hide');
			$('#espn input[name=espn]').removeClass('validate[required, custom[url]]');
		}
	});

	$('select[name=day], select[name=month], select[name=year]').change(function(){

		var day 	= $('select[name=day]').val();
		var month 	= $('select[name=month]').val();
		var year 	= $('select[name=year]').val();

		if(day 	!= ''
		&& month!= ''
		&& year != ''){
			$.ajax({type: 'POST', 
				url		: WWW + 'includes/check-date.php', 
				data	: ({day:day, month:month, year:year}),
				success	: function(msg){

					var msg1 = msg.split('____');
					if(msg1[0] < 13){
						alert("You are not eligible to register, please follow our guidelines for minimum age requirement.");
						$('select[name=day]').val('');
						$('select[name=month]').val('');
						$('select[name=year]').val('');
					}
					if(msg1[1] == ''){
						alert("Invalid date selected ... !");
						$('select[name=day]').val('');
						$('select[name=month]').val('');
						$('select[name=year]').val('');
					}
				}
			});
		}
	});
});

</script>
<?php include('common/footer.php'); ?>