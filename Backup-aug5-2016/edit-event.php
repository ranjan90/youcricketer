<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	<div class="middle">
		<h1>Update Event </h1>
		<? 	if(isset($_GET['action']) && $_GET['action'] == 'deletephoto'){
				$pid 	= $_GET['pid'];
				$row_img= get_record_on_id('photos',$pid);
				mysqli_query($conn,"delete from photos where id = '$pid'");
				unlink(WWW.'events/'.$_GET['id'].'/'.$row_img['file_name']);
				?>
				<script>
				window.location = '<?=WWW?>edit-event-<?=$_GET['id']?>.html';
				</script>
				<?
			}
			if(isset($_POST) && !empty($_POST)){ 
				$title 	= $_POST['title'];
				$content= $_POST['content'];
				$state_id=$_POST['state_id'];
				$city_name= $_POST['city_name'];
				$isGlobal = ($_POST['is_global'] == 'on')?'1':'0';
				$from_date = $_POST['start_date'];
				$to_date   = $_POST['end_date'];
				
				$date_from1 = strtotime($from_date);
				$date_to    = strtotime($to_date);
				
				if($date_from1 > $date_to) {
				
					echo "<div id='error'><b>Error : </b>Please Select Correct Date ... Start Date should be less than to End date  ... !</div><br><br>"; 
				}
				
				else {
				
				$start_date = date_converter($_POST['start_date'],'Y-m-d');
				$end_date 	= date_converter($_POST['end_date'],'Y-m-d');
				$user_id 	= $_SESSION['ycdc_dbuid'];
				$id 		= $_GET['id'];
				if(!isset($user_id) || empty($user_id)){
					echo '<div id="error">Please login first ... !</div>';
				}else{
					if(!empty($title) && !empty($content)){
						$query = "update events set is_global = '$isGlobal', title = '$title', content = '$content', start_date = '$start_date', end_date = '$end_date', state_id = '$state_id', city_name = '$city_name' where id = '$id'";
						if(mysqli_query($conn,$query)){
						
							if(!is_dir('events/'.$id)){
								mkdir('events/'.$id,0777);
							}
							chmod('events/'.$id,0777);
							if(!empty($_FILES['photo']['name'])){
								$filename 	= friendlyURL($title).'.jpg';
								$image 		= new SimpleImage();
								$image->load($_FILES["photo"]["tmp_name"]);
								$image->save('events/'.$id.'/'.$filename);
								chmod('events/'.$id.'/'.$filename,0777);
							
								if(!empty($photo_id)){
									mysqli_query($conn,"update photos set file_name = '$filename' where id = '$photo_id'");	
								}else{
									mysqli_query($conn,"insert into photos (file_name, entity_type, entity_id) values ('$filename','events','$id');");
								}
							}
						}
		                echo '<div id="success">Event Information updated successfully ... !</div>';
		                ?>
		                <script>
		                window.location = '<?=WWW?>my-events.html';
		                </script>
		                <?
		            }else{
						echo '<div id="error">Please fill all fields ... !</div>';
					}
				}
				
			}

		}
			$id 	= $_GET['id'];
			$row 	= get_record_on_id('events', $id);
			$row_img= mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'events' and entity_id = '$id'"));
		?>
		<div class="white-box content">
			<form method="post" enctype="multipart/form-data" id="event_form">
				<fieldset>
					<h2>Event Information</h2>
					<div class="form-box">
						<label>Title</label>
						<input type="text" name="title" value="<?=$row['title']?>" class="input-login validate[required]">
					</div>
					<div class="clear"></div>
					<div class="form-box" style="width:100%;">
						<label>Is Global</label>
						<input style="float:left; margin-left:70px;" type="checkbox" name="is_global" <?php echo ($row['is_global'] == '1')?'checked="checked"':'';?>>
						<label>Event will be Regional Event If checked</label>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Content</label>
						<textarea maxlength="500" name="content" style="width:600px; margin-left:110px; height:100px;" class="validate[required]"><?=$row['content']?></textarea>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Start Date</label>
						<input type="text" name="start_date" value="<?=date_converter($row['start_date'],'m/d/Y')?>" class="calender input-login validate[required]">
					</div>
					<div class="form-box">
						<label>End Date</label>
						<input type="text" name="end_date" class="calender input-login validate[required]" value="<?=date_converter($row['end_date'],'m/d/Y')?>">
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>States</label>
						<select style="margin-left:20px; float:right;" name="state_id" class="validate[required]">
							<option selected="selected" value=""></option>
							<? 	$rs_states = mysqli_query($conn,"select * from states where country_id = '".$row_user['country_id']."'"); 
								while($row_state_l = mysqli_fetch_assoc($rs_states)){ ?>
								<option <?=($row['state_id'] == $row_state_l['id'])?'selected="selected"':'';?> value="<?=$row_state_l['id']?>"><?=$row_state_l['name']?></option>
							<?  } ?>
						</select>
					</div>
					<div class="form-box">
						<label>City</label>
						<input type="text" name="city_name" value="<?=$row['city_name']?>" style="margin-left:20px; float:right;" type="text" class="input-login validate[required]" > 
					</div>
					<div class="clear"></div>
					
				<? /*
				<div class="form-box">
						<label>Zipcode</label>
						<input type="text" name="zipcode" value="<?=$row['zipcode']?>" class="input-login validate[required]">
					</div>  
					<div class="clear"></div> */ ?>
					<h2>Event Photo</h2>
					<div class="form-box">
						<label>Event Photo</label>
						<input type="file" name="photo">
						<br>
						<? 	if($row_img){ ?>
						<a href="<?=WWW?>edit-event-<?=$_GET['id']?>-deletephoto-<?=$row_img['id']?>.html"><img style="position:relative; top:20px; right:5px;" src="<?=WWW?>images/icons/delete.png"></a>
						<br>
						<img src="<?=WWW?>events/<?=$id?>/<?=$row_img['file_name']?>" width="100">
						<input type="hidden" name="photo_id" value="<?=$row_img['id']?>">
						<?	} ?>
					</div>
					<div class="form-box">
						<label>Max Photo size : 2MB</label>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<input type="submit" value=" Save " class="submit-login">
					</div>
					<div class="form-box">
						<a id="cancel" class="submit-login" href="<?=WWW?>blogs.html">Cancel</a>
					</div>
				</fieldset>
			</form>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>

<script type="text/javascript" src="<?=WWW?>js/jquery-ui.min.js"></script>
<link href="<?=WWW?>css/jquery-ui.css" type="text/css" rel="stylesheet">
<script>
$(document).ready(function(){
	var currentYear = (new Date).getFullYear();
	var last_of_year= currentYear + 20;
	$('.calender').datepicker({changeYear:"true", yearRange: currentYear +":"+ last_of_year});
	$('#event_form').validationEngine();
});
</script>
<style>
#cancel{background: url('<?php echo WWW;?>images/sign-in-button.png') no-repeat !important; color:#fff !important; padding-left:3px; top:0px !important;position:relative; left:-150px;}
</style>
<?php include('common/footer.php'); ?>