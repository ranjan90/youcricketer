<div class="team">
	<div class="member">
		<img src="<?php echo WWW;?>team/shuja+khan.jpg" width="100">
		<div class="rightbox">
			<h3>Shuja Khan</h3>
			<p>Co-Founder & CEO</p>
			<!-- <a href="http://pk.linkedin.com/in/naveedramzan" target="_blank" title="Profile on LinkedIn"><img src="<?php echo WWW;?>images/li-logo.png"></a> -->
		</div>
		<div class="clear"></div>
	</div>
	<div class="member">
		<img src="<?php echo WWW;?>team/sana+khan.jpg" width="100">
		<div class="rightbox">
			<h3>Sana Khan</h3>
			<p>Co-founder & President <br>US Operations</p>
			<!-- <a href="http://pk.linkedin.com/in/naveedramzan" target="_blank" title="Profile on LinkedIn"><img src="<?php echo WWW;?>images/li-logo.png"></a> -->
		</div>
		<div class="clear"></div>
	</div>
	<div class="member">
		<img src="<?php echo WWW;?>team/naveed+ramzan.jpg" width="100">
		<div class="rightbox">
			<h3>Naveed Ramzan</h3>
			<p>Technical Lead</p>
			<!-- <a href="http://pk.linkedin.com/in/naveedramzan" target="_blank" title="Profile on LinkedIn"><img src="<?php echo WWW;?>images/li-logo.png"></a> -->
		</div>
		<div class="clear"></div>
	</div>
	<div class="member">
		<img src="<?php echo WWW;?>team/rizwan+abbas.jpg" width="100">
		<div class="rightbox">
			<h3>Rizwan Abbas</h3>
			<p>IOS Developer</p>
			<!-- <a href="http://pk.linkedin.com/in/naveedramzan" target="_blank" title="Profile on LinkedIn"><img src="<?php echo WWW;?>images/li-logo.png"></a> -->
		</div>
		<div class="clear"></div>
	</div>
	<div class="member">
		<img src="<?php echo WWW;?>team/waqas+mazhar.jpg" width="100">
		<div class="rightbox">
			<h3>Waqas Mazhar</h3>
			<p>Android Developer</p>
			<!-- <a href="http://pk.linkedin.com/in/naveedramzan" target="_blank" title="Profile on LinkedIn"><img src="<?php echo WWW;?>images/li-logo.png"></a> -->
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div>
<p align="center"><a href="<?php echo WWW;?>careers.html" title="Click to see Careers">Join our team</a></p>

<style>
.team{}
.member{border:1px solid #ccc; border-radius:4px; padding:5px; margin:5px; float:left; width:250px; height:120px;}
.member img{float:left; margin:5px;}
.member .rightbox{float: left; margin-top:10px;}
</style>