<div id="login-box">
	<? if(!isset($_SESSION['ycdc_user_name']) || empty($_SESSION['ycdc_user_name'])){ ?>
	<a href="#login" class="popup-link">Login</a> | 
	<a href="<?=WWW?>sign-up-individual.html" >Register as Individual</a> | 
	<a href="<?=WWW?>sign-up-company.html" >Register as Club/League/Company</a>
	<? }else{ ?>
	<h3>Welcome <?=$_SESSION['ycdc_user_name']?></h3>
	<? } ?>
</div>
<div id="login" class="popup hide">
	<h1>
		Login Here
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h1>
	<?php include('common/social-login.php');?> 
	<h2>Login with your Credential</h2>
	<form method="post" action="<?=WWW?>login-box-submit.php">
		<input type="hidden" name="action" value="login">
		<fieldset>
			<div class="form-box">
				<label>Username/Email : </label>
				<input type="text" name="email" class="input-login validate[required, custom[email]]">
			</div>
			<div class="clear"></div>
			<div class="form-box">
				<label>Password </label>
				<input type="password" name="password" class="input-login validate[required]">
			</div>
			<div class="clear"></div>
			<div class="form-box">
				<input type="submit" value=" Login " class="submit-login">
			</div>
			<p>
				<a href="<?=WWW?>sign-up-individual.html" >Register as Individual</a><br>
				<a href="<?=WWW?>sign-up-company.html" >Register as Club/League/Company</a>
			</p>
		</fieldset>
	</form>
</div>
