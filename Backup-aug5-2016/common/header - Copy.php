<?php include_once('includes/configuration.php'); ?>
<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<title><?php if(isset($page_title)) echo $page_title.' - Youcricketer Overseas Cricket Agency';else echo $site_title;?></title>
	<meta name="description" content="<?php echo strip_tags($meta_description)?>">
	 <meta name="viewport" content="width=device-width"/>
	<meta name="keywords" content="<?php echo $meta_keywords?>">
	<link rel="shortcut icon" href="<?php echo WWW?>images/favicon.png" type="image/ico">
	<link href="<?php echo WWW?>css/reset.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo WWW?>css/style.css?v=1.23" rel="stylesheet" type="text/css" />
    <link href="<?php echo WWW?>css/css3.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo WWW?>css/developer.css?v=1.1" rel="stylesheet" type="text/css" />
	<link href="<?php echo WWW?>css/developer2.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo WWW?>css/media.css" rel="stylesheet" type="text/css" />
	<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
	<?php if(!empty($canonical_tag)){ ?>
	<link rel="canonical" href="<?php echo $canonical_tag;?>">
	<?php } ?>
	<link href="<?php echo WWW?>css/validation-engine.css" rel="stylesheet" type="text/css"/>
	<script src="<?=WWW?>ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="<?=WWW?>js/jquery-1.8.0.min.js"></script>
	<script type="text/javascript" src="<?=WWW?>js/timezone.js"></script>
	<script type="text/javascript" src="<?=WWW?>js/search-results.js"></script>
	<script type="text/javascript" src="<?=WWW?>js/check_message.js"></script>
	<script src="<?=WWW?>js/jquery.tabify.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=WWW?>js/jquery.cycle.pack.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=WWW?>js/banners.js?v=1.12" type="text/javascript" ></script>
	<script type="text/javascript" src="<?=WWW?>js/jquery.validationengine.js"></script>
	<script type="text/javascript" src="<?=WWW?>js/validation-engine.js"></script>
	<script src="<?=WWW?>js/jstz.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?=WWW?>js/site.js?v=1.12"></script>
	<script type="text/javascript" src="<?=WWW?>js/espn.js"></script>
	
	<script type="text/javascript">
		var WWW = '<?php echo WWW?>';
		var bro = navigator.userAgent;
		var n   = bro.search("Firefox");
		var hasFlash = false;
		if(n != -1){
			if (navigator.mimeTypes
		        && navigator.mimeTypes['application/x-shockwave-flash'] != undefined
		        && navigator.mimeTypes['application/x-shockwave-flash'].enabledPlugin) {
		  			hasFlash = true;
		  	}
		  	if(hasFlash === false){
				alert("Please upgrade your browser or enable Video Player / You may use Google Chrome ... !");
			}
		}

	</script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-52993586-1', 'auto');
	  ga('send', 'pageview');

	</script>
	<meta name="p:domain_verify" content="2c65a37f063c5f69ffeefea162836c1c"/>
	<style>
	<?php if(isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){ ?>
		.nav ul li a{padding:0 6px 0 7px !important;}
	<?php }else{ ?>
		.nav ul li a{padding:0 8px !important;}
	<?php } ?>
	.sub-nav1 li{width:129px;} .sub-nav1 a{text-align:left !important;}
	</style>
	<?php if($page == 'sign-up-successful.html?status=success_individual'
		  || $page == 'sign-up-successful.html?status=success_company'){ ?>
	<!-- Facebook Conversion Code for Registrations - You Cricketer 2015Feb -->
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6022566042911', {'value':'0.00','currency':'USD'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6022566042911&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>
<?php } ?>
</head>
<body>
	<!--[if lt IE 7]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
    <![endif]-->
    
    <? if(isset($_SESSION['ycdc_user_email']) 
       && ( $_SESSION['ycdc_user_email'] == 'naveed.ramzan@gmail.com'
       	  ||$_SESSION['ycdc_user_email'] == 'youcrickter00@gmail.com')){ ?>
	    <a href="#" id="meta-link" title="Update Meta Info" style="float:right;">
	        <img src="<?=WWW?>images/edit-meta.png">
	    </a>
	  
	    <script type="text/javascript">
	    $(document).ready(function(){
	        $('a#meta-link').click(function(){
	        	if($('#meta-box').css('display') == 'none' || $('#meta-box').css('display') == ''){
	                $('#meta-box').css('display','block');
	            }else{
	                $('#meta-box').css('display','none');
	            }
	        });
	        $('#close-meta-box').click(function(){
	            $('#meta-box').css('display', 'none');
	        });
	    });
	    </script>
	    <style>
	    #meta-box{border:1px solid #515151; display:none; width: 800px; padding:10px; box-shadow: 1px 1px 1px #515151; position: absolute; right: 0px; z-index: 999; background: #fff; box-shadow: 1px 1px 2px 2px #000; margin:50px 10px;}
	    </style>
	    <div id="meta-box" style="">
	        <form method="post">
	            <input type="hidden" name="action" value="metasubmit">
	            <h2>Update meta information
	                <div style="float:right; cursor:pointer;" id="close-meta-box">
	                    <img src="<?=WWW?>images/icons/delete.png" title="Close this popup">
	                </div>
	            </h2>
	            <div class="space"></div>
	            <table width="100%">
	                <tr>
	                    <td>Page Title : </td>
	                    <td><input type="text" name="page_title" value="<?=$site_title?>" class="validate[required] input-login" style="width:610px;"></td>
	                </tr>
	                <tr>
	                    <td>Page Meta Keywords : </td>
	                    <td><textarea name="page_meta_keywords" colspan="40" rows="5" class="validate[required]" style="width:620px; border: 1px solid #ccc;line-height:22px"><?=$meta_keywords?></textarea></td>
	                </tr>
	                <tr>
	                    <td>Page Meta Description : </td>
	                    <td><textarea name="page_meta_description" colspan="40" rows="5" class="validate[required]" style="width:620px; border: 1px solid #ccc;line-height:22px"><?=$meta_description?></textarea></td>
	                </tr>
	                <tr>
	                    <td>Canonical Tag : </td>
	                    <td><textarea name="canonical_tag" colspan="40" rows="5" class="validate[required]" style="width:620px; border: 1px solid #ccc;line-height:22px"><?=$canonical_tag?></textarea></td>
	                </tr>
	                <tr>
	                    <td colspan="2" align="right">
	                        <input type="submit" value="Submit" class="green-btn" >
	                    </td>
	                </tr>
	            </table>
	        </form>
	    </div>
	<? } ?>

    <div class="top">
	<div id="header">
		<div class="logo">
			<a href="<?=WWW?>" title="Home Page - <?=$site_title?>"><img alt="Logo" src="<?php echo WWW;?>images/logo.png" /></a>
			<p>Connect  & Discover Cricket Opportunities Globally </p>
		</div>
		<div class="header-right">
			<div class="header-top">
				<div class="searchbox" style="width: 325px;">
					<form method="post"  id="frm-search">
					  <input id='ja' name="keywords" type="text" class="input-search" style="width: 250px;" placeholder="Search our individuals/companies"/>
					  <input type="button"  id="search-btn" style="cursor: pointer;"  class="submit-search" />
					</form>
					<script type="text/javascript">
					 
					$('#search-btn').click(function(){
			        	var string = $('form#frm-search input[name=keywords]').val();
			        	if(string != ''){
							string = string.replace(/[^a-zA-Z0-9]+/g,'').toLowerCase();
							if(string.length > 0){
								window.location = '<?=WWW;?>searches-' + string + '.html';
							}
						}
					});

					$('form#frm-search').submit(function(){
			        	var string = $('form#frm-search input[name=keywords]').val();
						if(string != ''){
							string = string.replace(/[^a-zA-Z0-9]+/g,'').toLowerCase();
							if(string.length > 0){
								window.location = '<?=WWW;?>searches-' + string + '.html';
							}
						}
					});
			        </script>
			  </div>
			  <a href="<?=WWW?>advance-search.html" style="float:left; padding:10px;"> Advance Search </a>
				<div class="social">
					<ul>
						<li><a href="<?=$facebook_page_link['value']?>" title="Facebook Page - <?=$site_title?>" target="_blank"><img alt="Facebook Page - <?=$site_title?>" height="25" width="30" src="<?php echo WWW;?>images/facebook.png" /></a></li>
						<li><a href="<?=$twitter_page_link['value']?>" title="Twitter Page - <?=$site_title?>" target="_blank"><img alt="Twitter Page - <?=$site_title?>" height="25" width="30" src="<?php echo WWW;?>images/twitter.png" /></a></li>
						<li><a href="<?=$google_plus_page_link['value']?>" title="Google Plus Page - <?=$site_title?>" target="_blank"><img alt="Google Plus Page - <?=$site_title?>" height="25" width="30" src="<?php echo WWW;?>images/google.png" /></a></li>
						<li><a href="<?=$pinterest_page_link['value']?>" title="Pinterest Plus Page - <?=$site_title?>" target="_blank"><img alt="Pinterest Plus Page - <?=$site_title?>" height="25" width="30" src="<?php echo WWW;?>images/pinterest.png" /></a></li>
						<li><a href="<?=$tumblr_page_link['value']?>" title="Tumblr Plus Page - <?=$site_title?>" target="_blank"><img alt="Tumblr Plus Page - <?=$site_title?>" height="25" width="30" src="<?php echo WWW;?>images/tumblr.png" /></a></li>
						<!-- <li><a href="#"><img height="25" width="20" src="<?php echo WWW;?>images/youtube.png" /></a></li> -->
						<li><a href="<?php echo WWW;?>rss.html"><img alt="RSS" height="25" width="30" src="<?php echo WWW;?>images/rss.png" /></a></li>
						</ul>
				</div>
			</div>
			<div class="signup-buttons">
				<?
				if(isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
					$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select * from users where email = '".$_SESSION['ycdc_user_email']."'"));
					$_SESSION['ycdc_dbuid'] = $rowUser['id'];
				}
				?>
				<? if(!isset($_SESSION['ycdc_user_name']) || empty($_SESSION['ycdc_user_name'])){ ?>
				<? 	$browser = getBrowser();
					if($browser['name'] == 'MSIE' && $browser['version'] < 11){?>
					<div title="browser" style="display:inline; float:left; margin-left:25px; ">
						<div style="width:115px; float:left; font-size:25px; font-weight:bold;color:#fc8600; margin-top:10px;">
							Your browser is out dated, Please install latest version <a href="http://www.microsoft.com/en-pk/download/details.aspx?id=40902">HERE</a>
						</div>
					</div>
				<?  } ?>
					<a style="line-height:16px; padding:0px; height:40px;" title="Company Sign up - <?=$site_title?>" href="<?=WWW?>sign-up-company.html" class="submit-signup" >Sign up as Club/League/Company</a>
					<a title="Individual Sign up - <?=$site_title?>" href="<?=WWW?>sign-up-individual.html" class="submit-signup">Sign up as Individual</a>
				<? }else{ 
					$row_user = get_record_on_id('users', $_SESSION['ycdc_dbuid']);
					$row_comp = mysqli_fetch_assoc(mysqli_query($conn,"select * from companies where user_id = '".$row_user['id']."'"));
					?>
					<div id="header-welcome" title="<?=$_SESSION['ycdc_user_name']?>" style="display:inline; float:left; margin-left:25px; ">
						<div style="width:115px; float:left; font-size:22px; font-weight:bold;color:#fc8600; margin-top:10px;">
							Welcome
						</div>
						<div style="width:90px; margin-top: 12px; float:left; font-weight:bold; padding-left:4px;">
							<?=$row_user['f_name']?>
						</div>
					</div>
					<a title="Logout - <?=$site_title?>" href="<?=WWW?>logout.html" class="submit-signup" >Logout</a>
					<a title="My Dashboard - <?=$site_title?>" href="<?=WWW?>dashboard.html" class="submit-signup" >My Dashboard <img id="check_for_message" src="<?=WWW?>images/icons/mail.png"  style="display:none;float:right; margin-right:15px; margin-top: 2px;"></a>
				<? } ?>
				
			</div>
			<div class="clear"></div>
			<div class="nav" style="margin-top:8px;">
				<ul>
					<li class="<?=$top_menu['home']?>"><a title="Home - <?=$site_title?>" href="<?=WWW?>">Home</a></li>
					<li class="<?=$top_menu['about_us']?>"><a title="About Us - <?=$site_title?>" href="<?=WWW?>about-us.html">About Us</a>
						<ul class="sub" style="width:119px;">
							<li><a href="<?=WWW?>who-we-are.html" title="Who We Are - About Us">Who We Are</a></li>
							<li><a href="<?=WWW?>our-mission.html" title="Our Mission - About Us">Our Mission</a></li>
							<li><a href="<?=WWW?>our-philosphy.html" title="Our Philosphy - About Us">Our Philosphy</a></li>
							<li><a href="<?=WWW?>our-team.html" title="Our Team - About Us">Our Team</a></li>
						</ul>
					</li>
					<li class="<?=$top_menu['press_releases']?>"><a title="Press Releases - <?=$site_title?>" href="<?=WWW?>press-releases.html">Press Releases</a></li>
					<li class="<?=$top_menu['community']?>"><a title="Community - <?=$site_title?>" href="<?=WWW?>community.html">Community</a>
						<ul class="sub sub-nav1" id="sub1" style="width:129px;">
							<li><a href="<?=WWW?>blogs.html" title="Blog">Blogs</a></li>
							
							<li><a href="<?=WWW?>how-to.html" title="Forum">How To</a></li>
							
							<li><a href="<?=WWW?>forums.html" title="Forum">Forums</a></li>
							
							<li><a href="<?=WWW?>groups.html" title="Groups">Groups</a></li>
							
							<li><a href="<?=WWW?>testimonials.html" title="Testimonials">Testimonials</a></li>
							<li><a href="<?=WWW?>news.html" title="News">Members News</a></li>
							
							<li><a href="<?=WWW?>events.html" title="Events">Events</a></li>
							
							<li><a href="<?=WWW?>clubs.html" title="Clubs">Clubs</a></li>
							
							<li><a href="<?=WWW?>leagues.html" title="Leagues">Leagues</a></li>
							
							<li><a href="<?=WWW?>other-companies.html" title="Other Companies">Other Companies</a></li>
							<li><a href="<?=WWW?>tournaments" title="Tournaments">Tournaments</a></li>
						</ul>
					</li>
					<?php $referrer = strpos($_SERVER['HTTP_REFERER'], 'your-region');?>
					<li class="<?=$top_menu['in_your_region']?> <?php echo ($referrer !== false)?'nav_selected':'';?>"><a title="Activities in Your Region - <?=$site_title?>" href="<?=WWW?>in-your-region.html">Your Region</a>
						<ul class="sub sub-nav1" id="sub2" style="width:129px;">
							<li><a href="<?=WWW?>your-region-blogs.html" title="Your Region Blog">Blogs</a></li>
							
							<li><a href="<?=WWW?>your-region-forums.html" title="Your Region Forum">Forums</a></li>
							
							<li><a href="<?=WWW?>your-region-groups.html" title="Your Region Groups">Groups</a></li>
							<li><a href="<?=WWW?>your-region-individuals.html" title="Your Region Individual Players">Individuals</a></li>
							<li><a href="<?=WWW?>your-region-news.html" title="Your Region News">Members News</a></li>
							<li><a href="<?=WWW?>your-region-events.html" title="Your Region Events">Events</a></li>
							
							<li><a href="<?=WWW?>your-region-clubs.html" title="Your Region Clubs">Clubs</a></li>
							
							<li><a href="<?=WWW?>your-region-leagues.html" title="Your Region Leagues">Leagues</a></li>
							
							<li><a href="<?=WWW?>your-region-other-companies.html" title="Your Region Other Companies">Other Companies</a></li>
							
						</ul>
					</li>
					<li class="<?=$top_menu['top_players']?>"><a title="Most Favorite - <?=$site_title?>" href="<?=WWW?>most-favorite.html">Most Favorite</a></li>
					<li class="<?=$top_menu['faqs']?>"><a title="FAQs - <?=$site_title?>" href="<?=WWW?>faqs.html">FAQs</a></li>
					<li class="<?=$top_menu['contact-us']?>"><a title="Contact Us - <?=$site_title?>" href="<?=WWW?>contact-us.html">Contact Us</a></li>
					<? if(!isset($_SESSION['ycdc_user_name']) || empty($_SESSION['ycdc_user_name'])){ ?>	
					<li class="<?=$top_menu['login']?>"><a title="Login - <?=$site_title?>" href="<?=WWW?>login.html">Login</a></li>
					<? }else{ ?>
					<li><a href="#" title="My Account"><!--<img style="margin:3px 4px;" src="<?=WWW?>images/header-no-photo.png" width="30">-->My Account
						<ul class="sub" style="width:109px; position:absolute; top:30px; right:-3px;">
							<li><a href="<?=WWW?>dashboard.html" title="My Dashboard">My Dashboard</a></li>
							<li><a href="<?=WWW?>my-network.html" title="My Network">My Network</a></li>
							<li><a href="<?=WWW?>inbox.html" title="MY Messages">My Messages</a></li>
							<li><a href="<?=WWW?>my-community.html" title="My Community">My Community</a></li>
							<li><a href="<?=WWW?>individual-detail-<?=$_SESSION['ycdc_dbuid']?>-<?=friendlyURL($_SESSION['ycdc_user_name'])?>.html#activity-tab" title="My Profile">My Profile</a></li>
						</ul>
					</li>
					<? } ?>
					
				</ul>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
<div class="wrapper">
	<?php include('common/breadcrumbs.php');?>
	<?php if(isset($meta_msg) && !empty($meta_msg)){ ?>
		<?php echo $meta_msg;?>
	<?php } ?>
<!-- <div id="success">We are upgrading few sections, Sorry for inconvience ...</div> -->