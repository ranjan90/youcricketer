<?php include('common/header.php'); ?>
<? 	if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
<style>
#friends{}
#friends #friend{width:115px; height: 155px; padding:4px; margin: 3px; border:1px solid orange; border-radius: 4px; float:left;text-align:center;}
#friends #friend img{margin-left:5px;}
#friends p{text-align: center;}
</style>
	<div class="middle">
		<h1>Pending Friendship Requests</h1>
		<div class="white-box content" id="dashboard">
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
				<div class="white-box">
			        <div class="orange-box" id="friends">
			        <?php 	$userId 	= $_SESSION['ycdc_dbuid'];
			        		$rsFriends 	= mysqli_query($conn,"select * from friendship where to_user_id = '$userId' and status = '0' "); 
			        		if(mysqli_num_rows($rsFriends) > 0){
			        			echo '<h2>Pending Requests</h2>';
			        		}else{
			        			echo '<div id="error">No Pending Friendship Requests ... !</div>';
			        		}
			        		while($rowF = mysqli_fetch_assoc($rsFriends)){ 
			        			if($rowF['from_user_id'] == $userId){
			        				$friendId 	= $rowF['to_user_id'];
			        			}else{
			        				$friendId 	= $rowF['from_user_id'];
			        			}
			        			$rowUser = get_record_on_id('users', $friendId);
			        			$row_img = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$friendId."' and is_default = '1' "));
			        			?>
			        		<div id="friend" style="background:#fff5f5;">
			        			<input type="checkbox" name="friend" id="<?=$rowF['id']?>" class="accept-fr"> Accept <input type="checkbox" name="friend" id="<?=$rowF['id']?>" class="deny-fr"> Deny 
			        			<a class="no-bg" href="<?=WWW?>individual-detail-<?=$rowUser['id']?>-<?=friendlyURL($rowUser['f_name'].' '.$rowUser['last_name'])?>.html#activity-tab" title="<?=$rowUser['f_name'].' '.$rowrowUser['last_name']?>">
			        				<img id="<?=$rowF['id']?>" src="<?php echo WWW;?><?=($row_img && !empty($row_img['file_name']))?'users/'.$rowUser['id'].'/photos/'.$row_img['file_name']:'images/no-photo.jpg'?>" width="90" height="100" />
			        			</a>
			        			<p><?php echo $rowUser['f_name'].' '.$rowUser['last_name'];?></p>
			        		</div>

			        <?php 	} ?>
			        <div class="clear"></div>
			        </div>
			        
			    </div>
			</div>
		</div>
		<div class="clear"></div>
	</div>

<script type="text/javascript" src="<?php echo WWW;?>js/jquery-1.8.0.js"></script>
<script>
	$(document).ready(function(){
		//$('#friend input[type=checkbox]').click(function(){
		$('#friend .accept-fr').click(function(){
			var id = $(this).attr('id');
			if($('input[type=checkbox]#'+id+':checked').length == '1'){
				// checked
				$.ajax({type	: 'POST', 
						url		: WWW + 'includes/update-friendship.php', 
						data	: ({type:'makeFriend',id:id}),
						success	: function(msg){
							window.location = WWW + 'pending-request.html';
						}
					});
			}else{
				// unchecked
				$.ajax({type	: 'POST', 
						url		: WWW + 'includes/update-friendship.php', 
						data	: ({type:'makeUnfriend',id:id}),
						success	: function(msg){
							window.location = WWW + 'pending-request.html';
						}
					});
			}
		});
		
		$('#friend .deny-fr').click(function(){
			var id = $(this).attr('id');
			$.ajax({type	: 'POST', 
					url		: WWW + 'includes/update-friendship.php', 
					data	: ({type:'makeUnfriend',id:id}),
					success	: function(msg){
						window.location = WWW + 'pending-request.html';
					}
			});
		});
		
		$('#dashboard .black-box h2').click(function(){
			$('#dashboard .black-box ul').css('display','none');
			var id = this.id;
			$('#'+id+'-box ul').css('display','block');
		});
	});
</script>
<?php include('common/footer.php'); ?>