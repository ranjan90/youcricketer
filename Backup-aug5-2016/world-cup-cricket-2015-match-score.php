<?php include('common/header.php'); ?>
<style>
#page-left{width:65%;}
#page-right{width:34%;}
.match{border: 1px solid #ccc; border-radius:5px; margin: 4px; padding: 8px;}
.match p{width:48%; float: left;}
h3 a{font-size:12px;}
</style>
<?php $rowM = get_record_on_id('series_matches', $_GET['id']); ?>
<?php if(empty($rowM['title'])) {
	    $row1 = get_record_on_id('countries', $rowM['country_1']);
	    $row2 = get_record_on_id('countries', $rowM['country_2']);
	    $title = $row1['name'].' vs '.$row2['name'].' - Pool '.$rowM['pool'];
?>
<?php }else{ ?>
  	<?php $title = $rowM['title'];?>
<?php } ?>
<?php
	$blogRow = mysqli_fetch_assoc(mysqli_query($conn,"select * from blog_articles where series_match_id = '".$_GET['id']."'"));
	if(empty($blogRow)){
		$query = "insert into blog_articles (category_id, title, series_match_id, create_date, status, user_id) values ('5','".$title."','".$_GET['id']."','".date('Y-m-d H:i:s')."','1','".$_SESSION['ycdc_dbuid']."');";
		mysqli_query($conn,$query);
		$blogId = mysqli_insert_id();
	}else{
		$blogId = $blogRow['id'];
	}
?>
	<div class="middle">
		<h1>
			World Cup Cricket 2015 - <?php echo $title;?>			
		</h1>
		<h2>
			<?php 
			$venue = $rowM['match_ground'].', '.$rowM['match_city'].', ';
		    $row_country= get_record_on_id('countries', $rowM['match_country_id']);
		    echo 'Venue : '.$venue.$row_country['name'].'';
			?>
		</h2>
		<h3>
			Match Date : <?php echo date('d-M-Y', strtotime($rowM['match_date']));?> (GMT Time : <?php echo $rowM['gmt_time'];?>, IST Time : <?php echo $rowM['ist_time'];?>)
		</h3>
		<div style="height:30px;"></div>
		<h3>
			Comments
			<a class="right" target="_blank" href="<?php echo WWW;?>blog-detail-<?php echo $blogId;?>-<?php echo friendlyURL($title);?>.html"><img src="<?php echo WWW;?>images/blog-series.jpg"></a>
			<div class="clear"></div>
		</h3>
		<div class="white-box content detail" style="width:100%;">
			<?php if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){ 
				   if(isset($_POST['action']) && $_POST['action'] == 'match-update'){
				       	$date 	= date('Y-m-d H:i:s');
				       	$userId = $_SESSION['ycdc_dbuid'];
				       	$content= $_POST['activity'];
				       	$matchId= $_GET['id'];
				       	$rowCheck=mysqli_fetch_assoc(mysqli_query($conn,"select * from series_match_updates where series_match_id = '$matchId' and content = '$content'"));
				       	if(!$rowCheck){
					       	$query 	= "insert into series_match_updates (series_match_id, content, created_by, create_date, status) values ('$matchId','$content','$userId','$date','1');";
					       	//echo $query;exit;
					       	if(mysqli_query($conn,$query)){
					       		?><div id="success">Match Updated has been added ... !</div><?php
					       	}else{
					       		?><div id="error">Match Updated cannot be added ... !</div><?php
					       	}
					    }
			       }
		       ?>
			<form method="post" action="" enctype="multipart/form-data">
				<input type="hidden" name="action" value="match-update">
				<fieldset style="border:0px;">
					<textarea style="background:#fff;height:50px;width:800px; float:left; padding:3px;" name="activity" class="validate[required]" placeholder="Type match live score updates here ..."></textarea>
					<input type="submit" class="submit-login margin-top-5" style="float:right; position:relative; top:1x; left:-20px;" value="Post">
				</fieldset>
			</form>
			<?php }else{ ?>
				<div id="contact-message">
				<div id="success"><b>Please Login or Register!</b></div>
				<p><?php $rowWhoopsMsg = get_record_on_id('cms', 23); echo $rowWhoopsMsg['content'];?></p>
				<?
					include('common/login-box.php');
				?></div>
			<?php } ?>
			<div id="updates">
				<?php 	$rsUpdates = mysqli_query($conn,"select * from series_match_updates where series_match_id = '".$_GET['id']."' order by create_date desc");
						$updates = array();
						while($rowU= mysqli_fetch_assoc($rsUpdates)){ 
							$date = date('d-M-Y', strtotime($rowU['create_date']));
							$updates[$date][] = array('time' 	=> date('H:i:s', strtotime($rowU['create_date'])),
													  'content'	=> $rowU['content'],
														);
						} 
				foreach($updates as $key=>$u){ ?>
					<h3><?php echo $key?></h3>
					<ul>
						<?php foreach($u as $ud){ ?>
						<li><b><?php echo $ud['time']?> : </b><?php echo $ud['content']?></li>
						<?php } ?>
					</ul>
					<?
				}
				?>
			</div>
			
		</div>		
		<div class="clear"></div>
	</div>
<style>
	li{font-size: 15px; line-height: 30px;}
</style>
<?php include('common/footer.php'); ?>