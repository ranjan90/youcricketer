<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
<?php 
	$row = mysqli_fetch_assoc(mysqli_query($conn,"select * from blog_articles where id = '".$_GET['id']."'"));
?>
	<div class="middle">
		<h1>Add Blog <?php echo (isset($_GET['id']))?' - '.$row['title']:'';?></h1>
		<? 	if(isset($_POST) && !empty($_POST)){ 
				$title 	= $_POST['title'];
				if(isset($_GET['id'])){
					$title .= ' '.$row['title'];
				}
				$content= $_POST['content'];
				$category_id = (isset($_POST['category_id']))?$_POST['category_id']:$row['category_id'];
				$series_match_id= $_POST['series_match_id'];
				$date 		= date('Y-m-d H:i:s');
				$user_id 	= $_SESSION['ycdc_dbuid'];
				$isGlobal = ($_POST['is_global'] == 'on')?'1':'0';
				$espnLink = $_POST['espn_link'];
				if(empty($series_match_id)){
					$series_match_id = 0;
				}
				if(empty($_POST['record_count'])){
					$record_count = 0;
				}
				if(empty($_POST['is_featured'])){
					$is_featured = '';
				}
				if(!isset($user_id) || empty($user_id)){
					echo '<div id="error">Please login first ... !</div>';
				}else{
					if(!empty($title) && !empty($content)){
						
						$rs_blog = mysqli_query($conn,"select * from blog_articles where category_id = '$category_id' and title = '$title'");
						if(mysqli_num_rows($rs_blog) == 0){
							$query = "insert into blog_articles (title, content, status, create_date, user_id, category_id, is_global, espn_link, series_match_id,record_count,is_featured) 
							values ('$title','$content','1','$date','$user_id','$category_id','$isGlobal','$espnLink', '$series_match_id','$record_count','$is_featured')";

							if(mysqli_query($conn,$query)){
								$blog_id = mysqli_insert_id();

								if(!is_dir('blog/'.$blog_id)){
									mkdir('blog/'.$blog_id,0777);
								}
								chmod('blog/'.$blog_id,0777);

								if(!empty($_FILES['photo']['name'])){
									$filename 	= friendlyURL($title).'.jpg';
									$image 		= new SimpleImage();
									$image->load($_FILES["photo"]["tmp_name"]);
									$image->save('blog/'.$blog_id.'/'.$filename);
									chmod('blog/'.$blog_id.'/'.$filename,0777);
								
									mysqli_query($conn,"insert into photos (file_name, title, entity_id, entity_type) values ('$filename','$title','$blog_id','blog')");
								}
							}

			                echo '<div id="success">Blog Information added successfully ... !</div>';
			                ?>
			                <script>
			                window.location = '<?=WWW?>my-blog.html';
			                </script>
			                <?
						}else{
							echo '<div id="error">Information already exists ... !</div>';
						}
					}else{
						echo '<div id="error">Please fill all fields ... !</div>';
					}
				}
				
			}
			
		?>
				<div class="white-box content">
			<form method="post" enctype="multipart/form-data">
				<input type="hidden" name="series_match_id" value="<?php echo $row['series_match_id']?>">
				<fieldset>
					<h2>Blog Information</h2>
					<div class="form-box">
						<label>Blog Category</label>
						<?php if($row){ ?>
						<label><?=get_combo('blog_categories','name',$row['category_id'],'', 'text')?></label>
						<?php }else{ ?>
						<?=get_combo('blog_categories','name',$row['category_id'],'category_id')?>
						<?php } ?>
						
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Is Global</label>
						<input style="float:left; margin-left:70px;" type="checkbox" name="is_global" <?php echo ($row['is_global'] == '1')?'checked="checked"':'';?>>
						<label>Blog will be Regional Blog If checked</label>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>ESPN Link</label>
						<input type="text" name="espn_link" style="width: 253px;" class="input-login validate[required]" value="<?php echo $row['espn_link']?>">
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Title</label>
						<input type="text" name="title" style="width: 253px;"class="input-login validate[required]" >
					</div>
					<div class="clear"></div>
					
						<label>Content</label>
						
						
					<div style="width: 600px; display: block; margin-left:130px;">
					
					<textarea maxlength="500" name="content" id="content" style="width: 200px;" cols="0" rows="10" class="ckeditor validate[required]"></textarea>
					</div>
					
					
					<div class="clear"></div>
					<h2>Blog Photo</h2>
					<div class="form-box">
						<label>Blog Photo</label>
						<input type="file" name="photo">
					</div>
					<div class="form-box">
						<label>Max Photo size : 2MB</label>
					</div>
					
					<div class="clear"></div>
					<div class="form-box">
						<input type="submit" value=" Save " class="submit-login">
					</div>
					<div class="form-box">
						<a id="cancel" class="submit-login" href="<?=WWW?>blogs.html">Cancel</a>
					</div>
				</fieldset>
			</form>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>
<style>
#cke_content a{background: none !important;}
#cancel{background: url('<?php echo WWW;?>images/sign-in-button.png') no-repeat !important; color:#fff !important; padding-left:3px; top:0px !important;position:relative; left:-150px;}
</style>	
<script>
$(document).ready(function(){
	$('select[name=category_id]').append('<option value="6">How To</option>');
	
});
CKEDITOR.replace('content', {
		"filebrowserImageUploadUrl": "ckeditor/plugins/imgupload.php"
	});
</script>
<?php include('common/footer.php'); ?>