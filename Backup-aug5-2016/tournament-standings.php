<?php include_once('includes/configuration.php');
$page = 'tournament-standings.html';
$selected_country = getGeoLocationCountry(); 

$league_clubs = array();
$league_info = array();
$teams = array();
$error = '';

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
}

$tournament_id = trim($_GET['tour_id']);
$tournament_info = get_record_on_id('tournaments', $tournament_id);

$page_title = ucwords($tournament_info['title']).' Standings';

$sql = "SELECT c.id,c.company_name FROM companies as c inner join tournament_teams as t on c.id = t.team_id
 WHERE  t.tournament_id = $tournament_id ORDER BY c.company_name ";
$rs_teams = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_teams)){
	$teams[] = $row;
}

$sql = "SELECT id from tournament_matches WHERE tournament_id = $tournament_id  ";
$rs_tournament_matches = mysqli_query($conn,$sql);

?>
<?php include('common/header.php'); ?>
<style>
.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
dl {min-height:145px;}
.content {width:1010px;}
.large-column {width:790px;}
#table-list {width:100%;border:1px solid #ccc;}
#table-list tr{border:1px solid #ccc;}
#table-list th{padding:5px;background-color:#000;color:#FB7008;font-size:14px;}
#table-list td{padding:5px;text-align:center;}
#table-list a{color:#000;}
.add_tournament{font-size:14px;color:#000;}
</style>
	<div class="middle">
		<h1> Tournaments Standings - <?php echo $tournament_info['title']; ?> </h1>
		
		<div class="white-box content" id="dashboard">
			
			<?php if(!empty($error)): ?>
				<?php echo $error; ?>
			<?php endif; ?>
		
			<?php if(!empty($user_id)): ?>
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<?php endif; ?>
			
			<div class="large-column" <?php if(empty($user_id)): ?> style="width:96%;" <?php endif; ?>>
			
			 <div class="clear"></div>
				<table id="table-list" class="white-box">
				<thead> <tr><th>Group</th><th>Team</th><th>Played</th><th>Won</th><th>Lost</th><th>Tied</th><th>Wash</th><th>Points</th><th>Win %</th><th>NRR</th><th>Detail</th></tr></thead> 
				<?php if(mysqli_num_rows($rs_tournament_matches)>0): ?>
					<?php for($i=0;$i<count($teams);$i++): ?>
					<?php $sql = "SELECT count(id) as matches_played FROM tournament_matches WHERE tournament_id=$tournament_id and (winning_team_id >0 OR winning_team_id = -1 OR winning_team_id = -2) and ( team1=".$teams[$i]['id'].' OR team2 = '.$teams[$i]['id'].')';
					$row = mysqli_fetch_assoc(mysqli_query($conn,$sql));
					$matches_played = $row['matches_played'];
					
					$sql = "SELECT count(id) as matches_won FROM tournament_matches WHERE tournament_id=$tournament_id and  winning_team_id=".$teams[$i]['id'];
					$row = mysqli_fetch_assoc(mysqli_query($conn,$sql));
					$matches_won = $row['matches_won'];
					
					$sql = "SELECT count(id) as matches_lost FROM tournament_matches WHERE tournament_id=$tournament_id and ( team1=".$teams[$i]['id'].' OR team2 = '.$teams[$i]['id'].") and winning_team_id!=".$teams[$i]['id']." and winning_team_id>0";
					$row = mysqli_fetch_assoc(mysqli_query($conn,$sql));
					$matches_lost = $row['matches_lost'];
					
					$sql = "SELECT count(id) as matches_tied FROM tournament_matches WHERE tournament_id=$tournament_id and ( team1=".$teams[$i]['id'].' OR team2 = '.$teams[$i]['id'].")  and winning_team_id = -1";
					$row = mysqli_fetch_assoc(mysqli_query($conn,$sql));
					$matches_tied = $row['matches_tied'];
					
					$sql = "SELECT count(id) as matches_washed FROM tournament_matches WHERE tournament_id=$tournament_id and ( team1=".$teams[$i]['id'].' OR team2 = '.$teams[$i]['id'].")  and winning_team_id = -2";
					$row = mysqli_fetch_assoc(mysqli_query($conn,$sql));
					$matches_washed = $row['matches_washed'];
					
					$win_percent = round(($matches_won/($matches_played-$matches_washed))*100,2);
					
					$total_overs_faced = 0;
					
					$sql = 'SELECT sum(team1_score) as total_runs_scored,sum(team1_overs) as total_overs_faced,  
					sum(team2_score) as total_runs_conceded,sum(team2_overs) as total_overs_bowled FROM tournament_matches 
					WHERE tournament_id='.$tournament_id.' and (winning_team_id >0 OR winning_team_id = -1 OR winning_team_id = -2) and  batting_team1='.$teams[$i]['id'];
					$row1 = mysqli_fetch_assoc(mysqli_query($conn,$sql)); //var_dump($row1);
					
					$sql = 'SELECT team1_overs,maximum_overs,winning_team_id  FROM tournament_matches 
					WHERE tournament_id='.$tournament_id.' and (winning_team_id >0 OR winning_team_id = -1 OR winning_team_id = -2) and  batting_team1='.$teams[$i]['id'];
					$rs1 = mysqli_query($conn,$sql);
					while($row_data = mysqli_fetch_assoc($rs1)){
						if($row_data['winning_team_id'] == $teams[$i]['id']){
							$total_overs_faced+=$row_data['team1_overs'];
						}else{
							$total_overs_faced+=$row_data['maximum_overs'];
						}
					}
					
					$sql = 'SELECT sum(team2_score) as total_runs_scored,sum(team2_overs) as total_overs_faced , 
					sum(team1_score) as total_runs_conceded,sum(team1_overs) as total_overs_bowled FROM tournament_matches 
					WHERE tournament_id='.$tournament_id.' and (winning_team_id >0 OR winning_team_id = -1 OR winning_team_id = -2) and  batting_team2='.$teams[$i]['id'];
					$row2 = mysqli_fetch_assoc(mysqli_query($conn,$sql));//var_dump($row2);
					
					$sql = 'SELECT team2_overs ,maximum_overs,winning_team_id  FROM tournament_matches 
					WHERE tournament_id='.$tournament_id.' and (winning_team_id >0 OR winning_team_id = -1 OR winning_team_id = -2) and  batting_team2='.$teams[$i]['id'];
					$rs1 = mysqli_query($conn,$sql);
					while($row_data = mysqli_fetch_assoc($rs1)){
						if($row_data['winning_team_id'] == $teams[$i]['id']){
							$total_overs_faced+=$row_data['team1_overs'];
						}else{
							$total_overs_faced+=$row_data['maximum_overs'];
						}
					}

					
					$sql = "SELECT group_name FROM tournament_teams WHERE tournament_id=$tournament_id and team_id = ".$teams[$i]['id'];
					$row = mysqli_fetch_assoc(mysqli_query($conn,$sql));
					$group_name = $row['group_name'];
					
					//$data1 = ($row1['total_runs_scored']+$row2['total_runs_scored'])/($row1['total_overs_faced']+$row2['total_overs_faced']);
					$balls_arr = explode('.',$total_overs_faced);
					$overs_faced_converted = $balls_arr[0]+($balls_arr[1]/6);
					
					$balls_arr = explode('.',$row1['total_overs_bowled']+$row2['total_overs_bowled']);
					$overs_bowled_converted = $balls_arr[0]+($balls_arr[1]/6);
					
					if($overs_faced_converted>0){
						$data1 = ($row1['total_runs_scored']+$row2['total_runs_scored'])/($overs_faced_converted);
					}else{
						$data1 = 0;
					}
					if($overs_bowled_converted>0){
						$data2 = ($row1['total_runs_conceded']+$row2['total_runs_conceded'])/($overs_bowled_converted);
					}else{
						$data2 = 0;
					}
					
					//$data1 = ($row1['total_runs_scored']+$row2['total_runs_scored'])/($total_overs_faced);
					//$data2 = ($row1['total_runs_conceded']+$row2['total_runs_conceded'])/($row1['total_overs_bowled']+$row2['total_overs_bowled']);
					
					if(is_nan($data1)) $data1 = 0;
					if(is_nan($data2)) $data2 = 0;
					
					$net_run_rate = $data1-$data2;
					?>
						<tr>
							<td><?php echo $group_name; ?></td>
							<td><?php echo $teams[$i]['company_name']; ?></td>
							<td><?php echo $matches_played; ?></td>
							<td><?php echo $matches_won; ?></td>
							<td><?php echo $matches_lost; ?></td>
							<td><?php echo $matches_tied; ?></td>
							<td><?php echo $matches_washed; ?></td>
							<td><?php  ?></td>
							<td><?php if(is_nan($win_percent)) echo 0;else echo $win_percent; ?> %</td>
							<td><?php echo round($net_run_rate,3); ?></td>
							<td><a href="<?php echo WWW; ?>tournament/team-standings/<?php echo $tournament_id; ?>/<?php echo $teams[$i]['id']; ?>" title="View Details"><img alt="View Details" src="<?php echo WWW; ?>images/icons/view-details.png" border="0"></a></td>
						</tr>
					<?php endfor; ?>
				<?php endif; ?>
				</table>
			</div>  
		    <div class="clear"></div>
			</div>
		</div>
		<!--<div class="rightbar"><?php //include('common/right-panel.php');?></div>-->
		<div class="clear"></div>
	</div>
	
<script type="text/javascript" src="<?php echo WWW; ?>js/jquery.tablesorter.min.js"></script> 
<script type="text/javascript">
$(document).ready(function(){
	        $("#table-list").tablesorter( {sortList: [[7,1], [8,1], [9,1]]} ); 
});
</script>

<?php include('common/footer.php'); ?>