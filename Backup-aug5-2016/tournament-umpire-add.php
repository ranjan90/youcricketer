<?php include_once('includes/configuration.php');
$page = 'tournament-umpire-add.html';
$selected_country = getGeoLocationCountry(); 

$error = '';
$tournament_permission = 0;
$tournament_id = trim($_GET['t_id']);
$tournament_info = get_record_on_id('tournaments', $tournament_id);	

$page_title = 'Add New Umpire - '.ucwords($tournament_info['title']);

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
	if($user_id){
		$sql = 'select c.* from tournament_teams as t inner join companies as c on t.team_id=c.id where t.tournament_id = '.$tournament_id;
		$rs_club = mysqli_query($conn,$sql);
		while($row = mysqli_fetch_assoc($rs_club)){
			$tournament_clubs[] = $row;
		}
	}
}

if(isset($user_id) && !empty($user_id)){
	//$sql = "select * from tournament_permissions where tournament_id = $tournament_id and member_id = $user_id";
	//$rs_permission = mysqli_query($conn,$sql);
	if($tournament_info['user_id'] == $user_id){
		$tournament_permission = 1;
	}else{
		$tournament_permission = 0;
	}
}

if(isset($_POST['submit_btn']) && !empty($_POST['submit_btn'])){
	validate();
	if(empty($error)){
		if(empty($_POST['year_certified'])) $_POST['year_certified'] = 0;
		
		$sql = " INSERT INTO tournament_umpires SET tournament_id='".trim($tournament_id)."',umpire_name='".trim($_POST['umpire_name'])."',
		club_id='".trim($_POST['club_id'])."',year_certified='".trim($_POST['year_certified'])."',phone='".trim($_POST['phone'])."'
		,email='".trim($_POST['email'])."'";
		
		if(mysqli_query($conn,$sql)){
			$_SESSION['umpire_added'] = 1;
			header("Location:".WWW."tournament/umpires/list/".$tournament_id);
			exit();
		}else{
			$error = '<p id="error">Error in adding Umpire. Try again later</p>';
		}
	}
}

function validate(){
	global $error;
	
	if(empty($_POST['umpire_name'])){
		$error.= '<p id="error">Umpire Name is required field</p>';
	}
}

?>
<?php include('common/header.php'); ?>
<style>
.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
dl {min-height:145px;}
.content {width:1010px;}
.large-column {width:790px;}
.form-box {width:50%;}
.chk_left {float:left !important; }
</style>
	<div class="middle">
		<h1> Add New Umpire </h1>
		<h2><?php echo ucwords($tournament_info['title']); ?></h2>
		
		<div class="white-box content" id="dashboard">
			<?php if(empty($user_info)): ?>
				<div id="error">You are not logged... !</div>
			<?php endif; ?>
			
			<?php if(!empty($error)): ?>
				<?php echo $error; ?>
			<?php endif; ?>
		
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
				
				<?php if($tournament_permission){ ?>
				<form method="post"  enctype="multipart/form-data">
					<input type="hidden" name="action" value="submit">
					<fieldset>
						<h2>Umpire Details</h2>
						<p></p>
						<div class="form-box">
							<label>Name</label>
							<div class="text"><input type="text" name="umpire_name" id="umpire_name" class="input-login" value="<?php if(!empty($_POST['umpire_name'])) echo $_POST['umpire_name']; ?>"></div>
						</div>
						
						<div class="clear"></div>
						<div class="form-box">
							<label>Club</label>
							<div class="text">
							<select name="club_id" id="club_id"> 
							<option value="0">Independent*</option>
							<?php for($i=0;$i<count($tournament_clubs);$i++): if(isset($_POST['club_id']) && $_POST['club_id'] == $tournament_clubs[$i]['id']) $sel =  'selected';else $sel = '';  ?>
								<option <?php echo $sel; ?> value="<?php echo $tournament_clubs[$i]['id']; ?>"><?php echo $tournament_clubs[$i]['company_name']; ?></option>
							<?php endfor; ?>
							</select>
							</div>
						</div>
						<div class="clear"></div>
						<div class="form-box">
							<label>Year Certified</label>
							<div class="text"><input type="text"  name="year_certified" id="year_certified" class="input-login" value="<?php if(!empty($_POST['year_certified'])) echo $_POST['year_certified']; ?>"></div>
						</div>
						<div class="clear"></div>
						<div class="form-box">
							<label>Phone</label>
							<div class="text"><input type="text"  name="phone" id="phone" class="input-login" value="<?php if(!empty($_POST['phone'])) echo $_POST['phone']; ?>"></div>
						</div>
						<div class="clear"></div>
						
						<div class="form-box">
							<label>Email</label>
							<div class="text"><input type="text"  name="email" id="email" class="input-login" value="<?php if(!empty($_POST['email'])) echo $_POST['email']; ?>"></div>
						</div>
						<div class="clear"></div>
						
						<div class="form-box">
							<input type="button" name="cancel_btn" value=" Cancel " class="submit-login" onclick="window.location.href='<?php echo WWW; ?>tournament/umpires/list/<?php echo $tournament_id; ?>';">
							<input type="submit" name="submit_btn" value=" Submit " class="submit-login" >
						</div>
					</fieldset>
				</form>
				<?php }else{ ?>
				<div id="error">You do not have Permission for this Tournament... !</div>
				<?php } ?>
			</div>  
		    <div class="clear"></div>
			</div>
		</div>
		<!--<div class="rightbar"><?php //include('common/right-panel.php');?></div>-->
		<div class="clear"></div>
	</div>
	


<?php include('common/footer.php'); ?>