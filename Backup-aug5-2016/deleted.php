<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	<div class="middle">
		<h1>Deleted Messages</h1>
		<table width="100%" border="1" style="border-collapse:collapse;" id="inbox" style="margin-left:30px;">
		<tr title="Click for detail"  class="<?=($row_msg['status'] == '0')?'selected':'';?>">
				    		<th width="10%"> </th>
				    		<th width="38%" >From</th>
				    		
				        	<th width="">Subject</th>
				        	
				        	<th width="16%"> </th>
				        	<th width="60%"> Date </th>
				        </tr> </table>
		<div class="white-box content" id="dashboard">
		
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<?php 
				
				if(isset($_POST['action']) && $_POST['action'] == 'multi'){ 
					foreach(array_keys($_POST['ids']) as $ids){
						$record = get_record_on_id('messages', $ids);
						if($record['from_user_id'] == $_SESSION['ycdc_dbuid']){
							$key = 'p_delete_by_from';
						}else if($record['to_user_id'] == $_SESSION['ycdc_dbuid']){
							$key = 'p_delete_by_to';
						}
						mysqli_query($conn,"update messages set $key = '4' where id = '$ids' ");
					}
					echo '<div id="success">Message deleted successfully ... !</div>';
				}
				 ?>
			<div class="large-column" style="border-collapse:collapse; ">
				<form method="post" id="multiple">
					<input type="hidden" name="action" value="multi">
			<table width="100%" border="1" style="border-collapse:collapse;" id="inbox">
			
			    <? 	$rs_msg = mysqli_query($conn,"select * from messages where (from_user_id = '".$_SESSION['ycdc_dbuid']."' or to_user_id = '".$_SESSION['ycdc_dbuid']."' ) 
			    						and (p_delete_by_to = '1' or p_delete_by_from = '1') ORDER BY message_date DESC "); 
			    	if(mysqli_num_rows($rs_msg) == 0){
			        ?>
			        <tr>
			        	<td colspan="3">No Messages<td>
			        </tr>
			        <?
			        }else{
				    	while($row_msg = mysqli_fetch_assoc($rs_msg)){ 
				    	$thread =  $row_msg['thread_id'];
				    		?>
				    	<tr title="Click for detail" class="<?=($row_msg['status'] == '0')?'selected':'';?>">
				    		<td width="15%">
				    			<input type="checkbox" name="ids[<?=$row_msg['id']?>]">
				    			<a href="<?=WWW?>inbox-delete-<?=$row_msg['id']?>.html" title="Delete Message"><img src="<?=WWW?>images/icons/delete.png"></a>
				    		</td>
				    		<td width="25%"><?=get_combo('users','f_name',$row_msg['from_user_id'],'','text')."&nbsp;"?><? echo get_combo('users','last_name',$row_msg['from_user_id'],'','text');?></td>
				        	<td onclick="window.location='<?=WWW?>inbox-message-<?=$row_msg['id']?>-<?=$row_msg['thread_id']?>.html'" ><?=truncate_string($row_msg['subject'], 200)?></td>
				        	<td width="25%"><?=date_converter($row_msg['message_date'])." ".date("H:i:s",strtotime($row_msg['message_date']))?></td>
				        </tr>
				    <?  }
			    	} ?>
			    	<tr><td colspan="4"><hr></td></tr>
			    	<tr>
      <td colspan="2">
        <a class="checked" id="checkAll">Check All</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="checked" id="uncheckAll">Uncheck All</a>
      </td>
      <td>
        <select name="dd_status" class="dd1">
          <option value="" selected="selected">Select One</option>
          <option value="<?=DELETED_STATUS?>">Delete Messages</option>
        </select>
      </td>
      <td>
        <input type="submit" value="Update" class="submit-login">
      </td>
    </tr>
			</table>

		</form>
		    <div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<?php include('common/footer.php'); ?>