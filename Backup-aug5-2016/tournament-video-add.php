<?php include_once('includes/configuration.php');
$page = 'tournament-video-add.html';
$selected_country = getGeoLocationCountry(); 

$error = '';
$tournament_permission = 0;
$match_info = array();
$match_videos = array();
$match_id = trim($_GET['m_id']);
$sql = "SELECT tm.*,c1.company_name as team_bat_first,c2.company_name as team_bat_second
FROM tournament_matches as tm 
inner join companies as c1 on tm.batting_team1=c1.id  inner join companies as c2 on tm.batting_team2=c2.id
WHERE tm.id = $match_id and tm.status = 1 ";

$rs = mysqli_query($conn,$sql);
$match_info  = mysqli_fetch_assoc($rs);	

$tournament_id = $match_info['tournament_id'];

$tournament_info = get_record_on_id('tournaments', $tournament_id);	

$page_title = 'Add New Video - '.ucwords($tournament_info['title']);

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
}

if(isset($user_id) && !empty($user_id)){
	if($tournament_info['user_id'] == $user_id){
		$tournament_permission = 1;
	}else{
		$tournament_permission = 0;
	}
}

if(isset($_POST['submit_btn']) && !empty($_POST['submit_btn'])){
	validate();
	if(empty($error)){
		if($_POST['video_type'] == 'upload'){
			$targetFolder = 'video-uploads'; 
			$tempFile = $_FILES['video_file']['tmp_name'];
			$targetPath = $targetFolder;
			$fileParts = pathinfo($_FILES['video_file']['name']);
			$fileName = rand(99,9999).time().'.'.$fileParts['extension'];
			$targetFile = $targetPath . '/' .$fileName ;
			if(!move_uploaded_file($tempFile,$targetFile)){
				$error= 'Error in uploading video';
			}
			$sql = " INSERT INTO tournament_match_videos SET video_file='".trim($fileName)."', video_type = '".trim($_POST['video_type'])."',
			match_id='".$match_id."'";
		}else{
			$sql = " INSERT INTO tournament_match_videos SET embed_code='".mysqli_real_escape_string($conn,trim($_POST['embed_code']))."', video_type = '".trim($_POST['video_type'])."',
			match_id='".$match_id."'";
		}
		
		
		if(empty($error) && mysqli_query($conn,$sql)){
			$_SESSION['video_added'] = 1;
			header("Location:".WWW."tournament/matches/videos/list/".$match_id);
			exit();
		}else{
			$error = '<p id="error">Error in adding Video. Try again later</p>';
		}
	}
}

function validate(){
	global $error;
	
	if($_POST['video_type'] == 'embed' && empty($_POST['embed_code'])){
		$error.= '<p id="error">Video URL is required field</p>';
	}
	if($_POST['video_type'] == 'upload' && empty($_FILES['video_file']['name'])){
		$error.= '<p id="error">Video File is required field</p>';
	}
	
	$file_types = array('video/mp4','video/webm','video/ogg','video/mov');
	if(!empty($_FILES['video_file']['name']) && !in_array($_FILES['video_file']['type'],$file_types)){
		$error.= '<p id="error">These Video File extensions are allowed: mp4, webm, ogg, mov </p>';
	}
	
	$max_size = 1024*1024*10;		//10MB
	if(!empty($_FILES['video_file']['name']) && $_FILES['video_file']['size']>$max_size ){
		$error.= '<p id="error">Video File of maximum 10MB is allowed </p>';
	}
}

$sql = "SELECT * FROM tournament_match_videos WHERE match_id = $match_id ORDER BY id DESC";
$rs_venues = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_venues)){
	$match_videos[] = $row;
}

?>
<?php include('common/header.php'); ?>
<style>
.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
dl {min-height:145px;}
.content {width:1010px;}
.large-column {width:790px;}
.form-box {width:70%;}
.chk_left {float:left !important; }
#video_title,#video_url {width:410px;}
</style>
	<div class="middle">
		<h1> Match Videos </h1>
		<h2><?php echo $match_info['team_bat_first'] ?> vs <?php echo $match_info['team_bat_second'] ?> - <?php echo date('d F Y',strtotime($match_info['added_on'])); ?>
		</h2>
		<h2><?php echo ucwords($tournament_info['title']); ?></h2>
		
		<div class="white-box content" id="dashboard">
			<?php if(empty($user_info)): ?>
				<div id="error">You are not logged... !</div>
			<?php endif; ?>
			
			<?php if(!empty($error)): ?>
				<?php echo $error; ?>
			<?php endif; ?>
		
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
				
				<?php if($tournament_permission){ ?>
				<form method="post"  enctype="multipart/form-data">
					<input type="hidden" name="action" value="submit">
					<fieldset>
						<h2>Video Details</h2>
						<p></p>
						<div class="form-box">
							<label>Video Type</label>
							<div class="text" style="float:left;margin-left:50px;">
								<table><tr><td><input type="radio" name="video_type" id="video_type_file" checked value="upload" <?php if(isset($_POST['video_type']) && $_POST['video_type']=='upload') echo 'checked'; ?>></td><td> Upload&nbsp;</td>
								<td><input type="radio" name="video_type" id="video_type_em"  value="embed" <?php if(isset($_POST['video_type']) && $_POST['video_type']=='embed') echo 'checked'; ?>></td><td> Embed Video&nbsp;</td>
								</tr></table>
							</div>
						</div>
						
						<div class="clear"></div>
						<div class="form-box" id="video-emb-div" style="display:none;">
							<label>Embed Code</label>
							<div class="text" style="float:left;margin-left:15px;">
							<textarea style="width:100%;" name="embed_code" id="embed_code"  ><?php if(!empty($_POST['embed_code'])) echo $_POST['embed_code']; ?></textarea>
							<span style="font-weight:normal;margin-left:20px;float:left;width:100%;"> Please paste above YouTube/Vimeo/DailyMotion Embed Code</span></div>
						</div>
						<div class="clear"></div>
						<div class="form-box" id="video-file-div" >
							<label>Video File</label>
							<div class="text" style="float:left;margin-left:1px;width:72%;"><input type="file" style="float:left;margin-left:50px;" name="video_file" id="video_file"  value="">
							<br/><div style="font-weight:normal;clear:both;float:right;margin-top:10px;">Allowed File Types: mp4, webm, ogg, mov | Max File size: 10MB</div></div>
						</div>
						
						<div class="clear"></div>
						<div class="form-box" style="float:left;margin-left:5px;">
							<input type="button" name="cancel_btn" value=" Cancel " class="submit-login" onclick="window.location.href='<?php echo WWW; ?>tournament/matches/videos/list/<?php echo $match_id; ?>';">
							<input type="submit" name="submit_btn" value=" Submit " class="submit-login" >
						</div>
					</fieldset>
				</form>
					<div class="clear"><br/></div>
					<?php for($i=0;$i<count($match_videos);$i++): ?>
						<?php if($match_videos[$i]['video_type'] == 'embed'): ?>
							<?php echo $match_videos[$i]['embed_code']; ?>
						<?php else: ?>
							<?php $video_path = WWW.'video-uploads/'.$match_videos[$i]['video_file']; ?>
							<video width="540" height="300" controls>
							 <source src="<?php echo $video_path; ?>" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
							<source src="<?php echo $video_path; ?>" type='video/ogg; codecs="theora, vorbis"'>
							<source src="<?php echo $video_path; ?>" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
							<source src="<?php echo $video_path; ?>" type='video/webm; codecs="vp8, vorbis"'>
							</video>
						<?php endif; ?>
						<a href=""><img border="0" src="<?php echo WWW; ?>images/erase.png"></a>
						<div class="clear"><br/></div>
					<?php endfor; ?>
				<?php }else{ ?>
				<div id="error">You do not have Permission for this Tournament... !</div>
				<?php } ?>
			</div>  
		    <div class="clear"></div>
			</div>
		</div>
		<!--<div class="rightbar"><?php //include('common/right-panel.php');?></div>-->
		<div class="clear"></div>
	</div>
	
<script>
$(document).ready(function(){
	$("#video_type_em").bind('click',function(){
		$("#video-emb-div").show('slow');
		$("#video-file-div").hide('slow');
	});
	
	$("#video_type_file").bind('click',function(){
		$("#video-emb-div").hide('slow');
		$("#video-file-div").show('slow');
	});
	<?php if(isset($_POST['video_type']) && $_POST['video_type']=='embed'): ?>
		$("#video-emb-div").show();
		$("#video-file-div").hide();
	<?php endif; ?>
	<?php if(isset($_POST['video_type']) && $_POST['video_type']=='upload'): ?>
		$("#video-emb-div").hide();
		$("#video-file-div").show();
	<?php endif; ?>
});
</script>

<?php include('common/footer.php'); ?>