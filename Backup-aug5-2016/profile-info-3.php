<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	<div class="middle">
		<h1>Profile Information</h1>

		<? 	$user_id = $row_user['id'];
			
			if(isset($_POST) && !empty($_POST)){
				//print_r($_POST); exit;
				$state_id 		= $_POST['state_id'];
				$city_name		= $_POST['city_name'];
				$zipcode 		= $_POST['zipcode'];
				$streetaddress 	= $_POST['streetaddress'];
				mysqli_query($conn,"update users set post_code = '$zipcode', state_id = '$state_id',city_name = '$city_name',street_address='$streetaddress'  where id = '$user_id'");
				echo '<div id="success"><b>Success : </b>Information updated ... !</div><br><br>';
			}
			$row_country = get_record_on_id('countries', $row_user['country_id']);
			$row_city 	 = get_record_on_id('cities', $row_user['city_id']) ;
			$row_state 	 = get_record_on_id('states', $row_city['state_id']);
			
			// To Get User's State And City if he already has submited...
			$state          = mysqli_query($conn,"SELECT state_id,city_name FROM users WHERE id = '$user_id'");
			$user_state     = mysqli_fetch_assoc($state);
			$user_state_id  = $user_state['state_id'];
			$city_name      =$user_state['city_name'];
			?>

		<div class="white-box content" id="dashboard">
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
			<? include('common/profile-navigation.php');?>
			<form method="post" action="" enctype="multipart/form-data">
				<input type="hidden" name="action" value="submit">
				<fieldset>
					<legend>Location Details</legend>
					<div class="form-box">
						<label>Country </label>
					</div>
					<div class="form-box">
						<div class="text" style="margin-left:20px; float:left;"><?=get_combo('countries','name',$row_user['country_id'],'','text')?></div>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>State </label>
					</div>
					<div class="form-box">
						<select style="margin-left:20px; width: 275px; float:left;" name="state_id" class="validate[required]">
							<option selected="selected" value=""></option>
							<? 	$rs_states = mysqli_query($conn,"select * from states where country_id = '".$row_user['country_id']."'"); 
								while($row_state_l = mysqli_fetch_assoc($rs_states)){ ?>
								<option <?=($user_state_id == $row_state_l['id'])?'selected="selected"':'';?> value="<?=$row_state_l['id']?>"><?=$row_state_l['name']?></option>
							<?  } ?>
						</select>
					</div>
					
					<div class="clear"></div>
					<div class="form-box">
						<label>City </label>
					</div>
					<div class="form-box">
						<div id="city">
						<?php /*?>
						<select name="city_id" class="validate[required]">
							<option selected="selected" value=""></option>
							<? 	$rs_cities = mysqli_query($conn,"select * from cities where state_id = '".$row_state['id']."'"); 
								while($row_city_l = mysqli_fetch_assoc($rs_cities)){ ?>
								<option <?=($row_city['id'] == $row_city_l['id'])?'selected="selected"':'';?> value="<?=$row_city_l['id']?>"><?=$row_city_l['name']?></option>
							<?  } ?>
						</select>
						<?php */ ?>
						<input type="text" required name="city_name" maxlength="25" value="<?php if($city_name){ echo $city_name;} ?>" style="margin-left:20px; float:left;" type="text" class="input-login" > 
						</div>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Zip/Postal code</label>
					</div>
					<div class="form-box">
						<input style="margin-left:20px; float:left;" type="text" name="zipcode" class="input-login <?=($row_user['country_id'] == '221' || $row_user['country_id'] == '222')?'validate[required]':'';?>" maxlength="6" value="<?=$row_user['post_code']?>" >
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Street Address</label>
					</div>
					<? $stree =mysqli_fetch_assoc(mysqli_query($conn,"SELECT street_address FROM users WHERE id = '$user_id'"));?>
					<div class="form-box">
					<textarea style="margin-left:20px; float:left; width:265px; height:100px;overflow: hidden;" name="streetaddress" maxlength='170'><?php if($row_user['street_address']){echo trim($row_user['street_address']);} ?></textarea>	
					</div>
					<div class="form-box" style="float: right;">
							<label><a class="popup-link" href="#why-do-we-need-your-street-address">Why do we need your street address?</a></label>
						</div>
						<div class="clear"></div>
					</fieldset>
					<div class="clear"></div>
					<table width="100%" class="profile-action">
						<tr>
							<td width="33%">
								<a id="cancel" class="submit-login" style="position:relative; left:-150px; top:10px;" href="<?=WWW?>dashboard.html">Cancel</a>
							</td>
							<td width="33%">
								<input id="update" type="submit" value=" Update " style="left:-100px; position:relative;" class="submit-login">
							</td>
							<td width="33%">
								<a class="submit-login" style="position:relative;left:0px;  top:10px;" href="<?=WWW?>profile-information-step-4.html">Skip</a>
							</td>
						</tr>
					</table>
				
			</form>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>

<div class="popup hide" id="why-do-we-need-your-street-address" style="width:400px; margin-top: -500px;">
	<h1 style="font-size: 14px;">Why do we need your street Address 
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h1>
		<? $row = get_record_on_id('cms',19);?>
		<div>
			<?=$row['content']?>
		    <div class="clear"></div>
		</div>
</div>
<style>
#city select{float:left; margin-left:20px;}
</style>
<script src="<?php echo WWW?>js/jquery-1.8.0.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

	$('#dashboard .black-box h2').click(function(){
		$('#dashboard .black-box ul').css('display','none');
		var id = this.id;
		$('#'+id+'-box ul').css('display','block');
	});

	$('.close-popup').click(function(){
		$('.popup').addClass('hide');
	});
	
	$('a.popup-link').click(function(){
		var link = $(this).attr('href');
		
		$(link).removeClass('hide');
	});
/*
	$('select[name=state_id]').live('change',function(){
		var id = $('select[name=state_id]').val();
		$.ajax({type	: 'POST', 
				url		: WWW + 'includes/get-cities.php', 
				data	: ({id:id}),
				success	: function(msg){
					$("#city").html(msg);
				} */ // this function is not require now becoz city will be entered manualy by user 
		
});
</script>
<?php include('common/footer.php'); ?>