<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	<div class="middle">
		<h1>Profile Information</h1>

		<? 	$user_id = $row_user['id'];
			
			if(isset($_POST) && !empty($_POST)){
				$set = array();
				$set[] = "type 		= '".$_POST['player_type']."'";
				$set[] = "first_class_player = '".$_POST['first_class_player']."'";
				$set[] = "match_types = '".implode(',',$_POST['matchType'])."'";
				$set[] = "game_types = '".implode(',',$_POST['gameType'])."'";

				if($user_type_id == '17'){
					$set[] = "is_company = '1'";
				}else{
					$set[] = "is_company = '0'";
				}

				if($player_type == 'Batsman'){ // batsman
					$batsman_type = $_POST['batsman_type'];
					$batting_order= $_POST['batting_order'];
					
					$set[] 	= "type_of_batsman= '".$batsman_type."'";
					$set[] 	= "batting_order= '".$batting_order."'";
				}else if($player_type == 'Bowler'){
					$bowler_type  = $_POST['bowler_type'];
					$bowling_arm  = $_POST['bowling_arm'];
					$bowling_prefer=$_POST['bowling_prefer'];

					$set[] 	= "type_of_bowler= '".$bowler_type."'";
					$set[] 	= "which_arm_bowler= '".$bowling_arm."'";
					$set[] 	= "bowl_between_overs= '".$bowling_prefer."'";
				}
				
				if($_POST['first_class_player'] == '1'){
					
						
					if(!empty($_POST['espn'])){
						$link = '';
						$pattern = "!^https?://(?:[a-zA-Z0-9-]+\.)*espncricinfo\.com(?:/[^#]*(?:#[^#]+)?)?$!"; //setting pattern
						preg_match($pattern, trim($_POST['espn']), $matches);
						// we need to get user link if he has already posted .... 
						$user_espn_link = mysqli_query($conn,"SELECT espncrickinfo FROM users WHERE id = '".$row_user['id']."'");
						$user_link = mysqli_fetch_assoc($user_espn_link);
						preg_match($pattern, trim($user_link['espncrickinfo']), $user_link_match);
						if($user_link_match){
							$link = $user_link['espncrickinfo'];
						}
						///
										if($matches){
													$link = $_POST['espn'];	
														
										}
												else {
														
													echo '<div id="error"><b> Error : </b> Not a Valid Espncricinfo link ... !</div><br><br>';
													}
					
					} 
					
				}
				
				$set[] 	= "espncrickinfo= '".$link."'";
		
				$query  = "update users set ".implode(',',$set)." where id = '".$row_user['id']."'";
				mysqli_query($conn,$query);
				echo '<div id="success"><b>Success : </b>Information updated ... !</div><br><br>';
			}
			
			?>

		<div class="white-box content" id="dashboard">
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
			<? include('common/profile-navigation.php');?>
			<form method="post" action="" enctype="multipart/form-data">
				<input type="hidden" name="action" value="submit">
				<fieldset>
					<legend>Individual Traits</legend>
					<div class="form-box">
							<label>First Class Category</label>
						</div>
						<div class="form-box">
							<div style="float:left; margin-left:20px;">
							<?=get_first_class_player_combo($row_user['first_class_player'])?>
							</div>
						</div>
						<div class="clear"></div>
						<div id="espn" class="<?=($row_user['first_class_player'] != '1')?'hide':'';?>">
							<div class="form-box">
								<label>Paste your ESPNCRICINFO.com Link Here</label>
							</div>
							<div class="form-box">
							<textarea name="espn" style="width: 255px;"><?=(!empty($row_user['espncrickinfo']))?trim($row_user['espncrickinfo']):''?></textarea>	</div>
						</div>
						<div class="clear"></div>
						<?php $type = mysqli_query($conn,"SELECT user_type_id FROM users WHERE id = '$user_id'");
						 	$user_type_id = mysqli_fetch_assoc($type);
						 	$type_name = mysqli_query($conn,"SELECT *  FROM user_types WHERE id = '".$user_type_id['user_type_id']."'");
						 	$user_type = mysqli_fetch_assoc($type_name);
						 	if($user_type['name'] == "Player"){
						?>
						<div class="form-box">
							<label>Type of Player </label>
						</div>
						<div class="form-box">
							<div style="float:left; margin-left:20px;">
							<select class="validate[required]" name="player_type">
								<option value="" selected="selected">Select One</option>
								<option <?=($row_user['type'] == 'Batsman')?'selected="selected"':'';?> value="Batsman">Batsman</option>
								<option <?=($row_user['type'] == 'Bowler')?'selected="selected"':'';?> value="Bowler">Bowler</option>
								<option <?=($row_user['type'] == 'All Rounder')?'selected="selected"':'';?> value="All Rounder">All Rounder</option>
								<option <?=($row_user['type'] == 'Specialist Wicketkeeper')?'selected="selected"':'';?> value="Specialist Wicketkeeper">Specialist Wicketkeeper</option>
							</select>
							</div>
						</div>
						<?php }?>
						<div id="player-type-batsman" class="<?=($row_user['type'] != 'Batsman')?'hide':'';?>">
							<div class="form-box">
								<label>Type of Batsman</label>
							</div>
							<div class="form-box">
								<select name="batsman_type" style="float:left; margin-left:20px;" >
									<option <?=($row_user['type_of_batsman'] == 'Left-handed')?'selected="selected"':'';?> value="Left-handed">Left-handed</option>
									<option <?=($row_user['type_of_batsman'] == 'Right-handed')?'selected="selected"':'';?><?=(empty($row_user['type_of_batsman']))?'selected="selected"':'';?> value="Right-handed">Right-handed</option>
								</select>
							</div>
							<div class="clear"></div>
							<div class="form-box">
								<label>Preferred Batting Order</label>
							</div>
							<div class="form-box">
								<select name="batting_order" style="float:left; margin-left:20px;" >
									<option selected="selected" value="">- Select One -</option>
									<option <?=($row_user['batting_order'] == 'Top Order (1 - 3)')?'selected="selected"':'';?> value="Top Order (1 - 3)">Top Order (1 - 3)</option>
									<option <?=($row_user['batting_order'] == 'Middle Order (4 - 7)')?'selected="selected"':'';?> value="Middle Order (4 - 7)">Middle Order (4 - 7)</option>
									<option <?=($row_user['batting_order'] == 'Lower Order (8 - 11)')?'selected="selected"':'';?> value="Lower Order (8 - 11)">Lower Order (8 - 11)</option>
								</select>
							</div>
						</div>
						<div id="player-type-bowler" class="<?=($row_user['type'] != 'Bowler')?'hide':'';?>">
							<div class="form-box">
								<label>Type of Bowler</label>
							</div>
							<div class="form-box">
								<select name="bowler_type" style="float:left; margin-left:20px;" >
									<option selected="selected" value="">- None -</option>
									<option <?=($row_user['type_of_bowler'] == 'Fast')?'selected="selected"':'';?> value="Fast">Fast</option>
									<option <?=($row_user['type_of_bowler'] == 'Medium-fast')?'selected="selected"':'';?> value="Medium-fast">Medium-fast</option>
									<option <?=($row_user['type_of_bowler'] == 'Leg Spinner')?'selected="selected"':'';?> value="Leg Spinner">Leg Spinner</option>
									<option <?=($row_user['type_of_bowler'] == 'Off Spinner')?'selected="selected"':'';?> value="Off Spinner">Off Spinner</option>
								</select>
							</div>
							<div class="clear"></div>
							<div class="form-box">
								<label>With Which Arm do you Bowl?</label>
							</div>
							<div class="form-box">
								<select name="bowling_arm" style="float:left; margin-left:20px;" >
									<option selected="selected" value="" >- None -</option>
									<option <?=($row_user['which_arm_bowler'] == 'Left')?'selected="selected"':'';?> value="Left">Left</option>
									<option <?=($row_user['which_arm_bowler'] == 'Right')?'selected="selected"':'';?> value="Right">Right</option>
								</select>
							</div>
							<div class="clear"></div>
							<div class="form-box">
								<label>Your Bowling Overs Preference in One Day Games?</label>
							</div>
							<div class="form-box">
								<select name="bowling_prefer" style="float:left; margin-left:20px;" >
									<option selected="selected" value="">- None -</option>
									<option <?=($row_user['bowl_between_overs'] == '1 - 12 Overs')?'selected="selected"':'';?> value="1 - 12 Overs">1 - 12 Overs</option>
									<option <?=($row_user['bowl_between_overs'] == '13 - 20 Overs')?'selected="selected"':'';?> value="13 - 20 Overs">13 - 20 Overs</option>
									<option <?=($row_user['bowl_between_overs'] == '21 - 30 Overs')?'selected="selected"':'';?> value="21 - 30 Overs">21 - 30 Overs</option>
									<option <?=($row_user['bowl_between_overs'] == '31 - 40 Overs')?'selected="selected"':'';?> value="31 - 40 Overs">31 - 40 Overs</option>
									<option <?=($row_user['bowl_between_overs'] == '41 - 50 Overs')?'selected="selected"':'';?> value="41 - 50 Overs">41 - 50 Overs</option>
									<option <?=($row_user['bowl_between_overs'] == 'As Needed')?'selected="selected"':'';?> value="As Needed">As Needed</option>
								</select>
							</div>
							<div class="clear"></div>
						</div>
						<div class="clear"></div>
						<div>
							<div class="form-box">
								<label>Preferred Game Type</label>
							</div>
							<div class="form-box">
								<div style="float:left; margin-left:20px;">
								<? $prefferedGameTypeData = explode(',',$row_user['game_types']);?>
								<? foreach($prefferedGameType as $ma){ ?>
								<p><input style="float:left;" type="checkbox" <?=(in_array($ma, $prefferedGameTypeData))?'checked="checked"':'';?> name="gameType[]" value="<?=$ma?>">&nbsp; <?=$ma?></p>
								<? } ?>
							</div>
						</div>
						<div class="clear"></div>
						<div id="match-type" >
							<div class="form-box">
								<label>Preferred Match Type</label>
							</div>
							<div class="form-box">
								<div style="float:left; margin-left:20px;">
								<? $matchArrayData = explode(',',$row_user['match_types']);?>
								<? foreach($matchArray as $ma){ ?>
								<p><input style="float:left;" type="checkbox" <?=(in_array($ma, $matchArrayData))?'checked="checked"':'';?> name="matchType[]" value="<?=$ma?>">&nbsp; <?=$ma?></p>
								<? } ?>
							</div>
						</div>
					<div class="clear"></div>
					</fieldset>
					<table width="100%" class="profile-action">
						<tr>
							<td width="33%">
								<a id="cancel" class="submit-login" style="position:relative; left:-150px; top:10px;" href="<?=WWW?>dashboard.html">Cancel</a>
							</td>
							<td width="33%">
								<input id="update" type="submit" value=" Update " style="left:-100px; position:relative;" class="submit-login">
							</td>
							<td width="33%">
								<a class="submit-login" style="position:relative;left:0px;  top:10px;" href="<?=WWW?>profile-information-step-5.html">Skip</a>
							</td>
						</tr>
					</table>
				
			</form>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<style>
#city select{float:left; margin-left:20px;}
</style>
<script src="<?php echo WWW?>js/jquery-1.8.0.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

	$('#dashboard .black-box h2').click(function(){
		$('#dashboard .black-box ul').css('display','none');
		var id = this.id;
		$('#'+id+'-box ul').css('display','block');
	});
	$('select[name=user_type_id]').live('change',function(){
		var id = $('select[name=user_type_id]').val();
		if(id != '2'){
			$('#dependent-player').addClass("hide");
		}else{
			$('#dependent-player').removeClass("hide");
		}
	});

	$('select[name=player_type]').live('change',function(){
		var id = $('select[name=player_type]').val();
		if(id == 'Batsman'){
			$('#player-type-batsman').removeClass('hide');
			$('#player-type-batsman select[name=batsman_type]').addClass('validate[required]');
			$('#player-type-batsman select[name=batting_order]').addClass('validate[required]');

			$('#player-type-bowler').addClass('hide');
			$('#player-type-bowler select[name=bowler_type]').removeClass('validate[required]');
			$('#player-type-bowler select[name=bowling_arm]').removeClass('validate[required]');
			$('#player-type-bowler select[name=bowling_prefer]').removeClass('validate[required]');
		}else if(id == 'Bowler'){
			$('#player-type-batsman').addClass('hide');
			$('#player-type-batsman select[name=batsman_type]').removeClass('validate[required]');
			$('#player-type-batsman select[name=batting_order]').removeClass('validate[required]');

			$('#player-type-bowler').removeClass('hide');
			$('#player-type-bowler select[name=bowler_type]').addClass('validate[required]');
			$('#player-type-bowler select[name=bowling_arm]').addClass('validate[required]');
			$('#player-type-bowler select[name=bowling_prefer]').addClass('validate[required]');
		}else{
			$('#player-type-batsman').addClass('hide');
			$('#player-type-bowler').addClass('hide');
			$('#player-type-batsman select[name=batsman_type]').removeClass('validate[required]');
			$('#player-type-batsman select[name=batting_order]').removeClass('validate[required]');
			$('#player-type-bowler select[name=bowler_type]').removeClass('validate[required]');
			$('#player-type-bowler select[name=bowling_arm]').removeClass('validate[required]');
			$('#player-type-bowler select[name=bowling_prefer]').removeClass('validate[required]');
		}
	});

	$('select[name=first_class_player]').live('change',function(){
		var id = $('select[name=first_class_player]').val();
		if(id == '1'){
			$('#espn').removeClass('hide');
			$('#espn input[name=espn]').addClass('validate[required, custom[url]]');
		}else{
			$('#espn').addClass('hide');
			$('#espn input[name=espn]').removeClass('validate[required, custom[url]]');
		}
	});
});
</script>
<?php include('common/footer.php'); ?>