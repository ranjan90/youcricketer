<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	<div class="middle">
		<h1>Account Information</h1>

		<? 	if(isset($_POST) && !empty($_POST) && $_POST['action'] == 'submit'){

				$set = array();

				$set[] = "f_name = '".$_POST['f_name']."'";
				$set[] = "m_name = '".$_POST['m_name']."'";
				$set[] = "last_name = '".$_POST['last_name']."'";
				$set[] = "generic_id = '".$_POST['generic_id']."'";
				$set[] = "country_id = '".$_POST['country_id']."'";
				$set[] = "date_of_birth = '".date_converter($_POST['date_of_birth'], 'Y-m-d')."'";
				$set[] = "user_type_id = '".$_POST['user_type_id']."'";
				$set[] = "type 		= '".$_POST['player_type']."'";
				$set[] = "first_class_player = '".$_POST['first_class_player']."'";
				$set[] = "match_types = '".implode(',',$_POST['matchType'])."'";

				if($user_type_id == '17'){
					$set[] = "is_company = '1'";
				}else{
					$set[] = "is_company = '0'";
				}

				if($player_type == '0'){ // batsman
					$batsman_type = $_POST['batsman_type'];
					$batting_order= $_POST['batting_order'];
					
					$set[] 	= "type_of_batsman= '".$batsman_type."'";
					$set[] 	= "batting_order= '".$batting_order."'";
				}else if($player_type == '1'){
					$bowler_type  = $_POST['bowler_type'];
					$bowling_arm  = $_POST['bowling_arm'];
					$bowling_prefer=$_POST['bowling_prefer'];

					$set[] 	= "type_of_bowler= '".$bowler_type."'";
					$set[] 	= "which_arm_bowler= '".$bowling_arm."'";
					$set[] 	= "bowl_between_overs= '".$bowling_prefer."'";
				}
				
				if($_POST['first_class_player'] == '1'){
					$espn 		= $_POST['espn'];
					
					$set[] 	= "espncrickinfo= '".$espn."'";
				}

				$query  = "update users set ".implode(',',$set)." where id = '".$row_user['id']."'";
				//echo $query;exit;
				$user_id= $row_user['id'];

				if(mysqli_query($conn,$query)){
					if(!empty($_FILES['photo']['name'])){
						if(!is_dir('users/'.$user_id)){
							mkdir('users/'.$user_id,0777);
						}
						chmod('users/'.$user_id,0777);
						if(!is_dir('users/'.$user_id.'/thumbnails')){
							mkdir('users/'.$user_id.'/thumbnails',0777);
						}
						chmod('users/'.$user_id.'/thumbnails',0777);

						$filename 	= friendlyURL($f_name).'.jpg';
						$image 		= new SimpleImage();
						$image->load($_FILES["photo"]["tmp_name"]);
						$image->save('users/'.$user_id.'/'.$filename);
						chmod('users/'.$user_id.'/'.$filename,0777);
						
						$rs_photos = mysqli_query($conn,"select * from photos where entity_id = '$user_id' and entity_type = 'users' and is_default = '1'");
						if(mysqli_num_rows($rs_photos) > 0){
							mysqli_query($conn,"update photos set file_name = '$filename' where entity_id = '$user_id' and entity_type = 'users' and is_default = '1'");
						}else{
							mysqli_query($conn,"insert into photos (file_name, title, entity_id, entity_type, is_default) values ('$filename','$f_name','$user_id','users','1')");
						}		
						
						$image->load($_FILES["photo"]["tmp_name"]);
						$image->resizeToWidth('128');
						$image->save('users/'.$user_id.'/thumbnails/'.$filename);
						chmod('users/'.$user_id.'/thumbnails/'.$filename,0777);
					}

	                echo '<div id="success"><b>Success : </b>Information updated ... !</div><br><br>';
	                ?>
	                <script>
	                	window.location = '<?=WWW?>dashboard.html';
					</script>
	                <?
				}
			}
			$row_photo = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_id = '".$row_user['id']."' and entity_type = 'users' and is_default = '1'"));
		?>

		<div class="white-box content" id="dashboard">
			>
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
				<div id="success">Please fill this form to complete step 1 ... !</div>
				<form method="post" action="" enctype="multipart/form-data">
					<input type="hidden" name="action" value="submit">
					<fieldset>
						<h2>Account Details</h2>
						<div class="form-box">
							<label>Registered On</label>
							<div class="text"><?=date_converter($row_user['create_date'])?></div>
						</div>
						<div class="clear"></div>
						<div class="form-box">
							<label>Email Address </label>
							<div class="text"><?=$row_user['email']?></div>
						</div>
						<div class="clear"></div>
						<div class="form-box">
							<label>Password</label>
							<div class="text"><a href="<?=WWW?>change-password.html" title="Change your password">Change Password</a></div>
						</div>
						<div class="clear"></div>
						<div class="form-box">
							<a href="<?=WWW?>dashboard.html" class="submit-login left">Back</a>
						</div>
						<div class="form-box">
							<input type="submit" value=" Update " class="submit-login">
						</div>
					</fieldset>
				</form>
			</div>
			<div class="clear"></div>	
		</div>
		<div class="clear"></div>
	</div>
<div class="popup hide" id="why-do-we-need-your-birthday" style="width:600px;">
	<h1>Why do we need your Birthday 
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h1>
		<? $row = get_record_on_id('cms',7);?>
		<div>
			<?=$row['content']?>
		    <div class="clear"></div>
		</div>
</div>
<script src="<?php echo WWW?>js/jquery-1.8.0.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=WWW?>js/jquery-ui.min.js"></script>
<link href="<?=WWW?>css/jquery-ui.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="<?=WWW?>/js/jquery.validationengine.js"></script>
<script type="text/javascript" src="<?=WWW?>js/validation-engine.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('form').validationEngine();
	$('.close-popup').click(function(){
		$('.popup').addClass('hide');
	});

	$('a.popup-link').click(function(){
		var link = $(this).attr('href');
		if(link == '#edit-activity'){
			var id = this.id;
			$.ajax({type	: 'POST', 
				url		: WWW + 'includes/get-record.php', 
				data	: ({table:'activity',field:'activity',id:id}),
				success	: function(msg){
					$(link+' textarea').val(msg);
					$(link+' #recordId').val(id);
				}
			});
		}
		$(link).removeClass('hide');
	});

	$('select[name=player_type]').live('change',function(){
		var id = $('select[name=player_type]').val();
		if(id == '0'){
			$('#player-type-batsman').removeClass('hide');
			$('#player-type-batsman select[name=batsman_type]').addClass('validate[required]');
			$('#player-type-batsman select[name=batting_order]').addClass('validate[required]');

			$('#player-type-bowler').addClass('hide');
			$('#player-type-bowler select[name=bowler_type]').removeClass('validate[required]');
			$('#player-type-bowler select[name=bowling_arm]').removeClass('validate[required]');
			$('#player-type-bowler select[name=bowling_prefer]').removeClass('validate[required]');
		}else if(id == '1'){
			$('#player-type-batsman').addClass('hide');
			$('#player-type-batsman select[name=batsman_type]').removeClass('validate[required]');
			$('#player-type-batsman select[name=batting_order]').removeClass('validate[required]');

			$('#player-type-bowler').removeClass('hide');
			$('#player-type-bowler select[name=bowler_type]').addClass('validate[required]');
			$('#player-type-bowler select[name=bowling_arm]').addClass('validate[required]');
			$('#player-type-bowler select[name=bowling_prefer]').addClass('validate[required]');
		}else{
			$('#player-type-batsman').addClass('hide');
			$('#player-type-bowler').addClass('hide');
			$('#player-type-batsman select[name=batsman_type]').removeClass('validate[required]');
			$('#player-type-batsman select[name=batting_order]').removeClass('validate[required]');
			$('#player-type-bowler select[name=bowler_type]').removeClass('validate[required]');
			$('#player-type-bowler select[name=bowling_arm]').removeClass('validate[required]');
			$('#player-type-bowler select[name=bowling_prefer]').removeClass('validate[required]');
		}
	});

	$('select[name=first_class_player]').live('change',function(){
		var id = $('select[name=first_class_player]').val();
		if(id == '1'){
			$('#espn').removeClass('hide');
			$('#espn input[name=espn]').addClass('validate[required, custom[url]]');
		}else{
			$('#espn').addClass('hide');
			$('#espn input[name=espn]').removeClass('validate[required, custom[url]]');
		}
	});

	var currentYear = (new Date).getFullYear();
	var date_of_year= currentYear - 70;
	var last_of_year= currentYear - 13;
	$('.calender').datepicker({changeYear:"true", maxDate:0, yearRange: date_of_year +":"+ last_of_year});

	$('#dashboard .black-box:first ul').css('display','block');

	$('#dashboard .black-box h2').click(function(){
		$('#dashboard .black-box ul').css('display','none');
		var id = this.id;
		$('#'+id+'-box ul').css('display','block');
	});
	$('input[name=photo]').change(function(){
		var val = this.value;
		var filesize = Math.round((this.files[0].size)/1024,2);
		if(filesize > 2000){
			$(this).val('');
            alert("Image is too large ... !");
		}
	});
});
</script>
<?php include('common/footer.php'); ?>