<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>


	<div class="middle">
		<h1>Account Information</h1>

		<? 	
			$user_id = $_SESSION['ycdc_dbuid'];
			
			if(isset($_POST) && !empty($_POST) && $_POST['action'] == 'close'){
				$salt = 'zxzxqb79jsMaEzXMvCO2iWtzU2gT7rBoRmQzlvj5yNVgP4aGOrZ524pT5KoTDJ7vNiINPOVD9Tzx';
				$token = sha1($salt . $_SESSION['time']);
				if($token != $_POST['token'])
				{
					exit();
				}
				$closingComments = $_POST['close_comments'];
				mysqli_query($conn,"update users set closing_note = '$closingComments', status = '5' where id = '$user_id' and is_company = '0' ");
				?>
				<script>
					alert('Your account has been closed ... !');
					window.location = '<?=WWW?>logout.php';
				</script>
				<?
			}
			
			if(isset($_POST) && !empty($_POST) && $_POST['action'] == 'update'){

				$set = array();
				
				if($_POST['is_newsletter'] == 'on'){
					$isNews = '1';
				}else{
					$isNews = '0';
				}

				$set[] = "f_name = '".$_POST['f_name']."'";
				//$set[] = "m_name = '".$_POST['m_name']."'";
				$set[] = "last_name = '".$_POST['last_name']."'";
				//$set[] = "generic_id = '".$_POST['generic_id']."'";
				$set[] = "is_newsletter = '".$isNews."'";
				$set[] = "country_id = '".$_POST['country_id']."'";
				$set[] = "date_of_birth = '".$_POST['year'].'-'.$_POST['month'].'-'.$_POST['day']."'";
				$set[] = "user_type_id = '".$_POST['user_type_id']."'";
				$set[] = "type 		= '".$_POST['player_type']."'";
				$set[] = "timezone = '".$_POST['timezone']."'";
				$set[] = "gender = '".$_POST['gender']."'";
				/*
				$rsCheck  = mysqli_fetch_assoc(mysqli_query($conn,"select * from users where yc_email = '".$_POST['yc_email']."'"));
				if(isset($_POST['yc_email']) && mysqli_num_rows($rsCheck) == 0){
					$set[] = "yc_email = '".$_POST['yc_email']."'";
				}else{
					echo '<div id="error">Email Letters already exists ... !</div>';
				}
				*/
				$rsCheck  = mysqli_fetch_assoc(mysqli_query($conn,"select * from users where permalink = '".$_POST['permalink']."'"));
				if(isset($_POST['permalink'])){
					if(mysqli_num_rows($rsCheck) == 0){
						$set[] = "permalink = '".$_POST['permalink']."'";
					}
				}

				if($user_type_id == '17'){
					$set[] = "is_company = '1'";
				}else{
					$set[] = "is_company = '0'";
				}
				
				$query  = "update users set ".implode(',',$set)." where id = '".$row_user['id']."'";
				//echo $query;exit;
				$user_id= $row_user['id'];

				if(mysqli_query($conn,$query)){
					/*
					if(!empty($_FILES['photo']['name'])){
						
						if(!is_dir('users/'.$user_id)){
							mkdir('users/'.$user_id,0777);
						}
						chmod('users/'.$user_id,0777);
						if(!is_dir('users/'.$user_id.'/thumbnails')){
							mkdir('users/'.$user_id.'/thumbnails',0777);
						}
						chmod('users/'.$user_id.'/thumbnails',0777);

						$filename 	= friendlyURL($f_name).'.jpg';
						$image 		= new SimpleImage();
						$image->load($_FILES["photo"]["tmp_name"]);
						$image->save('users/'.$user_id.'/'.$filename);
						chmod('users/'.$user_id.'/'.$filename,0777);
						
						$rs_photos = mysqli_query($conn,"select * from photos where entity_id = '$user_id' and entity_type = 'users' and is_default = '1'");
						if(mysqli_num_rows($rs_photos) > 0){
							mysqli_query($conn,"update photos set file_name = '$filename' where entity_id = '$user_id' and entity_type = 'users' and is_default = '1'");
						}else{
							mysqli_query($conn,"insert into photos (file_name, title, entity_id, entity_type, is_default) values ('$filename','$f_name','$user_id','users','1')");
						}		
						
						$image->load($_FILES["photo"]["tmp_name"]);
						$image->resizeToWidth('128');
						$image->save('users/'.$user_id.'/thumbnails/'.$filename);
						chmod('users/'.$user_id.'/thumbnails/'.$filename,0777);
						
					}
					*/

	                echo '<div id="success"><b>Success : </b>Information updated ... !</div><br><br>';
	                ?>
	                <script>
	                	window.location = '<?=WWW?>account-information.html';
					</script>
	                <?
				}
			}
			$row_photo = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_id = '".$row_user['id']."' and entity_type = 'users' and is_default = '1'"));
		?>

		<div class="white-box content" id="dashboard">
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
				<?php if(empty($row_user['country_id'])){ ?>
				<div id="error" style="height:60px; line-height:30px;">Please enter Birthday, Country of Residence and select appropriate Role and click 'Update' button below only then your registration will be complete and all Dashboard, Search and Advanced Search features will be available..!</div>
				<?php } ?>
				<form method="post" action="" enctype="multipart/form-data">
					<input type="hidden" name="action" value="update">
					<fieldset>
						<div class="form-box">
							<label>First Name </label>
							<input value="<?=$row_user['f_name']?>"  style="width:257px;"type="text" name="f_name" maxlength="30"class="input-login validate[required]">
						</div>
						<div class="form-box">
							<label>Last Name </label>
							<input value="<?=$row_user['last_name']?>" style="width:257px;"type="text" maxlength="30" name="last_name" class="input-login">
						</div>
						<div class="clear"></div>
						<div class="form-box">
							<label>Birthday </label>
							<?  $date = explode('-', $row_user['date_of_birth']);?>
							<select style="width:90px;" name="year" class="validate[required]">
								<option value="" selected="selected">Year</option>
								<? for($x = (date('Y')-100); $x < (date('Y')-12); $x++) { ?>
								<option <?php echo ($date[0] == $x)?'selected="selected"':'';?> value="<?php echo $x;?>"><?php echo $x;?></option>
								<? } ?>
							</select>
							<select style="width:90px;" name="month" class="validate[required]">
								<option value="" selected="selected">Month</option>
								<option <?=($date[1] == 1)?'selected="selected"':'';?> value="1">Jan</option>
								<option <?=($date[1] == 2)?'selected="selected"':'';?> value="2">Feb</option>
								<option <?=($date[1] == 3)?'selected="selected"':'';?> value="3">Mar</option>
								<option <?=($date[1] == 4)?'selected="selected"':'';?> value="4">Apr</option>
								<option <?=($date[1] == 5)?'selected="selected"':'';?> value="5">May</option>
								<option <?=($date[1] == 6)?'selected="selected"':'';?> value="6">Jun</option>
								<option <?=($date[1] == 7)?'selected="selected"':'';?> value="7">Jul</option>
								<option <?=($date[1] == 8)?'selected="selected"':'';?> value="8">Aug</option>
								<option <?=($date[1] == 9)?'selected="selected"':'';?> value="9">Sep</option>
								<option <?=($date[1] == 10)?'selected="selected"':'';?> value="10">Oct</option>
								<option <?=($date[1] == 11)?'selected="selected"':'';?> value="11">Nov</option>
								<option <?=($date[1] == 12)?'selected="selected"':'';?> value="12">Dec</option>
							</select>
							<select style="width:90px;" name="day" class="validate[required]">
								<option value="" selected="selected">Day</option>
								<? for($x = 1; $x <= 31; $x++) { ?>
								<option value="<?=$x?>" <?=($x == $date[2])?'selected="selected"':'';?>><?=$x?></option>
								<? } ?>
							</select>
						</div>
						<div class="form-box">
							<label><a class="popup-link" href="#why-do-we-need-your-birthday">Why do we need your birthday?</a></label>
						</div>
						<div class="clear"></div>
						<div class="form-box">
							<label>Gender </label>
							<div class="text"><?=get_gender_combo($row_user['gender'],'');?></div>
						</div>
						<div class="form-box">
							<label>Country of <br>Residence </label>
							<div class="text"><?=get_combo('countries','name', $row_user['country_id'],'country_id');?></div>
						</div>
						<div class="clear"></div>
						<div class="form-box">
							<label style="margin-top:8px;">Your Role </label>
							<select name="user_type_id" id="user_type_id" style="width: 277px;" class="validate[required]">
							<option value="">Select One</option>
							<? 	$rs_uts = mysqli_query($conn,"select * from user_types where id  != '1' and status = '1' order by name;");
								while($row_ut = mysqli_fetch_assoc($rs_uts)){
								?><option <?=($row_ut['id'] == $row_user['user_type_id'])?'selected="selected"':'';?> value="<?=$row_ut['id']?>"><?=$row_ut['name']?></option><?
								}
							?>
							</select>
							
						</div>
						<div class="form-box">
							<label style="margin-top:8px;">You can further define Your Role in your Pofile section</label>
						</div>
						<div class="clear"></div>
						<div class="form-box">
							<label>Email Address </label>
							<div class="text"><?=$row_user['email']?></div>
						</div>
						<div class="clear"></div>
						<div class="form-box">
							<label>Registered On</label>
							<div class="text"><?=date_converter($row_user['create_date'])?></div>
						</div>
						<div class="clear"></div>
						<div class="form-box">
							<label>Timezone </label>
							<?php echo get_combo('timezones', 'title', $row_user['timezone'],'timezone')?>
						</div>
						<div class="clear"></div>
						<? /*
						<div class="form-box">
							<label>My ID : </label>
							<div class="text">
								<?	$idc 	= '';
									if($row_user['is_company'] == '0'){
										$idc .= 'I';
									}else{
										$idc .= 'C';
									}
									$idc .= '-';
									$idc .= date('Y', strtotime($row_user['create_date']));
									$userid = strlen($row_user['id']);
									$user_id= $row_user['id'];
									for($x = $userid; $x < 9; $x++){
										$user_id = '0'.$user_id;
									}
									$user_id_t = trim($user_id);
									$output_id = '';
									for($a = -1; $a < strlen($user_id_t); $a++){
										if($a%3 === 0 && $a > -1){
											$output_id .= '-';
										}
										$output_id .= $user_id_t[$a];
									}
									$idc .= $output_id;
									echo $idc;
								?>
							</div>
						</div> */ ?>
						<div class="clear"></div>
						

						<div class="form-box hide">
							<label>YC Email : </label>
							<? if(empty($row_user['yc_email'])){ ?>
								<input type="text" name="yc_email" maxlength="255" class="input-login validate[required]" value="<?=strtolower(str_replace(' ','',$row_user['f_name'].$row_user['last_name']))?>">
							<? }else{ ?>
								<div class="text"><?=$row_user['yc_email']?>@youcricketer.com</div>
							<? } ?>
						</div>
<? /*
						<div class="form-box">
							<? if(empty($row_user['yc_email'])){ ?>
							<div class="left">@youcricketer.com</div>
							<? } ?>
							<div class="left" style="position:relative; top:-10px;" id="yc_email_status">
								<? if(empty($row_user['yc_email'])){ ?>
								<img src="<?=WWW?>images/sucess-icon.png">
								<? } ?>
							</div>
							<? if(empty($row_user['yc_email'])){ ?>
							<input type="hidden" name="yc_email_value">
							<label>Only Characters allowed</label>
							<? } ?>
						</div>   <?  */ ?> 
						<div class="clear"></div>
						<div class="form-box" style="<?=(empty($row_user['permalink']))?'':'width:100%;';?>">
							<label>YC Profile <br>Permalink : </label>
							<? if(empty($row_user['permalink'])){ ?>
								<input type="text" name="permalink" style="width: 253px;"value="<?=strtolower(str_replace(' ','',$row_user['f_name'].$row_user['last_name']))?>" maxlength="255"class="input-login validate[required]">
							<? }else{ ?>
								<div class="text" style="float:left; margin-left:50px;"><?=WWW?>yci/<?=$row_user['permalink']?></div>
							<? } ?>
						</div>
						<div class="form-box" >
							<div class="left" style="position:relative; top:-10px;" id="permalink_status">
								<? if(empty($row_user['permalink'])){ ?>
								<img src="<?=WWW?>images/sucess-icon.png">
								<? } ?>
							</div>
							<? if(empty($row_user['permalink'])){ ?>
							<input type="hidden" name="permalink_value">
							<label>Only Characters allowed</label>
							<? } ?>
						</div>
						<div class="clear"></div> 
						<?php /* ?><div class="form-box" style="width:30%;">
							<label>Newsletter Subscription : </label>
							<input type="checkbox" name="is_newsletter" <?php echo ($row_user['is_newsletter'] == '1')?'checked="checked"':'';?>>
						</div>
						<div class="clear"></div> <?php */ ?>
						<div class="form-box" style="width:100% !important;">
							<label>Referral Acceptance Counter : </label>
							<div class="text" style="text-align:left;">
							<? 	
							
							$query 		= "select * from invitations where from_user_id = '$user_id' and status = 'Invitation Accepted'";
								$rs_friends = mysqli_query($conn,$query);
								echo mysqli_num_rows($rs_friends);
							?>&nbsp;&nbsp;&nbsp;&nbsp;
							<a class="popup-link" style="font-weight:normal;" href="#what-is-this-counter-for">What is this Counter for?</a>
							</div>
						</div>
						<div class="clear"></div>
						<div class="form-box">
							<a href="<?=WWW?>dashboard.html" class="submit-login left">Back</a>
						</div>
						<div class="form-box">
							<input type="submit" value=" Update " class="submit-login">
						</div>
					</fieldset>
				</form>
				<hr>
				<div class="form-box" style="width:100%;">
					<label id="close-my-account-link" style="cursor:pointer;color:#fb9000;font-weight:bold;">I want to close my account</label>	
					<div class="clear"></div>
					<div id="close-my-account" class="hide" style="margin-top:30px;">
						<form method="post">
							<input type="hidden" name="action" value="close">
							<div class="form-box">
								<textarea name="close_comments" class="validate[required]" placeholder="Why you want to close account ?"></textarea>
							</div>
							<div class="form-box right">
								<input type="submit" value=" Close " class="submit-login" onclick="return confirm('Are You sure you want to close your Account. If yes then please click OK');">
								<?php
									$time = time();
									$_SESSION['time'] = $time;
									$salt = 'zxzxqb79jsMaEzXMvCO2iWtzU2gT7rBoRmQzlvj5yNVgP4aGOrZ524pT5KoTDJ7vNiINPOVD9Tzx';
									$token = sha1($salt . $time);
								?>
								<input type="hidden" name="token" value="<?php echo $token; ?>" />
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="clear"></div>	
		</div>
		<div class="clear"></div>
	</div>
<div class="popup hide" id="what-is-this-counter-for" style="width:400px; margin-top:-500px;">
	<h1 style="font-size: 14px;">What is this counter for ? 
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h1>
		<? $row = get_record_on_id('cms',14);?>
		<div>
			<?=$row['content']?>
		    <div class="clear"></div>
		</div>
</div>
<div class="popup hide" id="why-do-we-need-your-birthday" style="width:400px; margin-top: -650px;">
	<h1 style="font-size: 14px;">Why do we need your Birthday 
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h1>
		<? $row = get_record_on_id('cms',7);?>
		<div>
			<?=$row['content']?>
		    <div class="clear"></div>
		</div>
</div>
<script src="<?php echo WWW?>js/jquery-1.8.0.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=WWW?>js/jquery-ui.min.js"></script>
<link href="<?=WWW?>css/jquery-ui.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="<?=WWW?>/js/jquery.validationengine.js"></script>
<script type="text/javascript" src="<?=WWW?>js/validation-engine.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('form').validationEngine();
	$('.close-popup').click(function(){
		$('.popup').addClass('hide');
	});

	$('label#close-my-account-link').click(function(){
		var divId = document.getElementById('close-my-account');
		if(divId.style.display == 'none'
		|| divId.style.display == ''){
			$(divId).show();
		}else{
			$(divId).hide();
		}
	})

	$('input[name=yc_email]').keyup(function(){
		var value = this.value;
		if (value.search(/[^a-zA-Z]+/) === -1) {
			$.ajax({type	: 'POST', 
			   		url		: WWW + 'includes/get-confirmation.php?type=email', 
					data 	: ({email:value}),
					success	: function(msg){
						if(msg === 'not_exists'){
							$('#yc_email_status').html('<img src="'+WWW+'images/sucess-icon.png">');
							$('input[name=yc_email_value]').val('0');
						}else{
							$('#yc_email_status').html('<img src="'+WWW+'images/error-icon.png">');
							$('input[name=yc_email_value]').val('1');
						}
					}
	   		});
	   	}else{
	   		$('#yc_email_status').html('<img src="'+WWW+'images/error-icon.png">');
	   		$('input[name=yc_email_value]').val('1');
	   	}
	});

	$('input[name=permalink]').keyup(function(){
		var value = this.value;
		if (value.search(/[^a-zA-Z]+/) === -1) {
			$.ajax({type	: 'POST', 
			   		url		: WWW + 'includes/get-confirmation.php?type=permalink', 
					data 	: ({email:value}),
					success	: function(msg){
						if(msg === 'not_exists'){
							$('#permalink_status').html('<img src="'+WWW+'images/sucess-icon.png">');
							$('input[name=permalink_value]').val('0');
						}else{
							$('#permalink_status').html('<img src="'+WWW+'images/error-icon.png">');
							$('input[name=permalink_value]').val('1');
						}
					}
	   		});
	   	}else{
	   		$('#permalink_status').html('<img src="'+WWW+'images/error-icon.png">');
	   		$('input[name=permalink_value]').val('1');
	   	}
	});

	$('select[name=user_type_id]').live('change',function(){
		var id = $('select[name=user_type_id]').val();
		if(id != '2'){
			$('#dependent-player').addClass("hide");
		}else{
			$('#dependent-player').removeClass("hide");
		}
	});

	$('a.popup-link').click(function(){
		var link = $(this).attr('href');
		if(link == '#edit-activity'){
			var id = this.id;
			$.ajax({type	: 'POST', 
				url		: WWW + 'includes/get-record.php', 
				data	: ({table:'activity',field:'activity',id:id}),
				success	: function(msg){
					$(link+' textarea').val(msg);
					$(link+' #recordId').val(id);
				}
			});
		}
		$(link).removeClass('hide');
	});

	$('#dashboard .black-box h2').click(function(){
		$('#dashboard .black-box ul').css('display','none');
		var id = this.id;
		$('#'+id+'-box ul').css('display','block');
	});
	
	$('select[name=day], select[name=month], select[name=year]').change(function(){
		var day 	= $('select[name=day]').val();
		var month 	= $('select[name=month]').val();
		var year 	= $('select[name=year]').val();

		if(day 	!= ''
		&& month!= ''
		&& year != ''){
			$.ajax({type: 'POST', 
				url		: WWW + 'includes/check-date.php', 
				data	: ({day:day, month:month, year:year}),
				success	: function(msg){
					if(msg == ''){
						alert("Invalid date selected ... !");
					}
				}
			});
		}
	});
});
<?php if(empty($row_user['timezone']) || $row_user['timezone'] == '0'){ ?>
	var timezone = jstz.determine();
	$.ajax({type	: 'POST', 
			url		: WWW + 'includes/get-record.php', 
			data	: ({table:'timezones',field:'id',id:timezone.name()}),
			success	: function(msg){
				$('select#timezone').val(msg);	
			}
	});
<?php } ?>
</script>
<?php include('common/footer.php'); ?>