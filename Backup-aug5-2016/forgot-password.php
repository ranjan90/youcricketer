<?php include('common/header.php'); ?>
	<div class="middle">
		<h1>Forgot Password </h1>

		<? 	if(isset($_POST) && !empty($_POST)){
				$username 	= $_POST['email'];
					
				$rs_user	= mysqli_query($conn,"select * from users where email = '$username'");
				
				if(mysqli_num_rows($rs_user) == 0){
					echo '<div id="error">Invalid email address ... !</div><br><br>';	
				}else{
					$row_u = mysqli_fetch_assoc($rs_user);
					$password = getRandomWord(8);

					$email_template = get_record_on_id('cms', 2);
					$mail_title		= $email_template['title'];
	                $mail_content	= $email_template['content'];
	                $mail_content 	= str_replace('{name}',$row_u['f_name'], $mail_content);
	                $mail_content 	= str_replace('{email}',$username, $mail_content);
	                $mail_content 	= str_replace('{password}',$password, $mail_content);
	                $mail_content 	= str_replace('{login_link}','<a href="'.WWW.'login.html" title="Login Here">HERE</a>', $mail_content);
	                $mail_content 	= str_replace('{contact_link}','<a href="'.WWW.'contact-us.html" title="Contact Us">HERE</a>', $mail_content);
	                $mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);

					$headers = "Content-type:text/html;charset=UTF-8" . "\r\n";
					$headers .= "From: no-reply@youcricketer.com" . "\r\n";


	                $query = "update users set password = '".md5($password)."' where email = '$username'";
	                mysqli_query($conn,$query);
	                mail($username,$mail_title,$mail_content,$headers);
	                echo '<div id="success"><b>Success : </b>Password Reset email has been sent. Please check your email and follow the instructions in it. If u do not see the email in your inbox check your Spam/Junk folder and mark it safe.. !</div><br><br>';
				}
			}
		?>

		<div class="white-box content">
			<form method="post" action="">
				<fieldset>
					<h2>Enter Information</h2>
					<div class="form-box">
						<label>Email Address </label>
						<input type="text" name="email" class="input-login validate[required, custom[email]]">
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<?
                        //checking google recaptcha
                        include('includes/captcha.php');
                        ?>
                        <script type="text/javascript" src="https://www.google.com/recaptcha/api/challenge?k=<?=PUBLIC_KEY?>"</script>
                        <noscript>
                        <iframe src="https://www.google.com/recaptcha/api/noscript?k=<?=PUBLIC_KEY?>" frameborder="0"></iframe><br>
                        </noscript>
                        <?
                        include('recaptchalib.php');
                        $publickey    = PUBLIC_KEY;
                        $privatekey   = PRIVATE_KEY;
                        $resp         = null;
                        $error        = null;
                        echo $publickey;

                        if ($_POST["recaptcha_response_field"]) {
                          $resp = recaptcha_check_answer ($privatekey,
                                                          $_SERVER["REMOTE_ADDR"],
                                                          $_POST["recaptcha_challenge_field"],
                                                          $_POST["recaptcha_response_field"]);

                          if ($resp->is_valid) {
                                  echo "You got it!";
                          } else {
                                  # set the error code so that we can display it
                                  $error = $resp->error;
                          }
                        }
                        echo recaptcha_get_html($publickey, $error);
                        //checking google recaptcha
                        ?>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<input type="submit" value=" Submit " class="submit-login">
					</div>
				</fieldset>
			</form>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>
<script>

$(window).load(function(){
	$('form').validationEngine();
	$('#recaptcha_response_field').addClass('validate[required]');
});
                        </script>

<?php include('common/footer.php'); ?>