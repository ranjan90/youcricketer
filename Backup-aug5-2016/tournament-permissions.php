<?php include_once('includes/configuration.php');
$page = 'tournament-permissions.html';
$selected_country = getGeoLocationCountry(); 

$ppage = intval($_GET["page"]);
if($ppage<=0) $ppage = 1;

$tournament_id = trim($_GET['tour_id']);
$tournament_info = get_record_on_id('tournaments', $tournament_id);	

$league_info = array();
$user_info  = array();
$permission_users = array();

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
	
	$sql = 'select * from companies where user_id = '.$tournament_info['user_id'];
	$rs_league = mysqli_query($conn,$sql);
	$league_info = mysqli_fetch_assoc($rs_league);
}

$sql = "select * from tournament_permissions where tournament_id = $tournament_id ";
$rs_permissions = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_permissions)){
	$permission_users[] = $row['member_id'];
}

if(isset($_POST['permissions_add_submit']) && !empty($_POST['permissions_add_submit'])){
	$members = $_POST['member_ids'];
	if(!empty($members)){
		for($i=0;$i<count($members);$i++){
			$sql = "INSERT into tournament_permissions SET tournament_id=".$tournament_id.", member_id=".$members[$i];
			mysqli_query($conn,$sql);
		}
	}
	
	$_SESSION['permission_added'] = 1;
	
	if(isset($_GET['member_name_srch']) && !empty($_GET['member_name_srch']) ){
		$url_q.='-'.$_GET['member_name_srch'];
	}else{
		$url_q.='-all';
	}
	
	if(isset($_GET['member_type_srch']) && !empty($_GET['member_type_srch'])){
		$url_q.='-'.$_GET['member_type_srch'];
	}else{
		$url_q.='-all';
	}
	
	header("Location:".WWW."tournament/tournament-permissions-$tournament_id{$url_q}-".$ppage.'.html');
	exit;
}

if(isset($_POST['permissions_revoke_submit']) && !empty($_POST['permissions_revoke_submit'])){
	$members = $_POST['member_ids'];
	if(!empty($members)){
		for($i=0;$i<count($members);$i++){
			$sql = "DELETE FROM tournament_permissions WHERE tournament_id=".$tournament_id." AND member_id=".$members[$i]." LIMIT 1";
			mysqli_query($conn,$sql);
		}
	}
	
	$_SESSION['permission_revoked'] = 1;
	
	if(isset($_GET['member_name_srch']) && !empty($_GET['member_name_srch']) ){
		$url_q.='-'.$_GET['member_name_srch'];
	}else{
		$url_q.='-all';
	}
	
	if(isset($_GET['member_type_srch']) && !empty($_GET['member_type_srch'])){
		$url_q.='-'.$_GET['member_type_srch'];
	}else{
		$url_q.='-all';
	}
	
	header("Location:".WWW."tournament/tournament-permissions-$tournament_id{$url_q}-".$ppage.'.html');
	exit;
}

if(isset($_SESSION['permission_added']) && $_SESSION['permission_added']==1) {
	$permission_added = 1;
	unset($_SESSION['permission_added']);
}

if(isset($_SESSION['permission_revoked']) && $_SESSION['permission_revoked']==1) {
	$permission_revoked = 1;
	unset($_SESSION['permission_revoked']);
}

$page_title = 'Tournament Umpires, Referees & Scorers - '.$tournament_info['title'];
?>
<?php include('common/header.php'); ?>
<style>
.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
dl {min-height:185px;}
.content {width:1010px;}
.large-column {width:790px;}
.details h3{height:30px;}
</style>
	<div class="middle">
		<h1> Tournament Umpires/Referees/Scorers Permission</h1>
		<?php if(isset($permission_added) && $permission_added == 1): ?>
			<div id="information">Permission added Successfully... !</div>
		<?php  endif; ?>
		
		<?php if(isset($permission_revoked) && $permission_revoked == 1): ?>
			<div id="information">Permission Revoked Successfully... !</div>
		<?php  endif; ?>
		
		<?php if(empty($user_info)): ?>
			<div id="error">You are not logged... !</div>
		<?php endif; ?>
	
		<div class="white-box content" id="dashboard">
			<div class="small-column">
				<? include('common/user-left-panel.php'); ?>
			</div>
			<div class="large-column">
			<div id="pagination-top"></div>
			
			<div id="adv_search">
			<div class="clear"><br></div>
			<h2> Umpires, Referees & Scorers Search </h2>
			<form id="advance_search_form" method="post" action=""> 
			
			<div id="member_name_div" style="float:left;margin-left:10px;">
				<input type="text" name="member_name_srch" id="member_name_srch" <?php if(isset($_GET['member_name_srch']) && !empty($_GET['member_name_srch']) && $_GET['member_name_srch'] != 'all'): ?> value="<?php echo $_GET['member_name_srch']; ?>" <?php else: ?>  placeholder="Member Name" <?php endif; ?> class="search_box">
			</div>
				
			<input type="hidden" name="club_id" id="club_id"  value="<?php if(isset($_GET['club_id']) && !empty($_GET['club_id'])) echo $_GET['club_id']; ?>" >
			&nbsp;
			
			<input type="submit" name="submit_adv_search" id="submit_adv_search" value="Search" class="submit-login margin-top-5" style="float:left;margin-left:10px;">
			
			</form>
			
			<div style="clear:both"></div>

			
			<div class="clear"><br></div>
			</div>
			
			<?php if($tournament_info['user_id'] == $user_id){ ?>
			
			<form method="post">
			<div class="list">
                <ul id="individual" class="content1">
                	<?php  
						
                		$rpp = PRODUCT_LIMIT_FRONT; // results per page
						
                		$country_id = 0;
						
      					$query = "select distinct u.*,year_certified,umpire_phone from users u inner join league_umpires as l on u.id=l.umpire_id ";
						$query_count = "select count(*) as users_count from users u inner join league_umpires as l on u.id=l.umpire_id";
				      	//=======================================
						$where = " where l.league_id = ".$league_info['id']."  and  u.status = 1";
						
						
							$member_type_srch = 'all';
						
						
					    if(isset($_GET['member_name_srch']) && $_GET['member_name_srch'] != 'Member Name' && $_GET['member_name_srch'] != 'all' ){
							$keywords = trim($_GET['member_name_srch']);
							$where  .= " and (u.f_name like '%{$keywords}%' OR u.m_name like '%{$keywords}%' OR u.last_name like '%{$keywords}%') ";
						}else{
							$keywords = 'all';
						}
						
						$query.=$where;
						$query_count.=$where;
					    $query .= " order by u.id desc ";
				    
						$rs_count   = mysqli_query($conn,$query_count);
						$row_count  = mysqli_fetch_assoc($rs_count);
						$tcount = $row_count['users_count'];
					  
						$tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
						$count = 0;
						$start = ($ppage-1)* $rpp;
						$x = 0;
					  
						$query .= " LIMIT $start,$rpp ";
						
						$rs   = mysqli_query($conn,$query);
						if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
							echo '<div id="information">No record found ... !</div>';
						}
						///echo $query;
						$i= 0 ;
						while($row 	= mysqli_fetch_assoc($rs)){
							$row_country= get_record_on_id('countries', $row['country_id']);	
			  				$location 	= $row_country['name'];
			  				$row_img = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row['id']."' and is_default = '1' "));
							$row_vid = mysqli_fetch_assoc(mysqli_query($conn,"select * from videos where entity_id = '".$row['id']."' and entity_type = 'users' and is_default = '1' and file_name != ''"));
							
		        		    ?>
		        		    
						<dl >
								<dt>
									
									<a href="<?=WWW?>individual-detail-<?=$row['id']?>-<?=friendlyURL($row['f_name'].' '.$row['m_name'].' '.$row['last_name'])?>.html#activity-tab" title="<?=$row['f_name'].' '.$row['m_name'].' '.$row['last_name']?>"><img src="<?php echo WWW;?><?=($row_img && !empty($row_img['file_name']))?'users/'.$row['id'].'/photos/'.$row_img['file_name']:'images/no-photo.jpg'?>" width="120" height="128" /></a></dt>
								<dd><div class="details">
								<?php if(!empty($row['company_name'])){ ?>
									<h3><?php echo $row['company_name']; ?></h3>
								<?php  }else{ ?>
									<h3><?=truncate_string($row['f_name'],20).' '.truncate_string($row['last_name'], 20)?></h3>
								<?php } ?>	
									<? echo $location;?><br />
									
									
<?if($row['user_type_id'] != 1)

{
    echo get_combo('user_types','name',$row['user_type_id'],'','text');  

?><br />
		 <?   if($row['user_type_id'] == 2){ echo $row['type'].'<br/>';} }?>
									
										<?php if(!empty($row['year_certified']) && $row['year_certified'] != '0000-00-00'): ?>
											Date Certified: <?php echo date('d F, Y',strtotime($row['year_certified'])) ; ?><br/>
										<?php endif; ?>
										
										<?php if(!empty($row['umpire_phone'])): ?>
											Phone No: <?php echo $row['umpire_phone'] ; ?><br/>
										<?php endif; ?>
										
										<div style="float:left;clear:both;margin-top:8px;">
										<?php if($tournament_info['user_id'] == $row['id']) { ?>
											<br/>Tournament Owner
										<?php } elseif(!in_array($row['id'],$permission_users)){ ?>
											<input type="checkbox" name="member_ids[]" id="member_id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>"> Add Permission
										<?php } else{ ?>
											Already have Permission<br/>
											<input type="checkbox" name="member_ids[]" id="member_id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>"><span style="color:#ff0000;"> Revoke Permission</span>
										<?php } ?>		 	
										
										</div>
									 
								
									
									<?php if(!empty($row['company_name'])){ ?>
										<a class="submit-login margin-top-5" href="<?=WWW?>cricket-club/<?php echo getSlug($row_country['name']).'-'.getSlug($row['company_name']); ?>/<?php echo $row['company_id']; ?>" title="<?=$row['company_name']?>">View Profile</a>
									<?php }else{ ?>	
										<a class="submit-login margin-top-5" href="<?=WWW?>individual-detail-<?=$row['id']?>-<?=friendlyURL($row['f_name'].' '.$row['m_name'].' '.$row['last_name'])?>.html#activity-tab" title="<?=$row['f_name'].' '.$row['m_name'].' '.$row['last_name'].' - '.$site_title?>">View Profile</a>
									<?php } ?>		 	
									</div>
									<div class="flag"><img title="<?=$row_country['name']?>" alt="<?=$row_country['name']?>" src="<?php echo WWW;?>countries/<?=$row_country['flag']?>"  width="25"/></div>
									<div class="video">
										<? 
										$video = $row_vid['file_name'];
											if(!empty($video)){
											if(preg_match('/<iframe(.*)<\/iframe>/', $row_vid['file_name'])){ 
														
														preg_match('/src="(.*?)"/',$row_vid['file_name'] , $src);
														$src = $src[1];
														$video = "<iframe width='205' height='130' frameborder='0' allowfullscreen src='$src'></iframe>";
														
														
											}else{	
												$filename = explode('.',$video);
												$filename1= $filename[0];
												$video = '<video width="220" height="130" controls>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mp4" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.ogg" type=\'video/ogg; codecs="theora, vorbis"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mov" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.webm" type=\'video/webm; codecs="vp8, vorbis"\'>
												</video>';
											}
										}else{
											$video = '<img src="'.WWW.'images/no-video.jpg" width="168" height="130">';
										}
										echo $video;?>									
									</div>
								</dd>
						</dl>
						<?php
                		
					      $i++;
					      $count++;
					      $x++;
					  } 
					  ?>
			</ul>
			</div><br/>
			<div style="float: left; clear: both; padding: 10px;">
				<input type="submit" class="submit-signup" style="height:35px;cursor:pointer;" name="permissions_revoke_submit" id="permissions_revoke_submit" value="Revoke Permission" > 
				<input type="submit" class="submit-signup" style="height:35px;cursor:pointer;" name="permissions_add_submit" id="permissions_add_submit" value="Add Permission" > &nbsp;&nbsp;
			</div>
			</form>
			
			<?php }else{ ?>
			<div id="error">You are not owner of this tournament... !</div>
			<?php } ?>
			
			<p>&nbsp;</p><p>&nbsp;</p>
			<div id="pagination-bottom">
				<? if($tcount != 0 && $tcount > PRODUCT_LIMIT_FRONT){ ?>
		      	<?php
		        	$q_url = '';
					if(isset($keywords) && !empty($keywords)){
						$q_url.="-{$keywords}";
					}else{
						$q_url.="-all";
					}
					if(isset($member_type_srch) ){
						$q_url.="-{$member_type_srch}";
					}
					
					
					if(!empty($q_url)){
						$reload = "tournament-permissions-{$tournament_id}{$q_url}.html?";
					}else{
						$reload = "tournament-permissions-{$tournament_id}.html?";
					}
		        	echo paginate_one($reload, $ppage, $tpages);
		      	?>
		      	<input type="hidden" name="pagination-page" value="tournament-permissions.html">
		        <? } ?>    
		        <form id="list-search" method="post" action="">
		        	<input type="text" name="txtsearch" placeholder="Search Here" class="validate[required] input-login">
		        	<input type="submit" value="">
		        </form>
				<div class="clear"></div>
		        <script type="text/javascript">
		        $('form#list-search').submit(function(e){
					var parentId = $(this).parent().attr('id');
					var string = $('#'+parentId +' form input[name=txtsearch]').val();
					if(string != '' && string != 'Search Here'){
						string = string.replace(/[^a-zA-Z0-9]+/g,'').toLowerCase();
						if(string.length > 0){
							$('form#list-search').attr('action','<?=WWW;?>tournament/tournament-permissions-<?php echo $tournament_id ?>-' + string + '.html');
						}
					}
				});
				
				$('#submit_adv_search').click(function(e){
				
					var string = $('#member_name_srch').val();
					if(string == ''){
						string = 'all';
					}
					if($("#search_box").val()!=''){
						var club_id = $('#club_id').val();
					}else{
						var club_id = 0;
					}
					var member_type_srch = 'all';
					//if(string != '' && string != 'Member Name'){
						string = string.replace(/[^a-zA-Z0-9]+/g,'').toLowerCase();
						//if(string.length > 0){
							$('form#advance_search_form').attr('action','<?=WWW;?>tournament/tournament-permissions-<?php echo $tournament_id ?>-' + string +"-"+member_type_srch+ '.html');
						//}
					//}
				});
		        </script>
				
				
		        <div class="clear"></div>
		    </div>  
		    <div class="clear"></div>
		</div>
		</div>
		
		<div class="clear"></div>
	</div>
<style>
.header_search{background-color:none;padding:20 0 5 200px;width:200px;float:left;}
.searchres{margin-left:2px;width:200px;border: #f0f0f0 1px solid;border-radius:5px;position:absolute;z-index:100;background-color:#fff;}.img{float:left;margin: 5 5 5 5px;float:left;}
.search_box{width: 190px;height: 15px;border-radius: 5px;padding: 10px;font-family: verdana;color:black; font-size: 15px;border:1px solid #ccc;}
.name{margin-top: 14px;}

</style>
<?php include('common/footer.php'); ?>