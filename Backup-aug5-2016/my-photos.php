<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	<div class="middle">
		<h1>My Photos - <?=$_SESSION['ycdc_user_name']?> </h1>
		<div class="white-box content" id="dashboard">
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
				<div id="upload-area">
					<? 	if(isset($_FILES) && !empty($_FILES)){
							$total = count($_FILES['photos']['name']);
							$user_id=$_SESSION['ycdc_dbuid'];

							if(!is_dir('users/'.$user_id)){
								mkdir('users/'.$user_id,0777);
							}
							chmod('users/'.$user_id,0777);
							if(!is_dir('users/'.$user_id.'/photos')){
								mkdir('users/'.$user_id.'/photos',0777);
							}
							chmod('users/'.$user_id.'/photos',0777);

							for($x = 0; $x < $total; $x++){
								if(!empty($_FILES['photos']['name'][$x])){
									$filename 	= friendlyURL($_FILES['photos']['name'][$x]).'.jpg';
									$rs_chk 	= mysqli_query($conn,"select * from photos where entity_id = '$user_id' and entity_type = 'users' and file_name = '$fileanme'");
									if(mysqli_num_rows($rs_chk) == 0){
										$image 		= new SimpleImage();
										$image->load($_FILES["photos"]["tmp_name"][$x]);
										if($image->getWidth() > 800){
											$image->resizeToWidth(800);
										}
										$image->save('users/'.$user_id.'/photos/'.$filename);
										chmod('users/'.$user_id.'/photos/'.$filename,0777);
									
										mysqli_query($conn,"insert into photos (file_name, entity_id, entity_type) 
											values ('$filename','$user_id','users')");
									}									
								}
							}
							echo '<div id="success">Photos uploaded ... !</div>';
						}
						$user_id=$_SESSION['ycdc_dbuid'];
					?>
					<form method="post" enctype="multipart/form-data">
						<input type="file" name="photos[]" multiple="multiple" accept="image/*">
						<input type="submit" value="Upload" class="submit-login">
					</form>
				</div>
				<div id="gallery" class="content1">
				<? 	$rs_imgs = mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '$user_id' and is_default <> '1'");
					while($row_img = mysqli_fetch_assoc($rs_imgs)){ ?>
					<div style="height:175px;width:150px;float:left; margin:2px;">
						<a class="photo" href="<?=WWW?>users/<?=$user_id?>/photos/<?=$row_img['file_name']?>">	
							<img style=" border-radius:4px;" src="<?=WWW?>users/<?=$user_id?>/photos/<?=$row_img['file_name']?>" alt="<?=$_SESSION['ycdc_user_name']?>" width="150" height="150"/>
						</a>
						<br>
						<center>
						<a href="<?=WWW?>delete-photo-<?=$row_img['id']?>.html" title="Delete Photo">
							<img src="<?=WWW?>images/icons/delete.png">
						</a>
						</center>
					</div>
				<?  } ?>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
<script type="text/javascript" src="<?=WWW?>js/jquery-1.8.0.js"></script>
<script src="<?=WWW?>js/jquery.lightbox-0.5.js" type="text/javascript"></script>
<script src="<?=WWW?>js/jquery.lightbox-0.5.min.js" type="text/javascript"></script>
<script src="<?=WWW?>js/jquery.lightbox-0.5.pack.js" type="text/javascript"></script>
<link href="<?=WWW?>css/jquery.lightbox-0.5.css" type="text/css" rel="stylesheet">
<script>
$(document).ready(function(){
	$('#gallery a.photo').lightBox();

	$('input').change(function(){
		var val = this.value;
		var filesize = Math.round((this.files[0].size)/1024,2);
		if(filesize > 2000){
			$(this).val('');
            alert("Image is too large ... !");
		}
	});
	$('#dashboard .black-box h2').click(function(){
		$('#dashboard .black-box ul').css('display','none');
		var id = this.id;
		$('#'+id+'-box ul').css('display','block');
	});
	
	$('input[name=photo]').change(function(){
		var val = this.value;
		var filesize = Math.round((this.files[0].size)/1024,2);
		if(filesize > 2000){
			$(this).val('');
            alert("Image is too large ... !");
		}
	});
});	
</script>
<?php include('common/footer.php'); ?>