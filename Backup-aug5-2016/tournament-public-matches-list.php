<?php include('common/header.php');
$selected_country = getGeoLocationCountry(); 

$tournament_id = trim($_GET['t_id']);
$tournament_info = get_record_on_id('tournaments', $tournament_id);	

if(isset($_GET['page']) && !empty($_GET['page'])){
	$page = str_replace('page','',trim($_GET['page']));
}else{
	$page = 1;
}

$records_per_page = 10;
$start_record  = ($page-1)*$records_per_page;
?>
<style>
.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
</style>
	<div class="middle">
		<h1> Tournament Matches </h1>
		<h2>&nbsp;<?php echo ucwords($tournament_info['title']); ?></h2>
		<div class="white-box content">
			<div id="pagination-top"></div>
			<div class="list">
                <ul id="individual" class="content1">
                	<?php  $user_id = $_SESSION['ycdc_dbuid'];
					$sql = "SELECT tm.*,c1.company_name as team1,c2.company_name as team2 FROM tournament_matches as tm inner join companies as c1 on tm.batting_team1=c1.id  inner join companies as c2 on tm.batting_team2=c2.id
					WHERE tm.tournament_id = $tournament_id and tm.status = 1 ORDER BY tm.id DESC LIMIT $start_record,$records_per_page";
					$rs_matches = mysqli_query($conn,$sql);
					
				
					$sql = "SELECT count(*) as record_count FROM tournament_matches as tm inner join companies as c1 on tm.batting_team1=c1.id  inner join companies as c2 on tm.batting_team2=c2.id
					WHERE tm.tournament_id = $tournament_id and tm.status = 1 ORDER BY tm.id DESC";
                	$rs_total = mysqli_query($conn,$sql);
					$row_total = mysqli_fetch_assoc($rs_total);
					$records_count = $row_total['record_count'];
					
					while($row = mysqli_fetch_assoc($rs_matches)){
		        		    ?>
		        		    
						<dl>
									<dt>
									<a href="javascript:;" title="<?php echo ucwords($row['team1']); ?> vs <?php echo ucwords($row['team2']); ?>"><img src="<?php echo WWW;?><?php echo 'images/no-photo.jpg'; ?>" width="120" height="128" /></a>
									</dt>
								<dd><div class="details">
									<h3><?php echo ucwords($row['team1']); ?> vs <?php echo ucwords($row['team2']); ?> - <?php echo date('d F, Y',strtotime($row['start_time']));?></h3>
									
									<p style="margin-top:0px;"> <b>Scores:</b> <?php echo ucwords($row['team1']); ?> <?php echo $row['team1_score']; ?> runs (<?php if($row['team1_wickets'] == 10) echo 'all out';else echo $row['team1_wickets'].' wickets'; ?>) in <?php echo $row['team1_overs']; ?> overs 
									<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<?php echo ucwords($row['team2']); ?> <?php echo $row['team2_score']; ?> runs (<?php if($row['team2_wickets'] == 10) echo 'all out';else echo $row['team2_wickets'].' wickets'; ?>) in <?php echo $row['team2_overs']; ?> overs 
									</p>
									<p><b>Result:</b><?php echo $row['match_result_text'];?><br /></p>
									
									<a class="submit-login margin-top-5" href="<?=WWW?>tournament/matches/scoresheet/<?php echo $row['id'] ?>" >Scoresheet</a></div>
									<?php /* ?><div class="flag"><img title="<?=$row_country['name']?>" alt="<?=$row_country['name']?>" src="<?php echo WWW;?>countries/<?=$row_country['flag']?>"  width="25"/></div> <?php */ ?>
									<div class="video">
										<?php 
										$video = '<img src="'.WWW.'images/no-video.jpg" width="168" height="130">';
										echo $video;?>									
									</div>
								</dd>
						</dl>
						<?php
					      $i++;
					      $count++;
					      $x++;
					  } 
					  ?>
					  <?php if(mysqli_num_rows($rs_matches)==0): ?>
						<dl><center><b>No Records</b></center></dl>
					  <?php endif; ?>
			</ul>
			</div>
			<div id="pagination-bottom">
				<?php  $paging_str = getPaging('tournament-matches/'.$tournament_id,$records_count,$page,$records_per_page);
				echo $paging_str;
				?>
		        <div class="clear"></div>
		    </div>  
		    <div class="clear"></div>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>

<?php include('common/footer.php'); ?>