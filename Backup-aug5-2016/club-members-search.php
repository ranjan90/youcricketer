<?php include_once('includes/configuration.php');
$page = 'club-members-search.html';
$selected_country = getGeoLocationCountry(); 

$countries = array();
$rs_countries = mysqli_query($conn,"select id,name from countries where status='1'");
while($row = mysqli_fetch_assoc($rs_countries)){
	$countries[] = $row;
}

$clubs = array();
$rs_clubs = mysqli_query($conn,"select id,title from clubs where status='1' order by title");
while($row = mysqli_fetch_assoc($rs_clubs)){
	$clubs[] = $row;
}

$ppage = intval($_GET["page"]);
if($ppage<=0) $ppage = 1;

$club_info = array();
$user_info  = array();
$club_members = array();

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
	if($user_info['is_company'] == 1){
		$sql = "select * from companies where user_id = $user_id and company_type_id = 4 and status=1";
		$rs_company = mysqli_query($conn,$sql);
		if(mysqli_num_rows($rs_company)){
			$club_info = mysqli_fetch_assoc($rs_company);
			$sql = 'select * from club_members where club_id = '.$club_info['id'];
			$rs_club = mysqli_query($conn,$sql);
			while($row = mysqli_fetch_assoc($rs_club)){
				$club_members[] = $row['member_id'];
			}
		}
	}
}



if(isset($_POST['members_add_submit']) && !empty($_POST['members_add_submit'])){
	$members = $_POST['member_ids'];
	if(!empty($members)){
		for($i=0;$i<count($members);$i++){
			$sql = "INSERT into club_members SET club_id=".$club_info['id'].", member_id=".$members[$i].",date_added = now()";
			mysqli_query($conn,$sql);
		}
		if(!mysqli_error($conn)){
			$sql = "UPDATE companies SET tournament_data_added = 'yes' WHERE id = ".$club_info['id'];
			mysqli_query($conn,$sql);
		}
	}
	
	$_SESSION['club_member_added'] = 1;
	
	if(isset($_GET['keywords']) && !empty($_GET['keywords'])){
		$url_q.='-'.$_GET['keywords'];
	}else{
		$url_q.='-all';
	}
	if(isset($_GET['club_id']) && !empty($_GET['club_id']) ){
		$url_q.='-'.$_GET['club_id'];
	}else{
		$url_q.='-0';
	}
	if(isset($_GET['country_id']) && !empty($_GET['country_id']) ){
		$url_q.='-'.$_GET['country_id'];
	}else{
		$url_q.='-0';
	}
	
	header("Location:".WWW."club-members-search{$url_q}-".$ppage.'.html');
	exit;
	//echo '<script>window.location.href="'.WWW.'club-members-search-all-'.$ppage.'.html"</script>';
}

if(isset($_SESSION['club_member_added']) && $_SESSION['club_member_added']==1) {
	$member_added = 1;
	unset($_SESSION['club_member_added']);
}

if(isset($_GET['club_id']) && !empty($_GET['club_id'])){
	
	$query = "select title from clubs where id =".$_GET['club_id'];	
	$rs   = mysqli_query($conn,$query);
	if(mysqli_num_rows($rs)){
		$club_arr = mysqli_fetch_assoc($rs);
		$club_name = $club_arr['title'];
	}
							
}
?>
<?php include('common/header.php'); ?>
<style>
.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
dl {min-height:145px;}
.content {width:1010px;}
.large-column {width:790px;}
</style>
	<div class="middle">
		<h1> Add Club Members </h1>
		<?php if(isset($member_added) && $member_added == 1): ?>
			<div id="information">Members added Successfully... !</div>
		<?php  endif; ?>
		
		<?php if(empty($user_info)): ?>
			<div id="error">You are not logged... !</div>
		<?php endif; ?>
		<?php if(!empty($user_info) && empty($club_info) ): ?>
			<div id="error">You are not logged as Club Owner.. !</div>
		<?php endif; ?>
		<div class="white-box content" id="dashboard">
			<div class="small-column">
				<? include('common/user-left-panel.php'); ?>
			</div>
			<div class="large-column">
			<div id="pagination-top"></div>
			
			<div id="adv_search">
			<div class="clear"><br></div>
			<h2> Advanced Search </h2>
			<form id="advance_search_form" method="post" action=""> 
			
			<div id="auto_suggest" style="float:left;">
				<div class="header_search"><input type="text" class="search_box" id="search_box" name="club_name" <?php if(isset($club_name) && !empty($club_name)){?> value="<?php echo $club_name; ?>" <?php } else{ ?> placeholder="Club Name" <?php } ?> autocomplete="off"></div>
				<div style="clear:both"></div>
				<div id="searchres" class="searchres"></div>
			</div>	
			
			<div id="country_dropdopwn" style="float:left;">
				<select name="country_id_srch" id="country_id_srch" style="font-size:14px;width:250px;">
					<option value="0">Country</option>
					<?php for($i=0;$i<count($countries);$i++): ?>
						<?php if(isset($_GET['country_id']) && $_GET['country_id'] == $countries[$i]['id']) $sel = 'selected';else $sel = ''; ?>
						<option <?php echo $sel; ?> value="<?php echo $countries[$i]['id']; ?>"><?php echo $countries[$i]['name']; ?></option>
					<?php endfor; ?>
				</select>	
			</div>	
			
			<div id="member_name_div" style="float:left;margin-left:10px;">
			<input type="text" name="member_name_srch" id="member_name_srch" <?php if(isset($_GET['keywords']) && !empty($_GET['keywords'])): ?> value="<?php echo $_GET['keywords']; ?>" <?php else: ?>  placeholder="Member Name" <?php endif; ?> class="search_box">
			</div>	
			<input type="hidden" name="club_id" id="club_id"  value="<?php if(isset($_GET['club_id']) && !empty($_GET['club_id'])) echo $_GET['club_id']; ?>" >
			<div style="clear:both"><br></div>
			
			<input type="submit" name="submit_adv_search" id="submit_adv_search" value="Search">
			
			</form>
			
			<div style="clear:both"></div>
			<?php /* ?>
			<form id="advance_search_frm" method="POST" action=""> 
				
				<input type="text" name="member_name_srch" id="member_name_srch" <?php if(isset($_GET['keywords']) && !empty($_GET['keywords'])): ?> value="<?php echo $_GET['keywords']; ?>" <?php else: ?>  placeholder="Member Name" <?php endif; ?> style="height:30px;">
				<select name="country_id_srch" id="country_id_srch" style="font-size:14px;">
					<option value="0">Country</option>
					<?php for($i=0;$i<count($countries);$i++): ?>
						<?php if(isset($_GET['country_id']) && $_GET['country_id'] == $countries[$i]['id']) $sel = 'selected';else $sel = ''; ?>
						<option <?php echo $sel; ?> value="<?php echo $countries[$i]['id']; ?>"><?php echo $countries[$i]['name']; ?></option>
					<?php endfor; ?>
				</select>
				<select name="club_id_srch" id="club_id_srch" style="width:235px;font-size:14px;">
					<option value="0">Club</option>
					<?php for($i=0;$i<count($clubs);$i++): ?>
						<?php if(isset($_GET['club_id']) && $_GET['club_id'] == $clubs[$i]['id']) $sel = 'selected';else $sel = ''; ?>
						<option <?php echo $sel; ?> value="<?php echo $clubs[$i]['id']; ?>"><?php echo $clubs[$i]['title']; ?></option>
					<?php endfor; ?>
				</select>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="submit" name="submit_adv_search" id="submit_adv_search" value="Search">
			</form> <?php */ ?>
			
			<div class="clear"><br></div>
			</div>
			
			<?php if(!empty($_GET['keywords']) || !empty($_GET['club_id']) || !empty($_GET['country_id'])){ ?>
			
			<form method="post">
			<div class="list">
                <ul id="individual" class="content1">
                	<?  
						$club_id = 0;
						
						/*if(isset($_GET['club_name']) && !empty($_GET['club_name'])){
							$club_name = trim($_GET['club_name']);
							$query = "select id from clubs where title = '{$club_name}' ";	
							$rs   = mysqli_query($conn,$query);
							if(mysqli_num_rows($rs) == 0){
								$club_arr = mysqli_fetch_assoc($rs);
								$club_id = $club_arr['id'];
							}
							
						}*/
						
                		$rpp = PRODUCT_LIMIT_FRONT; // results per page
						
                		$country_id = 0;
						
      					$query = "select distinct u.* from users u ";
						$query_count = "select count(*) as users_count from users u ";
				      	//=======================================
						$where = " where u.is_company = '0'  and u.status = 1";
						if(isset($_GET['club_id']) && !empty($_GET['club_id'])){
							$club_id = trim($_GET['club_id']);
							$join  = " INNER JOIN users_to_clubs as c ON u.id=c.user_id ";
							$query.=$join;
							$query_count.=$join;
							$where.=" AND c.club_id = ".$club_id;
						}
						
						if(isset($_GET['country_id']) && !empty($_GET['country_id'])){
							$country_id = trim($_GET['country_id']);
							$where.=" AND u.country_id = ".$country_id;
						}
						
					    if(isset($_GET['keywords']) && $_GET['keywords'] != 'Search Here' && $_GET['keywords'] != 'all' ){
							$keywords = trim($_GET['keywords']);
							if(strpos($keywords,'_') !== false){
								$keywords_arr = explode('_',$keywords);
								$where.= " and (u.f_name like '%".$keywords_arr[0]."%'  AND u.last_name like '%".$keywords_arr[1]."%') ";
							}else{
								$where  .= " and (u.f_name like '%{$keywords}%' OR u.m_name like '%{$keywords}%' OR u.last_name like '%{$keywords}%') ";
							}
						}else if(isset($_GET['member_name_srch']) && $_GET['member_name_srch'] != 'Member Name' && $_GET['member_name_srch'] != 'all' ){
							$keywords = trim($_GET['member_name_srch']);
							if(strpos($keywords,'_') !== false){
								$keywords_arr = explode('_',$keywords);
								$where.= " and (u.f_name like '%".$keywords_arr[0]."%'  AND u.last_name like '%".$keywords_arr[1]."%') ";
							}else{
								$where  .= " and (u.f_name like '%{$keywords}%' OR u.m_name like '%{$keywords}%' OR u.last_name like '%{$keywords}%') ";
							}
						}else{
							$keywords = '';
						}
						
						$query.=$where;
						$query_count.=$where;
					    $query .= " order by u.id desc ";
				    
						$rs_count   = mysqli_query($conn,$query_count);
						$row_count  = mysqli_fetch_assoc($rs_count);
						$tcount = $row_count['users_count'];
					  
						$tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
						$count = 0;
						$start = ($ppage-1)* $rpp;
						$x = 0;
					  
						$query .= " LIMIT $start,$rpp ";
						
						$rs   = mysqli_query($conn,$query);
						if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
							echo '<div id="information">No record found ... !</div>';
						}
						///echo $query;
						$i= 0 ;
						while($row 	= mysqli_fetch_assoc($rs)){
                		

							$row_img = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row['id']."' and is_default = '1' "));
			  				$row_country= get_record_on_id('countries', $row['country_id']);	
			  				$location 	= $row_country['name'];
			  				$row_vid = mysqli_fetch_assoc(mysqli_query($conn,"select * from videos where entity_id = '".$row['id']."' and entity_type = 'users' and is_default = '1' and file_name != ''"));
							
							$str_club = '';
							$sql = "select c.company_name from club_members as cm  inner join companies as c on c.id=cm.club_id WHERE cm.club_id != ".$club_info['id']." and cm.member_id=".$row['id']." and c.company_type_id = 4 and c.status=1";
							$rs_club = mysqli_query($conn,$sql);//echo $sql.mysqli_error($conn);
							if(mysqli_num_rows($rs_club)){
								while($row_club = mysqli_fetch_assoc($rs_club)){
									$str_club.=" <br/>Member of ".$row_club['company_name'];
								}
								
							}
		        		    ?>
		        		    
						<dl <?php if(!empty($str_club)): ?> style="height:170px;" <?php endif; ?>>
								<dt>
									
									<a href="<?=WWW?>individual-detail-<?=$row['id']?>-<?=friendlyURL($row['f_name'].' '.$row['m_name'].' '.$row['last_name'])?>.html#activity-tab" title="<?=$row['f_name'].' '.$row['m_name'].' '.$row['last_name']?>"><img src="<?php echo WWW;?><?=($row_img && !empty($row_img['file_name']))?'users/'.$row['id'].'/photos/'.$row_img['file_name']:'images/no-photo.jpg'?>" width="120" height="128" /></a></dt>
								<dd><div class="details">
									<h3><?=truncate_string($row['f_name'],20).'<br>'.truncate_string($row['last_name'], 20)?></h3>
									<p><? echo $location;?><br />
									
<?if($row['user_type_id'] != 1)

{
    echo get_combo('user_types','name',$row['user_type_id'],'','text');  

?><br />
		 <?   if($row['user_type_id'] == 2){ echo $row['type'];} }?>
									
									<?php if(!empty($club_info)) { ?>
										<div style="float:left;">
										<?php if(!in_array($row['id'],$club_members)){ ?>
											<input type="checkbox" name="member_ids[]" id="member_id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>"> Add to Club
										<?php } else{ ?>
											Already Club Member
										<?php } ?>		 	
										<?php echo $str_club; ?>
										</div>
									<?php } ?>		 
								
									</p>
									<a class="submit-login margin-top-5" href="<?=WWW?>individual-detail-<?=$row['id']?>-<?=friendlyURL($row['f_name'].' '.$row['m_name'].' '.$row['last_name'])?>.html#activity-tab" title="<?=$row['f_name'].' '.$row['m_name'].' '.$row['last_name'].' - '.$site_title?>">View Profile</a></div>
									<div class="flag"><img title="<?=$row_country['name']?>" alt="<?=$row_country['name']?>" src="<?php echo WWW;?>countries/<?=$row_country['flag']?>"  width="25"/></div>
									<div class="video">
										<?php
										$video = $row_vid['file_name'];
											if(!empty($video)){
						if(preg_match('/<iframe(.*)<\/iframe>/', $row_vid['file_name'])){ 
									
									preg_match('/src="(.*?)"/',$row_vid['file_name'] , $src);
									$src = $src[1];
									$video = "<iframe width='205' height='130' frameborder='0' allowfullscreen 
												src='$src'>
																	</iframe>";
									
									if(!preg_match('/height="(.*)"/', $video)){
										$video = preg_replace('/height="(.*)"/','height="130"',$video);	
									}else{
										$video = preg_replace('/width="(.*)"/','width="220" height="150"',$video);	
									}
						}else{	
												$filename = explode('.',$video);
												$filename1= $filename[0];
												$video = '<video width="220" height="130" controls>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mp4" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.ogg" type=\'video/ogg; codecs="theora, vorbis"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mov" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.webm" type=\'video/webm; codecs="vp8, vorbis"\'>
												</video>';
											}
										}else{
											$video = '<img src="'.WWW.'images/no-video.jpg" width="168" height="130">';
										}
										echo $video; ?>									
									</div>
								</dd>
						</dl>
						<?php
                		
					      $i++;
					      $count++;
					      $x++;
					  } 
					  ?>
			</ul>
			</div><br/>
			<div style="float: left; clear: both; padding: 10px;">
			<input type="submit" name="members_add_submit" id="members_add_submit" value="Add Members"> 
			</div>
			</form>
			
			<?php } ?>
			
			<p>&nbsp;</p><p>&nbsp;</p>
			<div id="pagination-bottom">
				<? if($tcount != 0 && $tcount > PRODUCT_LIMIT_FRONT){ ?>
		      	<?php
		        	$q_url = '';
					if(isset($keywords) && !empty($keywords)){
						$q_url.="-{$keywords}";
					}else{
						$q_url.="-all";
					}
					if(isset($club_id) ){
						$q_url.="-{$club_id}";
					}
					if(isset($country_id) ){
						$q_url.="-{$country_id}";
					}
					
					if(!empty($q_url)){
						$reload = "club-members-search{$q_url}.html?";
					}else{
						$reload = "club-members-search-all.html?";
					}
		        	echo paginate_one($reload, $ppage, $tpages);
		      	?>
		      	<input type="hidden" name="pagination-page" value="club-members-search.html">
		        <? } ?>    
		        <form id="list-search" method="post" action="">
		        	<input type="text" name="txtsearch" placeholder="Search Here" class="validate[required] input-login">
		        	<input type="submit" value="">
		        </form>
				<div class="clear"></div>
		        <script type="text/javascript">
		        $('form#list-search').submit(function(e){
					var parentId = $(this).parent().attr('id');
					var string = $('#'+parentId +' form input[name=txtsearch]').val();
					if(string != '' && string != 'Search Here'){
						string = string.replace(/[ ]+/g,'_');
						string = string.replace(/[^a-zA-Z0-9_]+/g,'').toLowerCase();
						if(string.length > 0){
							$('form#list-search').attr('action','<?=WWW;?>club-members-search-' + string + '.html');
						}
					}
				});
				
				$('#submit_adv_search').click(function(e){
				
					var string = $('#member_name_srch').val();
					if(string == ''){
						string = 'all';
					}
					if($("#search_box").val()!=''){
						var club_id = $('#club_id').val();
					}else{
						var club_id = 0;
					}
					var country_id = $('#country_id_srch').val();
					//if(string != '' && string != 'Member Name'){
						string = string.replace(/[ ]+/g,'_');
						string = string.replace(/[^a-zA-Z0-9_]+/g,'').toLowerCase();
						//if(string.length > 0){
							$('form#advance_search_form').attr('action','<?=WWW;?>club-members-search-' + string +"-"+club_id+"-"+country_id+ '.html');
						//}
					//}
				});
		        </script>
				
				<script type="text/javascript">
				$(document).ready(function(){
				$("#search_box").keyup(function(){var search_string = $("#search_box").val();
				if(search_string == ''){$("#searchres").html('');}else{postdata = {'string' : search_string}
				$.post("get_clubs.php",postdata,function(data){	$("#searchres").html(data);	});}});});
				function fillme(name,id){$("#search_box").val(name);$("#searchres").html(''); $("#club_id").val(id);}
				</script>
		        <div class="clear"></div>
		    </div>  
		    <div class="clear"></div>
		</div>
		</div>
		
		<div class="clear"></div>
	</div>
<style>
.header_search{background-color:none;padding:20 0 5 200px;width:200px;float:left;}
.searchres{margin-left:2px;width:200px;border: #f0f0f0 1px solid;border-radius:5px;position:absolute;z-index:100;background-color:#fff;}.img{float:left;margin: 5 5 5 5px;float:left;}
.search_box{width: 190px;height: 15px;border-radius: 5px;padding: 10px;font-family: verdana;color:black; font-size: 15px;border:1px solid #ccc;}
.name{margin-top: 14px;}
.user_div{clear:both;font-family: verdana;border-top: #f0f0f0 1px solid;color:black; font-size: 13px;height:20px;margin:1px;padding:7px;width:180px;border-radius:4px;vertical-align: top;  }
.user_div:hover{background:#F6F6F6;color:#fff;cursor:pointer}
.no_data{height: 40px;background-color: #F0F0F0;padding: 10 0 0 10px;width: 180px;border-radius: 5px;font-family: verdana;color:black; font-size: 15px;}
.name{float:left;margin:0 0 0 10px;}.cntry{margin:0 0 0 50px; font-size:13px}
</style>
<?php include('common/footer.php'); ?>