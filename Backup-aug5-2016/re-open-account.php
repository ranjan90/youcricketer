<?php include('common/header.php'); ?>
	<div class="middle">
		<h1>Re-Open Account </h1>

		<? 	if(isset($_POST) && !empty($_POST)){
				$username 	= $_POST['email'];
					
				$rs_user	= mysqli_query($conn,"select * from users where email = '$username'");
				
				if(mysqli_num_rows($rs_user) == 0){
					echo '<div id="error">Invalid email address ... !</div><br><br>';	
				}else{
					$row_u = mysqli_fetch_assoc($rs_user);
					$password = getRandomWord(8);
					if($row_u['status'] == '5'){
						$email_template = get_record_on_id('cms', 28);
						$mail_title		= $email_template['title'];
		                $mail_content	= $email_template['content'];
		                $mail_content 	= str_replace('{to_name}',$row_u['f_name'], $mail_content);
	    	            $mail_content 	= str_replace('{password}',$password, $mail_content);
	            	    $mail_content 	= str_replace('{click_here}','<a href="'.WWW.'re-open-account.html?email='.$row_u['email'].'" title="Click Here">HERE</a>', $mail_content);
	                	$mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);
	                	$headers = "Content-type:text/html;charset=UTF-8" . "\r\n";
						$headers .= "From: no-reply@youcricketer.com" . "\r\n";
						$query = "update users set password = '".md5($password)."' where email = '".$row_u['email']."'";
		                mysqli_query($conn,$query);
		                mail($row_u['email'],$mail_title,$mail_content,$headers);
		                echo '<div id="success"><b>Success : </b>Re-Open Account email has been sent, Please check your email ... !</div><br><br>';
					}
				}
			}
			if(isset($_GET['email'])){
				$email = $_GET['email'];
				mysqli_query($conn,"update users set status = '1' where email = '$email' ");
				echo '<div id="success"><b>Success : </b>Your account has been re-opened ... !</div><br><br>';
				?>
				<script>
				window.location = '<?php echo WWW;?>login.html';
				</script>
				<?
			}
		?>

		<div class="white-box content">
			<form method="post" action="">
				<fieldset>
					<h2>Enter Information</h2>
					<div class="form-box">
						<label>Email Address </label>
						<input type="text" name="email" class="input-login validate[required, custom[email]]">
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<?
                        //checking google recaptcha
                        include('includes/captcha.php');
                        ?>
                        <script type="text/javascript" src="https://www.google.com/recaptcha/api/challenge?k=<?=PUBLIC_KEY?>"</script>
                        <noscript>
                        <iframe src="https://www.google.com/recaptcha/api/noscript?k=<?=PUBLIC_KEY?>" frameborder="0"></iframe><br>
                        </noscript>
                        <?
                        include('recaptchalib.php');
                        $publickey    = PUBLIC_KEY;
                        $privatekey   = PRIVATE_KEY;
                        $resp         = null;
                        $error        = null;
                        echo $publickey;

                        if ($_POST["recaptcha_response_field"]) {
                          $resp = recaptcha_check_answer ($privatekey,
                                                          $_SERVER["REMOTE_ADDR"],
                                                          $_POST["recaptcha_challenge_field"],
                                                          $_POST["recaptcha_response_field"]);

                          if ($resp->is_valid) {
                                  echo "You got it!";
                          } else {
                                  # set the error code so that we can display it
                                  $error = $resp->error;
                          }
                        }
                        echo recaptcha_get_html($publickey, $error);
                        //checking google recaptcha
                        ?>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<input type="submit" value=" Submit " class="submit-login">
					</div>
				</fieldset>
			</form>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>
<script>

$(window).load(function(){
	$('form').validationEngine();
	$('#recaptcha_response_field').addClass('validate[required]');
});
                        </script>

<?php include('common/footer.php'); ?>