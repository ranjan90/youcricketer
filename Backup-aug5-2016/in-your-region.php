<?php include('common/header.php'); ?>
<?	$selected_country = getGeoLocationCountry();
		 
	$row_country = mysqli_fetch_assoc(mysqli_query($conn,"select * from countries where name = '$selected_country'"));
?>
<style>
.large-column{width:250px;}.orange-box{min-height:312px;}
</style>
	<div class="middle">
		<h1>In Your Region - <?=$selected_country?> </h1>

		<div class="white-box content" id="in-your-region">
		
			<?php if(!isset($_SESSION['ycdc_user_email']) || empty($_SESSION['ycdc_user_email'])): ?>
				<div class="community-box" onclick="window.location='<?=WWW?>sign-up-community.html?page=blogs&region=true';">
					<h2>Blogs in Your Region</h2>
					<img src="<?=WWW?>images/community-blog.png" alt="Community - Blog - <?=$site_title?>">
				</div>
				<div class="community-box" onclick="window.location='<?=WWW?>sign-up-community.html?page=forums&region=true';">
					<h2>Forums in Your Region</h2>
					<img src="<?=WWW?>images/community-forum.png" alt="Community - Forum - <?=$site_title?>">
				</div>
				<div class="community-box" onclick="window.location='<?=WWW?>sign-up-community.html?page=groups&region=true';">
					<h2>Groups in Your Region</h2>
					<img src="<?=WWW?>images/community-groups.png" alt="Community - Groups - <?=$site_title?>">
				</div>
				<div class="community-box" onclick="window.location='<?=WWW?>sign-up-community.html?page=testimonials&region=true';">
					<h2>Testimonials in Your Region</h2>
					<img src="<?=WWW?>images/community-testimonials.png" alt="Community - Testimonials - <?=$site_title?>">
				</div>
				<div class="community-box" onclick="window.location='<?=WWW?>sign-up-community.html?page=member-news&region=true';">
					<h2>Whats Happening in Your Region</h2>
					<img src="<?=WWW?>images/community-news.png" alt="Community - News - <?=$site_title?>">
				</div>
				<div class="community-box" onclick="window.location='<?=WWW?>sign-up-community.html?page=events&region=true';">
					<h2>Events in Your Region</h2>
					<img src="<?=WWW?>images/community-events.png" alt="Community - Events - <?=$site_title?>">
				</div>
				
			<?php elseif(isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])): ?>
			
				<div class="community-box" onclick="window.location='<?=WWW?>your-region-blogs.html';">
					<h2>Blogs in Your Region</h2>
					<img src="<?=WWW?>images/community-blog.png" alt="Community - Blog - <?=$site_title?>">
				</div>
				<div class="community-box" onclick="window.location='<?=WWW?>your-region-forums.html';">
					<h2>Forums in Your Region</h2>
					<img src="<?=WWW?>images/community-forum.png" alt="Community - Forum - <?=$site_title?>">
				</div>
				<div class="community-box" onclick="window.location='<?=WWW?>your-region-groups.html';">
					<h2>Groups in Your Region</h2>
					<img src="<?=WWW?>images/community-groups.png" alt="Community - Groups - <?=$site_title?>">
				</div>
				<div class="community-box" onclick="window.location='<?=WWW?>your-region-testimonials.html';">
					<h2>Testimonials in Your Region</h2>
					<img src="<?=WWW?>images/community-testimonials.png" alt="Community - Testimonials - <?=$site_title?>">
				</div>
				<div class="community-box" onclick="window.location='<?=WWW?>your-region-news.html';">
					<h2>Whats Happening in Your Region</h2>
					<img src="<?=WWW?>images/community-news.png" alt="Community - News - <?=$site_title?>">
				</div>
				<div class="community-box" onclick="window.location='<?=WWW?>your-region-events.html';">
					<h2>Events in Your Region</h2>
					<img src="<?=WWW?>images/community-events.png" alt="Community - Events - <?=$site_title?>">
				</div>
			
			<?php else: ?>
			
			<?php // this else data is not displayed ?>
		
			<div class="large-column">
				<div class="black-box">
			        <div class="orange-box">
			            <h2 class="black-heading"> Blogs In Your Region</h2>
			            <ul>
			            <?	$rs_g = mysqli_query($conn,"select * from blog_articles where status = '1' and user_id in (select id from users where country_id = '".$row_country['id']."') ORDER BY id DESC  limit 5");
			            	while($row_g = mysqli_fetch_assoc($rs_g)){
			            	?>
			            	<li>
			            		<a href="<?=WWW?>group-detail-<?=$row_g['id']?>-<?=friendlyURL($row_g['title'])?>.html" title="<?=$row_g['title'] .' - '.$site_title?>">
			            			<?php if(strlen($row_g['title'])>60) echo substr($row_g['title'],0,60).'...';else echo $row_g['title'] ;?>
			            		</a>
			            	</li>
			            	<?
			            	}
			            ?>
			             <? if(mysqli_num_rows($rs_g) > 0){ ?>
			            <li class="more"><a href="<?=WWW?>your-region-blogs.html">more...</a></li>
			            <? } ?>
			        	</ul>
			        </div>
			        <div class="orange-box">
			        	<h2 class="orange-heading"> Testimonials In Your Region </h2>
			        	<ul>
						<?php $query = "select * from testimonials as t  ";
						$query .= " join users u on u.id = t.user_id and (u.country_id = '".$row_country['id']."'  ) order by t.id desc limit 5"; ?>
			            <?	$rs_g = mysqli_query($conn,$query);
			            	while($row_t = mysqli_fetch_assoc($rs_g)){
			            	?>
			            	<li>
			            		<?=get_combo('users','concat(f_name," ",last_name)',$row_t['user_id'],'','text')?> for <?=get_combo('users','concat(f_name," ",last_name)',$row_t['entity_id'],'','text')?> on <?=date_converter($row_t['testimonial_date'],'M d,Y H:i')?>
								<p><?=$row_t['content']?></p>
			            	</li>
			            	<?
			            	}
			            ?>
			             <? if(mysqli_num_rows($rs_g) > 0){ ?>
			            <li class="more"><a href="<?=WWW?>your-region-testimonials.html">more...</a></li>
			            <? } ?>
			        	</ul>
			        </div>
			       <?php /* ?> <div class="orange-box">
			        	<h2 class="black-heading"> Clubs In Your Region </h2>
			        	<p>Clubs are also considered as companies</p>
			        	<ul>
			            <?	$rs_g = mysqli_query($conn,"select c.* from companies c join users u on u.id = c.user_id where c.company_type_id = '4' and c.country_id = '".$row_country['id']."' order by c.id desc limit 5");
			            	while($row_g = mysqli_fetch_assoc($rs_g)){
			            	?>
			            	<li>
			            		<a href="<?=WWW?>company-detail-<?=$row_g['id']?>-<?=friendlyURL($row_g['company_name'])?>.html" title="<?=$row_g['company_name'] .' - '.$site_title?>">
			            			<?=$row_g['company_name']?>
			            		</a>
			            	</li>
			            	<?
			            	}
			            ?>
			             <? if(mysqli_num_rows($rs_g) > 0){ ?>
			            <li class="more"><a href="<?=WWW?>your-region-clubs.html">more...</a></li>
			            <? } ?>
			        	</ul>
			        </div><?php */ ?>
			    </div>
			</div>
			<div class="large-column">
				<div class="orange-box">
			        <div class="orange-box">
			            <h2 class="orange-heading"> Forums In Your Region</h2>
			            <ul>
			            <?	$rs_g = mysqli_query($conn,"select * from forum_topics where status = '1' and user_id in (select id from users where country_id = '".$row_country['id']."')  ORDER BY id DESC limit 5");
			            	while($row_g = mysqli_fetch_assoc($rs_g)){
			            	?>
			            	<li>
			            		<a href="<?=WWW?>forum-detail-<?=$row_g['id']?>-<?=friendlyURL($row_g['title'])?>.html" title="<?=$row_g['title'] .' - '.$site_title?>">
			            			<?php if(strlen($row_g['title'])>60) echo substr($row_g['title'],0,60).'...';else echo $row_g['title'] ;?>
			            		</a>
			            	</li>
			            	<?
			            	}
			            ?>
			             <? if(mysqli_num_rows($rs_g) > 0){ ?>
			            <li class="more"><a href="<?=WWW?>your-region-forums.html">more...</a></li>
			            <? } ?>
			        	</ul>
			        </div>

			        <div class="orange-box">
			        	<h2 class="black-heading"> Events In Your Region </h2>
			        	<ul>
			            <?	$rs_g = mysqli_query($conn,"select * from events where status = '1' and user_id in (select id from users where country_id = '".$row_country['id']."')  ORDER BY id DESC limit 5");
			            	while($row_g = mysqli_fetch_assoc($rs_g)){
			            	?>
			            	<li>
			            		<a href="<?=WWW?>event-detail-<?=$row_g['id']?>-<?=friendlyURL($row_g['title'])?>.html" title="<?=$row_g['title'] .' - '.$site_title?>">
			            			<?php if(strlen($row_g['title'])>60) echo substr($row_g['title'],0,60).'...';else echo $row_g['title'] ;?>
			            		</a>
			            	</li>
			            	<?
			            	}
			            ?>
			             <? if(mysqli_num_rows($rs_g) > 0){ ?>
			            <li class="more"><a href="<?=WWW?>your-region-events.html">more...</a></li>
			            <? } ?>
			        	</ul>
			        </div>
			        <?php /* ?><div class="orange-box">
			        	<h2 class="orange-heading"> Leagues In Your Region  </h2>
			        	<p>Leagues are also considered as companies</p>
			        	<ul>
			            <?	$rs_g = mysqli_query($conn,"select c.* from companies c join users u on u.id = c.user_id where c.company_type_id = '2' and c.country_id = '".$row_country['id']."' order by c.id desc limit 5");
			            	while($row_g = mysqli_fetch_assoc($rs_g)){
			            	?>
			            	<li>
			            		<a href="<?=WWW?>company-detail-<?=$row_g['id']?>-<?=friendlyURL($row_g['company_name'])?>.html" title="<?=$row_g['company_name'] .' - '.$site_title?>">
			            			<?=$row_g['company_name']?>
			            		</a>
			            	</li>
			            	<?
			            	}
			            ?>
			             <? if(mysqli_num_rows($rs_g) > 0){ ?>
			            <li class="more"><a href="<?=WWW?>your-region-leagues.html">more...</a></li>
			            <? } ?>
			        	</ul>
			        </div><?php */ ?>
			        
			    </div>
			</div>
			<div class="large-column">
				<div class="black-box">
			        <div class="orange-box">
			            <h2 class="black-heading"> Groups In Your Region </h2>
			            <ul>
			            <?	$rs_g = mysqli_query($conn,"select * from groups where status = '1' and user_id in (select id from users where country_id = '".$row_country['id']."') ORDER BY id DESC  limit 5");
			            	while($row_g = mysqli_fetch_assoc($rs_g)){
			            	?>
			            	<li>
			            		<a href="<?=WWW?>blog-detail-<?=$row_g['id']?>-<?=friendlyURL($row_g['title'])?>.html" title="<?=$row_g['title'] .' - '.$site_title?>">
			            			<?php if(strlen($row_g['title'])>60) echo substr($row_g['title'],0,60).'...';else echo $row_g['title'] ;?>
			            		</a>
			            	</li>
			            	<?
			            	}
			            ?>
			            <? if(mysqli_num_rows($rs_g) > 0){ ?>
			            <li class="more"><a href="<?=WWW?>your-region-groups.html">more...</a></li>
			            <? } ?>
			        	</ul>
			        </div>
			        <div class="orange-box">
			        	<h2 class="orange-heading"> Members News In Your Region </h2>
			        	<ul>
			            <?	$rs_g = mysqli_query($conn,"select * from news where status = '1' and user_id in (select id from users where country_id = '".$row_country['id']."') ORDER BY id DESC  limit 5");
			            	while($row_g = mysqli_fetch_assoc($rs_g)){
			            	?>
			            	<li>
			            		<a href="<?=WWW?>news-detail-<?=$row_g['id']?>-<?=friendlyURL($row_g['title'])."+"?>.html" title="<?=$row_g['title'] .' - '.$site_title?>">
			            			<?php if(strlen($row_g['title'])>60) echo substr($row_g['title'],0,60).'...';else echo $row_g['title'] ;?>
			            		</a>
			            	</li>
			            	<?
			            	}
			            ?>
			             <? if(mysqli_num_rows($rs_g) > 0){ ?>
			            <li class="more"><a href="<?=WWW?>your-region-news.html">more...</a></li>
			            <? } ?>
			        	</ul>
			        </div>
			        
			        <?php /* ?><div class="orange-box">
			        	<h2 class="black-heading"> Other Companies In Your Region </h2>
			        	<p>Except Clubs and Leagues</p>
			        	<ul>
			            <?	$rs_g = mysqli_query($conn,"select c.* from companies c join users u on u.id = c.user_id where c.company_type_id not in (2,4) and c.country_id = '".$row_country['id']."' order by c.id desc limit 5");
			            	while($row_g = mysqli_fetch_assoc($rs_g)){
			            	?>
			            	<li>
			            		<a href="<?=WWW?>company-detail-<?=$row_g['id']?>-<?=friendlyURL($row_g['company_name'])?>.html" title="<?=$row_g['company_name'] .' - '.$site_title?>">
			            			<?=$row_g['company_name']?>
			            		</a>
			            	</li>
			            	<?
			            	}
			            ?>
			             <? if(mysqli_num_rows($rs_g) > 0){ ?>
			            <li class="more"><a href="<?=WWW?>your-region-other-companies.html">more...</a></li>
			            <? } ?>
			        	</ul>
			        </div><?php */ ?>
			        
			    </div>
			</div>
			<?php endif; ?>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>

<?php include('common/footer.php'); ?>