<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	<div class="middle">
		<h1>Compose Message </h1>
		<div class="white-box content" id="dashboard">
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column" style="position:relative;">
				<? 	if(isset($_POST['action']) && $_POST['action'] == 'send'){ 
						
						if(!empty($_POST['subject']) && !empty($_POST['message']) && !empty($_POST['to'])){
							$message 	= $_POST['message'];
							$subject 	= $_POST['subject'];
							$from_user_id = $_SESSION['ycdc_dbuid'];
							$date 		= date('Y-m-d H:i:s');
							$toEmails 	= explode('),', $_POST['to']);
							
							$counter 	= 0;
							foreach($toEmails as $te){
								$complete = explode('(', $te);
								$info 	  = explode(',', $complete[1]);
								$role  	  = $info[0];
								$country  = str_replace(')','',$info[1]);
								$rowRole = mysqli_fetch_assoc(mysqli_query($conn,"select * from user_types where name = '".trim($role)."'"));
								$rowcountry = mysqli_fetch_assoc(mysqli_query($conn,"select * from countries where name = '".trim($country)."'"));
								$name = explode(' ', $complete[0]);
								$rowUser= mysqli_fetch_assoc(mysqli_query($conn,"select * from users where f_name = '".$name[0]."' and last_name = '".$name[1]."' 
																		and user_type_id = '".$rowRole['id']."' and country_id = '".$rowcountry['id']."'"));
								/********************************/
								$to_user_id = $rowUser['id'];
								if(!empty($to_user_id)){
										$query = "insert into messages (from_user_id, to_user_id, message_date, subject, message, status, thread_id) 
										values ('$from_user_id','$to_user_id','$date','$subject','$message','0','0'); ";

										$email_template = get_record_on_id('cms', 16);
										$mail_title		= $email_template['title'];
							            $mail_content	= $email_template['content'];
							            $mail_content 	= str_replace('{to_name}',$rowUser['f_name'], $mail_content);
						                $mail_content 	= str_replace('{login_link}','<a href="'.WWW.'inbox.html" title="Login to your account">HERE</a>', $mail_content);
						                $mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);
										
											$headers = "Content-type:text/html;charset=UTF-8" . "\r\n";
											$headers .= "From: no-reply@youcricketer.com" . "\r\n";
							
										if(mysqli_query($conn,$query) && mail($rowUser['email'],$mail_title,$mail_content,$headers)){
											$counter++;
										}
									
								}
							}
							if($counter > 0){
								echo '<div id="success"> Message sent successfully</div>';
							}
						}else{
								echo '<div id="error"> Please fill all fields ... !</div>';
						}
					}
				?>
			<form method="post" action="">
				<input type="hidden" name="action" value="send">
				<fieldset>
				<h2>Fill all fields</h2>
					<div class="form-box ui-widget">
						<label>To : </label>
						<textarea class="validate[required]" id="to" name="to" style="height:30px; width:600px; margin-left:95px; margin-top:-20px;"></textarea>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Subject : </label>
						<input type="text" name="subject" class="input-login validate[required]" style="width:600px; margin-top:-20px;float:left; margin-left:95px;">
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Message : </label>
						<textarea class="validate[required]" name="message" style="width:600px; margin-top:-10px; margin-left:95px; height:100px;"></textarea>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<input type="submit" value=" Send " class="submit-login">
					</div>
				</fieldset>
			</form>
		    <div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<script src="<?php echo WWW?>js/jquery-1.8.0.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=WWW?>js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?=WWW?>js/select2.js"></script>
<link href="<?=WWW?>css/jquery-ui.css" type="text/css" rel="stylesheet">
<link href="<?=WWW?>css/select2.css" type="text/css" rel="stylesheet">
<script>
$(function(){
	function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }
 	/*
    $( "#to" )
      .bind( "keydown", function( event ) {

      	if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).data( "ui-autocomplete" ).menu.active ) {
          event.preventDefault();
        }
      })
      .autocomplete({
      	minLength: 0,
         source: function (request, response) {
         	$.ajax({
                url: WWW + 'includes/get-users.php',
                data: {
                    //filter: request.term,
                    pagesize: 10
                },
                dataType: "json",
                success: function(data) {
                	response($.map(data.users, function(el, index) {
                    	return {
                            value: el.display_name,
                            avatar: WWW + "users/" + el.user_id + '/' + el.email_hash
                        };
                    }));
                }
            });
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = split( this.value );
          terms.pop();
          terms.push( ui.item.value );
          terms.push( "" );
          this.value = terms.join( ", " );
          return false;
        }
      }).data("autocomplete")._renderItem = function (ul, item) {
        return $("<li />")
            .data("item.autocomplete", item)
            .append("<a><img class='autocomplete-photo' src='" + item.avatar + "' />" + item.value + "</a>")
            .appendTo(ul);
    	};
    	*/
    /*****************************************************/
    $.ajax({type	: 'POST', 
		   	url		: WWW + 'includes/get-users.php', 
			dataType: 'json',
			success	: function(msg){
				$("#to").select2({tags:msg});
			}
    });
	/*****************************************************/
	$('#dashboard .black-box h2').click(function(){
		$('#dashboard .black-box ul').css('display','none');
		var id = this.id;
		$('#'+id+'-box ul').css('display','block');
	});
	$('form').validationEngine();
});
</script>
<style>
.ui-autocomplete{margin: 460px 467px; min-height:200px;width:600px;}
img.autocomplete-photo{width:25px; height:25px; padding-right:5px;}
.form-box a.select2-search-choice-close{padding:0px; background:url("../images/select2.png") no-repeat scroll right top rgba(0, 0, 0, 0);}
</style>
<?php include('common/footer.php'); ?>