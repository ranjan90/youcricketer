<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	<div class="middle">
		<h1>Update Forum </h1>
		<? 	if(isset($_GET['action']) && $_GET['action'] == 'deletephoto'){
				$pid 	= $_GET['pid'];
				$row_img= get_record_on_id('photos',$pid);
				mysqli_query($conn,"delete from photos where id = '$pid'");
				unlink(WWW.'forum/'.$_GET['id'].'/'.$row_img['file_name']);
				?>
				<script>
				window.location = '<?=WWW?>edit-forum-<?=$_GET['id']?>.html';
				</script>
				<?
			}
			if(isset($_POST) && !empty($_POST)){ 
				$title 	= $_POST['title'];
				$content= $_POST['content'];
				$date 		= date('Y-m-d');
				$isGlobal = ($_POST['is_global'] == 'on')?'1':'0';
				$user_id 	= $_SESSION['ycdc_dbuid'];
				$id 		= $_GET['id'];
				$photo_id 	= $_POST['photo_id'];

				if(!isset($user_id) || empty($user_id)){
					echo '<div id="error">Please login first ... !</div>';
				}else{
					if(!empty($title) && !empty($content)){
						$query = "update forum_topics set is_global = '$isGlobal', title = '$title', content = '$content' where id = '$id'";
						if(mysqli_query($conn,$query)){
							chmod('forum',0777);
							if(!is_dir('forum/'.$id)){
								mkdir('forum/'.$id,0777);
							}
							chmod('forum/'.$id,0777);
							if(!empty($_FILES['photo']['name'])){
								$filename 	= friendlyURL($title).'.jpg';
								$image 		= new SimpleImage();
								$image->load($_FILES["photo"]["tmp_name"]);
								$image->save('forum/'.$id.'/'.$filename);
								chmod('forum/'.$id.'/'.$filename,0777);
								if(!empty($photo_id)){
									mysqli_query($conn,"update photos set file_name = '$filename' where id = '$photo_id'");	
								}else{
									mysqli_query($conn,"insert into photos (file_name, entity_type, entity_id) values ('$filename','forum','$id');");
								}								
							}
						}
		                echo '<div id="success">Forum Information updated successfully ... !</div>';
		                ?>
		                <script>
		                window.location = '<?=WWW?>my-forum.html';
		                </script>
		                <?
		            }else{
						echo '<div id="error">Please fill all fields ... !</div>';
					}
				}
				
			}
			$id 	= $_GET['id'];
			$row 	= get_record_on_id('forum_topics', $id);
			$row_img= mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'forum' and entity_id = '$id'"));
		?>
		<div class="white-box content">
			<form method="post" enctype="multipart/form-data">
				<fieldset>
					<h2>Forum Information</h2>
					<div class="form-box">
						<label>Forum Category</label>
						<?=get_combo('forum_categories','name',$row['category_id'],'category_id')?>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Title</label>
						<input type="text" name="title" value="<?=$row['title']?>" class="input-login validate[required]">
					</div>
					<div class="clear"></div>
					<div class="form-box" style="width:100%;">
						<label>Is Global</label>
						<input style="float:left; margin-left:70px;" type="checkbox" name="is_global" <?php echo ($row['is_global'] == '1')?'checked="checked"':'';?>>
						<label>Forum will be Regional Forum If checked</label>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Content</label>
						<textarea maxlength="500" name="content" class="validate[required]"><?=$row['content']?></textarea>
					</div>
					<div class="clear"></div>
					<h2>Forum Photo</h2>
					<div class="form-box">
						<label>Forum Photo</label>
						<input type="file" name="photo">
						<br>
						<? 	if($row_img){ ?>
						<a href="<?=WWW?>edit-forum-<?=$_GET['id']?>-deletephoto-<?=$row_img['id']?>.html"><img style="position:relative; top:20px; right:5px;" src="<?=WWW?>images/icons/delete.png"></a>
						<br>
						<img src="<?=WWW?>forum/<?=$id?>/<?=$row_img['file_name']?>" width="100">
						<input type="hidden" name="photo_id" value="<?=$row_img['id']?>">
						<?	} ?>
					</div>
					<div class="form-box">
						<label>Max Photo size : 2MB</label>
					</div>
					<div class="clear"></div>
					
					<div class="form-box">
						<input type="submit" value=" Save " class="submit-login">
					</div>
					<div class="form-box">
						<a id="cancel" class="submit-login" href="<?=WWW?>forums.html">Cancel</a>
					</div>
				</fieldset>
			</form>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>
<style>
#cancel{background: url('<?php echo WWW;?>images/sign-in-button.png') no-repeat !important; color:#fff !important; padding-left:3px; top:0px !important;position:relative; left:-150px;}
</style>
<?php include('common/footer.php'); ?>