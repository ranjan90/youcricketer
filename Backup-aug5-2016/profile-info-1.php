<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } 


if($_SESSION['delete_photo'] == "Del"){
	
	echo '<div id="success"><b>Success : </b>Photo Deleted ... !</div><br><br>';
	unset($_SESSION['delete_photo']);
}
 
?>
	<div class="middle">
	
		<h1>Profile Information</h1>

		<? 
		
		$user_id = $row_user['id'];
			$f_name  = $row_user['f_name'];	

			if(isset($_POST) && !empty($_POST)){
				if(!empty($_FILES['photo']['name'])){
					if(!is_dir('users/'.$user_id)){
						mkdir('users/'.$user_id,0777);
					}
					chmod('users/'.$user_id,0777);
					if(!is_dir('users/'.$user_id.'/photos')){
						mkdir('users/'.$user_id.'/photos',0777);
					}
					chmod('users/'.$user_id.'/photos',0777);

					$filename 	= friendlyURL($f_name).'.jpg';
					$image 		= new SimpleImage();
					$image->load($_FILES["photo"]["tmp_name"]);
					$image->save('users/'.$user_id.'/photos/'.$filename);
					chmod('users/'.$user_id.'/photos/'.$filename,0777);
						
					$rs_photos = mysqli_query($conn,"select * from photos where entity_id = '".$user_id."' and entity_type = 'users' and is_default = '1'");
					if(mysqli_num_rows($rs_photos) > 0){
						mysqli_query($conn,"update photos set file_name = '$filename' where entity_id = '$user_id' and entity_type = 'users' and is_default = '1'");
					}else{
						mysqli_query($conn,"insert into photos (file_name, title, entity_id, entity_type, is_default) values ('$filename','$f_name','$user_id','users','1')");
					}		
				}else{
					$row_photo_user = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$user_id."' and is_default = '1'"));
					$row_photo_id   = $row_photo_user['id'];
					mysqli_query($conn,"update photos set is_default = '0' where id = '".$row_photo_id."'");
					mysqli_query($conn,"update photos set is_default = '1' where id = '".$_POST['album_photo']."'");
				}

				echo '<div id="success"><b>Success : </b>Information updated ... !</div><br><br>';
			}
			$row_photo = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$user_id."' and is_default = '1'"));
			
			?>

		<div class="white-box content" id="dashboard">
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
			<?  if($row_user['is_company'] == '1'){
					include('common/company-profile-navigation.php');
				}else{
					include('common/profile-navigation.php');
				}
			?>
			<form method="post" action="" enctype="multipart/form-data">
				<input type="hidden" name="action" value="submit">
				<fieldset>
					<legend>Profile Picture</legend>
					<div class="form-box">
						<label>Photo </label>
						<div class="text">
							<? if($row_photo){ ?>
							
							<div style="float:left;width:160px;">
		<a class="no-bg"  href="<?=WWW?>delete-user-profile-picture.php?id=<?php echo $row_photo['id'];?>"><img  src="<?=WWW?>images/erase.png" border="0" style=" position:relative; left:140px;top:17px;z-index:1;" /></a>
		<img src="<?=WWW?>users/<?=$row_user['id']?>/photos/<?=$row_photo['file_name']?>" width="150">
							
		</div>
							
							
							
							<? }else{ ?>
							<img src="<?=WWW?>images/no-photo.png" width="150">
							<? } ?>
						</div>
						<input type="file" name="photo" accept="image/*"> 
					<div class="form-box" style="position: relative;  margin-top:-27px; margin-left: 250px; " id="from-album-btn">
						<a id="from-album" class="submit-login no-bg">  From Album</a>
					</div>
					
					</div>
					<div class="form-box">
						<label>Max Photo size : 2MB &nbsp; &nbsp; &nbsp; <a target="_blank" href="<?=WWW?>multimedia-policy.html">Multi Media Policy</a></label>
					</div>
					<div class="clear"></div>
					</fieldset>
					<table width="100%" class="profile-action">
						<tr>
							<td width="33%">
								<a id="cancel" class="submit-login" style="position:relative; left:-150px; top:10px;" href="<?=WWW?>dashboard.html">Cancel</a>
							</td>
							<td width="33%">
								<input id="update" type="submit" value=" Upload " style="left:-100px; position:relative;" class="submit-login">
							</td>
							<td width="33%">
								<a class="submit-login" style="position:relative;left:0px;  top:10px;" href="<?=WWW?>profile-information-step-2.html">Skip</a>
							</td>
						</tr>
					</table>
					<div class="clear"></div>
					
					<fieldset>
					
					<div id="photos" class="hide">
						<? 	$rs_photo = mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$user_id."' and is_default <> '1'");
							if(mysqli_num_rows($rs_photo) > 0){
								while($row_photos = mysqli_fetch_assoc($rs_photo)){
								?>
								<div id="photo">
									<img src="<?=WWW?>users/<?=$user_id?>/photos/<?=$row_photos['file_name']?>" width="200">
									<br>
									<center>
										<input type="radio" name="album_photo" value="<?=$row_photos['id']?>">
									</center>
								</div>
								<?
								}
							}else{
								echo '<div id="information">Photo album is empty</div>';
							}
						?>
					</div>
				</fieldset>
			</form>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<style>
#photos #photo{width:225px; margin:5px 5px 10px 5px; float:left; height:300px; }
</style>
<script src="<?php echo WWW?>js/jquery-1.8.0.js" type="text/javascript"></script>
<script>

$(document).ready(function (){

	$('#dashboard .black-box h2').click(function(){
		$('#dashboard .black-box ul').css('display','none');
		var id = this.id;
		$('#'+id+'-box ul').css('display','block');
	});
});
</script>
<script type="text/javascript">
$(document).ready(function(){
	$('a#from-album').click(function(){
		var divid = document.getElementById('photos');
		if(divid.style.display == 'block'){
			divid.style.display = 'none';
		}else{
			divid.style.display = 'block';
		}
	});

	$('#dashboard .black-box h2').click(function(){
	//	$('#dashboard .black-box ul').css('display','none');
		var id = this.id;
		$('#'+id+'-box ul').css('display','block');
	});
	
	$('input[name=photo]').change(function(){
		var val = this.value;
		var filesize = Math.round((this.files[0].size)/1024,2);
		if(filesize > 2000){
			$(this).val('');
            alert("Image is too large ... !");
		}
	});
});
</script>
<?php include('common/footer.php'); ?>