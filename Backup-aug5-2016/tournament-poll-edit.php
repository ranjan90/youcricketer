<?php include_once('includes/configuration.php');
$page = 'tournament-video-add.html';
$selected_country = getGeoLocationCountry(); 

$error = '';
$tournament_permission = 0;
$tournament_info = array();
$poll_info = array();

$poll_id = trim($_GET['p_id']);
$poll_info = get_record_on_id('tournament_polls', $poll_id);	

$tournament_id = $poll_info['tournament_id'];
$tournament_info = get_record_on_id('tournaments', $tournament_id);	

$page_title = 'Edit Poll - '.ucwords($tournament_info['title']);

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
}

if(isset($user_id) && !empty($user_id)){
	//$sql = "select * from tournament_permissions where tournament_id = $tournament_id and member_id = $user_id";
	//$rs_permission = mysqli_query($conn,$sql);
	if($tournament_info['user_id'] == $user_id){
		$tournament_permission = 1;
	}else{
		$tournament_permission = 0;
	}
}

if(isset($_POST['submit_btn']) && !empty($_POST['submit_btn'])){
	validate();
	if(empty($error)){
		$sql = "UPDATE tournament_polls SET poll_question='".trim($_POST['poll_question'])."',tournament_id=".$tournament_id.", is_active=".$_POST['is_active'].", ";
		for($i=1;$i<=6;$i++){
			$sql.="poll_option_$i='".$_POST['poll_option_'.$i]."', ";
		}
		
		$sql = rtrim($sql,", ");
		$sql.=" WHERE id = $poll_id ";
		
		if(mysqli_query($conn,$sql)){
			$_SESSION['poll_updated'] = 1;
			header("Location:".WWW."tournament/polls/list/".$tournament_id);
			exit();
		}else{
			$error = '<p id="error">Error in updating Poll. Try again later</p>';
		}
	}
}

function validate(){
	global $error;
	if(empty($_POST['poll_question'])){
		$error.= '<p id="error">Poll Question is required field</p>';
	}
	
	if(empty($_POST['poll_option_1']) || empty($_POST['poll_option_2'])){
		$error.= '<p id="error">At least two poll options are required</p>';
	}
}

?>
<?php include('common/header.php'); ?>
<style>
.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
dl {min-height:145px;}
.content {width:1010px;}
.large-column {width:790px;}
.form-box {width:90%;}
.chk_left {float:left !important; }
.poll_data {width:410px;}
.form-box .text {width:75%;float:left;margin-left:40px;font-weight:normal;}
</style>
	<div class="middle">
		<h1> Edit Poll </h1>
		<h2><?php echo ucwords($tournament_info['title']); ?></h2>
		
		<div class="white-box content" id="dashboard">
			<?php if(empty($user_info)): ?>
				<div id="error">You are not logged... !</div>
			<?php endif; ?>
			
			<?php if(!empty($error)): ?>
				<?php echo $error; ?>
			<?php endif; ?>
		
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
				
				<?php if($tournament_permission){ ?>
				<form method="post"  >
					<input type="hidden" name="action" value="submit">
					<fieldset>
						<h2>Poll Details</h2>
						<p></p>
						<div class="form-box">
							<label>Poll Question</label>
							<div class="text" style="margin-left:15px;"><input type="text"  style="float:left;" class="poll_data input-login" name="poll_question" id="poll_question" class="input-login" value="<?php if(!empty($_POST['poll_question'])) echo $_POST['poll_question'];else echo $poll_info['poll_question']; ?>"></div>
						</div>
						<div class="clear"></div>
						
						<?php for($i=1;$i<=6;$i++): ?>
						<div class="form-box">
							<label>Option <?php echo $i; ?></label>
							<div class="text"><input type="text" style="float:left;" class="poll_data input-login"  name="poll_option_<?php echo $i; ?>" id="poll_option_<?php echo $i; ?>" class="input-login" value="<?php if(!empty($_POST['poll_option_'.$i])) echo $_POST['poll_option_'.$i];else echo $poll_info['poll_option_'.$i] ?>">
							&nbsp; <?php if(!empty($poll_info['poll_option_'.$i])): ?> <?php echo $poll_info['votes_'.$i];  ?> Votes<?php endif; ?>
							</div>
						</div>
						<div class="clear"></div>
						<?php endfor; ?>
						
						<div class="form-box" >
							<label>Status</label>
							<div class="text" style="float:left;margin-left:50px;">
							<select name="is_active" id="is_active" style="float:left;">
								<option value="1" <?php if($poll_info['is_active'] == 1) echo 'selected'; ?>>Active</option>
								<option value="0" <?php if($poll_info['is_active'] == 0) echo 'selected'; ?>>Inactive</option>
							</select>
							</div>
						</div>
						<div class="clear"></div>
						
						<div class="form-box">
							<input type="button" name="cancel_btn" value=" Cancel " class="submit-login" onclick="window.location.href='<?php echo WWW; ?>tournament/polls/list/<?php echo $tournament_id; ?>';">
							<input type="submit" name="submit_btn" value=" Submit " class="submit-login" >
						</div>
					</fieldset>
				</form>
				<?php }else{ ?>
				<div id="error">You do not have Permission for this Tournament... !</div>
				<?php } ?>
			</div>  
		    <div class="clear"></div>
			</div>
		</div>
		<!--<div class="rightbar"><?php //include('common/right-panel.php');?></div>-->
		<div class="clear"></div>
	</div>
	
<?php include('common/footer.php'); ?>