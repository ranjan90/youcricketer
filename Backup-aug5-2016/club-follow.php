<?php include_once('includes/configuration.php');
$page = 'club-follow.html';
$selected_country = getGeoLocationCountry(); 

$ppage = intval($_GET["page"]);
if($ppage<=0) $ppage = 1;


$user_info  = array();
$club_members = array();

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
	
}

if(isset($_POST['clubs_del_submit']) && !empty($_POST['clubs_del_submit'])){
	$companies = $_POST['company_ids'];
	if(!empty($companies)){
		for($i=0;$i<count($companies);$i++){
			$sql = "DELETE FROM companies_following WHERE user_id = $user_id and company_id=".$companies[$i] .' LIMIT 1';
			mysqli_query($conn,$sql);
		}
	}
	
	$_SESSION['club_deleted'] = 1;
	header("Location:".WWW.'club-follow-all-'.$ppage.'.html');
	exit;
	
}

if(isset($_SESSION['club_deleted']) && $_SESSION['club_deleted']==1) {
	$club_deleted = 1;
	unset($_SESSION['club_deleted']);
}

?>
<?php include('common/header.php'); ?>
<style>
.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
dl {min-height:145px;}
.content {width:1010px;}
.large-column {width:790px;}
</style>
	<div class="middle">
		<h1> Clubs I Follow</h1>
		
		<?php if(isset($club_deleted) && $club_deleted == 1): ?>
			<div id="information">Clubs Unfollowed Successfully... !</div>
		<?php  endif; ?>
		
		<?php if(empty($user_info)): ?>
			<div id="error">You are not logged... !</div>
		<?php endif; ?>
		
		
		
		<div class="white-box content" id="dashboard">
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
				<?php
						$rpp = PRODUCT_LIMIT_FRONT; // results per page
                		
      					$query = "select distinct u.*,c.company_name,c.years_in_business,c.id as company_id,company_permalink from users u inner join companies as c on u.id=c.user_id inner join companies_following as l on l.company_id=c.id where c.company_type_id = 4 and u.status = 1 and l.user_id=".$user_id;
						$query_count = "select count(*) as users_count from users u inner join companies as c on u.id=c.user_id inner join companies_following as l on l.company_id=c.id where c.company_type_id = 4 and u.status = 1 and l.user_id=".$user_id;
				      	//=======================================
					    if(isset($_GET['keywords']) && $_GET['keywords'] != 'Search Here' && $_GET['keywords'] != 'all' && $_GET['keywords'] != ''){
							$keywords = str_replace('_',' ',trim($_GET['keywords']));
							$where  .= " and (c.company_name like '%{$keywords}%' ) ";
						}else{
							$keywords = '';
							$where  = '';
						}
						
						$query.=$where;
						$query_count.=$where;
					    $query .= " order by u.id desc ";
				    
						$rs_count   = mysqli_query($conn,$query_count);
						$row_count  = mysqli_fetch_assoc($rs_count);
						$tcount = $row_count['users_count'];
					  
						
						$tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
						$count = 0;
						$start = ($ppage-1)* $rpp;
						$x = 0;
					  
						$query .= " LIMIT $start,$rpp ";
				?>
			</div>
			<div class="large-column">
			<div id="pagination-top"></div>
			<form method="post">
			<div class="list">

			<?php if(!empty($user_info) && $tcount): ?>	
				<div style="float: left; clear: both; padding: 10px;">
					<input type="submit" name="clubs_del_submit" id="clubs_del_submit" value="UnFollow Clubs"> 
				</div>
			<?php endif; ?>
			
                <ul id="individual" class="content1">
                	<?php  
						$rs   = mysqli_query($conn,$query);
						if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
							echo '<div id="information">No record found ... !</div>';
						}
          			 
						$i= 0 ;
						while($row 	= mysqli_fetch_assoc($rs)){
                		

                		$row_img = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row['id']."' and is_default = '1' "));
			  				$row_country= get_record_on_id('countries', $row['country_id']);	
			  				$location 	= $row_country['name'];
			  				$row_vid = mysqli_fetch_assoc(mysqli_query($conn,"select * from videos where entity_id = '".$row['id']."' and entity_type = 'users' and is_default = '1' and file_name != ''"));
		        		    ?>
		        		    
						<dl>
								<dt>
									
									<a href="<?=WWW?><?php echo $row['company_permalink']; ?>" title="<?=$row['company_name']; ?>"><img src="<?php echo WWW;?><?=($row_img && !empty($row_img['file_name']))?'users/'.$row['id'].'/photos/'.$row_img['file_name']:'images/no-photo.jpg'?>" width="120" height="128" /></a></dt>
								<dd><div class="details">
									<h3><?=truncate_string($row['company_name'],30)?></h3>
									<p><? echo $location;?><br />
									Club <br />
		
									
									<?php if(!empty($user_info)): ?>	
										<div style="float:left;">
											<input type="checkbox" name="company_ids[]" id="company_id_<?php echo $i; ?>" value="<?php echo $row['company_id']; ?>"> Unfollow
										</div>
									<?php endif; ?>
								
									</p>
									<a class="submit-login margin-top-5" href="<?=WWW?><?php echo $row['company_permalink']; ?>" title="<?=$row['company_name'].' - '.$site_title?>">View Profile</a></div>
									<div class="flag"><img title="<?=$row_country['name']?>" alt="<?=$row_country['name']?>" src="<?php echo WWW;?>countries/<?=$row_country['flag']?>"  width="25"/></div>
									<div class="video">
										<? 
										$video = $row_vid['file_name'];
											if(!empty($video)){
											if(preg_match('/<iframe(.*)<\/iframe>/', $row_vid['file_name'])){ 
														
												preg_match('/src="(.*?)"/',$row_vid['file_name'] , $src);
												$src = $src[1];
												$video = "<iframe width='205' height='130' frameborder='0' allowfullscreen src='$src'></iframe>";
														
											}else{	
												$filename = explode('.',$video);
												$filename1= $filename[0];
												$video = '<video width="220" height="130" controls>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mp4" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.ogg" type=\'video/ogg; codecs="theora, vorbis"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mov" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.webm" type=\'video/webm; codecs="vp8, vorbis"\'>
												</video>';
											}
										}else{
											$video = '<img src="'.WWW.'images/no-video.jpg" width="168" height="130">';
										}
										echo $video;?>									
									</div>
								</dd>
						</dl>
						<?php
                		
					      $i++;
					      $count++;
					      $x++;
					  } 
					  ?>
			</ul>
			</div><br/>
			<?php if(!empty($user_info) && $tcount): ?>	
				<div style="float: left; clear: both; padding: 10px;">
				<input type="submit" name="clubs_del_submit" id="clubs_del_submit" value="UnFollow Clubs"> 
			</div>
			<?php endif; ?>
			</form>
			<p>&nbsp;</p><p>&nbsp;</p>
			<div id="pagination-bottom">
				<? if($tcount != 0 && $tcount > PRODUCT_LIMIT_FRONT){ ?>
		      	<?php
		        	//$reload = 'club-members-search.html?';
					if(!empty($keywords)){
						$reload = "club-follow-{$keywords}.html?";
					}else{
						$reload = "club-follow-all.html?";
					}
		        	echo paginate_one($reload, $ppage, $tpages);
		      	?>
		      	<input type="hidden" name="pagination-page" value="club-follow.html">
		        <? } ?>    
		        <form id="list-search" method="post" action="">
		        	<input type="text" name="txtsearch" placeholder="Search Here" class="validate[required] input-login">
		        	<input type="submit" value="">
		        </form>
				<div class="clear"></div>
		        <script type="text/javascript">
		        $('form#list-search').submit(function(e){
					var parentId = $(this).parent().attr('id');
					var string = $('#'+parentId +' form input[name=txtsearch]').val();
					if(string != '' && string != 'Search Here'){
						string = string.replace(/[ ]+/g,'_');
						string = string.replace(/[^a-zA-Z0-9_]+/g,'').toLowerCase();
						if(string.length > 0){
							$('form#list-search').attr('action','<?=WWW;?>club-follow-' + string + '.html');
						}
					}
				});
		        </script>
		        <div class="clear"></div>
		    </div>  
		    <div class="clear"></div>
			</div>
		</div>
		<!--<div class="rightbar"><?php //include('common/right-panel.php');?></div>-->
		<div class="clear"></div>
	</div>
	
	<script>
$(document).ready(function(){
	
});
</script>

<?php include('common/footer.php'); ?>