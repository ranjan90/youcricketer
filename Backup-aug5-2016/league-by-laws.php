<?php include_once('includes/configuration.php');
$page = 'league-by-laws.html';
$selected_country = getGeoLocationCountry(); 


$league_info = array();
$error = '';

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
	if($user_info['is_company'] == 1){
		$sql = "select * from companies where user_id = $user_id and company_type_id IN(1,2,3,35) and status=1";
		$rs_company = mysqli_query($conn,$sql);
		if(mysqli_num_rows($rs_company)){
			$league_info = mysqli_fetch_assoc($rs_company);
		}
	}
}

if(isset($_GET['action']) && $_GET['action'] == 'delete'){
	$id = $_GET['id'];
	$arr1 = explode(',',$league_info['by_laws_file']);$arr2 = explode(',',$league_info['by_laws_file_orig']);
	for($i=0;$i<count($arr1);$i++){
		if($i == $id){
			unlink('by-laws-uploads/'.$arr1[$i]);
			continue;
		}
		$files1.=$arr1[$i].',';
		$files2.=$arr2[$i].',';
	}
	
	$files1 = rtrim($files1,',');
	$files2 = rtrim($files2,',');
	$sql = " UPDATE companies SET by_laws_file='$files1',by_laws_file_orig='$files2' WHERE user_id='".$user_id."'";
	mysqli_query($conn,$sql);
	$_SESSION['file_deleted'] = 1;
	header("Location:".WWW."league-by-laws.html");
	exit();
}

if(isset($_POST['submit_btn']) && !empty($_POST['submit_btn'])){
	
	if(empty($error)){
			
			for($i=0;$i<count($_FILES['by_laws_file']['name']);$i++){
				$targetFolder = 'by-laws-uploads'; 
				$tempFile = $_FILES['by_laws_file']['tmp_name'][$i];
				$targetPath = $targetFolder;
				$fileParts = pathinfo($_FILES['by_laws_file']['name'][$i]);
				$fileName = $fileParts['filename'].'_'.rand(99,9999).time().'.'.$fileParts['extension'];
				$targetFile = $targetPath . '/' .$fileName ;
				$file_orig = $fileParts['filename'].'.'.$fileParts['extension'];
				if(!move_uploaded_file($tempFile,$targetFile)){
					$error= 'Error in uploading File';
				}else{
					$file1.=$fileName.',';
					$file2.=$file_orig.',';
				}
			}
			
			$file1 = rtrim($file1,',');
			$file2 = rtrim($file2,',');
			if(!empty($league_info['by_laws_file'])){
				$file1 = $league_info['by_laws_file'].','.$file1;		
			}
			if(!empty($league_info['by_laws_file_orig'])){
				$file2 = $league_info['by_laws_file_orig'].','.$file2;		
			}
			
			$sql = " UPDATE companies SET by_laws_file =  '$file1',by_laws_file_orig = '$file2' ,tournament_data_added = 'yes' WHERE user_id='".$user_id."'";		
				
		
		
				if(empty($error) && mysqli_query($conn,$sql)){
					$_SESSION['file_uploaded'] = 1;
				}else{
					$error = '<p id="error">Error in adding File. Try again later</p>';
				}
			
			if(empty($error)){
				$_SESSION['file_uploaded'] = 1;
				header("Location:".WWW."league-by-laws.html");
				exit();
			}else{
				$error = '<p id="error">Error in adding File. Try again later</p>';
			}
	}
}

if(isset($_SESSION['file_uploaded']) && $_SESSION['file_uploaded'] ==1){
	$file_uploaded = 1;
	unset($_SESSION['file_uploaded']);
}
if(isset($_SESSION['file_deleted']) && $_SESSION['file_deleted'] ==1){
	$file_deleted = 1;
	unset($_SESSION['file_deleted']);
}

?>
<?php include('common/header.php'); ?>
<style>
.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
dl {min-height:145px;}
.content {width:1010px;}
.large-column {width:790px;}

</style>
	<div class="middle">
		<h1> By Laws Document  </h1>
		
		<div class="white-box content" id="dashboard">
			
			<?php if(!empty($error)): ?>
				<?php echo $error; ?>
			<?php endif; ?>
			
			<?php if(isset($file_uploaded) && $file_uploaded == 1): ?>
				<div id="information">File Uploaded Successfully... !</div>
			<?php endif; ?>
			
			<?php if(isset($file_deleted) && $file_deleted == 1): ?>
				<div id="information">File Deleted Successfully... !</div>
			<?php endif; ?>
		
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
			
			 <div class="clear"></div>
			 
			 		<form method="post"  enctype="multipart/form-data">
					
					<fieldset>
						<h2>By Laws Document</h2>
						
						<?php if(!empty($league_info)): ?>
						<div class="clear"></div>
						<?php if(!empty($league_info['by_laws_file'])): ?>
						<div class="form-box"  >
						<label>By Laws File</label>
						<div class="text" style="width:70%;">	
						<?php $arr1 = explode(',',$league_info['by_laws_file']);$arr2 = explode(',',$league_info['by_laws_file_orig']);
						for($i=0;$i<count($arr1);$i++): ?>
						<a style="background:none;padding:5px;" target="_blank" href="by-laws-uploads/<?php echo $arr1[$i]; ?>"><?php echo $arr2[$i]; ?></a>
						&nbsp;<a style="background:none;padding:5px;" href="league-by-laws.html?action=delete&id=<?php echo $i; ?>" title="Delete" onclick="return confirm('Are you sure to delete File');"><img src="images/icons/delete.png"></a>
						<br/>
						<?php endfor; ?>
								</div>
						</div>		
						<?php endif; ?>
							
						<div class="clear"></div>
						
						<div class="form-box" id="video-file-div" >
							<label>Upload File</label>
							
							<div class="text" style="float:left;margin-left:1px;width:72%;"><input multiple="true" accept=".doc,.docx,.pdf" type="file" style="float:left;margin-left:30px;" name="by_laws_file[]" id="by_laws_file"  value="">
							<br/><div style="font-weight:normal;clear:both;float:right;margin-top:10px;margin-right:40px;">
							Press Ctrl to upload multiple files.<br/>Allowed File Types: doc, docx, pdf </div></div>
						</div>
						
						<div class="clear"></div>
						<div class="form-box" style="float:left;margin-left:5px;">
							<input type="submit" name="submit_btn" id="submit_btn" value=" Submit " style="float:left;" class="submit-login" >
						</div>
						<?php endif; ?>
					</fieldset>
				</form>
			
			</div>  
		    <div class="clear"></div>
			</div>
		</div>
		<!--<div class="rightbar"><?php //include('common/right-panel.php');?></div>-->
		<div class="clear"></div>
	</div>
	
<script>
$(document).ready(function(){
	
	$("#submit_btn").bind('click',function(){
		
		if($("#by_laws_file").val() == ''){
			alert('By Laws File is Requied Field');
			return false;
			
		}
		if($("#by_laws_file").val() != ''){
			
			var file_size = $("#by_laws_file")[0].files[0].size/(1024*1024);
			if(file_size>10){
				alert('File size is greater than 10MB');
				return false;
			}
		
		
			var res_field = $("#by_laws_file").val();  
			var extension = res_field.substr(res_field.lastIndexOf('.') + 1).toLowerCase();
			var allowedExtensions = ['doc', 'docx', 'pdf'];
			if (res_field.length > 0)
			{
				if (allowedExtensions.indexOf(extension) === -1) 
				{
					alert('Invalid file Format. Only ' + allowedExtensions.join(', ') + ' are allowed.');
					return false;
				}
			}
	}	
		
		
		return true;
	});
});
</script>	


<?php include('common/footer.php'); ?>