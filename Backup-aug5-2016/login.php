<?php include('common/header.php'); ?>
<?php $site_key = '6LdcVRETAAAAAD91vVdDOQLo_bhqq-3MUyHBi2nS';
$secret_key = '6LdcVRETAAAAAJgLM7q9C6SqsXHWwt6B2ccePxHo';
?>

<? if(isset($_SESSION['ycdc_user_name']) 
	&& !empty($_SESSION['ycdc_user_name'])){ ?>
		<script>
		window.location = '<?=WWW?>dashboard.html';
		</script>
<?  }?>
	<div class="middle">
		<h1>Login Area </h1>
		<? if(isset($_GET['status']) && $_GET['status'] == 'verified'){ ?>
		<div id="success">Account Verified, Login Here to continue ... !</div>
		<? } ?>
		
		<? 	if(isset($_POST) && !empty($_POST)){
				$username 	= $_POST['email'];
				$password 	= $_POST['password'];
				$timezone 	= $_POST['timezone'];
				$date 		= date('Y-m-d H:i:s');
				$captcha_error = false;
	
				$rs_user	= mysqli_query($conn,"select * from users where email = '$username'");
				if(isset($_POST['captcha_added']) && !empty($_POST['captcha_added'])){
					if(isset($_POST['g-recaptcha-response'])){
						$captcha=$_POST['g-recaptcha-response'];
						$response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret_key&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
						if($response.success==false){
							$captcha_error = true;
							echo 'Invalid Human Verification';
						}
					}else{
						echo 'Invalid Human Verification';
						$captcha_error = true;
					}
				}
				
				if(mysqli_num_rows($rs_user) == 0 && !$captcha_error){
					echo '<div id="error">Invalid email address ... !</div><br><br>';	
				}elseif(!$captcha_error){
					$row_user = mysqli_fetch_assoc($rs_user);
				
					if($row_user['password'] == md5($password)){
						if($row_user['status'] == 1){
							mysqli_query($conn,"update users set login_attempts = '0' where id = '".$row_user['id']."'");
							$row_timezone 				= mysqli_fetch_assoc(mysqli_query($conn,"select * from timezones where title = '$timezone'"));
							if($row_user[timezone] != $row_timezone['id']){
								$query = "insert into activity (user_id, activity_date, activity, status) values 
								('".$row_user['id']."','$date','Logged in from ".$row_timezone['title']."','1');";
								mysqli_query($conn,$query);
							}
							$_SESSION['ycdc_dbuid']		= $row_user['id'];
							$_SESSION['ycdc_user_email']= $username;
							$_SESSION['ycdc_user_name'] = $row_user['f_name'].' '.$row_user['last_name'];
							$_SESSION['ycdc_desig_id'] 	= $row_user['user_type_id'];
							$_SESSION['timezone']       = $timezone;
							$_SESSION['is_company']		= $row_user['is_company'];
							if($row_user['is_company'] ==1){
								$sql = 'Select comp.id as company_id, comp.company_name, comp.company_type_id,co.name as country_name from companies as comp
								left join countries as co on comp.country_id=co.id
								WHERE comp.user_id='.$row_user['id'];
								$row_company = mysqli_fetch_assoc(mysqli_query($conn,$sql));
								if(!empty($row_company)){
									$_SESSION['company_id']	= $row_company['company_id'];
									$_SESSION['company_name'] = $row_company['company_name'];
									$_SESSION['company_type_id'] = $row_company['company_type_id'];
									$_SESSION['company_country_name'] = $row_company['country_name'];
								}
							}
							?>
							<script>
						        window.location = '<?php echo WWW?>dashboard.html';
							</script>
							<?
						}else{
							echo '<div id="error">Your account has been deactivated by Administrator or closed, Please use "Re-Open Account" option ... !</div><br><br>';	
						}
					}else{
						$attempts = $row_user['login_attempts'] + 1;
							mysqli_query($conn,"update users set login_attempts = '$attempts' where id = '".$row_user['id']."'");
							if($attempts == 4){
								/*echo '<div id="error">Your account has been locked due to incorrect login attempts ... !</div><br><br>';	
								mysqli_query($conn,"update users set status = '2' where id = '".$row_user['id']."'");
								//sending to user
								$email_template = get_record_on_id('cms', 18);
								$mail_title		= $email_template['title'];
				                $mail_content	= $email_template['content'];
				                $mail_content 	= str_replace('{to_name}',$row_user['f_name'], $mail_content);
				                $mail_content 	= str_replace('{login_link}','<a href="'.WWW.'login.html" title="Verify your account">HERE</a>', $mail_content);
				                $mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);
								$headers = "Content-type:text/html;charset=UTF-8" . "\r\n";
								$headers .= "From: no-reply@youcricketer.com" . "\r\n";
							
									
				                mail($row_user['email'],$mail_title,$mail_content,$headers);*/
								

							}else{
								//echo '<div id="error">Please enter correct password ... !</div><br><br>';			
							}
							
							echo '<div id="error">Please enter correct password ... !</div><br><br>';		
						
					}
				}
			}
		?>
		
		<div class="white-box content">
			<div style="width:60%; float:left;">
			<h2>Login with your Credential</h2>
			<form method="post" action="">
				<input type="hidden" name="timezone" value="">
				<fieldset>
					<div class="form-box" style="width:100%;">
						<label>Email Address </label>
						<input type="text" name="email" class="input-login validate[required, custom[email]]">
					</div>
					<div class="clear"></div>
					<div class="form-box" style="width:100%;">
						<label>Password </label>
						<input type="password" name="password" class="input-login validate[required]" >
					</div>
					
					<?php if(isset($attempts) && $attempts > 2): ?>
						<div class="form-box" style="width:100%;">
							<label>Human Verification </label>
							<script src="https://www.google.com/recaptcha/api.js"></script>
							<div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
							<input type="hidden" name="captcha_added" value="1">
						</div>
					<?php endif; ?>
					
					<div class="clear"></div>
					<div class="form-box" style="padding-top:10px; width:100%;">
						<a href="<?=WWW?>forgot-password.html">Forgot Password</a> | 
						<a href="<?=WWW?>verify-account.html">Verify Account</a> | 
						<a href="<?=WWW?>re-open-account.html">Re-Open Account</a>
					</div>
					<div class="form-box" style="width:100%;">
						<input type="submit" value=" Submit " class="submit-login" style="float:left;">
					</div>
				</fieldset>
			</form>
			</div>
			<div style="width:38%; float:right; border-left:1px solid #ccc; padding-left:5px;">
				<?php include('common/social-login.php');?> 
			</div>
			<div class="clear"></div>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>
<script src="<?php echo WWW?>js/jquery-1.8.0.js" type="text/javascript"></script>
<script>
	$(document).ready(function(){
	if($('input[name=timezone]')){
		var timezone = jstz.determine();
		$('input[name=timezone]').val(timezone.name());	
	}	
	});
</script>
<style type="text/css">.g-recaptcha{float:right;}</style>
<?php include('common/footer.php'); ?>