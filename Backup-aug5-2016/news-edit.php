<?php include_once('includes/configuration.php');
if(!isset($_SESSION['ycdc_user_email']) || empty($_SESSION['ycdc_user_email'])){
	header("Location:login.html");
	exit();
}

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

$page = 'company-news-edit.html';
$page_title = 'Edit News';
$user_id 	= $_SESSION['ycdc_dbuid'];
$error = '';

$id = trim($_GET['id']);
$news_info = get_record_on_id('company_news', $id);	

if($news_info['user_id'] != $user_id){
	header("Location:company-news-list.html");
	exit();
}

if(isset($_POST['submit_news_btn']) && !empty($_POST['submit_news_btn'])){
	extract($_POST);
	validate();
	
	$sql = "UPDATE company_news SET title = '".mysqli_real_escape_string($conn,$title)."',news = '".mysqli_real_escape_string($conn,$news)."'";
	
	if(empty($error) && isset($_FILES['picture']['name']) && !empty($_FILES['picture']['name'])){
		$targetFolder = 'images/uploads/news'; 
		$tempFile = $_FILES['picture']['tmp_name'];
		$targetPath = $targetFolder;
		$fileParts = pathinfo($_FILES['picture']['name']);
		$fileName = rand(99,9999).time().'.'.$fileParts['extension'];
		$targetFile = $targetPath . '/' .$fileName ;
						
		// Validate the file type
		$fileTypes = array('jpg','jpeg','png','gif','bmp'); 
		if (in_array($fileParts['extension'],$fileTypes)) {
			if(!move_uploaded_file($tempFile,$targetFile)){
				$error = 'Error in Uploading Picture';
			}
		}else{
			$error = 'Invalid Picture Type';
		}
		
		if(empty($error)){
			$sql.=", picture ='".$fileName."'";	
		}
	}
	
	$sql.=" WHERE id = $id ";	
	
	if(empty($error)){
		if(!mysqli_query($conn,$sql)){
			$error = 'Error in Updating News. Try Again Later';
		}else{
			$_SESSION['news_updated']=1;
			header("Location:company-news-list.html");
			exit();
		}
	}
}

function validate(){
	global $error;
	
	if(empty($_POST['title'])){
		$error.= '<p id="error">Title is required field</p>';
	}
	if(empty($_POST['news'])){
		$error.= '<p id="error">News Content is required field</p>';
	}
}	

include('common/header.php'); ?>


	<div class="middle">
		<h1>Edit Company News</h1>
		
		<div class="white-box content" id="dashboard">
			<?php if(!empty($error)): ?>
				<?php echo $error; ?>
			<?php endif; ?>
			
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
			
				
				<form method="post"  enctype="multipart/form-data">
					<input type="hidden" name="action" value="submit">
					<fieldset>
						<h2>News Details</h2>
						<p>All Fields are Required</p>
						<div class="form-box">
							<label>Title</label>
							<div class="text"><input type="text" name="title" id="title" class="input-login" style="width:365px;" value="<?php if(!empty($news_info['title'])) echo stripslashes($news_info['title']); ?>"></div>
						</div>
						<div class="clear"></div>
						
						
						<div class="form-box">
							<label>Content</label>
							<div class="text"><textarea name="news" id="news" style="float:right;height:160px;width:365px;" ><?php if(!empty($news_info['news'])) echo stripslashes($news_info['news']); ?></textarea></div>
						</div>
						
						<div class="clear"></div>
						
						<div class="form-box">
							<label>Picture</label>
							<div class="text" style="width:80%;">
							<?php if(!empty($news_info['picture'])): ?>
								<p><img src="images/uploads/news/<?php echo $news_info['picture']; ?>" width="100"></p>
							<?php endif; ?>
							<input type="file" name="picture" id="picture" style="float:left;" accept="image/*" ></div>
						</div>
						<div class="clear"></div>
						

						<div class="form-box">
							<input type="button" name="cancel_btn" value=" Cancel " class="submit-login" onclick="window.location.href='<?php echo WWW; ?>company-news-list.html';">
							<input type="submit" name="submit_news_btn" value=" Submit " class="submit-login" >
						</div>
					</fieldset>
				</form>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>


<script src="<?php echo WWW?>js/jquery-1.8.0.js" type="text/javascript"></script>
<script>
$(document).ready(function(){
	
});
</script>
<style>
#table-list {width:100%;border:1px solid #ccc;}
#table-list tr{border:1px solid #ccc;}
#table-list th{padding:5px;background-color:#000;color:#FB7008;font-size:14px;}
#table-list td{padding:5px;text-align:center;}
#table-list a{color:#000;}
.add_news{font-size:14px;color:#000;}
.form-box{width:60%;}
</style>
<?php include('common/footer.php'); ?>