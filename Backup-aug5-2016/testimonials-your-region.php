<?php include('common/header.php'); 
$selected_country = getGeoLocationCountry();
	
?>
	<div class="middle">
		<h1>
			YouCricketer Testimonials In Your Region - <?php echo $selected_country;?></h1>			
		</h1>
		<div class="white-box content">
		<? include('common/login-box.php');?>	
			<style>
			#login-box{display:none;}
			</style>
			
			<div id="pagination-top"></div>
			<div class="list">
                <ul>
                	<?  $rpp = PRODUCT_LIMIT_FRONT; // results per page
            			$ppage = intval($_GET["page"]);
      					if($ppage<=0) $ppage = 1;
      					$query = "select * from testimonials as t  ";
						$row_country = mysqli_fetch_assoc(mysqli_query($conn,"select * from countries where name = '$selected_country'"));
							$query .= " join users u on u.id = t.user_id and (u.country_id = '".$row_country['id']."'  ) ";
				      	//=======================================
						$query  .= " where t.status=1";
					    if(isset($_GET['keywords']) && $_GET['keywords'] != 'Search Here' && $_GET['keywords'] != 'all'){
					        $query  .= "and content like '%".str_replace('-','%',$_GET['keywords'])."%'";
							$keywords = trim($_GET['keywords']);
					    }else{
							$keywords = '';
						}
					    if(isset($_GET['type']) && $_GET['type'] == 'my'){
					    	$query  .= "and user_id = '".$_SESSION['ycdc_dbuid']."'";	
					    }
					    $query .= " order by t.id desc ";
				      //echo $query;
				      //=======================================
				      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
				        echo '<div id="information">No record ...!</div>';
				      }
				      $rs   = mysqli_query($conn,$query);
				      $tcount = mysqli_num_rows($rs);
				      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
				      $count = 0;
				      $i = ($ppage-1)* $rpp;
          			  $x = 0;
          			  //=======================================
      				  while(($count<$rpp) && ($i<$tcount)){
                		mysqli_data_seek($rs,$i);
                		$row_t 	= mysqli_fetch_array($rs);
                		$row_i  = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_t['user_id']."' and is_default = '1'"));
                		?>
      					<div class="testi-box">
						<h2 style="font-size: 18px;">
							<img style="margin:4px;" src="<?=WWW?><?=($row_i)?'users/'.$row_t['user_id'].'/photos/'.$row_i['file_name']:'images/no-photo.jpg';?>" alt="<?=get_combo('users','concat(f_name," ",last_name)',$row_t['user_id'],'','text')?>" width="40">
							<?=get_combo('users','concat(f_name," ",last_name)',$row_t['user_id'],'','text')?> for <?=get_combo('users','concat(f_name," ",last_name)',$row_t['entity_id'],'','text')?> on <?=date_converter($row_t['testimonial_date'],'M d,Y H:i')?>
							<div class="stars">
								<? if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid']) && $row_t['user_id'] == $_SESSION['ycdc_dbuid']){ ?>
	                            <a href="<?=WWW?>edit-testimonial-<?=$row_t['id']?>.html"><img src="<?=WWW?>images/icons/edit.png"></a>
	                            <a href="<?=WWW?>delete-testimonial-<?=$row_t['id']?>.html"><img src="<?=WWW?>images/icons/delete.png"></a>
	                            <? } ?>
							<? 	$rating = $row_t['rating']/2;
								for($x = 0; $x < 5; $x++){
									if($x < $rating){
									?><img src="<?=WWW?>images/star-fill-gray.jpg" title="<?=$rating?>/5"><?
									}else{
									?><img src="<?=WWW?>images/star-gray.jpg" title="<?=$rating?>/5"><?
									}
								}
							?>
							</div>
						</h2>
						<p><?=$row_t['content']?></p>
					</div>
					    <?
					      $i++;
					      $count++;
					      $x++;
					  } 
					  ?>
			</ul>
			</div>
			<div id="pagination-bottom">
				<? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_FRONT){ ?>
		      	<?php
		        	//$reload = 'testimonials.html?';
					if(!empty($keywords)){
							$reload = "testimonials-{$keywords}.html?";
						}else{
							$reload = "your-region-testimonials.html?";
					}
		        	echo paginate_one($reload, $ppage, $tpages);
		      	?>
		      	<input type="hidden" name="pagination-page" value="testimonials.html">
		        <? } ?>    
		        <form id="list-search" method="post" action="">
		        	<input type="text" name="txtsearch" placeholder="Search Here" class="validate[required] input-login">
		        	<input type="submit" value="">
		        </form>
		        <script type="text/javascript">
		        $('form#list-search').submit(function(e){
					var parentId = $(this).parent().attr('id');
					var string = $('#'+parentId +' form input[name=txtsearch]').val();
					if(string != '' && string != 'Search Here'){
						string = string.replace(/[^a-zA-Z0-9]+/g,'-').toLowerCase();
						if(string.length > 0){
							$('form#list-search').attr('action','<?=WWW;?>testimonials-' + string + '.html');
						}
					}
				});
		        </script>
		        <div class="clear"></div>
		        <div class="add" style="<?=(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_FRONT)?'':'top:-27px !important';?>">
					<a href="<?=(!isset($_SESSION['ycdc_user_name']) || empty($_SESSION['ycdc_user_name']))?'#login':WWW.'add-testimonial.html';?>" class="popup-link right submit-login no-shaddow" >Create Testi</a>
				</div>
		    </div>  
		    <div class="clear"></div>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>
<? //include('common/login-box.php');?>	
<style>
#login-box{display:none;}
</style>
<?php include('common/footer.php'); ?>