<?php include('common/header.php'); ?>
<style>
.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
<?php if(isset($_GET['entity']) && $_GET['entity'] == 'clubs'): ?>
dl {min-height: 170px;}
<?php endif; ?>
</style>
<?php
		$selected_country = getGeoLocationCountry();
?>
	<?php 	$pageTitle = $tagLine = '';

			if(isset($_GET['entity'])){
				$pageTitle = $_GET['entity'];
				if($_GET['entity'] == 'other'){
					$pageTitle .= ' Companies';
					$tagLine   = ' - <span style="font-size:14px;">(Except Clubs and Leagues)</span>';
				}else{
					$tagLine   = ' - <span style="font-size:14px;">('.$_GET['entity'].' are also considered as Companies)</span>';
				}
				
			}else{
				$pageTitle = 'Companies';
			}
			if(isset($_GET['type']) && $_GET['type'] == 'your_region'){
				$pageTitle .= ' In Your Region - '.$selected_country;
			}
			$pageTitle .= $tagLine;
	?>
	<div class="middle">
		<h1> <?=$pageTitle;?></h1>
		<div class="white-box content" id="companies">
			<div id="pagination-top"></div>
			<div class="list">
                <ul id="individual" class="content1">
                	<?  $rpp = PRODUCT_LIMIT_FRONT; // results per page
                		$ppage = intval($_GET["page"]);
      					if($ppage<=0) $ppage = 1;
      					$select = "select c.*,co.name as country_name from companies c left join countries as co on c.country_id=co.id ";
      					$where  = " where c.status = 1";
				      	//=======================================
					    if(isset($_GET['keywords']) && $_GET['keywords'] != 'Search Here' && $_GET['keywords'] != 'all'){
					        $where  .= " and c.company_name like '%".str_replace('-','%',$_GET['keywords'])."%'";
							$keywords = trim($_GET['keywords']);
					    }else{
							$keywords = '';
						}
					    if(isset($_GET['type']) && $_GET['type'] == 'your_region'){
					    	
							$row_country = mysqli_fetch_assoc(mysqli_query($conn,"select * from countries where name = '$selected_country'"));
							$where .= " and c.country_id = '".$row_country['id']."'";
					    }
					    if(isset($_GET['entity']) && $_GET['entity'] == 'other'){
					    	$where .= " and company_type_id not in (4, 2) ";
					    }
					    if(isset($_GET['entity']) && $_GET['entity'] == 'clubs'){
					    	$where .= " and company_type_id = '4'";
					    }
					    if(isset($_GET['entity']) && $_GET['entity'] == 'leagues'){
					    	$where .= " and company_type_id = '2'";
					    }
					    
					    $query = $select.$join.$where ." order by c.id desc ";
				      //echo $query;
				      //=======================================
				      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
				        echo '<div id="information">No record found ... !</div>';
				      }
				      $rs   = mysqli_query($conn,$query);
				      $tcount = mysqli_num_rows($rs);
				      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
				      $count = 0;
				      $i = ($ppage-1)* $rpp;
          			  $x = 0;
          			  //=======================================
      				  while(($count<$rpp) && ($i<$tcount)){
                		mysqli_data_seek($rs,$i);
                		$row_g 	= mysqli_fetch_assoc($rs);
                		$row_u   = mysqli_fetch_assoc(mysqli_query($conn,"select * from users where id = '".$row_g['user_id']."'"));
                		$row_img = mysqli_fetch_assoc(mysqli_query($conn,"select id,file_name from photos where entity_type = 'users' and entity_id = '".$row_g['user_id']."' and is_default = '1' "));
			  			$row_vid = mysqli_fetch_assoc(mysqli_query($conn,"select id,file_name from videos where entity_type = 'users' and entity_id = '".$row_g['user_id']."' and is_default = '1' "));
						
						if(isset($_GET['entity']) && $_GET['entity'] == 'clubs'){
							$row_club_members   = mysqli_fetch_assoc(mysqli_query($conn,"select count(*) as members_count from club_members where club_id = '".$row_g['id']."'"));
						}
			  			
						if(!empty($row_u['country_id'])){
			  				$row_city   = $row_u['city_name'];
				           	$row_states = get_record_on_id('states', $row_u['state_id']);
				           	$row_country= get_record_on_id('countries', $row_g['country_id']);	
				           	$location   =  $row_city.' > '.$row_states['name'].' > '.$row_country['name'];
			  			}else{
			  				$location 	= 'Not Given';
			  			}
      					?>
      					<li><dl>
								<dt>
							<a href="<?=WWW?>cricket-club/<?php echo getSlug($row_g['country_name']).'-'.getSlug($row_g['company_name']); ?>/<?php echo $row_g['id']; ?>" title="Read more" />
								<img src="<?php echo WWW;?><?=($row_img && !empty($row_img['file_name']))?'users/'.$row_g['user_id'].'/photos/'.$row_img['file_name']:'images/no-photo.png'?>" width="120" height="128" />
							</a>
							</dt>
								<dd>
	                               <div class="details">
									<h3><?=$row_g['company_name']?></h3>
									<p> <?=get_combo('company_types','name',$row_g['company_type_id'],'','text')?></p>
									<p><b>Years in business </b>: <?=$row_g['years_in_business']?></p>
									<?php if(isset($_GET['entity']) && $_GET['entity'] == 'clubs'){ ?>
										<p><b>Members Count </b>: <?=$row_club_members['members_count']?></p>
									<?php } ?>
									<p><? echo $location;?></p>
									
										<a class="submit-login margin-top-5" href="<?=WWW?>cricket-club/<?php echo getSlug($row_g['country_name']).'-'.getSlug($row_g['company_name']); ?>/<?php echo $row_g['id']; ?>" title="<?=$row_g['company_name'].' - '.$site_title?>">View Profile</a>
								
								</div>
	                        
	                        
	                        	<div class="flag">
	                        	<img title="<?=$row_country['name']?>" src="<?php echo WWW;?>countries/<?=$row_country['flag']?>"  width="25"/>
	                        	</div>
									<div class="video">
										<? 
											$video = $row_vid['file_name']; 

  									
                              
										if(!empty($video)){
											if(preg_match('/<iframe(.*)><\/iframe>/', $video)){
							        			if(!preg_match('/height="(.*)"/', $video)){
													$video = preg_replace('/height="(.*)"/','height="130"',$video);	
												}else{
													$video = preg_replace('/width="(.*)"/','width="220" height="130"',$video);	
												}
											}else{
											$video_path = WWW.'videos/'.$row_vid['id'].'/'.$row_vid['file_name']; 
												$video = '<video width="220" height="130" controls>
												  <source src="'.$video_path.'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
									   			  <source src="'.$video_path.'" type=\'video/ogg; codecs="theora, vorbis"\'>
									 			  <source src="'.$video_path.'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
									 			  <source src="'.$video_path.'" type=\'video/webm; codecs="vp8, vorbis"\'>
											</video>';
											}
										}else{
											$video = '<img src="'.WWW.'images/no-video.jpg" width="220" height="130">';
										}
										echo $video; ?>
								</div>
								</dd>
						</dl>
						</li>
						<div class="clear"></div>
					    <?
					      $i++;
					      $count++;
					      $x++;
					  } 
					  ?>
			</ul>
			</div>
			<div id="pagination-bottom">
				<? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_FRONT){ ?>
		      	<?php
					if(isset($_GET['entity']) && $_GET['entity'] == 'other'){
						$p_name = 'other-companies';
					}
					elseif(isset($_GET['entity']) && $_GET['entity'] == 'clubs'){
					    $p_name = 'clubs';
					}
					elseif(isset($_GET['entity']) && $_GET['entity'] == 'leagues'){
					    $p_name = 'leagues';
					}else{
						$p_name = 'companies';
					}
		        	
					//$reload = 'individuals.html?';
					if(isset($_GET['type']) && $_GET['type'] == 'your_region'){
						$reload = "your-region-{$p_name}.html?";
					}else{
						
						if(!empty($keywords)){
							$reload = "{$p_name}-{$keywords}.html?";
						}else{
							$reload = "{$p_name}-all.html?";
						}
					}
		        	echo paginate_one($reload, $ppage, $tpages);
		      	?>
		      	<input type="hidden" name="pagination-page" value="individuals.html">
		        <? } ?>    
		        <form id="list-search" method="post" action="">
		        	<input type="text" name="txtsearch" placeholder="Search Here" class="validate[required] input-login">
		        	<input type="submit" value="">
		        </form>
		        <script type="text/javascript">
		        $('form#list-search').submit(function(e){
					var string = $('input[name=txtsearch]').val();
					if(string != '' && string != 'Search Here'){
						string = string.replace(/[^a-zA-Z0-9]+/g,'-').toLowerCase();
						if(string.length > 0){
							$('form#list-search').attr('action','<?=WWW;?><?php echo $p_name; ?>-' + string + '.html');
						}
					}
				});
		        </script>
		        <div class="clear"></div>
		    </div>  
		    <div class="clear"></div>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>
<?php include('common/footer.php'); ?>