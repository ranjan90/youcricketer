<?php include('common/header.php'); ?>
	<div class="middle">
		<h1>YouCricketer Events </h1>
		<div class="white-box content" id="dashboard">
			<div class="small-column">
				<? include('common/user-left-panel.php');

				$u_id = $_SESSION['ycdc_dbuid'];
						?>
			</div>
			<div class="large-column">
			<div id="pagination-top"></div>
			<div class="list">
                <ul>
                	<?  $rpp = PRODUCT_LIMIT_FRONT; // results per page
            			$ppage = intval($_GET["page"]);
      					if($ppage<=0) $ppage = 1;
      					$select = "select * from events e ";
      					$where = " where e.user_id = '$u_id' AND e.status=1 ";
				      	//=======================================
					    if(isset($_GET['keywords']) && $_GET['keywords'] != 'Search Here'){
					        $where  .= "and e.title like '%".str_replace('-','%',$_GET['keywords'])."%'";
					    }
					    if(isset($_GET['type']) && $_GET['type'] == 'my'){
					    	$where .= " and e.user_id = '".$_SESSION['ycdc_dbuid']."'";
					    }
					    if(isset($_GET['type']) && $_GET['type'] == 'your_region'){
					    	$ipData 	= file_get_contents('http://api.hostip.info/get_html.php?ip='.$user_ip);
							preg_match('/Country: (.*) /',$ipData, $expression);
							$selected_country = trim($expression[1]);
							$row_country = mysqli_fetch_assoc(mysqli_query($conn,"select * from countries where name = '$selected_country'"));
							$join .= " join users u on u.id = e.user_id and u.country_id = '".$row_country['id']."'";
					    }
					    $query = $select.$join.$where." order by e.id desc ";
				     //echo $query;
				      //=======================================
				      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
				        echo '<div id="error">No Events Found !</div>';
				      }
				      $rs   = mysqli_query($conn,$query);
				      $tcount = mysqli_num_rows($rs);
				      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
				      $count = 0;
				      $i = ($ppage-1)* $rpp;
          			  $x = 0;
          			  //=======================================
      				  while(($count<$rpp) && ($i<$tcount)){
                		mysqli_data_seek($rs,$i);
                		$row_g 	= mysqli_fetch_assoc($rs);
                		$row_img= mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'events' and entity_id = '".$row_g['id']."'"));
                		$row_u  = get_record_on_id('users', $row_g['user_id']);
      					?>
      					<li>
							<a href="<?=WWW?>event-detail-<?=$row_g['id']?>-<?=friendlyURL($row_g['title'])?>.html" title="Read more" />
								<img src="<?=($row_img)?WWW.'events/'.$row_g['id'].'/'.$row_img['file_name']:WWW.'images/community-groups.png';?>" alt="<?=$row_g['title']?>" title="<?=$row_g['title']?>" width="114" />
							</a>
	                        <span class="list-text">
	                            <h3 onclick="window.location='<?=WWW?>event-detail-<?=$row_g['id']?>-<?=friendlyURL($row_g['title'])?>.html'"><?=$row_g['title']?></h3>
	                            <p><b>Start Date</b> : <?=date_converter($row_g['start_date'])?>
	                            	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	                            	<b>End Date</b> : <?=date_converter($row_g['end_date'])?>
	                            </p>
	                            <p><b>Location : </b><?=$row_g['city_name']?> > <?=get_combo('states','name',$row_g['state_id'],'','text')?> > <?=get_combo('countries','name',$row_u['country_id'],'','text')?></p>
	                            <a href="<?=WWW?>event-detail-<?=$row_g['id']?>-<?=friendlyURL($row_g['title'])?>.html" title="Read more" class="submit-login" />Read More</a>
	                              <a href="<?=WWW?>edit-event-<?=$row_g['id']?>.html"><img src="<?=WWW?>images/icons/edit.png"></a>
	                            <a href="<?=WWW?>delete-event-<?=$row_g['id']?>.html"><img src="<?=WWW?>images/icons/delete.png"></a>
	                       
	                      
	                         	<!-- AddThis Button BEGIN -->
						        <div class="addthis_toolbox addthis_default_style ">
						        <a class="addthis_button_preferred_1"></a>
						        <a class="addthis_button_preferred_2"></a>
						        <a class="addthis_button_preferred_3"></a>
						        <a class="addthis_button_preferred_4"></a>
						        <a class="addthis_button_compact"></a>
						        <a class="addthis_counter addthis_bubble_style"></a>
						        </div>
						        <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
						        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5073efa669740139"></script>
						        <!-- AddThis Button END -->
	                         	
	                            
	                          
	                           
	                        </span> 
						</li>
					    <?
					      $i++;
					      $count++;
					      $x++;
					  } 
					  ?>
			</ul>
			</div> </div>
			<div id="pagination-bottom">
				<? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_FRONT){ ?>
		      	<?php
		        	$reload = 'events.html?';
		        	echo paginate_one($reload, $ppage, $tpages);
		      	?>
		      	<input type="hidden" name="pagination-page" value="events.html">
		        <? } ?>    
		        <form id="list-search" method="post" action="">
		        	<input type="text" name="txtsearch" placeholder="Search Here" class="validate[required] input-login">
		        	<input type="submit" value="">
		        </form>
		        <script type="text/javascript">
		        $('form#list-search').submit(function(e){
					var parentId = $(this).parent().attr('id');
					var string = $('#'+parentId +' form input[name=txtsearch]').val();
					if(string != '' && string != 'Search Here'){
						string = string.replace(/[^a-zA-Z0-9]+/g,'-').toLowerCase();
						if(string.length > 0){
							$('form#list-search').attr('action','<?=WWW;?>events-' + string + '.html');
						}
					}
				});
		        </script>
		        <div class="clear"></div>
		        <div class="add" style="<?=(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_FRONT)?'':'top:-27px !important';?>">
					<a href="<?=(!isset($_SESSION['ycdc_user_name']) || empty($_SESSION['ycdc_user_name']))?'#login':WWW.'add-event.html';?>" class="popup-link right submit-login no-shaddow" >Create Event</a>
				</div>
		    </div>  
		    <div class="clear"></div>
		</div>
		
		<div class="clear"></div>
	</div>
<? include('common/login-box.php');?>	
<style>
#login-box{display:none;}
#pagination-bottom {float:right;border-top:none;}
#pagination-top {border-bottom:none;}
</style>
<?php include('common/footer.php'); ?>