<?
//error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING );
 include('common/header.php');?>
<style>
p{margin:0;}
.company-img{overflow: hidden; height:177px; width:280px; margin:5px; padding:5px;}
.company-info{width:375px;}
.company-video{width:300px; height:200px; float:right;}
dd {width: 650px;}
.details {width: 420px;}
.details a {left: 330px;}

.table-list {width:100%;border:1px solid #ccc;}
.table-list tr{border:1px solid #ccc;}
.table-list th{padding:5px;background-color:#473E3D;color:#FB7008;font-size:14px;}
.table-list td{padding:5px;text-align:center;}
.table-list a{color:#000;}
.statistics-filters{float:left;margin:6px 15px 0px 0px;}
#statistics-data{float:left;margin:10px 0px 0px 0px;width:100%;}
.white-box{float:left;width:100%;}
</style>
<?php $permalink = $_GET['permalink'];
$sql = "Select id from companies WHERE company_permalink = '$permalink' ";
$row_permalink = mysqli_fetch_assoc(mysqli_query($conn,$sql));
 ?>
<? $id = $row_permalink['id']; ?>
<? $row= get_record_on_id('companies', $id); ?>
<? $row_u= get_record_on_id('users',$row['user_id']);?>
<? $row_i= mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_id = '".$row_u['id']."' and entity_type = 'users' and is_default = '1'"));?>
<? $row_v= mysqli_fetch_assoc(mysqli_query($conn,"select * from videos where entity_id = '".$row_u['id']."' and entity_type = 'users' and is_default = '1'"));?>
<? $row_c= get_record_on_id('countries',$row['country_id']);?>
<? $row_ct= get_record_on_id('company_types',$row['company_type_id'])?>
<? $fields = explode(',', $row_u['privacy_settings']); ?>
<?php
if($row['company_type_id'] == 4){
	$row_club_members   = mysqli_fetch_assoc(mysqli_query($conn,"select count(*) as members_count from club_members as c inner join users as u on c.member_id=u.id where c.club_id = '".$row['id']."' AND u.status = '1'"));
}
?>

<style>
p{margin:0;}
<?php
if($row['company_type_id'] == 4){ ?>
	.menu3 li a{padding:0 26px;}
<?php } elseif(in_array($row['company_type_id'],array(1,2,3,35))){ ?>
	.menu3 li a{padding:0 5px;font-size:14px;}
<?php }else{ ?>
	.menu3 li a{padding:0 42px;}
<?php } ?>
</style>

<? 	if(isset($_POST) && !empty($_POST)){
				$subject 	= $_POST['subject'];
				$message 	= $_POST['comments'];
				$from_user  = $_SESSION['ycdc_dbuid'];
				$to_user 	= $row['user_id'];
				$date 		= date('Y-m-d H:i:s');

				if(!empty($from_user)){
					$rs_chk = mysqli_query($conn,"select * from messages where from_user_id = '$from_user' and to_user_id = '$to_user' and subject = '$subject' and message = '$message'");
					if(mysqli_num_rows($rs_chk) == 0){
						$query = "insert into messages (from_user_id, to_user_id, subject, message, status, message_date) 
						values ('$from_user','$to_user','$subject','$message','0','$date')";
						mysqli_query($conn,$query);
						echo '<div id="success">Message sent ... !</div>';
					}
				}
			}
		?>
<div class="clear" style="height:5px;"></div>		
<ul id="menu7" class="menu3">
	<li class="active"><a class="activity-link"  href="#activity">Activity</a></li>
	<li><a href="#photos">Photos</a></li>
	<li><a href="#videos">Videos</a></li>
	<li><a href="#traits">Traits</a></li>
    <li><a href="#management">Management</a></li>
    <?php  if($row['company_type_id'] == 4): ?>
	<li><a href="#club_members">Club Members</a></li>
	<?php endif; ?>
	<li><a href="#news">News</a></li>
	<?php  if(in_array($row['company_type_id'],array(1,2,3,31,35)) ): ?>
		<li><a href="#by_laws" class="by_laws_link">By Laws</a></li>
		<li><a class="clubs_link" href="#clubs">Clubs</a></li>
		<li><a class="venues_link" href="#venues">Venues</a></li>
		<li><a class="umpires_link" href="#umpires">Umpires</a></li>
		<li><a href="#weekly_shots" class="weekly_shots_link">Weekly Snap</a></li>
		<li><a href="#schedule_scorebook" class="schedule_scorebook_link">Schedule, Scorebook, Standings & Polls</a></li>
		<li ><a class="stats-link" href="#statistics">Statistics</a></li>
	<?php endif; ?>
	
	<?php if($row['company_type_id'] == 4): ?>
		<li ><a class="club-stats-link" href="#club-statistics">Statistics</a></li>
	<?php endif; ?>
	
	<li><a href="#contact">Contact</a></li>
</ul>
<div class="clear" style="height:10px;"></div>

<div class="company-details"> 

			<div class="company-img" style="height: 250px;"><img src="<?=WWW?><?=($row_i)?'users/'.$row_u['id'].'/photos/'.$row_i['file_name']:'images/no-photo.png'?>" alt="<?=$row['company_name']?>" width="280"height="250"></div>
			<div class="company-info">
				<div class="country"><img src="<?=WWW?>countries/<?=$row_c['flag']?>" width="50"></div>
				<h1><?=$row['company_name']?></h1>
				<dl>
                	<dt>Location :</dt>
                    <dd>
                    	<? 	$row_country = get_record_on_id('countries', $row['country_id']); 
                    		$row_state 	= get_record_on_id('states', $row_u['state_id']);
                    	?>
						<span><?=$row_u['city_name'];?></span> 
						<span><?=$row_state['name'];?></span>  
						<span><?=$row_country['name'];?></span>
                    </dd>
                </dl>
				<dl>
                	<dt>Contact Person : </dt>
                    <dd><?=$row_u['f_name'].' '.$row_u['m_name'].' '.$row_u['last_name']?></dd>
                </dl>
                <dl>
                	<dt>Company Type : </dt>
                    <dd><?=get_combo('company_types','name',$row['company_type_id'],'','text')?></dd>
                </dl>
				<?php if($row['company_type_id'] == 4): ?>
				<dl>
                	<dt style="width:33%;">Members Count : </dt>
                    <dd><?php echo $row_club_members['members_count'];  ?></dd>
				</dl>
				<?php endif; ?>
				<dl>
					<dt>Favorited By: </dt>
					<dd><?php	$fav = mysqli_query($conn,"SELECT * FROM favorite_users WHERE player_id = '$id'");
					$count = mysqli_num_rows($fav);
					echo $count;?></dd>
				</dl>
                <dl>
                	<dt>Registered On : </dt>
                    <dd><?=date_converter($row_u['create_date'])?></dd>
                </dl>
                <? 
                /*if(in_array($row['company_type_id'], array('1', '2', '4', '35'))){ ?>
                <dl>
                	<dt>Preffered Game Type : </dt>
                	<dd><?=$row_u['game_types']?></dd>
                </dl>
                <dl>
                	<dt>Preffered Match Type : </dt>
                	<dd><?=$row_u['match_types']?></dd>
                </dl>
                <? }
                */ ?>
                <br><br>
                <? if(isset($_SESSION['ycdc_dbuid'])){ ?>
                <div style="float:left;">
                	<!-- AddThis Button BEGIN -->
					<div class="addthis_toolbox addthis_default_style ">
					<a class="addthis_button_preferred_1"></a>
					<a class="addthis_button_preferred_2"></a>
					<a class="addthis_button_preferred_3"></a>
					<a class="addthis_button_preferred_4"></a>
					<a class="addthis_button_compact"></a>
					<a class="addthis_counter addthis_bubble_style"></a>
					</div>
					<script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
					<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5073efa669740139"></script>
					<!-- AddThis Button END -->
                </div>
                <? if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){ ?>
					<? 	$row_fav = mysqli_fetch_assoc(mysqli_query($conn,"select * from favorite_users where user_id = ".$_SESSION['ycdc_dbuid']." and player_id = '".$row['id']."'"));
					
						if($row_fav){
						?><a class="right" style="position:relative; top:-5px;" href="<?=WWW?>remove-favorite-<?=$_SESSION['ycdc_dbuid'];?>-<?=$row['id']?>-<?=friendlyURL($row['company_name'])?>.html" title="Remove Favorite"><img src="<?=WWW?>images/golden-star.png"></a><?
						}else{
						?><a class="right" style="position:relative; top:-5px;" href="<?=WWW?>add-to-favorite-<?=$_SESSION['ycdc_dbuid'];?>-<?=$row['id']?>-<?=friendlyURL($row['company_name'])?>.html" title="Add to Favorite"><img src="<?=WWW?>images/silver-star.png"></a><?
						}
					  }
					?>
                <? } ?>

                <? if(isset($_SESSION['ycdc_dbuid']) && $_SESSION['ycdc_dbuid'] != $id){ ?>
				<a style="position:relative; top:-10px;margin-right:10px;" id="contact-btn" href="<?=(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid']))?'#contact-tab':'#contact-message';?>" class="submit-login popup-link">Contact</a>
				<? }else if(!isset($_SESSION['ycdc_dbuid'])){ ?>
				<a style="position:relative; top:-10px;margin-right:10px;" id="contact-btn" href="<?=(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid']))?'#contact-tab':'#contact-message';?>" class="submit-login popup-link">Contact</a>
				<? } ?>
			</div>
			<div class="company-video">
				<? 
						$video = $row_v['file_name']; 

  									
                              
										if(!empty($video)){
											if(preg_match('/<iframe(.*)><\/iframe>/', $video)){
							        			preg_match('/src="(.*?)"/',$row_v['file_name'] , $src);
												$src = $src[1];
												$video = "<iframe width='280' height='250' frameborder='0' allowfullscreen src='$src'></iframe>";
											}else{
											$video_path = WWW.'videos/'.$row_v['id'].'/'.$row_v['file_name']; 
												$video = '<video width="280" height="250" controls>
												  <source src="'.$video_path.'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
									   			  <source src="'.$video_path.'" type=\'video/ogg; codecs="theora, vorbis"\'>
									 			  <source src="'.$video_path.'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
									 			  <source src="'.$video_path.'" type=\'video/webm; codecs="vp8, vorbis"\'>
											</video>';
						}
					}else{
						$video = '<img src="'.WWW.'images/no-video.jpg" width="300" height="200">';
					}
					echo $video;
					?>
			</div>
			<div class="clear"></div>
		</div>
	<div class="middle">
		<div class="content max-width-grid-list">
			<div class="white-box" >
				<ul id="menu7_1" class="menu3">
					<li class="active"><a class="activity-link" href="#activity">Activity</a></li>
					<li><a href="#photos">Photos</a></li>
					<li><a href="#videos">Videos</a></li>
					<li><a href="#traits">Traits</a></li>
                    <li><a href="#management">Management</a></li>
					<?php if($row['company_type_id'] == 4): ?>
						<li><a href="#club_members">Club Members</a></li>
					<?php endif; ?>
					<li><a href="#news">News</a></li>
					<?php  if(in_array($row['company_type_id'],array(1,2,3,31,35)) ): ?>
						<li><a href="#by_laws" class="by_laws_link">By Laws</a></li>
						<li><a class="clubs_link" href="#clubs">Clubs</a></li>
						<li><a class="venues_link" href="#venues">Venues</a></li>
						<li><a class="umpires_link" href="#umpires">Umpires</a></li>
						<li><a href="#weekly_shots" class="weekly_shots_link">Weekly Snap</a></li>
						<li><a href="#schedule_scorebook" class="schedule_scorebook_link">Schedule, Scorebook, Standings & Polls</a></li>
						<li><a href="#statistics" class="stats-link">Statistics</a></li>
					<?php endif; ?>
					<?php if($row['company_type_id'] == 4): ?>
						<li ><a class="club-stats-link" href="#club-statistics">Statistics</a></li>
					<?php endif; ?>
					<li><a href="#contact">Contact</a></li>
			  	</ul>
			  	<div class="clear"></div>
			  	<? if(!isset($_SESSION['ycdc_user_name'])
				   || empty($_SESSION['ycdc_user_name'])){
					?>
					<script>
						$('#menu7 li a,#menu7_1 li a').bind('click', function(){
							$('#contact-message').removeClass('hide');
						});
						</script>
						<script>
						$('#contact-btn1').live('click', function(){
							$('#contact-message').removeClass('hide');
						});
						</script>
					<div id="contact-message" class="hide">
					<div id="success"><b>Please Login or Register!</b></div>
					<p><?php $rowWhoopsMsg = get_record_on_id('cms', 23); echo $rowWhoopsMsg['content'];?></p>
					<?
					include('common/login-box.php');
					?></div><?
				   }else{ ?>
				   <div id="activity" class="content2" style="width:98%;">
				   <? if(in_array('Status/Activity', $fields)){ ?>
				   <style>
					
					#dashboard-activity{position:relative;border: 1px solid orange; border-radius: 4px; margin: 3px 0px; padding: 4px;}
					#dashboard-activity #dashboard-activity-owner{float:left; width:90%;}
					#dashboard-activity #dashboard-activity-owner img{width:40px; height: 55px; float:left;}
					#dashboard-activity #dashboard-activity-owner h2{float:left; margin-left:10px; width:90%;}
					#dashboard-activity #dashboard-activity-owner #dashboard-datetime{display: block; margin:10px 50px;}
					#dashboard-activity #dashboard-activity-options{width:150px;position:absolute; top: 5px; right: 5px;}
					#dashboard-activity #dashboard-activity-options a{float:right;}
					#dashboard-activity #dashboard-activity-like{line-height: 22px;}
					#dashboard-activity #dashboard-activity-like a{padding:0px 4px;}
					#dashboard-activity-content{max-height: 100px;}
					#dashboard-activity-content{}
					.middle{min-height: 700px;}
					#comment img, #sharer img, #lover img{width:40px; height: 45px; float:left;}
					#comment h2, #sharer h2, #lover h2{float:left; margin-left:10px; width:80%;}
					#comment, #sharer, #lover{clear:both; height:50px;}
					#comments, #sharers, #lovers{max-height:500; overflow-y: scroll;}
					#comment h2{margin:0px; padding:0px 5px; font-size: 13px;}
					#comment p{padding:0px 5px; line-height: 22px;}
					</style>
						<? /*	$rs_activities = mysqli_query($conn,"select * from activity where user_id = '".$row_u['id']."' and status = '1' order by id desc");
						if(mysqli_num_rows($rs_activities) == 0){
							echo '<div id="error">Not Recently Updated</div>';
						}else{
							while($row_act = mysqli_fetch_assoc($rs_activities)){ 
							?>
							<dl>
								<dt><span><?=date_converter($row_act['activity_date'])?></span></dt>
								<dd><?=$row_act['activity']?></dd>
							</dl>
							<?
							}	
						}*/
						?>
						
						<?php $rs_msg = mysqli_query($conn,"select * from activity where user_id =".$row['user_id']." and activity not like '%Logged in%' order by id desc limit 15"); 
			        		if(mysqli_num_rows($rs_msg) == 0){
			        			?>
			        			<div id="error">No Activity</div>
			        			<?
			        		}else{
				        		while($row_msg = mysqli_fetch_assoc($rs_msg)){ 
				        			$rowUserActivity= get_record_on_id('users', $row_msg['user_id']);
				        			$row_img 		= mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$rowUserActivity['id']."' and is_default = '1' "));
				        			$rowStatusLike	= mysqli_query($conn,"select * from status_likes where activity_id = '".$row_msg['id']."' and user_id = '$userId'");
				        			$rsStatusLike 	= mysqli_query($conn,"select * from status_likes where activity_id = '".$row_msg['id']."'");
				        			$rsStatusShare 	= mysqli_query($conn,"select * from status_shares where activity_id = '".$row_msg['id']."'");
				        			$rsStatusComment= mysqli_query($conn,"select * from activity_comments where activity_id = '".$row_msg['id']."'");
				        			if($rowStatusLike){
				        				$imgTitle 	= 'Loved It';
				        			}else{
				        				$imgTitle 	= 'Love It';
				        			}
				        			$loversData 	= array();
				        			$sharersData 	= array();
				        			$commentData 	= array();
				        			if(mysqli_num_rows($rsStatusComment) > 0){
				        				while($rowStatusComment = mysqli_fetch_assoc($rsStatusComment)){
				        					$commentData[]    = $rowStatusComment['user_id'];
				        				}
				        				$commentCounter = '('.mysqli_num_rows($rsStatusComment).')';
				        			}else{
				        				$commentCounter = '';
				        			}
				        			if(mysqli_num_rows($rsStatusLike) > 0){
				        				while($rowStatusLike = mysqli_fetch_assoc($rsStatusLike)){
				        					$loversData[]    = $rowStatusLike['user_id'];
				        				}
				        				$loveCounter = '('.mysqli_num_rows($rsStatusLike).')';
				        			}else{
				        				$loveCounter = '';
				        			}
				        			if(mysqli_num_rows($rsStatusShare) > 0){
				        				while($rowStatusShare = mysqli_fetch_assoc($rsStatusShare)){
				        					$sharersData[]    = $rowStatusShare['user_id'];
				        				}
				        				$sharersCounter = '('.mysqli_num_rows($rsStatusShare).')';
				        			}else{
				        				$sharersCounter = '';
				        			}
				        			?>
				        			<div id="dashboard-activity">
				        				<div id="dashboard-activity-owner">
				        					<img src="<?php echo WWW;?><?=($row_img && !empty($row_img['file_name']))?'users/'.$rowUserActivity['id'].'/photos/'.$row_img['file_name']:'images/no-photo.jpg'?>" width="90" height="100" />
				        					<h2><?php echo $rowUserActivity['f_name'].' '.$rowUserActivity['last_name'];?></h2>
				        					<div id="dashboard-datetime"><?=date_converter($row_msg['activity_date'])." ".date("H:i:s",strtotime($row_msg['activity_date']))?></div>
				        				</div>
				        				<div id="dashboard-activity-options">
				        					<?php if($row_msg['user_id'] == $userId){ ?>
						        				<a onclick="return confirm('Are you sure that you want to delete this record ?')" href="<?=WWW?>dashboard.html?action=delete&type=activity&id=<?=$row_msg['id']?>"><img src="<?=WWW?>images/icons/delete.png"></a>
						        				<a class="popup-link" href="#edit-activity" id="<?=$row_msg['id']?>"><img src="<?=WWW?>images/icons/edit.png"></a>
						        			<?php } ?>
				        				</div>
				        				<div class="clear"></div>
				        				<div id="dashboard-activity-content">
				        					<div class="right" id="gallery" >
				        					<? 	$rsPhotos = mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_msg['user_id']."' and title = '".$row_msg['activity']."'");
				        						$rsVideos = mysqli_query($conn,"select * from videos where entity_type = 'users' and entity_id = '".$row_msg['user_id']."' and title = '".$row_msg['activity']."'");
				        						if(mysqli_num_rows($rsPhotos) > 0){
				        							$rowPhoto = mysqli_fetch_assoc($rsPhotos);
				        						?>
				        						<a href="<?=WWW?>users/<?=$row_msg['user_id']?>/photos/<?=$rowPhoto['file_name']?>" class="photo">
				        							<img style="border-radius:4px; border:1px solid #ccc;" src="<?=WWW?>users/<?=$row_msg['user_id']?>/photos/<?=$rowPhoto['file_name']?>" width="100">
				        						</a><?
				        						}
				        						if(mysqli_num_rows($rsVideos) > 0){
				        							$rowVideo = mysqli_fetch_assoc($rsVideos);
				        							if(!empty($rowVideo['file_name'])){
				        								$filename = explode('.',$rowVideo['file_name']);
														$filename1= $filename[0];
														$video = '<video width="220" height="130" controls>
																  <source src="'.WWW.'videos/'.$rowVideo['id'].'/'.$filename1.'.mp4" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
																  <source src="'.WWW.'videos/'.$rowVideo['id'].'/'.$filename1.'.ogg" type=\'video/ogg; codecs="theora, vorbis"\'>
																  <source src="'.WWW.'videos/'.$rowVideo['id'].'/'.$filename1.'.mov" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
																  <source src="'.WWW.'videos/'.$rowVideo['id'].'/'.$filename1.'.webm" type=\'video/webm; codecs="vp8, vorbis"\'>
																</video>';
														echo $video;
				        							}
				        						}
				        					?>
				        					</div>
				        					<div class="left"><?=$row_msg['activity']?></div>
				        					<div class="clear"></div>
				        				</div>
				        				<div class="clear"></div>
				        				<?php /* ?><div id="dashboard-activity-like">
				        					<a href="#clicktoloveit" class="love-it" id="<?=$row_msg['id']?>">
				        						<img title="<?=$imgTitle?>"  src="<?=WWW?>images/icons/<?=(in_array($_SESSION['ycdc_dbuid'], $loversData))?'love-it.png':'love-it-gray.png';?>">
				        					</a>
				        					<a class="popup-link love-it-counter" href="#activity-likes" title="Who Loved it" id="<?=$row_msg['id']?>"><?=$loveCounter;?></a>
				        					<a href="#clicktoshareit" title="Share It" class="share-it" id="<?=$row_msg['id']?>">
				        						<img src="<?=WWW?>images/icons/share-it.png">
				        					</a>
				        					<a title="Who Shared it" class="popup-link share-it-counter" href="#activity-shares" id="<?=$row_msg['id']?>"><?=$sharersCounter?></a>
				        					<a class="popup-link" href="#activity-comments" title="Comment on It" class="comment-it" id="<?=$row_msg['id']?>">
				        						<img src="<?=WWW?>images/icons/comments.png"><?=$commentCounter?>
				        					</a>
				        					<a title="Who Commented it" class="popup-link comment-it-counter" href="#activity-comments" id="<?=$row_msg['id']?>"><?=$commentsCounter?></a>
				        				</div><?php */ ?>
				        			</div>
				        			<div class="clear"></div>
				        	<?  } } ?>
							
				<? }else{ ?>
					<? 	$rowMsg = get_record_on_id('cms', 21); 
						echo '<div id="information">'.$rowMsg['content'].'</div>';
					?>
				<? } ?>
				</div> 
				
				<div id="photos" class="content1" style="width:98%;">
					<? if(in_array('photos_Albums', $fields)){ ?>
					<div id="gallery" class="content1">
					<? 	$rs_imgs = mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_u['id']."' and is_default <> '1'");
						
						while($row_img = mysqli_fetch_assoc($rs_imgs)){ ?>
						<div style="height:175px;width:150px;float:left; margin:5px;">
							<a class="photo" href="<?=WWW?>users/<?=$row_u['id']?>/photos/<?=$row_img['file_name']?>">	
								<img style=" border-radius:4px;" src="<?=WWW?>users/<?=$row_u['id']?>/photos/<?=$row_img['file_name']?>" alt="<?=$_SESSION['ycdc_user_name']?>" width="150" height="150"/>
							</a>
						</div>
					<?  } ?>
					</div>
				
				<? }else{ ?>
					<? 	$rowMsg = get_record_on_id('cms', 21); 
						echo '<div id="information">'.$rowMsg['content'].'</div>';
					?>
				<? } ?>
				</div>
				
                <div id="videos" class="content1" style="width:98%;">
                	<? if(in_array('videos_Albums', $fields)){ ?>
					<div id="videos" class="content1">
					<? 	$rs_imgs = mysqli_query($conn,"select * from videos where entity_type = 'users' and entity_id = '".$row_u['id']."' and is_default <> '1'");
						if(mysqli_num_rows($rs_imgs) > 0){
							while($row_img = mysqli_fetch_assoc($rs_imgs)){ ?>
							<div style="height:250px;width:350px;float:left; margin:5px;">
							<?
								$video = $row_img['file_name'];
								if(preg_match('/<iframe(.*)<\/iframe>/', $row_img['file_name'])){ 
											
											preg_match('/src="(.*?)"/',$row_img['file_name'] , $src);
											$src = $src[1];
											$video = "<iframe width='250' height='150'
														src='$src'>
																			</iframe>";
								}else{
							/*
									$video = '<video width="320px" height="240px" poster="'.WWW.'videos/'.$user_id.'/'.$row_img['file_name'].'.jpg" controls="controls"><source src="'.WWW.'videos/'.$user_id.'/'.$row_img['file_name'].'" type="video/*"></video>';
							$video .= '<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="320" height="240" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0">
											<param name="ShowControls" VALUE="true">
		   									<param name="ShowStatusBar" value="true">
											<param name="allowfullscreen" value="true" />
											<param name="src" value="'.WWW.'videos/'.$user_id.'/'.$row_img['file_name'].'" />
											<embed type="application/x-shockwave-flash" width="320" height="240" src="'.WWW.'videos/'.$user_id.'/'.$row_img['file_name'].'" ></embed>
											</object>';
									*/
									$video_id = $row_img['id'];
									$video = '<video id="video1"  width="250" height="150" controls preload="metadata">
									<source src="'.WWW.'videos/'.$user_id.'/'.$row_img['file_name'].'" type="video/mp4; codecs=\'avc1.42E01E, mp4a.40.2\'">
									</video> ';
								}
								echo $video;
							?>
							</div>
						<?  } ?>
					<? }else{ ?>
					<p>No Video .... !</p>
					<? } ?>
				</div>
				
				<? }else{ ?>
					<? 	$rowMsg = get_record_on_id('cms', 21); 
						echo '<div id="information">'.$rowMsg['content'].'</div>';
					?>
				<? } ?>
				</div>
				<div id="traits" class="content1">
					<p><strong>Year(s) of Formation : </strong><?=$row['years_in_business']?></p>
					<p><strong>Legal Status : </strong><?=$row['legal_status']?></p>
					<p><strong>Status Level : </strong><?=$row['status_level']?></p>
					<? if(!empty($row_u['game_types'])){ ?>
						<p><strong>Game Type : </strong><?=$row_u['game_types']?></p>
					<? } ?>
					<? if(!empty($row_u['match_types'])){ ?>
						<p><strong>Match Type : </strong><?=$row_u['match_types']?></p>
					<? } ?>
					<p><strong>Type of services : </strong><?=$row['service_offering']?></p>
				</div>
				<div id="management" class="content1 max-width-grid-list">
					<table width="100%" border="1" style="border-collapse:collapse;" id="inbox">
					<tr>
						<th class="tleft">Title</th>
						<th class="tleft">First Name</th>
						<th class="tleft">Last Name</th>
					</tr>
			        <? 	$rs_msg = mysqli_query($conn,"select cs.* from company_staff cs where cs.user_id = '".$row['user_id']."' and type='management' order by id ASC "); 
			        	if(mysqli_num_rows($rs_msg) == 0){
			        		?>
			        		<tr>
			        			<td colspan="3">No record added<td>
			        		</tr>
			        		<?
			        	}else{
				       		while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
				       		<tr title="Click for detail">
				       			
				       			<td><?=$row_msg['title']?></td>
				       			<td><?=$row_msg['first_name']?></td>
				       			<td><?=$row_msg['last_name']?></td>
				       		</tr>
				       	<?  }
				       	} ?>
			        </table>
				</div>
				
				<?php if($row['company_type_id'] == 4){ ?>
				<div id="club_members">
				<h2>Club Members</h2>
					<div class="list">
					
					<form>
					<div class="statistics-filters" id="club-member-search" style="15px 15px 15px 0px">
						<input type="text" name="club_member_name" class="input-login" id="club_member_name" value="<?php echo $_GET['club_member_name'] ?>" placeholder="Member Name" style="margin-left:4px;">
						<input type="submit" name="club_members_search" id="club_members_search" class="submit-login" value="Search" style="float:none;margin-left:20px;">
					</div>
					</form>
                <ul id="individual" class="content1">
                	<?php  
						$query = "select u.*,c.date_added  from users as u inner join club_members as c on u.id = c.member_id where c.club_id = '".$row['id']."' and u.status = 1 ";
						
						if(isset($_GET['club_member_name']) && $_GET['club_member_name'] != 'Search Here' && $_GET['club_member_name'] != 'all' && !empty($_GET['club_member_name'])){
							$keywords = trim($_GET['club_member_name']);
							if(strpos($keywords,' ') !== false){
								$keywords_arr = explode(' ',$keywords);
								$where.= " and (u.f_name like '%".$keywords_arr[0]."%'  AND u.last_name like '%".$keywords_arr[1]."%') ";
							}else{
								$where.= " and (u.f_name like '%{$keywords}%' OR u.m_name like '%{$keywords}%' OR u.last_name like '%{$keywords}%') ";
							}
						}else{
							$keywords = '';
							$where  = '';
						}
						
						$query.=$where;
						
						$query.=" ORDER BY u.f_name,u.last_name ";
						
						$rs_members   = mysqli_query($conn,$query);
						if(mysqli_num_rows($rs_members) == 0){
							echo '<div id="information">No record found ... !</div>';
						}
          			 
						$i= 0 ;
						while($row_member 	= mysqli_fetch_assoc($rs_members)){
                		

                		$row_img = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_member['id']."' and is_default = '1' "));
			  				$row_country= get_record_on_id('countries', $row_member['country_id']);	
			  				$location 	= $row_country['name'];
			  				$row_vid = mysqli_fetch_assoc(mysqli_query($conn,"select * from videos where entity_id = '".$row_member['id']."' and entity_type = 'users' and is_default = '1' and file_name != ''"));
		        		    ?>
		        		    
						<dl>
								<dt>
									
									<a href="<?=WWW?>individual-detail-<?=$row_member['id']?>-<?=friendlyURL($row_member['f_name'].' '.$row_member['m_name'].' '.$row_member['last_name'])?>.html#activity-tab" title="<?=$row_member['f_name'].' '.$row_member['m_name'].' '.$row_member['last_name']?>"><img src="<?php echo WWW;?><?=($row_img && !empty($row_img['file_name']))?'users/'.$row_member['id'].'/photos/'.$row_img['file_name']:'images/no-photo.jpg'?>" width="120" height="128" /></a></dt>
								<dd><div class="details">
									<h3><?=truncate_string($row_member['f_name'],20).'<br>'.truncate_string($row_member['last_name'], 20)?></h3>
									<p><? echo $location;?><br />
									
<?if($row_member['user_type_id'] != 1)

{
    echo get_combo('user_types','name',$row_member['user_type_id'],'','text');  

?><br />
		 <?   if($row_member['user_type_id'] == 2){ echo $row_member['type'];} }?>
									
									<?php if(strtotime($row_member['date_added'])>0): ?>
									<div class="clear" style="height:3px;"></div>
									Added on: <?php echo date('d, F Y H:i:s',strtotime($row_member['date_added'])); ?> <?php endif; ?>
											<div class="clear" style="height:5px;"></div>
								
									</p>
									<a class="submit-login margin-top-5" href="<?=WWW?>individual-detail-<?=$row_member['id']?>-<?=friendlyURL($row_member['f_name'].' '.$row_member['m_name'].' '.$row_member['last_name'])?>.html#activity-tab" title="<?=$row_member['f_name'].' '.$row_member['m_name'].' '.$row_member['last_name'].' - '.$site_title?>">View Profile</a></div>
									<div class="flag"><img title="<?=$row_country['name']?>" alt="<?=$row_country['name']?>" src="<?php echo WWW;?>countries/<?=$row_country['flag']?>"  width="25"/></div>
									<div class="video">
										<? 
										$video = $row_vid['file_name'];
											if(!empty($video)){
						if(preg_match('/<iframe(.*)<\/iframe>/', $row_vid['file_name'])){ 
									
									preg_match('/src="(.*?)"/',$row_vid['file_name'] , $src);
									$src = $src[1];
									$video = "<iframe width='205' height='130' frameborder='0' allowfullscreen 
												src='$src'>
																	</iframe>";
									
									if(!preg_match('/height="(.*)"/', $video)){
										$video = preg_replace('/height="(.*)"/','height="130"',$video);	
									}else{
										$video = preg_replace('/width="(.*)"/','width="220" height="150"',$video);	
									}
						}else{	
												$filename = explode('.',$video);
												$filename1= $filename[0];
												$video = '<video width="220" height="130" controls>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mp4" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.ogg" type=\'video/ogg; codecs="theora, vorbis"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mov" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.webm" type=\'video/webm; codecs="vp8, vorbis"\'>
												</video>';
											}
										}else{
											$video = '<img src="'.WWW.'images/no-video.jpg" width="168" height="130">';
										}
										echo $video;?>									
									</div>
								</dd>
						</dl>
						<?php
                		
					      $i++;
					      $count++;
					      $x++;
					  } 
					  ?>
			</ul>
			</div><br/>
			
				</div>
				<?php } ?>
				
				<div id="news" class="content2">
					<h2>Company News</h2>
					<br>
					<div class="list">
						<ul>
							<?  $rpp = PRODUCT_LIMIT_FRONT; // results per page
								$ppage = intval($_GET["page"]);
								if($ppage<=0) $ppage = 1;
								$query = "select distinct n.* from news n";
								//=======================================
								

								
								$query .= "  where   n.status=1 and n.type = '' and n.user_id = ".$row['user_id']." order by n.id desc ";
							 // pr($_SESSION);
							 //echo $query;
							  //=======================================
							  if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
								echo '<div id="information">No record ...!</div>';
							  }
							  $rs   = mysqli_query($conn,$query);
							  $tcount = mysqli_num_rows($rs);
							  $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
							  $count = 0;
							  $i = ($ppage-1)* $rpp;
							  $x = 0;
							  //=======================================
							  while(($count<$rpp) && ($i<$tcount)){
								mysqli_data_seek($rs,$i);
								$row_g 	= mysqli_fetch_assoc($rs);
								$row_img= mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'news' and entity_id = '".$row_g['id']."'"));
								?>
								<li>
									<a href="<?=WWW?>news-detail-<?=$row_g['id']?>-<?=friendlyURL($row_g['title'])?>.html" title="Read more" />
										<img src="<?=($row_img)?WWW.'news/'.$row_g['id'].'/'.$row_img['file_name']:WWW.'images/community-groups.png';?>" alt="<?=$row_g['title']?>" title="<?=$row_g['title']?>" width="114" />
									</a>
									<span class="list-text">
										<h3 onclick="window.location='<?=WWW?>news-detail-<?=$row_g['id']?>-<?=friendlyURL($row_g['title'])."+"?>.html"><?=$row_g['title']?></h3>
										<p><?=truncate_string($row_g['content'], 150)?></p>
										<a href="<?=WWW?>news-detail-<?=$row_g['id']?>-<?=friendlyURL($row_g['title'])."+"?>.html" class="submit-login" title="Read more" />
										Read More
									</a>
									</span>
									<span class="list-social">
										<!-- AddThis Button BEGIN -->
										<div class="addthis_toolbox addthis_default_style ">
										<a class="addthis_button_preferred_1"></a>
										<a class="addthis_button_preferred_2"></a>
										<a class="addthis_button_preferred_3"></a>
										<a class="addthis_button_preferred_4"></a>
										<a class="addthis_button_compact"></a>
										<a class="addthis_counter addthis_bubble_style"></a>
										</div>
										<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
										<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5073efa669740139"></script>
										<!-- AddThis Button END -->
										<br> <br> <br>
										<?=date_converter($row_g['news_date'],'M d, Y H:i:s')?>
										<? if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){ ?>
										<? if($row_g['user_id'] == $_SESSION['ycdc_dbuid'] 
											   || $_SESSION['ycdc_desig_id'] == '1'
											   || $_SESSION['ycdc_user_email'] == 'naveed.ramzan@gmail.com'
												||$_SESSION['ycdc_user_email'] == 'youcrickter00@gmail.com'){ ?>
												<a href="<?=WWW?>edit-news-<?=$row_g['id']?>.html"><img src="<?=WWW?>images/icons/edit.png"></a>
												<a href="<?=WWW?>delete-news-<?=$row_g['id']?>.html"><img src="<?=WWW?>images/icons/delete.png"></a>
											<? } ?>
										<? } ?>
									</span>
								</li>
								<?
								  $i++;
								  $count++;
								  $x++;
							  } 
							  ?>
					</ul>
					</div>
				</div>	
				<?php  if(in_array($row['company_type_id'],array(1,2,3,31,35))){ ?>
				<div id="by_laws" class="content2" style="width:98%;">
					<h2>By Laws</h2>
					<div class="clear"></div>
					<?php if(!empty($row['by_laws_file'])): ?>
						<?php $arr1 = explode(',',$row['by_laws_file']);$arr2 = explode(',',$row['by_laws_file_orig']);
						for($i=0;$i<count($arr1);$i++): ?>
							<a style="background:none;padding:5px;" target="_blank" href="<?=WWW?>by-laws-uploads/<?php echo $arr1[$i]; ?>"><h3 style="font-size:16px;"><?php echo $arr2[$i]; ?></h3></a>
						<?php endfor; ?>
					<?php else: ?>
						<div id="information">There are no By-Laws added by the respective Company</div>
					<?php endif; ?>
				</div>
				
				<div id="clubs" class="content2" style="width:98%;">
					<h2>Clubs</h2>
				    
                	<?php  
					$query = "select distinct u.*,c.company_name,c.years_in_business,c.id as company_id,c.company_permalink from users u inner join companies as c on u.id=c.user_id inner join league_clubs as l on l.club_id=c.id where u.status = 1 and l.league_id=".$row['id'];
						$rs   = mysqli_query($conn,$query);
						if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
							echo '<div id="information">No record found ... !</div>';
						}
          			 
						$i= 0 ;
						while($row_clubs 	= mysqli_fetch_assoc($rs)){
                		

                		$row_img = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_clubs['id']."' and is_default = '1' "));
			  				$row_country= get_record_on_id('countries', $row_clubs['country_id']);	
			  				$location 	= $row_country['name'];
			  				$row_vid = mysqli_fetch_assoc(mysqli_query($conn,"select * from videos where entity_id = '".$row_clubs['id']."' and entity_type = 'users' and is_default = '1' and file_name != ''"));
		        		    ?>
		        		    
						<dl>
								<dt>
									
									<a href="<?=WWW?><?php echo $row_clubs['company_permalink']; ?>" title="<?=$row_clubs['company_name']; ?>"><img src="<?php echo WWW;?><?=($row_img && !empty($row_img['file_name']))?'users/'.$row_clubs['id'].'/photos/'.$row_img['file_name']:'images/no-photo.jpg'?>" width="120" height="128" /></a></dt>
								<dd><div class="details">
									<h3><?=truncate_string($row_clubs['company_name'],20)?></h3>
									<p><? echo $location;?><br />
									
									<?if($row_clubs['user_type_id'] != 1)

									{
										echo get_combo('user_types','name',$row_clubs['user_type_id'],'','text');  

									?><br />
											 <?   if($row_clubs['user_type_id'] == 2){ echo $row_clubs['type'];} }?>
									
									
								
									</p>
									<a class="submit-login margin-top-5" href="<?=WWW?><?php echo $row_clubs['company_permalink']; ?>" title="<?=$row_clubs['company_name'].' - '.$site_title?>">View Profile</a></div>
									<div class="flag"><img title="<?=$row_country['name']?>" alt="<?=$row_country['name']?>" src="<?php echo WWW;?>countries/<?=$row_country['flag']?>"  width="25"/></div>
									<div class="video">
										<? 
										$video = $row_vid['file_name'];
											if(!empty($video)){
						if(preg_match('/<iframe(.*)<\/iframe>/', $row_vid['file_name'])){ 
									
									preg_match('/src="(.*?)"/',$row_vid['file_name'] , $src);
									$src = $src[1];
									$video = "<iframe width='205' height='130' frameborder='0' allowfullscreen 
												src='$src'>
																	</iframe>";
									
									if(!preg_match('/height="(.*)"/', $video)){
										$video = preg_replace('/height="(.*)"/','height="130"',$video);	
									}else{
										$video = preg_replace('/width="(.*)"/','width="220" height="150"',$video);	
									}
						}else{	
												$filename = explode('.',$video);
												$filename1= $filename[0];
												$video = '<video width="220" height="130" controls>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mp4" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.ogg" type=\'video/ogg; codecs="theora, vorbis"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mov" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.webm" type=\'video/webm; codecs="vp8, vorbis"\'>
												</video>';
											}
										}else{
											$video = '<img src="'.WWW.'images/no-video.jpg" width="168" height="130">';
										}
										echo $video;?>									
									</div>
								</dd>
						</dl>
						<?php
                		
					      $i++;
					      $count++;
					      $x++;
					  } 
					  ?>
					
				</div>
				
				<div id="venues" class="content1" style="width:98%;">
					<h2>Venues</h2>
					<?php $sql = "SELECT v.*,c.name as country_name FROM league_venues as v left join countries as c on v.country=c.id WHERE v.league_id=".$row['id']." ORDER BY v.id ";
						$rs_venues = mysqli_query($conn,$sql);
						while($row_venue = mysqli_fetch_assoc($rs_venues)){
							$venues[] = $row_venue;
						}
					?>
					<table  class="table-list">
					<tr><th>Venue</th><th>Address</th><th>State</th><th>City</th><th>Zip Code</th><th>Country</th><th>Contact Name</th></tr>
					<?php for($i=0;$i<count($venues);$i++): ?>
						<tr>
							<td><?php echo $venues[$i]['venue']; ?></td>
							<td><?php echo $venues[$i]['address']; ?></td>
							<td><?php echo $venues[$i]['state']; ?></td>
							<td><?php echo $venues[$i]['city']; ?></td>
							<td><?php echo $venues[$i]['zip_code']; ?></td>
							<td><?php echo $venues[$i]['country_name']; ?></td>
							<td><?php echo $venues[$i]['contact_name']; ?></td>
						</tr>
					<?php endfor; ?>
					<?php if(empty($venues)): ?>
					<tr><td colspan="4">No Records</td></tr>
					<?php endif; ?>
					</table>
				</div>
				
				<div id="umpires" class="content1" style="width:98%;">
					<h2>Umpires</h2>
                	<?  
						
                		$rpp = PRODUCT_LIMIT_FRONT; // results per page
                		
      					$query = "select distinct u.*,c.year_certified,c.umpire_phone from users u inner join league_umpires as c on u.id=c.umpire_id where u.status = 1 and c.league_id=".$row['id'];
						$query .= " order by u.id desc ";
				    
						$rs   = mysqli_query($conn,$query);
						if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
							echo '<div id="information">No record found ... !</div>';
						}
          			 
						$i= 0 ;
						while($row_umpires 	= mysqli_fetch_assoc($rs)){
                		$row_img = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_umpires['id']."' and is_default = '1' "));
			  				$row_country= get_record_on_id('countries', $row_umpires['country_id']);	
			  				$location 	= $row_country['name'];
			  				$row_vid = mysqli_fetch_assoc(mysqli_query($conn,"select * from videos where entity_id = '".$row_umpires['id']."' and entity_type = 'users' and is_default = '1' and file_name != ''"));
		        		    ?>
		        		    
						<dl>
								<dt>
									
									<a href="<?=WWW?>individual-detail-<?=$row_umpires['id']?>-<?=friendlyURL($row_umpires['f_name'].' '.$row_umpires['m_name'].' '.$row_umpires['last_name'])?>.html#activity-tab" title="<?=$row_umpires['f_name'].' '.$row_umpires['m_name'].' '.$row_umpires['last_name']?>"><img src="<?php echo WWW;?><?=($row_img && !empty($row_img['file_name']))?'users/'.$row_umpires['id'].'/photos/'.$row_img['file_name']:'images/no-photo.jpg'?>" width="120" height="128" /></a></dt>
								<dd><div class="details">
									<h3 style="height:30px;"><?=truncate_string($row_umpires['f_name'],20).' '.truncate_string($row_umpires['last_name'], 20)?></h3>
									<p><? echo $location;?><br />
									
							<?if($row_umpires['user_type_id'] != 1)

							{
								echo get_combo('user_types','name',$row_umpires['user_type_id'],'','text');  

							?><br />
									 <?   if($row_umpires['user_type_id'] == 2){ echo $row_umpires['type'];} }?>
									
									
								
									</p>
									<a class="submit-login margin-top-5" href="<?=WWW?>individual-detail-<?=$row_umpires['id']?>-<?=friendlyURL($row_umpires['f_name'].' '.$row_umpires['m_name'].' '.$row_umpires['last_name'])?>.html#activity-tab" title="<?=$row_umpires['f_name'].' '.$row_umpires['m_name'].' '.$row_umpires['last_name'].' - '.$site_title?>">View Profile</a></div>
									<div class="flag"><img title="<?=$row_country['name']?>" alt="<?=$row_country['name']?>" src="<?php echo WWW;?>countries/<?=$row_country['flag']?>"  width="25"/></div>
									<div class="video">
										<? 
										$video = $row_vid['file_name'];
											if(!empty($video)){
						if(preg_match('/<iframe(.*)<\/iframe>/', $row_vid['file_name'])){ 
									
									preg_match('/src="(.*?)"/',$row_vid['file_name'] , $src);
									$src = $src[1];
									$video = "<iframe width='205' height='130' frameborder='0' allowfullscreen 
												src='$src'>
																	</iframe>";
									
									if(!preg_match('/height="(.*)"/', $video)){
										$video = preg_replace('/height="(.*)"/','height="130"',$video);	
									}else{
										$video = preg_replace('/width="(.*)"/','width="220" height="150"',$video);	
									}
						}else{	
												$filename = explode('.',$video);
												$filename1= $filename[0];
												$video = '<video width="220" height="130" controls>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mp4" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.ogg" type=\'video/ogg; codecs="theora, vorbis"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mov" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.webm" type=\'video/webm; codecs="vp8, vorbis"\'>
												</video>';
											}
										}else{
											$video = '<img src="'.WWW.'images/no-video.jpg" width="168" height="130">';
										}
										echo $video;?>									
									</div>
								</dd>
						</dl>
						<?php
                		
					      $i++;
					      $count++;
					      $x++;
					  } 
					  ?>
					
				</div>
				
				
				<div id="weekly_shots" class="content" style="width:98%;">
					<div class="clear"><br></div>
					
					
					<ul id="weekly_shots_tournaments" class="menu3">
					<?php $weekly_first_tournament = 0;$i=0;
					$sql = "SELECT id,year,title FROM tournaments WHERE league_id = ".$row['id']." AND year=".date('Y')." ORDER BY title";
					$rs_current_year_tournaments = mysqli_query($conn,$sql);
					while($row1 = mysqli_fetch_assoc($rs_current_year_tournaments)){?>
					<li class="weekly-shots-li"><a href="#weekly_shots-tab" id="t_<?php echo $row1['id'] ?>" class="weekly-shots-tour-link"><?php echo $row1['title'] ?></a></li>	
					<?php if($i==0) $weekly_first_tournament=$row1['id']; ?>
					<?php $i++; } ?>
					</ul>
					<div id="weekly-shots-content"></div>
				</div>
				
				<div id="schedule_scorebook" class="content1" style="width:98%;">
				<div id="tournament_list_div" style="float:left;width:100%;">
				<?php 
				 $sql = "SELECT t.*,c.name as country_name FROM tournaments as t left join countries as c on .t.country_id = c.id
					 WHERE t.user_id = ".$row_u['id']." and t.status = 1  ORDER BY t.id DESC ";
					$rs_tournaments = mysqli_query($conn,$sql);
					while($row1 = mysqli_fetch_assoc($rs_tournaments)){
						$tournaments[] = $row1;
					}
				?>
				<div class="clear"><br/></div>
				<table id="table-list" class="table-list white-box">
				<tr><th>Tournament Title</th><th>Game Type</th><th>Overs</th><th>Ball Type</th><th>Country</th><th>Start Date</th><th>End Date</th><th>Action</th></tr>
				<?php for($i=0;$i<count($tournaments);$i++): ?>
					<tr>
						<td><?php echo ucwords($tournaments[$i]['title']); ?></td>
						<td><?php echo $tournaments[$i]['overs_type']; ?></td>
						<td><?php echo $tournaments[$i]['overs_count']; ?> overs</td>
						<td><?php echo ucwords(str_replace('_',' ',$tournaments[$i]['ball_type'])); ?></td>
						<td><?php echo ucwords($tournaments[$i]['country_name']); ?></td>
						<td><?php echo  date('d M, Y',strtotime($tournaments[$i]['start_time'])); ?></td>
						<td><?php echo date('d M, Y',strtotime($tournaments[$i]['end_time'])); ?></td>
						<td>
							<a onclick="displaySchedule(<?php echo $tournaments[$i]['id']; ?>);" href="javascript:;" title="Schedule & Score Book"><img alt="Schedule & Score Book" src="<?php echo WWW; ?>images/icons/match.png" border="0"></a> &nbsp;
							<a href="<?php echo WWW; ?>tournament/standings/<?php echo $tournaments[$i]['id']; ?>" title="Tournament Rankings"><img alt="Rankings" src="<?php echo WWW; ?>images/icons/ranking.png" border="0"></a>  &nbsp;
							<a href="<?php echo WWW; ?>tournament/public-polls/<?php echo $tournaments[$i]['id']; ?>" title="Tournament Polls"><img alt="Tournament Polls" src="<?php echo WWW; ?>images/icons/poll.png" border="0"></a>  
						</td>
					</tr>
				<?php endfor; ?>
				<?php if(empty($tournaments)): ?>
				<tr><td colspan="4">No Records</td></tr>
				<?php endif; ?>
				</table>
				
				<div class="clear"></div>
				</div>
				
				<div id="tournament_matches_div" style="float:left;width:980px;margin-left:5px;">
				</div>
				</div>
				
				
				
				<div id="statistics" class="content1" style="width:98%;">
					<h1 style="margin:5px 0px 5px 0px;"> Statistics </h1>
					<?php $game_type = array('Ten10'=>'Ten10','Fifteen15'=>'Fifteen15','Twenty20'=>'Twenty20','Thirty30'=>'Thirty30','One Day'=>'One Day');
						$overs_type = array('10'=>'10','15'=>'15','20'=>'20','30'=>'30','35'=>'35','40'=>'40','45'=>'45','50'=>'50','55'=>'55','60'=>'60');
						$ball_type = array('hard_ball'=>'Hard Ball','rubber_ball'=>'Rubber Ball','tape_ball'=>'Tape Ball','tennis_ball'=>'Tennis Ball');
					?>
					<div class="statistics-filters">
						
						<?php $rs_teams = mysqli_query($conn,"Select max(year) as max_year, min(year) as min_year FROM tournaments WHERE league_id = ".$row['id']);
						$year_row = mysqli_fetch_assoc($rs_teams);?>
						<select name="year_select_stats" id="year_select_stats" style="width:100px;">
							<option value="0">Year</option>
							<?php for($i=$year_row['min_year'];$i<=$year_row['max_year'];$i++): ?>
								<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
							<?php endfor; ?>
						</select>
						
						<?php $rs_tournaments = mysqli_query($conn,"Select id,title from tournaments WHERE league_id = ".$row['id'].' ORDER BY title'); ?>
						<select name="tournament_select_stats" id="tournament_select_stats">
						<option value="0">All Tournaments</option>
						<?php while($row1 = mysqli_fetch_assoc($rs_tournaments)): ?>
							<option value="<?php echo $row1['id']; ?>"><?php echo $row1['title']; ?></option>
						<?php endwhile; ?>
						</select>
						
						<select name="game_types_select_stats" id="game_types_select_stats" style="width:120px;">
							<option value="0">Game Type</option>
							<?php foreach($game_type as $key=>$val):  ?>
								<option <?php echo $sel; ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
							<?php endforeach; ?>
						</select>
						<select name="overs_type_select_stats" id="overs_type_select_stats" style="width:100px;">
							<option value="0">Overs</option>
							<?php foreach($overs_type as $key=>$val):  ?>
								<option <?php echo $sel; ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
							<?php endforeach; ?>
						</select>
						<select name="ball_type_select_stats" id="ball_type_select_stats" style="width:100px;">
							<option value="0">Ball Type</option>
							<?php foreach($ball_type as $key=>$val):  ?>
								<option <?php echo $sel; ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
							<?php endforeach; ?>
						</select>
						
						<div class="clear"><br/></div>
						
						<?php $rs_teams = mysqli_query($conn,"Select distinct c.id,c.company_name from companies as c inner join tournament_teams as 
						tt on c.id=tt.team_id inner join tournaments as t on t.id = tt.tournament_id 
						WHERE t.league_id = ".$row['id'].' ORDER BY c.company_name'); ?>
						<select name="club_select_stats" id="club_select_stats">
						<option value="0">All Clubs</option>
						<?php while($row1 = mysqli_fetch_assoc($rs_teams)): ?>
							<option value="<?php echo $row1['id']; ?>"><?php echo $row1['company_name']; ?></option>
						<?php endwhile; ?>
						</select>
						
						<?php $rs_players = mysqli_query($conn,"Select distinct u.id,u.f_name,u.m_name,u.last_name from users as u 
						inner join club_members as cbm on cbm.member_id = u.id
						inner join tournament_teams as tt on cbm.club_id = tt.team_id
						inner join tournaments as t on t.id = tt.tournament_id 
						WHERE t.league_id = ".$row['id'].' ORDER BY u.f_name'); ?>
						<select name="player_select_stats" id="player_select_stats">
						<option value="0">All Players</option>
						<?php while($row1 = mysqli_fetch_assoc($rs_players)): ?>
							<option value="<?php echo $row1['id']; ?>"><?php echo $row1['f_name'].' '.$row1['last_name']; ?></option>
						<?php endwhile; ?>
						</select>
						<input type="button" name="stats_search" id="stats_search" class="submit-login" value="Search" style="float:none;margin-left:20px;">
						
						
					</div>
					
					<div id="statistics-data"></div>
				</div>
				
				<?php } ?>	
				
				
				<?php  if($row['company_type_id'] == 4){ ?>
				
				<div id="club-statistics" class="content1" style="width:98%;">
					<h1 style="margin:10px 0px 10px 15px;"> Statistics </h1>
					<?php $game_type = array('Ten10'=>'Ten10','Fifteen15'=>'Fifteen15','Twenty20'=>'Twenty20','Thirty30'=>'Thirty30','One Day'=>'One Day');
						$overs_type = array('10'=>'10','15'=>'15','20'=>'20','30'=>'30','35'=>'35','40'=>'40','45'=>'45','50'=>'50','55'=>'55','60'=>'60');
						$ball_type = array('hard_ball'=>'Hard Ball','rubber_ball'=>'Rubber Ball','tape_ball'=>'Tape Ball','tennis_ball'=>'Tennis Ball');
					?>
					<div class="club-statistics-filters">
						
						<?php $rs_leagues = mysqli_query($conn,'Select DISTINCT c.id,c.company_name from companies as c inner join tournaments as t on c.id=t.league_id ORDER BY c.company_name'); ?>
						<select name="league_select_stats" id="league_select_stats" style="width:200px;">
						<option value="0">All Leagues</option>
						<?php while($row1 = mysqli_fetch_assoc($rs_leagues)): ?>
							<option value="<?php echo $row1['id']; ?>"><?php echo $row1['company_name']; ?></option>
						<?php endwhile; ?>
						</select>
						
						<?php //$rs_teams = mysqli_query($conn,"Select max(year) as max_year, min(year) as min_year FROM tournaments as t inner join tournament_teams as tt on t.id=tt.tournament_id WHERE tt.team_id = ".$row['id']);
						$rs_teams = mysqli_query($conn,"Select max(year) as max_year, min(year) as min_year FROM tournaments as t inner join tournament_teams as tt on t.id=tt.tournament_id ");
						$year_row = mysqli_fetch_assoc($rs_teams);?>
						<select name="year_select_stats" id="year_select_stats" style="width:100px;">
							<option value="0">All Years</option>
							<?php for($i=$year_row['min_year'];$i<=$year_row['max_year'];$i++): ?>
								<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
							<?php endfor; ?>
						</select>
						
						<?php //$rs_tournaments = mysqli_query($conn,"Select t.id,t.title from tournaments as t inner join tournament_teams as tt on t.id=tt.tournament_id WHERE tt.team_id = ".$row['id'].' ORDER BY title');
						$rs_tournaments = mysqli_query($conn,"Select distinct t.id,t.title from tournaments as t inner join tournament_teams as tt on t.id=tt.tournament_id  ORDER BY title"); ?>
						<select name="tournament_select_stats" id="tournament_select_stats" style="width:190px;">
						<option value="0">All Tournaments</option>
						<?php while($row1 = mysqli_fetch_assoc($rs_tournaments)): ?>
							<option value="<?php echo $row1['id']; ?>"><?php echo $row1['title']; ?></option>
						<?php endwhile; ?>
						</select>
						
						<select name="game_types_select_stats" id="game_types_select_stats" style="width:120px;">
							<option value="0">Game Type</option>
							<?php foreach($game_type as $key=>$val):  ?>
								<option <?php echo $sel; ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
							<?php endforeach; ?>
						</select>
						<select name="overs_type_select_stats" id="overs_type_select_stats" style="width:100px;">
							<option value="0">Overs</option>
							<?php foreach($overs_type as $key=>$val):  ?>
								<option <?php echo $sel; ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
							<?php endforeach; ?>
						</select>
						
						<select name="ball_type_select_stats" id="ball_type_select_stats" style="width:100px;">
							<option value="0">Ball Type</option>
							<?php foreach($ball_type as $key=>$val):  ?>
								<option <?php echo $sel; ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
							<?php endforeach; ?>
						</select>
						<div class="clear"><br/></div>
						<input type="button" name="club_stats_search" id="club_stats_search" class="submit-login" value="Search" style="float:none;margin-left:20px;">
					</div>
					<div class="clear"><br/></div>
					<div id="club-statistics-data"></div>
				</div>
				<?php } ?>
				
				<div id="contact" class="content2">
					<h2>Company Contact</h2>
					<br>
					<? include('common/login-box.php');?>
					<? if(isset($_SESSION['ycdc_user_name']) && !empty($_SESSION['ycdc_user_name'])){ ?>
				    <form method="post" action="">
				    	<input type="hidden" name="action" value="message">
						<fieldset>
							<div class="form-box">
								<div style="padding:15px 15px 0 15px;">
									To : <?=get_combo('users','concat(f_name, last_name)',$row['user_id'],'','text');?>
								</div>
								<div class="clear"></div>
								<input style="width:300px;float:left; margin-left:20px;" type="text" name="subject"dd placeholder="Subject" class="input-login <?=(!isset($_SESSION['ycdc_user_name']) || empty($_SESSION['ycdc_user_name']))?'':'validate[required]'?>">
								<textarea placeholder="Message here" name="comments" style="width:300px;" class="<?=(!isset($_SESSION['ycdc_user_name']) || empty($_SESSION['ycdc_user_name']))?'':'validate[required]'?>"></textarea>
							</div>
							<div class="clear"></div>
							<div class="clear"></div>
							<div class="form-box">
								<input type="submit" value="Send"  class="submit-login">
							</div>
						</fieldset>
					</form>
					<? }else{ ?>
						<div id="error">Please login first for post your reply ... !</div>
					<? } ?>
				</div>
				<? } ?>
				
				<?php // 76 is ID of Philadelphia Cricket League ?>
				<?php if(in_array($row['company_type_id'],array(1,2,3,31,35)) && $row['id'] == 76 ){ ?>
					<? if(!isset($_SESSION['ycdc_user_name']) || empty($_SESSION['ycdc_user_name'])){?>
					<div id="schedule_scorebook" class="content1" style="width:98%;">
					<div id="tournament_list_div" style="float:left;width:100%;">
					<?php 
					 $sql = "SELECT t.*,c.name as country_name FROM tournaments as t left join countries as c on .t.country_id = c.id
						 WHERE t.user_id = ".$row_u['id']." and t.status = 1  ORDER BY t.id DESC ";
						$rs_tournaments = mysqli_query($conn,$sql);
						while($row1 = mysqli_fetch_assoc($rs_tournaments)){
							$tournaments[] = $row1;
						}
					?>
					<div class="clear"><br/></div>
					<table id="table-list" class="table-list white-box">
					<tr><th>Tournament Title</th><th>Game Type</th><th>Overs</th><th>Ball Type</th><th>Country</th><th>Start Date</th><th>End Date</th><th>Action</th></tr>
					<?php for($i=0;$i<count($tournaments);$i++): ?>
						<tr>
							<td><?php echo ucwords($tournaments[$i]['title']); ?></td>
							<td><?php echo $tournaments[$i]['overs_type']; ?></td>
							<td><?php echo $tournaments[$i]['overs_count']; ?> overs</td>
							<td><?php echo ucwords(str_replace('_',' ',$tournaments[$i]['ball_type'])); ?></td>
							<td><?php echo ucwords($tournaments[$i]['country_name']); ?></td>
							<td><?php echo  date('d M, Y',strtotime($tournaments[$i]['start_time'])); ?></td>
							<td><?php echo date('d M, Y',strtotime($tournaments[$i]['end_time'])); ?></td>
							<td>
								<a onclick="displaySchedule(<?php echo $tournaments[$i]['id']; ?>);" href="javascript:;" title="Schedule & Score Book"><img alt="Schedule & Score Book" src="<?php echo WWW; ?>images/icons/match.png" border="0"></a> &nbsp;
								<a href="<?php echo WWW; ?>tournament/standings/<?php echo $tournaments[$i]['id']; ?>" title="Tournament Rankings"><img alt="Rankings" src="<?php echo WWW; ?>images/icons/ranking.png" border="0"></a>  &nbsp;
								<a href="javascript:;" onclick="hideScheduleByPoll();" class="poll-link" title="Tournament Polls"><img alt="Tournament Polls" src="<?php echo WWW; ?>images/icons/poll.png" border="0"></a>  
							</td>
						</tr>
					<?php endfor; ?>
					<?php if(empty($tournaments)): ?>
					<tr><td colspan="4">No Records</td></tr>
					<?php endif; ?>
					</table>
					
					<div class="clear"></div>
					</div>
					
					<div id="tournament_matches_div" style="float:left;width:980px;margin-left:5px;">
					</div>
					</div>
					
					
					<div id="statistics" class="content1" style="width:98%;">
					
					<?php $game_type = array('Ten10'=>'Ten10','Fifteen15'=>'Fifteen15','Twenty20'=>'Twenty20','Thirty30'=>'Thirty30','One Day'=>'One Day');
						$overs_type = array('10'=>'10','15'=>'15','20'=>'20','30'=>'30','35'=>'35','40'=>'40','45'=>'45','50'=>'50','55'=>'55','60'=>'60');
						$ball_type = array('hard_ball'=>'Hard Ball','rubber_ball'=>'Rubber Ball','tape_ball'=>'Tape Ball','tennis_ball'=>'Tennis Ball');
					?>
					<div class="statistics-filters">
						<h1 style="margin:5px 0px 5px 0px;"> Statistics </h1>
						<?php $rs_teams = mysqli_query($conn,"Select max(year) as max_year, min(year) as min_year FROM tournaments WHERE league_id = ".$row['id']);
						$year_row = mysqli_fetch_assoc($rs_teams);?>
						<select name="year_select_stats" id="year_select_stats" style="width:100px;">
							<option value="0">Year</option>
							<?php for($i=$year_row['min_year'];$i<=$year_row['max_year'];$i++): ?>
								<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
							<?php endfor; ?>
						</select>
						
						<?php $rs_tournaments = mysqli_query($conn,"Select id,title from tournaments WHERE league_id = ".$row['id'].' ORDER BY title'); ?>
						<select name="tournament_select_stats" id="tournament_select_stats">
						<option value="0">All Tournaments</option>
						<?php while($row1 = mysqli_fetch_assoc($rs_tournaments)): ?>
							<option value="<?php echo $row1['id']; ?>"><?php echo $row1['title']; ?></option>
						<?php endwhile; ?>
						</select>
						
						<select name="game_types_select_stats" id="game_types_select_stats" style="width:120px;">
							<option value="0">Game Type</option>
							<?php foreach($game_type as $key=>$val):  ?>
								<option <?php echo $sel; ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
							<?php endforeach; ?>
						</select>
						<select name="overs_type_select_stats" id="overs_type_select_stats" style="width:100px;">
							<option value="0">Overs</option>
							<?php foreach($overs_type as $key=>$val):  ?>
								<option <?php echo $sel; ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
							<?php endforeach; ?>
						</select>
						<select name="ball_type_select_stats" id="ball_type_select_stats" style="width:100px;">
							<option value="0">Ball Type</option>
							<?php foreach($ball_type as $key=>$val):  ?>
								<option <?php echo $sel; ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
							<?php endforeach; ?>
						</select>
						
						<div class="clear"><br/></div>
						
						<?php $rs_teams = mysqli_query($conn,"Select distinct c.id,c.company_name from companies as c inner join tournament_teams as 
						tt on c.id=tt.team_id inner join tournaments as t on t.id = tt.tournament_id 
						WHERE t.league_id = ".$row['id'].' ORDER BY c.company_name'); ?>
						<select name="club_select_stats" id="club_select_stats">
						<option value="0">All Clubs</option>
						<?php while($row1 = mysqli_fetch_assoc($rs_teams)): ?>
							<option value="<?php echo $row1['id']; ?>"><?php echo $row1['company_name']; ?></option>
						<?php endwhile; ?>
						</select>
						
						<?php $rs_players = mysqli_query($conn,"Select distinct u.id,u.f_name,u.m_name,u.last_name from users as u 
						inner join club_members as cbm on cbm.member_id = u.id
						inner join tournament_teams as tt on cbm.club_id = tt.team_id
						inner join tournaments as t on t.id = tt.tournament_id 
						WHERE t.league_id = ".$row['id'].' ORDER BY u.f_name'); ?>
						<select name="player_select_stats" id="player_select_stats">
						<option value="0">All Players</option>
						<?php while($row1 = mysqli_fetch_assoc($rs_players)): ?>
							<option value="<?php echo $row1['id']; ?>"><?php echo $row1['f_name'].' '.$row1['last_name']; ?></option>
						<?php endwhile; ?>
						</select>
						<input type="button" name="stats_search" id="stats_search" class="submit-login" value="Search" style="float:none;margin-left:20px;">
						
						
					</div>
					
					<div id="statistics-data"></div>
					</div>
					
					<div id="by_laws" class="content2" style="width:98%;">
					<h2>By Laws</h2>
					<div class="clear"></div>
					<?php if(!empty($row['by_laws_file'])): ?>
						<?php $arr1 = explode(',',$row['by_laws_file']);$arr2 = explode(',',$row['by_laws_file_orig']);
						for($i=0;$i<count($arr1);$i++): ?>
							<a style="background:none;padding:5px;" target="_blank" href="<?=WWW?>by-laws-uploads/<?php echo $arr1[$i]; ?>"><h3 style="font-size:16px;"><?php echo $arr2[$i]; ?></h3></a>
						<?php endfor; ?>
					<?php else: ?>
						<div id="information">There are no By-Laws added by the respective Company</div>
					<?php endif; ?>
					</div>
					
					<div id="weekly_shots" class="content" style="width:98%;">
						<div class="clear"><br></div>
						<ul id="weekly_shots_tournaments" class="menu3">
						<?php $weekly_first_tournament = 0;$i=0;
						$sql = "SELECT id,year,title FROM tournaments WHERE league_id = ".$row['id']." AND year=".date('Y')." ORDER BY title";
						$rs_current_year_tournaments = mysqli_query($conn,$sql);
						while($row1 = mysqli_fetch_assoc($rs_current_year_tournaments)){?>
						<li class="weekly-shots-li"><a href="#weekly_shots-tab" id="t_<?php echo $row1['id'] ?>" class="weekly-shots-tour-link"><?php echo $row1['title'] ?></a></li>	
						<?php if($i==0) $weekly_first_tournament=$row1['id']; ?>
						<?php $i++; } ?>
						</ul>
						<div id="weekly-shots-content"></div>
					</div>
					
					
					<div id="clubs" class="content2" style="width:98%;">
					<div id="clubs-div">
					<h2>Clubs</h2>
				    
                	<?php  
					$query = "select distinct u.*,c.company_name,c.years_in_business,c.id as company_id from users u inner join companies as c on u.id=c.user_id inner join league_clubs as l on l.club_id=c.id where u.status = 1 and l.league_id=".$row['id'];
						$rs   = mysqli_query($conn,$query);
						if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
							echo '<div id="information">No record found ... !</div>';
						}
          			 
						$i= 0 ;
						while($row_clubs 	= mysqli_fetch_assoc($rs)){
                		

                		$row_img = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_clubs['id']."' and is_default = '1' "));
			  				$row_country= get_record_on_id('countries', $row_clubs['country_id']);	
			  				$location 	= $row_country['name'];
			  				$row_vid = mysqli_fetch_assoc(mysqli_query($conn,"select * from videos where entity_id = '".$row_clubs['id']."' and entity_type = 'users' and is_default = '1' and file_name != ''"));
		        		    ?>
		        		    
						<dl>
								<dt>
									
									<a href="javascript:;" onclick="hideClubs();" title="<?=$row_clubs['company_name']; ?>"><img src="<?php echo WWW;?><?=($row_img && !empty($row_img['file_name']))?'users/'.$row_clubs['id'].'/photos/'.$row_img['file_name']:'images/no-photo.jpg'?>" width="120" height="128" /></a></dt>
								<dd><div class="details">
									<h3><?=truncate_string($row_clubs['company_name'],20)?></h3>
									<p><? echo $location;?><br />
									
									<?if($row_clubs['user_type_id'] != 1)

									{
										echo get_combo('user_types','name',$row_clubs['user_type_id'],'','text');  

									?><br />
											 <?   if($row_clubs['user_type_id'] == 2){ echo $row_clubs['type'];} }?>
									
									
									</p>
									<a class="submit-login margin-top-5" href="javascript:;" onclick="hideClubs();" title="<?=$row_clubs['company_name'].' - '.$site_title?>">View Profile</a></div>
									<div class="flag"><img title="<?=$row_country['name']?>" alt="<?=$row_country['name']?>" src="<?php echo WWW;?>countries/<?=$row_country['flag']?>"  width="25"/></div>
									<div class="video">
										<? 
										$video = $row_vid['file_name'];
											if(!empty($video)){
						if(preg_match('/<iframe(.*)<\/iframe>/', $row_vid['file_name'])){ 
									
									preg_match('/src="(.*?)"/',$row_vid['file_name'] , $src);
									$src = $src[1];
									$video = "<iframe width='205' height='130' frameborder='0' allowfullscreen 
												src='$src'>
																	</iframe>";
									
									if(!preg_match('/height="(.*)"/', $video)){
										$video = preg_replace('/height="(.*)"/','height="130"',$video);	
									}else{
										$video = preg_replace('/width="(.*)"/','width="220" height="150"',$video);	
									}
						}else{	
												$filename = explode('.',$video);
												$filename1= $filename[0];
												$video = '<video width="220" height="130" controls>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mp4" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.ogg" type=\'video/ogg; codecs="theora, vorbis"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mov" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.webm" type=\'video/webm; codecs="vp8, vorbis"\'>
												</video>';
											}
										}else{
											$video = '<img src="'.WWW.'images/no-video.jpg" width="168" height="130">';
										}
										echo $video;?>									
									</div>
								</dd>
						</dl>
						<?php
                		
					      $i++;
					      $count++;
					      $x++;
					  } 
					  ?>
					
					</div>
					</div>
				
					<div id="venues" class="content1" style="width:98%;">
						<h2>Venues</h2>
						<?php $sql = "SELECT v.*,c.name as country_name FROM league_venues as v left join countries as c on v.country=c.id WHERE v.league_id=".$row['id']." ORDER BY v.id ";
							$rs_venues = mysqli_query($conn,$sql);
							while($row_venue = mysqli_fetch_assoc($rs_venues)){
								$venues[] = $row_venue;
							}
						?>
						<table  class="table-list">
						<tr><th>Venue</th><th>Address</th><th>State</th><th>City</th><th>Zip Code</th><th>Country</th><th>Contact Name</th></tr>
						<?php for($i=0;$i<count($venues);$i++): ?>
							<tr>
								<td><?php echo $venues[$i]['venue']; ?></td>
								<td><?php echo $venues[$i]['address']; ?></td>
								<td><?php echo $venues[$i]['state']; ?></td>
								<td><?php echo $venues[$i]['city']; ?></td>
								<td><?php echo $venues[$i]['zip_code']; ?></td>
								<td><?php echo $venues[$i]['country_name']; ?></td>
								<td><?php echo $venues[$i]['contact_name']; ?></td>
							</tr>
						<?php endfor; ?>
						<?php if(empty($venues)): ?>
						<tr><td colspan="4">No Records</td></tr>
						<?php endif; ?>
						</table>
					</div>
				
					<div id="umpires" class="content1" style="width:98%;">
					<div id="umpires-div">
					<h2>Umpires</h2>
                	<?  
						
                		$rpp = PRODUCT_LIMIT_FRONT; // results per page
                		
      					$query = "select distinct u.*,c.year_certified,c.umpire_phone from users u inner join league_umpires as c on u.id=c.umpire_id where u.status = 1 and c.league_id=".$row['id'];
						$query .= " order by u.id desc ";
				    
						$rs   = mysqli_query($conn,$query);
						if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
							echo '<div id="information">No record found ... !</div>';
						}
          			 
						$i= 0 ;
						while($row_umpires 	= mysqli_fetch_assoc($rs)){
                		$row_img = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_umpires['id']."' and is_default = '1' "));
			  				$row_country= get_record_on_id('countries', $row_umpires['country_id']);	
			  				$location 	= $row_country['name'];
			  				$row_vid = mysqli_fetch_assoc(mysqli_query($conn,"select * from videos where entity_id = '".$row_umpires['id']."' and entity_type = 'users' and is_default = '1' and file_name != ''"));
		        		    ?>
		        		    
						<dl>
								<dt>
									
									<a href="javascript:;" onclick="hideUmpires();" title="<?=$row_umpires['f_name'].' '.$row_umpires['m_name'].' '.$row_umpires['last_name']?>"><img src="<?php echo WWW;?><?=($row_img && !empty($row_img['file_name']))?'users/'.$row_umpires['id'].'/photos/'.$row_img['file_name']:'images/no-photo.jpg'?>" width="120" height="128" /></a></dt>
								<dd><div class="details">
									<h3 style="height:30px;"><?=truncate_string($row_umpires['f_name'],20).' '.truncate_string($row_umpires['last_name'], 20)?></h3>
									<p><? echo $location;?><br />
									
							<?if($row_umpires['user_type_id'] != 1)

							{
								echo get_combo('user_types','name',$row_umpires['user_type_id'],'','text');  

							?><br />
									 <?   if($row_umpires['user_type_id'] == 2){ echo $row_umpires['type'];} }?>
									
									
								
									</p>
									<a class="submit-login margin-top-5" href="javascript:;" onclick="hideUmpires();" title="<?=$row_umpires['f_name'].' '.$row_umpires['m_name'].' '.$row_umpires['last_name'].' - '.$site_title?>">View Profile</a></div>
									<div class="flag"><img title="<?=$row_country['name']?>" alt="<?=$row_country['name']?>" src="<?php echo WWW;?>countries/<?=$row_country['flag']?>"  width="25"/></div>
									<div class="video">
										<? 
										$video = $row_vid['file_name'];
											if(!empty($video)){
						if(preg_match('/<iframe(.*)<\/iframe>/', $row_vid['file_name'])){ 
									
									preg_match('/src="(.*?)"/',$row_vid['file_name'] , $src);
									$src = $src[1];
									$video = "<iframe width='205' height='130' frameborder='0' allowfullscreen 
												src='$src'>
																	</iframe>";
									
									if(!preg_match('/height="(.*)"/', $video)){
										$video = preg_replace('/height="(.*)"/','height="130"',$video);	
									}else{
										$video = preg_replace('/width="(.*)"/','width="220" height="150"',$video);	
									}
						}else{	
												$filename = explode('.',$video);
												$filename1= $filename[0];
												$video = '<video width="220" height="130" controls>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mp4" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.ogg" type=\'video/ogg; codecs="theora, vorbis"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mov" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.webm" type=\'video/webm; codecs="vp8, vorbis"\'>
												</video>';
											}
										}else{
											$video = '<img src="'.WWW.'images/no-video.jpg" width="168" height="130">';
										}
										echo $video;?>									
									</div>
								</dd>
						</dl>
						<?php
                		
					      $i++;
					      $count++;
					      $x++;
					  } 
					  ?>
					
					</div>
					</div>
					
					<?php } ?>
				<?php } ?>
				
			</div>
		</div>
	</div>


<script src="<?=WWW?>js/jquery.lightbox-0.5.min.js" type="text/javascript"></script>
<link href="<?=WWW?>css/jquery.lightbox-0.5.css" type="text/css" rel="stylesheet">
<script src="<?php echo WWW; ?>js/jquery-ui.min.js"></script>
<script>
$(document).ready(function(){
	//$('.menu3').tabify();
	$('#menu7_1').tabify();
	var t_id = '';
	$('#gallery a.photo').lightBox();
	$('.close-popup').click(function(){
		$('.popup').addClass('hide');
	});

	$('a.popup-link').click(function(){
		var link = $(this).attr('href');
		var height = $(window).scrollTop();
		$(link).css('top',height-150);
		$(link).removeClass('hide');
	});
	
	<?php if(in_array($row['company_type_id'],array(1,2,3,31,35))){ ?>
	displaySchedule = function(id){
		t_id = id;	
		$.ajax({
			url: "<?php echo WWW; ?>match-list-ajax.php?t_id="+id,
			type:"GET",
			success: function(data) {
				$("#tournament_list_div").hide();
				$("#tournament_matches_div").show().html(data);
				document.getElementById('tournament_matches_div').scrollIntoView(true);
			}
		});
	}
	
	$(".schedule_scorebook_link").bind('click',function(){
		$("#tournament_matches_div").hide();
		$("#tournament_list_div").show();
		<?php if($row['id'] == 76): ?>
			$("#contact-message").addClass('hide');
		<?php endif; ?>
	});
	
	hideScheduleByPoll=function(){
		$("#contact-message").removeClass('hide');
		$("#tournament_list_div").hide();
		$("#tournament_matches_div").html('').hide();
	}
	
	$(document).on('click', "#schedule_search" , function() {
		$(".table-outer").html("<center><img src='<?php echo WWW; ?>images/loading.gif'></center>");
		$(".right-div,.suwala-doubleScroll-scroll-wrapper").hide();
		var data = "start_day="+$("#start_day").val()+"&start_month="+$("#start_month").val()+"&team_group="+$("#team_group").val()+"&tournament_team="+$("#tournament_team").val()+"&tournament_venue="+$("#tournament_venue").val();
		$.ajax({
			url: "<?php echo WWW; ?>match-list-ajax.php?t_id="+t_id,
			type:"POST",
			data:data,
			success: function(data) {
				$("#tournament_list_div").hide();
				$("#tournament_matches_div").show().html(data);
			}
		});
	});
	
	displayStatistics = function(){
		$("#statistics-data").html("<center><img src='<?php echo WWW; ?>images/loading.gif'></center>").show();
		var data = "lge_id=<?php echo $row['id'] ?>&tour_id="+$("#tournament_select_stats").val()+"&year="+$("#year_select_stats").val()+"&game_type="+$("#game_types_select_stats").val()+"&overs_type="+$("#overs_type_select_stats").val()+"&ball_type="+$("#ball_type_select_stats").val()+"&club_id="+$("#club_select_stats").val()+"&player_id="+$("#player_select_stats").val();
		$.ajax({
			url: "<?php echo WWW; ?>league-statistics-ajax.php",
			type:"POST",
			data:data,
			success: function(data) {
				$("#statistics-data").html(data).show();
				$(".statistics-filters").show();
			}
		});
	}
	
	$(".stats-link").bind('click',function(){
		displayStatistics();
		<?php if($row['id'] == 76): ?>
			$("#contact-message").addClass('hide');
		<?php endif; ?>
	});
	
	$("#stats_search").bind('click',function(){
		displayStatistics();
	});
	
	displayStatistics();
	
	hideStatisticsByPlayer = function(){
		$("#contact-message").removeClass('hide');
		$("#statistics-data").html('').hide();
		$(".statistics-filters").hide();
	}
	
	<?php if($row['id'] == 76): ?>
		$(".by_laws_link,.weekly_shots_link,.clubs_link,.venues_link,.umpires_link").bind('click',function(){
			$("#contact-message").addClass('hide');
			if($(this).hasClass('clubs_link')){
				$("#clubs-div").show();
			}
			if($(this).hasClass('umpires_link')){
				$("#umpires-div").show();
			}
		});
	<?php endif; ?>
	
	displayWeeklyShots = function(tour_id){
		var data = "tour_id="+tour_id;
		$.ajax({
			url: "<?php echo WWW; ?>weeky-shots-ajax.php",
			type:"POST",
			data:data,
			success: function(data) {
				$("#t_"+tour_id).parent('li').addClass('active');
				$("#weekly-shots-content").html(data);
			}
		});
	}
	
	$(".weekly-shots-tour-link").bind('click',function(){
		var id = $(this).attr('id');
		var tour_id = id.replace('t_','');
		$(".weekly-shots-li").removeClass('active');
		$(this).parent('li').addClass('active');
		displayWeeklyShots(tour_id);
	});
	
	displayWeeklyShots(<?php echo $weekly_first_tournament; ?>);
	$(".weekly_shots_link").parent('li').addClass('active');
	$(".activity-link").parent('li').removeClass('active');
	$("#activity").hide();
	$("#weekly_shots").show();
	
	hideClubs = function(){
		$('#clubs-div').hide();
		$('#contact-message').removeClass('hide');
	}
	
	hideUmpires = function(){
		$('#umpires-div').hide();
		$('#contact-message').removeClass('hide');
	}
	
	<?php } ?>
	
	<?php if(in_array($row['company_type_id'],array(4))){ ?>
	displayClubStatistics = function(){
		var data = "club_id=<?php echo $row['id'] ?>&league_id="+$("#league_select_stats").val()+"&year="+$("#year_select_stats").val()+"&game_type="+$("#game_types_select_stats").val()+"&overs_type="+$("#overs_type_select_stats").val()+"&ball_type="+$("#ball_type_select_stats").val()+"&tour_id="+$("#tournament_select_stats").val();
		$.ajax({
			url: "<?php echo WWW; ?>club-statistics-ajax.php",
			type:"POST",
			data:data,
			success: function(data) {
				$("#club-statistics-data").html(data);
			}
		});
	}
	
	$("#club_stats_search").bind('click',function(){
		displayClubStatistics();
	});
	$(".club-stats-link").bind('click',function(){
		displayClubStatistics();
	});
	
	displayClubStatistics();

	<?php if(isset($_GET['club_members_search']) && !empty($_GET['club_members_search'])): ?>
		document.getElementById('club-member-search').scrollIntoView(true);
	<?php endif; ?>
	
	<?php } ?>
	
});	
</script>
<? include('common/footer.php');?>