<?php include('common/header.php'); ?>
	<div class="middle">
		<h1>League Detail - <?=get_combo('leagues','title',$_GET['id'],'','text')?> </h1>
		<div class="white-box content detail">
			<? 
				$row = get_record_on_id('leagues',$_GET['id']);
				$row_img= mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'leagues' and entity_id = '".$row_g['id']."'"));
			?>
			<p>
				<img src="<?=($row_img)?WWW.'leagues/'.$row['id'].'/'.$row_img['file_name']:WWW.'images/community-groups.png';?>" alt="<?=$row['title']?>" title="<?=$row['title']?>" width="350" />
				<div style="height:30px;">
					<!-- AddThis Button BEGIN -->
					<div class="addthis_toolbox addthis_default_style ">
					<a class="addthis_button_preferred_1"></a>
					<a class="addthis_button_preferred_2"></a>
					<a class="addthis_button_preferred_3"></a>
					<a class="addthis_button_preferred_4"></a>
					<a class="addthis_button_compact"></a>
					<a class="addthis_counter addthis_bubble_style"></a>
					</div>
					<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
					<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5073efa669740139"></script>
					<!-- AddThis Button END -->
				</div>
				<div><b>League City : </b><?=get_combo('cities','name',$row['city_id'],'','text')?></div>
				<div><b>Years In Service : </b><?=$row['year_in_service']?></div>
				<div><b>Email Address : </b><?=$row['email']?></div>
				<div class="space10"></div>
				<?=$row['description']?>
			</p>
		    <div class="clear"></div>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>
<?php include('common/footer.php'); ?>