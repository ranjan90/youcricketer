<?php include_once('includes/configuration.php');
$page = 'venues/edit';
$selected_country = getGeoLocationCountry(); 

$error = '';
if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
}

$sql = 'select * from companies where user_id = '.$user_id;
$rs_league = mysqli_query($conn,$sql);
$league_info = mysqli_fetch_assoc($rs_league);
$league_id = $league_info['id'];

$venue_id = trim($_GET['v_id']);
$venue_info = get_record_on_id('league_venues', $venue_id);	

$page_title = 'Edit Venue - '.ucwords($venue_info['title']);

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($user_id) && !empty($user_id)){
	if(!empty($league_info)){
		$venue_permission = 1;
	}else{
		$venue_permission = 0;
	}
}

$sql = "SELECT id,name FROM countries  ORDER BY id ";
$rs_countries = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_countries)){
	$countries[] = $row;
}

if(isset($_POST['submit_btn']) && !empty($_POST['submit_btn'])){
	validate();
	if(empty($error)){
		if(empty($_POST['country'])) $_POST['country'] = 0;
		
		$sql = " UPDATE league_venues SET venue='".trim($_POST['venue'])."',
		address='".trim($_POST['address'])."',country='".trim($_POST['country'])."',state='".trim($_POST['state'])."',
		city='".trim($_POST['city'])."', zip_code='".trim($_POST['zip_code'])."',contact_name='".trim($_POST['contact_name'])."',
		directions='".trim($_POST['directions'])."' WHERE id=$venue_id";
		
		if(mysqli_query($conn,$sql)){
			$_SESSION['venue_updated'] = 1;
			header("Location:".WWW."venues/list");
			exit();
		}else{
			$error = '<p id="error">Error in updating Venue. Try again later</p>';
		}
	}
}

function validate(){
	global $error;
	if(empty($_POST['venue'])){
		$error.= '<p id="error">Venue is required field</p>';
	}
	if(empty($_POST['address'])){
		$error.= '<p id="error">Address is required field</p>';
	}
}

?>
<?php include('common/header.php'); ?>
<style>
.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
dl {min-height:145px;}
.content {width:1010px;}
.large-column {width:790px;}
.form-box {width:50%;}
.chk_left {float:left !important; }
</style>
	<div class="middle">
		<h1> Edit Venue </h1>
		<h2><?php echo ucwords($league_info['company_name']); ?></h2>
		
		<div class="white-box content" id="dashboard">
			<?php if(empty($user_info)): ?>
				<div id="error">You are not logged... !</div>
			<?php endif; ?>
			
			<?php if(!empty($error)): ?>
				<?php echo $error; ?>
			<?php endif; ?>
		
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
				
				<?php if($venue_permission){ ?>
				<form method="post"  enctype="multipart/form-data">
					<input type="hidden" name="action" value="submit">
					<fieldset>
						<h2>Venue Details</h2>
						<p></p>
						<div class="form-box">
							<label>Venue</label>
							<div class="text"><input type="text" name="venue" id="venue" class="input-login" value="<?php if(!empty($_POST['venue'])) echo $_POST['venue'];else echo $venue_info['venue']; ?>"></div>
						</div>
						
						<div class="clear"></div>
						<div class="form-box">
							<label>Address</label>
							<div class="text"  style="width:75%;"><textarea style="width:90%;height:125px;" name="address" id="address" ><?php if(!empty($_POST['address'])) echo $_POST['address'];else echo $venue_info['address']; ?></textarea> </div>
						</div>
						
						<div class="clear"></div>
						<div class="form-box">
							<label>Country</label>
							<div class="text">
							<select name="country" id="country"> 
							<option value="0">Select One</option>
							<?php for($i=0;$i<count($countries);$i++): if(isset($_POST['country']) && $_POST['country'] == $countries[$i]['id']) $sel =  'selected'; elseif($venue_info['country'] == $countries[$i]['id']) $sel =  'selected'; else $sel = '';  ?>
								<option <?php echo $sel; ?> value="<?php echo $countries[$i]['id']; ?>"><?php echo $countries[$i]['name']; ?></option>
							<?php endfor; ?>
							</select>
							</div>
						</div>
						<div class="clear"></div>
						<div class="form-box">
							<label>State</label>
							<div class="text"><input type="text"  name="state" id="state" class="input-login" value="<?php if(!empty($_POST['state'])) echo $_POST['state'];else echo $venue_info['state']; ?>"></div>
						</div>
						<div class="clear"></div>
						<div class="form-box">
							<label>City</label>
							<div class="text"><input type="text"  name="city" id="city" class="input-login" value="<?php if(!empty($_POST['city'])) echo $_POST['city'];else echo $venue_info['city']; ?>"></div>
						</div>
						<div class="clear"></div>
						
						<div class="form-box">
							<label>Zip Code</label>
							<div class="text"><input type="text"  name="zip_code" id="zip_code" class="input-login" value="<?php if(!empty($_POST['zip_code'])) echo $_POST['zip_code'];else echo $venue_info['zip_code']; ?>"></div>
						</div>
						<div class="clear"></div>
						
						<div class="form-box">
							<label>Contact Name</label>
							<div class="text"><input type="text"  name="contact_name" id="contact_name" class="input-login" value="<?php if(!empty($_POST['contact_name'])) echo $_POST['contact_name'];else echo $venue_info['contact_name']; ?>"></div>
						</div>
						<div class="clear"></div>
						
						
						<div class="form-box">
							<label>Directions</label>
							<div class="text" style="width:75%;"><textarea name="directions" style="width:90%;height:125px;" id="directions" ><?php if(!empty($_POST['directions'])) echo $_POST['directions'];else echo $venue_info['directions']; ?></textarea> </div>
						</div>
						
						<div class="clear"></div>
						
						<div class="form-box">
							<input type="button" name="cancel_btn" value=" Cancel " class="submit-login" onclick="window.location.href='<?php echo WWW; ?>venues/list';">
							<input type="submit" name="submit_btn" value=" Submit " class="submit-login" >
						</div>
					</fieldset>
				</form>
				<?php }else{ ?>
				<div id="error">You do not have Permission for Venues... !</div>
				<?php } ?>
			</div>  
		    <div class="clear"></div>
			</div>
		</div>
		<!--<div class="rightbar"><?php //include('common/right-panel.php');?></div>-->
		<div class="clear"></div>
	</div>
	


<?php include('common/footer.php'); ?>