<?php 	include('common/header.php'); 
		//include 'espn/espn_function.php';
?>

<script src="<?=WWW?>js/jquery.cycle.pack.js" type="text/javascript"></script>

<script>
	$(document).ready(function(){
		$("li#companieslist").click(function(){
			$('#companies').show();
		});
		$('#small-banner-show').addClass('hide').delay(3000);
		$('#large-banner-show').addClass('hide').delay(10000);
		$('#small-banner-hide').removeClass('hide');
		$('#large-banner-hide').removeClass('hide');
		$('#all-banners-navigation').html('');
		$('.all-banners').cycle({
			fx:'fade',
			timeout:5000,
			speed:800,
			pause:1,
		});
		$('#all-banners').css('visibility','visible');		
		
		$("#news_content").html('<p>Loading News... </p>');
		$("#score_content").html('<p>Loading Scores... </p>');
		$("#results_content").html('<p>Loading Results... </p>');
		$("#fixtures_content").html('<p>Loading Fixtures... </p>');
		
        $.ajax({
			url: "<?php echo WWW; ?>espn/news.php?t=<?php echo time(); ?>",
			type:"GET",
			success: function(data) {
				$("#news_content").html(data);
			}
		});
		
		$.ajax({
			url: "<?php echo WWW; ?>espn/score.php?t=<?php echo time(); ?>",
			type:"GET",
			success: function(data) {
				$("#score_content").html(data);
			}
		});
		
		$.ajax({
			url: "<?php echo WWW; ?>espn/espn_results.php?t=<?php echo time(); ?>",
			type:"GET",
			success: function(data) {
				$("#results_content").html(data);
			}
		});
		
		$.ajax({
			url: "<?php echo WWW; ?>espn/espn_fixtures.php?t=<?php echo time(); ?>",
			type:"GET",
			success: function(data) {
				$("#fixtures_content").html(data);
			}
		});
		
	});
</script>
<style>
.content{width:830px;}
</style>

<div class="banner" id="activity-slider" >
	<p>Latest Greatest </p>
	<ul class="all-banners">
	<? 	/*$rsEvents 		= mysqli_query($conn,"select id,title from events where is_announcement = '1'");
		if(mysqli_num_rows($rsEvents) == 0){
		?>
		<li>
			<a href="<?php echo WWW;?>contact-us.html">Contact us for Live Streaming of your League games - We will beat anyone's price by US $500. This service is currently available only in USA</a>
		</li>
		
		<?
		}else{
			while($rowEvent = mysqli_fetch_assoc($rsEvents)){
			?>
				<li>
					<a <?php if(stripos($rowEvent['title'],'live streaming') === false): ?> href="<?=WWW?>event-detail-<?php echo $rowEvent['id'];?>-<?php echo friendlyURL($rowEvent['title']);?>.html" <?php else: ?> href="<?=WWW?>live-streaming.html"  <?php endif; ?> title="<?=$rowEvent['title']?>">
						<?=$rowEvent['title']?>
					</a>
				</li>
			<?	
			}
		}*/
		?><li>
		<a href="<?=WWW?>latest-greatest.html" >
			<?php $rowWhoopsMsg = get_record_on_id('cms', 45); echo $rowWhoopsMsg['content'];?>
		</a></li>
	</ul>
</div>
		<div class="login-area banner hide" id="small-banner-hide" style="float:left;width:320px; height:270px; ">
			<div class="all-banners">
				<img src="<?php echo WWW;?>images/top-right/map.jpg" alt="Map" />
				<img src="<?php echo WWW;?>images/top-right/express.jpg" alt="Express" />
				<img src="<?php echo WWW;?>images/top-right/connected.jpg" alt="Connected" />
				<img src="<?php echo WWW;?>images/top-right/shine.jpg" alt="Shine" />				
			</div>
			<div class="clear"></div>
		</div>
		<div class="login-area banner" id="small-banner-show" style="float:left;width:320px; height:270px;">
			<div class="all-banners">
				<img src="<?php echo WWW;?>images/top-right/map.jpg" alt="Map" />			
			</div>
			<div class="clear"></div>
		</div>
		
		<div class="banner hide" id="large-banner-hide" style="width:650px; height:270px;">
			<div class="all-banners">
				<?	$rs = mysqli_query($conn,"select * from sliding_banners where status = '1' order by sort_order");
					$x  = 0;
				while($row_banner = mysqli_fetch_assoc($rs)){
					if($x == 0){
						$loadPath = WWW.'banners/'.$row_banner['content'];
					}
					?>
					<a target="_blank" href="<?=$row_banner['web_url']?>" title="<?=$row_banner['title']?>">
						<img src="<?php echo WWW;?>banners/<?=$row_banner['content']?>" alt="<?=$row_banner['title']?>" />
					</a>
			<?	$x++;
					} ?>
			<?php
				$rsEvents 		= mysqli_query($conn,"select * from events where is_announcement = '1' and content like '<iframe%'");
				while($rowEEvent = mysqli_fetch_assoc($rsEvents)){
				?>
					<a target="_blank" href="<?=WWW?>event-detail-<?=$rowEEvent['id']?>-<?=friendlyURL($rowEEvent['title'])?>.html" title="<?=$rowEEvent['title']?>">
						<?php
							$videoContent 	= preg_replace('/height="(.*)"/','height="270"',$rowEEvent['content']);
							$videoContent 	= preg_replace('/width="(.*)"/','width="650"',$videoContent);
							preg_match('/height="(.*)"/', $videoContent, $matches);
							if(empty($matches)){
								$videoContent = preg_replace('/width="650"/', 'width="650" height="270"', $videoContent);
							}
						?>
						<?php echo $videoContent;?>
					</a>
				<?
				}
			?>
			</div>
		  </div>
		<div class="clear"></div>
		<div class="banner" id="large-banner-show" style="width: 650px; margin-top: -270px; height: 270px;">
			<div class="all-banners">
				<a target="_blank" href="" title="">
					<img src="<?=$loadPath?>" alt="YouCricketer">
				</a>
			</div>
			<div id="all-banners-navigation">
				<span style="color:#b3d1e8;"></span>			
			</div>
		  </div>
	<div class="middle">
		<div class="content">
		<div class="column1">
			<div class="white-box">
				<ul id="menu" class="menu">
					<li class="active"><a href="#individual">Individuals</a></li>
					<li id="companieslist"><a href="#companies">Clubs/Leagues/Companies</a></li>
			  </ul>
			  <div id="individual" class="content1" >
			  		<fieldset style="height:1288x; overflow:hidden;">
			  		<? 	//$oneMonthAgo = date("Y-m-d", strtotime(date("Y-m-d",strtotime(date("Y-m-d")))."-2 month"));
			  			$query 	= "select distinct id,f_name,m_name,last_name,user_type_id,type,country_id from users u where is_company = '0' and u.status = 1 order by rand() limit ".HOMEPAGE_USERS_LIMIT; //create_date >= '$oneMonthAgo' and 
			  			$rs_users = mysqli_query($conn,$query);
			  			while($row= mysqli_fetch_assoc($rs_users)){ 
			  				$row_img = mysqli_fetch_assoc(mysqli_query($conn,"select id,file_name from photos where entity_type = 'users' and entity_id = '".$row['id']."' and is_default = '1' "));
			  				$row_country= get_record_on_id('countries', $row['country_id']);	
			  				$location 	= $row_country['name'];
			  				$row_vid = mysqli_fetch_assoc(mysqli_query($conn,"select id,file_name from videos where entity_id = '".$row['id']."' and entity_type = 'users' and is_default = '1' and file_name != ''"));
		        		    ?>
		        		    
						<dl>
								<dt>
									<a href="<?=WWW?>individual-detail-<?=$row['id']?>-<?=friendlyURL($row['f_name'].' '.$row['m_name'].' '.$row['last_name'])?>.html#activity-tab" title="<?=$row['f_name'].' '.$row['m_name'].' '.$row['last_name']?>"><img src="<?php echo WWW;?><?=($row_img && !empty($row_img['file_name']))?'users/'.$row['id'].'/photos/'.$row_img['file_name']:'images/no-photo.jpg'?>" alt="no photo" width="120" height="128" /></a></dt>
								<dd><div class="details">
									<h3><?=truncate_string($row['f_name'],20).'<br>'.truncate_string($row['last_name'], 20)?></h3>
									<p><? echo $location;?><br />
									
<?if($row['user_type_id'] != 1)

{
    echo get_combo('user_types','name',$row['user_type_id'],'','text');  

?><br />
		 <?   if($row['user_type_id'] == 2){ echo $row['type'];} }?> <br/>			
								
									</p>
									<a class="submit-login margin-top-5" href="<?=WWW?>individual-detail-<?=$row['id']?>-<?=friendlyURL($row['f_name'].' '.$row['m_name'].' '.$row['last_name'])?>.html#activity-tab" title="<?=$row['f_name'].' '.$row['m_name'].' '.$row['last_name'].' - '.$site_title?>">View Profile</a></div>
									<div class="flag"><img title="<?=$row_country['name']?>" alt="<?=$row_country['name']?>" src="<?php echo WWW;?>countries/<?=$row_country['flag']?>"  width="25"/></div>
									<div class="video">
										<? 
										$video = $row_vid['file_name'];
											if(!empty($video)){
						if(preg_match('/<iframe(.*)<\/iframe>/', $row_vid['file_name'])){ 
									
									preg_match('/src="(.*?)"/',$row_vid['file_name'] , $src);
									$src = $src[1];
									$video = "<iframe width='205' height='130' frameborder='0' allowfullscreen src='$src'></iframe>";
									
									
						}else{	
												$filename = explode('.',$video);
												$filename1= $filename[0];
												$video = '<video width="220" height="130" controls>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mp4" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.ogg" type=\'video/ogg; codecs="theora, vorbis"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mov" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.webm" type=\'video/webm; codecs="vp8, vorbis"\'>
												</video>';
											}
										}else{
											$video = '<img src="'.WWW.'images/no-video.jpg" alt="no video" width="168" height="130">';
										}
										echo $video;?>									
									</div>
								</dd>
						</dl>
					<?	} ?>
					</fieldset>
					<a href="<?=WWW?>individuals.html" title="View all Individuals" class="right submit-login">View All</a>
			    </div>
				<div id="companies" class="content1" style="display:none !important;">
					<fieldset style="height:1288x; overflow:hidden;">
						<? 	$rs_users = mysqli_query($conn,"select c.*,co.name as country_name from companies c left join countries as co on c.country_id=co.id where c.status = 1 order by c.id desc limit ".HOMEPAGE_USERS_LIMIT);
			  			while($row= mysqli_fetch_assoc($rs_users)){ 
			  				$row_img = mysqli_fetch_assoc(mysqli_query($conn,"select id,file_name from photos where entity_type = 'users' and entity_id = '".$row['user_id']."' and is_default = '1' "));
			  				$row_u   = mysqli_fetch_assoc(mysqli_query($conn,"select city_id,country_id from users where id = '".$row['user_id']."'"));
			  				$row_vid = mysqli_fetch_assoc(mysqli_query($conn,"select id,file_name from videos where entity_id = '".$row['user_id']."' and entity_type = 'users' and is_default = '1' "));
			  				if(!empty($row_u['city_id'])){
			  					$row_city   = get_record_on_id('cities', $row_u['city_id']);
				            	$row_states = get_record_on_id('states', $row_city['state_id']);
				            	$row_country= get_record_on_id('countries', $row['country_id']);	
				            	if(!$row_country){
				            		$row_country= get_record_on_id('countries', $row_u['country_id']);	
				            	}
				            	$location   =  $row_city['name'].' > '.$row_states['name'].' > '.$row_country['name'];
			  				}else{
			  					$row_country= get_record_on_id('countries', $row['country_id']);
			  					$location   = $row_country['name']	;
			  				}
			  				$location   = $row_country['name']	;
		        		    ?>
						<dl>
								<dt><a href="<?=WWW?><?php echo $row['company_permalink']; ?>" title="<?=$row['company_name']?>">
										<img src="<?php echo WWW;?><?=($row_img && !empty($row_img['file_name']))?'users/'.$row['user_id'].'/photos/'.$row_img['file_name']:'images/no-photo.png'?>" alt="no photo" width="120" height="128" />
									</a>
								</dt>
								<dd><div class="details">
										<h3 style="height:22px;"><?=$row['company_name']?></h3>
										<p><?=get_combo('company_types','name',$row['company_type_id'],'','text')?></p>
										<p><? echo $location;?></p>
										<a class="submit-login margin-top-5" href="<?=WWW?><?php echo $row['company_permalink']; ?>" title="<?=$row['company_name'].' - '.$site_title?>">View Profile</a>
									</div>
									<div class="flag"><img alt="<?=$row_country['name']?>" title="<?=$row_country['name']?>" src="<?php echo WWW;?>countries/<?=$row_country['flag']?>" width="25"/></div>
									<div class="video">
										<? 
										$video = $row_vid['file_name']; 

  									
                              
										if(!empty($video)){
											if(preg_match('/<iframe(.*)><\/iframe>/', $video)){
							        			preg_match('/src="(.*?)"/',$row_vid['file_name'] , $src);
												$src = $src[1];
												$video = "<iframe width='205' height='130' frameborder='0' allowfullscreen src='$src'></iframe>";
											}else{
											$video_path = WWW.'videos/'.$row_vid['id'].'/'.$row_vid['file_name']; 
												$video = '<video width="150" height="130" controls>
												  <source src="'.$video_path.'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
									   			  <source src="'.$video_path.'" type=\'video/ogg; codecs="theora, vorbis"\'>
									 			  <source src="'.$video_path.'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
									 			  <source src="'.$video_path.'" type=\'video/webm; codecs="vp8, vorbis"\'>
											</video>';
											}
										}else{
											$video = '<img src="'.WWW.'images/no-video.jpg" alt="no video" width="168" height="130">';
										}
										echo $video;?>									
									</div>
									<div class='clear'></div>
								</dd>
						</dl>
					<?	} ?>
					<br>
					<?php if(isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])): ?>
						<a href="<?=WWW?>advance-search.html#companies-tab" title="View all Companies" class="right submit-login">View All</a>
					<?php else: ?>
						<a href="<?=WWW?>companies-sign-up.php" title="View all Companies" class="right submit-login">View All</a>
					<?php endif; ?>
					</fieldset>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		
		<div class="column2">
			<div class="white-box" >
				<ul id="menu2" class="menu">
					<li class="active"><a href="#news">News</a></li>
					<li><a href="#scoreboard">Score</a></li>
				</ul>
			 
				
				<div id="news" class="content1" style="min-height:330px;">
					<div id="news_content" style="min-height:400px;"><?php //include 'espn/news.php';?></div>
				<a href="<?=WWW;?>espn-news.html" class="submit-login"> View All </a>
				</div>
				
				<div id="scoreboard" class="content1 hide" style="min-height:240px;">
					<div id="score_content"><?php //include 'espn/score.php';?></div>
				</div>
				
				<div class="clear"></div>
			
			</div>
			<div class="white-box spacer" >
				<ul id="menu3" class="menu">
					<li class="active"><a href="#result">Results</a></li>
					<li><a href="#blog">Fixtures</a></li>
					
			  </ul>
				<div id="result" class="content1 ">
					<div class="slide10" id="slide10"> <img id="minus10" alt="International" src="<?=WWW?>images/icons/minus-small-white-icon.png">International </div>
					<div id="results_content" style="min-height:300px;"><?php //include('espn/espn_results.php');?></div>
					<a href="<?=WWW?>espn-results.html" class="submit-login"> View All </a>
					<?php //echo results($text);?>
				</div> 
				<div id="blog" class="content1" style="min-height:240px;">
					<div class="slide10" id="slide11"> <img id="minus11" alt="International" src="<?=WWW?>images/icons/minus-small-white-icon.png">International </div>
					<div id="fixtures_content"><?php //include('espn/espn_fixtures.php');?></div>
					<a href="<?=WWW?>espn-fixtures.html" class="submit-login"> View All </a>
						</div>
				<div class="clear"></div>
				
			</div>
			
			<div class="white-box spacer">
				<ul id="menu4" class="menu">
					<li class="active"><a href="#facebook">Facebook</a></li>
					<li><a href="#twitter">Twitter</a></li>
			  </ul>
				 <div id="facebook" class="content1" style="min-height:400px; #min-height:400px;">
					<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2FYouCricketer&amp;width=250&amp;height=400&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=true&amp;show_border=true"   style="border:none; overflow:hidden; width:250px; height:400px;"></iframe>
					
					</div>
				<div id="twitter" class="content1" style="min-height:400px;">
					<h2>Twitter</h2>
					<a class="twitter-timeline" href="https://twitter.com/YouCricketer" data-widget-id="409331659427700736">
						Tweets by @YouCricketer</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>


				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	<div class="rightbar">
		<?php include('common/right-panel.php');?>
	</div>
	<div class="clear"></div>
</div>
<style>
table#stats-index{margin-top:10px; }
table#stats-index td{padding:3px;}
</style>
<div id="video-stream" class="popup hide">
	<img src="http://dev1.youcricketer.com/images/icons/delete.png" alt="Delete" class="close-popup">
	<br />
	<a href="http://www.ustream.tv/" style="padding: 2px 0px 4px; width: 400px; background: #ffffff; display: block; color: #000000; font-weight: normal; font-size: 10px; text-decoration: underline; text-align: center;" 
	target="_blank">Live streaming video by Ustream</a>
</div>
<?php include('common/footer.php'); ?>