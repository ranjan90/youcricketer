<?php include('common/header.php');  ?>
	<div class="middle">
		<h1><?=(isset($_GET['type']) && $_GET['type'] == 'my')?'My':'YouCricketer';?> Community </h1>
		<div class="white-box content">
		<?php if(!isset($_SESSION['ycdc_user_email']) || empty($_SESSION['ycdc_user_email'])): ?>
			<div class="community-box" onclick="window.location='<?=WWW?>sign-up-community.html?page=blogs';">
				<h2>Blogs</h2>
				<img src="<?=WWW?>images/community-blog.png" alt="Community - Blog - <?=$site_title?>">
			</div>
			<div class="community-box" onclick="window.location='<?=WWW?>sign-up-community.html?page=forums';">
				<h2>Forums</h2>
				<img src="<?=WWW?>images/community-forum.png" alt="Community - Forum - <?=$site_title?>">
			</div>
			<div class="community-box" onclick="window.location='<?=WWW?>sign-up-community.html?page=groups';">
				<h2>Groups</h2>
				<img src="<?=WWW?>images/community-groups.png" alt="Community - Groups - <?=$site_title?>">
			</div>
			<div class="community-box" onclick="window.location='<?=WWW?>sign-up-community.html?page=testimonials';">
				<h2>Testimonials</h2>
				<img src="<?=WWW?>images/community-testimonials.png" alt="Community - Testimonials - <?=$site_title?>">
			</div>
			<div class="community-box" onclick="window.location='<?=WWW?>sign-up-community.html?page=member-news';">
				<h2>Whats Happening</h2>
				<img src="<?=WWW?>images/community-news.png" alt="Community - News - <?=$site_title?>">
			</div>
			<div class="community-box" onclick="window.location='<?=WWW?>sign-up-community.html?page=events';">
				<h2>Events</h2>
				<img src="<?=WWW?>images/community-events.png" alt="Community - Events - <?=$site_title?>">
			</div>
		<?php else: ?>
			<? if(!isset($_GET['type']) || $_GET['type'] != 'my'){ ?>
			<div class="community-box" onclick="window.location='<?=WWW?>blog.html';">
				<h2>Blogs</h2>
				<img src="<?=WWW?>images/community-blog.png" alt="Community - Blog - <?=$site_title?>">
			</div>
			<div class="community-box" onclick="window.location='<?=WWW?>forum.html';">
				<h2>Forums</h2>
				<img src="<?=WWW?>images/community-forum.png" alt="Community - Forum - <?=$site_title?>">
			</div>
			<div class="community-box" onclick="window.location='<?=WWW?>groups.html';">
				<h2>Groups</h2>
				<img src="<?=WWW?>images/community-groups.png" alt="Community - Groups - <?=$site_title?>">
			</div>
			<div class="community-box" onclick="window.location='<?=WWW?>testimonials.html';">
				<h2>Testimonials</h2>
				<img src="<?=WWW?>images/community-testimonials.png" alt="Community - Testimonials - <?=$site_title?>">
			</div>
			<div class="community-box" onclick="window.location='<?=WWW?>news.html';">
				<h2>Whats Happening</h2>
				<img src="<?=WWW?>images/community-news.png" alt="Community - News - <?=$site_title?>">
			</div>
			<div class="community-box" onclick="window.location='<?=WWW?>events.html';">
				<h2>Events</h2>
				<img src="<?=WWW?>images/community-events.png" alt="Community - Events - <?=$site_title?>">
			</div>
			<?php /* ?><div class="community-box" onclick="window.location='<?=WWW?>clubs.html';" style="height:270px;">
				<h2>Clubs</h2>
				<p>Clubs are also considered as companies</p>
				<img src="<?=WWW?>images/community-club.png" alt="Companies - Clubs - <?=$site_title?>">
			</div>
			<div class="community-box" onclick="window.location='<?=WWW?>leagues.html';" style="height:270px;">
				<h2>Leagues</h2>
				<p>Leagues are also considered as companies</p>
				<img src="<?=WWW?>images/community-league.png" alt="Companies - Leagues - <?=$site_title?>">
			</div>
			<div class="community-box" onclick="window.location='<?=WWW?>other-companies.html';" style="height:270px;">
				<h2>Other Companies</h2>
				<p>Except Clubs and Leagues</p>
				<img src="<?=WWW?>images/community-companies.png" alt="Other Community- <?=$site_title?>">
			</div><?php */ ?>
			
			<? } ?>
			<? if(isset($_GET['type']) && $_GET['type'] == 'my'){ ?>
			<div class="community-box" onclick="window.location='<?=WWW?>my-blog.html';">
				<h2>My Blogs</h2>
				<img src="<?=WWW?>images/community-blog.png" alt="Community - Blog - <?=$site_title?>">
			</div>
			<div class="community-box" onclick="window.location='<?=WWW?>my-forum.html';">
				<h2>My Forums</h2>
				<img src="<?=WWW?>images/community-forum.png" alt="Community - Forum - <?=$site_title?>">
			</div>
			<div class="community-box" onclick="window.location='<?=WWW?>my-groups.html';">
				<h2>My Groups</h2>
				<img src="<?=WWW?>images/community-groups.png" alt="Community - Groups - <?=$site_title?>">
			</div>
			<?php if($row_user['is_company'] == '1'): ?>
				<div  class="community-box" onclick="window.location='<?=WWW?>my-testimonials.html';">
					<h2>My Testimonials</h2>
					<img  src="<?=WWW?>images/community-testimonials.png" alt="Community - Testimonials - <?=$site_title?>">
				</div>
				<div class="community-box" onclick="window.location='<?=WWW?>my-news.html';">
					<h2>My News</h2>
					<img src="<?=WWW?>images/community-news.png" alt="Community - News - <?=$site_title?>">
				</div>
				<div class="community-box"  onclick="window.location='<?=WWW?>my-events.html';">
					<h2>My Events</h2>
					<img  src="<?=WWW?>images/community-events.png" alt="Community - Events - <?=$site_title?>">
				</div>
			<?php else: ?>
				<div style="width:46%;" class="community-box" onclick="window.location='<?=WWW?>my-testimonials.html';">
					<h2>My Testimonials</h2>
					<img style="margin-left:30%;" src="<?=WWW?>images/community-testimonials.png" alt="Community - Testimonials - <?=$site_title?>">
				</div>
				<?php /* ?><div class="community-box" onclick="window.location='<?=WWW?>my-news.html';">
					<h2>My News</h2>
					<img src="<?=WWW?>images/community-news.png" alt="Community - News - <?=$site_title?>">
				</div><?php */ ?>
				<div class="community-box" style="width:45%;" onclick="window.location='<?=WWW?>my-events.html';">
					<h2>My Events</h2>
					<img style="margin-left:28%;" src="<?=WWW?>images/community-events.png" alt="Community - Events - <?=$site_title?>">
				</div>
			<?php endif; ?>
			<? } ?>
		<?php endif; ?>

		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>

<?php include('common/footer.php'); ?>