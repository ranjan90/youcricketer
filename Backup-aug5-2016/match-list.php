<?php include_once('includes/configuration.php'); //error_reporting(E_ALL);
$page = 'match-list.html';
$selected_country = getGeoLocationCountry(); 

$league_clubs = array();
$league_info = array();
$matches = array();
$user_tournaments = array();
$groups = array();
$tournament_teams = array();
$permission_umpires = array();
$tournament_permission = 0;
$error = '';
$abandon_array = array('N/A','Forfeit by One Team','Late Arrival of One Team','Umpire(s) Did not Show Up');
$week_days = array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
$months = array('January','February','March','April','May','June','July','August','September','October','November','December');

if(isset($_GET['page']) && !empty($_GET['page'])){
	$page_no = str_replace('page','',trim($_GET['page']));
}else{
	$page_no = 1;
}

$records_per_page = 26;
$start_record  = ($page_no-1)*$records_per_page;

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
}

$tournament_id = trim($_GET['t_id']);
$tournament_info = get_record_on_id('tournaments', $tournament_id);	

$page_title = 'Tournament Schedule - '.ucwords($tournament_info['title']);

if(isset($user_id) && !empty($user_id)){
	$sql = "select * from tournament_permissions where tournament_id = $tournament_id and member_id = $user_id LIMIT 1";
	$rs_permission = mysqli_query($conn,$sql);
	if($tournament_info['user_id'] == $user_id || mysqli_num_rows($rs_permission)>0 ){
		$tournament_permission = 1;
	}else{
		$tournament_permission = 0;
	}
}else{
	$tournament_permission = 0;
}

$sql = "SELECT id FROM tournaments WHERE user_id = $user_id and status = 1 ORDER BY id DESC";
$rs_tournaments = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_tournaments)){
	$user_tournaments[] = $row['id'];
}

$sql = "SELECT member_id FROM tournament_permissions WHERE tournament_id = $tournament_id ";
$rs_permission = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_permission)){
	$permission_umpires[] = $row['member_id'];
}

if(empty($error) && isset($_GET['m_id']) && $_GET['action'] == 'delete'){
	$m_id = trim($_GET['m_id']);
	$del_match_info = get_record_on_id('tournament_matches', $m_id);	
	$del_tournament_id = $del_match_info['tournament_id'];
	
	if($tournament_info['user_id'] == $user_id){
		$sql = "DELETE FROM tournament_matches WHERE id=$m_id and tournament_id = $tournament_id";
		mysqli_query($conn,$sql);
		$sql = "DELETE FROM tournament_bat_scorecard WHERE match_id = $m_id ";
		mysqli_query($conn,$sql);
		$sql = "DELETE FROM tournament_bowl_scorecard WHERE match_id = $m_id ";
		mysqli_query($conn,$sql);
		$sql = "DELETE FROM tournament_fow_scorecard WHERE match_id = $m_id ";
		mysqli_query($conn,$sql);
		$_SESSION['match_deleted'] = 1;
	}
	header("Location:".WWW."tournament/matches/list/".$tournament_id);
	exit();
}

if(isset($_SESSION['match_deleted']) && $_SESSION['match_deleted']==1) {
	$match_deleted = 1;
	unset($_SESSION['match_deleted']);
}
if(isset($_SESSION['match_updated']) && $_SESSION['match_updated']==1) {
	$match_updated = 1;
	unset($_SESSION['match_updated']);
}
if(isset($_SESSION['match_header_updated']) && $_SESSION['match_header_updated']==1) {
	$match_header_updated = 1;
	unset($_SESSION['match_header_updated']);
}
if(isset($_SESSION['match_added']) && $_SESSION['match_added']==1) {
	$match_added = 1;
	unset($_SESSION['match_added']);
}
if(isset($_SESSION['match_highlight_updated']) && $_SESSION['match_highlight_updated']==1) {
	$match_highlight_updated = 1;
	unset($_SESSION['match_highlight_updated']);
}

$where = '';//var_dump($_GET);
if(isset($_GET['start_day']) && !empty($_GET['start_day'])){
	$where.=" AND tm.start_day = '".trim($_GET['start_day'])."'";
}
if(isset($_GET['start_month']) && !empty($_GET['start_month'])){
	$where.=" AND MONTH(tm.start_time) = '".trim($_GET['start_month'])."'";
}
if(isset($_GET['team_group']) && !empty($_GET['team_group'])){
	$where.=" AND (tt1.group_name = '".trim($_GET['team_group'])."' OR tt2.group_name = '".trim($_GET['team_group'])."') ";
}
if(isset($_GET['tournament_team']) && !empty($_GET['tournament_team'])){
	$where.=" AND (tm.team1 = '".trim($_GET['tournament_team'])."' OR tm.team2 = '".trim($_GET['tournament_team'])."') ";
}
if(isset($_GET['tournament_venue']) && !empty($_GET['tournament_venue'])){
	$where.=" AND tm.venue_id = '".trim($_GET['tournament_venue'])."'";
}

$sql = "SELECT count(*) as record_count FROM tournament_matches as tm 
inner join companies as c1 on tm.team1=c1.id  
inner join companies as c2 on tm.team2=c2.id
inner join tournament_teams as tt1 on tm.team1 = tt1.team_id
inner join tournament_teams as tt2 on tm.team2 = tt2.team_id
left join users as u1 on tm.umpire1_id = u1.id
 WHERE tm.tournament_id = $tournament_id and tt1.tournament_id = $tournament_id and tt2.tournament_id = $tournament_id and tm.status = 1 ".$where;
$rs_total = mysqli_query($conn,$sql);
$row_total = mysqli_fetch_assoc($rs_total);
$records_count = $row_total['record_count'];

$sql = "SELECT tm.*,c1.company_name as team1,c2.company_name as team2,u1.f_name as umpire1_fname, u1.last_name as umpire1_lname,tt1.group_name as team1_group,
tt2.group_name as team2_group,lv.venue,c3.company_name as umpire1_from_name,u2.f_name as umpire2_fname, u2.last_name as umpire2_lname,c4.company_name as umpire2_from_name,
u3.f_name as umpire3_fname, u3.last_name as umpire3_lname,c5.company_name as umpire3_from_name,u4.f_name as scorer1_fname, u4.last_name as scorer1_lname,
u5.f_name as scorer2_fname, u5.last_name as scorer2_lname,u6.f_name as referee_fname, u6.last_name as referee_lname, l1.umpire_phone as umpire1_phone,
l2.umpire_phone as umpire2_phone, l3.umpire_phone as umpire3_phone, l4.umpire_phone as scorer_1_phone, l5.umpire_phone as scorer_2_phone, l6.umpire_phone as referee_phone
FROM tournament_matches as tm 
inner join companies as c1 on tm.team1=c1.id  
inner join companies as c2 on tm.team2=c2.id 
inner join tournament_teams as tt1 on tm.team1 = tt1.team_id
inner join tournament_teams as tt2 on tm.team2 = tt2.team_id
left join users as u1 on tm.umpire1_id = u1.id
left join companies as c3 on tm.umpire1_from = c3.id
left join users as u2 on tm.umpire2_id = u2.id
left join companies as c4 on tm.umpire2_from = c4.id
left join users as u3 on tm.umpire3_id = u3.id
left join companies as c5 on tm.umpire3_from = c5.id
left join users as u4 on tm.scorer_1 = u4.id
left join users as u5 on tm.scorer_2 = u5.id
left join users as u6 on tm.referee_id = u6.id 
left join league_venues as lv on tm.venue_id = lv.id 
left join league_umpires as l1 on l1.umpire_id = tm.umpire1_id
left join league_umpires as l2 on l2.umpire_id = tm.umpire2_id
left join league_umpires as l3 on l3.umpire_id = tm.umpire3_id
left join league_umpires as l4 on l4.umpire_id = tm.scorer_1
left join league_umpires as l5 on l5.umpire_id = tm.scorer_2
left join league_umpires as l6 on l6.umpire_id = tm.referee_id
 WHERE tm.tournament_id = $tournament_id and tt1.tournament_id = $tournament_id and tt2.tournament_id = $tournament_id
 and tm.status = 1 $where ORDER BY tm.start_time,tm.id ASC ";

if(empty($where)) {
	$sql.=" LIMIT $start_record,$records_per_page";
}
$rs_matches = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_matches)){
	$matches[] = $row;
}

$sql = "SELECT tt.group_name,c.id,c.company_name as team_name FROM tournament_teams as tt inner join companies as c on tt.team_id=c.id  
WHERE tt.tournament_id = $tournament_id and c.status = '1' ORDER BY c.company_name";
$rs_teams = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_teams)){
	$tournament_teams[] = $row;
	if(!in_array($row['group_name'],$groups)){
		$groups[] = $row['group_name'];
	}
}

$sql = "SELECT id,venue FROM league_venues WHERE league_id = ".$tournament_info['league_id']." ORDER BY venue";
$rs_teams = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_teams)){
	$venues[] = $row;
}

?>
<?php include('common/header.php'); ?>
<style>
.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
dl {min-height:145px;}
.content {width:1010px;}
.wrapper{width:1020px;}
.large-column {width:827px !important; margin-left:3px !important;}
#table-list {width:100%;border:1px solid #ccc;}
#table-list tr{border:1px solid #ccc;border-right:none;}
#table-list th{padding:5px;background-color:#000;color:#FB7008;font-size:14px;height:45px;}
#table-list td{padding:5px;text-align:center;vertical-align:middle;word-break:break-all;}
#table-list a{color:#000;}

.add_tournament{font-size:14px;color:#000;}
.table-outer{overflow-x:scroll;overflow-y:visible;width:80%;margin-left: 0px;float:left;}
.right-div{width:19%;float:left;}
.action-table tr:last-child td {height:75px;}
.content-div{height:52px;font-size:13px;}
.schedule-filters{float:left;}
</style>
	<div class="middle">
		<h1> Tournament Schedule</h1>
		<h2> Tournament: <?php echo $tournament_info['title']; ?>  </h2>
		<form>
		<div class="schedule-filters">
			<select name="start_day" id="start_day" style="width:130px;margin-left:5px;">
				<option value="0">All Days</option>
				<?php for($i=0;$i<count($week_days);$i++): ?>
					<?php if(isset($_GET['start_day']) && $_GET['start_day'] == $week_days[$i]) $sel = 'selected';else $sel = ''; ?>
					<option <?php echo $sel; ?> value="<?php echo $week_days[$i]; ?>"><?php echo $week_days[$i]; ?></option>
				<?php endfor; ?>
			</select>
			<select name="start_month" id="start_month" style="width:130px;">
				<option value="0">All Months</option>
				<?php for($i=0;$i<count($months);$i++): ?>
					<?php if(isset($_GET['start_month']) && $_GET['start_month'] == $i+1) $sel = 'selected';else $sel = ''; ?>
					<option <?php echo $sel; ?> value="<?php echo $i+1; ?>"><?php echo $months[$i]; ?></option>
				<?php endfor; ?>
			</select>
			<select name="team_group" id="team_group" style="width:100px;">
				<option value="0">All Groups</option>
				<?php for($i=0;$i<count($groups);$i++): ?>
					<?php if(isset($_GET['team_group']) && $_GET['team_group'] == $groups[$i]) $sel = 'selected';else $sel = ''; ?>
					<option <?php echo $sel; ?> value="<?php echo $groups[$i]; ?>"><?php echo $groups[$i]; ?></option>
				<?php endfor; ?>
			</select>
			<select name="tournament_team" id="tournament_team" style="width:230px;">
				<option value="0">All Teams</option>
				<?php for($i=0;$i<count($tournament_teams);$i++): ?>
					<?php if(isset($_GET['tournament_team']) && $_GET['tournament_team'] == $tournament_teams[$i]['id']) $sel = 'selected';else $sel = ''; ?>
					<option <?php echo $sel; ?> value="<?php echo $tournament_teams[$i]['id']; ?>"><?php echo $tournament_teams[$i]['team_name']; ?></option>
				<?php endfor; ?>
			</select>
			<select name="tournament_venue" id="tournament_venue" style="width:200px;">
				<option value="0">All Venues</option>
				<?php for($i=0;$i<count($venues);$i++): ?>
					<?php if(isset($_GET['tournament_venue']) && $_GET['tournament_venue'] == $venues[$i]['id']) $sel = 'selected';else $sel = ''; ?>
					<option <?php echo $sel; ?> value="<?php echo $venues[$i]['id']; ?>"><?php echo $venues[$i]['venue']; ?></option>
				<?php endfor; ?>
			</select>
			<input type="submit" name="schedule_search" id="schedule_search" class="submit-login" value="Search" style="float:none;margin-left:5px;">
		</div>		
		</form>
		
		<div class="clear"><br/></div>			
		<div class="white-box content" id="dashboard">
			<?php if(isset($match_deleted) && $match_deleted == 1): ?>
				<div id="information">Match deleted Successfully... !</div>
			<?php endif; ?>
			<?php if(isset($match_updated) && $match_updated == 1): ?>
				<div id="information">Match updated Successfully... !</div>
			<?php endif; ?>
			<?php if(isset($match_header_updated) && $match_header_updated == 1): ?>
				<div id="information">Match Header/Basic Information Updated Successfully. Please proceed to Detail Scoreboard Innings 1, Innings 2 and Match Highlights...!</div>
			<?php endif; ?>
			<?php if(isset($match_added) && $match_added == 1): ?>
				<div id="information">Match added Successfully... !</div>
			<?php endif; ?>
			
			<?php if(isset($match_highlight_updated) && $match_highlight_updated == 1): ?>
				<div id="information">Match Highlights Updated Successfully... !</div>
			<?php endif; ?>
		
			<?php if(empty($user_info)): ?>
				<div id="error">You are not logged... !</div>
			<?php endif; ?>
			
			<?php if(!empty($error)): ?>
				<?php echo $error; ?>
			<?php endif; ?>
			
			
		
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
			<?php if($tournament_permission){ ?>
			<?php if($tournament_info['user_id'] == $user_id): ?>
				<p style="float:right;"><a href="<?php echo WWW; ?>tournament/matches/add/<?php echo $tournament_id; ?>" class="add_tournament"><img alt="Add New Tournament" src="<?php echo WWW; ?>images/icons/add.png" border="0"> Add New Match Schedule</a></p>
			<?php endif; ?>
			 <div class="clear"></div>
				<div class="table-outer">
				<table id="table-list" class="white-box" style="min-width:1700px;border-right:none;">
				<thead >
				<tr><th>Game<br>Nbr</th><th>Date/Time</th><th>Day</th><th>Host Team</th><th>Group</th><th>Guest Team</th><th>Group</th><th>Venue</th><th>1st Umpire</th><th>Phone No</th><th>Umpire1 From</th>
				<th>2nd Umpire</th><th>Phone No</th><th>Umpire2 From</th><th>3rd Umpire</th><th>Phone No</th><th>Umpire3 From</th><th>1st Scorer</th><th>Phone No</th><th>2nd Scorer</th><th>Phone No</th><th>Match Referee</th><th>Phone No</th></tr>
				</thead>
				<tbody>
				<?php for($i=0;$i<count($matches);$i++): ?>
					<tr>
						<td><div class="content-div"><?php echo $matches[$i]['game_number']; ?></div></td>
						<td><div class="content-div" style="min-width:75px;"><?php echo date('d M Y',strtotime($matches[$i]['start_time'])); ?><br/><?php echo date('H:i:s',strtotime($matches[$i]['start_time'])); ?> </div></td>
						<td><div class="content-div"><?php if(!empty($matches[$i]['start_day'])) echo substr($matches[$i]['start_day'],0,3);else echo date('D',strtotime($matches[$i]['start_time'])); ?> </div></td>
						<td><div class="content-div"><?php echo $matches[$i]['team1']; ?></div></td>
						<td><div class="content-div"><?php echo $matches[$i]['team1_group']; ?></div></td>
						<td><div class="content-div"><?php echo $matches[$i]['team2']; ?></div></td>
						<td><div class="content-div"><?php echo $matches[$i]['team2_group']; ?></div></td>
						<td><div class="content-div"><?php echo $matches[$i]['venue']; ?></div></td>
						<td><div class="content-div"><?php echo $matches[$i]['umpire1_fname']; ?> <?php echo $matches[$i]['umpire1_lname']; ?></div></td>
						<td><div class="content-div"><?php if(in_array($matches[$i]['umpire1_id'],$permission_umpires)) echo $matches[$i]['umpire1_phone']; ?> </div></td>
						<td><div class="content-div"><?php if($matches[$i]['umpire1_from'] == -1) echo 'Independent'; else echo $matches[$i]['umpire1_from_name']; ?></div></td>
						<td><div class="content-div"><?php echo $matches[$i]['umpire2_fname']; ?> <?php echo $matches[$i]['umpire2_lname']; ?></div></td>
						<td><div class="content-div"><?php if(in_array($matches[$i]['umpire2_id'],$permission_umpires)) echo $matches[$i]['umpire2_phone']; ?> </div></td>
						<td><div class="content-div"><?php if($matches[$i]['umpire2_from'] == -1) echo 'Independent'; else echo $matches[$i]['umpire2_from_name']; ?></div></td>
						<td><div class="content-div"><?php echo $matches[$i]['umpire3_fname']; ?> <?php echo $matches[$i]['umpire3_lname']; ?></div></td>
						<td><div class="content-div"><?php if(in_array($matches[$i]['umpire3_id'],$permission_umpires)) echo $matches[$i]['umpire3_phone']; ?> </div></td>
						<td><div class="content-div"><?php if($matches[$i]['umpire3_from'] == -1) echo 'Independent'; else echo $matches[$i]['umpire3_from_name']; ?></div></td>
						<td><div class="content-div"><?php echo $matches[$i]['scorer1_fname']; ?> <?php echo $matches[$i]['scorer1_lname']; ?></div></td>
						<td><div class="content-div"><?php if(in_array($matches[$i]['scorer_1'],$permission_umpires)) echo $matches[$i]['scorer_1_phone']; ?> </div></td>
						<td><div class="content-div"><?php echo $matches[$i]['scorer2_fname']; ?> <?php echo $matches[$i]['scorer2_lname']; ?></div></td>
						<td><div class="content-div"><?php if(in_array($matches[$i]['scorer_2'],$permission_umpires)) echo $matches[$i]['scorer_2_phone']; ?> </div></td>
						<td><div class="content-div"><?php echo $matches[$i]['referee_fname']; ?> <?php echo $matches[$i]['referee_lname']; ?></div></td>
						<td><div class="content-div"><?php if(in_array($matches[$i]['referee_id'],$permission_umpires)) echo $matches[$i]['referee_phone']; ?> </div></td>
					</tr>
					<?php if($i && ($i+1)%9 ==0): ?>
						<tr><th>Game<br>Nbr</th><th>Date/Time</th><th>Day</th><th>Host Team</th><th>Group</th><th>Guest Team</th><th>Group</th><th>Venue</th><th>1st Umpire</th><th>Phone No</th><th>Umpire1 From</th>
						<th>2nd Umpire</th><th>Phone No</th><th>Umpire2 From</th><th>3rd Umpire</th><th>Phone No</th><th>Umpire3 From</th><th>1st Scorer</th><th>Phone No</th><th>2nd Scorer</th><th>Phone No</th><th>Match Referee</th><th>Phone No</th></tr>
					<?php endif; ?>
				<?php endfor; ?>
				</tbody>
				<?php if(empty($matches)): ?>
				<tr><td colspan="4">No Records</td></tr>
				<?php endif; ?>
				</table>
				</div>
				<div class="right-div">
				<table id="table-list" class="white-box action-table" style="border-left:none;">
					<tr style="border-left:none;"><th>Action</th></tr>
				<?php for($i=0;$i<count($matches);$i++): ?>
					<tr style="border-left:none;">
					<td ><div style="height:52px;float:left;margin:0px;">
						<a href="<?php echo WWW; ?>tournament/matches/match-header/<?php echo $matches[$i]['tournament_id']; ?>/<?php echo $matches[$i]['id']; ?>" title="Match Header"><img alt="Match Header" src="<?php echo WWW; ?>images/icons/posted-on.png" border="0"></a>&nbsp;
						<?php if(empty($matches[$i]['completely_abandon_due_to']) || in_array($matches[$i]['completely_abandon_due_to'],$abandon_array)): ?>	
							<a href="<?php echo WWW; ?>tournament/matches/batting/<?php echo $matches[$i]['tournament_id']; ?>/<?php echo $matches[$i]['id']; ?>/1" title="Scoreboard Innings 1"><img alt="Scoreboard Innings 1" src="<?php echo WWW; ?>images/icons/posted-on.png" border="0"></a>&nbsp;
							<a href="<?php echo WWW; ?>tournament/matches/batting/<?php echo $matches[$i]['tournament_id']; ?>/<?php echo $matches[$i]['id']; ?>/2" title="Scoreboard Innings 2" ><img alt="Scoreboard Innings 2" src="<?php echo WWW; ?>images/icons/posted-on.png" border="0"></a>&nbsp;
						<?php else: ?>	
							<a href="javascript:;" onclick="alert('Match Abandoned due to <?php echo $matches[$i]['completely_abandon_due_to']; ?>');" title="Scoreboard Innings 1"><img alt="Scoreboard Innings 1" src="<?php echo WWW; ?>images/icons/posted-on.png" border="0"></a>&nbsp;
							<a href="javascript:;" onclick="alert('Match Abandoned due to <?php echo $matches[$i]['completely_abandon_due_to']; ?>');" title="Scoreboard Innings 2" ><img alt="Scoreboard Innings 2" src="<?php echo WWW; ?>images/icons/posted-on.png" border="0"></a>&nbsp;
						<?php endif; ?>
						<a href="<?php echo WWW; ?>tournament/matches/edit/<?php echo $matches[$i]['tournament_id']; ?>/<?php echo $matches[$i]['id']; ?>" title="Match Highlights"><img alt="Match Highlights" src="<?php echo WWW; ?>images/icons/posted-on.png" border="0"></a>&nbsp;
						<a href="<?php echo WWW; ?>tournament/matches/scoresheet/<?php echo $matches[$i]['id']; ?>" title="View Scoresheet"><img alt="View Scoresheet" src="<?php echo WWW; ?>images/icons/posted-on.png" border="0"></a>&nbsp;&nbsp;
						<br/>
						<?php if(empty($matches[$i]['completely_abandon_due_to']) || in_array($matches[$i]['completely_abandon_due_to'],$abandon_array)): ?>	
							<a href="<?php echo WWW; ?>tournament/matches/commentary/<?php echo $matches[$i]['id']; ?>/<?php echo $matches[$i]['batting_team1']; ?>"  title="Commentary Innings 1"> <img alt="Commentary Innings 1" src="<?php echo WWW; ?>images/icons/commentary.png" border="0"></a>&nbsp;&nbsp;
							<a href="<?php echo WWW; ?>tournament/matches/commentary/<?php echo $matches[$i]['id']; ?>/<?php echo $matches[$i]['batting_team2']; ?>" title="Commentary Innings 2"> <img alt="Commentary Innings 2" src="<?php echo WWW; ?>images/icons/commentary.png" border="0"></a>&nbsp;&nbsp;
						<?php else: ?>
							<a href="javascript:;" onclick="alert('Match Abandoned due to <?php echo $matches[$i]['completely_abandon_due_to']; ?>');"  title="Commentary Innings 1"> <img alt="Commentary Innings 1" src="<?php echo WWW; ?>images/icons/commentary.png" border="0"></a>&nbsp;&nbsp;
							<a href="javascript:;" onclick="alert('Match Abandoned due to <?php echo $matches[$i]['completely_abandon_due_to']; ?>');" title="Commentary Innings 2"> <img alt="Commentary Innings 2" src="<?php echo WWW; ?>images/icons/commentary.png" border="0"></a>&nbsp;&nbsp;		
						<?php endif; ?>
						<a href="<?php echo WWW; ?>tournament/matches/gallery/<?php echo $matches[$i]['id']; ?>" title="Photo Gallery"><img alt="Photo Gallery" src="<?php echo WWW; ?>images/icons/photos.png" border="0"></a>&nbsp;&nbsp;
						<a href="<?php echo WWW; ?>tournament/matches/videos/list/<?php echo $matches[$i]['id']; ?>" title="Videos"><img alt="Videos" style="width:16px;height:16px;" src="<?php echo WWW; ?>images/icons/video.png" border="0"></a>&nbsp;&nbsp;
						<a href="<?php echo WWW; ?>tournament/matches/match-edit-basic/<?php echo $matches[$i]['tournament_id']; ?>/<?php echo $matches[$i]['id']; ?>" title="Edit Match Schedule"><img alt="Edit Match Schedule" src="<?php echo WWW; ?>images/icons/edit.png" border="0"></a>&nbsp;
						<?php if($tournament_info['user_id'] == $user_id): ?>
							<a href="<?php echo WWW; ?>match-list.php?m_id=<?php echo $matches[$i]['id']; ?>&t_id=<?php echo $matches[$i]['tournament_id']; ?>&action=delete" onclick="return confirm('Are you sure to delete match');" title="Delete"><img alt="Delete" src="<?php echo WWW; ?>images/icons/delete.png" border="0"></a>&nbsp;&nbsp;
						<?php endif; ?>
						</div>
					</td>
					</tr>
					<?php if($i && ($i+1)%9 ==0): ?>
						<tr style="border-left:none;"><th >&nbsp;</th></tr>
					<?php endif; ?>
				<?php endfor; ?>	
				</table>
				</div>
				<?php }else{ ?>
				<div id="error">You do not have Permission for this Tournament... !</div>
				<?php } ?>
				<div class="clear"></div>
				
				<?php if(empty($where)): ?>
					<div id="pagination-bottom">
						<?php  $paging_str = getPaging('tournament/matches/list/'.$tournament_id,$records_count,$page_no,$records_per_page);
						echo $paging_str;
						?>
						<div class="clear"></div>
					</div>  
				<?php endif; ?>
			
			</div>  
			<div class="clear"></div>
			
			
		</div>
	</div>
		<!--<div class="rightbar"><?php //include('common/right-panel.php');?></div>-->
		<div class="clear"></div>
	</div>
<script src="<?php echo WWW; ?>js/jquery-ui.min.js"></script>	
<script src="<?php echo WWW; ?>js/jquery.doubleScroll.js"></script>	
<script type="text/javascript">
    $(document).ready(function(){
		$('.table-outer').doubleScroll();
    });
</script>



<?php include('common/footer.php'); ?>