<?php include('common/header.php'); ?>
	<div class="middle">
		<h1>Blog Detail - <?=get_combo('blog_articles','title',$_GET['id'],'','text')?> </h1>
		<div class="white-box content detail">
			<? 
				$row = get_record_on_id('blog_articles',$_GET['id']);
				if(!$row){
					?>
					<script>
					window.location = '<?php echo WWW;?>blogs.html';
					</script>
					<?
				}
				$rowC= get_record_on_id('blog_categories',$row['category_id']);
				$rowU= get_record_on_id('users',$row['user_id']);
				$row_img= mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'groups' and entity_id = '".$row_g['id']."'"));
			?>
			<?
			if(isset($_POST['action']) && $_POST['action'] == 'comment' && isset($_SESSION['ycdc_user_name']) && !empty($_SESSION['ycdc_user_name'])){
					$comments 	= addslashes($_POST['comments']);
					$user_id 	= $_SESSION['ycdc_dbuid'];
					$date 		= date('Y-m-d H:i:s');
					$topic_id 	= $_GET['id'];

					$rs_chk 	= mysqli_query($conn,"select * from blog_comments where user_id = '$user_id' and blog_article_id = '$topic_id' and comments = '$comments'");
					if(mysqli_num_rows($rs_chk) == 0){
						$query 		= "insert into blog_comments (comments, blog_article_id, user_id, comment_date, status) 
									values ('$comments','$topic_id','$user_id','$date','1')";	
						if(mysqli_query($conn,$query)){
							?><div id="success">Comments saved successfully ... !</div><?
						}else{
							?><div id="success">Comments already submitted ... !</div><?
						}
					}
					
				}
			?>
			<h2><?=get_combo('blog_articles','title',$row['id'],'','text')?></h2>
			<table width="100%">
				<tr>
					<td><b>Created By : </b></td><td><?php echo (!isset($rowU['f_name']))?'Administrator':$rowU['f_name'];?></td>
					<td><b>Created On : </b></td><td><?php echo date_converter($row['create_date'])." ".date("H:i:s",strtotime($row['create_date']))?></td>
					<td><b>Category : </b></td><td><?php echo $rowC['name']?></td>
				</tr>
			</table>
			<?php if(!empty($row['series_match_id'])){ ?>
				<div style="margin-left:50px; height:150px; margin-top:15px; margin-bottom:30px;">
					<a class="popup-link" href="#blogging-contest-rules" >
						<img src="<?php echo WWW;?>images/world-cup-cricket-blogging-contest-100-doller.jpg">
					</a>
				</div>
			<?php } ?>
		    
		    <?php if(!empty($row['series_match_id'])){ ?>
		    	<?php if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){ ?>
				    <div style="width:500px; float:left">
				    	<a class="submit-login" href="<?php echo WWW;?>add-blog-<?php echo $row['id']?>-<?php echo friendlyURL($row['title'])?>.html">Submit Yours</a>
				    	<div class="clear"></div>
				    	<?=$row['content']?>
				    </div>	    
				<?php } ?>
		    <div style="width:370px; float:right;">
		    	<?php if(!empty($row['espn_link'])){ ?>
				<iframe style="height:255px !important; width:370px !important; overflow:hidden !important;" src="<?php echo $row['espn_link']?>"></iframe>
				<?php } ?>
			</div>	
			<div class="clear"></div>
		    <?php }else{ ?>
		    <?=$row['content']?>
		    <?php } ?>

		    <?php if(!empty($row['series_match_id'])){ ?>
		    <h2>Other Blogs for Same Match</h2>
		    <ul style="list-style:square; font-size:13px; line-height:25px; margin-left:20px;">
		    <?php
		    	$rsBlog = mysqli_query($conn,"select * from blog_articles where series_match_id = '".$row['series_match_id']."' and id <> '".$row['id']."' and status = '1'");
		    	while($rowBlog = mysqli_fetch_assoc($rsBlog)){
		    	?>
		    	<li><a href="<?php echo WWW;?>blog-detail-<?php echo $rowBlog['id']?>-<?php echo friendlyURL($rowBlog['title'])?>.html" title="<?php echo $rowBlog['title']?>"><?php echo $rowBlog['title']?></a></li>
		    	<?php
		    	}
		    ?>
			</ul>
		    <?php } ?>
		    
		    <h2>Post Your Reply</h2>
				<? include('common/login-box.php');?>
				<? if(isset($_SESSION['ycdc_user_name']) && !empty($_SESSION['ycdc_user_name'])){ ?>
			    <form method="post" action="">
			    	<input type="hidden" name="action" value="comment">
					<fieldset>
						<div class="form-box" style="width:100%;">
							<textarea name="comments" style="margin-left: -5px; width:400px;" class="ckeditor <?=(!isset($_SESSION['ycdc_user_name']) || empty($_SESSION['ycdc_user_name']))?'':'validate[required]'?>"></textarea>
						</div>
						<div class="clear"></div>
						<div class="form-box">
							<input type="submit" value="Save"  class="submit-login">
						</div>
						<div class="form-box">
							<a id="cancel" class="submit-login" href="<?=WWW?>blogs.html">Cancel</a>
						</div>
					</fieldset>
				</form>
				<? }else{ ?>
					<div id="success">Please login first to post your reply ... !</div>
				<? } ?>
		    <h2>Posted Replies</h2>
		    <div class="list" >
                <ul>
                	<?  $rpp = PRODUCT_LIMIT_FRONT; // results per page
            			$ppage = intval($_GET["page"]);
      					if($ppage<=0) $ppage = 1;
      					$query = "select * from blog_comments where status=1 and blog_article_id = '".$row['id']."'";
				      	//=======================================
				      	if(isset($_GET['keywords']) && $_GET['keywords'] != 'Search Here'){
					        $query  .= "and title like '%".$_GET['keywords']."%'";
					    }
					    $query .= " order by id desc ";
				      //echo $query;
				      //=======================================
				      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
				        echo '<div id="information">No Record found...!</div>';
				      }
				      $rs   = mysqli_query($conn,$query);
				      $tcount = mysqli_num_rows($rs);
				      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
				      $count = 0;
				      $i = ($ppage-1)* $rpp;
          			  $x = 0;
          			  //=======================================
      				  while(($count<$rpp) && ($i<$tcount)){
                		mysqli_data_seek($rs,$i);
                		$row_g 	= mysqli_fetch_assoc($rs);
                		?>
      					<li>
      						<? if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	                            if($row['user_id'] == $_SESSION['ycdc_dbuid']){ ?>
	                            <a href="<?=WWW?>delete-blog-comment-<?=$row_g['id']?>.html"><img src="<?=WWW?>images/icons/delete.png"></a>
	                         <? } ?>
	                      	<? } ?>
							<span class="list-text" style="margin-left: -10px;">
								<?php if(isset($_SESSION['ycdc_dbuid']) 
									  && !empty($_SESSION['ycdc_dbuid']) 
									  && $_SESSION['ycdc_dbuid'] == $row_g['user_id']){ ?>
								<a href="<?=WWW?>edit-comments-<?=$row_g['id']?>.html"><img src="<?=WWW?>images/icons/edit.png"></a>
			                    <a href="<?=WWW?>delete-comments-<?=$row_g['id']?>.html"><img src="<?=WWW?>images/icons/delete.png"></a>
			                    &nbsp;&nbsp;&nbsp;
								<?php } ?>
	                            <h3><?=get_combo('users','concat(f_name, last_name)',$row_g['user_id'],'','text')?> on <?=date_converter($row_g['comment_date'])?></h3>
	                            <p><?=$row_g['comments'];?></p>	                            
	                        </span>
						</li>
					    <?
					      $i++;
					      $count++;
					      $x++;
					  } 
					  ?>
				</ul>
			</div>
			<div id="pagination-bottom">
				<? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_FRONT){ ?>
		      	<?php
		        	$reload = 'blog-detail-'.$_GET['id'].'-'.urlencode($row['title']).'.html?';
		        	echo paginate_one($reload, $ppage, $tpages);
		      	?>
		      	<input type="hidden" name="pagination-page" value="blog-detail-<?=$_GET['id']?>-<?=urlencode($row['title'])?>.html">
		        <? } ?>    
		        <form id="list-search" method="post" action="">
		        	<input type="text" name="txtsearch" placeholder="Search Here" class="validate[required] input-login">
		        </form>
		        <script type="text/javascript">
		        $('form#list-search').submit(function(e){
					var string = $('input[name=txtsearch]').val();
					if(string != '' && string != 'Search Here'){
						string = string.replace(/\s+/g,'_').toLowerCase();
						$('form#list-search').attr('action','<?=WWW;?>blog-detail-<?=$_GET['id']?>-<?=urlencode($row['title'])?>-' + string + '.html');
					}
				});
		        </script>
		        <div class="clear"></div>
		    </div>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>
<style>
.detail p{font-weight: normal;}
.desktopText{display:none;}
#cancel{background: url('<?php echo WWW;?>images/sign-in-button.png') no-repeat !important; color:#fff !important; padding-left:3px; top:0px !important;position:relative; left:-150px;}
#cke_comments a{background: none !important;}
</style>
<div class="popup hide" style="width:600px;" id="blogging-contest-rules">
	<h1 style="font-size: 14px;">Blogging Contest Rules
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h1>
		<? $row = get_record_on_id('cms',42);?>
		<div>
			<?=$row['content']?>
		    <div class="clear"></div>
		</div>
</div>
<? if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){ ?>
<script type="text/javascript" src="<?=WWW?>js/jquery-1.8.0.js"></script>
<script>
$(document).ready(function(){
	$('.close-popup').live('click',function(){
		var id = this.parentNode.parentNode.id;
		if(id){
			$('#' + id).addClass('hide');	
		}else{
			$('.popup').addClass('hide');
		}
	});

	$('a.popup-link').live('click',function(){
		// alert("laskjdf")
		var link = $(this).attr('href');
		var height = $(window).scrollTop();
		$(link).css('top',height-150);

		$(link).removeClass('hide');
	});
});
</script>
<?php } ?>
<?php include('common/footer.php'); ?>