<?php include('common/header.php'); ?>
	<div class="middle">
		<h1>Press Release Detail </h1>
		<div class="white-box content detail">
			<? 
				$row = get_record_on_id('press_releases',$_GET['id']);
			?>
			<h2><?=$row['title']?></h2>
			<p>
				<div>
					<!-- AddThis Button BEGIN -->
					<div class="addthis_toolbox addthis_default_style ">
					<a class="addthis_button_preferred_1"></a>
					<a class="addthis_button_preferred_2"></a>
					<a class="addthis_button_preferred_3"></a>
					<a class="addthis_button_preferred_4"></a>
					<a class="addthis_button_compact"></a>
					<a class="addthis_counter addthis_bubble_style"></a>
					</div>
					<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
					<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5073efa669740139"></script>
					<!-- AddThis Button END -->
				</div>
				<div><b>Released On : </b><?=date_converter($row['release_date'])?></div>
				<div class="space10"></div>
				<h3>Summary</h3>
				<?=$row['summary']?>
				<h3>Detail</h3>
				<?=$row['content']?>
			</p>
		    <div class="clear"></div>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>
<?php include('common/footer.php'); ?>