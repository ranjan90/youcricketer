<?php include_once('includes/configuration.php');
$page = 'venues/list';
$selected_country = getGeoLocationCountry(); 

$venues = array();
$error = '';

if(isset($_GET['page']) && !empty($_GET['page'])){
	$current_page = str_replace('page','',trim($_GET['page']));
}else{
	$current_page = 1;
}

$records_per_page = 10;
$start_record  = ($current_page-1)*$records_per_page;

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
}

$sql = 'select * from companies where user_id = '.$user_id;
$rs_league = mysqli_query($conn,$sql);
$league_info = mysqli_fetch_assoc($rs_league);
$league_id = $league_info['id'];

$page_title = 'Venues List - '.$league_info['company_name'];

if(isset($user_id) && !empty($user_id)){
	if(!empty($league_info)){
		$venue_permission = 1;
	}else{
		$venue_permission = 0;
	}
}

if(empty($error) && isset($_GET['v_id']) && $_GET['action'] == 'delete'){
	$venue_id = trim($_GET['v_id']);
	if($venue_permission){
		$sql = "DELETE FROM league_venues WHERE id = $venue_id and league_id=$league_id";
		mysqli_query($conn,$sql);
		$_SESSION['venue_deleted'] = 1;
	}
	header("Location:".WWW."venues/list");
	exit();
}

if(isset($_SESSION['venue_deleted']) && $_SESSION['venue_deleted']==1) {
	$venue_deleted = 1;
	unset($_SESSION['venue_deleted']);
}
if(isset($_SESSION['venue_updated']) && $_SESSION['venue_updated']==1) {
	$venue_updated = 1;
	unset($_SESSION['venue_updated']);
}
if(isset($_SESSION['venue_added']) && $_SESSION['venue_added']==1) {
	$venue_added = 1;
	unset($_SESSION['venue_added']);
}


$sql = "SELECT count(*) as record_count FROM league_venues WHERE league_id=$league_id ";
$rs_total = mysqli_query($conn,$sql);
$row_total = mysqli_fetch_assoc($rs_total);
$records_count = $row_total['record_count'];

$sql = "SELECT v.*,c.name as country_name FROM league_venues as v left join countries as c on v.country=c.id WHERE v.league_id=$league_id ORDER BY v.id LIMIT $start_record,$records_per_page";
$rs_venues = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_venues)){
	$venues[] = $row;
}

?>
<?php include('common/header.php'); ?>
<style>
.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
dl {min-height:145px;}
.content {width:1010px;}
.large-column {width:790px;}
#table-list {width:100%;border:1px solid #ccc;}
#table-list tr{border:1px solid #ccc;}
#table-list th{padding:5px;background-color:#000;color:#FB7008;font-size:14px;}
#table-list td{padding:5px;text-align:center;}
#table-list a{color:#000;}
.add_tournament{font-size:14px;color:#000;}
</style>
	<div class="middle">
		<h1>League Venues </h1>
		<h2><?php echo ucwords($league_info['company_name']); ?></h2>
		<div class="white-box content" id="dashboard">
			<?php if(isset($venue_deleted) && $venue_deleted == 1): ?>
				<div id="information">Venue deleted Successfully... !</div>
			<?php endif; ?>
			<?php if(isset($venue_updated) && $venue_updated == 1): ?>
				<div id="information">Venue updated Successfully... !</div>
			<?php endif; ?>
			<?php if(isset($venue_added) && $venue_added == 1): ?>
				<div id="information">Venue added Successfully... !</div>
			<?php endif; ?>
		
			<?php if(empty($user_info)): ?>
				<div id="error">You are not logged... !</div>
			<?php endif; ?>
			
			<?php if(!empty($error)): ?>
				<?php echo $error; ?>
			<?php endif; ?>
		
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
			<?php if($venue_permission){ ?>
			<p style="float:right;"><a href="<?php echo WWW; ?>venues/add" class="add_tournament" title="Add New Venue"><img alt="Add" src="<?php echo WWW; ?>images/icons/add.png" border="0"> Add New Venue</a></p>
			 <div class="clear"></div>
				<table id="table-list" class="white-box">
				<tr><th>Venue</th><th>Address</th><th>State</th><th>City</th><th>Zip Code</th><th>Country</th><th>Contact Name</th><th>Action</th></tr>
				<?php for($i=0;$i<count($venues);$i++): ?>
					<tr>
						<td><?php echo $venues[$i]['venue']; ?></td>
							<td><?php echo $venues[$i]['address']; ?></td>
							<td><?php echo $venues[$i]['state']; ?></td>
							<td><?php echo $venues[$i]['city']; ?></td>
							<td><?php echo $venues[$i]['zip_code']; ?></td>
							<td><?php echo $venues[$i]['country_name']; ?></td>
							<td><?php echo $venues[$i]['contact_name']; ?></td>
						<td>
							<a href="<?php echo WWW; ?>venues/edit/<?php echo $venues[$i]['id']; ?>" title="Edit"><img alt="Edit" src="<?php echo WWW; ?>images/icons/edit.png" border="0"></a>  &nbsp;&nbsp;
							<a href="<?php echo WWW; ?>venue-list.php?v_id=<?php echo $venues[$i]['id']; ?>&action=delete" onclick="return confirm('Are you sure to delete Venue.');" title="Delete"><img alt="Delete" src="<?php echo WWW; ?>images/icons/delete.png" border="0"></a>
						</td>
					</tr>
				<?php endfor; ?>
				<?php if(empty($venues)): ?>
				<tr><td colspan="4">No Records</td></tr>
				<?php endif; ?>
				</table>
				<?php }else{ ?>
				<div id="error">You do not have Permission for Venues... !</div>
				<?php } ?>
				
				<div id="pagination-bottom">
				<?php  $paging_str = getPaging('venues/list',$records_count,$current_page,$records_per_page);
				echo $paging_str;
				?>
		        <div class="clear"></div>
				</div>  
			</div>  
		    <div class="clear"></div>
			</div>
		</div>
		<!--<div class="rightbar"><?php //include('common/right-panel.php');?></div>-->
		<div class="clear"></div>
	</div>
	


<?php include('common/footer.php'); ?>