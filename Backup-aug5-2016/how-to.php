<?php include('common/header.php'); ?>
	<div class="middle">
		<h1> How-To Articles</h1>
		<div class="white-box content">
			<div id="pagination-top"></div>
			<div class="list">
                <ul>
                	<?  $rpp = PRODUCT_LIMIT_FRONT; // results per page
            			$ppage = intval($_GET["page"]);
      					if($ppage<=0) $ppage = 1;
      					$select = "select * from how_tos as h";
      					$where  = " where h.status=1 ";
      					//=======================================
      					//$cat_id = '6';
      					//$where .= " and category_id = '$cat_id'";
      					//=======================================
					    $orderBy = ' order by h.id desc';
					    $query = $select.$join.$where.$orderBy;
				      // echo $query;
				      //=======================================
				      $rs = mysqli_query($conn,$query);
      				  while($row_g = mysqli_fetch_assoc($rs)){
                		
                	
      					?>
      					<li>
							<a href="<?=WWW?>how-to/<?php echo $row_g['slug']; ?>" title="Read more" />
								<img src="<?php echo WWW.'images/community-groups.png';?>" alt="<?=$row_g['title']?>" title="<?=$row_g['title']?>" width="114" />
							</a>
	                        <span class="list-text">
	                            <h3 onclick="window.location='<?=WWW?>how-to/<?php echo $row_g['slug']; ?>'"><?=$row_g['title']?></h3>
	                            <p><?php echo truncate_string(strip_tags($row_g['content']), 180)?></p>
	                            <a href="<?=WWW?>how-to/<?php echo $row_g['slug']; ?>" title="Read more" class="submit-login" />Read More</a>
	                        </span>
	                        <span class="list-social">
	                         	<!-- AddThis Button BEGIN -->
						        <div class="addthis_toolbox addthis_default_style ">
						        <a class="addthis_button_preferred_1"></a>
						        <a class="addthis_button_preferred_2"></a>
						        <a class="addthis_button_preferred_3"></a>
						        <a class="addthis_button_preferred_4"></a>
						        <a class="addthis_button_compact"></a>
						        <a class="addthis_counter addthis_bubble_style"></a>
						        </div>
						        <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
						        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5073efa669740139"></script>
						        <!-- AddThis Button END -->
	                         	<br>
	                         	 <div style="float: left;text-align: center;width: 100%;"><?php echo date('d M, Y',strtotime($row_g['date_added'])); ?></div>
	                         	<br>
	                            
	                            <div class="clear"></div>
	                            
	                            
	                        </span>
						</li>
					    <?
					      $i++;
					      $count++;
					      $x++;
					  } 
					  ?>
			</ul>
			</div>
		
		    <div class="clear"></div>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>

<script>
$(document).ready(function(){
	$('select#pagination-combo').live('change',function(){
		var x = this.value;
		var page = $('input[name=pagination-page]').val();
		var ppage= page.split('.html');
		var curPage = '';
		if(x == 1){
			curPage = WWW + ppage[0] + '.html'
		}else{
			curPage = WWW + ppage[0] + '-' + x + '.html'
		}
		window.location = curPage;
	});

	$('#pagination-top').html($('#pagination-bottom').html());
	$('#pagination-bottom').hide();
	$('.close-popup').click(function(){
		$('.popup').addClass('hide');
	});
});
</script>
<? include('common/login-box.php');?>	
<style>
#login-box{display:none;}
</style>
<div id="invite" class="popup hide">
	<h1>
		Invite Your Friends
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h1>
	<form method="post" action="">
		<input type="hidden" name="action" value="invitation">
		<? for($x = 0; $x < 10; $x++){ ?>
		<div class="form-box">
			<input type="text" name="to_email[<?=$x?>]" placeholder="Enter email" class="validate[<?=($x==0)?'required,':'';?> custom[email]]  input-login">
		</div>
		
		<? } ?>
		<div class="form-box">
			<input type="submit" value="Send" class="submit-login">
		</div>
	</form>
</div>
<?php include('common/footer.php'); ?>