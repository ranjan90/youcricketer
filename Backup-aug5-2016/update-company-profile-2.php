<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	<div class="middle">
		<h1>Company Details</h1>

		<? 	if(isset($_POST) && !empty($_POST)){
				$state_id 		= $_POST['state_id'];
				$city_name		= $_POST['city_name'];
				$zipcode 		= $_POST['zipcode'];
				$user_id 		= $row_user['id'];

				mysqli_query($conn,"update users set post_code = '$zipcode', state_id = '$state_id',city_name = '$city_name' where id = '$user_id'");
				
				$company_id 	= $row_comp['id'];
				$company_name 	= $_POST['company_name'];
				$company_type_id= $_POST['company_type_id'];
				$country_id= $_POST['country_id'];
				$office_phone   = $_POST['office_phone'];
				$address 		= $_POST['address'];

				mysqli_query($conn,"update companies set address = '$address', office_phone = '$office_phone', company_name = '$company_name', company_type_id = '$company_type_id',country_id = '$country_id' where id = '$company_id'");

				echo '<div id="success"><b>Success : </b>Information updated ... !</div>';
				?>
				<script>
				window.location = '<?=WWW?>update-company-profile-3.html';
				</script>
				<?
			}

		?>

		<div class="white-box content" id="dashboard">
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
				<?  include('common/company-profile-navigation.php');?>
			<form method="post" action="" enctype="multipart/form-data">
				<fieldset>
					<h2>Company Information</h2>
					<div class="form-box">
						<label>Company<br>Name </label>
						<input type="text" value="<?=$row_comp['company_name']?>" style="width: 253px;" name="company_name" class="input-login validate[required]">
					</div>
					<div class="form-box">
						<label>Company<br>Type </label>
						<?=get_combo('company_types','name',$row_comp['company_type_id'],'company_type_id');?>
					</div>
					<div class="form-box">
						<label>Company<br>Country </label>
						<?=get_combo('countries','name',$row_comp['country_id'],'country_id');?>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>State </label>
						<select style="margin-left:20px; float:right;" name="state_id" class="">
							<option selected="selected" value=""></option>
							<? 	$rs_states = mysqli_query($conn,"select * from states where country_id = '".$row_comp['country_id']."'"); 
								while($row_state_l = mysqli_fetch_assoc($rs_states)){ ?>
								<option <?=($row_user['state_id'] == $row_state_l['id'])?'selected="selected"':'';?> value="<?=$row_state_l['id']?>"><?=$row_state_l['name']?></option>
							<?  } ?>
						</select>
					</div>
					
					<div class="clear"></div>
					<div class="form-box">
						<label>City </label>
						<div id="city">
						<?php /*?>
						<select name="city_id" class="validate[required]">
							<option selected="selected" value=""></option>
							<? 	$rs_cities = mysqli_query($conn,"select * from cities where state_id = '".$row_state['id']."'"); 
								while($row_city_l = mysqli_fetch_assoc($rs_cities)){ ?>
								<option <?=($row_city['id'] == $row_city_l['id'])?'selected="selected"':'';?> value="<?=$row_city_l['id']?>"><?=$row_city_l['name']?></option>
							<?  } ?>
						</select>
						<?php */ ?>
						<input type="text" name="city_name" value="<?php echo $row_user['city_name']; ?>" style="margin-left:20px; float:right; width: 253px;" type="text" class="input-login" > 
						</div>
					</div>
					<div class="clear"></div>  
					<div class="form-box">
						<label>Zip/Postal <br>Code</label>
						<input style=" width: 253px; " type="text" name="zipcode" class="input-login" maxlength="10" value="<?=$row_user['post_code']?>" >
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Street<br>Address </label>
						<input type="text" value="<?=$row_comp['address']?>" style="width: 253px;" name="address" class="input-login">
					</div>
					<div class="clear"></div>
					<div class="form-box" style="width:100%;">
						<label>Phone<br>Number </label>
						<div class="text" style="float:left; width:30px; margin-left:30px;"><?=get_combo('countries','phone_code',$row_comp['country_id'],'','text')?></div>
						<input style="float:left; width:241px;" type="text" value="<?=$row_comp['office_phone']?>" name="office_phone" class="input-login">
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<a href="<?=WWW?>dashboard.html" class="submit-login left">Back</a>
					</div>
					<div class="form-box">
						<input type="submit" value=" Update " class="submit-login">
					</div>
				</fieldset>
			</form>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<?php include('common/footer.php'); ?>