<?php include_once('includes/configuration.php');
$page = 'tournament-team-standings.html';
$selected_country = getGeoLocationCountry(); 

$matches = array();
$error = '';

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
}

$team_id = trim($_GET['team_id']);
$team_info = get_record_on_id('companies', $team_id);

$tournament_id = trim($_GET['tour_id']);
$tournament_info = get_record_on_id('tournaments', $tournament_id);

$page_title = ucwords($team_info['company_name'].' - '.$tournament_info['title']).' Standings';

$sql = "SELECT tm.*,c1.id as company1_id,c1.company_name as company1_name,c2.id as company2_id,c2.company_name as company2_name
FROM tournament_matches as tm INNER JOIN companies as c1 on c1.id = tm.batting_team1 INNER JOIN companies as c2 on c2.id = tm.batting_team2
WHERE (tm.batting_team1 = $team_id OR tm.batting_team2 = $team_id) and tournament_id = $tournament_id and (winning_team_id >0 OR winning_team_id = -1 OR winning_team_id = -2) ORDER BY tm.start_time DESC ";
$rs_matches = mysqli_query($conn,$sql);//echo $sql;
while($row = mysqli_fetch_assoc($rs_matches)){
	$matches[] = $row;
}

$runs_scored = 0;
$overs_played = 0;
$runs_conceded = 0;
$overs_bowled = 0;
$balls_played = 0;
$balls_bowled = 0;
?>
<?php include('common/header.php'); ?>
<style>
.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
dl {min-height:145px;}
.content {width:1010px;}
.large-column {width:790px;}
#table-list {width:100%;border:1px solid #ccc;}
#table-list tr{border:1px solid #ccc;}
#table-list th{padding:5px;background-color:#000;color:#FB7008;font-size:14px;}
#table-list td{padding:5px;text-align:center;}
#table-list a{color:#000;}

#dashboard .large-column {width:100%;margin-left:0px;}
</style>
	<div class="middle">
		<h1> Team Standings - <?php echo $team_info['company_name']; ?> </h1>
		<h2 style="text-transform: capitalize;"><?php echo $tournament_info['title']; ?> </h2>
		
		<div class="white-box content" id="dashboard">
			
			<?php if(!empty($error)): ?>
				<?php echo $error; ?>
			<?php endif; ?>
		
			<div class="small-column">
				<? //include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
			
			 <div class="clear"></div>
				<table id="table-list" class="white-box">
				<tr><th>Date Played</th><th>Team A Name</th><th>Score</th><th>Wickets</th><th>Overs</th><th>Team B Name</th><th>Score</th><th>Wickets</th><th>Overs</th><th>Team A NRR</th><th>Team B NRR</th></tr>
				<?php for($i=0;$i<count($matches);$i++): ?>
				<tr>
					<td><?php echo date('d M,Y', strtotime($matches[$i]['start_time'])); ?></td>
						<?php if($matches[$i]['batting_team1'] == $team_id): ?>
							
							<td><?php echo $matches[$i]['company1_name']; ?></td>
							<td><?php echo $matches[$i]['team1_score']; ?></td>
							<td><?php echo $matches[$i]['team1_wickets']; ?></td>
							<?php if($matches[$i]['winning_team_id'] == $matches[$i]['batting_team1']){
								$overs_faced1 = $matches[$i]['team1_overs'];
							}else{
								$overs_faced1 = $matches[$i]['maximum_overs'];
							} ?>
							
							<!--<td><?php echo $matches[$i]['team1_overs']; ?></td> -->
							<td><?php echo $overs_faced1; ?></td>
							<?php $runs_scored = $runs_scored + $matches[$i]['team1_score'];
							$overs_played = $overs_played + $overs_faced1;
							$balls_arr = explode('.',$overs_faced1);
							$balls_played += ($balls_arr[0]*6)+$balls_arr[1];
							?>
							
							<td><?php echo $matches[$i]['company2_name']; ?></td>
							<td><?php echo $matches[$i]['team2_score']; ?></td>
							<td><?php echo $matches[$i]['team2_wickets']; ?></td>
							
							<?php if($matches[$i]['winning_team_id'] == $matches[$i]['batting_team2']){
								$overs_faced2 = $matches[$i]['team2_overs'];
							}else{
								$overs_faced2 = $matches[$i]['maximum_overs'];
							} ?>
							
							<!--<td><?php echo $matches[$i]['team2_overs']; ?></td>-->
							<td><?php echo $overs_faced2; ?></td>
							<?php $runs_conceded = $runs_conceded + $matches[$i]['team2_score'];
							$overs_bowled = $overs_bowled + $overs_faced2;
							$balls_arr = explode('.',$overs_faced2);
							$balls_bowled += ($balls_arr[0]*6)+$balls_arr[1];
							?>
							
							<?php //$nrr = round(($matches[$i]['team1_score']/$matches[$i]['team1_overs']) - ($matches[$i]['team2_score']/$matches[$i]['team2_overs']),3); ?>
							<?php $balls_arr = explode('.',$overs_faced1);
							$overs_converted1 = $balls_arr[0]+($balls_arr[1]/6);
							$balls_arr = explode('.',$matches[$i]['team2_overs']);
							$overs_converted2 = $balls_arr[0]+($balls_arr[1]/6); ?>
							
							<?php //$data1 = $matches[$i]['team1_score']/$overs_faced1; 
							//$data2 = $matches[$i]['team2_score']/$matches[$i]['team2_overs'];
							if($overs_converted1>0)
								$data1 = $matches[$i]['team1_score']/$overs_converted1; 
							else
								$data1 = 0;
							if($overs_converted2>0)
								$data2 = $matches[$i]['team2_score']/$overs_converted2;
							else
								$data2 = 0;
							if(is_nan($data1)) $data1 = 0;
							if(is_nan($data2)) $data2 = 0;
							?>
							<?php $nrr = round(($data1 - $data2),3); ?>
							<td><?php echo $nrr; ?></td>
							
							<?php //$nrr = round(($matches[$i]['team2_score']/$matches[$i]['team2_overs']) - ($matches[$i]['team1_score']/$matches[$i]['team1_overs']),3); ?>
							
							<?php $balls_arr = explode('.',$overs_faced2);
							$overs_converted1 = $balls_arr[0]+($balls_arr[1]/6);
							$balls_arr = explode('.',$matches[$i]['team1_overs']);
							$overs_converted2 = $balls_arr[0]+($balls_arr[1]/6); ?>
							
							<?php //$data1 = $matches[$i]['team2_score']/$overs_faced2; 
							//$data2 = $matches[$i]['team1_score']/$matches[$i]['team1_overs'];
							if($overs_converted1>0)
								$data1 = $matches[$i]['team2_score']/$overs_converted1; 
							else
								$data1 = 0;
							if($overs_converted2>0)
								$data2 = $matches[$i]['team1_score']/$overs_converted2;
							else
								$data2 = 0;
							if(is_nan($data1)) $data1 = 0;
							if(is_nan($data2)) $data2 = 0;
							?>
							
							<?php $nrr = round(($data1 - $data2),3); ?>
							<td><?php echo $nrr; ?></td>
						<?php else: ?>
						
							<td><?php echo $matches[$i]['company2_name']; ?></td>
							<td><?php echo $matches[$i]['team2_score']; ?></td>
							<td><?php echo $matches[$i]['team2_wickets']; ?></td>
							<?php if($matches[$i]['winning_team_id'] == $matches[$i]['batting_team2']){
								$overs_faced1 = $matches[$i]['team2_overs'];
							}else{
								$overs_faced1 = $matches[$i]['maximum_overs'];
							} ?>
							
							<!--<td><?php echo $matches[$i]['team2_overs']; ?></td>-->
							<td><?php echo $overs_faced1; ?></td>
							<?php $runs_scored = $runs_scored + $matches[$i]['team2_score'];
							$overs_played = $overs_played + $overs_faced1;
							$balls_arr = explode('.',$overs_faced1);
							$balls_played+= ($balls_arr[0]*6)+$balls_arr[1];
							?>
							
							<td><?php echo $matches[$i]['company1_name']; ?></td>
							<td><?php echo $matches[$i]['team1_score']; ?></td>
							<td><?php echo $matches[$i]['team1_wickets']; ?></td>
							
							<?php if($matches[$i]['winning_team_id'] == $matches[$i]['batting_team1']){
								$overs_faced2 = $matches[$i]['team1_overs'];
							}else{
								$overs_faced2 = $matches[$i]['maximum_overs'];
							} ?>
							
							<!--<td><?php echo $matches[$i]['team1_overs']; ?></td>-->
							<td><?php echo $overs_faced2; ?></td>
							<?php $runs_conceded = $runs_conceded + $matches[$i]['team1_score'];
							$overs_bowled = $overs_bowled + $overs_faced2; 
							$balls_arr = explode('.',$overs_faced2);
							$balls_bowled += ($balls_arr[0]*6)+$balls_arr[1];
							?>
							
							<?php //$nrr = round(($matches[$i]['team2_score']/$matches[$i]['team2_overs']) - ($matches[$i]['team1_score']/$matches[$i]['team1_overs']),3); ?>
							
							<?php $balls_arr = explode('.',$overs_faced1);
							$overs_converted1 = $balls_arr[0]+($balls_arr[1]/6);
							$balls_arr = explode('.',$matches[$i]['team1_overs']);
							$overs_converted2 = $balls_arr[0]+($balls_arr[1]/6); ?>
							
							<?php //$data1 = $matches[$i]['team2_score']/$overs_faced1; 
							//$data2 = $matches[$i]['team1_score']/$matches[$i]['team1_overs'];
							if($overs_converted1>0)
								$data1 = $matches[$i]['team2_score']/$overs_converted1; 
							else
								$data1 = 0;
							if($overs_converted2>0)
								$data2 = $matches[$i]['team1_score']/$overs_converted2;
							else
								$data2 = 0;
							if(is_nan($data1)) $data1 = 0;
							if(is_nan($data2)) $data2 = 0; ?>
							
							<?php $nrr = round(($data1 - $data2),3); ?>
							<td><?php echo $nrr; ?></td>
							
							<?php //$nrr = round(($matches[$i]['team1_score']/$matches[$i]['team1_overs']) - ($matches[$i]['team2_score']/$matches[$i]['team2_overs']),3); ?>
							
							<?php $balls_arr = explode('.',$overs_faced2);
							$overs_converted1 = $balls_arr[0]+($balls_arr[1]/6);
							$balls_arr = explode('.',$matches[$i]['team2_overs']);
							$overs_converted2 = $balls_arr[0]+($balls_arr[1]/6); ?>
							
							<?php //$data1 = $matches[$i]['team1_score']/$overs_faced2; 
							//$data2 = $matches[$i]['team2_score']/$matches[$i]['team2_overs'];
							if($overs_converted1>0)
								$data1 = $matches[$i]['team1_score']/$overs_converted1; 
							else
								$data1 = 0;
							if($overs_converted2>0)
								$data2 = $matches[$i]['team2_score']/$overs_converted2;
							else
								$data2 = 0;
							
							if(is_nan($data1)) $data1 = 0;
							if(is_nan($data2)) $data2 = 0; ?>
							
							<?php $nrr = round(($data1 - $data2),3); ?>
							
							<td><?php echo $nrr; ?></td>
						<?php endif; ?>
					
				</tr>
				<?php endfor; ?>
				
				<?php $total_overs_played = floor($balls_played/6).'.'.($balls_played%6); ?>
				<?php $total_overs_bowled = floor($balls_bowled/6).'.'.($balls_bowled%6); ?>
				<tr>
				<td><b>Total</b></td><td></td><td><b><?php echo $runs_scored; ?></b></td><td></td><td><b><?php echo $total_overs_played; //echo $overs_played; ?></b></td><td></td><td><b><?php echo $runs_conceded; ?></b></td><td></td><td><b><?php echo $total_overs_bowled; //echo $overs_bowled; ?></b></td>
				</tr>
				
				</table>
			</div>  
		    <div class="clear"></div>
			</div>
		</div>
		<!--<div class="rightbar"><?php //include('common/right-panel.php');?></div>-->
		<div class="clear"></div>
	</div>
	


<?php include('common/footer.php'); ?>