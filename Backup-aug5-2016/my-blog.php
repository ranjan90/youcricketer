<?php include('common/header.php'); ?>
	<div class="middle">
		<h1>YouCricketer Blog <?=(isset($_GET['type']) && $_GET['type'] == 'your_region')?'In Your Region':'';?>			
		</h1>
		<?
		if(isset($_POST['action']) && $_POST['action'] == 'invitation'){
			$user_id 	= $_SESSION['ycdc_dbuid'];
			$date 		= date('Y-m-d H:i:s');
			$from_name	= $_SESSION['ycdc_user_name'];
			for($x = 0; $x < count($_POST['to_email']); $x++){
				$email  = $_POST['to_email'][$x];

				if(!empty($email)){
					$rs_u = mysqli_query($conn,"select * from users where email = '$email'");
					$rs_i = mysqli_query($conn,"select * from invitations where to_emails = '$email' and from_user_id = '$user_id'");
					if(mysqli_num_rows($rs_i) == 1){
						$status = 'Invitation Sent';
						$groupStatus = '0';
					}
					if(mysqli_num_rows($rs_u) == 1){
						$status = 'Already joined';
						$groupStatus = '0';
					}
					if(mysqli_num_rows($rs_i) == 0) {
						if(empty($status)){
							$status = 'Invitation Sent';
							$groupStatus = '5';
						}
					}else{
						$status = 'Invitation Sent';
					}
						mysqli_query($conn,"insert into invitations (from_user_id, to_name, to_emails, create_date,status) 
							values ('$user_id','$name','$email','$date','$status');");
						$id = mysqli_insert_id();
						$email_template = get_record_on_id('cms', 12);							
						$mail_title		= $email_template['title'].' from '.$from_name;
			        	$mail_content	= $email_template['content'];
			            $mail_content 	= str_replace('{to_name}',$name, $mail_content);	
			            $mail_content 	= str_replace('{from_name}',$from_name, $mail_content);	
			            $mail_content 	= str_replace('{site_link}','<a href="'.WWW.'" title="Visit The Site">YouCricketer</a>', $mail_content);
			            $mail_content 	= str_replace('{register_link}','<a href="'.WWW.'sign-up.html?ref='.$id.'" title="Sign Up">HERE</a>', $mail_content);
			            $mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);

			            mail($email,$mail_title,$mail_content,'Content-type: text/html; From: '.FROM_EMAIL);
				}
			}
			?><div id="success">Congratulations : Invitation sent to all email addresses... !</div><?
		}
		?>
		<div class="white-box content" id="dashboard">
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
			<div class="categories">
				<h2>Blog Categories</h2>
				<ul>
				<? 	$rs_cats = mysqli_query($conn,"select * from blog_categories where status = 1");
					while($row_cat = mysqli_fetch_assoc($rs_cats)){ ?>
					<li>
						<a href="<?=WWW?>blog-category-<?=$row_cat['id']?>-<?=friendlyURL($row_cat['name'])?>.html" title="<?=$row_cat['name']?> - Blog Categories - <?=$site_title?>"><?=$row_cat['name']?></a>
					</li>
				<? 	} ?>
				<div class="clear"></div>
				</ul>
			</div>

			<div id="pagination-top"></div>
			<div class="list">
                <ul>
                	<?  $rpp = PRODUCT_LIMIT_FRONT; // results per page
            			$ppage = intval($_GET["page"]);
      					if($ppage<=0) $ppage = 1;
      					$select = "select * from blog_articles b";
      					$where  = " where b.status=1 ";
      					//=======================================
      					if(isset($_GET['type']) && $_GET['type'] == 'cat'){
      						$cat_id = $_GET['id'];
      						$where .= " and category_id = '$cat_id'";
      					}
      					//=======================================
					    if(isset($_GET['keywords']) && $_GET['keywords'] != 'Search Here'){
					        $where  .= "and title like '%".str_replace('-','%',$_GET['keywords'])."%'";
					    }
					    if(isset($_GET['type']) && $_GET['type'] == 'my'){
					    	$where .= " and user_id = '".$_SESSION['ycdc_dbuid']."'";
					    }
					    if(isset($_GET['type']) && $_GET['type'] == 'your_region'){
					    	$ipData 	= file_get_contents('http://api.hostip.info/get_html.php?ip='.$user_ip);
							preg_match('/Country: (.*) /',$ipData, $expression);
							$selected_country = trim($expression[1]);
							$row_country = mysqli_fetch_assoc(mysqli_query($conn,"select * from countries where name = '$selected_country'"));
							$join .= " join users u on u.id = b.user_id and u.country_id = '".$row_country['id']."'";
					    }
					    $query = $select.$join.$where." order by b.id desc ";
				      //echo $query;
				      //=======================================
				      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
				        echo '<div id="error">Your search returned no results</div>';
				      }
				      $rs   = mysqli_query($conn,$query);
				      $tcount = mysqli_num_rows($rs);
				      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
				      $count = 0;
				      $i = ($ppage-1)* $rpp;
          			  $x = 0;
          			  //=======================================
      				  while(($count<$rpp) && ($i<$tcount)){
                		mysqli_data_seek($rs,$i);
                		$row_g 	= mysqli_fetch_assoc($rs);
                		$row_img= mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'blog' and entity_id = '".$row_g['id']."'"));
                		$row_u  = get_record_on_id('users', $row_g['user_id']);
      					?>
      					<li>
							<a href="<?=WWW?>blog-detail-<?=$row_g['id']?>-<?=friendlyURL($row_g['title'])?>.html" title="Read more" />
								<img src="<?=($row_img)?WWW.'blog/'.$row_g['id'].'/'.$row_img['file_name']:WWW.'images/community-groups.png';?>" alt="<?=$row_g['title']?>" title="<?=$row_g['title']?>" width="114" />
							</a>
	                        <span class="list-text">
	                            <h3 onclick="window.location='<?=WWW?>blog-detail-<?=$row_g['id']?>-<?=friendlyURL($row_g['title'])?>.html'"><?=$row_g['title']?></h3>
	                            <br/> <br/>
	                            <a href="<?=WWW?>blog-detail-<?=$row_g['id']?>-<?=friendlyURL($row_g['title'])?>.html" title="Read more" class="submit-login" />Read More</a>
	                       <a href="<?=WWW?>edit-blog-<?=$row_g['id']?>.html"><img src="<?=WWW?>images/icons/edit.png"></a>
			                            <a href="<?=WWW?>delete-blog-<?=$row_g['id']?>.html"><img src="<?=WWW?>images/icons/delete.png"></a>
			                      
	                         	<!-- AddThis Button BEGIN -->
						        <div class="addthis_toolbox addthis_default_style ">
						        <a class="addthis_button_preferred_1"></a>
						        <a class="addthis_button_preferred_2"></a>
						        <a class="addthis_button_preferred_3"></a>
						        <a class="addthis_button_preferred_4"></a>
						        <a class="addthis_button_compact"></a>
						        <a class="addthis_counter addthis_bubble_style"></a>
						        </div>
						        <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
						        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5073efa669740139"></script>
						        <!-- AddThis Button END -->
	                         	
	                           
	                            
			                          
	                            	<a title="Invite Friends" class="popup-link" href="#invite" ><img class="margin-2" src="<?=WWW?>images/invite-friends.png" width="16"></a>
	                           
	                            
	                        </span>
						</li>
					    <?
					      $i++;
					      $count++;
					      $x++;
					  } 
					  ?>
			</ul>
			</div>
			</div>
			<div id="pagination-bottom">
				<? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_FRONT){ ?>
		      	<?php
		        	$reload = 'blog.html?';
		        	echo paginate_one($reload, $ppage, $tpages);
		      	?>
		      	<input type="hidden" name="pagination-page" value="blog.html">
		        <? } ?>    
		        <form id="list-search" method="post" action="">
		        	<input type="text" name="txtsearch" placeholder="Search Here" class="validate[required] input-login">
		        	<input type="submit" value="">
		        </form>
		        <script type="text/javascript">
		        $('form#list-search').submit(function(e){
		        	var parentId = $(this).parent().attr('id');
					var string = $('#'+parentId +' form input[name=txtsearch]').val();
					if(string != '' && string != 'Search Here'){
						string = string.replace(/[^a-zA-Z0-9]+/g,'-').toLowerCase();
						if(string.length > 0){
							$('form#list-search').attr('action','<?=WWW;?>blogs-' + string + '.html');
						}
					}
				});
		        </script>
		        <style>
		       		#pagination-top .add{top:-27px; clear:both;}
		       		#pagination-bottom .add{top:8px; right:-24px;}
		        </style>
		        <div class="add" style="<?=(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_FRONT)?'':'';?>">
					<a href="<?=(!isset($_SESSION['ycdc_user_name']) || empty($_SESSION['ycdc_user_name']))?'#login':WWW.'add-blog.html';?>" class="popup-link right submit-login no-shaddow" >Create Blog</a>
				</div>
		    </div>  
		    <div class="clear"></div>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>
<script>
$(document).ready(function(){
	$('.close-popup').click(function(){
		$('.popup').addClass('hide');
	});

	$('a.popup-link').click(function(){
		var link = $(this).attr('href');
		if(link == '#edit-activity'){
			var id = this.id;
			$.ajax({type	: 'POST', 
				url		: WWW + 'includes/get-record.php', 
				data	: ({table:'activity',field:'activity',id:id}),
				success	: function(msg){
					$(link+' textarea').val(msg);
					$(link+' #recordId').val(id);
				}
		});
		}
		$(link).removeClass('hide');
	});

});
</script>
<? include('common/login-box.php');?>	
<style>
#login-box{display:none;}
</style>
<div id="invite" class="popup hide">
	<h1>
		Invite Your Friends
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h1>
	<form method="post" action="">
		<input type="hidden" name="action" value="invitation">
		<? for($x = 0; $x < 10; $x++){ ?>
		<div class="form-box">
			<input type="text" name="to_email[<?=$x?>]" placeholder="Enter email" class="validate[<?=($x==0)?'required,':'';?> custom[email]]  input-login">
		</div>
		
		<? } ?>
		<div class="form-box">
			<input type="submit" value="Send" class="submit-login">
		</div>
	</form>
</div>
<?php include('common/footer.php'); ?>