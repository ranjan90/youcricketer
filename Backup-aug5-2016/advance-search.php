<?php include('common/header.php'); 
include ('includes/my_pagination.php')

?>
<style>
div#pagination_controls{font-size:16px;}

div#pagination_controls > a{ color:#06F; }
div#pagination_controls > a:visited{ color:#06F; }


.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
</style>
<?php 
extract($_GET);
?>
		
	<div class="middle" >
		<div class="content" style="width: 100%;" id="advance-search">
		<div class="column1" style="width: 100%;">
			<div class="white-box">
				<ul id="menu" class="menu">
					<li class="active"><a href="#individual">Individuals</a></li>
					<li><a href="#companies">Clubs/Leagues/Companies</a></li>
			  </ul>
			  <div id="individual" class="content1">
			  		<fieldset>
			  		<form method="GET" action="">
			  		<?php $sql = mysqli_query($conn,"SELECT * FROM user_types WHERE id NOT IN(1,14,17) ORDER BY `name`");
			  		
			  		?>
			  		<table width="100%">
			  		<tr >
			  		<td>
			  		<label> Searching For</label>
			  		</td>
			  		<td>
			  		<select name="type">
			  		
			  		<?php while( $row = mysqli_fetch_assoc($sql)){?>
			  		<option value="<?php echo $row['id'];?>" <?php if(isset($_GET['type']) && !empty($_GET['type'])){ if($type == $row['id']) echo 'selected'; } else{ if($row['name'] == 'Player') echo 'selected'; } ?>> <?php echo $row['name'];?></option>
			  		<?php }?>
			  		</select>
			  		</td>
			  		<td> 
			  		<label style="padding-left: 15px;"> Country </label> </td>
			  		<?php $sql = mysqli_query($conn,"SELECT * FROM countries");
			  		?>
			  		<td>
			  		<select name='country' id='c_id' onchange="states();"> 
			  		
			  		<?php while($row = mysqli_fetch_assoc($sql)){?>
			  		<option value="<?php echo $row['id'];?>" <?php if(isset($_GET['country']) && !empty($_GET['country'])){ if($country == $row['id']) echo 'selected'; } else{ if($row['id'] == '239') echo 'selected'; } ?> > <?php echo $row['name'];?></option>
			  		<?php }?>
			  		</select>
			  		</td>
			  		</tr> 
			  	  <tr> <td> &nbsp;</td> <td>&nbsp; </td> </tr>
			  		<tr>  
			  	<td>
			  	<label> States </label> </td>
			  		<td>
			  		<div id="state_data">
			  		<?php if(isset($state)){
			  		
			  			$sql = mysqli_query($conn,"SELECT * FROM states WHERE country_id = '$country'");
			  			?>
			  		<select name='state' > 
			  		<option>All</option>
			  		<?php while($row = mysqli_fetch_assoc($sql)){?>
			  		<option value="<?php echo $row['id'];?>" <?php if($row['id'] == $state){?> selected <?php }?>> <?php echo $row['name'];?></option>
			  		<?php }?>
			  		</select>
			  		<?php  } else {?>
			  		 Select country, state values appear here
			  		 <?php }?>
			  		</div>
			  		 </td>
			  		
			  		<td>
			  		<label style="padding-left: 15px;"> City </label>
			  		
			  		</td> 
			  		<td>
			  		<input type="text" name = "city" value="<?php echo $city;?>"class="input-login" style="width: 253px;">
			  		</td>
			  		</tr>
			  		<tr> <td> &nbsp;</td> <td>&nbsp; </td> </tr>
			  		
			  		<tr>
			  		<td>
			  		<label> First class Player </label>
			  		 </td>
			  		 <td>
			  		  <select name="firstclassplayer">
			  		<option> All </option>
			  		<option value="1" <?php if($firstclassplayer == 1){ ?> selected <?php }?>> Yes </option>
			  		<option value="0" <?php if($firstclassplayer == 0){ ?> selected <?php }?>> No </option> 
			  		</select> 
			  		</td>
			  		<td> <label style="padding-left: 15px;"> Type of Player</label> </td>
			  		<td>
			  		<select name="typeofplayer">
			  		<option> All </option>
			  		<option value="Bowler" <?php if($typeofplayer == 'Bowler'){ ?> selected <?php }?>> Bowler </option>
			  		<option value="Batsman" <?php if($typeofplayer == 'Batsman'){ ?> selected <?php }?>> Batsman </option> 
			  		<option value="All Rounder" <?php if($typeofplayer == 'All Rounder'){ ?> selected <?php }?>> All Rounder </option> 
			  		<option value="Specialist Wicketkeeper" <?php if($typeofplayer == 'Specialist Wicketkeeper'){ ?> selected <?php }?>> Specialist Wicketkeeper </option> 
			  		</select> </td>
			  		</tr>
			  		<tr> <td> &nbsp;</td> <td>&nbsp; </td> </tr>
			  		<tr>
			  		
			  		
			  		<td> <label> Type of Batsman</label> 
			  		</td>
			  		<td>
			  		<select name="typeofbatsman">
			  		<option> All </option>
			  		<option value="Right-handed"  <?php if($typeofbatsman == 'Right-handed'){ ?> selected <?php }?>> Right Hand </option>
			  		<option value="Left-handed"  <?php if($typeofbatsman == 'Left-handed'){ ?> selected <?php }?>> Left Hand </option> 
			  		
			  		</select> </td>
			  		
			  		<td> <label style="padding-left: 15px;"> Type of Bowler</label> </td>
			  		<td>
			  		<select name="typeofbowler">
			  		<option> All </option>
			  		<option value="Fast"<?php if($typeofbowler == 'Fast'){ ?> selected <?php }?>> Fast </option>
			  		<option value="Medium-fast"<?php if($typeofbowler == 'Medium-fast'){ ?> selected <?php }?>> Medium-Fast </option> 
			  		<option value="Leg Spinner"<?php if($typeofbowler == 'Leg Spinner'){ ?> selected <?php }?>> Leg Spinner </option> 
			  		<option value="Off Spinner"<?php if($typeofbowler == 'Off Spinner'){ ?> selected <?php }?>> Off Spinner </option> 
			  		</select> </td>
			  		</tr>
			  		<tr> <td> &nbsp;</td> <td>&nbsp; </td> </tr>
			  		
			  		<tr>
			  		<td>
			  		<label> First Name </label>
			  		
			  		</td>
			  		<td>
			  		<input type="text" name = "firstname" value="<?php echo $firstname;?>" class="input-login" style="width: 253px;">
			  		</td>
			  		<td>
			  		<label style="padding-left: 15px;"> Last Name </label>
			  		
			  		</td> 
			  		<td>
			  		<input type="text" name = "lastname" value="<?php echo $lastname;?>"class="input-login" style="width: 253px;">
			  		</td>
			  		</tr> </table> <br/>
			  		<input type="submit" value="Search" name='individual' class="submit-login">
			  			</form>
			  			
			  			
			  		</fieldset>
			    </div>
				<div id="companies" class="content1">
					<fieldset>
					<Table widht="100%">
					<form method="GET" action="">
					<?php $sql = mysqli_query($conn,"SELECT * FROM company_types ORDER BY `name` ");
			  		
			  		?>
			  		<tr> <td> 
			  		<label> Searching For</label>
			  		</td>
			  		 <td> 
			  		<select name="type">
			  		
			  		<?php while( $row = mysqli_fetch_assoc($sql)){?>
			  		<option <?php if(isset($_GET['type']) && !empty($_GET['type'])){ if($type == $row['id']) echo 'selected'; } else{ if($row['name'] == 'Club') echo 'selected'; } ?> value="<?php echo $row['id'];?>"> <?php echo $row['name'];?></option>
			  		<?php }?>
			  		</select>
			  		</td>
			  		 <Td>	<label style="padding-left: 15px;" >Country </label> </td>
			  		<?php $sql = mysqli_query($conn,"SELECT * FROM countries");
			  		?>
			  		<td>
			  		<select name='country' id='country_id' onchange="states_company();" > </div>
			  		
			  		<?php while($row = mysqli_fetch_assoc($sql)){?>
			  		<option <?php if(isset($_GET['country']) && !empty($_GET['country'])){ if($country == $row['id']) echo 'selected'; } else{ if($row['id'] == '239') echo 'selected'; } ?>  value="<?php echo $row['id'];?>"> <?php echo $row['name'];?></option>
			  		<?php }?>
			  		</select>
			  		</td>
			  		</tr>
			  		<tr> <td>&nbsp; </td> <td>&nbsp; </td> </tr>
			  		<tr>
			  		<td>
			  		
			  		<label> State </label>
			  		
			  		 </td>
			  		<td id="state_company_data" >
			  		<div class="state_company_data" style="padding-left: 15px;">
			  		 Select country, state values appear here
			  		</div>
			  		 </td>
			  		
			  		<td> 
			  		<label style="padding-left: 15px;"> City </label>
			  		
			  		</td>
			  		<td>
			  		<input type="text" name = "city" <?php if(isset($_GET['city']) && !empty($_GET['city'])) echo $_GET['city'];  ?> class="input-login" style="width: 253px;">
			  		</td>
			  		<tr>
			  		<tr> <td> &nbsp; </td> <td>&nbsp; </td> </tr>
			  		<tr>
			  		<td style="vertical-align:top;">
			  		<label> Club/League/<br>Company Name </label>
			  		
			  		</td>
			  		<td>
			  		<input type="text" name = "companyname" <?php if(isset($_GET['companyname']) && !empty($_GET['companyname'])) echo $_GET['companyname'];  ?> value="<?php echo $_GET['companyname']; ?>" class="input-login" style="width: 253px;">
			  		</td>
			  		<td>
			  		<label style="padding-left: 15px;"> Years Of Service </label>
			  		
			  		</td>
			  		<td>
			  		<select name="yearofservice">
			  		<option> All </option>
			  		<?php for($i =1; $i<=350; $i++){?>
			  		<option <?php if(isset($_GET['yearofservice']) && $_GET['yearofservice'] == $i) echo 'selected'; ?> value="<?=$i?>"> <?php echo $i; ?> </option>
			  		<?php }?>
			  		</select>
			  		</td> </tr>
			  		<tr> <td> &nbsp; </td> <td>&nbsp; </td> </tr>
			  	
			  		
			  		<tr> <td> &nbsp; </td><td> &nbsp; </td><td> &nbsp; </td>
					<td> 
			  		<input type="submit" value="Search" name="company" class="submit-login">
			  		</td>
					</form>
					</Table>
						</fieldset>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<br/>
		
		<?php if(!isset($_SESSION['ycdc_user_name']) || empty($_SESSION['ycdc_user_name'])){ ?>
		<?php if(isset($_GET['individual']) || isset($_GET['company']) ){ ?>
			<div class="middle" id="login-div">
			<div  class="content"  style="width: 100%;">
			<div class="column1" style="width: 100%;">
			<div class="white-box" style="width: 99%;">
			<div id="contact-message" >
				<div id="success"><b>Please Login or Register!</b></div>
				<p><?php $rowWhoopsMsg = get_record_on_id('cms', 23); echo $rowWhoopsMsg['content'];?></p>
				<?php include('common/login-box.php');?>
			</div>
			</div>
			</div>
			</div>
			</div>
		<?php } ?>	
		<?php } else { ?>
		
		<?php 
		if(isset($_GET['individual'])){
			$search_result = " Individuals Search Result";
		extract($_GET);
		if(isset($_GET['individual'])){
		$keywords= '?individual=';
		
		if(isset($_GET['is_company'])){
			$sql ="SELECT * FROM users WHERE status = 1 AND is_company = '0' ";
		}
		else {
		$sql ="SELECT * FROM users WHERE status = 1 AND  is_company = '0' ";
		}
		$keywords .= "&is_company=0";
		if(!empty($type) && $type != 'All'){
		
			$sql .=" AND user_type_id = ' $type' ";
			$keywords .= "&type=$type";
		}
		
		
		if(!empty($country) && $country != 'All'){
			$sql .=" AND country_id = '$country'";
			$keywords .= "&country=$country";
		}
		
		if(!empty($state) && $state != 'All'){
			$sql .=" AND state_id = '$state'";
			$keywords .= "&state=$state";
			
		}
		
		if(!empty($city)){
			$sql .=" AND city_name LIKE '%$city%'";
			$keywords .= "&city=$city";
		}
		
		if(!empty($firstclassplayer) && $firstclassplayer != 'All'){
			$sql .=" AND first_class_player = '$firstclassplayer'";
			$keywords .= "&firstclassplayer=$firstclassplayer";
			}
		if(!empty($typeofplayer) && $typeofplayer != 'All'){
			$sql .=" AND type = '$typeofplayer'";
			$keywords .= "&typeofplayer=$typeofplayer";
		}
		if(!empty($typeofbatsman) && $typeofbatsman != 'All'){
			$sql .=" AND type_of_batsman = '$typeofbatsman'";
			$keywords .= "&typeofbatsman=$typeofbatsman";
		}

		
		if(!empty($typeofbowler) && $typeofbowler != 'All'){
			$sql .=" AND type_of_bowler = '$typeofbowler'";
			$keywords .= "&typeofbowler=$typeofbowler";
		}


		if(!empty($firstname)){
			$sql .=" AND f_name LIKE'%$firstname%'";
			$keywords .= "&firstname=$firstname";
			}

		if(!empty($lastname)){
			$sql .=" AND last_name LIKE  '%$lastname%'";
			$keywords .= "&lastname=$lastname";
		}
		
		$rw            = mysqli_query($conn,$sql);
		$total_rows    = mysqli_num_rows($rw);
		$advance_link  = "advance-search.html";
		$returnData    = pagination($total_rows,$sql,$keywords,$advance_link);
		$limit      =$returnData['limit'];
		$pagination =$returnData['pagination'];
		
		$sql .=" ".$limit;
		
		?>
		<div class="middle">
		
		<div class="content" style="width: 100%;">
		<div class="column1" style="width: 100%;">
				<ul id="menu" class="menu" >
					<li class="active" ><a href="#" style="width: 100%;"> <?php echo $search_result;?></a></li>
					
			  </ul>
		
		
		<div class="white-box" style="width: 99%;">
				  		<fieldset>
			  		<? 	$rs_users = mysqli_query($conn,$sql);
			  		   $total_record = mysqli_num_rows($rs_users);
			  		   
			  		   if($total_record > 0 ){
			  		   
			  			while($row_g= mysqli_fetch_assoc($rs_users)){ 
			  				
						$row_img = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_g['id']."' and is_default = '1' "));
                		$row_vid = mysqli_fetch_assoc(mysqli_query($conn,"select * from videos where entity_type = 'users' and entity_id = '".$row_g['id']."' and is_default = '1' "));
			  			if(!empty($row_g['city_name'])){
			  				$row_states = get_record_on_id('states', $row_g['state_id']);
				           	$row_country= get_record_on_id('countries', $row_g['country_id']);	
				           	$location   =  $row_g['city_name'].' > '.$row_states['name'].' > '.$row_country['name'];
			  			}else{
			  				$row_country= get_record_on_id('countries', $row_g['country_id']);	
			  				$location 	= $row_country['name'];
			  			}
      					?>
      					
      					<dl>
								<dt>
									<a href="<?=WWW?>individual-detail-<?=$row_g['id']?>-<?=friendlyURL($row_g['f_name'].' '.$row_g['m_name'].' '.$row_g['last_name'])?>.html" title="<?=$row_g['f_name'].' '.$row_g['m_name'].' '.$row_g['last_name']?>"><img src="<?php echo WWW;?><?=($row_img && !empty($row_img['file_name']))?'users/'.$row_g['id'].'/photos/'.$row_img['file_name']:'images/no-photo.jpg'?>" width="120" height="128" /></a></dt>
								<dd><div class="details">
									<h3><?=$row_g['f_name'].'<br>'.$row_g['last_name']?></h3>
									<p><? echo $location;?><br />
									<?=get_combo('user_types','name',$row_g['user_type_id'],'','text');?><br />
									<?=($row_g['first_class_player'] == '1')?'First Class Player<br />':''?>
									<?=$row_g['type']?><br />
									</p>
									<a class="submit-login margin-top-5" href="<?=WWW?>individual-detail-<?=$row_g['id']?>-<?=friendlyURL($row_g['f_name'].' '.$row_g['m_name'].' '.$row_g['last_name'])?>.html" title="<?=$row_g['f_name'].' '.$row_g['m_name'].' '.$row_g['last_name'].' - '.$site_title?>">View Profile</a></div>
									<div class="flag"><img title="<?=$row_country['name']?>" alt="<?=$row_country['name']?>" src="<?php echo WWW;?>countries/<?=$row_country['flag']?>"  width="25"/></div>
									<div class="video">
										<? 
											$video = $row_vid['file_name'];
											if(!empty($video)){
						if(preg_match('/<iframe(.*)<\/iframe>/', $row_vid['file_name'])){ 
									
									preg_match('/src="(.*?)"/',$row_vid['file_name'] , $src);
									$src = $src[1];
									$video = "<iframe width='205' height='130' frameborder='0' allowfullscreen src='$src'></iframe>";
									
									
						}else{
									
												$filename = explode('.',$video);
												$filename1= $filename[0];
												$video = '<video width="220" height="130" controls>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mp4" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.ogg" type=\'video/ogg; codecs="theora, vorbis"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mov" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.webm" type=\'video/webm; codecs="vp8, vorbis"\'>
												</video>';
											}
										}else{
											$video = '<img src="'.WWW.'images/no-video.jpg" width="220" height="130">';
										}
										echo $video;?>									
									</div>
								</dd>
						</dl>
					<?	} ?>
					
					<?php  }// Total Result ends here 
										else {
											
											echo "<br/>No Record Found  ";
										} 
		}
										?>
										<div id='pagination_controls' style='float: left;'>
										<?php 
										 echo $pagination;
										 ?>
										 </div>
					</fieldset>
			    </div>
		
		
			</div></div></div> </div>
			
			
			<?php }?>
			
			
			
<?php  
/*
 * 
 * 
 *  Companies Advance Search 
 * 
 */

?>			
			
	<?php 
		if(isset($_GET['company'])){
			$search_result = " Clubs/Leagues/Companies Search Result";
		extract($_GET);
	
		if(isset($_GET['company'])){
		$keywords= '?company=';
		
		if(isset($_GET['is_company'])){
			$sql ="SELECT * FROM companies INNER JOIN users ON companies.user_id = users.id WHERE users.is_company = '1' and users.status = '1' and companies.status=1 ";
		}
		else {
			$sql ="SELECT * FROM companies INNER JOIN users ON companies.user_id = users.id WHERE users.is_company = '1' and users.status = '1' and companies.status=1";
		}
		$keywords .= "&is_company=1";
		if(!empty($type) && $type != 'All'){
		
			$sql .=" AND companies.company_type_id = ' $type' ";
			$keywords .= "&type=$type";
		}
		
		
		if(!empty($country) && $country != 'All'){
			$sql .=" AND users.country_id = '$country'";
			$keywords .= "&country=$country";
		}
		
		if(!empty($state) && $state != 'All'){
			$sql .=" AND users.state_id = '$state'";
			$keywords .= "&state=$state";
			
		}
		
		if(!empty($city)){
			$sql .=" AND users.city_name LIKE '%$city%'";
			$keywords .= "&city=$city";
		}
		
		

		if(!empty($companyname)){
			$sql .=" AND companies.company_name LIKE'%$companyname%'";
			$keywords .= "&companyname=$companyname";
			}
		if(!empty($yearofservice) && $yearofservice != 'All'){
				$sql .=" AND companies.yearofservice = '$yearofservice'";
				$keywords .= "&yearofservice=$yearofservice";
			}
		
			
		
		$rw            = mysqli_query($conn,$sql);//echo $sql;
		$total_rows    = mysqli_num_rows($rw);
		$advance_link  = "advance-search.html";
		$returnData    = com_pagination($total_rows,$sql,$keywords,$advance_link);
		$limit      =$returnData['limit'];
		$pagination =$returnData['pagination'];
		
		$sql .=" ".$limit;
		
		?>
		<div class="middle">
		
		<div class="content" style="width: 100%;">
		<div class="column1" style="width: 100%;">
				<ul id="menu" class="menu" >
					<li class="active" ><a href="#" style="width: 100%;"> <?php echo $search_result;?></a></li>
					
			  </ul>
		
		
		<div class="white-box" style="width: 99%;">
				  		<fieldset>
			  		<? 	$rs_users = mysqli_query($conn,$sql);
			  		   $total_record = mysqli_num_rows($rs_users);
			  		   
			  		   if($total_record > 0 ){
			  		   
			  			while($row_g= mysqli_fetch_assoc($rs_users)){ 
			  			$row_comp= mysqli_fetch_assoc(mysqli_query($conn,"select * from companies where user_id = '".$row_g['id']."'"));
						$row_img = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_g['id']."' and is_default = '1' "));
                		$row_vid = mysqli_fetch_assoc(mysqli_query($conn,"select * from videos where entity_type = 'users' and entity_id = '".$row_g['id']."' and is_default = '1' "));
			  			if(!empty($row_g['city_id'])){
			  				$row_city   = get_record_on_id('cities', $row_g['city_id']);
				           	$row_states = get_record_on_id('states', $row_g['state_id']);
				           	$row_country= get_record_on_id('countries', $row_comp['country_id']);	
				           	if(!$row_country){
				           		$row_country= get_record_on_id('countries', $row_g['country_id']);		
				           	}
				           	$location   =  $row_g['city_name'].' > '.$row_states['name'].' > '.$row_country['name'];
			  			}else{
			  				$row_country= get_record_on_id('countries', $row_comp['country_id']);	
			  				$location 	= $row_country['name'];
			  			}
      					?>
      					
      					<dl>
								<dt>
									<a href="<?=WWW?><?php echo $row_comp['company_permalink']; ?>" title="<?=$row_g['f_name'].' '.$row_g['m_name'].' '.$row_g['last_name'].' - '.$site_title?>"><img src="<?php echo WWW;?><?=($row_img && !empty($row_img['file_name']))?'users/'.$row_g['id'].'/photos/'.$row_img['file_name']:'images/no-photo.jpg'?>" width="120" height="128" /></a></dt>
								<dd><div class="details">
									<h3><?=$row_g['company_name']?></h3>
									<p><? echo $location;?><br />
									<?=get_combo('company_types','name',$row_g['company_type_id'],'','text');?><br />
									Years in business : <?=$row_g['years_in_business']?></p>
									<a class="submit-login margin-top-5" href="<?=WWW?><?php echo $row_comp['company_permalink']; ?>" title="<?=$row_g['f_name'].' '.$row_g['m_name'].' '.$row_g['last_name']?>">View Profile</a></div>
									<div class="flag"><img title="<?=$row_country['name']?>" alt="<?=$row_country['name']?>" src="<?php echo WWW;?>countries/<?=$row_country['flag']?>"  width="25"/></div>
									<div class="video">
										<? 
										$video = $row_vid['file_name'];
										$video_name = $row_vid['file_name'];
										if(!empty($video)){
											if(preg_match('/<iframe(.*)><\/iframe>/', $video)){
							        			preg_match('/src="(.*?)"/',$row_vid['file_name'] , $src);
												$src = $src[1];
												$video = "<iframe width='205' height='130' frameborder='0' allowfullscreen src='$src'></iframe>";
											}else{
												$filename = explode('.',$video);
												$filename1= $filename[0];
												$video = '<video width="200" height="150" controls style="margin-top: -25px;">
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$video_name.'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$video_name.'" type=\'video/ogg; codecs="theora, vorbis"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$video_name.'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$video_name.'" type=\'video/webm; codecs="vp8, vorbis"\'>
												</video>';
											}
										}else{
											$video = '<img src="'.WWW.'images/no-video.jpg" width="220" height="130">';
										}
										echo $video;?>									
									</div>
								</dd>
						</dl>
					<?	} ?>
					
					<?php  }// Total Result ends here 
										else {
											
											echo "<br/>This Club/League/Company is not Yet registered with Youcricketer. Please advise the Club/League/Company to Register with Youcricketer.com. ";
										} 
		}
										?>
										<div id='pagination_controls' style='float: left;'>
										<?php 
										 echo $pagination;
										 ?>
										 </div>
					</fieldset>
			    </div>
		
		
			</div></div></div> </div>
			
			
			<?php }?>
			
			<?php }?>
			
	</div>
	
	<div class="clear"></div>
	</div>
	<script type="text/javascript">
	function states(){
		var xml;
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xml=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xml=new ActiveXObject("Microsoft.XMLHTTP");
		  }

		 var c_id 		= document.getElementById("c_id").value;
		 var id = "id="+c_id;
		 var url    		= 'states.php';
		    xml.open("POST",url,true);
		    xml.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		    xml.onreadystatechange = function(){
		  
		    	if(xml.readyState == 4 && xml.status == 200) {
		        
		  		    var return_data = xml.responseText;
					document.getElementById("state_data").innerHTML = return_data;
					 //document.getElementById("status").innerHTML = "";
					
			    

		        } 
		    }
		    xml.send(id);
			   document.getElementById("state_data").innerHTML = "Loading <img src='images/ajax-loader.gif' />"; 
		  
		  
		}
		states();
	function states_company(){
		var xml;
		
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xml=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xml=new ActiveXObject("Microsoft.XMLHTTP");
		  }

		 var c_id 		= document.getElementById("country_id").value;
		 var id = "id="+c_id; 
		 var url    		= 'states.php';
		    xml.open("POST",url,true);
		    xml.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		    xml.onreadystatechange = function(){
		  
		    	if(xml.readyState == 4 && xml.status == 200) {
		        
		  		    var return_data = xml.responseText;
					document.getElementById("state_company_data").innerHTML = return_data;
					 //document.getElementById("status").innerHTML = "";
					
			    

		        } 
		    }
		    xml.send(id);
			   document.getElementById("state_company_data").innerHTML = "Loading <img src='images/ajax-loader.gif' />"; 
		  
		  
		}
		states_company();
	</script>
	
	<script>
$(document).ready(function(){
	
	$('.close-popup').click(function(){
		$('.popup').addClass('hide');
	});

	$('a.popup-link').click(function(){
		var link = $(this).attr('href');
		var height = $(window).scrollTop();
		$(link).css('top',height-150);
		$(link).removeClass('hide');
	});
	
	});
</script>

	
<style>
table#stats-index{margin-top:10px; }
table#stats-index td{padding:3px;}
</style>
<?php include('common/footer.php'); ?>