<?php include('common/header.php'); ?>
	<div class="middle">
		<h1>Quick Tour</h1>
		<? $row = get_record_on_id('cms',27);?>
		<div class="white-box content detail" id="whats-new">
			<?=$row['content']?>
		    <div class="clear"></div>
		    <h2>With Features including:</h2>
			<h3>Interactive Status Page</h3>
			<ul>
				<li>After Logged In -> <a title="Visit Now" href="<?=WWW;?>dashboard.html">Dashboard</a></li>
				<li>After Logged In -> Left Panel -> My Dashboard -> <a title="Visit Now" href="<?=WWW;?>dashboard.html">My Status/Activity</a></li>
			</ul>
			<img src="<?=WWW;?>whats-new/interactive-status-page.png">

			<h3>Invite Friends</h3>
			<ul>
				<li>After Logged In -> Dashboard -> <a title="Visit Now" href="<?=WWW;?>invite-friends.html">Invite Friends</a></li>
				<li>After Logged In -> Left Panel -> My Network -> <a title="Visit Now" href="<?=WWW;?>invite-friends.html">Invite Friends</a></li>
			</ul>
			<img src="<?=WWW;?>whats-new/invite-friend.png" >

			<h3>Build Network of Friends</h3>
			<ul>
				<li>After Logged In -> Dashboard -> <a title="Visit Now" href="<?=WWW;?>invite-friends.html">Invite Friends</a></li>
				<li>After Logged In -> Left Panel -> My Network -> <a title="Visit Now" href="<?=WWW;?>invite-friends.html">Invite Friends</a></li>
				<li>After Logged In -> Left Panel -> My Network -> <a title="Visit Now" href="<?=WWW;?>my-network.html">My Friends</a></li>
				<li>After Logged In -> Left Panel -> My Network -> <a title="Visit Now" href="<?=WWW;?>pending-requests.html">Pending Requests</a></li>
				<li>After Logged In -> Left Panel -> My Network -> <a title="Visit Now" href="<?=WWW;?>network-notifications.html">Network Notifications</a></li>
			</ul>
			<img src="<?=WWW;?>whats-new/build-network-of-friends.png" >

			<h3>Love it, Comment, Share Statuses</h3>
			<ul>
				<li>After Logged In -> <a title="Visit Now" href="<?=WWW;?>dashboard.html">Dashboard</a></li>
			</ul>
			<img src="<?=WWW;?>whats-new/love-it-comment-share-statuses.png" >

			<h3>Attach Photos/Video in Status</h3>
			<ul>
				<li>After Logged In -> <a title="Visit Now" href="<?=WWW;?>dashboard.html">Dashboard</a></li>
			</ul>
			<img src="<?=WWW;?>whats-new/attach-photos-video-in-status.png" >

			<h3>Photo Albums	</h3>
			<ul>
				<li>After Logged In -> Left Panel -> My Albums -> <a title="Visit Now" href="<?=WWW;?>my-photos.html">My Photos</a></li>
			</ul>
			<img src="<?=WWW;?>whats-new/photo-album.png" >

			<h3>Video Albums</h3>	
			<ul>
				<li>After Logged In -> Left Panel -> My Albums -> <a title="Visit Now" href="<?=WWW;?>my-videos.html">My Videos</a></li>
			</ul>
			<img src="<?=WWW;?>whats-new/video-album.png" >

			<h3>Referral Points</h3>
			<ul>
				<li>After Logged In -> Left Panel -> My Dashboard -> <a title="Visit Now" href="<?=WWW;?>account-information.html">Account Information</a></li>
			</ul>
			<img src="<?=WWW;?>whats-new/referral-points.png" >

			<h3>Blogs, Forums & Groups</h3>
			<ul>
				<li>Top Menu -> <a title="Visit Now" href="<?=WWW;?>community.html">Community</a></li>
			</ul>
			<img src="<?=WWW;?>whats-new/blog-forums-groups.png" >

			<h3>Events & Testimonials</h3>
			<ul>
				<li>Top Menu -> <a title="Visit Now" href="<?=WWW;?>community.html">Community</a></li>
			</ul>
			<img src="<?=WWW;?>whats-new/events-testimonials.png" >

			<h3>In Your Region</h3>
			<ul>
				<li>Top Menu -> <a title="Visit Now" href="<?=WWW;?>in-your-region.html">In Your Region</a></li>
			</ul>
			<img src="<?=WWW;?>whats-new/in-your-region.png" >

			<h3>Register/Login with Facebook</h3>
			<ul>
				<li>Header Part -> <a title="Visit Now" href="<?=WWW;?>login.html">Login</a></li>
			</ul>
			<img src="<?=WWW;?>whats-new/register-login-with-facebook.png" >

			<h3>Register as Individual	</h3>
			<ul>
				<li>Header Part -> <a title="Visit Now" href="<?=WWW;?>sign-up-individual.html">Sign Up Individual</a></li>
			</ul>
			<img src="<?=WWW;?>whats-new/register-as-individual.png" >

			<h3>Register as Company/Club/League</h3>
			<ul>
				<li>Header Part -> <a title="Visit Now" href="<?=WWW;?>sign-up-company.html">Sign Up Company</a></li>
			</ul>
			<img src="<?=WWW;?>whats-new/register-as-company-club-league.png" >

			<h3>Messaging System	</h3>
			<ul>
				<li>After Logged In -> Left Panel -> My Messages -> <a title="Visit Now" href="<?=WWW;?>compose.html">Compose Message</a></li>
				<li>After Logged In -> Left Panel -> My Messages -> <a title="Visit Now" href="<?=WWW;?>inbox.html">Inbox</a></li>
				<li>After Logged In -> Left Panel -> My Messages -> <a title="Visit Now" href="<?=WWW;?>outbox.html">Outbox</a></li>
				<li>After Logged In -> Left Panel -> My Messages -> <a title="Visit Now" href="<?=WWW;?>deleted.html">Delete Messages</a></li>
			</ul>
			<img src="<?=WWW;?>whats-new/messaging-system.png" >

			<h3>Make & Print your ID Card	</h3>
			<ul>
				<li>After Logged In -> Left Panel -> My Dashboard -> <a title="Visit Now" href="<?=WWW;?>identity-card.html">Identity Card</a></li>
			</ul>
			<img src="<?=WWW;?>whats-new/make-print-your-id-card.png" >

			<h3>Privacy Settings</h3>
			<ul>
				<li>After Logged In -> Left Panel -> My Dashboard -> <a title="Visit Now" href="<?=WWW;?>privacy-settings.html">Privacy Settings</a></li>
			</ul>
			<img src="<?=WWW;?>whats-new/privacy-settings.png" >
			
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>
<style>
.detail img{width:750px; border: 1px solid #515151; border-radius: 5px; margin: 10px 30px 30px 30px;}
.detail li{list-style: square; margin-left:20px;}
</style>
<?php include('common/footer.php'); ?>