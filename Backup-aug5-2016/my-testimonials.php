<?php include('common/header.php'); ?>
	<div class="middle">
		<h1>
			YouCricketer Testimonials 			
		</h1>
		<div class="white-box content" id="dashboard">
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
			<div id="pagination-top"></div>
			<div class="list">
                <ul>
                	<?  $rpp = PRODUCT_LIMIT_FRONT; // results per page
            			$ppage = intval($_GET["page"]);
      					if($ppage<=0) $ppage = 1;
      					$query = "select * from testimonials where status=1 ";
				      	//=======================================
					    if(isset($_GET['keywords']) && $_GET['keywords'] != 'Search Here'){
					        $query  .= "and content like '%".str_replace('-','%',$_GET['keywords'])."%'";
					    }
					    if(isset($_GET['type']) && $_GET['type'] == 'my'){
					    	$query  .= "and user_id = '".$_SESSION['ycdc_dbuid']."'";	
					    }
					    $query .= " order by id desc ";
				      //echo $query;
				      //=======================================
				      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
				        echo '<div id="information">No record ...!</div>';
				      }
				      $rs   = mysqli_query($conn,$query);
				      $tcount = mysqli_num_rows($rs);
				      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
				      $count = 0;
				      $i = ($ppage-1)* $rpp;
          			  $x = 0;
          			  //=======================================
      				  while(($count<$rpp) && ($i<$tcount)){
                		mysqli_data_seek($rs,$i);
                		$row_t 	= mysqli_fetch_array($rs);
                		$row_i  = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_t['user_id']."' and is_default = '1'"));
                		?>
      					<div class="testi-box">
						<table class="testimonial"><tr>
						<td>  <?php if($row_i['file_name'] && file_exists('users/'.$row_t['user_id'].'/photos/'.$row_i['file_name'])){?>
								<img style="margin:4px" src="<?=WWW?><?='users/'.$row_t['user_id'].'/photos/'.$row_i['file_name']?>" alt="<?=get_combo('users','concat(f_name," ",last_name)',$row_t['entity_id'],'','text')?>" width="40">
							<?php } else { ?>
								<img  src="<?=WWW?>images/no-photo.png" width="70" height="65">
							<?php } ?></td>
							<td valign="top"><h2><?=get_combo('users','concat(f_name," ",last_name)',$row_t['user_id'],'','text')?> for </td>
							
							<td><?php $row_i  = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_t['entity_id']."' and is_default = '1'")); ?>
							 <?php if($row_i['file_name'] && file_exists('users/'.$row_t['entity_id'].'/photos/'.$row_i['file_name'])){?>
							<img style="margin:4px" src="<?=WWW?><?='users/'.$row_t['entity_id'].'/photos/'.$row_i['file_name']?>" alt="<?=get_combo('users','concat(f_name," ",last_name)',$row_t['entity_id'],'','text')?>" width="40">
							<?php } else { ?>
								<img  src="<?=WWW?>images/no-photo.png" width="70" height="65">
							<?php } ?>
							</td><td>
							<h2><?=get_combo('users','concat(f_name," ",last_name)',$row_t['entity_id'],'','text')?> on <?=date_converter($row_t['testimonial_date'],'M d,Y H:i')?></h2>
							
							</td>
							<td><div class="stars">
								  <a href="<?=WWW?>edit-testimonial-<?=$row_t['id']?>.html"><img src="<?=WWW?>images/icons/edit.png"></a>
	                            <a href="<?=WWW?>delete-testimonial-<?=$row_t['id']?>.html"><img src="<?=WWW?>images/icons/delete.png"></a>
	                           
							<? 	$rating = $row_t['rating'];
								for($x = 0; $x < 5; $x++){
									if($x < $rating){
									?><img src="<?=WWW?>images/star-fill-gray.jpg" title="<?=$rating?>/5"><?
									}else{
									?><img src="<?=WWW?>images/star-gray.jpg" title="<?=$rating?>/5"><?
									}
								}
							?>
							</div></td>
						
						
						</tr>
						<tr><td></td><td colspan="6" style="padding-left:1px;"><p><?=$row_t['content']?></p></td></tr>
						</table>
					</div>
					    <?
					      $i++;
					      $count++;
					      $x++;
					  } 
					  ?>
			</ul>
			</div>
			</div>
			<div id="pagination-bottom">
				<? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_FRONT){ ?>
		      	<?php
		        	$reload = 'testimonials.html?';
		        	echo paginate_one($reload, $ppage, $tpages);
		      	?>
		      	<input type="hidden" name="pagination-page" value="testimonials.html">
		        <? } ?>    
		        <form id="list-search" method="post" action="">
		        	<input type="text" name="txtsearch" placeholder="Search Here" class="validate[required] input-login">
		        	<input type="submit" value="">
		        </form>
		        <script type="text/javascript">
		        $('form#list-search').submit(function(e){
					var parentId = $(this).parent().attr('id');
					var string = $('#'+parentId +' form input[name=txtsearch]').val();
					if(string != '' && string != 'Search Here'){
						string = string.replace(/[^a-zA-Z0-9]+/g,'-').toLowerCase();
						if(string.length > 0){
							$('form#list-search').attr('action','<?=WWW;?>test
								-' + string + '.html');
						}
					}
				});
		        </script>

  <script src="<?php echo WWW?>js/jquery-1.8.0.js" type="text/javascript"></script>
		 <script type="text/javascript">

$(document).ready(function (){

	$('#dashboard .black-box h2').click(function(){
		$('#dashboard .black-box ul').css('display','none');
		var id = this.id;
		$('#'+id+'-box ul').css('display','block');
	});
});
</script>
		        <div class="clear"></div>
		        <div class="add" style="<?=(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_FRONT)?'':'top:-27px !important';?>">
					<a href="<?=(!isset($_SESSION['ycdc_user_name']) || empty($_SESSION['ycdc_user_name']))?'#login':WWW.'add-testimonial.html';?>" class="popup-link right submit-login no-shaddow" >Create Testi</a>
				</div>
		    </div>  
		    <div class="clear"></div>
		</div>
		
		<div class="clear"></div>
	</div>
<? include('common/login-box.php');?>	
<style>
#login-box{display:none;}
.testimonial td{vertical-align:middle;}
.testi-box .stars {padding:8px;}
#pagination-bottom {float:right;border-top:none;}
#pagination-top {border-bottom:none;}
</style>
<?php include('common/footer.php'); ?>