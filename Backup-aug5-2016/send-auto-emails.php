<?php 	include('includes/configuration.php'); 
		
		/*
 		 * activate_account
 		 * profile_photo
 		 * profile_video
 		 * traits
 		 * affiliations
 		 * experience
 		 * opportunities
 		 * travel_visas
 		 * personal_data
		 */


		//checking inactive users
		$query 			= "	SELECT u. * 
							FROM users u 
							LEFT JOIN reminders r ON r.user_id = u.id AND checklist =  'activate_account' 
							WHERE u.status =  '2'
							AND DATEDIFF( CURDATE( ) , u.create_date ) > 7 
							AND DATEDIFF( CURDATE( ) , r.send_date ) > 7 
							order by id desc
							";
		$users 			= mysqli_query($conn,$query);
		$activate 		= 0;
		while($userRow 	= mysqli_fetch_assoc($users)){

			$verification_code 	= getRandomWord(12);
			mysqli_query($conn,"update users set verification_code = '$verification_code' where email = '".$userRow['email']."'");

			$email_template = get_record_on_id('cms', 29);
			$mail_title		= $email_template['title'];
		    $mail_content	= $email_template['content'];
		    $mail_content 	= str_replace('{to_name}',$userRow['f_name'], $mail_content);
		    $mail_content 	= str_replace('{email}',$userRow['email'], $mail_content);
		    $mail_content 	= str_replace('{activation_code}',$verification_code, $mail_content);
		    $mail_content 	= str_replace('{click_here}','<a href="'.WWW.'verify-account.html" title="Verify your account">HERE</a>', $mail_content);
		    $mail_content 	= str_replace('{contact_link}','<a href="'.WWW.'contact-us.html" title="Contact Us">HERE</a>', $mail_content);
		    $mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);
			
			$headers 		= "Content-type:text/html;charset=UTF-8" . "\r\n";
			$headers 		.= "From: no-reply@youcricketer.com\r\n";			
							
		    mail($userRow['email'],$mail_title,$mail_content,$headers);
		    $activate++;
		    $date 			= date('Y-m-d H:i:s');
		    $userId 		= $userRow['id'];
		    mysqli_query($conn,"insert into reminders (user_id, checklist, content, send_date, status) values ('$userId','activate_account','$mail_content','$date','1');");
		}

		//checking photo
		$query 			= " SELECT u. * 
							FROM users u 
							order by id desc
							limit 1
							";
		$users 			= mysqli_query($conn,$query);
		$photo 			= 0;
		$reminder 		= false;
		$event 			= false;
		$eventType 		= '';
		while($rowUser  = mysqli_fetch_assoc($users)){
			$userId 	= $rowUser['id'];
			$rowRem = mysqli_fetch_assoc(mysqli_query($conn,"select * from reminders where user_id = '".$rowUser['id']."'")); // and checklist = 'profile_photo' 
			if(!$rowRem){
				$reminder = true;
			}else{
				if(date('d', strtotime($rowRem['send_date']) - strtotime(date('Y-m-d'))) > 7){
					$reminder = true;	
				}else{
					$reminder = false;
				}
			}

			$rowPhoto 	= mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$rowUser['id']."' and is_default <> '1'"));
			if(!$rowPhoto && $reminder == true){
				$email_template = get_record_on_id('cms', 30);
				$mail_title		= $email_template['title'];
			    $mail_content	= $email_template['content'];
			    $mail_content 	= str_replace('{to_name}',$rowUser['f_name'], $mail_content);
			    $mail_content 	= str_replace('{click_here}','<a href="'.WWW.'login.html" title="Login your account">HERE</a>', $mail_content);
			    $mail_content 	= str_replace('{contact_link}','<a href="'.WWW.'contact-us.html" title="Contact Us">HERE</a>', $mail_content);
			    $mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);
				$headers 		= "Content-type:text/html;charset=UTF-8" . "\r\n";
				$headers 		.= "From: no-reply@youcricketer.com\r\n";
			    mail($rowUser['email'],$mail_title,$mail_content,$headers);
			    $date 			= date('Y-m-d H:i:s');
			    mysqli_query($conn,"insert into reminders (user_id, checklist, content, send_date, status) values ('$userId','profile_photo','$mail_content','$date','1');");
			}

			$rowVideo 	= mysqli_fetch_assoc(mysqli_query($conn,"select * from videos where entity_type = 'users' and entity_id = '".$rowUser['id']."' and is_default <> '1'"));
			if(!$rowVideo && $reminder == true){
				$email_template = get_record_on_id('cms', 31);
				$mail_title		= $email_template['title'];
			    $mail_content	= $email_template['content'];
			    $mail_content 	= str_replace('{to_name}',$rowUser['f_name'], $mail_content);
			    $mail_content 	= str_replace('{click_here}','<a href="'.WWW.'login.html" title="Login your account">HERE</a>', $mail_content);
			    $mail_content 	= str_replace('{contact_link}','<a href="'.WWW.'contact-us.html" title="Contact Us">HERE</a>', $mail_content);
			    $mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);
				$headers 		= "Content-type:text/html;charset=UTF-8" . "\r\n";
				$headers 		.= "From: no-reply@youcricketer.com\r\n";
			    mail($rowUser['email'],$mail_title,$mail_content,$headers);
			    $date 			= date('Y-m-d H:i:s');
			    mysqli_query($conn,"insert into reminders (user_id, checklist, content, send_date, status) values ('$userId','profile_video','$mail_content','$date','1');");
			}

			if(empty($rowUser['type']) && empty($rowUser['game_types']) && $reminder == true){
				$email_template = get_record_on_id('cms', 32);
				$mail_title		= $email_template['title'];
			    $mail_content	= $email_template['content'];
			    $mail_content 	= str_replace('{to_name}',$rowUser['f_name'], $mail_content);
			    $mail_content 	= str_replace('{click_here}','<a href="'.WWW.'login.html" title="Login your account">HERE</a>', $mail_content);
			    $mail_content 	= str_replace('{contact_link}','<a href="'.WWW.'contact-us.html" title="Contact Us">HERE</a>', $mail_content);
			    $mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);
				$headers 		= "Content-type:text/html;charset=UTF-8" . "\r\n";
				$headers 		.= "From: no-reply@youcricketer.com\r\n";
			    mail($rowUser['email'],$mail_title,$mail_content,$headers);
			    $date 			= date('Y-m-d H:i:s');
			    mysqli_query($conn,"insert into reminders (user_id, checklist, content, send_date, status) values ('$userId','traits','$mail_content','$date','1');");
			}

			$usersClubs 	= mysqli_fetch_assoc(mysqli_query($conn,"select * from users_to_clubs where user_id = '$userId'"));
			$usersLeagues 	= mysqli_fetch_assoc(mysqli_query($conn,"select * from users_to_leagues where user_id = '$userId'"));
			$usersAgents 	= mysqli_fetch_assoc(mysqli_query($conn,"select * from agents where user_id = '$userId'"));
			if($reminder == true && (!$usersClubs || !$usersLeagues || !$usersAgents)){
				$email_template = get_record_on_id('cms', 33);
				$mail_title		= $email_template['title'];
			    $mail_content	= $email_template['content'];
			    $mail_content 	= str_replace('{to_name}',$rowUser['f_name'], $mail_content);
			    $mail_content 	= str_replace('{click_here}','<a href="'.WWW.'login.html" title="Login your account">HERE</a>', $mail_content);
			    $mail_content 	= str_replace('{contact_link}','<a href="'.WWW.'contact-us.html" title="Contact Us">HERE</a>', $mail_content);
			    $mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);
				$headers 		= "Content-type:text/html;charset=UTF-8" . "\r\n";
				$headers 		.= "From: no-reply@youcricketer.com\r\n";
			    mail($rowUser['email'],$mail_title,$mail_content,$headers);
			    $date 			= date('Y-m-d H:i:s');
			    mysqli_query($conn,"insert into reminders (user_id, checklist, content, send_date, status) values ('$userId','affiliations','$mail_content','$date','1');");
			}

			$usersExp 	= mysqli_fetch_assoc(mysqli_query($conn,"select * from experiences where user_id = '$userId'"));
			if($reminder == true && !$usersExp){
				$email_template = get_record_on_id('cms', 34);
				$mail_title		= $email_template['title'];
			    $mail_content	= $email_template['content'];
			    $mail_content 	= str_replace('{to_name}',$rowUser['f_name'], $mail_content);
			    $mail_content 	= str_replace('{click_here}','<a href="'.WWW.'login.html" title="Login your account">HERE</a>', $mail_content);
			    $mail_content 	= str_replace('{contact_link}','<a href="'.WWW.'contact-us.html" title="Contact Us">HERE</a>', $mail_content);
			    $mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);
				$headers 		= "Content-type:text/html;charset=UTF-8" . "\r\n";
				$headers 		.= "From: no-reply@youcricketer.com\r\n";
			    mail($rowUser['email'],$mail_title,$mail_content,$headers);
			    $date 			= date('Y-m-d H:i:s');
			    mysqli_query($conn,"insert into reminders (user_id, checklist, content, send_date, status) values ('$userId','experience','$mail_content','$date','1');");
			}

			$usersOpp 	= mysqli_fetch_assoc(mysqli_query($conn,"select * from opportunities where user_id = '$userId'"));
			if($reminder == true && !$usersOpp){
				$email_template = get_record_on_id('cms', 35);
				$mail_title		= $email_template['title'];
			    $mail_content	= $email_template['content'];
			    $mail_content 	= str_replace('{to_name}',$rowUser['f_name'], $mail_content);
			    $mail_content 	= str_replace('{click_here}','<a href="'.WWW.'login.html" title="Login your account">HERE</a>', $mail_content);
			    $mail_content 	= str_replace('{contact_link}','<a href="'.WWW.'contact-us.html" title="Contact Us">HERE</a>', $mail_content);
			    $mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);
				$headers 		= "Content-type:text/html;charset=UTF-8" . "\r\n";
				$headers 		.= "From: no-reply@youcricketer.com\r\n";
			    mail($rowUser['email'],$mail_title,$mail_content,$headers);
			    $date 			= date('Y-m-d H:i:s');
			    mysqli_query($conn,"insert into reminders (user_id, checklist, content, send_date, status) values ('$userId','opportunities','$mail_content','$date','1');");
			}

			$usersTV 	= mysqli_fetch_assoc(mysqli_query($conn,"select * from travel_visas where user_id = '$userId'"));
			if($reminder == true && !$usersTV){
				$email_template = get_record_on_id('cms', 36);
				$mail_title		= $email_template['title'];
			    $mail_content	= $email_template['content'];
			    $mail_content 	= str_replace('{to_name}',$rowUser['f_name'], $mail_content);
			    $mail_content 	= str_replace('{click_here}','<a href="'.WWW.'login.html" title="Login your account">HERE</a>', $mail_content);
			    $mail_content 	= str_replace('{contact_link}','<a href="'.WWW.'contact-us.html" title="Contact Us">HERE</a>', $mail_content);
			    $mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);
				$headers 		= "Content-type:text/html;charset=UTF-8" . "\r\n";
				$headers 		.= "From: no-reply@youcricketer.com\r\n";
			    mail($rowUser['email'],$mail_title,$mail_content,$headers);
			    $date 			= date('Y-m-d H:i:s');
			    mysqli_query($conn,"insert into reminders (user_id, checklist, content, send_date, status) values ('$userId','travel_visas','$mail_content','$date','1');");
			}

			$usersEducation	= mysqli_fetch_assoc(mysqli_query($conn,"select * from educations where user_id = '$userId'"));
			$usersLanguages = mysqli_fetch_assoc(mysqli_query($conn,"select * from languages where user_id = '$userId'"));
			if($reminder == true && (!$usersClubs || !$usersEducation)){
				$email_template = get_record_on_id('cms', 37);
				$mail_title		= $email_template['title'];
			    $mail_content	= $email_template['content'];
			    $mail_content 	= str_replace('{to_name}',$rowUser['f_name'], $mail_content);
			    $mail_content 	= str_replace('{click_here}','<a href="'.WWW.'login.html" title="Login your account">HERE</a>', $mail_content);
			    $mail_content 	= str_replace('{contact_link}','<a href="'.WWW.'contact-us.html" title="Contact Us">HERE</a>', $mail_content);
			    $mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);
				$headers 		= "Content-type:text/html;charset=UTF-8" . "\r\n";
				$headers 		.= "From: no-reply@youcricketer.com\r\n";
			    mail($rowUser['email'],$mail_title,$mail_content,$headers);
			    $date 			= date('Y-m-d H:i:s');
			    mysqli_query($conn,"insert into reminders (user_id, checklist, content, send_date, status) values ('$userId','personal_data','$mail_content','$date','1');");
			}
		}


?>
<p>Activate Reminders : <?php echo $activate;?></p>
<p>Profile Photo Reminders : <?php echo $photo;?></p>
<?
