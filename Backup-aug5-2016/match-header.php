<?php include_once('includes/configuration.php');
$page = 'match-header.html';
$selected_country = getGeoLocationCountry(); 

$error = '';
$umpires = array();
$venues = array();
$team1_players = array();
$team2_players = array();
$tournament_permission = 0;
$tournament_id = trim($_GET['t_id']);
$tournament_info = get_record_on_id('tournaments', $tournament_id);	
$match_id = trim($_GET['m_id']);
$match_info = get_record_on_id('tournament_matches', $match_id);	
$abandon_conditions = array('N/A','Forfeit by One Team','Forfeit by Both Teams','Rain',  'Late Arrival of One Team', 'Late Arrival of Both Teams',
'Unplayable Pitch conditions', 'Unplayable Ground Conditions', 'Unplayable Pitch and Ground', 'Umpire(s) Did not Show Up');
$abandon_array = array('N/A','Forfeit by One Team','Late Arrival of One Team','Umpire(s) Did not Show Up') ; 

$page_title = 'Match Header - '.ucwords($tournament_info['title']);

if(isset($_SESSION['toss_added']) && $_SESSION['toss_added']==1) {
	$toss_added = 1;
	unset($_SESSION['toss_added']);
}

if(isset($_SESSION['basic_info_error']) && $_SESSION['basic_info_error']==1) {
	$basic_info_error = 1;
	unset($_SESSION['basic_info_error']);
}

$sql = "SELECT tm.*,c1.company_name as batting_team1_name,c2.company_name as batting_team2_name,c3.company_name as toss_won_team_name FROM tournament_matches as tm inner join companies as c1 on tm.batting_team1=c1.id  inner join companies as c2 on tm.batting_team2=c2.id
inner join companies as c3 on tm.toss_won_team_id=c3.id WHERE tm.id = $match_id ";
$rs_match = mysqli_query($conn,$sql);
$match_teams = mysqli_fetch_assoc($rs_match);

$sql = "SELECT c1.company_name as team1_name,c2.company_name as team2_name FROM tournament_matches as tm inner join companies as c1 on tm.team1=c1.id  
inner join companies as c2 on tm.team2=c2.id WHERE tm.id = $match_id ";
$rs_match = mysqli_query($conn,$sql);
$teams_name = mysqli_fetch_assoc($rs_match);

$sql = "SELECT u.id,u.f_name,u.m_name,u.last_name FROM club_members as c inner join users as u on c.member_id=u.id WHERE c.club_id=".$match_info['team1']." ORDER BY u.f_name ASC";
$rs_team_batting = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_team_batting)){
	$team1_players[] = $row;
}

$sql = "SELECT u.id,u.f_name,u.m_name,u.last_name FROM club_members as c inner join users as u on c.member_id=u.id WHERE c.club_id=".$match_info['team2']." ORDER BY u.f_name ASC";
$rs_team_batting = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_team_batting)){
	$team2_players[] = $row;
}

$match_team_ids = array($match_info['team1'],$match_info['team2']);
	
if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
	if($user_id){
		$sql = 'select c.* from tournament_teams as t inner join companies as c on t.team_id=c.id where t.tournament_id = '.$tournament_id;
		$rs_club = mysqli_query($conn,$sql);
		while($row = mysqli_fetch_assoc($rs_club)){
			$tournament_clubs[] = $row;
		}
		
		$sql = 'select * from companies where user_id = '.$tournament_info['user_id'];
		$rs_league = mysqli_query($conn,$sql);
		$league_info = mysqli_fetch_assoc($rs_league);
		
		$sql = 'select c.* from league_clubs as t inner join companies as c on t.club_id=c.id where t.league_id = '.$league_info['id'];
		$rs_club = mysqli_query($conn,$sql);
		while($row = mysqli_fetch_assoc($rs_club)){
			$league_clubs[] = $row;
		}
	}
}

if(isset($user_id) && !empty($user_id)){
	$sql = "select * from tournament_permissions where tournament_id = $tournament_id and member_id = $user_id LIMIT 1";
	$rs_permission = mysqli_query($conn,$sql);
	if($tournament_info['user_id'] == $user_id || mysqli_num_rows($rs_permission)>0 ){
		$tournament_permission = 1;
	}else{
		$tournament_permission = 0;
	}
}else{
	$tournament_permission = 0;
}

$sql = "select u.f_name,u.m_name,u.last_name,u.id as user_id from users as u inner join tournament_permissions as tm on u.id=tm.member_id
 where tm.tournament_id = $tournament_id ";
$rs_umpires = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_umpires)){
	$umpires[] = $row;
}

$sql = "SELECT id,venue FROM league_venues WHERE league_id = ".$league_info['id']." ORDER BY id ";
$rs_venues = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_venues)){
	$venues[] = $row;
}

if(isset($_POST['submit_btn']) && !empty($_POST['submit_btn'])){
	validate1();
	if(empty($error)){
		$date = date('Y/m/d H:i:s')	;
		
		$toss_sql = '';
		if(isset($_POST['toss_won_decision']) && !empty($_POST['toss_won_decision'])){
			if($_POST['toss_won_decision'] == 'bat'){
				$batting_team1 = $_POST['toss_won_team_id'];
				if($_POST['toss_won_team_id'] == $match_info['team1']){
					$batting_team2 = $match_info['team2'];
				}else{
					$batting_team2 = $match_info['team1'];
				}
			}else{
				$batting_team2 = $_POST['toss_won_team_id'];
				if($_POST['toss_won_team_id'] == $match_info['team1']){
					$batting_team1 = $match_info['team2'];
				}else{
					$batting_team1 = $match_info['team1'];
				}
			}
			$toss_sql = ",toss_won_team_id=".$_POST['toss_won_team_id'].",batting_team1 = $batting_team1,batting_team2 = $batting_team2,toss_won_decision='".trim($_POST['toss_won_decision'])."'";
		}
		
		if($_POST['completely_abandon_due_to'] != 'N/A' && empty($match_info['toss_won_team_id'])){	//if toss not happen and match abandoned
			$toss_sql = ",batting_team1 = ".$match_info['team1'].",batting_team2 = ".$match_info['team2'];
		}
		
		if(isset($_POST['start_time'])){
			$start_time = trim($_POST['start_time']);
		}else{
			$start_time = date('Y/m/d',strtotime($match_info['start_time'])).' '.$_POST['start_hours'].':'.$_POST['start_minutes'];
		}
		
		if(empty($_POST['captain_team1'])){
			$_POST['captain_team1'] = 0;
		}
		if(empty($_POST['captain_team2'])){
			$_POST['captain_team2'] = 0;
		}
		
		$sql = " UPDATE tournament_matches SET start_time='".trim($start_time)."', maximum_overs='".trim($_POST['maximum_overs'])."',
		umpire1_id='".trim($_POST['umpire1_id'])."',umpire2_id='".trim($_POST['umpire2_id'])."',umpire3_id='".trim($_POST['umpire3_id'])."',
		umpire1_from='".trim($_POST['umpire1_from'])."',umpire2_from='".trim($_POST['umpire2_from'])."',umpire3_from='".trim($_POST['umpire3_from'])."',
		referee_id='".trim($_POST['referee_id'])."',venue_id='".trim($_POST['venue_id'])."',updated_on='".$date."',last_updated_by = $user_id ,
		scorer_1='".trim($_POST['scorer_1'])."',scorer_2='".trim($_POST['scorer_2'])."',captain_team1='".trim($_POST['captain_team1'])."',
		captain_team2='".trim($_POST['captain_team2'])."',completely_abandon_due_to='".trim($_POST['completely_abandon_due_to'])."' $toss_sql 
		WHERE id=".$match_id;
		
		if(mysqli_query($conn,$sql)){
			$_SESSION['match_header_updated'] = 1;
			header("Location:".WWW."tournament/matches/list/$tournament_id");
		}else{
			$error = '<p id="error">Error in updating Match. Try again later</p>';
		}
	}
}

function validate1(){
	global $error,$abandon_array;
	
	if(isset($_POST['start_time']) && empty($_POST['start_time'])){
		$error.= '<p id="error">Start Date/Time is required field</p>';
	}
	
	if(isset($_POST['completely_abandon_due_to']) && $_POST['completely_abandon_due_to'] == 'N/A' ){
		if(isset($_POST['toss_won_decision']) && empty($_POST['toss_won_decision'])){
			if(empty($_POST['toss_won_team_id'])){
			$error.= '<p id="error">Toss Won By is required field</p>';
			}
			if(empty($_POST['toss_won_decision'])){
				$error.= '<p id="error">Elected To is required field</p>';
			}
		}
		if(empty($_POST['captain_team1'])){
			$error.= '<p id="error">Captain of Team1 is required field</p>';
		}
		if(empty($_POST['captain_team2'])){
			$error.= '<p id="error">Captain of Team2 is required field</p>';
		}
		if(empty($_POST['venue_id'])){
			$error.= '<p id="error">Venue is required field</p>';
		}
	}
}

?>
<?php include('common/header.php'); ?>
<style>
.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
dl {min-height:145px;}
.content {width:1010px;}
.large-column {width:790px;}
.form-box {width:60%;}
.chk_left {float:left !important; }
</style>
	<div class="middle">
		<h1>Match Header</h1>
		<h2>Tournament: <?php echo $tournament_info['title'] ?></h2>
		<div class="white-box content" id="dashboard">
			<?php if(empty($user_info)): ?>
				<div id="error">You are not logged... !</div>
			<?php endif; ?>
			
			<?php if(!empty($error)): ?>
				<?php echo $error; ?>
			<?php endif; ?>
			
			<?php if(isset($toss_added) && $toss_added == 1): ?>
				<div id="information">Basic information updated Successfully. You can now add detailed information about Match.</div>
			<?php endif; ?>
			
			<?php if(isset($basic_info_error) && $basic_info_error == 1): ?>
				<div id="error">Fill the basic information in the Match Header page before updating Detail Scoreboard, Commentary and Match Highlights.</div>
			<?php endif; ?>
		
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
			<?php if($tournament_permission){ ?>
				
					<form method="post"  enctype="multipart/form-data">
						<input type="hidden" name="action" value="submit">
						<fieldset>
							<h2>Match Header</h2>
							<!--<p>All Fields are Required</p> -->
							
							<div class="clear"></div>
							<div class="form-box">
								<label>Start Date/Time*</label>
								<div class="text" style="width:56%;">
								<?php if($tournament_info['user_id'] == $user_id): ?>
									<input type="text" name="start_time" autocomplete="off"   id="start_time" class="input-login" value="<?php if(isset($_POST['start_time'])) echo $_POST['start_time'];else echo $match_info['start_time']; ?>">
								<?php else: ?>	
									<span style="margin-top:10px;float:left;"><?php echo date('d F, Y',strtotime($match_info['start_time'])); ?></span>
									
									<select name="start_minutes"  id="start_minutes" style="width:90px;margin-left:5px;">
									<option value="0">Minutes</option>
									<?php for($i=0;$i<60;$i++): ?>
									<?php if(isset($_POST['start_minutes']) && $_POST['start_minutes'] == $i) $sel =  'selected';elseif(date('i',strtotime($match_info['start_time'])) == $i) $sel = 'selected';else $sel = ''; ?>
										<option <?php echo $sel; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
									<?php endfor; ?>
									</select>
									<select name="start_hours"  id="start_hours" style="width:80px;">
									<option value="0">Hours</option>
									<?php for($i=0;$i<24;$i++): ?>
										<?php if(isset($_POST['start_hours']) && $_POST['start_hours'] == $i) $sel =  'selected';elseif(date('H',strtotime($match_info['start_time'])) == $i) $sel = 'selected';else $sel = ''; ?>
										<option <?php echo $sel; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
									<?php endfor; ?>
									</select>
								<?php endif; ?>
									<span style="font-weight:normal;float:right;">Change the time to actual time game started</span>
								</div>
							</div>
							
							<div class="clear"></div>
							<div class="form-box">
								<label>Game Number</label>
								<div class="text"><input disabled type="text" name="game_number" style="width:150px;" id="game_number" class="input-login" value="<?php echo $match_info['game_number']; ?>"></div>
							</div>
							
							<div class="clear"></div>
							<div class="form-box">
								<label>Overs*</label>
								<div class="text"><input type="text" name="maximum_overs" style="width:150px;" id="maximum_overs" class="input-login" value="<?php if(isset($_POST['maximum_overs'])) echo $_POST['maximum_overs'];else echo $match_info['maximum_overs']; ?>"></div>
							</div>
							
							<div class="form-box">
								<label>Completely Abandoned Due To</label>
								<div class="text" style="width:55%;">
								<select name="completely_abandon_due_to" id="completely_abandon_due_to"> 
									<?php for($i=0;$i<count($abandon_conditions);$i++): ?>
									<?php if($_POST['completely_abandon_due_to'] == $abandon_conditions[$i]) $sel = 'selected'; elseif(empty($_POST['completely_abandon_due_to']) && $match_info['completely_abandon_due_to'] == $abandon_conditions[$i]) $sel = 'selected';else $sel = ''; ?>
										<option <?php echo $sel; ?> value="<?php echo $abandon_conditions[$i]; ?>"><?php echo $abandon_conditions[$i]; ?></option>
									<?php endfor; ?>
								</select>
								</div>
							</div>
							
							<div class="clear"></div>
							
							<?php if(!empty($match_info['toss_won_team_id']) && !empty($match_info['toss_won_decision'])): ?>
								<div class="clear"></div>
								<div class="form-box">
									<label>Toss Won By</label>
									<div class="text" style="width:auto;"><?php echo $match_teams['toss_won_team_name'];  ?> and Elected to <?php echo $match_info['toss_won_decision']; ?></div>
								</div>
								<div class="clear"></div>
								<div class="form-box">
									<label>Batting Team1</label>
									<div class="text" style="width:auto;"><?php echo $match_teams['batting_team1_name'];  ?> </div>
								</div>
								<div class="clear"></div>	
								<div class="form-box">
									<label>Batting Team2</label>
									<div class="text" style="width:auto;"><?php echo $match_teams['batting_team2_name'];  ?> </div>
								</div>
								<div class="clear"></div>	
							<?php else: ?>
								<div class="clear"></div>
								
								<div class="form-box">
								<label>Toss Won By*</label>
								<div class="text">
								<select name="toss_won_team_id" id="toss_won_team_id" <?php if(!empty($match_info['completely_abandon_due_to']) && $match_info['completely_abandon_due_to'] != 'N/A') echo 'disabled'; ?>> 
								<option value="">Select One</option>
								<?php for($i=0;$i<count($tournament_clubs);$i++): 
									if(!in_array($tournament_clubs[$i]['id'],$match_team_ids)) continue;
									if(isset($_POST['toss_won_team_id']) && $_POST['toss_won_team_id'] == $tournament_clubs[$i]['id']) $sel =  'selected';else $sel = '';  ?>
									<option <?php echo $sel; ?> value="<?php echo $tournament_clubs[$i]['id']; ?>"><?php echo $tournament_clubs[$i]['company_name']; ?></option>
								<?php endfor; ?>
								</select>
								<span style="font-weight:normal;float:right;width:265px;">Make sure to select the right team, once saved you will not be able to update</span>
								</div>
								</div>
								<div class="clear"></div>
							
								<div class="form-box">
									<label>Elected To*</label>
									<div class="text">
									<select name="toss_won_decision" id="toss_won_decision" <?php if(!empty($match_info['completely_abandon_due_to']) && $match_info['completely_abandon_due_to'] != 'N/A') echo 'disabled'; ?>> 
									<option value="">Select One</option>
									<option value="bat" <?php if(isset($_POST['toss_won_decision']) && $_POST['toss_won_decision'] == 'bat') echo 'selected'; ?>>Bat</option>
									<option value="bowl" <?php if(isset($_POST['toss_won_decision']) && $_POST['toss_won_decision'] == 'bowl') echo 'selected'; ?>>Bowl</option>
									</select>
									<span style="font-weight:normal;float:right;width:265px;">Make sure to select the right value, once saved you will not be able to update</span>
									</div>
								</div>
								<div class="clear"></div>
							<?php endif; ?>
							
							<div class="form-box">
								<label>Captain*</label>
								<div class="text">
									<select name="captain_team1" id="captain_team1"> 
									<option value="">--<?php echo $teams_name['team1_name']; ?>--</option>
									<?php for($i=0;$i<count($team1_players);$i++): if(isset($_POST['captain_team1']) && $_POST['captain_team1'] == $team1_players[$i]['id']) $sel =  'selected';elseif($match_info['captain_team1'] == $team1_players[$i]['id']) $sel = 'selected';else $sel = ''; ?>
										<option <?php echo $sel; ?> value="<?php echo $team1_players[$i]['id']; ?>"><?php echo $team1_players[$i]['f_name']; ?> <?php echo $team1_players[$i]['m_name']; ?> <?php echo $team1_players[$i]['last_name']; ?></option>
									<?php endfor; ?>
									</select>
								</div>
							</div>
							<div class="clear"></div>
							
							<div class="form-box">
								<label>Captain*</label>
								<div class="text">
									<select name="captain_team2" id="captain_team2"> 
									<option value="">--<?php echo $teams_name['team2_name']; ?>--</option>
									<?php for($i=0;$i<count($team2_players);$i++): if(isset($_POST['captain_team2']) && $_POST['captain_team2'] == $team2_players[$i]['id']) $sel =  'selected';elseif($match_info['captain_team2'] == $team2_players[$i]['id']) $sel = 'selected';else $sel = ''; ?>
										<option <?php echo $sel; ?> value="<?php echo $team2_players[$i]['id']; ?>"><?php echo $team2_players[$i]['f_name']; ?> <?php echo $team2_players[$i]['m_name']; ?> <?php echo $team2_players[$i]['last_name']; ?></option>
									<?php endfor; ?>
									</select>
								</div>
							</div>
							<div class="clear"></div>
							
							<div class="form-box">
								<label>1st Umpire<?php ?></label>
								<div class="text">
									<select name="umpire1_id" id="umpire1_id"> 
									<option value="0">N/A</option>
									<?php for($i=0;$i<count($umpires);$i++): if(isset($_POST['umpire1_id']) && $_POST['umpire1_id'] == $umpires[$i]['user_id']) $sel =  'selected';elseif($match_info['umpire1_id'] == $umpires[$i]['user_id']) $sel = 'selected';else $sel = ''; ?>
										<option <?php echo $sel; ?> value="<?php echo $umpires[$i]['user_id']; ?>"><?php echo $umpires[$i]['f_name']; ?> <?php echo $umpires[$i]['m_name']; ?> <?php echo $umpires[$i]['last_name']; ?></option>
									<?php endfor; ?>
									</select>
								</div>
							</div>
							<div class="clear"></div>
							
							<div class="form-box">
								<label>1st Umpire From</label>
								<div class="text">
								<select name="umpire1_from" id="umpire1_from"> 
								<option value="0">N/A</option>
								<option value="-1" <?php if(isset($_POST['umpire1_from']) && $_POST['umpire1_from'] == -1) echo 'selected';elseif($match_info['umpire1_from'] == -1) echo 'selected'; ?> >Independent</option>
								<?php for($i=0;$i<count($league_clubs);$i++): if(isset($_POST['umpire1_from']) && $_POST['umpire1_from'] == $league_clubs[$i]['id']) $sel =  'selected';elseif($match_info['umpire1_from'] == $league_clubs[$i]['id']) $sel = 'selected';else $sel = '';  ?>
									<option <?php echo $sel; ?> value="<?php echo $league_clubs[$i]['id']; ?>"><?php echo $league_clubs[$i]['company_name']; ?></option>
								<?php endfor; ?>
								</select>
								</div>
							</div>
							<div class="clear"></div>
							
							<div class="form-box">
								<label>2nd Umpire</label>
								<div class="text">
									<select name="umpire2_id" id="umpire2_id"> 
									<option value="0">N/A</option>
									<?php for($i=0;$i<count($umpires);$i++): if(isset($_POST['umpire2_id']) && $_POST['umpire2_id'] == $umpires[$i]['user_id']) $sel =  'selected';elseif($match_info['umpire2_id'] == $umpires[$i]['user_id']) $sel = 'selected';else $sel = ''; ?>
										<option <?php echo $sel; ?> value="<?php echo $umpires[$i]['user_id']; ?>"><?php echo $umpires[$i]['f_name']; ?> <?php echo $umpires[$i]['m_name']; ?> <?php echo $umpires[$i]['last_name']; ?></option>
									<?php endfor; ?>
									</select>
								</div>
							</div>
							<div class="clear"></div>
							
							<div class="form-box">
								<label>2nd Umpire From</label>
								<div class="text">
								<select name="umpire2_from" id="umpire2_from"> 
								<option value="0">N/A</option>
								<option value="-1" <?php if(isset($_POST['umpire2_from']) && $_POST['umpire2_from'] == -1) echo 'selected';elseif($match_info['umpire2_from'] == -1) echo 'selected'; ?> >Independent</option>
								<?php for($i=0;$i<count($league_clubs);$i++): if(isset($_POST['umpire2_from']) && $_POST['umpire2_from'] == $league_clubs[$i]['id']) $sel =  'selected';elseif($match_info['umpire2_from'] == $league_clubs[$i]['id']) $sel = 'selected';else $sel = '';  ?>
									<option <?php echo $sel; ?> value="<?php echo $league_clubs[$i]['id']; ?>"><?php echo $league_clubs[$i]['company_name']; ?></option>
								<?php endfor; ?>
								</select>
								</div>
							</div>
							<div class="clear"></div>
							
							<div class="form-box">
							<label>Ist Scorer</label>
							<div class="text">
								<select name="scorer_1" id="scorer_1"> 
								<option value="0">N/A</option>
								<?php for($i=0;$i<count($umpires);$i++): if(isset($_POST['scorer_1']) && $_POST['scorer_1'] == $umpires[$i]['user_id']) $sel =  'selected';elseif($match_info['scorer_1'] == $umpires[$i]['user_id']) $sel = 'selected';else $sel = '';  ?>
									<option <?php echo $sel; ?> value="<?php echo $umpires[$i]['user_id']; ?>"><?php echo $umpires[$i]['f_name']; ?> <?php echo $umpires[$i]['m_name']; ?> <?php echo $umpires[$i]['last_name']; ?></option>
								<?php endfor; ?>
								</select>
							</div>
							</div>
							<div class="clear"></div>
							
							<div class="form-box">
								<label>2nd Scorer</label>
								<div class="text">
									<select name="scorer_2" id="scorer_2"> 
									<option value="0">N/A</option>
									<?php for($i=0;$i<count($umpires);$i++): if(isset($_POST['scorer_2']) && $_POST['scorer_2'] == $umpires[$i]['user_id']) $sel =  'selected';elseif($match_info['scorer_2'] == $umpires[$i]['user_id']) $sel = 'selected';else $sel = '';  ?>
										<option <?php echo $sel; ?> value="<?php echo $umpires[$i]['user_id']; ?>"><?php echo $umpires[$i]['f_name']; ?> <?php echo $umpires[$i]['m_name']; ?> <?php echo $umpires[$i]['last_name']; ?></option>
									<?php endfor; ?>
									</select>
								</div>
							</div>
							<div class="clear"></div>
							
							<div class="form-box">
								<label>3rd Umpire</label>
								<div class="text">
									<select name="umpire3_id" id="umpire3_id"> 
									<option value="0">N/A</option>
									<?php for($i=0;$i<count($umpires);$i++): if(isset($_POST['umpire2_id']) && $_POST['umpire3_id'] == $umpires[$i]['user_id']) $sel =  'selected';elseif($match_info['umpire3_id'] == $umpires[$i]['user_id']) $sel = 'selected';else $sel = ''; ?>
										<option <?php echo $sel; ?> value="<?php echo $umpires[$i]['user_id']; ?>"><?php echo $umpires[$i]['f_name']; ?> <?php echo $umpires[$i]['m_name']; ?> <?php echo $umpires[$i]['last_name']; ?></option>
									<?php endfor; ?>
									</select>
								</div>
							</div>
							<div class="clear"></div>
							
							<div class="form-box">
								<label>3rd Umpire From</label>
								<div class="text">
								<select name="umpire3_from" id="umpire3_from"> 
								<option value="0">N/A</option>
								<option value="-1" <?php if(isset($_POST['umpire3_from']) && $_POST['umpire3_from'] == -1) echo 'selected';elseif($match_info['umpire3_from'] == -1) echo 'selected'; ?> >Independent</option>
								<?php for($i=0;$i<count($league_clubs);$i++): if(isset($_POST['umpire3_from']) && $_POST['umpire3_from'] == $league_clubs[$i]['id']) $sel =  'selected';elseif($match_info['umpire3_from'] == $league_clubs[$i]['id']) $sel = 'selected';else $sel = '';  ?>
									<option <?php echo $sel; ?> value="<?php echo $league_clubs[$i]['id']; ?>"><?php echo $league_clubs[$i]['company_name']; ?></option>
								<?php endfor; ?>
								</select>
								</div>
							</div>
							<div class="clear"></div>
							
							<div class="form-box">
								<label>Match Referee</label>
								<div class="text">
									<select name="referee_id" id="referee_id"> 
									<option value="0">N/A</option>
									<?php for($i=0;$i<count($umpires);$i++): if(isset($_POST['referee_id']) && $_POST['referee_id'] == $umpires[$i]['user_id']) $sel =  'selected';elseif($match_info['referee_id'] == $umpires[$i]['user_id']) $sel = 'selected';else $sel = '';  ?>
										<option <?php echo $sel; ?> value="<?php echo $umpires[$i]['user_id']; ?>"><?php echo $umpires[$i]['f_name']; ?> <?php echo $umpires[$i]['m_name']; ?> <?php echo $umpires[$i]['last_name']; ?></option>
									<?php endfor; ?>
									</select>
									
								</div>
							</div>
							<div class="clear"></div>
							
							<div class="form-box">
								<label>Venue*</label>
								<div class="text">
									<select name="venue_id" id="venue_id"> 
									<option value="0">Select One</option>
									<?php for($i=0;$i<count($venues);$i++): if(isset($_POST['venue_id']) && $_POST['venue_id'] == $venues[$i]['id']) $sel =  'selected';elseif($match_info['venue_id'] == $venues[$i]['id']) $sel = 'selected';else $sel = '';  ?>
										<option <?php echo $sel; ?> value="<?php echo $venues[$i]['id']; ?>"><?php echo $venues[$i]['venue']; ?></option>
									<?php endfor; ?>
									</select>
								</div>
							</div>
							<div class="clear"></div>
							
							<div class="form-box">
								<label>Last Updated</label>
								<div class="text" style="width:auto;"><?php echo date('d F, Y H:i:s', strtotime($match_info['updated_on']));  ?> </div>
							</div>
							
							<?php $last_updated_by = get_record_on_id('users', $match_info['last_updated_by']);	 ?>
							<div class="clear"></div>
							<div class="form-box">
								<label>Last Updated by</label>
								<div class="text" style="width:auto;"><?php echo $last_updated_by['f_name'].' '.$last_updated_by['m_name'].' '.$last_updated_by['last_name'] ;  ?> </div>
							</div>
							
							<div class="clear"></div>
							<div class="form-box">
								<input type="button" name="cancel_btn" value=" Cancel " class="submit-login" onclick="window.location.href='<?php echo WWW; ?>tournament/matches/list/<?php echo $tournament_id; ?>';">
								<input type="submit" name="submit_btn" value=" Update " class="submit-login" >
								
							</div>
							
							<div class="clear"></div>
							<?php if(empty($match_info['toss_won_team_id']) || empty($match_info['toss_won_decision'])): ?>
							<div class="form-box">
								<span style="font-weight:normal;float:right;text-align:justify;">Make sure you have the right team for 'Toss Won by' and right value for 'Elected to' before clicking 'Update' button. If you have updated with wrong values then please do not proceed any further with Scoreboard and notify your League Admin to correct it for you, only after the correction you can proceed forward.</span>
							</div>
							<?php endif; ?>
						</fieldset>
					</form>
				
			<?php }else{ ?>
				<div id="error">You do not have Permission for this Tournament... !</div>
			<?php } ?>
			</div>  
		    <div class="clear"></div>
			</div>
		</div>
		<!--<div class="rightbar"><?php //include('common/right-panel.php');?></div>-->
		<div class="clear"></div>
	</div>
	

<script src="<?php echo WWW; ?>js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo WWW; ?>css/jquery.datetimepicker.css"/>
<script>
$('#start_time').datetimepicker();
$(document).ready(function(){
	$("#completely_abandon_due_to").bind('change',function(){
		if($(this).val() == 'N/A'){
			$("#toss_won_team_id").removeAttr('disabled');
			$("#toss_won_decision").removeAttr('disabled');
		}else{
			$("#toss_won_team_id").attr('disabled','disabled');
			$("#toss_won_decision").attr('disabled','disabled');
		}
	});
	
	if($("#completely_abandon_due_to").val() == 'N/A'){
		$("#toss_won_team_id").removeAttr('disabled');
		$("#toss_won_decision").removeAttr('disabled');
	}
});
</script>
<?php include('common/footer.php'); ?>