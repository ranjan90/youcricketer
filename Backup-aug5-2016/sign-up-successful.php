<?php include('common/header.php'); ?>
	<div class="middle">
		<h1>Sign Up Completed <? // $site_title?> </h1>
		<? $row = get_record_on_id('cms',3);?>
		<div class="white-box content detail">
			
			<? if(isset($_GET['status'])){ ?>
			<? if($_GET['status'] == 'success_individual'){ ?>
			<div class="alert-success" style="margin-bottom:10px; height:50px; line-height:25px;">You Registration is successful, please check your email from YouCricketer and verify the links in it to complete your registration. If you do not see then check your Spam/Junk folder and click it to be safe!.</div>
			<? } ?>
			<? if($_GET['status'] == 'success_company'){ ?>
			<div class="alert-success" style="margin-bottom:10px;">You Registration is successful, please check your email from YouCricketer and verify the links in it to complete your registration.</div>
			<? } ?>
		<? } ?>
			
		    <div class="clear"></div>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>
<?php include('common/footer.php'); ?>