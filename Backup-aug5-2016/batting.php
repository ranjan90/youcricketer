<?php include_once('includes/configuration.php');
$page = 'scorecard.html';
$selected_country = getGeoLocationCountry(); 

$league_clubs = array();
$league_info = array();
$matches = array();
$user_tournaments = array();
$tournament_permission = 0;
$error = '';
$how_out_arr = array('Bowled','Caught','Caught & Bowled','Caught Behind','DNB','Handled Ball','Hit Wicket','Hit the ball twice','LBW','Not Out',
'Obstructed Fielder','Retired Hurt','Retired Out','Run Out','Stumped');
$abandon_array = array('N/A','Forfeit by One Team','Late Arrival of One Team','Umpire(s) Did not Show Up') ; 

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
}

$tournament_id = trim($_GET['tour_id']);
$tournament_info = get_record_on_id('tournaments', $tournament_id);	

$match_id = trim($_GET['match_id']);
$match_info = get_record_on_id('tournament_matches', $match_id);	

// if the match is not abandoned and toss fields are empty
if((empty($match_info['completely_abandon_due_to']) || $match_info['completely_abandon_due_to'] == 'N/A')  && (empty($match_info['toss_won_team_id']) || empty($match_info['toss_won_decision'])  || empty($match_info['venue_id']))) {
	$_SESSION['basic_info_error'] = 1;
	header("Location:".WWW."tournament/matches/match-header/$tournament_id/".$match_id);
}

if(isset($user_id) && !empty($user_id)){
	$sql = "select * from tournament_permissions where tournament_id = $tournament_id and member_id = $user_id LIMIT 1";
	$rs_permission = mysqli_query($conn,$sql);
	if($tournament_info['user_id'] == $user_id || mysqli_num_rows($rs_permission)>0 ){
		$tournament_permission = 1;
	}else{
		$tournament_permission = 0;
	}
}else{
	$tournament_permission = 0;
}

$batting_no = trim($_GET['batting_no']);
if($batting_no == 1) $bowling_no = 2;else $bowling_no = 1;

$page_title = 'Scoreboard Innings '.$batting_no.' - '.ucwords($tournament_info['title']);

$team_batting = $match_info['batting_team'.$batting_no];
if($team_batting == $match_info['batting_team1']){
	$team_bowling = $match_info['batting_team2'];
}else{
	$team_bowling = $match_info['batting_team1'];
}

$batting_team_info = get_record_on_id('companies', $team_batting);
$bowling_team_info = get_record_on_id('companies', $team_bowling);

$team1_info = get_record_on_id('companies', $match_info['batting_team1']);
$team2_info = get_record_on_id('companies', $match_info['batting_team2']);

$sql = "SELECT u.* FROM club_members as c inner join users as u on c.member_id=u.id WHERE c.club_id=$team_batting ORDER BY u.f_name ASC";
$rs_team_batting = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_team_batting)){
	$batting_team_data[] = $row;
}

$sql = "SELECT u.* FROM club_members as c inner join users as u on c.member_id=u.id WHERE c.club_id=$team_bowling ORDER BY u.f_name ASC";
$rs_team_bowling = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_team_bowling)){
	$bowling_team_data[] = $row;
}

$sql = "SELECT * FROM tournament_bat_scorecard WHERE match_id=$match_id LIMIT 1";
$rs_scorecard = mysqli_query($conn,$sql);

if(mysqli_num_rows($rs_scorecard) == 0){
	for($i=1;$i<=11;$i++){
		$sql = "INSERT INTO tournament_bat_scorecard SET match_id=$match_id, team_id=$team_batting,batsman_no=$i,batsman_id=0,
		how_out='',fielder_id=0,bowler_id=0,runs_scored=0,balls_played=0,fours_scored=0,sixes_scored=0,strike_rate=0";
		mysqli_query($conn,$sql);
	}
	
	for($i=1;$i<=11;$i++){
		$sql = "INSERT INTO tournament_bat_scorecard SET match_id=$match_id, team_id=$team_bowling,batsman_no=$i,batsman_id=0,
		how_out='',fielder_id=0,bowler_id=0,runs_scored=0,balls_played=0,fours_scored=0,sixes_scored=0,strike_rate=0";
		mysqli_query($conn,$sql);
	}
}

$sql = "SELECT * FROM tournament_bowl_scorecard WHERE match_id=$match_id LIMIT 1";
$rs_scorecard = mysqli_query($conn,$sql);

if(mysqli_num_rows($rs_scorecard) == 0){
	for($i=1;$i<=11;$i++){
		$sql = "INSERT INTO tournament_bowl_scorecard SET match_id=$match_id, team_id=$team_batting,bowler_no=$i,bowler_id=0,
		overs_bowled=0,maidens_bowled=0,runs_conceded=0,wide_balls_bowled=0,no_balls_bowled=0,wickets_taken=0";
		mysqli_query($conn,$sql);
	}
	
	for($i=1;$i<=11;$i++){
		$sql = "INSERT INTO tournament_bowl_scorecard SET match_id=$match_id, team_id=$team_bowling,bowler_no=$i,bowler_id=0,
		overs_bowled=0,maidens_bowled=0,runs_conceded=0,wide_balls_bowled=0,no_balls_bowled=0,wickets_taken=0";
		mysqli_query($conn,$sql);
	}
}

$sql = "SELECT * FROM tournament_fow_scorecard WHERE match_id=$match_id LIMIT 1";
$rs_scorecard = mysqli_query($conn,$sql);

if(mysqli_num_rows($rs_scorecard) == 0){
	for($i=1;$i<=10;$i++){
		$sql = "INSERT INTO tournament_fow_scorecard SET match_id=$match_id, team_id=$team_batting,wicket_no=$i,score=0,
		partnership=0,overs=0,batsman_id=0";
		mysqli_query($conn,$sql);
	}
	
	for($i=1;$i<=10;$i++){
		$sql = "INSERT INTO tournament_fow_scorecard SET match_id=$match_id, team_id=$team_bowling,wicket_no=$i,score=0,
		partnership=0,overs=0,batsman_id=0";
		mysqli_query($conn,$sql);
	}
}

$sql = "SELECT * FROM tournament_bat_scorecard WHERE match_id=$match_id and team_id=$team_batting ORDER BY batsman_no ASC";
$rs_scorecard = mysqli_query($conn,$sql);

while($row = mysqli_fetch_assoc($rs_scorecard)){
	$scorecard_data[] = $row;
}

$sql = "SELECT * FROM tournament_bowl_scorecard WHERE match_id=$match_id and team_id=$team_bowling ORDER BY bowler_no ASC";
$rs_scorecard_bowling = mysqli_query($conn,$sql);

while($row = mysqli_fetch_assoc($rs_scorecard_bowling)){
	$scorecard_bowling_data[] = $row;
}

$sql = "SELECT * FROM tournament_fow_scorecard WHERE match_id=$match_id and team_id=$team_batting ORDER BY wicket_no ASC";
$rs_scorecard_fow = mysqli_query($conn,$sql);

while($row = mysqli_fetch_assoc($rs_scorecard_fow)){
	$scorecard_fow_data[] = $row;
}

$team_score = 0;
$wickets = 0;
$balls = 0;
$bowlers_array = array();

if(isset($_POST['update_submit_bowling']) && !empty($_POST['update_submit_bowling'])){
	
	/*	updated_by_scorer field added to check if field is updated by user and not by system. Field is updated by system for statistics accuracy	*/
	
	validate();
	if(empty($error)){
		for($i=1;$i<=11;$i++){
			$sql = "UPDATE tournament_bat_scorecard SET batsman_id=".$_POST['batsman_id_'.$i].",
			how_out='".$_POST['how_out_'.$i]."',fielder_id=".$_POST['fielder_id_'.$i].",bowler_id=".$_POST['bowler_id_'.$i].",
			runs_scored=".$_POST['runs_scored_'.$i].",balls_played=".$_POST['balls_played_'.$i].",fours_scored=".$_POST['fours_scored_'.$i].",
			sixes_scored=".$_POST['sixes_scored_'.$i].",updated_by_scorer=1 WHERE batsman_no=$i and match_id=$match_id and team_id=$team_batting";
			$team_score+=$_POST['runs_scored_'.$i];
			
			if(!empty($_POST['how_out_'.$i]) && ($_POST['how_out_'.$i] != 'DNB' && $_POST['how_out_'.$i] != 'Not Out')){
				$wickets+=1;
			}
			mysqli_query($conn,$sql);
		}
		
		$team_score = $team_score+$_POST['leg_byes']+$_POST['byes']+$_POST['wides']+$_POST['no_balls'];
		//$overs = floor($balls/6).'.'.($balls%6);
		
		for($i=1;$i<=11;$i++){
			$sql = "UPDATE tournament_bowl_scorecard SET bowler_id=".$_POST['bowler_id_bowl_'.$i].",
			maidens_bowled='".$_POST['maidens_bowled_'.$i]."',overs_bowled=".$_POST['overs_bowled_'.$i].",runs_conceded=".$_POST['runs_conceded_'.$i].",
			wide_balls_bowled=".$_POST['wide_balls_bowled_'.$i].",no_balls_bowled=".$_POST['no_balls_bowled_'.$i].",wickets_taken=".$_POST['wickets_taken_'.$i]."
			 WHERE bowler_no=$i and match_id=$match_id and team_id=$team_bowling";
			mysqli_query($conn,$sql);
			$overs+=$_POST['overs_bowled_'.$i];
			if(!empty($_POST['bowler_id_bowl_'.$i])){
				$bowlers_array[] = $_POST['bowler_id_bowl_'.$i];
			}
		}
		
		if(!empty($bowlers_array)){	//Add bowlers to batting table for stats
			$sql = "SELECT max(batsman_id) as max_batsman_id FROM tournament_bat_scorecard WHERE match_id=$match_id and team_id=$team_bowling ";
			$max_data = mysqli_fetch_assoc(mysqli_query($conn,$sql));
			if($max_data['max_batsman_id']==0){
				for($i=0;$i<count($bowlers_array);$i++){
					$k=$i+1;
					$sql = "UPDATE tournament_bat_scorecard SET batsman_id=".$bowlers_array[$i].",
					how_out='',fielder_id=0,bowler_id=0,runs_scored=0,balls_played=0,fours_scored=0,sixes_scored=0,strike_rate=0 
					WHERE batsman_no=$k and match_id=$match_id and team_id=$team_bowling";
					mysqli_query($conn,$sql);
				}
			}
		}
		
		$sql = "UPDATE tournament_matches SET team{$batting_no}_overs = $overs, team{$batting_no}_leg_byes = '".$_POST['leg_byes']."',
		team{$batting_no}_byes = '".$_POST['byes']."',team{$batting_no}_wides = '".$_POST['wides']."',team{$batting_no}_no_balls = '".$_POST['no_balls']."', 
		team{$batting_no}_innings_result = '".$_POST['innings_result']."',team{$batting_no}_score = $team_score,  team{$batting_no}_wickets = $wickets 
		WHERE id=".$match_id;
		mysqli_query($conn,$sql);
		
		$_SESSION['scorecard_updated'] = 1;
		header('Location:'.WWW.'tournament/matches/batting/'.$tournament_id.'/'.$match_id.'/'.$batting_no);
		exit();
	}
}

if(isset($_POST['update_submit_fow']) && !empty($_POST['update_submit_fow'])){
	for($i=1;$i<=11;$i++){
		if(!empty($_POST['score_'.$i])){
			$status = 1;
		}else{
			$status = 0;
		}
		$sql = "UPDATE tournament_fow_scorecard SET batsman_id = '".$_POST['fow_batsman_id_'.$i]."',
		score='".$_POST['score_'.$i]."',partnership=".$_POST['partnership_'.$i].",overs=".$_POST['overs_'.$i].",status = $status
		WHERE wicket_no=$i and match_id=$match_id and team_id=$team_batting";
		mysqli_query($conn,$sql);
	}
	
	$_SESSION['scorecard_updated'] = 1;
	header('Location:'.WWW.'tournament/matches/batting/'.$tournament_id.'/'.$match_id.'/'.$batting_no);
	exit();
}

if(isset($_SESSION['scorecard_updated']) && $_SESSION['scorecard_updated']==1) {
	$scorecard_updated = 1;
	unset($_SESSION['scorecard_updated']);
}

if(isset($_SESSION['match_updated']) && $_SESSION['match_updated']==1) {
	$match_updated = 1;
	unset($_SESSION['match_updated']);
}

if(isset($_SESSION['match_added']) && $_SESSION['match_added']==1) {
	$match_added = 1;
	unset($_SESSION['match_added']);
}

function validate(){
	global $error;
	
	for($i=1;$i<=11;$i++){
		if($_POST['batsman_id_'.$i]>0 && empty($_POST['how_out_'.$i])){
			$error.= '<p id="error">How Out is Required for Batsman '.$i.'</p>';
		}
	}
	
	$arr = array('DNB', 'Not Out', 'Obstructed Fielder', 'Retired Hurt', 'Retired Out', 'Run Out');
	for($i=1;$i<=11;$i++){
		if($_POST['batsman_id_'.$i]>0 && !empty($_POST['how_out_'.$i]) && empty($_POST['bowler_id_'.$i]) && (!in_array($_POST['how_out_'.$i],$arr)) ){
			$error.= '<p id="error">Bowler is Required for Batsman '.$i.'</p>';
		}
	}
}

$total_runs = 0; $total_balls = 0; $total_fours = 0;$total_sixes = 0;
?>
<?php include('common/header.php'); ?>
<style>
.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
dl {min-height:145px;}
.content {width:1010px;}
.large-column {width:790px;}
#table-list {width:90%;border:1px solid #ccc;}
#table-list tr{border:1px solid #ccc;}
#table-list th{padding:5px;background-color:#000;color:#FB7008;font-size:14px;}
#table-list td{padding:5px;text-align:center;}
#table-list a{color:#000;}
#table-list select {width:145px;}
.textb1 {width:50px;height:20px;}
.add_tournament{font-size:14px;color:#000;}
#dashboard.content{width:1050px;}

#table-fow {width:90%;border:1px solid #ccc;}
#table-fow tr{border:1px solid #ccc;}
#table-fow th{padding:2px;background-color:#000;color:#FB7008;font-size:14px;}
#table-fow td{padding:2px;text-align:center;}
#table-fow a{color:#000;}
#table-fow select {width:89px;margin-left:2px;}
</style>
	<div class="middle">
		<h1> Match Scorecard </h1>
		<h2> Tournament: <?php echo $tournament_info['title']; ?>  </h2>
		<h2><?php echo $team1_info['company_name']; ?> vs <?php echo $team2_info['company_name']; ?></h2>
		
		<div class="white-box content" id="dashboard">
			<?php if(isset($scorecard_updated) && $scorecard_updated == 1): ?>
				<div id="information">Scorecard updated Successfully... !</div>
			<?php endif; ?>
			<?php if(isset($match_updated) && $match_updated == 1): ?>
				<div id="information">Match updated Successfully... !</div>
			<?php endif; ?>
			<?php if(isset($match_added) && $match_added == 1): ?>
				<div id="information">Match added Successfully... !</div>
			<?php endif; ?>
		
			<?php if(empty($user_info)): ?>
				<div id="error">You are not logged... !</div>
			<?php endif; ?>
			
			<?php if(!empty($error)): ?>
				<?php echo $error; ?>
			<?php endif; ?>
		
			<div class="small-column">
				<? //include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
			
			<?php if($tournament_permission){ ?>
			<?php $batsman_selected = array(); ?>
			<p ></p>
			 <div class="clear"></div>
			 <h2><?php echo $batting_team_info['company_name']; ?> - Batting</h2>
			 <form method="post">
				<table id="table-list" class="white-box">
				<tr><th>No</th><th>Batsman</th><th>How Out</th><th>Fielder</th><th>Bowler</th><th>Runs</th><th>Balls</th><th>4s</th><th>6s</th></tr>
				<?php for($i=0;$i<count($scorecard_data);$i++): ?>
				<?php $z=$i+1; ?>
					<tr>
						<td><?php echo $i+1; ?></td>
						<td><select name="batsman_id_<?php echo $i+1; ?>" id="batsman_id_<?php echo $i+1; ?>">
						<option value="0">Select One</option>
						<?php for($k=0;$k<count($batting_team_data);$k++): ?>
							<?php if(in_array($batting_team_data[$k]['id'],$batsman_selected)) continue; ?>
						<?php if(isset($_POST['batsman_id_'.$z]) && $_POST['batsman_id_'.$z] == $batting_team_data[$k]['id']) $sel = 'selected'; elseif(!isset($_POST['batsman_id_'.$z]) && ($scorecard_data[$i]['balls_played'] > 0 || !empty($scorecard_data[$i]['how_out'])) && $scorecard_data[$i]['batsman_id'] == $batting_team_data[$k]['id']) { $sel = 'selected'; $batsman_selected[]= $batting_team_data[$k]['id'];} else {$sel = '';} ?>
							<option value="<?php echo $batting_team_data[$k]['id']; ?>" <?php echo $sel; ?>><?php echo $batting_team_data[$k]['f_name']; ?> <?php echo $batting_team_data[$k]['last_name']; ?></option>
						<?php endfor; ?>
						</select></td>
						
						<?php $z=$i+1; ?>
						<td><select name="how_out_<?php echo $i+1; ?>" id="how_out_<?php echo $i+1; ?>">
						<option value="">Select One</option>
						<?php for($k=0;$k<count($how_out_arr);$k++): ?>
						<?php if(isset($_POST['how_out_'.$z]) && $_POST['how_out_'.$z] == $how_out_arr[$k]) $sel = 'selected'; elseif(!isset($_POST['how_out_'.$z]) && $scorecard_data[$i]['how_out'] == $how_out_arr[$k]) $sel = 'selected';else $sel = ''; ?>
							<option value="<?php echo $how_out_arr[$k]; ?>" <?php echo $sel; ?>><?php echo $how_out_arr[$k]; ?></option>
						<?php endfor; ?>
						</select></td>
						
						<td><select name="fielder_id_<?php echo $i+1; ?>" id="fielder_id_<?php echo $i+1; ?>">
						<option value="0">Select One</option>
						<?php for($k=0;$k<count($bowling_team_data);$k++): ?>
						<?php if(isset($_POST['fielder_id_'.$z]) && $_POST['fielder_id_'.$z] == $bowling_team_data[$k]['id']) $sel = 'selected'; elseif(!isset($_POST['fielder_id_'.$z]) && $scorecard_data[$i]['fielder_id'] == $bowling_team_data[$k]['id']) $sel = 'selected';else $sel = ''; ?>
							<option value="<?php echo $bowling_team_data[$k]['id']; ?>" <?php echo $sel; ?>><?php echo $bowling_team_data[$k]['f_name']; ?> <?php echo $bowling_team_data[$k]['last_name']; ?></option>
						<?php endfor; ?>
						</select></td>
						
						<td><select name="bowler_id_<?php echo $i+1; ?>" id="bowler_id_<?php echo $i+1; ?>">
						<option value="0">Select One</option>
						<?php for($k=0;$k<count($bowling_team_data);$k++): ?>
						<?php if(isset($_POST['bowler_id_'.$z]) && $_POST['bowler_id_'.$z] == $bowling_team_data[$k]['id']) $sel = 'selected'; elseif(!isset($_POST['bowler_id_'.$z]) && $scorecard_data[$i]['bowler_id'] == $bowling_team_data[$k]['id']) $sel = 'selected';else $sel = ''; ?>
							<option value="<?php echo $bowling_team_data[$k]['id']; ?>" <?php echo $sel; ?>><?php echo $bowling_team_data[$k]['f_name']; ?> <?php echo $bowling_team_data[$k]['last_name']; ?></option>
						<?php endfor; ?>
						</select></td>
						
						<td><input type="text" class="textb1" name="runs_scored_<?php echo $i+1; ?>" id="runs_scored_<?php echo $i+1; ?>" value="<?php if(isset($_POST['runs_scored_'.$z])) echo $_POST['runs_scored_'.$z];else echo $scorecard_data[$i]['runs_scored']; ?>"></td>
						<td><input type="text" class="textb1" name="balls_played_<?php echo $i+1; ?>" id="balls_played_<?php echo $i+1; ?>" value="<?php if(isset($_POST['balls_played_'.$z])) echo $_POST['balls_played_'.$z];else echo $scorecard_data[$i]['balls_played']; ?>"></td>
						<td><input type="text" class="textb1" name="fours_scored_<?php echo $i+1; ?>" id="fours_scored_<?php echo $i+1; ?>" value="<?php if(isset($_POST['fours_scored_'.$z])) echo $_POST['fours_scored_'.$z];else echo $scorecard_data[$i]['fours_scored']; ?>"></td>
						<td><input type="text" class="textb1" name="sixes_scored_<?php echo $i+1; ?>" id="sixes_scored_<?php echo $i+1; ?>" value="<?php if(isset($_POST['sixes_scored_'.$z])) echo $_POST['sixes_scored_'.$z];else echo $scorecard_data[$i]['sixes_scored']; ?>"></td>
						
					</tr>
					<?php $total_runs+=$scorecard_data[$i]['runs_scored']; $total_balls+=$scorecard_data[$i]['balls_played'];
					$total_fours+=$scorecard_data[$i]['fours_scored'];$total_sixes+=$scorecard_data[$i]['sixes_scored']; ?>
				<?php endfor; ?>
				
					<tr style="background-color:#E0E0E0;"><td colspan="4" ></td><td style="text-align:right;"><b>Total:</b></td><td><?php echo $total_runs; ?></td><td><?php echo $total_balls; ?></td>
					<td><?php echo $total_fours; ?></td><td><?php echo $total_sixes; ?></td></tr>
				<tr><td colspan="4" style="text-align:left;">
				<b>Innings Result: </b><select name="innings_result" id="innings_result">
					<option value="">Select One</option>
					<option value="Abandoned" <?php if(isset($_POST['innings_result']) && $_POST['innings_result'] == 'Abandoned') echo 'selected'; elseif($match_info["team{$batting_no}_innings_result"] =="Abandoned" ) echo 'selected'; ?>>Abandoned</option>
					<option value="All Out" <?php if(isset($_POST['innings_result']) && $_POST['innings_result'] == 'All Out') echo 'selected'; elseif($match_info["team{$batting_no}_innings_result"] =="All Out" ) echo 'selected'; ?>>All Out</option>
					<option value="Closed" <?php if(isset($_POST['innings_result']) && $_POST['innings_result'] == 'Closed') echo 'selected'; elseif($match_info["team{$batting_no}_innings_result"] =="Closed" ) echo 'selected'; ?>>Closed</option>
					<option value="Declared" <?php if(isset($_POST['innings_result']) && $_POST['innings_result'] == 'Declared') echo 'selected'; elseif($match_info["team{$batting_no}_innings_result"] =="Declared" ) echo 'selected'; ?>>Declared</option>
				</select>
				</td></tr>
				
				</table>
				 <div class="clear"><br/></div>
				<?php /* ?> <input type="submit" name="update_submit" id="update_submit" style="float:left;" value=" Update " class="submit-login">
				 <input type="button" name="cancel_btn" value=" Cancel " style="float:left;" class="submit-login" onclick="window.location.href='<?php echo WWW; ?>tournament/matches/list/<?php echo $tournament_id; ?>';">
				<?php */ ?>		
				
				
				<div class="clear"><br/></div>
				 <h2><?php echo $bowling_team_info['company_name']; ?> - Bowling</h2>
				
				
				<table id="table-list" class="white-box">
				<tr><th>No</th><th>Bowler</th><th>Overs</th><th>Maidens</th><th>Runs</th><th>Wides</th><th>No Balls</th><th>Wickets</th></tr>
				<?php for($i=0;$i<count($scorecard_bowling_data);$i++): ?>
					<?php $p = $i+1; ?>
					<tr>
						<td><?php echo $i+1; ?></td>
						<td><select name="bowler_id_bowl_<?php echo $i+1; ?>" id="bowler_id_bowl_<?php echo $i+1; ?>">
						<option value="0">Select One</option>
						<?php for($k=0;$k<count($bowling_team_data);$k++): ?>
						<?php if(isset($_POST['bowler_id_bowl_'.$p]) && $_POST['bowler_id_bowl_'.$p] == $bowling_team_data[$k]['id']) $sel = 'selected'; elseif(!isset($_POST['bowler_id_bowl_'.$p]) && $scorecard_bowling_data[$i]['bowler_id'] == $bowling_team_data[$k]['id']) $sel = 'selected';else $sel = ''; ?>
							<option value="<?php echo $bowling_team_data[$k]['id']; ?>" <?php echo $sel; ?>><?php echo $bowling_team_data[$k]['f_name']; ?> <?php echo $bowling_team_data[$k]['last_name']; ?></option>
						<?php endfor; ?>
						</select></td>
						
						<td><input type="text" class="textb1" name="overs_bowled_<?php echo $i+1; ?>" id="overs_bowled_<?php echo $i+1; ?>" value="<?php if(isset($_POST['overs_bowled_'.$p])) echo round($_POST['overs_bowled_'.$p],1);else echo round($scorecard_bowling_data[$i]['overs_bowled'],1); ?>"></td>
						<td><input type="text" class="textb1" name="maidens_bowled_<?php echo $i+1; ?>" id="maidens_bowled_<?php echo $i+1; ?>" value="<?php if(isset($_POST['maidens_bowled_'.$p])) echo $_POST['maidens_bowled_'.$p];else echo $scorecard_bowling_data[$i]['maidens_bowled']; ?>"></td>
						<td><input type="text" class="textb1" name="runs_conceded_<?php echo $i+1; ?>" id="runs_conceded_<?php echo $i+1; ?>" value="<?php if(isset($_POST['runs_conceded_'.$p])) echo $_POST['runs_conceded_'.$p];else echo $scorecard_bowling_data[$i]['runs_conceded']; ?>"></td>
						<td><input type="text" class="textb1" name="wide_balls_bowled_<?php echo $i+1; ?>" id="wide_balls_bowled_<?php echo $i+1; ?>" value="<?php if(isset($_POST['wide_balls_bowled_'.$p])) echo $_POST['wide_balls_bowled_'.$p];else echo $scorecard_bowling_data[$i]['wide_balls_bowled']; ?>"></td>
						<td><input type="text" class="textb1" name="no_balls_bowled_<?php echo $i+1; ?>" id="no_balls_bowled_<?php echo $i+1; ?>" value="<?php if(isset($_POST['no_balls_bowled_'.$p])) echo $_POST['no_balls_bowled_'.$p];else echo $scorecard_bowling_data[$i]['no_balls_bowled']; ?>"></td>
						<td><input type="text" class="textb1" name="wickets_taken_<?php echo $i+1; ?>" id="wickets_taken_<?php echo $i+1; ?>" value="<?php if(isset($_POST['wickets_taken_'.$p])) echo $_POST['wickets_taken_'.$p];else echo $scorecard_bowling_data[$i]['wickets_taken']; ?>"></td>
						
					</tr>
					<?php $total_overs+=$scorecard_bowling_data[$i]['overs_bowled']; $total_maidens+=$scorecard_bowling_data[$i]['maidens_bowled']; 
					$total_runs_conceded+=$scorecard_bowling_data[$i]['runs_conceded'];$total_wide_balls_bowled+=$scorecard_bowling_data[$i]['wide_balls_bowled']; 
					$total_no_balls_bowled+=$scorecard_bowling_data[$i]['no_balls_bowled'];$total_wickets_taken+=$scorecard_bowling_data[$i]['wickets_taken'];?>
				<?php endfor; ?>
				<tr style="background-color:#E0E0E0;"><td ></td><td style="text-align:right;padding-right:25px;"><b>Total:</b></td><td><?php echo $total_overs; ?></td><td><?php echo $total_maidens; ?></td>
				<td><?php echo $total_runs_conceded; ?></td><td><?php echo $total_wide_balls_bowled; ?></td>
				<td><?php echo $total_no_balls_bowled; ?></td><td><?php echo $total_wickets_taken; ?></td></tr>
					<tr><td colspan="6" style="text-align:left;">
							<b>Leg Byes: </b> <input type="text" class="textb1" name="leg_byes" id="leg_byes" value="<?php if(isset($_POST['leg_byes'])) echo $_POST['leg_byes'];else echo $match_info["team{$batting_no}_leg_byes"]; ?>">&nbsp;
							<b>Byes: </b> <input type="text" class="textb1" name="byes" id="byes" value="<?php if(isset($_POST['byes'])) echo $_POST['byes'];else echo $match_info["team{$batting_no}_byes"]; ?>">&nbsp;
							<b>Wide Balls:</b> <input type="text" class="textb1" name="wides" id="wides" value="<?php if(isset($_POST['wides'])) echo $_POST['wides'];else echo $match_info["team{$batting_no}_wides"]; ?>">&nbsp;
							<b>No Balls: </b> <input type="text" class="textb1" name="no_balls" id="no_balls" value="<?php if(isset($_POST['no_balls'])) echo $_POST['no_balls'];else echo $match_info["team{$batting_no}_no_balls"]; ?>">&nbsp;
							<b>Total Extras:</b>  <?php echo $match_info["team{$batting_no}_leg_byes"]+$match_info["team{$batting_no}_byes"]+$match_info["team{$batting_no}_wides"]+$match_info["team{$batting_no}_no_balls"]; ?> 
					</td></tr>		
				<tr><td colspan="10" style="text-align:left;"><b>Total score of <?php echo $batting_team_info['company_name']; ?>:</b> <?php echo $match_info["team{$batting_no}_score"]; ?> for <?php echo $match_info["team{$batting_no}_wickets"]; ?> wickets in <?php echo $match_info["team{$batting_no}_overs"]; ?> Overs</td></tr>
				</table>
				
				 <div class="clear"><br/></div>
				 <input type="submit" name="update_submit_bowling" id="update_submit_bowling" style="float:left;" value=" Update " class="submit-login">
				 <input type="button" name="cancel_btn" value=" Cancel " style="float:left;" class="submit-login" onclick="window.location.href='<?php echo WWW; ?>tournament/matches/list/<?php echo $tournament_id; ?>';">
				
				</form>
				
				
				<div class="clear"><br/></div>
				 <h2><?php echo $batting_team_info['company_name']; ?> - Fall of wickets</h2>
				
				<form method="post">
				<table id="table-fow" class="white-box">
				<tr><th>Wicket No</th>
				<?php for($i=1;$i<=10;$i++): ?>
					<th><?php echo $i; ?></th>
				<?php endfor; ?>
				</tr>
				
				<tr><td style="text-align:left;"><b>Batsman</b></td>
				<?php for($i=0;$i<count($scorecard_fow_data);$i++):  ?>
					<?php $p = $i+1; ?>
					<td><select name="fow_batsman_id_<?php echo $i+1; ?>" id="fow_batsman_id_<?php echo $i+1; ?>" >
						<option value="0">Select One</option>
						<?php for($k=0;$k<count($batting_team_data);$k++): ?>
						<?php if(isset($_POST['fow_batsman_id_'.$p]) && $_POST['fow_batsman_id_'.$p] == $bowling_team_data[$k]['id']) $sel = 'selected';elseif(!isset($_POST['fow_batsman_id_'.$p]) && $scorecard_fow_data[$i]['batsman_id'] == $batting_team_data[$k]['id']) $sel = 'selected';else $sel = ''; ?>
							<option value="<?php echo $batting_team_data[$k]['id']; ?>" <?php echo $sel; ?>><?php echo $batting_team_data[$k]['f_name']; ?> <?php echo $batting_team_data[$k]['last_name']; ?></option>
						<?php endfor; ?>
						</select>
					</td>
				<?php endfor; ?>
				</tr>
				
				<tr><td style="text-align:left;"><b>Score</b></td>
				<?php for($i=1;$i<=10;$i++):  ?>
					<td><input type="text" class="textb1" name="score_<?php echo $i; ?>" id="score_<?php echo $i; ?>" value="<?php if(isset($_POST['score_'.$i])) echo $_POST['score_'.$i];else echo $scorecard_fow_data[$i-1]["score"]; ?>"></td>
				<?php endfor; ?>
				</tr>
				
				<tr><td style="text-align:left;"><b>Partnership</b></td>
				<?php for($i=1;$i<=10;$i++):  ?>
					<td><input type="text" class="textb1" name="partnership_<?php echo $i; ?>" id="partnership_<?php echo $i; ?>" value="<?php if(isset($_POST['partnership_'.$i])) echo $_POST['partnership_'.$i];else echo $scorecard_fow_data[$i-1]["partnership"]; ?>"></td>
				<?php endfor; ?>
				</tr>
				
				<tr><td style="text-align:left;"><b>Overs</b></td>
				<?php for($i=1;$i<=10;$i++):  ?>
					<td><input type="text" class="textb1" name="overs_<?php echo $i; ?>" id="overs_<?php echo $i; ?>" value="<?php if(isset($_POST['overs_'.$i])) echo $_POST['overs_'.$i];else echo $scorecard_fow_data[$i-1]["overs"]; ?>"></td>
				<?php endfor; ?>
				</tr>
				
				</table>
				 <div class="clear"><br/></div>
				 <input type="submit" name="update_submit_fow" id="update_submit_fow" style="float:left;" value=" Update " class="submit-login">
				 <input type="button" name="cancel_btn" value=" Cancel " style="float:left;" class="submit-login" onclick="window.location.href='<?php echo WWW; ?>tournament/matches/list/<?php echo $tournament_id; ?>';">
				
				</form>
				
				<?php }else{ ?>
				<div id="error">You do not have Permission for this Tournament... !</div>
				<?php } ?>
				
			</div>  
		    <div class="clear"></div>
			</div>
		</div>
		<!--<div class="rightbar"><?php //include('common/right-panel.php');?></div>-->
		<div class="clear"></div>
	</div>
	


<?php include('common/footer.php'); ?>