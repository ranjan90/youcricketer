<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	<div class="middle">
		<h1>Change Password </h1>
		<? 	
		if(isset($_POST) && !empty($_POST)){
				$user_id	= $_SESSION['ycdc_dbuid'];
				$old	= $_POST['old_password'];
				$new	= $_POST['new_password'];

				$row	= get_record_on_id('users',$user_id);
				if(md5($old) == $row['password']){
					$query = "update users set password = '".md5($new)."' where id = '$user_id'";
					if(mysqli_query($conn,$query)){
						
						$userid = $_SESSION['ycdc_dbuid'];
						$date = date('m/d/Y h:i:s a', time());	
						$newTime = date('l, jS F Y', strtotime($date));
						$sql=mysqli_fetch_assoc(mysqli_query($conn,"select email,f_name,last_name from users where id = '$userid'"));
						$email_template = get_record_on_id('cms', 20);
						$mail_title		= $email_template['title'];
		                $mail_content	= $email_template['content'];
		                $mail_content 	= str_replace('{name}',$sql['f_name'], $mail_content);
					    $mail_content 	= str_replace('{last_name}',$sql['last_name'], $mail_content);
						$mail_content 	= str_replace('{date}',$newTime, $mail_content);
						 $mail_content 	= str_replace('{login_link}','<a href="'.WWW.'login.html" title="Contact Us">HERE</a>', $mail_content);
		               $mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);
						

							$headers = "Content-type:text/html;charset=UTF-8" . "\r\n";
							$headers .= "From: YouCricketer@no-reply.com\r\n";	

 
		
						mail($sql['email'],$mail_title,$mail_content,$headers);
						
						echo '<div id="success">Information Updated ... !</div>';
						
						?>
						
						<?
					}
				}else{
					echo '<div id="error">Please enter correct old password ... !</div>';
				}
			}
		?>

		<div class="white-box content" id="dashboard">
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
			<form method="post" action="">
				<fieldset>
					<h2>Enter Information</h2>
					<div class="form-box">
						<label>Current Password </label>
						<input type="password" name="old_password" class="input-login validate[required]">
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>New Password </label>
						<input type="password" name="new_password" id="new_password" class="input-login validate[required, custom[password]]">
					</div>
					<div class="form-box" style="width:30%;">
						<div id="password-result"></div>
						<p>One digit and length should be minimum 8</p>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Confirm New Password </label>
						<input type="password" name="confirm_password" class="input-login validate[required, equals[new_password]]">
					</div>
					<div class="form-box" style="width:30%">
						<div id="confirm-password-result"></div>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<input type="submit" value=" Submit " class="submit-login">
					</div>
				</fieldset>
			</form>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<style>
.form-box{width:60%;}
</style>
<script src="<?=WWW?>js/jquery-1.8.0.js" type="text/javascript"></script>
<script>
$(document).ready(function(){
	$('input[name=new_password]').keyup(function(){
		var password = this.value;
		var score = 0;

		if (password.length >= 8)
	        score++;
	    if (password.length >= 9)
	        score++;
	    if (password.match(/^.*(?=.{7,})(?=.*[a-z])(?=\S*?[0-9]).*$/))
	        score++;

	    if(score == '3'){
	    	$('#password-result').html('Strong Password');
	    	$('#password-result').css('background','green');
	    }else if(score == '1'){
	    	$('#password-result').html('Not Strong Password');
	    	$('#password-result').css('background','orange');
	    }
	    else if(score == '2'){
	    	$('#password-result').html('Not Strong Password');
	    	$('#password-result').css('background','orange');
	    }
	});

	$('input[name=confirm_password]').keyup(function(){
		if($('input[name=confirm_password]').val() === $('input[name=new_password]').val()){
			$('#confirm-password-result').css('background', 'green');
			$('#confirm-password-result').html('Password Matched');
		}
	});
	$('#dashboard .black-box h2').click(function(){
		$('#dashboard .black-box ul').css('display','none');
		var id = this.id;
		$('#'+id+'-box ul').css('display','block');
	});
});
</script>
<?php include('common/footer.php'); ?>