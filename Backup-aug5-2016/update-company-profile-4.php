<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	<div class="middle">
		<h1>Company Management</h1>
		<? 	if(isset($_POST) && $_POST['action'] == 'add-staff'){
			
				$title 	 		= $_POST['title'];
				$first_name 	= $_POST['first_name'];
				$last_name 		= $_POST['last_name'];
				$user_id 		= $_SESSION['ycdc_dbuid'];
				if(!empty($title) && !empty($first_name) && !empty($last_name)){
					$rs_chk = mysqli_query($conn,"select * from company_staff where user_id = '$user_id' and title = '$title' and first_name = '$first_name' and last_name = '$last_name'");
					if(mysqli_num_rows($rs_chk) == 0){
						mysqli_query($conn,"insert into company_staff (user_id, title, first_name, last_name, type) 
							values ('$user_id','$title','$first_name','$last_name','management');");
						echo '<div id="success"><b>Success : </b>Information updated ... !</div><br><br>';
					}
				}
			}
			if(isset($_GET) && $_GET['action'] == 'delete' && $_GET['type'] == 'staff'){
				mysqli_query($conn,"delete from company_staff where id = '".$_GET['id']."'");
				echo '<div id="success"><b>Success : </b>Information updated ... !</div><br><br>';
				
			}
			if(isset($_POST) && $_POST['action'] == 'edit-staff'){

				$title 	 		= $_POST['title'];
				$first_name 	= $_POST['first_name'];
				$last_name 		= $_POST['last_name'];
				$recordId 		= $_POST['recordId'];

				mysqli_query($conn,"update company_staff set title = '$title', first_name = '$first_name', last_name = '$last_name' where id = '$recordId'");
				echo '<div id="success"><b>Success : </b>Information updated ... !</div><br><br>';
				
			}
		?>

		<div class="white-box content" id="dashboard">
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
				<? include('common/company-profile-navigation.php');?>
			<form method="post" action="" enctype="multipart/form-data">
				<fieldset>
					
					<div class="form-box" style="width:100%;">
						<label><b>Company Management (please list them in hierarchy)</b><a style="color:#fff;" class="no-bg right popup-link" href="#add-staff"><img src="<?=WWW?>images/icons/add.png"> Add</a></label>
						
					</div>
					<table width="100%" border="1" style="border-collapse:collapse;" id="inbox">
					<tr>
						<th></th>
						<th class="tleft">Title</th>
						<th class="tleft">First Name</th>
						<th class="tleft">Last Name</th>
					</tr>
			        <? 	$rs_msg = mysqli_query($conn,"select cs.* from company_staff cs where cs.user_id = '".$row_user['id']."' and type='management' order by id "); 
			        	if(mysqli_num_rows($rs_msg) == 0){
			        		?>
			        		<tr>
			        			<td colspan="3">No record added<td>
			        		</tr>
			        		<?
			        	}else{
				       		while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
				       		<tr title="Click for detail">
				       			<td width="20%">
				       				<a class="no-bg no-padding popup-link" href="#edit-staff" id="<?=$row_msg['id']?>"><img src="<?=WWW?>images/icons/edit.png"></a>
				       				<a class="no-bg no-padding" href="<?=WWW?>update-company-profile-5.html?action=delete&type=staff&id=<?=$row_msg['id']?>"><img src="<?=WWW?>images/icons/delete.png"></a>
				       			</td>
				       			<td><?=$row_msg['title']?></td>
				       			<td><?=$row_msg['first_name']?></td>
				       			<td><?=$row_msg['last_name']?></td>
				       		</tr>
				       	<?  }
				       	} ?>
			        </table>
					<div class="clear"></div>
					<div class="form-box">
						<a href="<?=WWW?>dashboard.html" class="submit-login left">Back</a>
					</div>
					<div class="form-box">
						<input type="submit" value=" Update " class="submit-login">
					</div>
				</fieldset>
			</form>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="add-staff" class="popup hide"style="margin-top:-500px;">
	<h1>
		Add Company Staff
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h1>
	<form method="post" action="">
		<input type="hidden" name="action" value="add-staff">
		<fieldset>
			<div class="form-box">
				<input type="text" name="title" class="validate[required] input-login margin-10" placeholder="Type Title of employee">
				<br>
				<input type="text" name="first_name" class="validate[required] input-login margin-10" placeholder="Type First Name">
				<br>
				<input type="text" name="last_name" class="validate[required] input-login margin-10" placeholder="Type Last Name">
				<br><br>
				<input type="submit" class="submit-login margin-top-5" value="Save">
			</div>
		</fieldset>
	</form>
</div>
<div id="edit-staff" class="popup hide"style="margin-top:-500px;">
	<h1>
		Update Company Staff
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h1>
	<form method="post" action="">
		<input type="hidden" name="action" value="edit-staff">
		<input type="hidden" name="recordId" id="recordId" value="">
		<fieldset>
			<div class="form-box">
				<input type="text" name="title" class="validate[required] input-login margin-10" placeholder="Type Title of employee">
				<br>
				<input type="text" name="first_name" class="validate[required] input-login margin-10" placeholder="Type First Name">
				<br>
				<input type="text" name="last_name" class="validate[required] input-login margin-10" placeholder="Type Last Name">
				<br><br>
				<input type="submit" class="submit-login margin-top-5" value="Save">
			</div>
		</fieldset>
	</form>
</div>
<script src="<?php echo WWW?>js/jquery-1.8.0.js" type="text/javascript"></script>
<script>
$(document).ready(function(){
	$('a.popup-link').click(function(){
		var link = $(this).attr('href');
		if(link == '#edit-staff'){
			var id = this.id;
			$.ajax({type	: 'POST', 
				url		: WWW + 'includes/get-record.php', 
				data	: ({table:'company_staff',field:'*',id:id}),
				success	: function(msg){
					msg1 = msg.split('___');
					$(link+' input[name=title]').val(msg1[0]);
					$(link+' input[name=first_name]').val(msg1[1]);
					$(link+' input[name=last_name]').val(msg1[2]);
					$(link+' #recordId').val(id);
				}
			});
		}
		
		$(link).removeClass('hide');
	});
	$('.close-popup').click(function(){
		$('.popup').addClass('hide');
	});
});
</script>
<?php include('common/footer.php'); ?>