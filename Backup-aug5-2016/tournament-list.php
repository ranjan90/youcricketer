<?php include_once('includes/configuration.php');
$page = 'tournament-list.html';

$selected_country = getGeoLocationCountry(); 

$league_clubs = array();
$league_info = array();
$tournaments = array();
$permitted_tournaments = array();
$error = '';

if(isset($_GET['page']) && !empty($_GET['page'])){
	$current_page = str_replace('page','',trim($_GET['page']));
}else{
	$current_page = 1;
}

$records_per_page = 10;
$start_record  = ($current_page-1)*$records_per_page;

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
}

$sql = "SELECT  *  FROM companies  WHERE user_id = $user_id ";
$rs_league = mysqli_query($conn,$sql);
$league_info = mysqli_fetch_assoc($rs_league);
if(!empty($league_info) && in_array($league_info['company_type_id'],array(1,2,3,35))){
	$league_permission = 1;
}else{
	$league_permission = 0;
}

$sql = "select * from tournament_permissions where member_id = $user_id";
$rs_permission = mysqli_query($conn,$sql);
if(mysqli_num_rows($rs_permission)>0){
	$member_permission = 1;
	while($row = mysqli_fetch_assoc($rs_permission)){
		$permitted_tournaments[] = $row['tournament_id'];
	}
}else{
	$member_permission = 0;
}

if($league_permission || $member_permission){
	$tournament_permission = 1;
}else{
	$tournament_permission = 0;
}

if(empty($error) && isset($_GET['id']) && $_GET['action'] == 'delete'){
	$id = trim($_GET['id']);
	$tournament_info = get_record_on_id('tournaments', $id);	
	if($tournament_info['user_id'] == $user_id){
		$sql = "DELETE FROM tournaments WHERE id=$id ";
		mysqli_query($conn,$sql);
		$sql = "DELETE FROM tournament_teams WHERE tournament_id=$id ";
		mysqli_query($conn,$sql);
	
		$sql = "DELETE FROM tournament_bat_scorecard WHERE match_id IN(Select match_id from tournament_matches WHERE tournament_id=$id) ";
		mysqli_query($conn,$sql);
		$sql = "DELETE FROM tournament_bowl_scorecard WHERE match_id IN(Select match_id from tournament_matches WHERE tournament_id=$id) ";
		mysqli_query($conn,$sql);
		$sql = "DELETE FROM tournament_fow_scorecard WHERE match_id IN(Select match_id from tournament_matches WHERE tournament_id=$id) ";
		mysqli_query($conn,$sql);
		$sql = "DELETE FROM tournament_matches WHERE tournament_id=$id ";
		mysqli_query($conn,$sql);
		$sql = "DELETE FROM tournament_permissions WHERE tournament_id=$id ";
		mysqli_query($conn,$sql);
		
		$_SESSION['tournament_deleted'] = 1;
	}
	header("Location:".WWW."tournament/list");
	exit();
}

if(isset($_SESSION['tournament_deleted']) && $_SESSION['tournament_deleted']==1) {
	$tournament_deleted = 1;
	unset($_SESSION['tournament_deleted']);
}
if(isset($_SESSION['tournament_updated']) && $_SESSION['tournament_updated']==1) {
	$tournament_updated = 1;
	unset($_SESSION['tournament_updated']);
}
if(isset($_SESSION['tournament_added']) && $_SESSION['tournament_added']==1) {
	$tournament_added = 1;
	unset($_SESSION['tournament_added']);
}



 
if(!empty($permitted_tournaments)){
	$str = implode(',',$permitted_tournaments);
	//$perms_sql = " OR t.id IN($str) ";
	$perms_sql = '';
}else{
	$perms_sql = '';
}

 $sql = "SELECT  count(*) as record_count FROM tournaments as t WHERE (t.user_id = $user_id $perms_sql) and t.status = 1";
$rs_total = mysqli_query($conn,$sql);
$row_total = mysqli_fetch_assoc($rs_total);
$records_count = $row_total['record_count'];
					

 
 $sql = "SELECT t.*,c.name as country_name FROM tournaments as t 
 left join countries as c on .t.country_id = c.id
 WHERE (t.user_id = $user_id $perms_sql) and t.status = 1  ORDER BY t.id DESC LIMIT $start_record,$records_per_page";
$rs_tournaments = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_tournaments)){
	$tournaments[] = $row;
}

$page_title = 'League Tournaments List - '.$league_info['company_name'];
?>
<?php include('common/header.php'); ?>
<style>
.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
dl {min-height:145px;}
.content {width:1010px;}
.large-column {width:790px;}
#table-list {width:100%;border:1px solid #ccc;}
#table-list tr{border:1px solid #ccc;}
#table-list th{padding:5px;background-color:#000;color:#FB7008;font-size:14px;}
#table-list td{padding:5px;text-align:center;}
#table-list a{color:#000;}
.add_tournament{font-size:14px;color:#000;}
</style>
	<div class="middle">
		<h1> League Tournaments <?php if($league_permission): ?> - <?php echo $league_info['company_name']; ?><?php endif; ?></h1>
		
		<div class="white-box content" id="dashboard">
			<?php if(isset($tournament_deleted) && $tournament_deleted == 1): ?>
				<div id="information">Tournament deleted Successfully... !</div>
			<?php endif; ?>
			<?php if(isset($tournament_updated) && $tournament_updated == 1): ?>
				<div id="information">Tournament updated Successfully... !</div>
			<?php endif; ?>
			<?php if(isset($tournament_added) && $tournament_added == 1): ?>
				<div id="information">Tournament added Successfully... !</div>
			<?php endif; ?>
		
			<?php if(empty($user_info)): ?>
				<div id="error">You are not logged... !</div>
			<?php endif; ?>
			
			<?php if(!empty($error)): ?>
				<?php echo $error; ?>
			<?php endif; ?>
		
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
			<?php if($tournament_permission): ?>
			<?php if($league_permission): ?>
				<p style="float:right;"><a href="<?php echo WWW; ?>tournament/add" class="add_tournament" title="Add New Tournament"><img alt="Add New Tournament" src="<?php echo WWW; ?>images/icons/add.png" border="0"> Add New Tournament</a></p>
			<?php endif; ?>
			 <div class="clear"></div>
				<table id="table-list" class="white-box">
				<tr><th>Tournament Title</th><th>Game Type</th><th>Overs</th><th>Ball Type</th><th>Country</th><th>Start Date</th><th>End Date</th><th style="width:16%;">Action</th></tr>
				<?php for($i=0;$i<count($tournaments);$i++): ?>
					<tr>
						<td><?php echo ucwords($tournaments[$i]['title']); ?></td>
						<td><?php echo $tournaments[$i]['overs_type']; ?></td>
						<td><?php echo $tournaments[$i]['overs_count']; ?> overs</td>
						<td><?php echo ucwords(str_replace('_',' ',$tournaments[$i]['ball_type'])); ?></td>
						<td><?php echo ucwords($tournaments[$i]['country_name']); ?></td>
						<td><?php echo  date('d M, Y',strtotime($tournaments[$i]['start_time'])); ?></td>
						<td><?php echo date('d M, Y',strtotime($tournaments[$i]['end_time'])); ?></td>
						<td>
						<?php if($tournaments[$i]['user_id'] == $user_id): ?>
							<a href="<?php echo WWW; ?>tournament/clubs/<?php echo $tournaments[$i]['id']; ?>" title="Tournament Clubs & Groups"><img alt="Tournament Clubs & Groups" src="<?php echo WWW; ?>images/icons/club.png" border="0"></a>  &nbsp;&nbsp;
							<a href="<?php echo WWW; ?>tournament/tournament-permissions-<?php echo $tournaments[$i]['id']; ?>.html" title="Umpires/Referees/Scorers Permission"><img alt="Umpires/Referees/Scorers Permission" src="<?php echo WWW; ?>images/icons/users.png" border="0"></a> &nbsp;&nbsp;
							<a href="<?php echo WWW; ?>tournament/standings/<?php echo $tournaments[$i]['id']; ?>" title="Tournament Standings"><img alt="Tournament Standings" src="<?php echo WWW; ?>images/icons/ranking.png" border="0"></a>  &nbsp;&nbsp;
							<a href="<?php echo WWW; ?>tournament/polls/list/<?php echo $tournaments[$i]['id']; ?>" title="Tournament Polls"><img alt="Polls" src="<?php echo WWW; ?>images/icons/poll.png" border="0"></a>  &nbsp;&nbsp;
							<br/>
						<?php endif; ?>
						
						
							<a href="<?php echo WWW; ?>tournament/matches/list/<?php echo $tournaments[$i]['id']; ?>" title="Schedule & Score Book"><img alt="Schedule & Score Book" src="<?php echo WWW; ?>images/icons/match.png" border="0"></a> 
						<?php if($tournaments[$i]['user_id'] == $user_id): ?>			
							&nbsp;&nbsp; <a href="<?php echo WWW; ?>tournament/edit/<?php echo $tournaments[$i]['id']; ?>" title="Edit Tournament"><img alt="Edit Tournament" src="<?php echo WWW; ?>images/icons/edit.png" border="0"></a>  
							&nbsp;&nbsp; <a href="<?php echo WWW; ?>tournament-list.php?id=<?php echo $tournaments[$i]['id']; ?>&action=delete" onclick="return confirm('Are you sure to delete tournament.\nAll matches and other data will be deleted.');" title="Delete Tournament"><img alt="Delete" src="<?php echo WWW; ?>images/icons/delete.png" border="0"></a>
						<?php endif; ?>	 
						
						</td>
					</tr>
				<?php endfor; ?>
				<?php if(empty($tournaments)): ?>
				<tr><td colspan="4">No Records</td></tr>
				<?php endif; ?>
				</table>
				
				<div class="clear"></div>
				
				<div id="pagination-bottom">
				<?php  $paging_str = getPaging('tournament/list',$records_count,$current_page,$records_per_page);
				echo $paging_str;
				?>
		        <div class="clear"></div>
				</div>  
				<?php else: ?>
					<p id="error">You do not have Permission for this page.</p>
				<?php endif; ?>
			</div>  
		    <div class="clear"></div>
			
			
			
			</div>
		</div>
		<!--<div class="rightbar"><?php //include('common/right-panel.php');?></div>-->
		<div class="clear"></div>
	</div>
	


<?php include('common/footer.php'); ?>