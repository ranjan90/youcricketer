<?php include('common/header.php'); ?>
	<div class="middle">
		<h1>Forum Detail - <?=get_combo('forum_topics','title',$_GET['id'],'','text')?> </h1>
		<div class="white-box content detail">
			<? 
				$row = get_record_on_id('forum_topics',$_GET['id']);
				$row_img= mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'groups' and entity_id = '".$row_g['id']."'"));
			?>
			<?
			if(isset($_POST['action']) && $_POST['action'] == 'comment' && isset($_SESSION['ycdc_user_name']) && !empty($_SESSION['ycdc_user_name'])){
					$comments 	= $_POST['comments'];
					$user_id 	= $_SESSION['ycdc_dbuid'];
					$date 		= date('Y-m-d H:i:s');
					$topic_id 	= $_GET['id'];

					$rs_chk 	= mysqli_query($conn,"select * from forum_replies where user_id = '$user_id' and topic_id = '$topic_id' and comment = '$comments'");
					if(mysqli_num_rows($rs_chk) == 0){
						$query 		= "insert into forum_replies (comment, topic_id, user_id, post_date, status) 
									values ('$comments','$topic_id','$user_id','$date','1')";	
						if(mysqli_query($conn,$query)){
							?><div id="success">Comments saved successfully ... !</div><?
						}else{
							?><div id="success">Comments already submitted ... !</div><?
						}
					}
					
				}
			?>
			<h2><?=get_combo('forum_topics','title',$row['id'],'','text')?></h2>
			<table border="1" width="100%" style="border-collapse:collapse; ">
				<tr>
					<td width="25%" >
						<div><b>Created By : </b><?=get_combo('users','concat(f_name, last_name)',$row['user_id'],'','text')?></div>
						<div><b>Created On : </b><?=date_converter($row['create_date'])?></div>
						<div><b>Category : </b<?=get_combo('forum_categories','name',$row['category_id'],'','text')?></div>
					</td>
					<td >
						<?=$row['content']?>
					</td>
				</tr>
				<tr><td colspan="2"><h2>Posted Replies</h2></td></tr>
				<?  $query = "select * from forum_replies where status=1 and topic_id = '".$row['id']."' order by id desc ";
				    //echo $query;
				    //=======================================
				    $rs   = mysqli_query($conn,$query);
				    //=======================================
      				while($rowC = mysqli_fetch_assoc($rs)){ ?>
      				<tr>
      					<td>
      						<div><b>Replied By : </b><?=get_combo('users','concat(f_name, last_name)',$rowC['user_id'],'','text')?></div>
      						<div><b>Replied On : </b><?=date_converter($rowC['post_date'])?></div>
      						<? if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	                            if($row['user_id'] == $_SESSION['ycdc_dbuid']){ ?>
	                            <a href="<?=WWW?>delete-forum-replies-<?=$rowC['id']?>.html"><img src="<?=WWW?>images/icons/delete.png"></a>
	                         <? } ?>
	                      	<? } ?>
      					</td>
      					<td>
      						<p><?=$rowC['comment'];?></p>	    
      					</td>
      				</tr>
      				<tr><td colspan="2"><hr></td></tr>
      				<?php } ?>
			</table>
			
			<h2>Post Your Reply</h2>
			<? include('common/login-box.php');?>
			<? if(isset($_SESSION['ycdc_user_name']) && !empty($_SESSION['ycdc_user_name'])){ ?>
		    <form method="post" action="">
		    	<input type="hidden" name="action" value="comment">
				<fieldset>
					<div class="form-box">
						<textarea name="comments" style="margin-left: -5px; width:400px;" class="<?=(!isset($_SESSION['ycdc_user_name']) || empty($_SESSION['ycdc_user_name']))?'':'validate[required]'?>"></textarea>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<input type="submit" value="Save"  class="submit-login">
					</div>
					<div class="form-box">
						<a id="cancel" class="submit-login" href="<?=WWW?>forums.html">Cancel</a>
					</div>
				</fieldset>
			</form>
			<? }else{ ?>
				<div id="success">Please login first to post your reply ... !</div>
			<? } ?>
		    <div class="clear"></div>
		
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>
<style>
.detail p{font-weight: normal;}
#cancel{background: url('<?php echo WWW;?>images/sign-in-button.png') no-repeat !important; color:#fff !important; padding-left:3px; top:0px !important;position:relative; left:-150px;}
</style>
<?php include('common/footer.php'); ?>