<?php include('common/header.php'); ?>
	<div class="middle">
		<?php
		$selected_country = getGeoLocationCountry();
			 
		?>
		<h1> Events <?=(isset($_GET['type']) && $_GET['type'] == 'your_region')?'In Your Region - '.$selected_country:'';?> </h1>
		<div class="white-box content">
		<? include('common/login-box.php');?>	
			<style>
			#login-box{display:none;}
			</style>
			
			<div id="pagination-top"></div>
			<div class="list">
                <ul>
                	<?  $rpp = PRODUCT_LIMIT_FRONT; // results per page
            			$ppage = intval($_GET["page"]);
      					if($ppage<=0) $ppage = 1;
      					$select = "select distinct e.id as eid,e.start_date, e.end_date,e.state_id as estate,e.city_name as ecity, e.title as title, e.create_date from events e ";
      					$where = " where e.status=1 ";
				      	//=======================================
					    if(isset($_GET['keywords']) && $_GET['keywords'] != 'Search Here' && $_GET['keywords'] != 'all'){
					        $where  .= "and e.title like '%".str_replace('-','%',$_GET['keywords'])."%'";
							$keywords = trim($_GET['keywords']);
					    }else{
							$keywords = '';
						}
					    if(isset($_GET['type']) && $_GET['type'] == 'your_region'){ 
					    //	$where .= " and e.user_id = '".$_SESSION['ycdc_dbuid']."'";
					    }
					    if(isset($_GET['type']) && $_GET['type'] == 'your_region'){
					    	
							$row_country = mysqli_fetch_assoc(mysqli_query($conn,"select * from countries where name = '$selected_country'"));
							$join .= " join users u on u.id = e.user_id and (u.country_id = '".$row_country['id']."' or e.is_global = '1')";
					    }
					    $query = $select.$join.$where." order by e.id desc ";
				 //  echo $query; 
				      //=======================================
				      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
				        echo '<div id="error">Your search returned no results</div>';
				      }
				      $rs   = mysqli_query($conn,$query);
				      $tcount = mysqli_num_rows($rs);
				      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
				      $count = 0;
				      $i = ($ppage-1)* $rpp;
          			  $x = 0;
          			  //=======================================
      				  while(($count<$rpp) && ($i<$tcount)){
                		mysqli_data_seek($rs,$i);
                		$row_g 	= mysqli_fetch_assoc($rs);
                		$row_img= mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'events' and entity_id = '".$row_g['eid']."'"));
                		$row_u  = get_record_on_id('users', $row_g['user_id']);
  	
      					?>

      					<li>
							<a href="<?=WWW?>event-detail-<?=$row_g['eid']?>-<?=friendlyURL($row_g['title'])?>.html" title="Read more" />
								<img src="<?=($row_img)?WWW.'events/'.$row_g['eid'].'/'.$row_img['file_name']:WWW.'images/community-groups.png';?>" alt="<?=$row_g['title']?>" title="<?=$row_g['title']?>" width="114" />
							</a>
	                        <span class="list-text">
	                            <h3 onclick="window.location='<?=WWW?>event-detail-<?=$row_g['eid']?>-<?=friendlyURL($row_g['title'])?>.html'"><?=$row_g['title']?></h3>
	                            <p><b>Start Date</b> : <?=date_converter($row_g['start_date'])?>
	                            	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	                            	<b>End Date</b> : <?=date_converter($row_g['end_date'])?>
	                            </p>
	                            <p><b>Location : </b> <?=get_combo('countries','name',$row_u['country_id'],'','text')?>> <?=get_combo('states','name',$row_g['estate'],'','text')?> ><?=$row_g['ecity']?> </p>
	                            <a href="<?=WWW?>event-detail-<?=$row_g['eid']?>-<?=friendlyURL($row_g['title'])?>.html" title="Read more" class="submit-login" />Read More</a>
	                        </span>
	                        <span class="list-social">
	                         	<!-- AddThis Button BEGIN -->
						        <div class="addthis_toolbox addthis_default_style ">
						        <a class="addthis_button_preferred_1"></a>
						        <a class="addthis_button_preferred_2"></a>
						        <a class="addthis_button_preferred_3"></a>
						        <a class="addthis_button_preferred_4"></a>
						        <a class="addthis_button_compact"></a>
						        <a class="addthis_counter addthis_bubble_style"></a>
						        </div>
						        <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
						        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5073efa669740139"></script>
						        <!-- AddThis Button END -->
	                         	<br> <br> <br>
	                         	Timezone : <?=get_combo('timezones','time_difference',$row_u['timezone'],'','text')?>
	                         	<br>
	                            <?=date_converter($row_g['create_date'],'M d,Y H:i')?>
	                           <br/>
	                            <? if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){ ?>
	                            <? if($row_g['user_id'] == $_SESSION['ycdc_dbuid'] 
	                            	   || $_SESSION['ycdc_desig_id'] == '1'
	                            	   || $_SESSION['ycdc_user_email'] == 'naveed.ramzan@gmail.com'
       	  								||$_SESSION['ycdc_user_email'] == 'youcrickter00@gmail.com'){ ?>
	                            		<a href="<?=WWW?>edit-event-<?=$row_g['eid']?>.html"><img src="<?=WWW?>images/icons/edit.png"></a>
	                            		<a href="<?=WWW?>delete-event-<?=$row_g['eid']?>.html"><img src="<?=WWW?>images/icons/delete.png"></a>
	                            	<? } ?>
	                            <? } ?>
	                        </span>
						</li>
					    <?
					      $i++;
					      $count++;
					      $x++;
					  } 
					  ?>
			</ul>
			</div>
			<div id="pagination-bottom" class="hide">
				<? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_FRONT){ ?>
		      	<?php
		        	//$reload = 'events.html?';
					if(isset($_GET['type']) && $_GET['type'] == 'your_region'){
						$reload = 'your-region-events.html?';
					}else{
						if(!empty($keywords)){
							$reload = "events-{$keywords}.html?";
						}else{
							$reload = "events-all.html?";
						}
					}
		        	echo paginate_one($reload, $ppage, $tpages);
		      	?>
		      	<input type="hidden" name="pagination-page" value="events.html">
		        <? } ?>    
		        <form id="list-search" method="post" action="">
		        	<input type="text" name="txtsearch" placeholder="Search Here" class="validate[required] input-login">
		        	<input type="submit" value="">
		        </form>
		        <script type="text/javascript">
		        $('form#list-search').submit(function(e){
					var parentId = $(this).parent().attr('id');
					var string = $('#'+parentId +' form input[name=txtsearch]').val();
					if(string != '' && string != 'Search Here'){
						string = string.replace(/[^a-zA-Z0-9]+/g,'-').toLowerCase();
						if(string.length > 0){
							$('form#list-search').attr('action','<?=WWW;?>events-' + string + '.html');
						}
					}
				});
		        </script>
		        <div class="clear"></div>
		        <div class="add" style="<?=(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_FRONT)?'':'top:-27px !important';?>">
					<a href="<?=(!isset($_SESSION['ycdc_user_name']) || empty($_SESSION['ycdc_user_name']))?'#login':WWW.'add-event.html';?>" class="popup-link right submit-login no-shaddow" >Create Event</a>
				</div>
		    </div>  
		    <div class="clear"></div>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>
<? //include('common/login-box.php');?>	
<style>
#login-box{display:none;}
</style>
<?php include('common/footer.php'); ?>