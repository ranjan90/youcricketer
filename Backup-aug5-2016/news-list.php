<?php include_once('includes/configuration.php');
if(!isset($_SESSION['ycdc_user_email']) || empty($_SESSION['ycdc_user_email'])){
	header("Location:login.html");
	exit();
}

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

$page = 'company-news-list.html';
$page_title = 'My News List';
$user_id 	= $_SESSION['ycdc_dbuid'];

if(isset($_GET['id']) && $_GET['action'] == 'delete'){
	$id = trim($_GET['id']);
	$news_info = get_record_on_id('company_news', $id);	
	if($news_info['user_id'] == $user_id){
		$sql = "DELETE FROM company_news WHERE id = $id LIMIT 1";
		mysqli_query($conn,$sql);
		$_SESSION['news_deleted']=1;
		header("Location:company-news-list.html");
		exit();
	}
}	

if(isset($_SESSION['news_deleted']) && $_SESSION['news_deleted']==1) {
	$news_deleted = 1;
	unset($_SESSION['news_deleted']);
}
if(isset($_SESSION['news_updated']) && $_SESSION['news_updated']==1) {
	$news_updated = 1;
	unset($_SESSION['news_updated']);
}
if(isset($_SESSION['news_added']) && $_SESSION['news_added']==1) {
	$news_added = 1;
	unset($_SESSION['news_added']);
}

include('common/header.php'); ?>


	<div class="middle">
		<h1>Company News</h1>
		
		<div class="white-box content" id="dashboard">
		<?php if(isset($news_deleted) && $news_deleted == 1): ?>
				<div id="information">News deleted Successfully... !</div>
			<?php endif; ?>
			<?php if(isset($news_updated) && $news_updated == 1): ?>
				<div id="information">News updated Successfully... !</div>
			<?php endif; ?>
			<?php if(isset($news_added) && $news_added == 1): ?>
				<div id="information">News added Successfully... !</div>
			<?php endif; ?>
			
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
			<p style="float:right;"><a href="<?php echo WWW; ?>company-news-add.html" class="add_news" title="Add News"><img alt="Add News" src="<?php echo WWW; ?>images/icons/add.png" border="0"> Add News</a></p>
			 <div class="clear"></div>
				
			<form method="post" action="" enctype="multipart/form-data">
				<fieldset>
					<table id="table-list" class="white-box">
					<tr>
						
						<th >Title</th>
						<th >News</th>
						<th >Date Added</th>
						<th >Action</th>
					</tr>
			        <? 	$rs_msg = mysqli_query($conn,"select * from company_news where user_id = '".$user_id."' order by id desc "); 
			        	if(mysqli_num_rows($rs_msg) == 0){
			        		?>
			        		<tr>
			        			<td colspan="3">No record added<td>
			        		</tr>
			        		<?
			        	}else{
				       		while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
				       		<tr>
				       			<td >
				       				
				       				<a class="no-bg no-padding" href="<?=WWW?>news-detail/<?=$row_msg['id']?>"><?=$row_msg['title']?></a>
				       			</td>
				       			
				       			<td><?php echo substr($row_msg['news'],0,50); ?>...</td>
				       			<td><?php echo date('d M, Y',strtotime($row_msg['date_added'])); ?></td>
								<td><a class="no-bg no-padding" href="<?=WWW?>company-news-edit.html?id=<?=$row_msg['id']?>"><img alt="Edit News" src="<?php echo WWW; ?>images/icons/edit.png" border="0"></a> 
								<a class="no-bg no-padding" href="<?=WWW?>company-news-detail/<?=$row_msg['id']?>" ><img alt="View News" src="<?php echo WWW; ?>images/icons/view.png" border="0"></a>
								<a class="no-bg no-padding" href="<?=WWW?>company-news-list.html?id=<?=$row_msg['id']?>&action=delete" onclick="return confirm('Are you sure to delete news');"><img alt="Delete News" src="<?php echo WWW; ?>images/icons/delete.png" border="0"></a>
								</td>
				       		</tr>
				       	<?  }
				       	} ?>
			        </table>
					<div class="clear"></div>
					
				</fieldset>
			</form>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>


<script src="<?php echo WWW?>js/jquery-1.8.0.js" type="text/javascript"></script>
<script>
$(document).ready(function(){
	
});
</script>
<style>
#table-list {width:100%;border:1px solid #ccc;}
#table-list tr{border:1px solid #ccc;}
#table-list th{padding:5px;background-color:#000;color:#FB7008;font-size:14px;}
#table-list td{padding:5px;text-align:center;}
#table-list a{color:#000;}
.add_news{font-size:14px;color:#000;}
</style>
<?php include('common/footer.php'); ?>