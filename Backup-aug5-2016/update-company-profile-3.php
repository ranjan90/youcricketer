<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	<div class="middle">
		<h1>Company Traits</h1>
		<? 	if(isset($_POST) && !empty($_POST)){ 
				$years_in_business 	= $_POST['years_in_business'];
				$legal_status 		= $_POST['legal_status'];
				$status_level 		= $_POST['status_level'];
				$gameType 			= implode(',', $_POST['gameType']);
				$matchType 			= implode(',', $_POST['matchType']);
				$types_of_services 	= $_POST['types_of_services'];
				$id 				= $_SESSION['ycdc_dbuid'];
				
				mysqli_query($conn,"update users set game_types = '$gameType', match_types = '$matchType' where id = '$id'");
				mysqli_query($conn,"update companies set years_in_business = '$years_in_business', legal_status = '$legal_status', status_level = '$status_level', service_offering = '$types_of_services' where user_id = '$id'");
				echo '<div id="success"><b>Success : </b>Information updated ... !</div>';
				?>
				<script>
				window.location = '<?=WWW?>update-company-profile-4.html';
				</script>
				<?

		}
		?>

		<div class="white-box content" id="dashboard">
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
				<? include('common/company-profile-navigation.php');?>
			<form method="post" action="" enctype="multipart/form-data">
				<fieldset>
					
					<div class="form-box">
						<label>Year(s) of <br>Formation</label>
						<select name="years_in_business" class="validate[required]">
							<option selected="selected" value="">Select One</option>
							<? for($x = 0; $x <= 100; $x++){ ?>
							<option value="<?=$x?>" <?=($x == $row_comp['years_in_business'])?'selected="selected"':'';?>><?=$x?></option>
							<? } ?>
						</select>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Legal <br>Status</label>
						<select name="legal_status" class="validate[required]">
							<option selected="selected" value="">Select One</option>
							<? foreach($legalStatus as $ls){ ?>
							<option value="<?=$ls?>" <?=($ls == $row_comp['legal_status'])?'selected="selected"':'';?>><?=$ls?></option>
							<? } ?>
						</select>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Status <br>Level</label>
						<select name="status_level" class="validate[required]">
							<option selected="selected" value="">Select One</option>
							<? foreach($statusLevel as $ls){ ?>
							<option value="<?=$ls?>" <?=($ls == $row_comp['status_level'])?'selected="selected"':'';?>><?=$ls?></option>
							<? } ?>
						</select>
					</div>
					<div class="clear"></div>
					<? if($row_comp['company_type_id'] == 1
					   || $row_comp['company_type_id'] == 2
					   || $row_comp['company_type_id'] == 4
					   || $row_comp['company_type_id'] == 35){ ?>
					<div class="form-box">
						<label>Preffered Game Type</label>
						<div style="float:left; margin-left:20px;">
							<? $prefferedGameTypeData = explode(',',$row_user['game_types']);?>
							<? foreach($prefferedGameType as $ma){ ?>
							<p><input style="float:left;" type="checkbox" <?=(in_array($ma, $prefferedGameTypeData))?'checked="checked"':'';?> name="gameType[]" value="<?=$ma?>">&nbsp; <?=$ma?></p>
							<? } ?>
						</div>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Preffered Match Type</label>
						<div class="form-box">
							<div style="float:left; margin-left:20px;">
							<? $matchArrayData = explode(',',$row_user['match_types']);?>
							<? foreach($matchArray as $ma){ ?>
							<p><input style="float:left;" type="checkbox" <?=(in_array($ma, $matchArrayData))?'checked="checked"':'';?> name="matchType[]" value="<?=$ma?>">&nbsp; <?=$ma?></p>
							<? } ?>
						</div>
					</div>
					<? } ?>
					<div class="clear"></div>
					<div class="form-box max-width-grid-list">
						<label>Type of services : </label>
						<textarea name="types_of_services" style="width:600px;" class="validate[required]" maxlength="1000" ><?=$row_comp['service_offering']?></textarea>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<a href="<?=WWW?>dashboard.html" class="submit-login left">Back</a>
					</div>
					<div class="form-box">
						<input type="submit" value=" Update " class="submit-login">
					</div>
				</fieldset>
			</form>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>

<?php include('common/footer.php'); ?>