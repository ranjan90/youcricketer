<?php include_once('includes/configuration.php');
$page = 'company-follow-search.html';
$selected_country = getGeoLocationCountry(); 

$countries = array();
$rs_countries = mysqli_query($conn,"select id,name from countries where status='1'");
while($row = mysqli_fetch_assoc($rs_countries)){
	$countries[] = $row;
}

$company_types = array();
$rs_clubs = mysqli_query($conn,"select id,name from company_types where status='1' order by name");
while($row = mysqli_fetch_assoc($rs_clubs)){
	$company_types[] = $row;
}

$ppage = intval($_GET["page"]);
if($ppage<=0) $ppage = 1;


$user_info  = array();
$companies_following = array();

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
	
		$sql = "select * from companies_following where user_id = $user_id ";
		$rs_company = mysqli_query($conn,$sql);
		if(mysqli_num_rows($rs_company)){
			while($row = mysqli_fetch_assoc($rs_company)){
				$companies_following[] = $row['company_id'];
			}
		}
}

if(isset($_POST['follow_company_submit']) && !empty($_POST['follow_company_submit'])){
	$companies = $_POST['company_ids'];
	if(!empty($companies)){
		for($i=0;$i<count($companies);$i++){
			$sql = "INSERT into companies_following SET user_id=".$user_id.", company_id=".$companies[$i];
			mysqli_query($conn,$sql);
		}
	}
	
	$_SESSION['follow_company_added'] = 1;
	
	if(isset($_GET['keywords']) && !empty($_GET['keywords'])){
		$url_q.='-'.$_GET['keywords'];
	}else{
		$url_q.='-all';
	}
	if(isset($_GET['company_type_id']) && !empty($_GET['company_type_id']) ){
		$url_q.='-'.$_GET['company_type_id'];
	}else{
		$url_q.='-0';
	}
	if(isset($_GET['country_id']) && !empty($_GET['country_id']) ){
		$url_q.='-'.$_GET['country_id'];
	}else{
		$url_q.='-0';
	}
	
	header("Location:".WWW."company-follow-search{$url_q}-".$ppage.'.html');
	exit;
	//echo '<script>window.location.href="'.WWW.'club-members-search-all-'.$ppage.'.html"</script>';
}

if(isset($_SESSION['follow_company_added']) && $_SESSION['follow_company_added']==1) {
	$company_added = 1;
	unset($_SESSION['follow_company_added']);
}



?>
<?php include('common/header.php'); ?>
<style>
.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
dl {min-height:145px;}
.content {width:1010px;}
.large-column {width:790px;}
</style>
	<div class="middle">
		<h1> Add Clubs/Leagues/Companies I Follow </h1>
		<?php if(isset($company_added) && $company_added == 1): ?>
			<div id="information">Club/Leagues/Companies followed Successfully... !</div>
		<?php  endif; ?>
		
		<?php if(empty($user_info)): ?>
			<div id="error">You are not logged... !</div>
		<?php endif; ?>
		
		<div class="white-box content" id="dashboard">
			<div class="small-column">
				<? include('common/user-left-panel.php'); ?>
			</div>
			<div class="large-column">
			<div id="pagination-top"></div>
			
			<div id="adv_search">
			<div class="clear"><br></div>
			<h2> Advanced Search </h2>
			
			<form id="advance_search_form" method="post" action=""> 
			
			<div id="club_name_div" style="float:left;margin-left:10px;">
				<input type="text" name="company_name_srch" class="search_box"  id="company_name_srch" <?php if(isset($_GET['keywords']) && !empty($_GET['keywords'])): ?> value="<?php echo str_replace('_',' ',$_GET['keywords']); ?>" <?php else: ?>  placeholder="League/Club/Company Name" <?php endif; ?> >
			</div>	
			
			<div id="country_dropdopwn" style="float:left;">
				<select name="company_type_id" id="company_type_id" style="font-size:14px;width:250px;">
					<option value="0">Company Type</option>
					<?php for($i=0;$i<count($company_types);$i++): ?>
						<?php if(isset($_GET['company_type_id']) && $_GET['company_type_id'] == $company_types[$i]['id']) $sel = 'selected';else $sel = ''; ?>
						<option <?php echo $sel; ?> value="<?php echo $company_types[$i]['id']; ?>"><?php echo $company_types[$i]['name']; ?></option>
					<?php endfor; ?>
				</select>	
			</div>	
			
			<div id="country_dropdopwn" style="float:left;">
				<select name="country_id_srch" id="country_id_srch" style="font-size:14px;width:250px;">
					<option value="0">Country</option>
					<?php for($i=0;$i<count($countries);$i++): ?>
						<?php if(isset($_GET['country_id']) && $_GET['country_id'] == $countries[$i]['id']) $sel = 'selected';else $sel = ''; ?>
						<option <?php echo $sel; ?> value="<?php echo $countries[$i]['id']; ?>"><?php echo $countries[$i]['name']; ?></option>
					<?php endfor; ?>
				</select>	
			</div>	
			
			
			<input type="hidden" name="company_type_id" id="company_type_id"  value="<?php if(isset($_GET['company_type_id']) && !empty($_GET['company_type_id'])) echo $_GET['company_type_id']; ?>" >
			<div style="clear:both"><br></div>
			
			<input type="submit" name="submit_adv_search" id="submit_adv_search" value="Search">
			
			</form>
			
			
			<div class="clear"><br></div>
			</div>
			
			<?php if(!empty($_GET['keywords']) || !empty($_GET['company_type_id']) || !empty($_GET['country_id'])){ ?>
			
			<form method="post">
			<div class="list">
                <ul id="individual" class="content1">
                	<?  
						
                		$rpp = PRODUCT_LIMIT_FRONT; // results per page
						$company_type_id = 0;
                		$country_id = 0;
						
      					$query = "select distinct u.*,c.company_name,c.years_in_business,c.id as company_id,ct.name as company_type,company_permalink from users u inner join companies as c on u.id=c.user_id inner join company_types as ct on ct.id=c.company_type_id ";
						$query_count = "select count(*) as users_count from users u inner join companies as c on u.id=c.user_id inner join company_types as ct on ct.id=c.company_type_id";
				      	//=======================================
						$where = " where u.status = 1 and c.status = 1 ";
						if(isset($_GET['company_type_id']) && !empty($_GET['company_type_id'])){
							$company_type_id = trim($_GET['company_type_id']);
							$where.=" AND c.company_type_id = ".$company_type_id;
						}
						
						if(isset($_GET['country_id']) && !empty($_GET['country_id'])){
							$country_id = trim($_GET['country_id']);
							$where.=" AND u.country_id = ".$country_id;
						}
						
					    if(isset($_GET['keywords']) && $_GET['keywords'] != 'Search Here' && $_GET['keywords'] != 'all' && !empty($_GET['keywords'])){
							$keywords = str_replace('_',' ',trim($_GET['keywords']));
							$where  .= " and (c.company_name like '%{$keywords}%' ) ";
						}else if(isset($_GET['club_name_srch']) && $_GET['club_name_srch'] != 'Club Name' && $_GET['club_name_srch'] != 'all' && !empty($_GET['club_name_srch']) ){
							$keywords = str_replace('_',' ',trim($_GET['keywords']));
							$where  .= " and (c.company_name like '%{$keywords}%' ) ";
						}else{
							$keywords = '';
						}
						
						$query.=$where;
						$query_count.=$where;
					    $query .= " order by u.id desc ";
				    
						$rs_count   = mysqli_query($conn,$query_count);
						$row_count  = mysqli_fetch_assoc($rs_count);
						$tcount = $row_count['users_count'];
					  
						$tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
						$count = 0;
						$start = ($ppage-1)* $rpp;
						$x = 0;
					  
						$query .= " LIMIT $start,$rpp ";
						//echo $query;
						$rs   = mysqli_query($conn,$query);
						if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
							echo '<div id="information">No record found ... !</div>';
						}
          			 
						$i= 0 ;
						while($row 	= mysqli_fetch_assoc($rs)){
							
							$row_img = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row['id']."' and is_default = '1' "));
			  				$row_country= get_record_on_id('countries', $row['country_id']);	
			  				$location 	= $row_country['name'];
			  				$row_vid = mysqli_fetch_assoc(mysqli_query($conn,"select * from videos where entity_id = '".$row['id']."' and entity_type = 'users' and is_default = '1' and file_name != ''"));
						?>
		        		    
						<dl>
								<dt>
									
									<a  href="<?=WWW?><?php echo $row['company_permalink']; ?>" title="<?=$row['f_name'].' '.$row['m_name'].' '.$row['last_name']?>"><img src="<?php echo WWW;?><?=($row_img && !empty($row_img['file_name']))?'users/'.$row['id'].'/photos/'.$row_img['file_name']:'images/no-photo.jpg'?>" width="120" height="128" /></a></dt>
								<dd><div class="details">
									<h3><?=truncate_string($row['company_name'],30)?></h3>
									<p><? echo $location;?><br />
									<?php echo $row['company_type']; ?><br/>
										
									
									<?php if(!empty($user_info)) { ?>
										<div style="float:left;">
										<?php if(!in_array($row['company_id'],$companies_following)){ ?>
											<input type="checkbox" name="company_ids[]" id="company_id_<?php echo $i; ?>" value="<?php echo $row['company_id']; ?>"> Follow
										<?php } else{ ?>
											Already Following
										<?php } ?>		 	
										</div>
									<?php } ?>		 
								
									</p>
									<a class="submit-login margin-top-5" href="<?=WWW?><?php echo $row['company_permalink']; ?>"  title="<?=$row['company_name'].' - '.$site_title?>">View Profile</a></div>
									<div class="flag"><img title="<?=$row_country['name']?>" alt="<?=$row_country['name']?>" src="<?php echo WWW;?>countries/<?=$row_country['flag']?>"  width="25"/></div>
									<div class="video">
										<? 
										$video = $row_vid['file_name'];
											if(!empty($video)){
											if(preg_match('/<iframe(.*)<\/iframe>/', $row_vid['file_name'])){ 
														
												preg_match('/src="(.*?)"/',$row_vid['file_name'] , $src);
												$src = $src[1];
												$video = "<iframe width='205' height='130' frameborder='0' allowfullscreen src='$src'></iframe>";
														
											}else{	
												$filename = explode('.',$video);
												$filename1= $filename[0];
												$video = '<video width="220" height="130" controls>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mp4" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.ogg" type=\'video/ogg; codecs="theora, vorbis"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mov" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.webm" type=\'video/webm; codecs="vp8, vorbis"\'>
												</video>';
											}
										}else{
											$video = '<img src="'.WWW.'images/no-video.jpg" width="168" height="130">';
										}
										echo $video; ?>									
									</div>
								</dd>
						</dl>
						<?php
                		
					      $i++;
					      $count++;
					      $x++;
					  } 
					  ?>
			</ul>
			</div><br/>
			<div style="float: left; clear: both; padding: 10px;">
			<input type="submit" name="follow_company_submit" id="follow_company_submit"  value="Follow Leagues/Clubs/Companies"> 
			</div>
			</form>
			
			<?php } ?>
			
			<p>&nbsp;</p><p>&nbsp;</p>
			<div id="pagination-bottom">
				<? if($tcount != 0 && $tcount > PRODUCT_LIMIT_FRONT){ ?>
		      	<?php
		        	$q_url = '';
					if(isset($keywords) && !empty($keywords)){
						$q_url.="-{$keywords}";
					}else{
						$q_url.="-all";
					}
					if(isset($company_type_id) ){
						$q_url.="-{$company_type_id}";
					}
					if(isset($country_id) ){
						$q_url.="-{$country_id}";
					}
					
					if(!empty($q_url)){
						$reload = "company-follow-search{$q_url}.html?";
					}else{
						$reload = "company-follow-search-all.html?";
					}
		        	echo paginate_one($reload, $ppage, $tpages);
		      	?>
		      	<input type="hidden" name="pagination-page" value="company-follow-search.html">
		        <? } ?>    
		        <form id="list-search" method="post" action="">
		        	<input type="text" name="txtsearch" placeholder="Search Here" class="validate[required] input-login">
		        	<input type="submit" value="">
		        </form>
				<div class="clear"></div>
		        <script type="text/javascript">
		        $('form#list-search').submit(function(e){
					var parentId = $(this).parent().attr('id');
					var string = $('#'+parentId +' form input[name=txtsearch]').val();
					if(string != '' && string != 'Search Here'){
						string = string.replace(/[ ]+/g,'_');
						string = string.replace(/[^a-zA-Z0-9_]+/g,'').toLowerCase();
						if(string.length > 0){
							$('form#list-search').attr('action','<?=WWW;?>company-follow-search-' + string + '.html');
						}
					}
				});
				
				 $('#submit_adv_search').click(function(e){
				
					var string = $('#company_name_srch').val();
					if(string == ''){
						string = 'all';
					}
					
					if($("#company_type_id").val()!=''){
						var company_type_id = $('#company_type_id').val();
					}else{
						var company_type_id = 0;
					}
					var country_id = $('#country_id_srch').val();
					//if(string != '' && string != 'Member Name'){
						string = string.replace(' ','_');
						string = string.replace(/[^a-zA-Z0-9_]+/g,'').toLowerCase();
						//if(string.length > 0){
							$('form#advance_search_form').attr('action','<?=WWW;?>company-follow-search-' + string +"-"+company_type_id+"-"+country_id+ '.html');
						//}
					//}
				});
		        </script>
				
				
		        <div class="clear"></div>
		    </div>  
		    <div class="clear"></div>
		</div>
		</div>
		
		<div class="clear"></div>
	</div>
	
<style type="text/css">
.header_search{background-color:none;padding:20 0 5 200px;width:200px;float:left;}
.searchres{margin-left:2px;width:200px;border: #f0f0f0 1px solid;border-radius:5px;position:absolute;z-index:100;background-color:#fff;}.img{float:left;margin: 5 5 5 5px;float:left;}
.search_box{width: 190px;height: 15px;border-radius: 5px;padding: 10px;font-family: verdana;color:black; font-size: 13px;border:1px solid #ccc;}
.name{margin-top: 14px;}
.user_div{clear:both;font-family: verdana;border-top: #f0f0f0 1px solid;color:black; font-size: 13px;height:20px;margin:1px;padding:7px;width:180px;border-radius:4px;vertical-align: top;  }
.user_div:hover{background:#F6F6F6;color:#fff;cursor:pointer}
.no_data{height: 40px;background-color: #F0F0F0;padding: 10 0 0 10px;width: 180px;border-radius: 5px;font-family: verdana;color:black; font-size: 15px;}
.name{float:left;margin:0 0 0 10px;}.cntry{margin:0 0 0 50px; font-size:13px}
</style>
<?php include('common/footer.php'); ?>