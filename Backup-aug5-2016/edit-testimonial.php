<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	<div class="middle">
		<h1>Update Testimonial </h1>
		<? 	if(isset($_POST) && !empty($_POST)){ 
				$entity_id 	= $_POST['entity_id'];
				$content 	= $_POST['content'];
				$rating 	= $_POST['rating'];
				$id 		= $_GET['id'];
				$user_id 	= $_SESSION['ycdc_dbuid'];
				
				if(!isset($user_id) || empty($user_id)){
					echo '<div id="error">Please login first ... !</div>';
				}else{
					if(!empty($content) && !empty($rating)){
						$query = "update testimonials set content = '$content', rating = '$rating' where id = '$id'";

						if(mysqli_query($conn,$query)){
							echo '<div id="success">Testimonial Information updated successfully ... !</div>';
				            ?>
				            <script>
				            window.location = '<?=WWW?>my-testimonials.html';
				            </script>
				            <?
						}else{
							echo '<div id="error">Information already exists ... !</div>';
						}
					}else{
						echo '<div id="error">Please fill all fields ... !</div>';
					}
				}
			}
			$id 	= $_GET['id'];
			$row 	= get_record_on_id('testimonials', $id);
		?>
		<div class="white-box content">
			<form method="post" enctype="multipart/form-data">
				<fieldset>
					<h2>Testimonial Information</h2>
					<div class="form-box">
						<label>Testimonial For</label>
						<div class="text">
						<?php 	if($row['entity_id'] == '0'){
									echo 'About Website';
								}else{
									echo get_combo('users','concat(f_name, last_name)',$row['entity_id'],'','text');
								}
						?>
						</div>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Rating</label>
						<select class="validate[required]" name="rating">
						<? for($x = 5; $x > 0; $x--){ ?>
						<option value="<?=$x?>" <?=($x == $row['rating'])?'selected="selected"':'';?>><?=$x?></option>
						<? }?>
						</select>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Content</label>
						<textarea maxlength="500" style="width:65%;float:right;" name="content" id="content" class="validate[required]"><?=$row['content']?></textarea>
						<div style="float:left;margin-left:130px;" >Maximum 160 characters &nbsp;<input type="text" name="text_counter" id="text_counter" style="width:50px;" value="0"></div>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<input type="submit" value=" Save " class="submit-login">
					</div>
					<div class="form-box">
						<a id="cancel" class="submit-login" style="background: none !important;position:relative; left:-150px; top:10px;" href="<?=WWW?>testimonials.html">Cancel</a>
					</div>
				</fieldset>
			</form>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>
	
	<script>
$(function(){
	
	
		$("#content").bind('keydown',function(){
			var length = $(this).val().length+1;
			if(length>160){ var val = $("#content").val().substr(0,159);$("#content").val(val); return; }
			$("#text_counter").val(length);
		});
	});
	</script>
<?php include('common/footer.php'); ?>