<?php include_once('includes/configuration.php');
$page = 'tournament-match-gallery.html';
$selected_country = getGeoLocationCountry(); 

$venues = array();
$error = '';

if(isset($_GET['page']) && !empty($_GET['page'])){
	$current_page = str_replace('page','',trim($_GET['page']));
}else{
	$current_page = 1;
}

$records_per_page = 10;
$start_record  = ($current_page-1)*$records_per_page;

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
}

$tournament_permission = 0;
$match_info = array();
$match_id = trim($_GET['m_id']);
$match_info = get_record_on_id('tournament_matches', $match_id);	
$tournament_id = $match_info['tournament_id'];

$tournament_info = get_record_on_id('tournaments', $tournament_id);	

$page_title = 'Gallery - '.ucwords($tournament_info['title']);

$team1_info = get_record_on_id('companies', $match_info['team1']);
$team2_info = get_record_on_id('companies', $match_info['team2']);

if(isset($user_id) && !empty($user_id)){
	$sql = "select * from tournament_permissions where tournament_id = $tournament_id and member_id = $user_id LIMIT 1";
	$rs_permission = mysqli_query($conn,$sql);
	if($tournament_info['user_id'] == $user_id || mysqli_num_rows($rs_permission)>0 ){
		$tournament_permission = 1;
	}else{
		$tournament_permission = 0;
	}
}else{
	$tournament_permission = 0;
}

if(empty($error) && isset($_GET['v_id']) && $_GET['action'] == 'delete'){
	$venue_id = trim($_GET['v_id']);
	if($tournament_permission){
		$sql = "DELETE FROM tournament_venues WHERE id = $venue_id ";
		mysqli_query($conn,$sql);
		$_SESSION['venue_deleted'] = 1;
	}
	header("Location:".WWW."tournament/venues/list/".$tournament_id);
	exit();
}

if(isset($_SESSION['venue_deleted']) && $_SESSION['venue_deleted']==1) {
	$venue_deleted = 1;
	unset($_SESSION['venue_deleted']);
}

?>
<?php include('common/header.php'); ?>
<style>
.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
dl {min-height:145px;}
.content {width:1010px;}
.large-column {width:790px;}
#table-list {width:100%;border:1px solid #ccc;}
#table-list tr{border:1px solid #ccc;}
#table-list th{padding:5px;background-color:#000;color:#FB7008;font-size:14px;}
#table-list td{padding:5px;text-align:center;}
#table-list a{color:#000;}
.add_tournament{font-size:14px;color:#000;}
#dashboard .large-column {width:75%;}
.match-gallery {float:left;padding:2px;margin:4px;min-height:150px;text-align:center;}
.match-gallery img{border:1px solid #cccccc;padding:2px;}
#gallery {float:left;padding:2px;}
.black-box h2{margin-top:2px;}
.h1_heading{color:#fb8300;font-size:26px;font-weight:bold;}
.h2_heading{color:#096da9;font-size:20px;font-weight:bold;}
.delete_img{color:#ff0000;font-size:12px;}
#success,#error {display:none;clear:both;}
</style>
	<div class="middle">
		<h1 class="h1_heading">Match Gallery - <?php echo $team1_info['company_name']; ?> vs <?php echo $team2_info['company_name']; ?> - <?php echo date('d M Y',strtotime($match_info['start_time'])); ?> </h1>
		<h2 class="h2_heading"><?php echo ucwords($tournament_info['title']); ?></h2>
		<div class="white-box content" id="dashboard">
			
		
			
			
			<?php if(!empty($error)): ?>
				<?php echo $error; ?>
			<?php endif; ?>
		
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
			<?php if($tournament_permission){ ?>
			
			<div class="clear">&nbsp;</div>
			<div id="success"></div>
			 <div class="clear">&nbsp;</div>
			<div id="error"></div>	
			<div class="clear"></div>
				
					<span class="btn btn-success fileinput-button">
			<i class="glyphicon glyphicon-plus"></i>
			<span>Add Pictures...</span>
			<!-- The file input field used as target for the file upload widget -->
			<input id="fileupload" type="file" name="files[]" multiple>
		</span>
		<br>
		<br>
		<!-- The global progress bar -->
		<div id="progress" class="progress">
			<div class="progress-bar progress-bar-success"></div>
		</div>
		<!-- The container for the uploaded files -->
		<div id="files" class="files"></div>
				
		<div class="clear"></div>	
			
		<div id="gallery"></div>
				
		<?php }else{ ?>
		<div id="error">You do not have Permission for this Tournament... !</div>
		<?php } ?>
				
				
		</div>  
		    <div class="clear"></div>
			</div>
		</div>
		<!--<div class="rightbar"><?php //include('common/right-panel.php');?></div>-->
		<div class="clear"></div>
	</div>
	
<link rel="stylesheet" href="<?php echo WWW; ?>js/colorbox/colorbox.css" />
<script src="<?php echo WWW; ?>js/colorbox/jquery.colorbox-min.js"></script>
		
<link rel="stylesheet" href="<?php echo WWW; ?>file_upload/css/jquery.fileupload.css">
<link rel="stylesheet" href="<?php echo WWW; ?>file_upload/css/bootstrap.min.css">
<script src="<?php echo WWW; ?>file_upload/js/vendor/jquery.ui.widget.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="<?php echo WWW; ?>file_upload/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="<?php echo WWW; ?>file_upload/js/canvas-to-blob.min.js"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
<script src="<?php echo WWW; ?>file_upload/js/bootstrap.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?php echo WWW; ?>file_upload/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?php echo WWW; ?>file_upload/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="<?php echo WWW; ?>file_upload/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="<?php echo WWW; ?>file_upload/js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="<?php echo WWW; ?>file_upload/js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="<?php echo WWW; ?>file_upload/js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="<?php echo WWW; ?>file_upload/js/jquery.fileupload-validate.js"></script>

<script>
/*jslint unparam: true, regexp: true */
/*global window, $ */
$(function () {
    //'use strict';
	loadGallery = function(){
		$.ajax({
			url: "<?php echo WWW; ?>add_match_gallery.php?t=<?php echo time(); ?>",
			type:"POST",
			data:"match_id=<?php echo $match_id; ?>&action=get",
			success: function(data) {
				data = $.parseJSON(data);
				if(data.error == ''){
					$("#gallery").html(data.str);
					$(".match-gallery-link").colorbox({rel:'match-gallery-link'});
				}
			}
		});
	}
	
    // Change this to the location of your server-side upload handler:
    var url =  '<?php echo WWW; ?>images/gallery/',
        uploadButton = $('<button/>')
            .addClass('btn btn-primary')
            .prop('disabled', true)
            .text('Processing...')
            .on('click', function () {
                var $this = $(this),
                    data = $this.data();
                $this
                    .off('click')
                    .text('Abort')
                    .on('click', function () {
                        $this.remove();
                        data.abort();
                    });
                data.submit().always(function () {
                    $this.remove();
                });
            });
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        autoUpload: false,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        maxFileSize: 999000,
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 100,
        previewMaxHeight: 100,
        previewCrop: true
    }).on('fileuploadadd', function (e, data) {
        data.context = $('<div/>').appendTo('#files');
        $.each(data.files, function (index, file) {
            var node = $('<p/>')
                    .append($('<span/>').text(file.name));
            if (!index) {
                node
                    .append('<br>')
                    .append(uploadButton.clone(true).data(data));
            }
            node.appendTo(data.context);
        });
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);
        if (file.preview) {
            node
                .prepend('<br>')
                .prepend(file.preview);
        }
        if (file.error) {
            node
                .append('<br>')
                .append($('<span class="text-danger"/>').text(file.error));
        }
        if (index + 1 === data.files.length) {
            data.context.find('button')
                .text('Upload')
                .prop('disabled', !!data.files.error);
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    }).on('fileuploaddone', function (e, data) {
        $.each(data.result.files, function (index, file) {
            if (file.url) {
                var link = $('<a>')
                    .attr('target', '_blank')
                    .prop('href', file.url);
                $(data.context.children()[index])
                    .wrap(link);
				
				$.ajax({
				url: "<?php echo WWW; ?>add_match_gallery.php?t=<?php echo time(); ?>",
				type:"POST",
				data:"match_id=<?php echo $match_id; ?>&action=add&img="+file.name,
				success: function(data) {
					data = $.parseJSON(data);
					if(data.error == ''){
						$("#success").html('Picture uploaded successfully').show();
						loadGallery();
					}else{
						$("#error").html(data.error).show();
					}
				}
				});
		
			} else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            }
        });
    }).on('fileuploadfail', function (e, data) {//alert(data.toSource());
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
            $(data.context.children()[index])
                .append('<br>')
                .append(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
		
		
	deleteGalleryImage = function(id){
		if(!confirm("Are you sure to delete Picture")){
			return;
		}
		$.ajax({
			url: "<?php echo WWW; ?>add_match_gallery.php?t=<?php echo time(); ?>",
			type:"POST",
			data:"match_id=<?php echo $match_id; ?>&action=delete&id="+id,
			success: function(data) {
				data = $.parseJSON(data);
				if(data.error == ''){
					$("#success").html('Picture deleted successfully').show();
					loadGallery();
				}else{
					$("#error").html(data.error).show();
				}
			}
		});
	}
	
	loadGallery();
	
});
</script>

<?php include('common/footer.php'); ?>