<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	<div class="middle">
		<h1>Profile Information</h1>

		<? 	$user_id = $row_user['id'];
			if(isset($_POST) && $_POST['action'] == 'add-domestic'){
				$city_id		= $_POST['city_id'];
				$state_id   	= $_POST['state_id'];
				$city_name		= $_POST['city_name'];
			if(!empty($_POST['from_date'])){
					$from_date 	 		= $_POST['from_date'];
				}
				else {
					$from_date = date("Y-m-d");
				}
				
				if(!empty($_POST['to_date'])){
					$to_date 	 		= $_POST['to_date'];
				}
				else {
					$to_date = date("Y-m-d");
				}
				$date_from1 = strtotime($from_date);
				$date_to    = strtotime($to_date);
				
				if($date_from1 > $date_to) {
				
					echo "<div id='error'><b>Error : </b>Please Select Correct Date ... From Date should not be greater than to date  ... !</div><br><br>"; 
				}
				
				else {
				$rs_chk = mysqli_query($conn,"select * from opportunities where user_id = '$user_id' and type = 'domestic' and city_name = '$city_name' and state_id='$state_id'");
				if(mysqli_num_rows($rs_chk) == 0){
					$query = "insert into opportunities (user_id, type, country_id, city_name, from_date, to_date,state_id) 
						values ('$user_id','domestic','".$row_user['country_id']."','$city_name','$from_date','$to_date','$state_id');";
					mysqli_query($conn,$query);
					echo '<div id="success"><b>Success : </b>Information updated ... !</div><br><br>';
					
				}
				
				}
			}
			if(isset($_POST) && $_POST['action'] == 'edit-domestic'){
				$state_id   	= $_POST['state_id'];
				$city_name      = $_POST['city_name'];
				$from_date		= $_POST['from_date'];
				$to_date		= $_POST['to_date'];			;
				$recordId 		= $_POST['recordId'];
				$date_from1 = strtotime($from_date);
				$date_to    = strtotime($to_date);
				
				if($date_from1 > $date_to) {
				
					echo "<div id='error'><b>Error : </b>Please Select Correct Date ... From Date should not be greater than to date  ... !</div><br><br>";
				}
				
				else {
				mysqli_query($conn,"update opportunities set city_name='$city_name',from_date = '$from_date', to_date = '$to_date',state_id='$state_id' where id = '$recordId'");
				echo '<div id="success"><b>Success : </b>Information updated ... !</div><br><br>';
				
			}
			}
			if(isset($_GET) && $_GET['action'] == 'delete' && $_GET['type'] == 'opportunities'){
				mysqli_query($conn,"delete from opportunities where id = '".$_GET['id']."'");
				echo '<div id="success"><b>Success : </b>Information updated ... !</div><br><br>';
				
			}
			if(isset($_POST) && $_POST['action'] == 'add-international'){
				
				$country_id		= $_POST['country_id'];
				$state_id		= $_POST['state']; //$_POST['state_id']
				$city_id		= $_POST['city_id'];
				$city_name		= $_POST['city_name'];
				if(!empty($_POST['from_date'])){
					$from_date 	 		= $_POST['from_date'];
				}
				else {
					$from_date = date("Y-m-d");
				}
				
				if(!empty($_POST['to_date'])){
					$to_date 	 		= $_POST['to_date'];
				}
				else {
					$to_date = date("Y-m-d");
				}

				$date_from1 = strtotime($from_date);
				$date_to    = strtotime($to_date);
				
				if($date_from1 > $date_to) {
				
					echo "<div id='error'><b>Error : </b>Please Select Correct Date ... From Date should not be greater than to date  ... !</div><br><br>";
				}
				
				else {
				//$rs_chk = mysqli_query($conn,"select * from opportunities where user_id = '$user_id' and type = 'international' and city_id = '$city_id'");
				
				$rs_chk = mysqli_query($conn,"select * from opportunities where user_id = '$user_id' and type = 'international' and city_name = '$city_name'");
				
				if(mysqli_num_rows($rs_chk) == 0){
					$query = "insert into opportunities (user_id, type, country_id, state_id, city_name, from_date, to_date) 
						values ('$user_id','international','$country_id','$state_id','$city_name','$from_date','$to_date');";
					mysqli_query($conn,$query);
					echo '<div id="success"><b>Success : </b>Information updated ... !</div><br><br>';
					
				}
				}
			}
			if(isset($_POST) && $_POST['action'] == 'edit-international'){
				print_r($_POST); 
				$country_id		= $_POST['country_id'];
				$state_id		= $_POST['state']; //$_POST['state_id']
				
				$city_name		= $_POST['city_name'];
				$from_date		= $_POST['from_date'];
				$to_date		= $_POST['to_date'];			;
				$recordId 		= $_POST['recordId'];
				
				$date_from1 = strtotime($from_date);
				$date_to    = strtotime($to_date);
				
				if($date_from1 > $date_to) {
				
					echo "<div id='error'><b>Error : </b>Please Select Correct Date ... From Date should not be greater than to date  ... !</div><br><br>";
				}
				
				else {
				mysqli_query($conn,"update opportunities set country_id='$country_id',state_id='$state_id',city_name='$city_name', from_date = '$from_date', to_date = '$to_date' where id = '$recordId'");
				echo '<div id="success"><b>Success : </b>Information updated ... !</div><br><br>';
				}	
			}
			?>

		<div class="white-box content" id="dashboard">
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
			<? include('common/profile-navigation.php');?>
			<form method="post" action="" enctype="multipart/form-data">
				<input type="hidden" name="action" value="submit">
				<fieldset>
					<legend>Seeking Opportunities</legend>
					<div class="form-box" style="margin-left: -12px; ">
						<label>Actively Seeking Domestic Opportunity</label>
					</div>
					<? $rs_msg = mysqli_query($conn,"select * from opportunities where user_id = '".$row_user['id']."' and type='domestic' order by to_date desc ");  ?>
					<div class="form-box">
						<input style="float:left;" type="checkbox" name="domestic" <?=(mysqli_num_rows($rs_msg) > 0)?'checked="checked" disabled="disabled"':'';?>>
					</div>
					<div class="clear"></div>
					<div id="domestic-detail" class="<?=(mysqli_num_rows($rs_msg) > 0)?'':'hide';?>">
						<div class="form-box" style="width:90%;">
							<div>
								Domestic Availibility
								<a style="color:#fff;" class="no-bg right popup-link" href="#add-domestic"><img src="<?=WWW?>images/icons/add.png"> Add</a>
							</div>
						</div>
						<div class="form-box" style="width:90%;">
							<div class="space10"></div>
					        <table width="100%" border="1" style="border-collapse:collapse;" id="inbox">
					        	<tr>
					        		<th></th>
					        		<th class="tleft">State</th>
					        		<th class="tleft">City</th>
					        		<th class="tleft">From Date</th>
					        		<th class="tleft">To Date</th>
					        	</tr>
					        <? 	if(mysqli_num_rows($rs_msg) > 0){
					        		while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
						       		<tr title="Click for detail">
						       			<td width="20%">
						       				<a class="no-bg no-padding popup-link" href="#edit-domestic" id="<?=$row_msg['id']?>"><img src="<?=WWW?>images/icons/edit.png"></a>
						       				<a class="no-bg no-padding" href="<?=WWW?>profile-information-step-7.html?action=delete&type=opportunities&id=<?=$row_msg['id']?>"><img src="<?=WWW?>images/icons/delete.png"></a>
						       			</td>
						     			<td>
						       				<?=get_combo('states','name',$row_msg['state_id'],'','text')?>
						       			
						       			</td>
						       			<td><?php // $city = mysqli_fetch_assoc(mysqli_query($conn,"SELECT city_name FROM opportunities where user_id = '$user_id' and type = 'domestic'"));
						       				echo $row_msg['city_name'];
						       			?>
						       			</td>
						       				<td><?=date_converter($row_msg['from_date'])?></td>
						       			<td><?=date_converter($row_msg['to_date'])?></td>
						       		</tr>
						       	<?  }
						       	} ?>
					        </table>
						</div>
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
				</fieldset>
				<fieldset>
					<? $rs_msg = mysqli_query($conn,"select * from opportunities where user_id = '".$row_user['id']."' and type = 'international' order by to_date desc");  ?>
					<div class="form-box " style="width:100%;">
					</div>
					<div class="form-box" style="margin-left: -12px; ">
						<label>Actively Seeking International Opportunity</label>
					</div>
					<div class="form-box">
						<input style="float:left;" type="checkbox" name="international" <?=(mysqli_num_rows($rs_msg) > 0)?'checked="checked" disabled="disabled"':'';?>>
					</div>
					<div class="clear"></div>
					<div id="international-detail" class="<?=(mysqli_num_rows($rs_msg) > 0)?'':'hide';?>">
						<div class="form-box" style="width:90%;">
							<div>
								International Availibility
								<a style="color:#fff;" class="no-bg right popup-link" href="#add-international"><img src="<?=WWW?>images/icons/add.png"> Add</a>
							</div>
						</div>
						<div class="form-box" style="width:90%;">
							<div class="space10"></div>
					        <table width="100%" border="1" style="border-collapse:collapse;" id="inbox">
					        	<tr>
					        		<th></th>
					        		<th class="tleft">Country</th>
					        		<th class="tleft">State</th>
					        		<th class="tleft">City</th>
					        		<th class="tleft">From Date</th>
					        		<th class="tleft">To Date</th>
					        	</tr>
					        <? 	if(mysqli_num_rows($rs_msg) > 0){
					        		while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
						       		<tr title="Click for detail">
						       			<td width="10%">
						       				<a class="no-bg no-padding popup-link" href="#edit-international" id="<?=$row_msg['id']?>"><img src="<?=WWW?>images/icons/edit.png"></a>
						       				<a class="no-bg no-padding " href="<?=WWW?>profile-information-step-7.html?action=delete&type=opportunities&id=<?=$row_msg['id']?>"><img src="<?=WWW?>images/icons/delete.png"></a>
						       			</td>
						       			<td><?=get_combo('countries','name',$row_msg['country_id'],'','text')?></td>
						       			<td><?=get_combo('states','name',$row_msg['state_id'],'','text')?></td>
						       			<td><?php // $city = mysqli_fetch_assoc(mysqli_query($conn,"SELECT city_name FROM opportunities where user_id = '$user_id' and type = 'international'"));
						       				echo $row_msg['city_name'];
						       			?></td>
						       			<td><?=date_converter($row_msg['from_date'])?></td>
						       			<td><?=date_converter($row_msg['to_date'])?></td>
						       		</tr>
						       	<?  }
						       	} ?>
					        </table>
						</div>
						<div class="clear"></div>
						
					</div>
					<div class="clear"></div>
					</fieldset>
					<table width="100%" class="profile-action">
						<tr>
							<td width="33%">
								<a id="cancel" class="submit-login" style="position:relative; left:-150px; top:10px;" href="<?=WWW?>dashboard.html">Cancel</a>
							</td>
							<td width="33%">
								
							</td>
							<td width="33%">
								<a class="submit-login" style="position:relative;left:0px;  top:10px;" href="<?=WWW?>profile-information-step-8.html">Skip</a>
							</td>
						</tr>
					</table>
				
			</form>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<style>
#city select{float:left; margin-left:20px;}
</style>
<script src="<?php echo WWW?>js/jquery-1.8.0.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

	$('#dashboard .black-box h2').click(function(){
		$('#dashboard .black-box ul').css('display','none');
		var id = this.id;
		$('#'+id+'-box ul').css('display','block');
	});
	$('input[name=domestic]').click(function(){

		if($(this).is(':checked') == true){
			$('#domestic-detail').removeClass('hide');
		}else{
			$('#domestic-detail').addClass('hide');
		}
	});
	$('input[name=international]').click(function(){
		if($(this).is(':checked') == true){
			$('#international-detail').removeClass('hide');
		}else{
			$('#international-detail').addClass('hide');
		}
	});
	$('a.popup-link').click(function(){
		var link = $(this).attr('href');
		if(link == '#edit-domestic'){
			var id = this.id;

			$.ajax({type	: 'POST', 
				url		: WWW + 'includes/get_domestic_state.php', 
				data	: ({id:id}),
				success	: function(msg){
					
					$("#user_state_domestic").html(msg);
					
					$(link+' #recordId').val(id);
				}
			});
			
			$.ajax({type	: 'POST', 
				url		: WWW + 'includes/get-record.php', 
				data	: ({table:'opportunities',field:'*',id:id}),
				success	: function(msg){
					msg1 = msg.split('___');
					$(link+' input[name=city_name]').val(msg1[0]);
					$(link+' input[name=from_date]').val(msg1[1]);
					$(link+' input[name=to_date]').val(msg1[2]);
					$(link+' #recordId').val(id);
				}
			});
		}
		if(link == '#edit-international'){
			var id = this.id;
			$('#country').html();
			$('#state_edit').html();
			$.ajax({type	: 'POST', 
				url		: WWW + 'includes/get-country.php', 
				data	: ({id:id}),
				success	: function(msg){
					$('#country').html(msg);
	//	alert(msg);
					
				}
			});
			$.ajax({type	: 'POST', 
				url		: WWW + 'includes/get-state.php', 
				data	: ({id:id}),
				success	: function(msg){
					$('#state_edit').html(msg);
				
				}
			});

			
			$.ajax({type	: 'POST', 
				url		: WWW + 'includes/get-record.php', 
				data	: ({table:'opportunities',field:'*',id:id}),
				success	: function(msg){
					msg1 = msg.split('___');
					//$(link+' label#name').html(msg1[0]);
				//	alert(msg1[0]);
					
					$(link+' input[name=city_name]').val(msg1[0]);
					$(link+' input[name=from_date]').val(msg1[1]);
					$(link+' input[name=to_date]').val(msg1[2]);
					$(link+' #recordId').val(id);
				}
			});

			
		}
		$(link).removeClass('hide');
	});
	$('.close-popup').click(function(){
		$('.popup').addClass('hide');
	});
	$('select[name=country_id]').live('change',function(){
		var id = $('select[name=country_id]').val();
		$.ajax({type	: 'POST', 
				url		: WWW + 'includes/get-states.php', 
				data	: ({id:id}),
				success	: function(msg){
					$("#state").html(msg);
				}
		});
	});
	$('select[name=state_id]').live('change',function(){
		var id = $('select[name=state_id]').val();
		$.ajax({type	: 'POST', 
				url		: WWW + 'includes/get-cities.php', 
				data	: ({id:id}),
				success	: function(msg){
					$("#city").html(msg);
				}
		});
	});
});
</script>
<div id="add-domestic" class="popup hide" style="margin-top: -500px">
	<h1>
		Add Domestic Opportunity
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h1>
	<form method="post" action="" >
		<input type="hidden" name="action" value="add-domestic">
		<fieldset>
			<div class="form-box">
			<div id="city" >
					
					
					<b> <span > <? //get_combo('countries','name',$row_user['country_id'],'','text') ?> </span></b> 
				</div>
				<br><br>
				<div id="states_domestic">
					<select style="margin-left:10px; float:left; " name="state_id" class="validate[required]]">
							<option selected="selected" value="">-- Select --</option>
							<? 	$rs_states = mysqli_query($conn,"select * from states where country_id = '".$row_user['country_id']."'"); 
								while($row_state_l = mysqli_fetch_assoc($rs_states)){ ?>
								<option  value="<?=$row_state_l['id']?>"><?=$row_state_l['name']?></option>
							<?  } ?>
						</select>
				</div>
				<br><br>
				<div id="city">
					<input type="text" name="city_name" class="input-login validate[required]"required="required" placeholder="Add city Name">
				</div>
				<br><br>
				<input type="text" name="from_date" id="from_date_add"class="input-login validate[required]"required="required" placeholder="From Date">
				<br><br>
				<input type="text" name="to_date"  id="to_date_add"class="input-login validate[required]" required="required"placeholder="To Date">
				<br>
				<br>
				<input type="submit" class="submit-login margin-top-5" value="Save">
			</div>
		</fieldset>
	</form>
</div>
<div id="edit-domestic" class="popup hide" style="margin-top: -500px">
	<h2>
		Update Domestic Opportunity
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h2>
	<form method="post" action="">
		<input type="hidden" name="action" value="edit-domestic">
		<input type="hidden" name="recordId" id="recordId" value="">
		<fieldset>
			<div class="form-box">
				<label id="name"></label>
				<div id="city">
					<b> <? // get_combo('countries','name',$row_user['country_id'],'','text')?> </b>
				</div>
				<br><br>
				<div id="user_state_domestic">
					<img src='images/ajax-loader.gif' />
				</div>
				
				<br><br>
				<input type="text" name="city_name"   class="input-login validate[required]" required="required" value="">
				<br><br>
				<input type="text" name="from_date"  id="from_date_edit" class="input-login validate[required]" required="required"placeholder="From Date">
				<br><br>
				<input type="text" name="to_date"  id="to_date_edit"class="input-login validate[required]" required="required"placeholder="To Date">
				<br>
				<br>
				<br><br>
				<input type="submit" class="submit-login margin-top-5" value="Save">
			</div>
		</fieldset>
	</form>
</div>
<div id="add-international" class="popup hide" style="margin-top: -500px">
	<h2>
		Add International Opportunity
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h2>
	<form method="post" action="">
		<input type="hidden" name="action" value="add-international">
		<fieldset>
			<div class="form-box">
			<?php $sql = mysqli_query($conn,"SELECT * FROM countries");
			  		?>
			  		<select name='country_id' id='c_id' onchange="states();" class="validate[required]" required="required"> 
			  		<option>--Any--</option>
			  		<?php while($row = mysqli_fetch_assoc($sql)){?>
			  		<option value="<?php echo $row['id'];?>"> <?php echo $row['name'];?></option>
			  		<?php }?>
			  		</select>
			  		
				<br><br>
				<div id="state">
					<div id="state_data">
			  		  State values appear here
			  		</div>
				</div>
				<br><br>
					<div id="city">
					<input type="text" name="city_name" class="input-login validate[required]" required="required"placeholder="Add city Name">
				</div>
				<br><br>
				<input type="text" name="from_date" id="from_date_internation_add"required="required" class="input-login validate[required]" placeholder="From Date ">
				<br><br>
				<input type="text" name="to_date" id="to_date_internation_add"  required="required"class="input-login validate[required]" placeholder="To Date">
				<br>
				<br>
				<input type="submit" class="submit-login margin-top-5" value="Save">
			</div>
		</fieldset>
	</form>
</div>
<div id="edit-international" class="popup hide" style="margin-top: -500px">
	<h2>
		Update International Opportunity
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h2>
	<form method="post" action="">
		<input type="hidden" name="action" value="edit-international">
		<input type="hidden" name="recordId" id="recordId" value="">
		<fieldset>
			<div class="form-box">
				<label id="name"></label>
				<br><br>
			<div id="country"> <img src='images/ajax-loader.gif' /></div>			  		
				<br><br>
				<div id="state">
					<div id="states_data">
			  		  <div id='state_edit'> <img src='images/ajax-loader.gif' /> </div>
			  		</div>
				</div>
				<br><br>
					<div id="city">
					<input type="text" name="city_name" class="input-login validate[required]" required="required" placeholder="Add city Name">
				</div>
				<br><br>
				<input type="text" name="from_date"id="from_date_internation_edit" required="required" class="input-login validate[required]" placeholder="From Date">
				<br><br>
				<input type="text" name="to_date" id="to_date_internation_edit"  required="required"class="input-login validate[required]" placeholder="To Date">
				<br>
				<br>
				<br><br>
				<input type="submit" class="submit-login margin-top-5" value="Save">
			</div>
		</fieldset>
	</form>
</div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.9.1.js"></script>
  <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

 <script>
  $(function() {
    $( "#from_date_add" ).datepicker({ dateFormat: "yy-mm-dd"});
  });

  $(function() {
	    $( "#to_date_add" ).datepicker({ dateFormat: "yy-mm-dd"});
	  });
  
  $(function() {
	    $( "#from_date_edit" ).datepicker({ dateFormat: "yy-mm-dd"});
	  });

  $(function() {
	    $( "#to_date_edit" ).datepicker({ dateFormat: "yy-mm-dd"});
	  });
  $(function() {
	    $( "#from_date_internation_add" ).datepicker({ dateFormat: "yy-mm-dd"});
	  });
  $(function() { 
	    $( "#to_date_internation_add" ).datepicker({ dateFormat: "yy-mm-dd"});
	  });
  $(function() {
	    $( "#from_date_internation_edit" ).datepicker({ dateFormat: "yy-mm-dd"});
	  });
  $(function() {
	    $( "#to_date_internation_edit" ).datepicker({ dateFormat: "yy-mm-dd"});
	  });
  </script>
  
  <script type="text/javascript">
	function states(){
		var xml;
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xml=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xml=new ActiveXObject("Microsoft.XMLHTTP");
		  }

		 var c_id 		= document.getElementById("c_id").value;
		 var id = "id="+c_id;
		 
		 var url    		= 'states.php';
		    xml.open("POST",url,true);
		    xml.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		    xml.onreadystatechange = function(){
		  
		    	if(xml.readyState == 4 && xml.status == 200) {
		        
		  		    var return_data = xml.responseText;
					document.getElementById("state_data").innerHTML = return_data;
					 //document.getElementById("status").innerHTML = "";
					
			    

		        } 
		    }
		    xml.send(id);
			   document.getElementById("state_data").innerHTML = "Loading <img src='images/ajax-loader.gif' />"; 
		  
		  
		}

	function get_states(){
		var xml;
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xml=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xml=new ActiveXObject("Microsoft.XMLHTTP");
		  }

		 var c_id 		= document.getElementById("get_country").value;
		 var id = "id="+c_id;
		 
		 var url    		= 'states.php';
		    xml.open("POST",url,true);
		    xml.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		    xml.onreadystatechange = function(){
		  
		    	if(xml.readyState == 4 && xml.status == 200) {
		        
		  		    var return_data = xml.responseText;
					document.getElementById("states_data").innerHTML = return_data;
					 //document.getElementById("status").innerHTML = "";
					
			    

		        } 
		    }
		    xml.send(id);
			   document.getElementById("states_data").innerHTML = "Loading <img src='images/ajax-loader.gif' />"; 
		  
		  
		}
	</script>
<?php include('common/footer.php'); ?>