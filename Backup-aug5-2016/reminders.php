<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	<div class="middle">
		<h1>Reminders to already sent invitations</h1>
		<div class="white-box content" id="dashboard">
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
				<h2>Remind Now</h2>
				<div id="pagination-top"></div>
				<div class="list">
					<?php
					if(isset($_POST) && count($_POST['invitations']) > 0){ 
						$from_name	= $_SESSION['ycdc_user_name'];
						foreach($_POST['invitations'] as $invites){
							$rowInvite 	= get_record_on_id('invitations', $invites);
							$email 		= $rowInvite['to_emails'];

							if(!empty($email)){
								$email_template = get_record_on_id('cms', 12);							
								$mail_title		= $email_template['title'].' from '.$from_name;
					            $mail_content	= $email_template['content'];
					            $mail_content 	= str_replace('{to_name}',$name, $mail_content);	
					            $mail_content 	= str_replace('{from_name}',$from_name, $mail_content);	
					            $mail_content 	= str_replace('{site_link}','<a href="'.WWW.'" title="Visit The Site">YouCricketer</a>', $mail_content);
					            $mail_content 	= str_replace('{register_link}','<a href="'.WWW.'sign-up.html?ref='.$invites.'" title="Sign Up">HERE</a>', $mail_content);
					            $mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);
								$headers 		= "Content-type:text/html;charset=UTF-8" . "\r\n";
								$headers 	   .= "From: no-reply@youcricketer.com \r\n";
					            mail($email,$mail_title,$mail_content,$headers);
							}
						}
						echo '<div id="success">Invitation Sent Successfuly... !</div>';
					}
					?>
					<form method="post" id="multiple">
						<input type="hidden" name="action" value="resend">
	                <ul>
	                	<?  $rpp = PRODUCT_LIMIT_FRONT; // results per page
	            			$ppage = intval($_GET["page"]);
	      					if($ppage<=0) $ppage = 1;
	      					$query 	= "select i.id, i.to_emails from invitations i where i.from_user_id ='".$_SESSION['ycdc_dbuid']."' and i.to_emails not in (select email from users) order by i.id";
	      					//echo $query;
					      //=======================================
					      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
					        echo '<div id="error">Your search returned no results</div>';
					      }
					      $rs   = mysqli_query($conn,$query);
					      $tcount = mysqli_num_rows($rs);
					      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
					      $count = 0;
					      $i = ($ppage-1)* $rpp;
	          			  $x = 0;
	          			  //=======================================
	      				  while(($count<$rpp) && ($i<$tcount)){
	                		mysqli_data_seek($rs,$i);
	                		$row_g 	= mysqli_fetch_assoc($rs);
	      					?>
	      					<li><input type="checkbox" name="invitations[]" value="<?php echo $row_g['id']?>"> <?php echo $row_g['to_emails']?></li>
						    <?
						      $i++;
						      $count++;
						      $x++;
						  } 
						  ?>
					</ul>
					<div class="clear"></div>
					<input type="submit" value="Resend" class="submit-signup fr" style="height:35px;">
					<div class="clear"></div>
					</form>
				</div>
				<div id="pagination-bottom">
					<? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_FRONT){ ?>
			      	<?php
			        	$reload = 'clubs.html?';
			        	echo paginate_one($reload, $ppage, $tpages);
			      	?>
			      	<input type="hidden" name="pagination-page" value="clubs.html">
			        <? } ?>    
			    </div>  
			    <div class="clear"></div>
			    <a class="checked" id="checkAll">Check All</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        		<a class="checked" id="uncheckAll">Uncheck All</a>
			</div>	
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<style>
.list tr{line-height: 24px;}
a.checked{cursor: pointer;}
</style>
<script type="text/javascript" src="<?=WWW?>js/jquery-1.8.0.js"></script>
<script>
$(document).ready(function(){
	$('#dashboard .black-box h2').click(function(){
		$('#dashboard .black-box ul').css('display','none');
		var id = this.id;
		$('#'+id+'-box ul').css('display','block');
	});
	$('a#checkAll').click(function(){
		$('form#multiple input[type=checkbox]').attr('checked', 'checked');
	});
	$('a#uncheckAll').click(function(){
		$('form#multiple input[type=checkbox]').removeAttr('checked', 'checked');
	});
});
</script>
<?php include('common/footer.php'); ?>