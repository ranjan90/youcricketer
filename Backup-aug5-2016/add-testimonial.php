<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	<div class="middle">
		<h1>Add Testimonial </h1>
		<? 	$user_id = $_SESSION['ycdc_dbuid'];
			
				if(isset($_POST) && !empty($_POST)){ 
				$content 	= $_POST['content'];
				$date 		= date('Y-m-d H:i:s');
				$user_id 	= $_SESSION['ycdc_dbuid'];
				$rating 	= $_POST['rating'];
    
				if(!isset($user_id) || empty($user_id)){
					echo '<div id="error">Please login first ... !</div>';
				}else{
					if(!empty($content) && !empty($rating)){
						$toEmails 	= explode('),', $_POST['to']);
						$counter 	= 0;
						foreach($toEmails as $te){
							$complete = explode('(', $te);
							$info 	  = explode(',', $complete[1]);
							$role  	  = $info[0];
							$country  = str_replace(')','',$info[1]);
							$rowRole = mysqli_fetch_assoc(mysqli_query($conn,"select * from user_types where name = '".trim($role)."'"));
							$rowcountry = mysqli_fetch_assoc(mysqli_query($conn,"select * from countries where name = '".trim($country)."'"));
							$name = explode(' ', $complete[0]);
							$rowUser= mysqli_fetch_assoc(mysqli_query($conn,"select * from users where trim(f_name) = '".$name[0]."' and trim(last_name) = '".$name[1]."' 
																	and user_type_id = '".$rowRole['id']."' and country_id = '".$rowcountry['id']."'"));
							/********************************/
							$to_user_id = $rowUser['id'];
							
							if(!empty($to_user_id)){
								$rsCheck = mysqli_query($conn,"select * from testimonials where user_id = '$user_id' 
									and entity_id = '$to_user_id' and content = '$content'");
								if(mysqli_num_rows($rsCheck) == 0){
									$query = "insert into testimonials (content, user_id, rating, status, entity_id, testimonial_date) 
											values ('$content','$user_id','$rating','1','$to_user_id','$date')";
									if(mysqli_query($conn,$query)){
										$counter++;
									}
								}
							}
						}
						if($counter > 0){
							echo '<div id="success">'.$counter.' Testimonial Information added successfully ..... !</div>'; ?>
							 <script>
			                window.location = '<?=WWW?>my-testimonials.html';
			                </script>
						<?
						}
					}else{
						echo '<div id="error">Please fill all fields ... !</div>';
					}
				}
				
			}
		?>
		<div class="white-box content" style="position:relative;">
			<form method="post" enctype="multipart/form-data" id="testimonial_form" name="testimonial_form">
				<fieldset>
					<h2>Testimonial Information</h2>
					<div class="form-box">
						<label>Testi For</label>
						<textarea class="validate[required]" id="to" name="to" style="height:30px; width:600px; margin-left:125px; margin-top:-20px;"></textarea>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Rating</label>
						<select class="validate[required]" name="rating">
						<? for($x = 5; $x > 0; $x--){ ?>
						<option value="<?=$x?>" <?=($x == 10)?'selected="selected"':'';?>><?=$x?></option>
						<? }?>
						</select>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Content</label>
						<textarea maxlength="500" name="content" id="content" style="width:600px; margin-left:125px; height:100px;" class="validate[required]"></textarea>
						<div style="float:left;margin-left:130px;" >Maximum 160 characters &nbsp;<input type="text" name="text_counter" id="text_counter" style="width:50px;" value="0"></div>
					</div>
					<div class="clear"></div>
					
					<div class="form-box">
						<input type="submit" value=" Save " class="submit-login">
					</div>
					<div class="form-box">
						<a id="cancel" class="submit-login" style="background: none !important;position:relative; left:-150px; top:10px;" href="<?=WWW?>testimonials.html">Cancel</a>
					</div>
				</fieldset>
			</form>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>
<script src="<?php echo WWW?>js/jquery-1.8.0.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=WWW?>js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?=WWW?>js/select2.js"></script>
<link href="<?=WWW?>css/jquery-ui.css" type="text/css" rel="stylesheet">
<link href="<?=WWW?>css/select2.css" type="text/css" rel="stylesheet">


<script>
$(function(){
	function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }
	
	$("#content").bind('keydown',function(){
		var length = $(this).val().length+1;
		if(length>160){ var val = $("#content").val().substr(0,159);$("#content").val(val); return; }
		$("#text_counter").val(length);
	});
 	/*
    $( "#to" )
      .bind( "keydown", function( event ) {

      	if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).data( "ui-autocomplete" ).menu.active ) {
          event.preventDefault();
        }
      })
      .autocomplete({
      	minLength: 0,
         source: function (request, response) {
         	$.ajax({
                url: WWW + 'includes/get-users.php',
                data: {
                    //filter: request.term,
                    pagesize: 10
                },
                dataType: "json",
                success: function(data) {
                	response($.map(data.users, function(el, index) {
                    	return {
                            value: el.display_name,
                            avatar: WWW + "users/" + el.user_id + '/' + el.email_hash
                        };
                    }));
                }
            });
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = split( this.value );
          terms.pop();
          terms.push( ui.item.value );
          terms.push( "" );
          this.value = terms.join( ", " );
          return false;
        }
      }).data("autocomplete")._renderItem = function (ul, item) {
        return $("<li />")
            .data("item.autocomplete", item)
            .append("<a><img class='autocomplete-photo' src='" + item.avatar + "' />" + item.value + "</a>")
            .appendTo(ul);
    	};
    	*/
    /*****************************************************/
    $.ajax({type	: 'POST', 
		   	url		: WWW + 'includes/get-users.php', 
			dataType: 'json',
			success	: function(msg){
				$("#to").select2({tags:msg});
			}
    });
	/*****************************************************/
	$('#dashboard .black-box h2').click(function(){
		$('#dashboard .black-box ul').css('display','none');
		var id = this.id;
		$('#'+id+'-box ul').css('display','block');
	});
	//$('#testimonial_form').validationEngine();
});
</script>
<style>
.ui-autocomplete{margin: 460px 467px; min-height:200px;width:600px;}
img.autocomplete-photo{width:25px; height:25px; padding-right:5px;}
.form-box a.select2-search-choice-close{padding:0px; background:url("../images/select2.png") no-repeat scroll right top rgba(0, 0, 0, 0);}
</style>
<?php include('common/footer.php'); ?>