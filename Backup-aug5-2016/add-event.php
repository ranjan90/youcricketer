<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	<div class="middle">
		<h1>Add Event </h1>
		<? 	if(isset($_POST) && !empty($_POST)){ 
				$title 	 = $_POST['title'];
				$content = $_POST['content'];
				$state_id =$_POST['state_id'];
				$city_name= $_POST['city_name'];
				$isGlobal = ($_POST['is_global'] == 'on')?'1':'0';
				$from_date = $_POST['start_date'];
				$to_date   = $_POST['end_date'];
				
				$date_from1 = strtotime($from_date);
				$date_to    = strtotime($to_date);
				
				if($date_from1 > $date_to) {
				
					echo "<div id='error'><b>Error : </b>Please Select Correct Date ... Start Date should be less than End date  ... !</div><br><br>"; 
				}
				
				else {
				
				$start_date = date_converter($_POST['start_date'],'Y-m-d');
				$end_date 	= date_converter($_POST['end_date'],'Y-m-d');
				$date 		= date('Y-m-d H:i:s');
				$user_id 	= $_SESSION['ycdc_dbuid'];
				
				if(!isset($user_id) || empty($user_id)){
					echo '<div id="error">Please login first ... !</div>';
				}else{
					if(!empty($title) && !empty($content)){
						$rs_check = mysqli_query($conn,"select * from events where user_id = '$user_id' and start_date = '$start_date' and end_date = '$end_date' and title = '$title'");
						if(mysqli_num_rows($rs_check) == 0){
							$query = "insert into events (title, content, user_id, start_date, end_date, status, create_date, city_name, state_id, is_global) 
							values ('$title','$content','$user_id','$start_date','$end_date','1','$date','$city_name','$state_id','$isGlobal')";

							if(mysqli_query($conn,$query)){
								$forum_id = mysqli_insert_id();
								
								if(!is_dir('events/'.$forum_id)){
									mkdir('events/'.$forum_id,0777);
								}
								chmod('events/'.$forum_id,0777);

								if(!empty($_FILES['photo']['name'])){
									$filename 	= friendlyURL($title).'.jpg';
									$image 		= new SimpleImage();
									$image->load($_FILES["photo"]["tmp_name"]);
									$image->save('events/'.$forum_id.'/'.$filename);
									chmod('events/'.$forum_id.'/'.$filename,0777);
								
									mysqli_query($conn,"insert into photos (file_name, title, entity_id, entity_type) values ('$filename','$title','$forum_id','events')");
								}
							}

			                echo '<div id="success">Forum Information added successfully ... !</div>';
			                ?>
			                <script>
			                window.location = '<?=WWW?>my-events.html';
			                </script>
			                <?
						}else{
							echo '<div id="error">Information already exists ... !</div>';
						}
					}else{
						echo '<div id="error">Please fill all fields ... !</div>';
					}
				}
				
			}
		}
		?>
		<div class="white-box content">
			<form method="post" enctype="multipart/form-data" id="event_form">
				<fieldset>
					<h2>Event Information</h2>
					<div class="form-box">
						<label>Title</label>
						<input type="text" name="title" value="<? if(isset($_POST['title'])){echo $_POST['title'];}?>"class="input-login validate[required]">
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Is Global</label>
						<input style="float:left; margin-left:70px;" type="checkbox" name="is_global">
						<label>Event will be Regional Event If checked</label>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Content</label>
						<textarea maxlength="500" name="content" style="width:600px; margin-left:110px; height:100px;" class="validate[required]"><? if(isset($_POST['content'])){echo $_POST['content'];}?></textarea>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Start Date</label>
						<input type="text" name="start_date" value="<?php echo $_POST['start_date']; ?>" class="calender input-login validate[required]">
					</div>
					<div class="form-box">
						<label>End Date</label>
						<input type="text" name="end_date" value="<?php echo $_POST['end_date']; ?>" class="calender input-login validate[required]">
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>State</label>
						<select style="margin-left:10px; width: 277px; float:right;" name="state_id" class="validate[required]">
							<option selected="selected" value=""></option>
							<? 	$rs_states = mysqli_query($conn,"select * from states where country_id = '".$row_user['country_id']."'"); 
								while($row_state_l = mysqli_fetch_assoc($rs_states)){ ?>
								<option <?=($_POST['state_id'] == $row_state_l['id'])?'selected="selected"':'';?> value="<?=$row_state_l['id']?>"><?=$row_state_l['name']?></option>
							<?  } ?>
						</select>
					</div>
					<div class="form-box">
						<label>City</label>
						<input type="text"  name="city_name" value="<?php if($city_name){ echo $city_name;} ?>" style="margin-left:20px; float:right;" type="text" class="input-login validate[required]" > 
					</div>
					<div class="clear"></div>
					
					<div class="clear"></div>
					<h2>Event Photo</h2>
					<div class="form-box">
						<label>Event Photo</label>
						<input type="file" name="photo">
					</div>
					<div class="form-box">
						<label>Max Photo size : 2MB</label>
					</div>
					
					<div class="clear"></div>
					<div class="form-box">
						<input type="submit" value=" Save " class="submit-login">
					</div>
					<div class="form-box">
						<a id="cancel" class="submit-login" href="<?=WWW?>blogs.html">Cancel</a>
					</div>
				</fieldset>
			</form>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>

<script type="text/javascript" src="<?=WWW?>js/jquery-ui.min.js"></script>
<link href="<?=WWW?>css/jquery-ui.css" type="text/css" rel="stylesheet">
<script>
$(document).ready(function(){
	var currentYear = (new Date).getFullYear();
	var last_of_year= currentYear + 20;
	$('.calender').datepicker({changeYear:"true", yearRange: currentYear +":"+ last_of_year});
	
	$('#event_form').validationEngine();
});
</script>
<style>
#cancel{background: url('<?php echo WWW;?>images/sign-in-button.png') no-repeat !important; color:#fff !important; padding-left:3px; top:0px !important;position:relative; left:-150px;}
</style>
<?php include('common/footer.php'); ?>