<?php include_once('includes/configuration.php');
$page = 'tournament-venue-list.html';
$selected_country = getGeoLocationCountry(); 

$videos = array();
$error = '';

if(isset($_GET['page']) && !empty($_GET['page'])){
	$current_page = str_replace('page','',trim($_GET['page']));
}else{
	$current_page = 1;
}

$records_per_page = 10;
$start_record  = ($current_page-1)*$records_per_page;

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
}

$tournament_permission = 0;


$tournament_id = trim($_GET['t_id']);;
$tournament_info = get_record_on_id('tournaments', $tournament_id);		

$page_title = 'Polls List - '.ucwords($tournament_info['title']);

if(isset($user_id) && !empty($user_id)){
	//$sql = "select * from tournament_permissions where tournament_id = $tournament_id and member_id = $user_id";
	//$rs_permission = mysqli_query($conn,$sql);
	if($tournament_info['user_id'] == $user_id){
		$tournament_permission = 1;
	}else{
		$tournament_permission = 0;
	}
}

if(empty($error) && isset($_GET['p_id']) && $_GET['action'] == 'delete'){
	$poll_id = trim($_GET['p_id']);
	if($tournament_permission){
		$sql = "DELETE FROM tournament_polls WHERE id = $poll_id ";
		mysqli_query($conn,$sql);
		$_SESSION['poll_deleted'] = 1;
	}
	header("Location:".WWW."tournament/polls/list/".$tournament_id);
	exit();
}

if(isset($_SESSION['poll_deleted']) && $_SESSION['poll_deleted']==1) {
	$poll_deleted = 1;
	unset($_SESSION['poll_deleted']);
}
if(isset($_SESSION['poll_updated']) && $_SESSION['poll_updated']==1) {
	$poll_updated = 1;
	unset($_SESSION['poll_updated']);
}
if(isset($_SESSION['poll_added']) && $_SESSION['poll_added']==1) {
	$poll_added = 1;
	unset($_SESSION['poll_added']);
}


$sql = "SELECT count(*) as record_count FROM tournament_polls WHERE tournament_id=$tournament_id ";
$rs_total = mysqli_query($conn,$sql);
$row_total = mysqli_fetch_assoc($rs_total);
$records_count = $row_total['record_count'];

$sql = "SELECT * FROM tournament_polls WHERE tournament_id=$tournament_id ORDER BY id DESC LIMIT $start_record,$records_per_page";
$rs_polls = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_polls)){
	$polls[] = $row;
}

?>
<?php include('common/header.php'); ?>
<style>
.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
dl {min-height:145px;}
.content {width:1010px;}
.large-column {width:790px;}
#table-list {width:100%;border:1px solid #ccc;}
#table-list tr{border:1px solid #ccc;}
#table-list th{padding:5px;background-color:#000;color:#FB7008;font-size:14px;}
#table-list td{padding:5px;text-align:center;}
#table-list a{color:#000;}
.add_tournament{font-size:14px;color:#000;}
</style>
	<div class="middle">
		<h1>Tournament Polls </h1>
		<h2><?php echo ucwords($tournament_info['title']); ?></h2>
		<div class="white-box content" id="dashboard">
			<?php if(isset($poll_deleted) && $poll_deleted == 1): ?>
				<div id="information">Poll deleted Successfully... !</div>
			<?php endif; ?>
			<?php if(isset($poll_updated) && $poll_updated == 1): ?>
				<div id="information">Poll updated Successfully... !</div>
			<?php endif; ?>
			<?php if(isset($poll_added) && $poll_added == 1): ?>
				<div id="information">Poll added Successfully... !</div>
			<?php endif; ?>
		
			<?php if(empty($user_info)): ?>
				<div id="error">You are not logged... !</div>
			<?php endif; ?>
			
			<?php if(!empty($error)): ?>
				<?php echo $error; ?>
			<?php endif; ?>
		
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
			<?php if($tournament_permission){ ?>
			<p style="float:right;"><a href="<?php echo WWW; ?>tournament/polls/add/<?php echo $tournament_id; ?>" class="add_tournament" title="Add New Poll"><img alt="Add New Poll" src="<?php echo WWW; ?>images/icons/add.png" border="0"> Add New Poll</a></p>
			 <div class="clear"></div>
				<table id="table-list" class="white-box">
				<tr><th>Title</th><th>Status</th><th>Action</th></tr>
				<?php for($i=0;$i<count($polls);$i++): ?>
					<tr>
						<td style="text-align:left;"><?php echo $polls[$i]['poll_question']; ?></td>
						<td><?php if($polls[$i]['is_active'] == 1) echo 'Active';else echo 'Inactive'; ?></td>
						<td>
							<a href="<?php echo WWW; ?>tournament/polls/edit/<?php echo $polls[$i]['id']; ?>" title="Edit Poll"><img alt="Edit Poll" src="<?php echo WWW; ?>images/icons/edit.png" border="0"></a>  &nbsp;&nbsp;
							<a href="<?php echo WWW; ?>tournament-poll-list.php?p_id=<?php echo $polls[$i]['id']; ?>&t_id=<?php echo $tournament_id; ?>&action=delete" onclick="return confirm('Are you sure to delete Poll.');" title="Delete Poll" ><img alt="Delete Poll" src="<?php echo WWW; ?>images/icons/delete.png" border="0"></a>
						</td>
					</tr>
				<?php endfor; ?>
				<?php if(empty($polls)): ?>
				<tr><td colspan="4">No Records</td></tr>
				<?php endif; ?>
				</table>
				<?php }else{ ?>
				<div id="error">You do not have Permission for this Tournament... !</div>
				<?php } ?>
				
				<div id="pagination-bottom">
				<?php  $paging_str = getPaging('tournament/polls/list/'.$tournament_id,$records_count,$current_page,$records_per_page);
				echo $paging_str;
				?>
		        <div class="clear"></div>
				</div>  
			</div>  
		    <div class="clear"></div>
			</div>
		</div>
		<!--<div class="rightbar"><?php //include('common/right-panel.php');?></div>-->
		<div class="clear"></div>
	</div>
	


<?php include('common/footer.php'); ?>