<?php include('common/header.php'); ?>
	<div class="middle">
		<h1>Our Team</h1>
		<div class="white-box content detail">
			<?php include('common/our-team.php');?>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>

<?php include('common/footer.php'); ?>