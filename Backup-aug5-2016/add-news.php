<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	<div class="middle">
		<h1>Add News </h1>
		<? 	if(isset($_POST) && !empty($_POST)){ 
				$title 	= $_POST['title'];
				$content= $_POST['content'];
				$date 		= date('Y-m-d H:i:s');
				$user_id 	= $_SESSION['ycdc_dbuid'];
				$isGlobal = ($_POST['is_global'] == 'on')?'1':'0';

				if(!isset($user_id) || empty($user_id)){
					echo '<div id="error">Please login first ... !</div>';
				}else{
					if(!empty($title) && !empty($content)){
						/*if($_POST['category']){
							$rs_cat = mysqli_query($conn,"select * from news_categories where name = '".$_POST['category']."'");
							if(mysqli_num_rows($rs_cat) == 0){
								mysqli_query($conn,"insert into news_categories (name, status) values ('".$_POST['category']."','1') ");
								$category_id = mysqli_insert_id();
							}else{
								$row_cat = mysqli_fetch_assoc($rs_cat);
								$category_id  = $row_cat['id'];
							}
						}else{
							$category_id = $_POST['category_id'];
						}*/

						$rs_news = mysqli_query($conn,"select * from news where title = '$title'");
						if(mysqli_num_rows($rs_news) == 0){
							$query = "insert into news (title, content, status, news_date, user_id, is_global,record_count,`type`) 
							values ('$title','$content','1','$date','$user_id','$isGlobal','0','')";
							//echo $query;exit;
							if(mysqli_query($conn,$query)){
								$news_id = mysqli_insert_id();

								if(!is_dir('news/'.$news_id)){
									mkdir('news/'.$news_id,0777);
								}
								chmod('news/'.$news_id,0777);

								if(!empty($_FILES['photo']['name'])){
									$filename 	= friendlyURL($title).'.jpg';
									$image 		= new SimpleImage();
									$image->load($_FILES["photo"]["tmp_name"]);
									$image->save('news/'.$news_id.'/'.$filename);
									chmod('news/'.$news_id.'/'.$filename,0777);
								
									mysqli_query($conn,"insert into photos (file_name, title, entity_id, entity_type,is_default,is_thumb) values ('$filename','$title','$news_id','news','0','0')");
									
								}
							}
							
			                echo '<div id="success">News Information added successfully ... !</div>';
			                ?>
			                <script>
			                window.location = '<?=WWW?>my-news.html';
			                </script>
			                <?
						}else{
							echo '<div id="error">Information already exists ... !</div>';
						}
					}else{
						echo '<div id="error">Please fill all fields ... !</div>';
					}
				}
				
			}
		?>
		<div class="white-box content">
			<form method="post" enctype="multipart/form-data">
				<fieldset>
					<h2>News Information</h2>
					<?php /* ?><div class="form-box">
						<label>News Category</label>
						<?=get_combo('news_categories','name','','category_id')?>
					</div><?php */ ?>
					<div class="clear"></div>
					<div class="form-box">
						<label>Title</label>
						<input type="text" name="title" style="width: 254px;" class="input-login validate[required]">
					</div>
					<div class="clear"></div>
					<div class="form-box" style="width:100%;">
						<label>Is Global</label>
						<input style="float:left; margin-left:70px;" type="checkbox" name="is_global">
						<label>News will be Regional News If checked</label>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Content</label>
						<textarea maxlength="500" name="content" style="width:600px; margin-left:125px; height:100px;" class="validate[required]"></textarea>
					</div>
					<div class="clear"></div>
					<h2>News Photo</h2>
					<div class="form-box">
						<label>News Photo</label>
						<input type="file" name="photo">
					</div>
					<div class="form-box">
						<label>Max Photo size : 2MB</label>
					</div>
					<div class="clear"></div>
					
					<div class="form-box">
						<input type="submit" value=" Save " class="submit-login">
					</div>
					<div class="form-box">
						<a id="cancel" class="submit-login" href="<?=WWW?>news.html">Cancel</a>
					</div>
				</fieldset>
			</form>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>
<style>
#cancel{background: url('<?php echo WWW;?>images/sign-in-button.png') no-repeat !important; color:#fff !important; padding-left:3px; top:0px !important;position:relative; left:-150px;}
</style>
<?php include('common/footer.php'); ?>