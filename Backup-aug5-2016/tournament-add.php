<?php include_once('includes/configuration.php');
$page = 'tournament-add.html';
$page_title = 'Add New Tournament';
$selected_country = getGeoLocationCountry(); 

$league_clubs = array();
$league_info = array();
$error = '';
$tournament_overs = array('Ten10'=>'Ten10','Fifteen15'=>'Fifteen15','Twenty20'=>'Twenty20','Thirty30'=>'Thirty30','One Day'=>'One Day');
$overs = array('10'=>'10','15'=>'15','20'=>'20','30'=>'30','35'=>'35','40'=>'40','45'=>'45','50'=>'50','55'=>'55','60'=>'60');
$ball_type = array('hard_ball'=>'Hard Ball','rubber_ball'=>'Rubber Ball','tape_ball'=>'Tape Ball','tennis_ball'=>'Tennis Ball');

$sql = "select id,name from countries where status=1";
$rs_countries = mysqli_query($conn,$sql);


if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
	if($user_info['is_company'] == 1){
		$sql = "select * from companies where user_id = $user_id and company_type_id = 2 and status=1";
		$rs_company = mysqli_query($conn,$sql);
		if(mysqli_num_rows($rs_company)){
			$league_info = mysqli_fetch_assoc($rs_company);
			$sql = 'select c.* from league_clubs as lc inner join companies as c on lc.club_id=c.id where lc.league_id = '.$league_info['id'];
			$rs_club = mysqli_query($conn,$sql);
			while($row = mysqli_fetch_assoc($rs_club)){
				$league_clubs[] = $row;
			}
		}else{
			$error = '<p id="error">You must be logged as League Owner</p>';
		}
	}
}

//var_dump($_POST);
if(isset($_POST['submit_btn']) && !empty($_POST['submit_btn'])){
	validate();
	if(empty($error)){
		$date = date('Y/m/d H:i:s')	;
		if(empty($_POST['state_id'])) $_POST['state_id'] = 0;
		$sql = " INSERT INTO tournaments SET year='".trim($_POST['year'])."',title='".trim($_POST['title'])."',league_id='".trim($league_info['id'])."',
		user_id='".trim($user_id)."',country_id='".trim($_POST['country_id'])."',state_id='".trim($_POST['state_id'])."',city='".trim($_POST['city'])."'
		,overs_type='".trim($_POST['overs_type'])."',overs_count='".trim($_POST['overs_count'])."',start_time='".trim($_POST['start_time'])."',ball_type='".trim($_POST['ball_type'])."'
		,end_time='".trim($_POST['end_time'])."',added_on='".$date."',updated_on='".$date."',status=1";
		
		if(mysqli_query($conn,$sql)){
			$tournament_id = mysqli_insert_id();
			$sql = "UPDATE companies SET tournament_data_added = 'yes' WHERE id = ".$league_info['id'];
			mysqli_query($conn,$sql);
			
			$_SESSION['tournament_added'] = 1;
			header("Location:".WWW."tournament/list");
		}else{
			$error = '<p id="error">Error in adding Tournament. Try again later</p>';
		}
	}
	
}

function validate(){
	global $error;
	if(empty($_POST['year'])){
		$error.= '<p id="error">Year is required field</p>';
	}
	if(empty($_POST['title'])){
		$error.= '<p id="error">Title is required field</p>';
	}
	if(empty($_POST['country_id'])){
		$error.= '<p id="error">Country is required field</p>';
	}
	/*if(empty($_POST['state_id'])){
		$error.= '<p id="error">State is required field</p>';
	}
	if(empty($_POST['city'])){
		$error.= '<p id="error">City is required field</p>';
	}*/
	if(empty($_POST['overs_type'])){
		$error.= '<p id="error">Game Type is required field</p>';
	}
	if(empty($_POST['overs_count'])){
		$error.= '<p id="error">Game Overs is required field</p>';
	}
	if(empty($_POST['ball_type'])){
		$error.= '<p id="error">Preferred Ball Type is required field</p>';
	}
	if(empty($_POST['start_time'])){
		$error.= '<p id="error">Start Time is required field</p>';
	}
	if(empty($_POST['end_time'])){
		$error.= '<p id="error">End Time is required field</p>';
	}
	if(!empty($_POST['start_time']) && !empty($_POST['end_time']) && strtotime($_POST['end_time']) < strtotime($_POST['start_time'])){
		$error.= '<p id="error">Start Time must be Less than End Time</p>';
	}
}

?>
<?php include('common/header.php'); ?>
<style>
.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
dl {min-height:145px;}
.content {width:1010px;}
.large-column {width:790px;}
.form-box {width:60%;}
.form-box .text {width:70%;}
.form-box .text select {width:277px;}
.chk_left {float:left !important; }
</style>
	<div class="middle">
		<h1> Add New Tournament </h1>
		
		<div class="white-box content" id="dashboard">
			<?php if(empty($user_info)): ?>
				<div id="error">You are not logged... !</div>
			<?php endif; ?>
			
			<?php if(!empty($error)): ?>
				<?php echo $error; ?>
			<?php endif; ?>
		
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
			
				<form method="post"  enctype="multipart/form-data">
					<input type="hidden" name="action" value="submit">
					<fieldset>
						<h2>Tournament Details</h2>
						<div class="form-box">
							<label>Year*</label>
							<div class="text">
								<select name="year" id="year" > 
								<option value="">Select One</option>
								<?php for($i=date('Y')-1;$i<=date('Y')+1;$i++): ?>
									<?php  if(isset($_POST['year']) && $_POST['year'] == $i) $sel =  'selected';else $sel = ''; ?>
									<option <?php echo $sel; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>	
								<?php endfor; ?>
								</select>
							</div>
						</div>
						<div class="clear"></div>
						<div class="form-box">
							<label>Title*</label>
							<div class="text"><input type="text" name="title" id="title" class="input-login" value="<?php if(!empty($_POST['title'])) echo $_POST['title']; ?>"></div>
						</div>
						<div class="clear"></div>
						<div class="form-box">
							<label>Country*</label>
							<div class="text">
								<select name="country_id" id="country_id" onchange="getStates(this.value,'')"> 
								<option value="">Select One</option>
								<?php while($row = mysqli_fetch_assoc($rs_countries)): if(isset($_POST['country_id']) && $_POST['country_id'] == $row['id']) $sel =  'selected';else $sel = '';  ?>
									<option <?php echo $sel; ?> value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
								<?php endwhile; ?>
								</select>
							</div>
						</div>
						<div class="clear"></div>
						
						<div class="form-box">
							<label>State</label>
							<div class="text">
								<select name="state_id" id="state_id" > 
									<option value="0">Select One</option>
								</select>
							</div>
						</div>
						<div class="clear"></div>
						
						<div class="form-box">
							<label>City</label>
							<div class="text"><input type="text" name="city" id="city" class="input-login" value="<?php if(!empty($_POST['city'])) echo $_POST['city']; ?>"></div>
						</div>
						<div class="clear"></div>
						
						<div class="form-box">
							<label>Game Type*</label>
							<div class="text">
							<select name="overs_type" id="overs_type"> 
							<option value="">Select One</option>
							<?php foreach($tournament_overs as $key=>$val): if(isset($_POST['overs_type']) && $_POST['overs_type'] == $key) $sel =  'selected';else $sel = '';  ?>
								<option <?php echo $sel; ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
							<?php endforeach; ?>
							</select>
							</div>
						</div>
						<div class="clear"></div>
						
						<div class="form-box">
							<label>Overs*</label>
							<div class="text">
							<select name="overs_count" id="overs_count"> 
							<option value="">Select One</option>
							<?php foreach($overs as $key=>$val): if(isset($_POST['overs_count']) && $_POST['overs_count'] == $key) $sel =  'selected'; else $sel = '';  ?>
								<option <?php echo $sel; ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
							<?php endforeach; ?>
							</select>
							</div>
						</div>
						<div class="clear"></div>
						
						<div class="form-box">
							<label>Preferred Ball Type*</label>
							<div class="text">
							<select name="ball_type" id="ball_type" style="width:277px;"> 
							<option value="">Select One</option>
							<?php foreach($ball_type as $key=>$val): if(isset($_POST['ball_type']) && $_POST['ball_type'] == $key) $sel =  'selected';else $sel = '';  ?>
								<option <?php echo $sel; ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
							<?php endforeach; ?>
							</select>
							</div>
						</div>
						<div class="clear"></div>

						<div class="form-box">
							<label>Start Date*</label>
							<div class="text"><input type="text" autocomplete="off" name="start_time" id="start_time" class="input-login" value="<?php if(!empty($_POST['start_time'])) echo $_POST['start_time']; ?>"></div>
						</div>
						<div class="clear"></div>
						<div class="form-box">
							<label>End Date*</label>
							<div class="text"><input type="text" autocomplete="off" name="end_time" id="end_time" class="input-login" value="<?php if(!empty($_POST['end_time'])) echo $_POST['end_time']; ?>"></div>
						</div>
						<div class="clear"></div><div class="clear"><br/></div>
						
						<div class="form-box">
							<input type="button" name="cancel_btn" value=" Cancel " class="submit-login" onclick="window.location.href='<?php echo WWW; ?>tournament/list';">
							<input type="submit" name="submit_btn" value=" Submit " class="submit-login" <?php if(empty($user_info) || empty($league_info)): ?> disabled <?php endif; ?>>
						</div>
					</fieldset>
				</form>
			</div>  
		    <div class="clear"></div>
			</div>
		</div>
		<!--<div class="rightbar"><?php //include('common/right-panel.php');?></div>-->
		<div class="clear"></div>
	</div>
	

<script src="<?php echo WWW; ?>js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo WWW; ?>css/jquery.datetimepicker.css"/>
<script>
$('#start_time').datetimepicker();
$('#end_time').datetimepicker();

function getStates(id,sel){
	var data = "sel="+sel;
 $.ajax({
			url: "<?php echo WWW; ?>get_states.php?id="+id,
			type:"POST",
			data:data,
			success: function(data) {
				$("#state_id").html(data);
			}
		});
}		
<?php if(!empty($error) && !empty($_POST['country_id'])){ ?>
	<?php if(!empty($_POST['state_id'])) $state = $_POST['state_id'];else $state = '0'; ?>
	getStates(<?php echo $_POST['country_id']; ?>,<?php echo $state; ?>);
<?php } ?>
</script>
<?php include('common/footer.php'); ?>