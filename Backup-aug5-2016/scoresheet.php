<?php include_once('includes/configuration.php');
$page = 'scoresheet.html';
$selected_country = getGeoLocationCountry(); 

$league_clubs = array();
$league_info = array();
$matches = array();
$user_tournaments = array();
$overs_score1 = array();
$overs_score2 = array();
$how_out_first_innings = array();
$match_videos = array();
$error = '';

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
	
}

$match_id = trim($_GET['match_id']);

$sql = "SELECT tm.*,c1.company_name as team_bat_first,c2.company_name as team_bat_second, c3.company_name as team_won_toss ,
c4.company_name as team1,c5.company_name as team2,c6.company_name as winning_team_name, concat(u1.f_name,' ',u1.last_name) as umpire1_name,
 concat(u2.f_name,' ',u2.last_name) as umpire2_name, concat(u3.f_name,' ',u3.last_name) as umpire3_name,
 concat(u4.f_name,' ',u4.last_name) as umpire4_name,tv.venue,tv.address,concat(u5.f_name,' ',u5.last_name) as captain_team1
 ,concat(u6.f_name,' ',u6.last_name) as captain_team2,concat(u7.f_name,' ',u7.last_name) as man_of_match_player_name
FROM tournament_matches as tm 
left join companies as c1 on tm.batting_team1=c1.id  
left join companies as c2 on tm.batting_team2=c2.id 
left join companies as c3 on tm.toss_won_team_id=c3.id 
inner join companies as c4 on tm.team1=c4.id 
inner join companies as c5 on tm.team2=c5.id 
left join companies as c6 on tm.winning_team_id=c6.id 
left join league_umpires as tu1 on tm.umpire1_id = tu1.umpire_id left join users as u1 on tu1.umpire_id= u1.id 
left join league_umpires as tu2 on tm.umpire2_id = tu2.umpire_id left join users as u2 on tu2.umpire_id= u2.id 
left join league_umpires as tu3 on tm.umpire3_id = tu3.umpire_id left join users as u3 on tu3.umpire_id= u3.id 
left join league_umpires as ref on tm.referee_id = ref.umpire_id left join users as u4 on ref.umpire_id= u4.id 
left join league_venues as tv on tm.venue_id = tv.id
left join users as u5 on tm.captain_team1= u5.id left join users as u6 on tm.captain_team2 = u6.id 
left join users as u7 on tm.man_of_match_player_id = u7.id 
WHERE tm.id = $match_id and tm.status = 1 ";
$rs = mysqli_query($conn,$sql);//echo $sql.mysqli_error($conn);
$match_info  = mysqli_fetch_assoc($rs);

$sql = "SELECT * from tournament_bat_scorecard WHERE match_id=$match_id and team_id=".$match_info['batting_team1']." ORDER by batsman_no ";
$rs_batting1 = mysqli_query($conn,$sql);

$sql = "SELECT * from tournament_bat_scorecard WHERE match_id=$match_id and team_id=".$match_info['batting_team2']." ORDER by batsman_no ";
$rs_batting2 = mysqli_query($conn,$sql);

$sql = "SELECT * from tournament_bowl_scorecard WHERE match_id=$match_id and team_id=".$match_info['batting_team2']." ORDER by bowler_no ";
$rs_bowling1 = mysqli_query($conn,$sql);

$sql = "SELECT * from tournament_bowl_scorecard WHERE match_id=$match_id and team_id=".$match_info['batting_team1']." ORDER by bowler_no ";
$rs_bowling2 = mysqli_query($conn,$sql);

$sql = "SELECT * from tournament_fow_scorecard WHERE match_id=$match_id and team_id=".$match_info['batting_team1']." and status = 1 ORDER by id ";
$rs_fow1 = mysqli_query($conn,$sql);

$sql = "SELECT * from tournament_fow_scorecard WHERE match_id=$match_id and team_id=".$match_info['batting_team2']." and status = 1 ORDER by id ";
$rs_fow2 = mysqli_query($conn,$sql);

$tournament_info = get_record_on_id('tournaments', $match_info['tournament_id']);	
$tournament_id = $tournament_info['id'];
$tournament_owner_id = $tournament_info['user_id'];

$batting1_ids = array();$batting2_ids = array();

$sql = "SELECT u.id,u.f_name,u.last_name from club_members as c inner join users as u on c.member_id=u.id WHERE c.club_id=".$match_info['batting_team1'];
$rs = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs)){
	$batting1_data[$row['id']] = $row['f_name'].' '.$row['m_name'].' '.$row['last_name'];
	$batting1_ids[] = $row['id'];
}

$sql = "SELECT u.id,u.f_name,u.last_name from tournament_bat_scorecard as c inner join users as u on c.batsman_id=u.id WHERE c.match_id=".$match_info['id']." and team_id = ".$match_info['batting_team1'];
if(!empty($batting1_ids)){
	$batting1_ids_str = implode(',',$batting1_ids);
	$sql.=" AND c.batsman_id NOT IN($batting1_ids_str) ";

	$rs = mysqli_query($conn,$sql);
	if(mysqli_num_rows($rs)){
		while($row = mysqli_fetch_assoc($rs)){
			$batting1_data[$row['id']] = $row['f_name'].' '.$row['m_name'].' '.$row['last_name'];
		}
	}
}

$sql = "SELECT u.id,u.f_name,u.last_name from club_members as c inner join users as u on c.member_id=u.id WHERE c.club_id=".$match_info['batting_team2'];
$rs = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs)){
	$batting2_data[$row['id']] = $row['f_name'].' '.$row['m_name'].' '.$row['last_name'];
	$batting2_ids[] = $row['id'];
}

$sql = "SELECT u.id,u.f_name,u.last_name from tournament_bat_scorecard as c inner join users as u on c.batsman_id=u.id WHERE c.match_id=".$match_info['id']." and team_id = ".$match_info['batting_team2'];
if(!empty($batting2_ids)){
	$batting2_ids_str = implode(',',$batting2_ids);
	$sql.=" AND c.batsman_id NOT IN($batting2_ids_str) ";

	$rs = mysqli_query($conn,$sql);
	if(mysqli_num_rows($rs)){
		while($row = mysqli_fetch_assoc($rs)){
			$batting2_data[$row['id']] = $row['f_name'].' '.$row['m_name'].' '.$row['last_name'];
		}
	}
}

$sql = "SELECT team_overs,team_score,team_wickets,score_in_over FROM `tournament_match_commentary` 
WHERE match_id=$match_id and team_id=".$match_info['batting_team1']." and team_overs like '%.6' and ball_type='legal_ball' ";
$rs = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs)){
	$overs_score1[] = $row;
}

$sql = "SELECT count(id) as no_balls_count FROM `tournament_match_commentary` 
WHERE match_id=$match_id and team_id=".$match_info['batting_team1']."  and ball_type='no_ball' ";
$rs = mysqli_query($conn,$sql);
$row = mysqli_fetch_assoc($rs);
$no_ball_first_innings = $row['no_balls_count'];
if(empty($no_ball_first_innings)) $no_ball_first_innings =0;

$sql = "SELECT sum(ball_extra_runs) as wide_balls_runs FROM `tournament_match_commentary` 
WHERE match_id=$match_id and team_id=".$match_info['batting_team1']."  and ball_type='wide_ball' ";
$rs = mysqli_query($conn,$sql);
$row = mysqli_fetch_assoc($rs);
$wide_ball_first_innings = $row['wide_balls_runs'];
if(empty($wide_ball_first_innings)) $wide_ball_first_innings =0;

$sql = "SELECT sum(ball_score) as byes_runs FROM `tournament_match_commentary` 
WHERE match_id=$match_id and team_id=".$match_info['batting_team1']."  and runs_type='byes' ";
$rs = mysqli_query($conn,$sql);
$row = mysqli_fetch_assoc($rs);
$byes_runs_first_innings = $row['byes_runs'];
if(empty($byes_runs_first_innings)) $byes_runs_first_innings =0;

$sql = "SELECT sum(ball_score) as leg_byes_runs FROM `tournament_match_commentary` 
WHERE match_id=$match_id and team_id=".$match_info['batting_team1']."  and runs_type='leg_byes' ";
$rs = mysqli_query($conn,$sql);
$row = mysqli_fetch_assoc($rs);
$leg_byes_runs_first_innings = $row['leg_byes_runs'];
if(empty($leg_byes_runs_first_innings)) $leg_byes_runs_first_innings =0;

$sql = "SELECT count(id) as out_count,how_out FROM `tournament_match_commentary` WHERE match_id=$match_id and team_id=".$match_info['batting_team1']." 
and batsman_out='yes' group by how_out ";
$rs = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs)){
	$how_out_first_innings[] = $row;
}

$sql = "SELECT count(ball_score) as score_type_count,ball_score FROM `tournament_match_commentary` WHERE match_id=$match_id and team_id=".$match_info['batting_team1']." 
and runs_type='batsman_runs' and (ball_type='legal_ball' OR ball_type='no_ball') group by ball_score having ball_score>0";
$rs = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs)){
	$runs_type_first_innings[] = $row;
}

$sql = "SELECT team_overs,team_score,team_wickets,score_in_over FROM `tournament_match_commentary` 
WHERE match_id=$match_id and team_id=".$match_info['batting_team2']." and team_overs like '%.6' and ball_type='legal_ball' ";
$rs = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs)){
	$overs_score2[] = $row;
}

$sql = "SELECT count(id) as no_balls_count FROM `tournament_match_commentary` 
WHERE match_id=$match_id and team_id=".$match_info['batting_team2']."  and ball_type='no_ball' ";
$rs = mysqli_query($conn,$sql);
$row = mysqli_fetch_assoc($rs);
$no_ball_second_innings = $row['no_balls_count'];
if(empty($no_ball_second_innings)) $no_ball_second_innings =0;

$sql = "SELECT sum(ball_extra_runs) as wide_balls_runs FROM `tournament_match_commentary` 
WHERE match_id=$match_id and team_id=".$match_info['batting_team2']."  and ball_type='wide_ball' ";
$rs = mysqli_query($conn,$sql);
$row = mysqli_fetch_assoc($rs);
$wide_ball_second_innings = $row['wide_balls_runs'];
if(empty($wide_ball_second_innings)) $wide_ball_second_innings =0;

$sql = "SELECT sum(ball_score) as byes_runs FROM `tournament_match_commentary` 
WHERE match_id=$match_id and team_id=".$match_info['batting_team2']."  and runs_type='byes' ";
$rs = mysqli_query($conn,$sql);
$row = mysqli_fetch_assoc($rs);
$byes_runs_second_innings = $row['byes_runs'];
if(empty($byes_runs_second_innings)) $byes_runs_second_innings =0;

$sql = "SELECT sum(ball_score) as leg_byes_runs FROM `tournament_match_commentary` 
WHERE match_id=$match_id and team_id=".$match_info['batting_team2']."  and runs_type='leg_byes' ";
$rs = mysqli_query($conn,$sql);
$row = mysqli_fetch_assoc($rs);
$leg_byes_runs_second_innings = $row['leg_byes_runs'];
if(empty($leg_byes_runs_second_innings)) $leg_byes_runs_second_innings =0;

$sql = "SELECT count(id) as out_count,how_out FROM `tournament_match_commentary` WHERE match_id=$match_id and team_id=".$match_info['batting_team2']." 
and batsman_out='yes' group by how_out ";
$rs = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs)){
	$how_out_second_innings[] = $row;
}

$sql = "SELECT count(ball_score) as score_type_count,ball_score FROM `tournament_match_commentary` WHERE match_id=$match_id and team_id=".$match_info['batting_team2']." 
and runs_type='batsman_runs' and (ball_type='legal_ball' OR ball_type='no_ball') group by ball_score having ball_score>0";
$rs = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs)){
	$runs_type_second_innings[] = $row;
}

$sql = "SELECT * FROM tournament_match_videos WHERE match_id=$match_id";
$rs = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs)){
	$match_videos[] = $row;
}

$total_runs = 0;$total_balls = 0;$total_fours = 0;$total_sixes = 0;

$page_title = 'Scorecard - '.$match_info['team1'].' vs '.$match_info['team2'].' - '.ucwords($tournament_info['title']);
?>
<?php include('common/header.php'); ?>
<style>
.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
dl {min-height:145px;}
.content {width:1010px;}
.large-column {width:790px;}
#table-list {width:90%;border:1px solid #ccc;}
#table-list tr{border:1px solid #ccc;}
#table-list th{padding:5px;background-color:#000;color:#FB7008;font-size:14px;}
#table-list td{padding:5px;text-align:center;}
#table-list a{color:#000;}
#table-list select {width:145px;}
.textb1 {width:50px;height:20px;}
.add_tournament{font-size:14px;color:#000;}
#dashboard.content{width:1050px;}
#scoresheet {float:left;width:100%;}
#commentary {float:left;width:100%;}
#statistics {float:left;width:100%;}
#innings1-commentary,#innings2-commentary{float:left;width:70%;}
#innings1-statistics,#innings2-statistics,#bar1_chart{float:left;width:100%;}
#menu a{width:24%;}
.match-gallery {float:left;padding:2px;margin:4px;min-height:150px;text-align:center;}
.match-gallery img{border:1px solid #cccccc;padding:2px;}
#gallery {float:left;padding:2px;}
.poll-div{ border: 1px solid #b4c1a7;border-radius: 2px;float: left;min-width:300px;padding: 6px;}
.poll_question{color: #5A5A5A;float:left;font-size: 14px;font-weight:bold;padding: 4px 0;width: 100%;}
.poll_option{float:left;font-size:12px;padding:4px 0;width: 100%;}
.option_text{width:30%;float:left;}
.option_bar{width:70%;float:left;}
.option_percent{float:left;}
</style>
  <script type="text/javascript" src="https://www.google.com/jsapi"></script>
  <script type="text/javascript">
  google.load('visualization', '1', {packages: ['corechart', 'bar','line']});
google.setOnLoadCallback(drawGraphs);

function drawGraphs() {
	var width1 = 800;	
      var data = new google.visualization.DataTable();
      data.addColumn('number', 'Overs');
      data.addColumn('number', 'Runs');
	  
     
	data.addRows([
	  <?php for($i=0;$i<count($overs_score1);$i++){ ?>
        [<?php $overs =  $overs_score1[$i]['team_overs']+0.4; echo $overs; ?>, <?php echo $overs_score1[$i]['score_in_over']; ?>],
		<?php $overs_str.=$overs.',';  ?>
      <?php } ?>
      ]);

      var options = {
        title: 'Runs in the Overs',
        hAxis: {
          title: 'Overs',
          ticks: [ <?php echo rtrim($overs_str,','); ?>]
		},
        vAxis: {
          title: 'Runs'
        },
		width:width1
      };
	
	
	var chart = new google.visualization.ColumnChart(document.getElementById('bar1_chart'));
	chart.draw(data, options);
	
	<?php $overs_str = ''; ?>	
	var data = new google.visualization.DataTable();
    data.addColumn('number', 'Runs');
    data.addColumn('number', 'Number of shots');
	  
    data.addRows([
	  <?php for($i=0;$i<count($runs_type_first_innings);$i++){ ?>
        [<?php echo $runs_type_first_innings[$i]['ball_score']; ?>,<?php  echo $runs_type_first_innings[$i]['score_type_count']; ?>],
		<?php } ?>
    ]);

    var options = {
        title: 'Types of Shots',
        hAxis: {
          title: 'Shots',
           ticks: [1,2,3,4,5,6]
		},
        vAxis: {
          title: 'Number of shots'
        },
		width:width1
    };
	
	var chart = new google.visualization.ColumnChart(document.getElementById('runs_type1_chart'));
	chart.draw(data, options);
	
	<?php $overs_str = ''; ?>	
	
	var data = new google.visualization.DataTable();
      data.addColumn('number', 'Overs');
      data.addColumn('number', 'Runs');
	  data.addColumn('number', 'Wickets');
     
	data.addRows([
	  <?php for($i=0;$i<count($overs_score1);$i++){ ?>
        [<?php $overs =  $overs_score1[$i]['team_overs']+0.4; echo $overs; ?>, <?php echo $overs_score1[$i]['team_score']; ?>, <?php echo $overs_score1[$i]['team_wickets']; ?>],
		<?php $overs_str.=$overs.',';  ?>
      <?php } ?>
      ]);

      var options = {
        title: 'Runs and Wickets with Overs',
        hAxis: {
          title: 'Overs',
          ticks: [ <?php echo rtrim($overs_str,','); ?>]
		},
        vAxis: {
          title: 'Runs'
        },
		width:width1
      };
		
	var chart = new google.visualization.LineChart(document.getElementById('line1_chart'));
	chart.draw(data, options);
	
	<?php $overs_str = ''; ?>	
	var data = new google.visualization.DataTable();
      data.addColumn('number', 'Overs');
      data.addColumn('number', 'Run Rate');
	  
     
	data.addRows([
	  <?php for($i=0;$i<count($overs_score1);$i++){ ?>
        [<?php $overs =  $overs_score1[$i]['team_overs']+0.4; echo $overs; ?>, <?php echo round($overs_score1[$i]['team_score']/$overs,2); ?>],
		<?php $overs_str.=$overs.',';  ?>
      <?php } ?>
      ]);

      var options = {
        title: 'Run Rate with Overs',
        hAxis: {
          title: 'Overs',
          ticks: [ <?php echo rtrim($overs_str,','); ?>]
		},
        vAxis: {
          title: 'Run Rate'
        },
		width:width1
      };
		
	var chart = new google.visualization.LineChart(document.getElementById('run_rate1_chart'));
	chart.draw(data, options);
	
	var data = google.visualization.arrayToDataTable([
          ['Extras Type', 'Runs'],
          ['Wides',  <?php echo $wide_ball_first_innings; ?>],
          ['No Balls',      <?php echo $no_ball_first_innings; ?>],
          ['Byes',  <?php echo $byes_runs_first_innings; ?>],
          ['Leg Byes', <?php echo $leg_byes_runs_first_innings; ?>]
		  ]);

    var options = {
		title: 'Extra Runs Chart',width:width1
    };

    var chart = new google.visualization.PieChart(document.getElementById('extras1_chart'));
	chart.draw(data, options);
	
	var data = google.visualization.arrayToDataTable([
          ['How Out', 'Number'],
		  <?php for($i=0;$i<count($how_out_first_innings);$i++){ ?>
          ['<?php echo $how_out_first_innings[$i]['how_out']; ?>',  <?php echo $how_out_first_innings[$i]['out_count']; ?>],
		  <?php } ?>
          ]);

    var options = {
		title: 'Wickets Chart',width:width1
    };

    var chart = new google.visualization.PieChart(document.getElementById('how_out1_chart'));
	chart.draw(data, options);
	
	
	<?php $overs_str = ''; ?>	
	 var data = new google.visualization.DataTable();
      data.addColumn('number', 'Overs');
      data.addColumn('number', 'Runs');
     
	data.addRows([
	  <?php for($i=0;$i<count($overs_score2);$i++){ ?>
        [<?php $overs =  $overs_score2[$i]['team_overs']+0.4; echo $overs; ?>, <?php echo $overs_score2[$i]['score_in_over']; ?>],
		<?php $overs_str.=$overs.',';  ?>
      <?php } ?>
      ]);

      var options = {
        title: 'Runs in the Overs',
        hAxis: {
          title: 'Overs',
          ticks: [ <?php echo rtrim($overs_str,','); ?>]
		},
        vAxis: {
          title: 'Runs'
        },
		width:width1
      };
	

	var chart = new google.visualization.ColumnChart(document.getElementById('bar2_chart'));
	chart.draw(data, options);
	
	<?php $overs_str = ''; ?>	
	var data = new google.visualization.DataTable();
    data.addColumn('number', 'Runs');
    data.addColumn('number', 'Number of shots');
	  
    data.addRows([
	  <?php for($i=0;$i<count($runs_type_second_innings);$i++){ ?>
        [<?php echo $runs_type_second_innings[$i]['ball_score']; ?>,<?php  echo $runs_type_second_innings[$i]['score_type_count']; ?>],
		<?php } ?>
    ]);

    var options = {
        title: 'Types of Shots',
        hAxis: {
          title: 'Shots',
		  ticks: [1,2,3,4,5,6]
        },
        vAxis: {
          title: 'Number of shots'
        },
		width:width1
    };
	
	var chart = new google.visualization.ColumnChart(document.getElementById('runs_type2_chart'));
	chart.draw(data, options);
	
	<?php $overs_str = ''; ?>	
	var data = new google.visualization.DataTable();
      data.addColumn('number', 'Overs');
      data.addColumn('number', 'Runs');
	  data.addColumn('number', 'Wickets');
     
	data.addRows([
	  <?php for($i=0;$i<count($overs_score2);$i++){ ?>
        [<?php $overs =  $overs_score2[$i]['team_overs']+0.4; echo $overs; ?>, <?php echo $overs_score2[$i]['team_score']; ?>, <?php echo $overs_score2[$i]['team_wickets']; ?>],
		<?php $overs_str.=$overs.',';  ?>
      <?php } ?>
      ]);

      var options = {
        title: 'Runs and Wickets with Overs',
        hAxis: {
          title: 'Overs',
          ticks: [ <?php echo rtrim($overs_str,','); ?>]
		},
        vAxis: {
          title: 'Runs'
        },
		width:width1
      };
		
	var chart = new google.visualization.LineChart(document.getElementById('line2_chart'));
	chart.draw(data, options);
	
	<?php $overs_str = ''; ?>	
	var data = new google.visualization.DataTable();
    data.addColumn('number', 'Overs');
    data.addColumn('number', 'Run Rate');
	  
    data.addRows([
	  <?php for($i=0;$i<count($overs_score2);$i++){ ?>
        [<?php $overs =  $overs_score2[$i]['team_overs']+0.4; echo $overs; ?>, <?php echo round($overs_score2[$i]['team_score']/$overs,2); ?>],
		<?php $overs_str.=$overs.',';  ?>
      <?php } ?>
      ]);

      var options = {
        title: 'Run Rate with Overs',
        hAxis: {
          title: 'Overs',
          ticks: [ <?php echo rtrim($overs_str,','); ?>]
		},
        vAxis: {
          title: 'Run Rate'
        },
		width:width1
      };
		
	var chart = new google.visualization.LineChart(document.getElementById('run_rate2_chart'));
	chart.draw(data, options);
	
	var data = google.visualization.arrayToDataTable([
          ['Extras Type', 'Runs'],
          ['Wides',  <?php echo $wide_ball_second_innings; ?>],
          ['No Balls',      <?php echo $no_ball_second_innings; ?>],
          ['Byes',  <?php echo $byes_runs_second_innings; ?>],
          ['Leg Byes', <?php echo $leg_byes_runs_second_innings; ?>]
		  ]);

    var options = {
		title: 'Extra Runs Chart',width:width1
    };

    var chart = new google.visualization.PieChart(document.getElementById('extras2_chart'));
	chart.draw(data, options);
	
	var data = google.visualization.arrayToDataTable([
          ['How Out', 'Number'],
		  <?php for($i=0;$i<count($how_out_second_innings);$i++){ ?>
          ['<?php echo $how_out_second_innings[$i]['how_out']; ?>',  <?php echo $how_out_second_innings[$i]['out_count']; ?>],
		  <?php } ?>
          ]);

    var options = {
		title: 'Wickets Chart',width:width1
    };

    var chart = new google.visualization.PieChart(document.getElementById('how_out2_chart'));
	chart.draw(data, options);

}
  </script>
	<div class="middle">
		<h1> Match Scorecard </h1>
		<h2> Tournament: <?php echo ucwords($tournament_info['title']); ?>  </h2>
		
		<div class="white-box content" id="dashboard">
			
		
			<?php /*if(empty($user_info)): ?>
				<div id="error">You are not logged... !</div>
			<?php endif;*/ ?>
			
			<?php if(!empty($error)): ?>
				<?php echo $error; ?>
			<?php endif; ?>
		
			<div class="small-column">
				<? //include('common/user-left-panel.php');?>
			</div>
			<div class="large-column" style="width:97%">
			<ul id="menu" class="menu">
				<li class="active"><a href="#scoresheet">Scoresheet</a></li>
				<li><a href="#commentary">Commentary</a></li>
				<li><a href="#statistics">Statistics</a></li>
				<li><a href="#gallery-tab">Gallery</a></li>
			</ul>
			  
			<p ></p>
			<div id="scoresheet">
			 <div class="clear"></div>
			 <h2><?php echo $match_info['team1']; ?> vs <?php echo $match_info['team2']; ?></h2>
			<?php echo $match_info['maximum_overs']; ?> Overs Match - <?php echo date('d M, Y H:i',strtotime($match_info['start_time'])); ?><br>
			<b>Venue:</b> <?php echo $match_info['venue']?><?php if(!empty($match_info['address']))  echo ', '.$match_info['address']?><br>
			<b>Captain <?php echo $match_info['team1']?>: </b> <?php echo $match_info['captain_team1'];  ?><br/>
			<b>Captain <?php echo $match_info['team2']?>: </b> <?php echo $match_info['captain_team2'];  ?><br/>
			<b>1st Umpire:</b> <?php echo $match_info['umpire1_name'];?><br>
			<?php if(!empty($match_info['umpire2_name'])): ?>
				<b>2nd Umpire:</b> <?php echo $match_info['umpire2_name'];?><br>
			<?php endif; ?>
			<?php if(!empty($match_info['umpire3_name'])): ?>
				<b>3rd Umpire:</b> <?php echo $match_info['umpire3_name'];?><br>
			<?php endif; ?>
			<?php if(!empty($match_info['referee_name'])): ?>
				<b>Match Referee:</b> <?php echo $match_info['referee_name'];?><br>
			<?php endif; ?>
			<?php if(!empty($match_info['completely_abandon_due_to'])): ?>
				<b>Match Abandoned due to :</b> <?php echo $match_info['completely_abandon_due_to'];?><br>
			<?php endif; ?>
			
			<?php if(!empty($match_info['winning_team_id'])){ ?>
				<b>Match Result:</b>
				<?php if($match_info['winning_team_id']>0){
					echo $match_info['winning_team_name'].' Wins ';
					if(!empty($match_info['completely_abandon_due_to']) && $match_info['completely_abandon_due_to']=='Forfeit by One Team') echo ' by Forfeit';
					if(!empty($match_info['completely_abandon_due_to']) && $match_info['completely_abandon_due_to']=='Late Arrival of One Team') echo ' by Late Arrival';	
				}else{
					if($match_info['winning_team_id'] == -1) echo 'Match Tied';
					if($match_info['winning_team_id'] == -2) echo 'Match Abandoned';
					if($match_info['winning_team_id'] == -3) echo 'Decision Pending Review';
				}
				echo '<br/>';
			} ?>
			
			<?php if(!empty($match_info['match_result_text'])): ?>
				<b>Result:</b> <?php echo $match_info['match_result_text']; ?>	<br>
			<?php endif; ?>
			
			<?php if(!empty($match_info['man_of_match_player_name'])): ?>
				<b>Man of the Match:</b> <?php echo $match_info['man_of_match_player_name'];?><br>
			<?php endif; ?>
			
			<?php if(!empty($match_info['team_won_toss']) && !empty($match_info['toss_won_decision'])){ ?>
			
				<?php echo $match_info['team_won_toss']; ?> Won the toss and elected to <?php echo $match_info['toss_won_decision']; ?><br>
				<?php echo $match_info['team_bat_first']; ?> score: <?php echo $match_info['team1_score']; ?> runs (<?php echo $match_info['team1_wickets']; ?> wickets) in <?php if(strpos($match_info['team1_overs'],'.6')!== false) echo ceil($match_info['team1_overs']);else echo $match_info['team1_overs']; ?> overs<br>
				<?php echo $match_info['team_bat_second']; ?> score: <?php echo $match_info['team2_score']; ?> runs (<?php echo $match_info['team2_wickets']; ?> wickets) in <?php if(strpos($match_info['team2_overs'],'.6')!== false) echo ceil($match_info['team2_overs']);else echo $match_info['team2_overs']; ?> overs<br>
				
				<?php echo $match_info['highlights_text']; ?>	
			 
				<div class="clear"><br/></div>
			 
				<p><h3><?php echo $match_info['team_bat_first']; ?> Innings </h3></p>
				<table id="table-list" class="white-box">
				<tr><th>Batsman</th><th>How Out</th><th>Fielder</th><th>Bowler</th><th>Runs</th><th>Balls</th><th>4s</th><th>6s</th><th>SR</th></tr>
				<?php while($row = mysqli_fetch_assoc($rs_batting1)): ?>
				<?php if(empty($row['batsman_id']) || ($row['balls_played'] == 0 && empty($row['how_out'])) ) continue; ?>
				<tr>
					<td style="text-align:left;">
					<a href="<?=WWW?>individual-detail-<?=$row['batsman_id']?>-<?=friendlyURL($batting1_data[$row['batsman_id']])?>.html#activity-tab" title="<?php echo $batting1_data[$row['batsman_id']]; ?> Profile ">
					<?php echo $batting1_data[$row['batsman_id']];  ?></a></td>
					<td><?php echo $row['how_out']; ?></td>
					<td>
					<a href="<?=WWW?>individual-detail-<?=$row['fielder_id']?>-<?=friendlyURL($batting2_data[$row['fielder_id']])?>.html#activity-tab" title="<?php echo $batting2_data[$row['fielder_id']]; ?> Profile ">
					<?php echo $batting2_data[$row['fielder_id']];  ?></a>
					</td>
					<td>
					<a href="<?=WWW?>individual-detail-<?=$row['bowler_id']?>-<?=friendlyURL($batting2_data[$row['bowler_id']])?>.html#activity-tab" title="<?php echo $batting2_data[$row['bowler_id']]; ?> Profile ">
					<?php echo $batting2_data[$row['bowler_id']];  ?></a>
					</td>
					<td><?php echo $row['runs_scored']; ?></td>
					<td><?php echo $row['balls_played']; ?></td>
					<td><?php echo $row['fours_scored']; ?></td>
					<td><?php echo $row['sixes_scored']; ?></td>
					<td><?php if($row['balls_played']==0) echo 0;else { $sr =  round(($row['runs_scored']/$row['balls_played'])*100,2); echo $sr; } ?><?php if(strpos($sr,'.') === false) echo '.00'; ?></td>
				</tr>
				<?php $total_runs+=$row['runs_scored'];$total_balls+=$row['balls_played'];$total_fours+=$row['fours_scored'];$total_sixes+=$row['sixes_scored'];  ?>
				<?php endwhile; ?>
				<tr><td colspan="3"></td><td style="text-align:right;font-weight:700;">Total:</td><td><?php echo $total_runs; ?></td><td><?php echo $total_balls; ?></td><td><?php echo $total_fours; ?></td><td><?php echo $total_sixes; ?></td></tr>
				</table>
				
				<div class="clear"><br/></div>
				<b>Fall of wickets: </b><?php $fow1_str = ''; if(mysqli_num_rows($rs_fow1)): ?>
					<?php while($row = mysqli_fetch_assoc($rs_fow1)): ?>
						<?php $fow1_str.= $row['wicket_no'].'-'.$row['score'].' ('. $batting1_data[$row['batsman_id']].', '.$row['overs'].' ov), '; ?>
					<?php endwhile; ?>
					<?php echo rtrim($fow1_str,', '); ?>
				<?php endif; ?>
				
				<div class="clear"><br/></div>
				
				<table id="table-list" class="white-box">
				<tr><th>Bowler</th><th>Overs</th><th>Maidens</th><th>Runs</th><th>Wides</th><th>No Balls</th><th>Wickets</th><th>Economy</th></tr>
				<?php while($row = mysqli_fetch_assoc($rs_bowling1)): ?>
				<?php if(empty($row['bowler_id'])) continue; ?>
				<?php $balls_arr = explode('.',$row['overs_bowled']); $balls_bowled  = ($balls_arr[0]*6)+$balls_arr[1];
				$economy_rate = round($row['runs_conceded']/($balls_bowled/6),2);	?>
				<tr>
					<td style="text-align:left;">
					<a href="<?=WWW?>individual-detail-<?=$row['bowler_id']?>-<?=friendlyURL($batting2_data[$row['bowler_id']])?>.html#activity-tab" title="<?php echo $batting2_data[$row['bowler_id']]; ?> Profile ">
					<?php echo $batting2_data[$row['bowler_id']];  ?></a>
					</td>
					<td><?php echo round($row['overs_bowled'],1); ?></td>
					<td><?php echo $row['maidens_bowled']; ?></td>
					<td><?php echo $row['runs_conceded']; ?></td>
					<td><?php echo $row['wide_balls_bowled']; ?></td>
					<td><?php echo $row['no_balls_bowled']; ?></td>
					<td><?php echo $row['wickets_taken']; ?></td>
					<td><?php echo $economy_rate; ?><?php if(strpos($economy_rate,'.') === false) echo '.00'; ?></td>
				</tr>
				<?php endwhile; ?>
				</table>
				<div class="clear"><br/></div>
				<b>Extras: </b>
				Leg Byes <?php echo $match_info['team1_leg_byes'] ?>, Byes <?php echo $match_info['team1_byes'] ?>, Wides <?php echo $match_info['team1_wides'] ?>, No Balls <?php echo $match_info['team1_no_balls'] ?><br/>
				<b>Total Extras: </b> <?php echo $match_info['team1_leg_byes']+$match_info['team1_byes']+$match_info['team1_wides']+$match_info['team1_no_balls']; ?><br/>
				<b>Total:</b> <?php echo $match_info['team1_score'] ?> (<?php echo $match_info['team1_wickets']; ?> wickets) in <?php if(strpos($match_info['team1_overs'],'.6')!== false) echo ceil($match_info['team1_overs']);else echo $match_info['team1_overs']; ?> overs
			
				<div class="clear"><br/></div>
				<?php $total_runs=0;$total_balls=0;$total_fours=0;$total_sixes=0; ?>
				<p><h3><?php echo $match_info['team_bat_second']; ?> Innings </h3></p>
				<table id="table-list" class="white-box">
				<tr><th>Batsman</th><th>How Out</th><th>Fielder</th><th>Bowler</th><th>Runs</th><th>Balls</th><th>4s</th><th>6s</th><th>SR</th></tr>
				<?php while($row = mysqli_fetch_assoc($rs_batting2)): ?>
				<?php if(empty($row['batsman_id']) || ($row['balls_played'] == 0 && empty($row['how_out'])) ) continue; ?>
				<tr>
					<td style="text-align:left;">
					<a href="<?=WWW?>individual-detail-<?=$row['batsman_id']?>-<?=friendlyURL($batting2_data[$row['batsman_id']])?>.html#activity-tab" title="<?php echo $batting2_data[$row['batsman_id']]; ?> Profile ">
					<?php echo $batting2_data[$row['batsman_id']]; ?></a>
					</td>
					<td><?php echo $row['how_out']; ?></td>
					<td>
					<a href="<?=WWW?>individual-detail-<?=$row['fielder_id']?>-<?=friendlyURL($batting1_data[$row['fielder_id']])?>.html#activity-tab" title="<?php echo $batting1_data[$row['fielder_id']]; ?> Profile ">
					<?php echo $batting1_data[$row['fielder_id']]; ?></a>
					</td>
					<td>
					<a href="<?=WWW?>individual-detail-<?=$row['bowler_id']?>-<?=friendlyURL($batting1_data[$row['bowler_id']])?>.html#activity-tab" title="<?php echo $batting1_data[$row['bowler_id']]; ?> Profile ">
					<?php echo $batting1_data[$row['bowler_id']]; ?></a>
					</td>
					<td><?php echo $row['runs_scored']; ?></td>
					<td><?php echo $row['balls_played']; ?></td>
					<td><?php echo $row['fours_scored']; ?></td>
					<td><?php echo $row['sixes_scored']; ?></td>
					<td><?php if($row['balls_played']==0) echo 0;else { $sr =  round(($row['runs_scored']/$row['balls_played'])*100,2); echo $sr; } ?><?php if(strpos($sr,'.') === false) echo '.00'; ?></td>
				</tr>
				<?php $total_runs+=$row['runs_scored'];$total_balls+=$row['balls_played'];$total_fours+=$row['fours_scored'];$total_sixes+=$row['sixes_scored'];  ?>
				<?php endwhile; ?>
				<tr><td colspan="3"></td><td style="text-align:right;font-weight:700;">Total:</td><td><?php echo $total_runs; ?></td><td><?php echo $total_balls; ?></td><td><?php echo $total_fours; ?></td><td><?php echo $total_sixes; ?></td></tr>
				</table>
				
				<div class="clear"><br/></div>
				<b>Fall of wickets: </b><?php $fow1_str = ''; if(mysqli_num_rows($rs_fow2)): ?>
					<?php while($row = mysqli_fetch_assoc($rs_fow2)): ?>
						<?php $fow1_str.= $row['wicket_no'].'-'.$row['score'].' ('. $batting2_data[$row['batsman_id']].', '.$row['overs'].' ov), '; ?>
					<?php endwhile; ?>
					<?php echo rtrim($fow1_str,', '); ?>
				<?php endif; ?>
				
				<div class="clear"><br/></div>
				
				<table id="table-list" class="white-box">
				<tr><th>Bowler</th><th>Overs</th><th>Maidens</th><th>Runs</th><th>Wides</th><th>No Balls</th><th>Wickets</th><th>Economy</th></tr>
				<?php while($row = mysqli_fetch_assoc($rs_bowling2)): ?>
				<?php if(empty($row['bowler_id'])) continue; ?>
				<?php $balls_arr = explode('.',$row['overs_bowled']); $balls_bowled  = ($balls_arr[0]*6)+$balls_arr[1];
				$economy_rate = round($row['runs_conceded']/($balls_bowled/6),2);	?>
				<tr>
					<td style="text-align:left;">
					<a href="<?=WWW?>individual-detail-<?=$row['bowler_id']?>-<?=friendlyURL($batting1_data[$row['bowler_id']])?>.html#activity-tab" title="<?php echo $batting1_data[$row['bowler_id']]; ?> Profile ">
					<?php echo $batting1_data[$row['bowler_id']]; ?></a>
					</td>
					<td><?php echo $row['overs_bowled']; ?></td>
					<td><?php echo $row['maidens_bowled']; ?></td>
					<td><?php echo $row['runs_conceded']; ?></td>
					<td><?php echo $row['wide_balls_bowled']; ?></td>
					<td><?php echo $row['no_balls_bowled']; ?></td>
					<td><?php echo $row['wickets_taken']; ?></td>
					<td><?php echo $economy_rate; ?><?php if(strpos($economy_rate,'.') === false) echo '.00'; ?></td>
				</tr>
				<?php endwhile; ?>
				</table>
				<div class="clear"><br/></div>
				<b>Extras:</b>
				Leg Byes <?php echo $match_info['team2_leg_byes'] ?>, Byes <?php echo $match_info['team2_byes'] ?>, Wides <?php echo $match_info['team2_wides'] ?>, No Balls <?php echo $match_info['team2_no_balls'] ?><br>
				<b>Total Extras: </b> <?php echo $match_info['team2_leg_byes']+$match_info['team2_byes']+$match_info['team2_wides']+$match_info['team2_no_balls']; ?><br/>
				<b>Total:</b> <?php echo $match_info['team2_score'] ?> (<?php echo $match_info['team2_wickets']; ?> wickets) in <?php if(strpos($match_info['team2_overs'],'.6')!== false) echo ceil($match_info['team2_overs']);else echo $match_info['team2_overs']; ?> overs
				
			<?php } ?>
				</div>  
				
				<div id="commentary">
					<h2>Commentary</h2>
					
					
					<ul id="menu2" class="menu">
						<li class="active"><a href="#innings1-commentary"><?php echo $match_info['team_bat_first']; ?> Innings</a></li>
						<li ><a href="#innings2-commentary"><?php echo $match_info['team_bat_second']; ?> Innings</a></li>
					</ul>
					<div class="clear" style="height:5px;"></div>
					<div id="innings1-commentary">
					</div>
					<div id="innings2-commentary">
					</div>
				</div>
				
				<div id="statistics">
					<ul id="menu3" class="menu">
					<h2>Statistics</h2>
						<li class="active"><a href="#innings1-statistics"><?php echo $match_info['team_bat_first']; ?> Innings</a></li>
						<li ><a href="#innings2-statistics"><?php echo $match_info['team_bat_second']; ?> Innings</a></li>
					</ul>
					<div class="clear" ><br></div>
					<div id="innings1-statistics">
						<div class="clear" ><br></div>
						<div id="bar1_chart" ></div>
						<div class="clear" ><br></div>
						<div id="runs_type1_chart" ></div>
						<div class="clear" ><br></div>
						<div id="line1_chart"></div>
						<div class="clear" ><br></div>
						<div id="run_rate1_chart"></div>
						<div class="clear" ><br></div>
						<div id="extras1_chart"></div>
						<div class="clear" ><br></div>
						<div id="how_out1_chart"></div>
						<div class="clear" ><br></div>
					</div>
					
					<div class="clear" ><br></div>
					
					<div id="innings2-statistics">
						<div class="clear" ><br></div>
						<div id="bar2_chart" ></div>
						<div class="clear" ><br></div>
						<div id="runs_type2_chart" ></div>
						<div class="clear" ><br></div>
						<div id="line2_chart"></div>
						<div class="clear" ><br></div>
						<div id="run_rate2_chart"></div>
						<div class="clear" ><br></div>
						<div id="extras2_chart"></div>
						<div class="clear" ><br></div>
						<div id="how_out2_chart"></div>
						<div class="clear" ><br></div>
					</div>
					
					<div class="clear" ><br></div>
				</div>
				
				<div id="gallery-tab">
					<div class="clear"><br/></div>
					<h3>Pictures</h3>
					<div id="gallery"></div>
					<div class="clear"><br/></div>
					<h3>Videos</h3>
					<div class="clear"><br/></div>
					<?php if(empty($match_videos)): ?> No Videos <?php endif; ?>
					<?php for($i=0;$i<count($match_videos);$i++): 
					
					?>
					<?php if($match_videos[$i]['video_type'] == 'embed'): ?>
						<?php echo $match_videos[$i]['embed_code']; ?>
					<?php else: ?>
						<?php $video_path = WWW.'video-uploads/'.$match_videos[$i]['video_file']; ?>
						<video width="540" height="300" controls>
						 <source src="<?php echo $video_path; ?>" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
						<source src="<?php echo $video_path; ?>" type='video/ogg; codecs="theora, vorbis"'>
						<source src="<?php echo $video_path; ?>" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
						<source src="<?php echo $video_path; ?>" type='video/webm; codecs="vp8, vorbis"'>
						</video>
					<?php endif; ?>
						
						<div class="clear"></div>
						<b><?php echo $match_videos[$i]['video_title']; ?></b>
						<div class="clear"><br/></div>
					<?php endfor; ?>
					
					<div class="clear"><br/></div>
					<div id="polls"></div>
				</div>
				
			</div>  
			</div>
		</div>
		<!--<div class="rightbar"><?php //include('common/right-panel.php');?></div>-->
		<div class="clear"></div>
	</div>
	
<link rel="stylesheet" href="<?php echo WWW; ?>js/colorbox/colorbox.css" />
<script src="<?php echo WWW; ?>js/colorbox/jquery.colorbox-min.js"></script>	

<script type="text/javascript">
$(document).ready(function(){
	getCommentary1 = function(p){
		$.ajax({
			url: "<?php echo WWW; ?>get_commentary.php?t=<?php echo time(); ?>",
			type:"POST",
			data:"match_id=<?php echo $match_id; ?>&team_id=<?php echo $match_info['batting_team1']; ?>&scoresheet=1&team=1&p="+p,
			success: function(data) {
				$("#innings1-commentary").html(data);
			}
		});
	}
	
	getCommentary2 = function(p){
		$.ajax({
			url: "<?php echo WWW; ?>get_commentary.php?t=<?php echo time(); ?>",
			type:"POST",
			data:"match_id=<?php echo $match_id; ?>&team_id=<?php echo $match_info['batting_team2']; ?>&scoresheet=1&team=2&p="+p,
			success: function(data) {
				$("#innings2-commentary").html(data);
			}
		});
	}
	
	getCommentary1(1);
	getCommentary2(1);
	setInterval('getCommentary1(1)', 60000);
	setInterval('getCommentary2(1)', 60000);
	
	loadGallery = function(){
		$.ajax({
			url: "<?php echo WWW; ?>add_match_gallery.php?t=<?php echo time(); ?>",
			type:"POST",
			data:"match_id=<?php echo $match_id; ?>&action=get&scoresheet=1",
			success: function(data) {
				data = $.parseJSON(data);
				if(data.error == ''){
					$("#gallery").html(data.str);
					if(data.str == '') $("#gallery").html("<br>No Pictures");
					$(".match-gallery-link").colorbox({rel:'match-gallery-link'});
				}
			}
		});
	}
	
	/*loadPolls = function(){
		$.ajax({
			url: "<?php echo WWW; ?>get_polls.php?t=<?php echo time(); ?>",
			type:"POST",
			data:"t_id=<?php echo $match_info['tournament_id']; ?>&action=get&scoresheet=1",
			success: function(data) {
				data = $.parseJSON(data);
				if(data.error == '' && data.str != '' ){
					$("#polls").html('<h3>Polls</h3><div class="clear"><br/></div>'+data.str);
				}
			}
		});
	}
	
	submitPoll = function(id){
		if(!$("input[name=poll_option_"+id+"]").is(":checked")){
			alert('Select one poll option');return;
		}
		
		var option_id = $('input[name=poll_option_'+id+']:checked').val();
		$.ajax({
			url: "<?php echo WWW; ?>get_polls.php?t=<?php echo time(); ?>",
			type:"POST",
			data:"poll_id="+id+"&option_id="+option_id+"&action=submit",
			success: function(data) {
				data = $.parseJSON(data);
				if(data.error == ''){
					$("#poll_div_"+id).html(data.str);
				}
			}
		});
	}
	
	loadPolls();
	*/
	
	
	loadGallery();
	
});
</script>
<?php include('common/footer.php'); ?>