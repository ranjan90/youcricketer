<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	<div class="middle">
		<h1>Profile Information</h1>

		<? 	$user_id = $row_user['id'];
			if(isset($_POST) && !empty($_POST) && $_POST['action'] == 'submit'){
				mysqli_query($conn,"update users set other_information = '".$_POST['overall_experience']."', currently_playing = '".$_POST['currently_playing']."', no_of_playing = '".$_POST['no_of_playing']."' where id = '$user_id'");
				echo '<div id="success"><b>Success : </b>Information updated ... !</div><br><br>';
			}
			if(isset($_GET) && $_GET['action'] == 'delete' && $_GET['type'] == 'experiences'){
				mysqli_query($conn,"delete from experiences where id = '".$_GET['id']."'");
				echo '<div id="success"><b>Success : </b>Information updated ... !</div><br><br>';
				
			}
			if(isset($_POST) && $_POST['action'] == 'add-exp'){
				$country_id 		= $_POST['country_id'];
				$organization 		= $_POST['organization'];
				$job_title 	 		= $_POST['job_title'];
				if(!empty($_POST['start_date'])){
				$from_date 	 		= $_POST['start_date'];
				}
				else {
					$from_date = date("Y-m-d");
				}
				
				if(!empty($_POST['end_date'])){
					$to_date 	 		= $_POST['end_date'];
				}
				else {
					$to_date = date("Y-m-d");
				}
				if($to_date < $from_date){ 
					echo '<div id="error"><b>Error  : </b>To date should be less than From date ... !</div><br><br>';
					
				}
				else {
				if(!empty($country_id) && !empty($organization) && !empty($job_title)){
					$rs_chk = mysqli_query($conn,"select * from experiences where user_id = '$user_id' and country_id = '$country_id' and organization = '$organization' and job_title = '$job_title'");
					if(mysqli_num_rows($rs_chk) == 0){ 
						$query = "insert into experiences (user_id, country_id, organization, job_title, from_date, to_date) 
							values ('$user_id','$country_id', '$organization', '$job_title','$from_date', '$to_date'); ";
						mysqli_query($conn,$query);					
						echo '<div id="success"><b>Success : </b>Information updated ... !</div><br><br>';
					}
				}
			}
}
			if(isset($_POST) && $_POST['action'] == 'edit-exp'){
				$organization 		= $_POST['organization'];
				$job_title 	 		= $_POST['job_title'];
				$from_date 	 		= $_POST['start_date'];
				$to_date 	 		= $_POST['end_date'];
				$id 				= $_POST['recordId'];
			if($to_date < $from_date){ 
					echo '<div id="error"><b>Error  : </b>End/t date should be less than From date ... !</div><br><br>';
					
				}
				else {
				$query = "update experiences set organization = '$organization', job_title = '$job_title', from_date = '$from_date', to_date = '$to_date' where id = '$id'";
				mysqli_query($conn,$query);					
				echo '<div id="success"><b>Success : </b>Information updated ... !</div><br><br>';
			}
}
			?>

		<div class="white-box content" id="dashboard">
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
			<? include('common/profile-navigation.php');?>
			<form method="post" action="" enctype="multipart/form-data">
				<input type="hidden" name="action" value="submit">
				<fieldset>
					<legend>Overall Experience</legend>
					<div class="form-box">
						<label>Currently Active in your individual role </label>
					</div>
					<div class="form-box">
						<select name="currently_playing" style="float:left; margin-left:20px;">
							<option value="1" <?=($row_user['currently_playing'] == '1')?'selected="selected"':'';?>> Yes </option>
							<option value="0" <?=($row_user['currently_playing'] == '0')?'selected="selected"':'';?>> No </option>
						</select>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Years of Experience</label>
					</div>
					<div class="form-box">
						<select name="no_of_playing" class="validate[required]" style="float:left; margin-left:20px;">
							<option <?=(empty($row_user['no_of_playing']))?'selected="selected"':'';?> value=""></option>
							<? for($x = 1; $x <= 100; $x++){ ?>
							<option <?=($x == $row_user['no_of_playing'])?'selected="selected"':'';?> value="<?=$x?>"><?=$x?></option>
							<? } ?>
						</select>
					</div>
					<div class="clear"></div>
					<div class="form-box" style="width:100%">
						<label>Describe your experience based on your listed individual role or any other roles you had</label>
					</div>
					<div class="form-box" style="width:100%">
						<textarea name="overall_experience" style="width:80%; height:100px;"><?=$row_user['other_information']?></textarea>
					</div>
					<div class="clear"></div>
				</fieldset>
				<fieldset>
					<div class="form-box " style="width:100%">
						<label>Countries traveled related to your individual role or any type of work or education</label>
						<a style="color:#fff;" class="no-bg right popup-link" href="#add-exp"><img src="<?=WWW?>images/icons/add.png"> Add</a>
					</div>
					<div class="form-box" style="width:100%">
						<div class="form-box" style="width:100%;">
						<div class="space10"></div>
				        <table width="100%" border="1" style="border-collapse:collapse;" id="">
				        	<tr>
				        		<th></th>
				        		<th class="tleft">Country</th>
				        		<th class="tleft">Organization</th>
				        		<th class="tleft">Job Title</th>
				        		<th class="tleft">From Date</th>
				        		<th class="tleft">To Date</th>
				        	</tr>
				        <? 	$rs_msg = mysqli_query($conn,"select * from experiences where user_id = '".$row_user['id']."' order by to_date desc "); 
				        	if(mysqli_num_rows($rs_msg) == 0){
				        		?>
				        		<tr>
				        			<td colspan="3"><td>
				        		</tr>
				        		<?
				        	}else{
					       		while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
					       		<tr title="Click for detail">
					       			<td width="10%">
					       				<a class="popup-link no-bg no-padding" href="#edit-exp" id="<?=$row_msg['id']?>"><img src="<?=WWW?>images/icons/edit.png"></a>
					       				<a class="no-bg no-padding" href="<?=WWW?>profile-information-step-6.html?action=delete&type=experiences&id=<?=$row_msg['id']?>"><img src="<?=WWW?>images/icons/delete.png"></a>
					       			</td>
					       			<td><?=get_combo('countries','name',$row_msg['country_id'],'','text')?></td>
					       			<td><?=$row_msg['organization']?></td>
					       			<td><?=$row_msg['job_title']?></td>
					       			<td><?=date_converter($row_msg['from_date'])?></td>
					       			<td><?=date_converter($row_msg['to_date'])?></td>
					       		</tr>
					       	<?  }
					       	} ?>
				        </table>
					</div>
					</div>
					<div class="clear"></div>			
				</fieldset>
				<table width="100%" class="profile-action">
						<tr>
							<td width="33%">
								<a id="cancel" class="submit-login" style="position:relative; left:-150px; top:10px;" href="<?=WWW?>dashboard.html">Cancel</a>
							</td>
							<td width="33%">
								<input id="update" type="submit" value=" Update " style="left:-100px; position:relative;" class="submit-login">
							</td>
							<td width="33%">
								<a class="submit-login" style="position:relative;left:0px;  top:10px;" href="<?=WWW?>profile-information-step-7.html">Skip</a>
							</td>
						</tr>
					</table>	
			</form>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<style>
#city select{float:left; margin-left:20px;}
</style>
<script src="<?php echo WWW?>js/jquery-1.8.0.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

	$('#dashboard .black-box h2').click(function(){
		$('#dashboard .black-box ul').css('display','none');
		var id = this.id;
		$('#'+id+'-box ul').css('display','block');
	});

	$('a.popup-link').click(function(){
		var link = $(this).attr('href');
		if(link == '#edit-exp'){
			var id = this.id;
			$.ajax({type	: 'POST', 
				url		: WWW + 'includes/get-record.php', 
				data	: ({table:'experiences',field:'*',id:id}),
				success	: function(msg){
					msg1 = msg.split('___');
					
					$(link+' #country').html(msg1[0]);
					$(link+' input[name=organization]').val(msg1[1]);
					$(link+' input[name=job_title]').val(msg1[2]);
					$(link+' input[name=start_date]').val(msg1[3]);
					$(link+' input[name=end_date]').val(msg1[4]);
					$(link+' #recordId').val(id);
				}
			});
		}
		$(link).removeClass('hide');
	});
	$('.close-popup').click(function(){
		$('.popup').addClass('hide');
	});
});
</script>
<div id="add-exp" class="popup hide"style="margin-top:-500px;">
	<h1>
		Add your Experience
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h1>
	<form method="post" action="">
		<input type="hidden" name="action" value="add-exp">
		<fieldset>
			<div class="form-box">
				<?=get_combo('countries','name','','country_id','','','Select Country')?>
				<br>
				<input type="text" name="organization" class="validate[required] input-login margin-10" placeholder="Type Organization">
				<br>
				<input type="text" name="job_title" class="validate[required] input-login margin-10" placeholder="Type Job Title">
				<br>
				<input type="text" name="start_date" id="start_date_add" class="validate[required] input-login margin-10" placeholder="Start Date">
				<br>
				<input type="text" name="end_date" id="end_date_add" class="validate[required] input-login margin-10" placeholder="End Date">
				<br><br>
				<input type="submit" class="submit-login margin-top-5" value="Save">
			</div>
		</fieldset>
	</form>
</div>
<div id="edit-exp" class="popup hide"style="margin-top:-500px;">
	<h1>
		Update your Experience
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h1>
	<form method="post" action="">
		<input type="hidden" name="action" value="edit-exp">
		<input type="hidden" name="recordId" id="recordId" value="">
		<fieldset>
			<div class="form-box">
				<label id="country"></label>
				<br>
				<input type="text" name="organization" class="validate[required] input-login margin-10" placeholder="Type Organization">
				<br>
				<input type="text" name="job_title" class="validate[required] input-login margin-10" placeholder="Type Job Title">
				<br>
				<input type="text" name="start_date"id="start_date_edit"  class="validate[required] input-login margin-10" placeholder="<?=date('Y')-1?>-<?=date('m')?>-<?=date('d');?>">
				<br>
				<input type="text" name="end_date" id="end_date_edit" class="validate[required] input-login margin-10" placeholder="<?=date('Y')-1?>-<?=date('m')?>-<?=date('d');?>">
				<br><br>
				<input type="submit" class="submit-login margin-top-5" value="Save">
			</div>
		</fieldset>
	</form>
</div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.9.1.js"></script>
  <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

 <script>
  $(function() {
    $( "#start_date_add" ).datepicker({ dateFormat: "yy-mm-dd"});
  });

  $(function() {
	    $( "#end_date_add" ).datepicker({ dateFormat: "yy-mm-dd"});
	  });
  
  $(function() {
	    $( "#end_date_edit" ).datepicker({ dateFormat: "yy-mm-dd"});
	  });

  $(function() {
	    $( "#start_date_edit" ).datepicker({ dateFormat: "yy-mm-dd"});
	  });
  </script>
<?php include('common/footer.php'); ?>