<?php include('common/header.php'); ?>
<style>
#page-left{width:65%;}
#page-right{width:34%;}
.match{margin: 4px; padding: 8px;}
h3 a{font-size:12px;}
</style>
<?php $row = get_record_on_id('countries', $_GET['id']);?>
	<div class="middle">
		<h1>World Cup Cricket 2015 - <?php echo $row['name']?></h1>
		<div class="white-box content detail" style="width:100%;">
			<div id="page-left" class="left">
				<?php $rowDetail = mysqli_fetch_assoc(mysqli_query($conn,"select * from series_profiles where series_id = '1' and country_id = '".$_GET['id']."'"));?>
				<?php echo $rowDetail['content'];?>
			</div>
			<div id="page-right" class="right" style="border-left:1px solid #ccc; padding-left:3px;">
				<h3>Past Performance</h3>
				<?php echo $rowDetail['past_performance']?>
				<hr>
				<h3>World Cup 2015 Matches</h3>
				<?php $query = "select * from series_matches where (country_1 = '".$row['id']."' or country_2 = '".$row['id']."') order by match_date asc";?>
				<?php $matches = mysqli_query($conn,$query);?>
				<?php while($rowM = mysqli_fetch_assoc($matches)){ ?>
					<div class="match">
						<h3>
							<?php if(empty($rowM['title'])) {
				        		$row1 = get_record_on_id('countries', $rowM['country_1']);
				        		$row2 = get_record_on_id('countries', $rowM['country_2']);
				        		$title = '<a href="'.WWW.'world-cup-cricket-2015-country-profile-'.$row1['id'].'-'.friendlyURL($row1['name']).'.html" title="Click here to view Country Profile">'.$row1['name'].'</a> vs <a href="'.WWW.'world-cup-cricket-2015-country-profile-'.$row2['id'].'-'.friendlyURL($row2['name']).'.html" title="Click here to view Country Profile">'.$row2['name'].'</a> - Pool '.$rowM['pool'].' ('.date('d-M-Y', strtotime($rowM['match_date'])).')';
				        		?>
				        	<?php }else{ ?>
				        	<?php $title = $rowM['title'].' ('.date('d-M-Y', strtotime($rowM['match_date'])).')';?>
				        	<?php } ?>
				        	<?php echo $title;?>
				        	<?php
				        	$blogRow = mysqli_fetch_assoc(mysqli_query($conn,"select * from blog_articles where series_match_id = '".$rowM['id']."'"));
				        	$blogId = $blogRow['id'];
				        	?>
				        	<a class="right" href="<?php echo WWW;?>blog-detail-<?php echo $blogId;?>-<?php echo friendlyURL(strip_tags($title));?>.html">Word Cup Blogs/Discussion Contest</a>				        		
						</h3>
						<p>Match Date : <?php echo date('d-M-Y', strtotime($rowM['match_date']));?></p>
						<p>GMT Time : <?php echo $rowM['gmt_time'];?></p>
						<p>IST Time : <?php echo $rowM['ist_time'];?></p>
						<p>Venue : 
							<?php
					        	$venue = $rowM['match_ground'].', '.$rowM['match_city'].', ';
		          				$row_country= get_record_on_id('countries', $rowM['match_country_id']);
		          				echo $venue.$row_country['name'];
				        	?>
						</p>
						<div class="clear"></div>
					</div>
			        <?php } ?>
			</div>
		</div>		
		<div class="clear"></div>
	</div>

<?php include('common/footer.php'); ?>