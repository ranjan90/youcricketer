<?php include('includes/configuration.php'); 
extract($_POST);
$scores_array = array(1=>'one',2=>'two',3=>'three',4=>'four',5=>'five',6=>'six');
$commmentary_str = '';
$row_count = 0;

$paging_str = '';
$rec_page = 48;
if(isset($_POST['p']) && is_numeric($_POST['p'])) $page = $_POST['p'];else $page = 1;
$start_rec = ($page-1)*$rec_page;

$sql = "SELECT count(*) as records_count
from tournament_match_commentary as tmc INNER JOIN users as u1 on tmc.batsman_id = u1.id INNER JOIN users as u2 on tmc.bowler_id = u2.id
 LEFT JOIN users as u3 on tmc.fielder_id = u3.id INNER JOIN companies as c ON tmc.team_id = c.id
 WHERE tmc.match_id = $match_id and tmc.team_id = $team_id ";
$rs_c = mysqli_query($conn,$sql);
$row_c = mysqli_fetch_assoc($rs_c);
$total_rows = $row_c['records_count'];
$total_pages = ceil($total_rows/$rec_page);
if($total_pages>1){
	$paging_str = '<div class="clear"></div> <div id="pagination-top" style="border-bottom:none;"><div class="pagin">';
	for($i=1;$i<=$total_pages;$i++){
		if($page == $i){
			$paging_str.='<span>'.$i.'</span>';
		}else{
			if($scoresheet == 1 && $team == 1){
				$paging_str.='<a href="javascript:;" onClick="getCommentary1('.$i.')">'.$i.'</a>';
			}elseif($scoresheet == 1 && $team == 2){
				$paging_str.='<a href="javascript:;" onClick="getCommentary2('.$i.')">'.$i.'</a>';
			}else{
				$paging_str.='<a href="javascript:;" onClick="getCommentary('.$i.')">'.$i.'</a>';
			}
		}
	}
	$paging_str.= '</div></div> <div class="clear"></div>';
}
//var_dump($_POST);exit;

$sql = "SELECT tmc.*,c.company_name,u1.f_name as batsman_f_name,u1.last_name as batsman_last_name ,u2.f_name as bowler_f_name,u2.last_name as bowler_last_name 
,u3.f_name as fielder_f_name,u3.last_name as fielder_last_name
from tournament_match_commentary as tmc INNER JOIN users as u1 on tmc.batsman_id = u1.id INNER JOIN users as u2 on tmc.bowler_id = u2.id
 LEFT JOIN users as u3 on tmc.fielder_id = u3.id INNER JOIN companies as c ON tmc.team_id = c.id
 WHERE tmc.match_id = $match_id and tmc.team_id = $team_id ORDER BY tmc.team_overs DESC,tmc.id DESC LIMIT $start_rec,$rec_page";
$rs_commentary = mysqli_query($conn,$sql);
if(mysqli_num_rows($rs_commentary)>0){
	$num_rows = mysqli_num_rows($rs_commentary);
	while($row = mysqli_fetch_assoc($rs_commentary)){
		if(strpos($row['team_overs'],'.6') !== false && $row['ball_type'] == 'legal_ball') {
			
			$batsman_str = '';
			$overs = intval($row['team_overs'])+1;
			$sql = "SELECT  tmc.id,tmc.batsman_id ,tmc.batsman_score,tmc.batsman_balls,tmc.batsman_sixes,tmc.batsman_fours,
			u1.f_name as batsman_f_name,u1.last_name as batsman_last_name 
			from tournament_match_commentary as tmc INNER JOIN users as u1 on tmc.batsman_id = u1.id 
			WHERE tmc.match_id = $match_id and tmc.team_id = $team_id and tmc.batsman_out='no' and team_overs<$overs ORDER BY tmc.id DESC LIMIT 1";
			$rs_batsman = mysqli_query($conn,$sql);
			if(mysqli_num_rows($rs_batsman)){
				$batsman_data = mysqli_fetch_assoc($rs_batsman);
				$batsman_str.='<br/> <b>'.ucwords($batsman_data['batsman_f_name'].' '.$batsman_data['batsman_last_name']).' '.$batsman_data['batsman_score'].' ('.$batsman_data['batsman_balls'].'b '.$batsman_data['batsman_fours'].'x4 '.$batsman_data['batsman_sixes'].'x6) </b>';
				
				$sql = "SELECT  tmc.id,tmc.batsman_id ,tmc.batsman_score,tmc.batsman_balls,tmc.batsman_sixes,tmc.batsman_fours,tmc.batsman_out,
				u1.f_name as batsman_f_name,u1.last_name as batsman_last_name 
				from tournament_match_commentary as tmc INNER JOIN users as u1 on tmc.batsman_id = u1.id 
				WHERE tmc.match_id = $match_id and tmc.team_id = $team_id and team_overs<$overs and batsman_id!=".$batsman_data['batsman_id']." ORDER BY tmc.id DESC LIMIT 1";
				$rs_batsman = mysqli_query($conn,$sql);
				$batsman_data = mysqli_fetch_assoc($rs_batsman);
				if(!empty($batsman_data) && $batsman_data['batsman_out'] == 'no' ){
					$batsman_str.='<br/> <b>'.ucwords($batsman_data['batsman_f_name']).' '.ucwords($batsman_data['batsman_last_name']).' '.$batsman_data['batsman_score'].' ('.$batsman_data['batsman_balls'].'b '.$batsman_data['batsman_fours'].'x4 '.$batsman_data['batsman_sixes'].'x6) </b>';
				}
			}
			
			
			$run_rate = round($row['team_score']/$overs,2);
			$commmentary_str.='<div class="over_end"> End of over '.$overs.' ('.$row['score_in_over'].' runs) '.$row['company_name'].' '.$row['team_score'].'/'.$row['team_wickets'].' (RR: '.$run_rate.')'.$batsman_str.'</div>';	
			$commmentary_str.='<div class="clear"></div>';
		}
		
		$commmentary_str.='<div class="ball_desc">'.$row['team_overs'].' '.ucwords($row['bowler_f_name']).' '.ucwords($row['bowler_last_name']).' to '.ucwords($row['batsman_f_name']).' '.ucwords($row['batsman_last_name']);
		if($row['ball_type'] == 'legal_ball'){
			if($row['batsman_out'] == 'yes'){
				$commmentary_str.=', '.ucwords($row['batsman_f_name']).' '.ucwords($row['batsman_last_name']).'<span class="batsman_out"> OUT</span>, '.$row['how_out'];	
				if($row['fielder_id']>0){
					$commmentary_str.=' by '.$row['fielder_f_name'].' '.$row['fielder_last_name'];
				}
				$strike_rate = round(($row['batsman_score']/$row['batsman_balls'])*100,2);
				$commmentary_str.='<br/> <b>'.ucwords($row['batsman_f_name']).' '.ucwords($row['batsman_last_name']).' '.$row['batsman_score'].' ('.$row['batsman_balls'].'b '.$row['batsman_fours'].'x4 '.$row['batsman_sixes'].'x6) SR: '.$strike_rate.'</b>';
			}else{
				if($row['runs_type'] == 'batsman_runs'){
					if($row['ball_score'] == 0){
						$commmentary_str.=', '.ucwords($row['batsman_f_name']).' '.ucwords($row['batsman_last_name']).' takes ';
						$commmentary_str.=' no run';
					}elseif($row['ball_score'] == 4 || $row['ball_score'] == 6){
						$commmentary_str.=', <span class="four_six">'.strtoupper($scores_array[$row['ball_score']]).'</span> runs off the ball';
					}else{
						$commmentary_str.=', '.ucwords($row['batsman_f_name']).' '.ucwords($row['batsman_last_name']).' takes ';
						$commmentary_str.=' '.$row['ball_score'].' run';
						if($row['ball_score']>1){
							$commmentary_str.='s';
						}
					}
				}elseif($row['runs_type'] == 'byes'){
					$commmentary_str.=', <span class="no_ball">'.strtoupper($scores_array[$row['ball_score']]).' BYE';
					if($row['ball_score']>1){
						$commmentary_str.='S';
					}
					$commmentary_str.='</span>';
				}elseif($row['runs_type'] == 'leg_byes'){
					$commmentary_str.=', <span class="no_ball">'.strtoupper($scores_array[$row['ball_score']]).' LEG BYE';
					if($row['ball_score']>1){
						$commmentary_str.='S';
					}
					$commmentary_str.='</span>';
				}
			}
			
		}elseif($row['ball_type'] == 'wide_ball'){
			
			$commmentary_str.=', <span class="no_ball">'.strtoupper($scores_array[$row['ball_score']]).' WIDE';
			if($row['ball_score']>1){
				$commmentary_str.='S';
			}elseif($row['ball_score'] == 0){
				$commmentary_str.=' BALL';
			}
			$commmentary_str.='</span>';
			
			if($row['batsman_out'] == 'yes'){
				$commmentary_str.=', '.ucwords($row['batsman_f_name']).' '.ucwords($row['batsman_last_name']).'<span class="batsman_out"> OUT</span>, '.$row['how_out'];	
				if($row['fielder_id']>0){
					$commmentary_str.=' by '.$row['fielder_f_name'].' '.$row['fielder_last_name'];
				}
				$strike_rate = round(($row['batsman_score']/$row['batsman_balls'])*100,2);
				$commmentary_str.='<br/> <b>'.ucwords($row['batsman_f_name']).' '.ucwords($row['batsman_last_name']).' '.$row['batsman_score'].' ('.$row['batsman_balls'].'b '.$row['batsman_fours'].'x4 '.$row['batsman_sixes'].'x6) SR: '.$strike_rate.'</b>';
			}
		}elseif($row['ball_type'] == 'no_ball'){
			
			$commmentary_str.=', <span class="no_ball">NO BALL </span>';
			
				if($row['runs_type'] == 'batsman_runs'){
					if($row['ball_score'] == 0){
						$commmentary_str.=', '.ucwords($row['batsman_f_name'].' '.$row['batsman_last_name']).' takes ';
						$commmentary_str.=' no run';
					}elseif($row['ball_score'] == 4 || $row['ball_score'] == 6){
						$commmentary_str.=', <span class="four_six">'.strtoupper($scores_array[$row['ball_score']]).'</span> runs off the ball';
					}else{
						$commmentary_str.=', '.ucwords($row['batsman_f_name'].' '.$row['batsman_last_name']).' takes ';
						$commmentary_str.=' '.$row['ball_score'].' run';
						if($row['ball_score']>1){
							$commmentary_str.='s';
						}
					}
				}elseif($row['runs_type'] == 'byes'){
					$commmentary_str.=', <span class="no_ball">'.strtoupper($scores_array[$row['ball_score']]).' BYE';
					if($row['ball_score']>1){
						$commmentary_str.='S';
					}
					$commmentary_str.='</span>';
				}elseif($row['runs_type'] == 'leg_byes'){
					$commmentary_str.=', <span class="no_ball">'.strtoupper($scores_array[$row['ball_score']]).' LEG BYE';
					if($row['ball_score']>1){
						$commmentary_str.='S';
					}
					$commmentary_str.='</span>';
				}
			
			
			if($row['batsman_out'] == 'yes'){
				$commmentary_str.=', '.ucwords($row['batsman_f_name']).' '.ucwords($row['batsman_last_name']).'<span class="batsman_out"> OUT</span>, '.$row['how_out'];	
				if($row['fielder_id']>0){
					$commmentary_str.=' by '.$row['fielder_f_name'].' '.$row['fielder_last_name'];
				}
				$strike_rate = round(($row['batsman_score']/$row['batsman_balls'])*100,2);
				$commmentary_str.='<br/> <b>'.ucwords($row['batsman_f_name']).' '.ucwords($row['batsman_last_name']).' '.$row['batsman_score'].' ('.$row['batsman_balls'].'b '.$row['batsman_fours'].'x4 '.$row['batsman_sixes'].'x6) SR: '.$strike_rate.'</b>';
			}
		}
		
		if($row_count == 0 && !isset($scoresheet)){
			$commmentary_str.=' &nbsp; <a class="delete_row" href="javascript:deleteCommentary('.$row['id'].')" onclick="return confirm(\'Are you sure to delete \')">Delete</a>';
		}
		
		$commmentary_str.='</div>';
		$commmentary_str.='<div class="clear"></div>';
		$row_count++;
	}
}
//echo mysqli_error($conn);
$commmentary_str.=$paging_str;
echo $commmentary_str;
exit;