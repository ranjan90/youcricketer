<?php include('common/header.php'); ?>
	<div class="middle">
		<h1>Event Detail - <?=get_combo('events','title',$_GET['id'],'','text')?> </h1>
		<div class="white-box content detail">
			<? 
				$row = get_record_on_id('events',$_GET['id']);
				$row_img= mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'events' and entity_id = '".$row['id']."'"));
			?>
			<p>
				<img src="<?=($row_img)?WWW.'events/'.$row['id'].'/'.$row_img['file_name']:WWW.'images/community-groups.png';?>" alt="<?=$row['title']?>" title="<?=$row['title']?>" width="350" />
				<div style="height:30px;">
					<!-- AddThis Button BEGIN -->
					<div class="addthis_toolbox addthis_default_style ">
					<a class="addthis_button_preferred_1"></a>
					<a class="addthis_button_preferred_2"></a>
					<a class="addthis_button_preferred_3"></a>
					<a class="addthis_button_preferred_4"></a>
					<a class="addthis_button_compact"></a>
					<a class="addthis_counter addthis_bubble_style"></a>
					</div>
					<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
					<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5073efa669740139"></script>
					<!-- AddThis Button END -->
				</div>
				<div><b>Created By : </b><?=get_combo('users','f_name',$row['user_id'],'','text')?>&nbsp;<?=get_combo('users','last_name',$row['user_id'],'','text')?></div>
				<div><b>Created On : </b><?=date_converter($row['create_date'])?></div>
				<div><b>Start Date &nbsp;&nbsp;: </b><?=date_converter($row['start_date'])?></div>
				<div><b>End Date &nbsp;&nbsp;&nbsp;   : </b><?=date_converter($row['end_date'])?></div>
				<div class="space10"></div>
				<?=($rpw['id']!='2')?$row['content']:'';?>
			</p>
		    <div class="clear"></div>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>
<?php include('common/footer.php'); ?>