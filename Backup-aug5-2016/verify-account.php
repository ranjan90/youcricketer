<?php include('common/header.php'); ?>
<?  if(isset($_SESSION['ycdc_user_name']) 
	&& !empty($_SESSION['ycdc_user_name'])){ ?>
		<script>
		window.location = '<?=WWW?>dashboard.html';
		</script>
<?  }?>
	<div class="middle">
		<h1>Verify Account </h1>

		<? 	if(isset($_POST) && !empty($_POST) && $_POST['action'] == 'verify-account'){
				$email 	= $_POST['email'];
				$code 	= $_POST['verification_code'];

				$rs_chk	= mysqli_query($conn,"select * from users where email = '$email'");
				if(mysqli_num_rows($rs_chk) == 0){
					echo '<div id="error">Invalid Email Address ... !</div><br><br>';
				}else{
					$row = mysqli_fetch_assoc($rs_chk);
					if($row['status'] == '1'){
						echo '<div id="error">Your Account already activiated ... !</div><br><br>';	
					}else if($row['status'] == '0'){
						echo '<div id="error">Your Account has been deactivated by Administrator ... !</div><br><br>';	
					}else if($row['status'] == '2'){
						if($row['verification_code'] == $_POST['verification_code']){
							$query = "update users set status = '1' where email = '$email'";
							mysqli_query($conn,$query);
							$query = "update companies set status = '1' where user_id = ".$row['id'];
							mysqli_query($conn,$query);
						$result =	mysqli_query($conn,"UPDATE invitations SET status = 'Invitation Accepted' WHERE to_emails = '$email' AND accept_status = 1");
						if($result) {
								mysqli_query($conn,"UPDATE invitations SET status = 'Already Joined'   WHERE to_emails = '$email' AND accept_status = 0 ");
									
												}

							?>
							<script>
							window.location = '<?=WWW?>login.html?status=verified';
							</script>
							<?
						}else{
							echo '<div id="error">Verification code is not correct ... !</div><br><br>';	
						}
					}
				}
			}
			if(isset($_POST) && !empty($_POST) && $_POST['action'] == 'resend-verification'){
				$email 	= $_POST['email'];
				$rs_chk	= mysqli_query($conn,"select * from users where email = '$email'");
				if(mysqli_num_rows($rs_chk) == 0){
					echo '<div id="error">Invalid Email Address ... !</div><br><br>';
				}else{
					$row_u = mysqli_fetch_assoc($rs_chk);

					if($row_u['status'] == '0'){
						echo '<div id="error">Your account is disabled by Administrator ... !</div><br><br>';	
					}else if($row_u['status'] == '1'){
						echo '<div id="error">Your account is already activated, Please use forget password if do not remember your password ... !</div><br><br>';	
					}else{
						$verification_code 	= getRandomWord(12);
						mysqli_query($conn,"update users set verification_code = '$verification_code' where email = '$email'");

						// sending email
						$email_template = get_record_on_id('cms', 11);
						$mail_title		= $email_template['title'];
		                $mail_content	= $email_template['content'];
		                $mail_content 	= str_replace('{name}',$row_u['f_name'], $mail_content);
		                $mail_content 	= str_replace('{verification_code}',$verification_code, $mail_content);
		                $mail_content 	= str_replace('{verification_link}','<a href="'.WWW.'verify-account.html?id='.$row_u['id'].'" title="Verify your account">HERE</a>', $mail_content);
		                $mail_content 	= str_replace('{email}',$email, $mail_content);
		                $mail_content 	= str_replace('{contact_link}','<a href="'.WWW.'contact-us.html" title="Contact Us">HERE</a>', $mail_content);
		                $mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);


						$headers = "Content-type:text/html;charset=UTF-8" . "\r\n";
	               		$headers .= "From: no-reply@youcricketer.com" . "\r\n";

		                mail($_POST['email'],$mail_title,$mail_content,$headers);	
		                echo '<div id="success">Verification Code sent, Please check your email.... !</div><br><br>';
					}
				}
			}
		?>

		<div class="white-box content">
			<form method="post" action="">
				<input type="hidden" name="action" value="verify-account">
				<fieldset>
					<h2>Enter Verification Code</h2>
					<div class="form-box">
						<label>Email Address </label>
						<input type="text" name="email" class="input-login validate[required, custom[email]]">
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Code </label>
						<input type="text" name="verification_code" class="input-login validate[required]">
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<a href="#" id="resend" class="popup-link" style="position:relative; top;10px;">Re-send Verification Code</a>
						<input type="submit" value=" Submit " class="submit-login">
					</div>
				</fieldset>
			</form>
			<div id="resend-verification-code" class="hide topline">
						<h1>
							Re-Send Verification Code
							<img src="<?=WWW?>images/icons/delete.png" id="close-resend"class="close-popup">
						</h1>
						<form method="post" action="">
							<input type="hidden" name="action" value="resend-verification">
							<fieldset>
								<div class="form-box">
									<label>Email Address</label>
									<input type="text" name="email" class="input-login validate[required, custom[email]]">
									<input type="submit" class="submit-login margin-top-5" value="Submit">
								</div>
								
									
								
							</fieldset>
						</form>
					</div>
					<div class="clear"></div>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>
<script>

$(window).load(function(){
	$("#resend").click(function(){
    $("#resend-verification-code").show();
		});
	$("#close-resend").click(function(){
		$("#resend-verification-code").hide();
		});
	
});
                        </script>
<?php include('common/footer.php'); ?>