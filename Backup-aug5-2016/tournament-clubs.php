<?php include_once('includes/configuration.php');
$page = 'tournament-clubs.html';

$selected_country = getGeoLocationCountry(); 



$tournament_id = trim($_GET['t_id']);
$tournament_info = get_record_on_id('tournaments', $tournament_id);	

$page_title = 'Tournament Clubs- '.ucwords($tournament_info['title']);

$tournament_teams = array();
$tournament_teams_groups = array();

$sql = 'select * from tournament_teams WHERE  tournament_id = '.$tournament_id.' order by id asc';
$rs_teams = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_teams)){
	$tournament_teams[] = $row['team_id'];
	$tournament_teams_groups[$row['team_id']] = $row['group_name'];
}

$league_clubs = array();
$league_info = array();
$error = '';

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
	if($user_info['is_company'] == 1){
		$sql = "select * from companies where user_id = $user_id and company_type_id = 2 and status=1";
		$rs_company = mysqli_query($conn,$sql);
		if(mysqli_num_rows($rs_company)){
			$league_info = mysqli_fetch_assoc($rs_company);
			$sql = 'select c.* from league_clubs as lc inner join companies as c on lc.club_id=c.id where lc.league_id = '.$league_info['id'];
			$rs_club = mysqli_query($conn,$sql);
			while($row = mysqli_fetch_assoc($rs_club)){
				$league_clubs[] = $row;
			}
		}else{
			$error = '<p id="error">You must be logged as League Owner</p>';
		}
	}
}

if(isset($_SESSION['tournament_clubs_updated']) && $_SESSION['tournament_clubs_updated']==1) {
	$tournament_clubs_updated = 1;
	unset($_SESSION['tournament_clubs_updated']);
}

if(isset($_POST['submit_btn']) && !empty($_POST['submit_btn'])){
	validate();
	if(empty($error)){
		
		$tournament_teams = $_POST['part_clubs'];
		$sql = "DELETE FROM tournament_teams WHERE tournament_id = $tournament_id";
		mysqli_query($conn,$sql);
			
		for($i=0;$i<count($tournament_teams);$i++){
			$group_name = trim($_POST['group_name_'.$tournament_teams[$i]]);	
			$sql = "INSERT INTO tournament_teams SET tournament_id = $tournament_id, team_id = ".$tournament_teams[$i].", group_name = '$group_name' ";
			mysqli_query($conn,$sql);
		}
			
		$_SESSION['tournament_clubs_updated'] = 1;
		header("Location:".WWW."tournament/clubs/".$tournament_id);
	}
}

function validate(){
	global $error;
	
	if(empty($_POST['part_clubs']) || count($_POST['part_clubs'])<2){
		$error.= '<p id="error">Minimum Two Participating Clubs are Required</p>';
	}
}



?>
<?php include('common/header.php'); ?>
<style>
.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
dl {min-height:145px;}
.content {width:1010px;}
.large-column {width:790px;}
.form-box {width:50%;}
.chk_left {float:left !important; }
</style>
	<div class="middle">
		<h1>Tournament Clubs - <?php echo $tournament_info['title']; ?></h1>
		
		<div class="white-box content" id="dashboard">
			<?php if(empty($user_info)): ?>
				<div id="error">You are not logged... !</div>
			<?php endif; ?>
			
			<?php if(!empty($error)): ?>
				<?php echo $error; ?>
			<?php endif; ?>
			
			<?php if(isset($tournament_clubs_updated) && $tournament_clubs_updated == 1): ?>
				<div id="information">Tournament clubs updated Successfully... !</div>
			<?php endif; ?>
		
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
			<?php if($tournament_info['user_id'] == $user_id){ ?>
				<form method="post"  enctype="multipart/form-data">
					<input type="hidden" name="action" value="submit">
					<fieldset>
						<h2>Tournament Clubs</h2>
						<p style="padding-left:12px;color:#6C6C6C;">Please Check Mark the Clubs Participating in this Specific Tournament and Click Update at the bottom. To Remove from Tournament please uncheck and Click Update. Group can have only Alphanumeric Characters</p>
						
					
						<div class="clear" style="height:15px;"></div>
						
						<div class="form-box" style="width:70%;">
							<label style="font-size:13px;">Participating Clubs: </label>
							<div class="text" style="width:75%;">
								<table style="width:90%;"><tr><td>Club Name</td><td>Group Name</td></tr>
								<?php for($i=0;$i<count($league_clubs);$i++): ?>
								<tr><td style="vertical-align:middle;padding-bottom:5px;"><input type="checkbox" class="chk_left"  <?php if(!empty($_POST['part_clubs']) && in_array($league_clubs[$i]['id'],$_POST['part_clubs'])) echo 'checked';elseif(empty($_POST) && in_array($league_clubs[$i]['id'],$tournament_teams)) echo 'checked'; ?> name="part_clubs[]" value="<?php echo $league_clubs[$i]['id']; ?>"> <?php echo $league_clubs[$i]['company_name']; ?></td>
								<td style="vertical-align:middle;padding-bottom:5px;"><input type="text" maxlength="20" style="width:120px;float:left;"  name="group_name_<?php echo $league_clubs[$i]['id']; ?>" id="group_name_<?php echo $league_clubs[$i]['id']; ?>" class="input-login" value="<?php if(!empty($_POST['group_name'])) echo $_POST['group_name'];elseif(!empty($tournament_teams_groups[$league_clubs[$i]['id']])) echo $tournament_teams_groups[$league_clubs[$i]['id']];else echo 'N/A'; ?>"></td>
								<?php endfor; ?>
								</table>
							</div>
						</div>
						<div class="clear"></div>
						
						<div class="form-box">
							<input type="button" name="cancel_btn" value=" Cancel " class="submit-login" onclick="window.location.href='<?php echo WWW; ?>tournament/list';">
							<input type="submit" name="submit_btn" value=" Update " class="submit-login" <?php if(empty($user_info) || empty($league_info)): ?> disabled <?php endif; ?>>
						</div>
					</fieldset>
				</form>
				
				<?php }else{ ?>
				<div id="error">You are not owner of this tournament... !</div>
				<?php } ?>
			
			</div>  
		    <div class="clear"></div>
			</div>
		</div>
		<!--<div class="rightbar"><?php //include('common/right-panel.php');?></div>-->
		<div class="clear"></div>
	</div>
	

<script src="<?php echo WWW; ?>js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo WWW; ?>css/jquery.datetimepicker.css"/>
<script>
$('#start_time').datetimepicker();
$('#end_time').datetimepicker();
</script>
<?php include('common/footer.php'); ?>