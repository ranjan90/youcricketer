<?php include('common/header.php'); ?>

<div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1> YouCricketer Press Releases </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <div class="white-box">
            <div id="pagination-top">
              <div class="row">
                <div class="col-sm-4 col-sm-offset-8" id="div-form1">
                  <form id="list-search" method="post" action="">
                    <div class="input-group">
                      <input type="text" class="form-control validate[required] input-login" name="txtsearch" placeholder="Search Here">
                      <span class="input-group-btn">
                        <button class="btn btn-default" type="submit">Go!</button>
                      </span>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            
            <div class="row">
              <div class="col-sm-12">
              	<div class="list">
                  <ul>
				  	<?php $rpp = PRODUCT_LIMIT_FRONT; // results per page
            			$ppage = intval($_GET["page"]);
      					if($ppage<=0) $ppage = 1;
      					$query = "select * from press_releases where status=1 ";
				      	//=======================================
					    if(isset($_GET['keywords']) && $_GET['keywords'] != 'Search Here'){
					        $query  .= "and title like '%".str_replace('-','%',$_GET['keywords'])."%'";
					    }
					    $query .= " order by id desc ";
				      //echo $query;
				      //=======================================
				      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
				        echo '<div id="information">No record found ... !</div>';
				      }
				      $rs   = mysqli_query($conn,$query);
				      $tcount = mysqli_num_rows($rs);
				      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
				      $count = 0;
				      $i = ($ppage-1)* $rpp;
          			  $x = 0;
          			  //=======================================
      				  while(($count<$rpp) && ($i<$tcount)){
                		mysqli_data_seek($rs,$i);
                		$row_g 	= mysqli_fetch_assoc($rs);
                		?>
                    <li>
                      <div class="row">
                      	<div class="col-sm-12">
							<h3 onclick="window.location='<?php echo WWW?>press-release-detail-<?php echo $row_g['id']?>-<?php echo friendlyURL($row_g['title'])?>.html'">
							<? if(isset($_SESSION['ycdc_user_email']) && ( $_SESSION['ycdc_user_email'] == 'naveed.ramzan@gmail.com' || $_SESSION['ycdc_user_email'] == 'youcrickter00@gmail.com')){ ?>
								       <div class="left">
								       	<a href="<?php echo WWW;?>edit-press-release-<?php echo $row_g['id']?>.html"><img src="<?php echo WWW;?>images/icons/edit.png"></a>
								       	<a href="<?php echo WWW;?>delete-press-release-<?php echo $row_g['id']?>.html"><img src="<?php echo WWW;?>images/icons/delete.png"></a>
								       </div>
								    <?php } ?>
								    <?php echo $row_g['title']?>
							</h3>
                          <p><?php echo (!empty($row_g['summary']))?$row_g['summary']:truncate_string($row_g['content'], 100)?></p>
                        </div>
                      </div>
                      <div class="row">
                      	<div class="col-sm-4">
							<!-- AddThis Button BEGIN -->
								        <div class="addthis_toolbox addthis_default_style ">
								        <a class="addthis_button_preferred_1"></a>
								        <a class="addthis_button_preferred_2"></a>
								        <a class="addthis_button_preferred_3"></a>
								        <a class="addthis_button_preferred_4"></a>
								        <a class="addthis_button_compact"></a>
								        <a class="addthis_counter addthis_bubble_style"></a>
								        </div>
								        <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
								        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5073efa669740139"></script>
							<!-- AddThis Button END -->
                        </div>
                        <div class="col-sm-4">Published on: <?php echo date_converter($row_g['release_date'])?> </div>
                        <div class="col-sm-4">
							<a href="<?php echo WWW?>press-release-detail-<?php echo $row_g['id']?>-<?php echo friendlyURL($row_g['title'])?>.html" title="Read more" class="btn btn-default" />Read More</a>
                        </div>
                      </div>
                    </li>
                    <?php
					      $i++;
					      $count++;
					      $x++;
					 } ?>
                   
					</ul>
                </div>
              </div>
            </div>
            
            <div id="pagination-bottom">
              <div class="row">
                <div class="col-sm-4 col-sm-offset-8" id="div-form2">
					<? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_FRONT){ ?>
					<?php
						if(isset($_GET['keywords'])){
							$reload = 'press-releases-'.$_GET['keywords'].'.html?';
						}else{
							$reload = 'press-releases.html?';
						}
						echo paginate_one($reload, $ppage, $tpages);
					?>
					<input type="hidden" name="pagination-page" value="<?php echo $reload;?>">
					<? } ?>    
                  <form id="list-search" method="post" action="">
                    <div class="input-group">
                      <input type="text" class="form-control validate[required] input-login" name="txtsearch" placeholder="Search Here">
                      <span class="input-group-btn">
                        <button class="btn btn-default" type="submit">Go!</button>
                      </span>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <script type="text/javascript">
		        $('form#list-search').submit(function(e){
					var parentId = $(this).parent().attr('id');
					var string = $('#'+parentId +' form input[name=txtsearch]').val();
					if(string != '' && string != 'Search Here'){
						string = string.replace(/[^a-zA-Z0-9]+/g,'-').toLowerCase();
						if(string.length > 0){
							$('form#list-search').attr('action','<?php echo WWW;?>press-releases-' + string + '.html');
						}
					}
				});
		    </script>
			
			<script type="text/javascript">
			$(document).ready(function(){
				$('select#pagination-combo').change(function(){
					var x = this.value;
					var page = $('input[name=pagination-page]').val();
					var ppage= page.split('.html');
					var curPage = '';
					if(x == 1){
						curPage = WWW + ppage[0] + '.html'
					}else{
						curPage = WWW + ppage[0] + '-' + x + '.html'
					}
					window.location = curPage;
				});
			});
			</script>

		  </div>
        </div>
        
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
				<?php include('common/right-panel.php');?>
            </div>
          </div>
         
        </div>
      </div>
    </div><!-- /.container -->
	
<?php include('common/footer.php'); ?>