<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	<div class="middle">
		<h1>Profile Information</h1>

		<? 	if(isset($_POST) && !empty($_POST)){
				$post_code 		= $_POST['post_code'];
				
				$phone 			= $_POST['phone'];
				$education 		= $_POST['education'];
				$agent 			= $_POST['agent'];
				$zipcode 		= $_POST['zipcode'];
				$no_of_playing  = $_POST['no_of_playing'];
				$first_class_player = $_POST['first_class_player'];
				$player_type 	= $_POST['player_type'];
				
				$state_id 		= $_POST['state_id'];
				$city_id 		= $_POST['city_id'];

				$set = array();

				$set[] 	= "post_code	= '".$zipcode."'";
				$set[] 	= "city_id		= '".$city_id."'";
				$set[] 	= "phone 		= '".$phone."'";
				$set[] 	= "education	= '".$education."'";
				$set[] 	= "agent 		= '".$agent."'";
				$set[] 	= "no_of_playing= '".$no_of_playing."'";
				$set[] 	= "first_class_player= '".$first_class_player."'";
				

				if($first_class_player == '1'){
					$espn 		= $_POST['espn'];
					
					$set[] 	= "espncrickinfo= '".$espn."'";
				}
				
				$query 	= "update users set ".implode(',',$set)." where id = '".$row_user['id']."'";
				//echo $query;exit;
				mysqli_query($conn,$query);

				echo '<div id="success"><b>Success : </b>Information updated ... !</div><br><br>';
                ?>
                <script>
                	window.location = '<?=WWW?>dashboard.html';
				</script>
                <?
			}
			$row_country = get_record_on_id('countries', $row_user['country_id']);
			$row_city 	 = get_record_on_id('cities', $row_user['city_id']) ;
			$row_state 	 = get_record_on_id('states', $row_city['state_id']);
			?>

		<div class="white-box content" id="dashboard">
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
			<form method="post" action="" id="profile" enctype="multipart/form-data">
				<fieldset>
					<legend>Profile Picture</legend>
					<div class="form-box">
							<label>Photo </label>
							<div class="text">
								<? if($row_photo){ ?>
								<img src="<?=WWW?>users/<?=$row_user['id']?>/<?=$row_photo['file_name']?>" width="150">
								<? }else{ ?>
								<img src="<?=WWW?>images/no-photo.png" width="150">
								<? } ?>
							</div>
							<input type="file" name="photo">
						</div>
						<div class="form-box">
							<label>Max Photo size : 2MB &nbsp; &nbsp; &nbsp; <a target="_blank" href="<?=WWW?>multimedia-policy.html">Multi Media Policy</a></label>
						</div>
						<div class="clear"></div>
				</fieldset>
				<fieldset>
					<legend>Location Details</legend>
					<div class="form-box">
						<label>Country </label>
					</div>
					<div class="form-box">
						<div class="text" style="margin-left:20px; float:left;"><?=get_combo('countries','name',$row_user['country_id'],'','text')?></div>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>State </label>
					</div>
					<div class="form-box">
						<select style="margin-left:20px; float:left;" name="state_id" class="validate[required]">
							<option selected="selected" value=""></option>
							<? 	$rs_states = mysqli_query($conn,"select * from states where country_id = '".$row_user['country_id']."'"); 
								while($row_state_l = mysqli_fetch_assoc($rs_states)){ ?>
								<option <?=($row_state['id'] == $row_state_l['id'])?'selected="selected"':'';?> value="<?=$row_state_l['id']?>"><?=$row_state_l['name']?></option>
							<?  } ?>
						</select>
					</div>
					
					<div class="clear"></div>
					<div class="form-box">
						<label>City </label>
					</div>
					<div class="form-box">
						<div id="city">
						<select style="margin-left:20px; float:left;" name="city_id" class="validate[required]">
							<option selected="selected" value=""></option>
							<? 	$rs_cities = mysqli_query($conn,"select * from cities where state_id = '".$row_state['id']."'"); 
								while($row_city_l = mysqli_fetch_assoc($rs_cities)){ ?>
								<option <?=($row_city['id'] == $row_city_l['id'])?'selected="selected"':'';?> value="<?=$row_city_l['id']?>"><?=$row_city_l['name']?></option>
							<?  } ?>
						</select>
						</div>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Zipcode</label>
					</div>
					<div class="form-box">
						<input style="margin-left:20px; float:left;" type="text" name="zipcode" class="input-login <?=($row_user['country_id'] == '221' || $row_user['country_id'] == '222')?'validate[required]':'';?>" maxlength="6" value="<?=$row_user['post_code']?>" >
					</div>
					<div class="clear"></div>
				</fieldset>
				<fieldset>
					<legend>Cricketing Traits</legend>	
					<div id="dependent-player" class="<?=($row_user['user_type_id'] != '2')?'hide':'';?>">
						<div class="form-box">
							<label>First Class Player</label>
						</div>
						<div class="form-box">
							<div style="float:left; margin-left:20px;">
							<?=get_first_class_player_combo($row_user['first_class_player'])?>
							</div>
						</div>
						<div class="clear"></div>
						<div id="espn" class="<?=($row_user['first_class_player'] != '1')?'hide':'';?>">
							<div class="form-box">
								<label>Paste your ESPNCRICINFO.com Link Here</label>
							</div>
							<div class="form-box">
								<input type="text" value="<?=(!empty($row_user['espncrickinfo']))?$row_user['espncrickinfo']:''?>" style="float:left; margin-left:20px;" name="espn" class="input-login">
							</div>
						</div>
						<div class="clear"></div>
						<div class="form-box">
							<label>What Type of Player are You? </label>
						</div>
						<div class="form-box">
							<div style="float:left; margin-left:20px;">
							<select class="validate[required]" name="player_type">
								<option value="" selected="selected">Select One</option>
								<option <?=($row_user['type'] == 'Batsman')?'selected="selected"':'';?> value="Batsman">Batsman</option>
								<option <?=($row_user['type'] == 'Bowler')?'selected="selected"':'';?> value="Bowler">Bowler</option>
								<option <?=($row_user['type'] == 'All Rounder')?'selected="selected"':'';?> value="All Rounder">All Rounder</option>
								<option <?=($row_user['type'] == 'Specialist Wicketkeeper')?'selected="selected"':'';?> value="Specialist Wicketkeeper">Specialist Wicketkeeper</option>
							</select>
							</div>
						</div>
						<div id="player-type-batsman" class="<?=($row_user['type'] != 'Batsman')?'hide':'';?>">
							<div class="form-box">
								<label>Type of Batsman</label>
							</div>
							<div class="form-box">
								<select name="batsman_type" style="float:left; margin-left:20px;" >
									<option selected="selected" value="">- None -</option>
									<option <?=($row_user['type_of_batsman'] == 'Left-handed')?'selected="selected"':'';?> value="Left-handed">Left-handed</option>
									<option <?=($row_user['type_of_batsman'] == 'Right-handed')?'selected="selected"':'';?> value="Right-handed">Right-handed</option>
								</select>
							</div>
							<div class="clear"></div>
							<div class="form-box">
								<label>What Order do you Bat?</label>
							</div>
							<div class="form-box">
								<select name="batting_order" style="float:left; margin-left:20px;" >
									<option selected="selected" value="">- None -</option>
									<option <?=($row_user['batting_order'] == 'Top Order (1 - 3)')?'selected="selected"':'';?> value="Top Order (1 - 3)">Top Order (1 - 3)</option>
									<option <?=($row_user['batting_order'] == 'Middle Order (4 - 7)')?'selected="selected"':'';?> value="Middle Order (4 - 7)">Middle Order (4 - 7)</option>
									<option <?=($row_user['batting_order'] == 'Lower Order (8 - 11)')?'selected="selected"':'';?> value="Lower Order (8 - 11)">Lower Order (8 - 11)</option>
								</select>
							</div>
						</div>
						<div id="player-type-bowler" class="<?=($row_user['type'] != 'Bowler')?'hide':'';?>">
							<div class="form-box">
								<label>Type of Bowler</label>
							</div>
							<div class="form-box">
								<select name="bowler_type" style="float:left; margin-left:20px;" >
									<option selected="selected" value="">- None -</option>
									<option <?=($row_user['type_of_bowler'] == 'Fast')?'selected="selected"':'';?> value="Fast">Fast</option>
									<option <?=($row_user['type_of_bowler'] == 'Medium-fast')?'selected="selected"':'';?> value="Medium-fast">Medium-fast</option>
									<option <?=($row_user['type_of_bowler'] == 'Leg Spinner')?'selected="selected"':'';?> value="Leg Spinner">Leg Spinner</option>
									<option <?=($row_user['type_of_bowler'] == 'Off Spinner')?'selected="selected"':'';?> value="Off Spinner">Off Spinner</option>
								</select>
							</div>
							<div class="clear"></div>
							<div class="form-box">
								<label>With Which Arm do you Bowl?</label>
							</div>
							<div class="form-box">
								<select name="bowling_arm" style="float:left; margin-left:20px;" >
									<option selected="selected" value="" >- None -</option>
									<option <?=($row_user['which_arm_bowler'] == 'Left')?'selected="selected"':'';?> value="Left">Left</option>
									<option <?=($row_user['which_arm_bowler'] == 'Right')?'selected="selected"':'';?> value="Right">Right</option>
								</select>
							</div>
							<div class="clear"></div>
							<div class="form-box">
								<label>During One Day Games, do you Prefer to Bowl Between Overs?</label>
							</div>
							<div class="form-box">
								<select name="bowling_prefer" style="float:left; margin-left:20px;" >
									<option selected="selected" value="">- None -</option>
									<option <?=($row_user['bowl_between_overs'] == '1 - 12 Overs')?'selected="selected"':'';?> value="1 - 12 Overs">1 - 12 Overs</option>
									<option <?=($row_user['bowl_between_overs'] == '13 - 20 Overs')?'selected="selected"':'';?> value="13 - 20 Overs">13 - 20 Overs</option>
									<option <?=($row_user['bowl_between_overs'] == '21 - 30 Overs')?'selected="selected"':'';?> value="21 - 30 Overs">21 - 30 Overs</option>
									<option <?=($row_user['bowl_between_overs'] == '31 - 40 Overs')?'selected="selected"':'';?> value="31 - 40 Overs">31 - 40 Overs</option>
									<option <?=($row_user['bowl_between_overs'] == '41 - 50 Overs')?'selected="selected"':'';?> value="41 - 50 Overs">41 - 50 Overs</option>
									<option <?=($row_user['bowl_between_overs'] == 'As Needed')?'selected="selected"':'';?> value="As Needed">As Needed</option>
								</select>
							</div>
							<div class="clear"></div>
						</div>
						<div id="match-type" >
							<div class="form-box">
								<label>What Type of Player</label>
							</div>
							<div class="form-box">
								<div style="float:left; margin-left:20px;">
								<? $matchArrayData = explode(',',$row_user['match_types']);?>
								<? foreach($matchArray as $ma){ ?>
								<p><input style="float:left;" type="checkbox" <?=(in_array($ma, $matchArrayData))?'checked="checked"':'';?> name="matchType[]" value="<?=$ma?>">&nbsp; <?=$ma?></p>
								<? } ?>
							</div>
						</div>
						<div class="clear"></div>
						<div class="form-box">
							<label>Preffered Game Type</label>
						</div>
						<div class="form-box">
							<div style="float:left; margin-left:20px;">
							<? $prefferedGameTypeData = explode(',',$row_user['game_types']);?>
							<? foreach($prefferedGameType as $ma){ ?>
							<p><input style="float:left;" type="checkbox" <?=(in_array($ma, $prefferedGameTypeData))?'checked="checked"':'';?> name="gameType[]" value="<?=$ma?>">&nbsp; <?=$ma?></p>
							<? } ?>
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend>Current Affiliations</legend>
					<h3>Your Current Clubs/Association/Academy/Board</h3>
					<div class="space10"></div>
			        <? 	$rs_msg = mysqli_query($conn,"select utc.id, c.title from users_to_clubs utc join clubs c on c.id = utc.club_id where utc.user_id = '".$row_user['id']."' order by utc.id desc "); 
			        	$total  = mysqli_num_rows($rs_msg);
			        	if(mysqli_num_rows($rs_msg) != 0){
			        		while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
				       		<div class="form-box">
				       			<input type="text" name="clubs[]" style="float:left;" class="input-login" value="<?=$row_msg['title']?>">
				       		</div>
				       	<?  } ?>
				       	<?  for($x = $total; $x < 5; $x++){ ?>
				       		<div class="form-box">
				       			<input type="text" name="clubs[]" style="float:left;" value="" class="input-login">
				       		</div>
				       	<?  } ?>
				    <?  } ?>
			        <div class="clear space10"></div>
			        <h3>Your Current League Names</h3>
					<div class="space10"></div>
			        <? 	$rs_msg = mysqli_query($conn,"select utc.id, c.title from users_to_leagues utc join leagues c on c.id = utc.league_id where utc.user_id = '".$row_user['id']."' order by utc.id desc "); 
			        	$total  = mysqli_num_rows($rs_msg);
			        	if(mysqli_num_rows($rs_msg) != 0){
			        		while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
				       		<div class="form-box">
				       			<input type="text" name="leagues[]" style="float:left;" class="input-login" value="<?=$row_msg['title']?>">
				       		</div>
				       	<?  } ?>
				       	<?  for($x = $total; $x < 5; $x++){ ?>
				       		<div class="form-box">
				       			<input type="text" style="float:left;" name="leagues[]" value="" class="input-login">
				       		</div>
				       	<?  } ?>
				    <?  } ?>
					<div class="clear space10"></div>
			        <h3>Name of Agent/Agency Representing you</h3>
					<div class="space10"></div>
			        <? 	$agents = explode(',', $row_user['agent']); 
			        	if(!empty($row_user['agent'])){
			        		foreach($agents as $a){ ?>
				       		<div class="form-box">
				       			<input type="text" name="agents[]" value="<?=$a?>" class="input-login" style="float:left;">
				       		</div>
				    <?  	} ?>
				    		<?  for($x = count($agents); $x < 5; $x++){ ?>
					       		<div class="form-box">
					       			<input type="text" name="agents[]" value="" class="input-login" style="float:left;">
					       		</div>
					       	<?  } ?>
					<? } ?>
			        <div class="clear space10"></div>
				</fieldset>
				<fieldset>
					<legend>Profile Video</legend>
					<input type="radio" name="video" value="embed">&nbsp; Embed Video &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="radio" name="video" value="upload">&nbsp; Upload Video
					<div class="space10"></div>
					<div id="embed-video" class="hide">
						<div class="form-box" style="width:100%;">
							<label>Attach Default Video </label>
							<textarea style="width:92%;" name="video_code" ><?=($row_video)?$row_video['file_name']:'';?></textarea>
						</div>
						<div class="clear"></div>
						<div class="form-box">
							<label>Please paste here YouTube/DailyMotion Embed Code</label>
						</div>
						<div class="clear"></div>
					</div>
					<div id="upload-video" class="hide">
						<div class="form-box">
							<label>Upload Default Video </label>
							<input type="file" name="video">
						</div>
						<div class="form-box">
							<label>Max Video size : 10MB</label>
						</div>
						<div class="clear"></div>
					</div>
				</fieldset>

				<fieldset>
					<legend>Overall Experience</legend>

				</fieldset>

				<fieldset>
					<legend>Seeking Opportunities</legend>

				</fieldset>

				<fieldset>
					<legend>Travel Requirements</legend>

				</fieldset>

				<fieldset>
					<legend>Personal Details</legend>
					<div class="form-box">
						<label>Phone Number </label>
					</div>
					<div class="form-box">
						<div class="text" style="float:left;width:15%; text-align:right;"><?=$row_country['phone_code']?></div>
						<input style="width:50%; float:left;" value="<?=$row_user['phone']?>" type="text" name="phone" class="input-login validate[required]">
					</div>
					<div class="clear"></div>
					<div class="form-box" style="width:90%;">
						<label>Education Details</label>
					</div>
					<div class="form-box" style="width:90%;">
					<table width="100%" border="1" style="border-collapse:collapse;">
					<tr>
						<th>Degree</th>
						<th>Institute</th>
						<th>Passing Year</th>
					</tr>
					<? for($x=0;$x<5;$x++){ ?>
					<tr>
						<td><input type="text" name="degree[]" class="input-login"></td>
						<td><input type="text" name="institute[]" class="input-login"></td>
						<td><input type="text" name="institute[]" class="input-login" style="width:50px;"></td>
					</tr>
					<tr><td colspan="4"><hr style="color:#ccc;"></td></tr>
					<? } ?>
					</table>
					</div>
					<div class="clear"></div>
					<div class="form-box" style="width:90%;">
						<label>Language Familiarity</label>
					</div>
					<div class="form-box" style="width:90%;">
					<table width="100%" border="1" style="border-collapse:collapse;">
					<tr>
						<th></th>
						<th class="tcenter">Speaking</th>
						<th class="tcenter">Reading</th>
						<th class="tcenter">Writing</th>
					</tr>
					<? for($x=0;$x<5;$x++){ ?>
					<tr>
						<td width="200"><input value="<?=($x == 0)?'English':'';?>" type="text" name="languages[]" class="input-login"></td>
						<td><input type="checkbox" name="speaking[]" style="float:left; margin-left:50px;"></td>
						<td><input type="checkbox" name="reading[]" style="float:left; margin-left:50px;"></td>
						<td><input type="checkbox" name="writing[]" style="float:left; margin-left:50px;"></td>
					</tr>
					<tr><td colspan="4"><hr style="color:#ccc;"></td></tr>
					<? } ?>
					</table>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Write About Yourself</label>
					</div>
					<div class="clear"></div>
					<div class="form-box" style="width:90%;">
						<textarea style="width:100%;" name="education" maxlimit="1000" class="left validate[required]"><?=$row_user['education']?></textarea>
						<p>Content limited to 1000 characters.</p>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Number of Years Playing Cricket </label>
					</div>
					<div class="form-box">
						<select name="no_of_playing" class="validate[required]" style="float:left; margin-left:20px;">
							<option <?=(empty($row_user['no_of_playing']))?'selected="selected"':'';?> value=""></option>
							<? for($x = 1; $x <= 100; $x++){ ?>
							<option <?=($x == $row_user['no_of_playing'])?'selected="selected"':'';?> value="<?=$x?>"><?=$x?></option>
							<? } ?>
						</select>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Favorite Cricketer(s)</label>
					</div>
					<div class="form-box">
						<textarea cols="40" rows="3" name="favorite_cricketers" style="height:100px;"></textarea>
						<div class="text" style="width:100%;">Enter names with comma seperated</div>
					</div>
					<div class="clear"></div>
				</fieldset>

				<fieldset>
					<legend>Social Media Details</legend>
					<div class="form-box">
						<label>Your Facebook Page Link</label>
					</div>
					<div class="form-box">
						<input style="width:100%;" type="text" name="facebook" value="<?=$row_user['facebook']?>" class="input-login validate[custom[url]]">
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Your Twitter Page Link</label>
					</div>
					<div class="form-box">
						<input style="width:100%;" type="text" name="twitter" value="<?=$row_user['twitter']?>" class="input-login validate[custom[url]]">
					</div>
					<div class="clear"></div>
					
					<div class="form-box" style="float:right;">
						<input type="submit" value=" Update " id="SaveAccount" class="submit-login">
					</div>
				</fieldset>
			</form>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<style>
.margin-10 td{padding:3px;}
</style>
<script src="<?php echo WWW?>js/jquery-1.8.0.js" type="text/javascript"></script>
<script src="<?php echo WWW?>js/formToWizard.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#profile").formToWizard({ submitButton: 'SaveAccount' });

	$('input[name=video]').click(function(){
		if(this.value == 'embed'){
			$('#upload-video').addClass('hide');
			$('#embed-video').removeClass('hide');
		}else{
			$('#embed-video').addClass('hide');
			$('#upload-video').removeClass('hide');
		}
	});

	$('select[name=user_type_id]').live('change',function(){
		var id = $('select[name=user_type_id]').val();
		if(id != '2'){
			$('#dependent-player').addClass("hide");
		}else{
			$('#dependent-player').removeClass("hide");
		}
	});

	$('select[name=player_type]').live('change',function(){
		var id = $('select[name=player_type]').val();
		if(id == 'Batsman'){
			$('#player-type-batsman').removeClass('hide');
			$('#player-type-batsman select[name=batsman_type]').addClass('validate[required]');
			$('#player-type-batsman select[name=batting_order]').addClass('validate[required]');

			$('#player-type-bowler').addClass('hide');
			$('#player-type-bowler select[name=bowler_type]').removeClass('validate[required]');
			$('#player-type-bowler select[name=bowling_arm]').removeClass('validate[required]');
			$('#player-type-bowler select[name=bowling_prefer]').removeClass('validate[required]');
		}else if(id == 'Bowler'){
			$('#player-type-batsman').addClass('hide');
			$('#player-type-batsman select[name=batsman_type]').removeClass('validate[required]');
			$('#player-type-batsman select[name=batting_order]').removeClass('validate[required]');

			$('#player-type-bowler').removeClass('hide');
			$('#player-type-bowler select[name=bowler_type]').addClass('validate[required]');
			$('#player-type-bowler select[name=bowling_arm]').addClass('validate[required]');
			$('#player-type-bowler select[name=bowling_prefer]').addClass('validate[required]');
		}else{
			$('#player-type-batsman').addClass('hide');
			$('#player-type-bowler').addClass('hide');
			$('#player-type-batsman select[name=batsman_type]').removeClass('validate[required]');
			$('#player-type-batsman select[name=batting_order]').removeClass('validate[required]');
			$('#player-type-bowler select[name=bowler_type]').removeClass('validate[required]');
			$('#player-type-bowler select[name=bowling_arm]').removeClass('validate[required]');
			$('#player-type-bowler select[name=bowling_prefer]').removeClass('validate[required]');
		}
	});

	$('select[name=first_class_player]').live('change',function(){
		var id = $('select[name=first_class_player]').val();
		if(id == '1'){
			$('#espn').removeClass('hide');
			$('#espn input[name=espn]').addClass('validate[required, custom[url]]');
		}else{
			$('#espn').addClass('hide');
			$('#espn input[name=espn]').removeClass('validate[required, custom[url]]');
		}
	});

	$('select[name=state_id]').live('change',function(){
		var id = $('select[name=state_id]').val();
		$.ajax({type	: 'POST', 
				url		: WWW + 'includes/get-cities.php', 
				data	: ({id:id}),
				success	: function(msg){
					$("#city").html(msg);
				}
		});
	});
	
	$('#dashboard .black-box h2').click(function(){
		$('#dashboard .black-box ul').css('display','none');
		var id = this.id;
		$('#'+id+'-box ul').css('display','block');
	});
	
	$('input[name=photo]').change(function(){
		var val = this.value;
		var filesize = Math.round((this.files[0].size)/1024,2);
		if(filesize > 2000){
			$(this).val('');
            alert("Image is too large ... !");
		}
	});

	$('ul#steps li').click(function(){
		var id = this.id;
		var idd=id.replace('stepDesc','');
		for(var x = 0; x < 10; x++){
			$('form div#step'+x).css('display', 'none');
			$('ul#steps li#stepDesc'+x).removeClass('current');
		}
		$('form div#step'+idd).css('display', 'block');
		$('ul#steps li#stepDesc'+idd).addClass('current');
		
	});
});
</script>
<?php include('common/footer.php'); ?>