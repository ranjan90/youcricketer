<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	
		<? 	$user_id = $_SESSION['ycdc_dbuid'];
			
				if(isset($_POST) && !empty($_POST)){ 
				$content 	= $_POST['content'];
				$date 		= date('Y-m-d H:i:s');
				$user_id 	= $_SESSION['ycdc_dbuid'];
				$rating 	= $_POST['rating'];
    
				if(!isset($user_id) || empty($user_id)){
					echo '<div id="error" class="alert alert-danger">Please login first ... !</div>';
				}else{
					if(!empty($content) && !empty($rating)){
						$toEmails 	= explode('),', $_POST['to']);
						$counter 	= 0;
						foreach($toEmails as $te){
							$complete = explode('(', $te);
							$info 	  = explode(',', $complete[1]);
							$role  	  = $info[0];
							$country  = str_replace(')','',$info[1]);
							$rowRole = mysqli_fetch_assoc(mysqli_query($conn,"select * from user_types where name = '".trim($role)."'"));
							$rowcountry = mysqli_fetch_assoc(mysqli_query($conn,"select * from countries where name = '".trim($country)."'"));
							$name = explode(' ', $complete[0]);
							$rowUser= mysqli_fetch_assoc(mysqli_query($conn,"select * from users where trim(f_name) = '".$name[0]."' and trim(last_name) = '".$name[1]."' 
																	and user_type_id = '".$rowRole['id']."' and country_id = '".$rowcountry['id']."'"));
							/********************************/
							$to_user_id = $rowUser['id'];
							
							if(!empty($to_user_id)){
								$rsCheck = mysqli_query($conn,"select * from testimonials where user_id = '$user_id' 
									and entity_id = '$to_user_id' and content = '$content'");
								if(mysqli_num_rows($rsCheck) == 0){
									$query = "insert into testimonials (content, user_id, rating, status, entity_id, testimonial_date) 
											values ('$content','$user_id','$rating','1','$to_user_id','$date')";
									if(mysqli_query($conn,$query)){
										$counter++;
									}
								}
							}
						}
						if($counter > 0){
							echo '<div id="success" class="alert alert-success">'.$counter.' Testimonial Information added successfully ..... !</div>'; ?>
							 <script>
			                window.location = '<?=WWW?>my-testimonials.html';
			                </script>
						<?
						}
					}else{
						echo '<div id="error" class="alert alert-danger">Please fill all fields ... !</div>';
					}
				}
				
			}
		?>
		

<div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1> Add Testimonial </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <form method="post" action="" enctype="multipart/form-data" class="form-horizontal">
            
            <div class="white-box">
              <div class="form-group">
                <div class="col-sm-12 text-center">
                  <h2>Testimonial Information</h2>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Testimonial For</label>
                <div class="col-sm-9">
                  <textarea class="validate[required]" id="to" name="to" style="height:30px; width:100%;  "></textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Rating</label>
                <div class="col-sm-9">
                  <select class="form-control validate[required]" name="rating">
                    <? for($x = 5; $x > 0; $x--){ ?>
						<option value="<?=$x?>" <?=($x == 10)?'selected="selected"':'';?>><?=$x?></option>
						<? }?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Content</label>
                <div class="col-sm-9">
                  <textarea maxlength="500" rows="5" name="content" id="content" class="form-control validate[required]"></textarea>
                  <span class="help-block">Maximum 160 characters <input name="text_counter" id="text_counter" value="0" type="text" class="form-control input-sm inline" style="max-width:80px; display:inline"> </span>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12 text-center">
                  <input value=" Save " class="btn orange hvr-float-shadow" type="submit">
                  <a id="cancel" class="btn blue hvr-float-shadow" href="<?=WWW?>testimonials.html">Cancel</a>
                </div>
              </div>
            </div>
		  </form>
        </div>
        
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
              <?php include('common/right-panel.php');?>
            </div>
          </div>
          
        </div>
      </div>
    </div><!-- /.container -->
	
	
<script type="text/javascript" src="<?=WWW?>js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?=WWW?>js/select2.js"></script>
<link href="<?=WWW?>css/jquery-ui.css" type="text/css" rel="stylesheet">
<link href="<?=WWW?>css/select2.css" type="text/css" rel="stylesheet">


<script>
$(function(){
	function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }
	
	$("#content").bind('keydown',function(){
		var length = $(this).val().length+1;
		if(length>160){ var val = $("#content").val().substr(0,159);$("#content").val(val); return; }
		$("#text_counter").val(length);
	});

    /*****************************************************/
    $.ajax({type	: 'POST', 
		   	url		:  'includes/get-users.php', 
			dataType: 'json',
			success	: function(msg){
				$("#to").select2({tags:msg});
			}
    });
	/*****************************************************/
	$('#dashboard .black-box h2').click(function(){
		$('#dashboard .black-box ul').css('display','none');
		var id = this.id;
		$('#'+id+'-box ul').css('display','block');
	});
	//$('#testimonial_form').validationEngine();
});
</script>
<style>
.ui-autocomplete{margin: 460px 467px; min-height:200px;width:600px;}
img.autocomplete-photo{width:25px; height:25px; padding-right:5px;}
.form-box a.select2-search-choice-close{padding:0px; background:url("../images/select2.png") no-repeat scroll right top rgba(0, 0, 0, 0);}
</style>

<?php include('common/footer.php'); ?>