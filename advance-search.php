<?php include('common/header.php');
include ('includes/my_pagination.php');
extract($_GET);
?>

    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="white-box_listting">
            <ul class="menu" id="menu">
              <li><a href="#" id="individual-tab" <?php if($_GET['company']!="Search"){?>class="active"<?php }?>>Individuals</a></li>
              <li><a href="#" id="companies-tab" class="<?php if($_GET['company']=="Search"){ echo 'active'; } ?>">Clubs/Leagues/Organizations</a></li>
			</ul>
			<div class="content1" id="individual" style="padding-top:15px; display:<?php if($_GET['company']!="Search"){ echo 'block'; }else{ echo 'none'; }?>">
              <form method="GET" action="" class="form-horizontal">
                <div class="form-group">
                  <label class="col-sm-3 control-label"> Searching For</label>
                  <div class="col-sm-3">
                    <select name="type" class="form-control">
					<?php $sql = mysqli_query($conn,"SELECT * FROM user_types WHERE id NOT IN(1,14,17) ORDER BY `name`");?>
						<?php while( $row = mysqli_fetch_assoc($sql)){?>
							<option value="<?php echo $row['id'];?>"
                <?php if($_GET['individual']=="Search"){
                  if($type==$row['id']){ echo 'selected';}
                }else{
                  if($row['name']=="Player"){ echo 'selected';}
                } ?>>
                <?php echo $row['name'];?></option>
				<?php }?>
			  		</select>
                  </div>
                  <label class="col-sm-3 control-label"> Country </label>
                  <div class="col-sm-3">
                    <select name="country" id="c_id" onchange="states();" class="form-control">
						<?php $sql = mysqli_query($conn,"SELECT * FROM countries");?>
						<?php while($row = mysqli_fetch_assoc($sql)){?>
							<option value="<?php echo $row['id'];?>"
                <?php if($_GET['individual']=="Search"){
                  if($country == $row['id']){ echo 'selected'; }
                }else{
                  if($row['id'] == '239'){ echo 'selected';}
                } ?> > <?php echo $row['name'];?></option>
				<?php }?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label"> States </label>
                  <div class="col-sm-3">
                    <div id="state_data">
						<?php if(isset($state)){
							$sql = mysqli_query($conn,"SELECT * FROM states WHERE country_id = '$country'"); ?>
							<select name='state' >
							<option>All</option>
							<?php while($row = mysqli_fetch_assoc($sql)){?>
							<option value="<?php echo $row['id'];?>" <?php if($row['id'] == $state){?> selected <?php }?>> <?php echo $row['name'];?></option>
							<?php }?>
							</select>
						<?php  } else {?>
								Select country, state values appear here
						<?php }?>
			  		</div>
                  </div>
                  <label class="col-sm-3 control-label">City</label>
                  <div class="col-sm-3">
                    <input name="city" value="<?php if($_GET['individual']=="Search"){ echo $city; }?>" class="form-control" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label"> First class Player </label>
                  <div class="col-sm-3">
                    <select name="firstclassplayer" class="form-control">
						<option> All </option>
						<option value="1" <?php if($firstclassplayer == 1){ ?> selected <?php }?>> Yes </option>
						<option value="0" <?php if($firstclassplayer == 0){ ?> selected <?php }?>> No </option>
                    </select>
                  </div>
                  <label class="col-sm-3 control-label">Type of Player</label>
                  <div class="col-sm-3">
                    <select name="typeofplayer" class="form-control">
						<option> All </option>
						<option value="Bowler" <?php if($typeofplayer == 'Bowler'){ ?> selected <?php }?>> Bowler </option>
						<option value="Batsman" <?php if($typeofplayer == 'Batsman'){ ?> selected <?php }?>> Batsman </option>
						<option value="All Rounder" <?php if($typeofplayer == 'All Rounder'){ ?> selected <?php }?>> All Rounder </option>
						<option value="Specialist Wicketkeeper" <?php if($typeofplayer == 'Specialist Wicketkeeper'){ ?> selected <?php }?>> Specialist Wicketkeeper </option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label"> Type of Batsman </label>
                  <div class="col-sm-3">
                    <select name="typeofbatsman" class="form-control">
						<option> All </option>
						<option value="Right-handed"  <?php if($typeofbatsman == 'Right-handed'){ ?> selected <?php }?>> Right Hand </option>
						<option value="Left-handed"  <?php if($typeofbatsman == 'Left-handed'){ ?> selected <?php }?>> Left Hand </option>
                    </select>
                  </div>
                  <label class="col-sm-3 control-label">Type of Bowler</label>
                  <div class="col-sm-3">
                    <select name="typeofbowler" class="form-control">
						<option> All </option>
						<option value="Fast"<?php if($typeofbowler == 'Fast'){ ?> selected <?php }?>> Fast </option>
						<option value="Medium-fast"<?php if($typeofbowler == 'Medium-fast'){ ?> selected <?php }?>> Medium-Fast </option>
						<option value="Leg Spinner"<?php if($typeofbowler == 'Leg Spinner'){ ?> selected <?php }?>> Leg Spinner </option>
						<option value="Off Spinner"<?php if($typeofbowler == 'Off Spinner'){ ?> selected <?php }?>> Off Spinner </option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">First Name</label>
                  <div class="col-sm-3">
                    <input name="firstname" value="<?php echo $firstname;?>" class="form-control" type="text">
                  </div>
                  <label class="col-sm-3 control-label"> Last Name </label>
                  <div class="col-sm-3">
                    <input name="lastname"  value="<?php echo $lastname;?>" class="form-control" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-12 text-center">
                    <input value="Search" name="individual" class="btn orange hvr-float-shadow" type="submit">
                  </div>
                </div>
              </form>
			</div>
			<div style="display: <?php if($_GET['company']=="Search"){ echo 'block'; }else{ echo 'none'; }?>; padding-top:15px" class="content1" id="companies">
              <form method="GET" action="" class="form-horizontal">
                <div class="form-group">
                  <label class="col-sm-3 control-label"> Searching For</label>
                  <div class="col-sm-3">
                    <select name="type" class="form-control">
					<?php $sql = mysqli_query($conn,"SELECT * FROM company_types ORDER BY `name` ");?>
						<?php while( $row = mysqli_fetch_assoc($sql)){?>
							<option <?php if($_GET['company']=="Search"){ if($type == $row['id']){ echo 'selected'; } } else{ if($row['name'] == 'Club') { echo 'selected'; } } ?> value="<?php echo $row['id'];?>"> <?php echo $row['name'];?></option>
						<?php }?>
                    </select>
                  </div>
                  <label class="col-sm-3 control-label"> Country </label>
                  <div class="col-sm-3">
                    <select name="country" id="country_id" onchange="states_company();" class="form-control">
					<?php $sql = mysqli_query($conn,"SELECT * FROM countries"); ?>
						<?php while($row = mysqli_fetch_assoc($sql)){?>
							<option <?php if($_GET['company']=="Search"){ if($country == $row['id']){ echo 'selected'; } } else{ if($row['id'] == '239'){ echo 'selected'; } } ?>  value="<?php echo $row['id'];?>"> <?php echo $row['name'];?></option>
						<?php }?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">State</label>
                  <div class="col-sm-3">
					<div id="state_company_data" >
						Select country, state values appear here
					</div>
                  </div>
                  <label class="col-sm-3 control-label"> City </label>
                  <div class="col-sm-3">
                    <input name="city" class="form-control" type="text" value="<?php if($_GET['company']=="Search") echo $_GET['city'];  ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Club/League/Organization Name</label>
                  <div class="col-sm-3">
                    <input name="companyname" value="<?php if(isset($_GET['companyname']) && !empty($_GET['companyname'])) echo $_GET['companyname'];  ?>" class="form-control" type="text">
                  </div>
                  <label class="col-sm-3 control-label"> Years Of Service </label>
                  <div class="col-sm-3">
                    <select name="yearofservice" class="form-control">
						<option> All </option>
						<?php for($i =1; $i<=350; $i++){?>
							<option <?php if(isset($_GET['yearofservice']) && $_GET['yearofservice'] == $i) echo 'selected'; ?> value="<?=$i?>"> <?php echo $i; ?> </option>
						<?php }?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12 text-center">
                    <input value="Search" name="company" class="btn orange hvr-float-shadow" type="submit">
                  </div>
                </div>
              </form>
            </div>




		  </div>
        </div>
      </div>
	 </div>

	 <div class="container-fluid">

		<?php if(!isset($_SESSION['ycdc_user_name']) || empty($_SESSION['ycdc_user_name'])){ ?>
			<?php if(isset($_GET['individual']) || isset($_GET['company']) ){ ?>

				<div class="col-md-12">
					<div class="white-box">
						<div class="alert alert-warning">Please Login or Register!</div>
						<p><?php $rowWhoopsMsg = get_record_on_id('cms', 23); echo str_replace('{type}',$page_type["$page"].$region,$rowWhoopsMsg['content']);?></p>
						<?php include('common/login-box.php');?>
					</div>
				</div>

			<?php } ?>
		<?php } else { ?>

		<?php
		if(isset($_GET['individual'])){
			$search_result = " Individuals Search Result";
		extract($_GET);
		if(isset($_GET['individual'])){
		$keywords= '?individual=';

		if(isset($_GET['is_company'])){
			$sql ="SELECT * FROM users WHERE status = 1 AND is_company = '0' ";
		}
		else {
		$sql ="SELECT * FROM users WHERE status = 1 AND  is_company = '0' ";
		}
		$keywords .= "&is_company=0";
		if(!empty($type) && $type != 'All'){

			$sql .=" AND user_type_id = ' $type' ";
			$keywords .= "&type=$type";
		}


		if(!empty($country) && $country != 'All'){
			$sql .=" AND country_id = '$country'";
			$keywords .= "&country=$country";
		}

		if(!empty($state) && $state != 'All'){
			$sql .=" AND state_id = '$state'";
			$keywords .= "&state=$state";

		}

		if(!empty($city)){
			$sql .=" AND city_name LIKE '%$city%'";
			$keywords .= "&city=$city";
		}

		if(!empty($firstclassplayer) && $firstclassplayer != 'All'){
			$sql .=" AND first_class_player = '$firstclassplayer'";
			$keywords .= "&firstclassplayer=$firstclassplayer";
			}
		if(!empty($typeofplayer) && $typeofplayer != 'All'){
			$sql .=" AND type = '$typeofplayer'";
			$keywords .= "&typeofplayer=$typeofplayer";
		}
		if(!empty($typeofbatsman) && $typeofbatsman != 'All'){
			$sql .=" AND type_of_batsman = '$typeofbatsman'";
			$keywords .= "&typeofbatsman=$typeofbatsman";
		}


		if(!empty($typeofbowler) && $typeofbowler != 'All'){
			$sql .=" AND type_of_bowler = '$typeofbowler'";
			$keywords .= "&typeofbowler=$typeofbowler";
		}


		if(!empty($firstname)){
			$sql .=" AND f_name LIKE'%$firstname%'";
			$keywords .= "&firstname=$firstname";
			}

		if(!empty($lastname)){
			$sql .=" AND last_name LIKE  '%$lastname%'";
			$keywords .= "&lastname=$lastname";
		}

		$rw            = mysqli_query($conn,$sql);
		$total_rows    = mysqli_num_rows($rw);
		$advance_link  = "advance-search.html";
		$returnData    = pagination($total_rows,$sql,$keywords,$advance_link);
		$limit      =$returnData['limit'];
		$pagination =$returnData['pagination'];

		$sql .=" ".$limit;

		?>
		<div class="white-box">
		<div class="row">
        <div class="col-md-12">




					<fieldset>

			  		<? 	$rs_users = mysqli_query($conn,$sql);
			  		   $total_record = mysqli_num_rows($rs_users);

			  		   if($total_record > 0 ){

			  			while($row_g= mysqli_fetch_assoc($rs_users)){

						$row_img = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_g['id']."' and is_default = '1' "));
                		$row_vid = mysqli_fetch_assoc(mysqli_query($conn,"select * from videos where entity_type = 'users' and entity_id = '".$row_g['id']."' and is_default = '1' "));
			  			if(!empty($row_g['city_name'])){
			  				$row_states = get_record_on_id('states', $row_g['state_id']);
				           	$row_country= get_record_on_id('countries', $row_g['country_id']);
				           	$location   =  $row_g['city_name'].' > '.$row_states['name'].' > '.$row_country['name'];
			  			}else{
			  				$row_country= get_record_on_id('countries', $row_g['country_id']);
			  				$location 	= $row_country['name'];
			  			}
      					?>

      					<dl>
								<dt>
									<a href="<?=WWW?>individual-detail-<?=$row_g['id']?>-<?=friendlyURL($row_g['f_name'].' '.$row_g['m_name'].' '.$row_g['last_name'])?>.html" title="<?=$row_g['f_name'].' '.$row_g['m_name'].' '.$row_g['last_name']?>"><img src="<?php echo WWW;?><?=($row_img && !empty($row_img['file_name']))?'users/'.$row_g['id'].'/photos/'.$row_img['file_name']:'images/no-photo.jpg'?>"  /></a></dt>
								<dd><div class="details">
									<h3><a href='<?=WWW?>individual-detail-<?=$row_g['id']?>-<?=friendlyURL($row_g['f_name'].' '.$row_g['m_name'].' '.$row_g['last_name'])?>.html'><?=$row_g['f_name'].'<br> '.$row_g['last_name']?></a></h3>
									<p><? echo $location;?><br />
									<?=get_combo('user_types','name',$row_g['user_type_id'],'','text');?><br />
									<?//=($row_g['first_class_player'] == '1')?'First Class Player<br />':''?>
									<?=$row_g['type']?><br />
									</p>
									<a class="submit-login margin-top-5" href="<?=WWW?>individual-detail-<?=$row_g['id']?>-<?=friendlyURL($row_g['f_name'].' '.$row_g['m_name'].' '.$row_g['last_name'])?>.html" title="<?=$row_g['f_name'].' '.$row_g['m_name'].' '.$row_g['last_name'].' - '.$site_title?>">View Profile</a>
									<span class="flag"><img title="<?=$row_country['name']?>" alt="<?=$row_country['name']?>" src="<?php echo WWW;?>countries/<?=$row_country['flag']?>"  width="25"/></span>
									</div>

									<div class="video">
										<?
											$video = $row_vid['file_name'];
											if(!empty($video)){
						if(preg_match('/<iframe(.*)<\/iframe>/', $row_vid['file_name'])){

									preg_match('/src="(.*?)"/',$row_vid['file_name'] , $src);
									$src = $src[1];
									$video = "<iframe width='205' height='130' frameborder='0' allowfullscreen src='$src'></iframe>";


						}else{

												$filename = explode('.',$video);
												$filename1= $filename[0];
												$video = '<video width="220" height="130" controls>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mp4" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.ogg" type=\'video/ogg; codecs="theora, vorbis"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mov" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.webm" type=\'video/webm; codecs="vp8, vorbis"\'>
												</video>';
											}
										}else{
											$video = '<img src="'.WWW.'images/no-video.jpg" width="168" height="130">';
										}
										echo $video;?>
									</div>
								</dd>
						</dl>
					<?	} ?>
<div class="clearfix"></div>
					<?php  }// Total Result ends here
										else {

											echo "<br/>No Record Found  ";
										}
		}
										?>

										<?php
										 echo $pagination;
										 ?>

					</fieldset>

				</div>

				</div>
				</div>





			<?php }?>



		<?php
		if(isset($_GET['company'])){
			$search_result = " Clubs/Leagues/Companies Search Result";
		extract($_GET);

		if(isset($_GET['company'])){
		$keywords= '?company=';

		if(isset($_GET['is_company'])){
			$sql ="SELECT * FROM companies INNER JOIN users ON companies.user_id = users.id WHERE users.is_company = '1' and users.status = '1' and companies.status=1 ";
		}
		else {
			$sql ="SELECT * FROM companies INNER JOIN users ON companies.user_id = users.id WHERE users.is_company = '1' and users.status = '1' and companies.status=1";
		}
		$keywords .= "&is_company=1";
		if(!empty($type) && $type != 'All'){

			$sql .=" AND companies.company_type_id = ' $type' ";
			$keywords .= "&type=$type";
		}


		if(!empty($country) && $country != 'All'){
			$sql .=" AND users.country_id = '$country'";
			$keywords .= "&country=$country";
		}

		if(!empty($state) && $state != 'All'){
			$sql .=" AND users.state_id = '$state'";
			$keywords .= "&state=$state";

		}

		if(!empty($city)){
			$sql .=" AND users.city_name LIKE '%$city%'";
			$keywords .= "&city=$city";
		}



		if(!empty($companyname)){
			$sql .=" AND companies.company_name LIKE'%$companyname%'";
			$keywords .= "&companyname=$companyname";
			}
		if(!empty($yearofservice) && $yearofservice != 'All'){
				$sql .=" AND companies.yearofservice = '$yearofservice'";
				$keywords .= "&yearofservice=$yearofservice";
			}



		$rw            = mysqli_query($conn,$sql);//echo $sql;
		$total_rows    = mysqli_num_rows($rw);
		$advance_link  = "advance-search.html";
		$returnData    = com_pagination($total_rows,$sql,$keywords,$advance_link);
		$limit      =$returnData['limit'];
		$pagination =$returnData['pagination'];

		$sql .=" ".$limit;

		?>
		<div class="row">
        <div class="col-md-12">
          <div class="white-box_listting">

		<div class="content1" id="individual">
				<!--<ul id="menu" class="menu" >
					<li class="active" ><a href="#" style="width: 100%;"> <?php echo $search_result;?></a></li>
				</ul>-->
					<!--<fieldset >-->
					<center><h3> <?php echo $search_result;?></h3></center>
			  		<? 	$rs_users = mysqli_query($conn,$sql);
			  		   $total_record = mysqli_num_rows($rs_users);

			  		   if($total_record > 0 ){

			  			while($row_g= mysqli_fetch_assoc($rs_users)){
			  			$row_comp= mysqli_fetch_assoc(mysqli_query($conn,"select * from companies where user_id = '".$row_g['id']."'"));
						$row_img = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_g['id']."' and is_default = '1' "));
                		$row_vid = mysqli_fetch_assoc(mysqli_query($conn,"select * from videos where entity_type = 'users' and entity_id = '".$row_g['id']."' and is_default = '1' "));
			  			if(!empty($row_g['city_id'])){
			  				$row_city   = get_record_on_id('cities', $row_g['city_id']);
				           	$row_states = get_record_on_id('states', $row_g['state_id']);
				           	$row_country= get_record_on_id('countries', $row_comp['country_id']);
				           	if(!$row_country){
				           		$row_country= get_record_on_id('countries', $row_g['country_id']);
				           	}
				           	$location   =  $row_g['city_name'].' > '.$row_states['name'].' > '.$row_country['name'];
			  			}else{
			  				$row_country= get_record_on_id('countries', $row_comp['country_id']);
			  				$location 	= $row_country['name'];
			  			}
      					?>

      					<dl>
								<dt>
									<a href="<?=WWW?><?php echo $row_comp['company_permalink']?>" title="<?=$row_g['f_name'].' '.$row_g['m_name'].' '.$row_g['last_name'].' - '.$site_title?>"><img src="<?php echo WWW;?><?=($row_img && !empty($row_img['file_name']))?'users/'.$row_g['id'].'/photos/'.$row_img['file_name']:'images/no-photo.jpg'?>" width="120" height="128" /></a></dt>
								<dd><div class="details">
									<h3><a href="<?=WWW?><?php echo $row_comp['company_permalink']?>" title="<?=$row_g['f_name'].' '.$row_g['m_name'].' '.$row_g['last_name'].' - '.$site_title?>"><?=$row_g['company_name']?></a></h3>
									<p><? echo $location;?><br />
									<?=get_combo('company_types','name',$row_g['company_type_id'],'','text');?><br />
									Years in business : <?=$row_g['years_in_business']?></p>
									<a class="submit-login margin-top-5" href="<?=WWW?><?php echo $row_comp['company_permalink']?>" title="<?=$row_g['f_name'].' '.$row_g['m_name'].' '.$row_g['last_name']?>">View Profile</a>

									<div class="flag"><img title="<?=$row_country['name']?>" alt="<?=$row_country['name']?>" src="<?php echo WWW;?>countries/<?=$row_country['flag']?>"  width="25"/></div>
									</div>
									<div class="video">
										<?
										$video = $row_vid['file_name'];
										$video_name = $row_vid['file_name'];
										if(!empty($video)){
											if(preg_match('/<iframe(.*)><\/iframe>/', $video)){
							        			preg_match('/src="(.*?)"/',$row_vid['file_name'] , $src);
												$src = $src[1];
												$video = "<iframe width='205' height='130' frameborder='0' allowfullscreen src='$src'></iframe>";
											}else{
												$filename = explode('.',$video);
												$filename1= $filename[0];
												$video = '<video width="200" height="150" controls style="margin-top: -25px;">
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$video_name.'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$video_name.'" type=\'video/ogg; codecs="theora, vorbis"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$video_name.'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$video_name.'" type=\'video/webm; codecs="vp8, vorbis"\'>
												</video>';
											}
										}else{
											$video = '<img src="'.WWW.'images/no-video.jpg" width="220" height="130">';
										}
										echo $video;?>
									</div>
								</dd>
						</dl>
					<?	} ?>

					<?php  }// Total Result ends here
										else {

											echo "<br/>This Club/League/Company is not Yet registered with Youcricketer. Please advise the Club/League/Company to Register with Youcricketer.com. ";
										}
		}
										?>

										<?php
										 echo $pagination;
										 ?>

					<!--</fieldset>-->
			    </div>
				</div></div>


				</div>


			<?php }?>

		<?php } ?>


    </div><!-- /.container -->


	<script type="text/javascript">
	function states(){
		var xml;
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xml=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xml=new ActiveXObject("Microsoft.XMLHTTP");
		  }

		 var c_id 		= document.getElementById("c_id").value;
		 var id = "id="+c_id;
		 var url    		= 'states.php';
		    xml.open("POST",url,true);
		    xml.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		    xml.onreadystatechange = function(){

		    	if(xml.readyState == 4 && xml.status == 200) {

		  		    var return_data = xml.responseText;
					document.getElementById("state_data").innerHTML = return_data;
					 //document.getElementById("status").innerHTML = "";



		        }
		    }
		    xml.send(id);
			   document.getElementById("state_data").innerHTML = "Loading <img src='images/ajax-loader.gif' />";


		}
		states();
	function states_company(){
		var xml;

		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xml=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xml=new ActiveXObject("Microsoft.XMLHTTP");
		  }

		 var c_id 		= document.getElementById("country_id").value;
		 var id = "id="+c_id;
		 var url    		= 'states.php';
		    xml.open("POST",url,true);
		    xml.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		    xml.onreadystatechange = function(){

		    	if(xml.readyState == 4 && xml.status == 200) {

		  		    var return_data = xml.responseText;
					document.getElementById("state_company_data").innerHTML = return_data;
					 //document.getElementById("status").innerHTML = "";



		        }
		    }
		    xml.send(id);
			   document.getElementById("state_company_data").innerHTML = "Loading <img src='images/ajax-loader.gif' />";


		}
		states_company();
	</script>

<?php include('common/footer.php');  ?>
