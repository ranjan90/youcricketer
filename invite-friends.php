<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>

<? 	if(isset($_POST) && count($_POST['to_email']) > 0){ 
				$date 		= date('Y-m-d H:i:s');
				$user_id 	= $_SESSION['ycdc_dbuid'];
				$from_name	= $_SESSION['ycdc_user_name'];
				$to 		= $_POST['emails'];
				foreach($_POST['to_email'] as $email){
					if(!empty($email)){
						$rs_u 	= mysqli_query($conn,"select * from users where email = '".$email."'");
						$rs_i 	= mysqli_query($conn,"select * from invitations where to_emails = '$email' and from_user_id = '$user_id'");
						$status = '';
						
						if(mysqli_num_rows($rs_i) == 1){
							$status = 'Invitation Already Sent';
						}else if(mysqli_num_rows($rs_u) == 1){
							$rowUser 	= mysqli_fetch_assoc($rs_u);
							$status 	= 'Already joined';
							$rsFriend 	= mysqli_query($conn,"select * from friendship where from_user_id = '$user_id' and to_user_id = '".$rowUser['id']."' ");
							if(mysqli_num_rows($rsFriend) == 0){
								mysqli_query($conn,"insert into friendship (from_user_id, to_user_id, create_date, status) values ('$user_id','".$rowUser['id']."','$date','0'); ");
								$status 	= 'Friend Request Sent';
							}else{
								$status 	= 'Already Friend';
							}
							mysqli_query($conn,"insert into invitations (from_user_id, to_name, to_emails, create_date,status) 
								values ('$user_id','$name','$email','$date','$status');");
						}else{
							$status 		= 'Invitation Sent';
							mysqli_query($conn,"insert into invitations (from_user_id, to_name, to_emails, create_date,status) 
								values ('$user_id','$name','$email','$date','$status');");
							$id = mysqli_insert_id($conn);
							if($status != 'Already Joined'){
								$email_template = get_record_on_id('cms', 12);							
								$mail_title		= $email_template['title'].' from '.$from_name;
			                	$mail_content	= $email_template['content'];
			                	$mail_content 	= str_replace('{to_name}',$name, $mail_content);	
			                	$mail_content 	= str_replace('{from_name}',$from_name, $mail_content);	
			                	$mail_content 	= str_replace('{site_link}','<a href="'.WWW.'" title="Visit The Site">YouCricketer</a>', $mail_content);
			                	$mail_content 	= str_replace('{register_link}','<a href="'.WWW.'sign-up.html?ref='.$id.'" title="Sign Up">HERE</a>', $mail_content);
			                	$mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);
								$headers 		= "Content-type:text/html;charset=UTF-8" . "\r\n";
								$headers 	   .= "From: no-reply@youcricketer.com \r\n";
			                	mail($email,$mail_title,$mail_content,$headers);
			                }
						}
					}
				}
				echo '<div id="success" class="alert alert-success">Invitation Sent Successfuly... !</div>';
			}
		?>
		

<div class="page-container"> 
		<?php  include('common/user-left-panel.php');?>
      <!-- END SIDEBAR -->
      
      <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
		<div class="page-content">
		
		<div class="white-box">
            
            <div id="pagination-top" style="margin-bottom:15px;">
              <div class="row">
                <div class="col-sm-8">
                  <h3> Invite Friends </h3>
                </div>
                <div class="col-sm-4">
                    <div class="input-group1">
                      <input class="form-control validate[required] input-login" name="searchhere" placeholder="Search Your Friends" type="text">
                      <!--<span class="input-group-btn">
                        <button class="btn btn-default" type="button">Go!</button>
                      </span>-->
                    </div>
                </div>
              </div>
            </div>
            
            <div class="form-group" id="friends">
            </div>
			
            <form method="post" id="invite_friends" action="" enctype="multipart/form-data" class="form-horizontal req-frm">
              <div class="form-group">
                <div class="col-sm-12">
                  <h3> Invite By Email to Join Your Social Network </h3>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-4">
                  <input name="to_email[]" placeholder="Enter Email address" class="form-control validate[required, custom[email]]" type="text">
                </div>
                <div class="col-sm-4">
                  <input name="to_email[]" placeholder="Enter Email address" class="form-control validate[custom[email]]" type="text">
                </div>
                <div class="col-sm-4">
                  <input name="to_email[]" placeholder="Enter Email address" class="form-control validate[custom[email]]" type="text">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-4">
                  <input name="to_email[]" placeholder="Enter Email address" class="form-control validate[custom[email]]" type="text">
                </div>
                <div class="col-sm-4">
                  <input name="to_email[]" placeholder="Enter Email address" class="form-control validate[custom[email]]" type="text">
                </div>
                <div class="col-sm-4">
                  <input name="to_email[]" placeholder="Enter Email address" class="form-control validate[custom[email]]" type="text">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-4">
                  <a href="#" onclick="FacebookInviteFriends()" class="btn blue hvr-float-shadow">Invite Facebook Friends</a>
                </div>
                <div class="col-sm-8">
                  <input name="friends_del_submit" id="friends_del_submit" value="Invite" class="btn orange hvr-float-shadow" type="submit">
                </div>
              </div>
            </form>
          </div>
		
		
		</div>
	</div>
</div>		

<script>
$(document).ready(function(){
	
	var WWW = "<?=WWW?>";
	$('input[name=searchhere]').keyup(function(){
		var letter 	= $(this).val();
		if(letter == ''){
			$('#friends').html('');
		}else{
			$.ajax({type	: 'POST', 
					url		: WWW + 'includes/get-users-for-friends.php', 
					data	: ({letter:letter}),
					success	: function(msg){
						$('#friends').html(msg);
					}
			});
		}
	});
	
	//$('#friend input[type=checkbox]').click(function(){
	$(document).on('click', '#friend input[type=checkbox]' , function() {
		var id = $(this).attr('id');
		//if($('input[type=checkbox]#'+id+':checked').length == '1'){
		if($(this).is(":checked")){
			// checked
			$.ajax({type	: 'POST', 
					url		: WWW + 'includes/invitations.php', 
					data	: ({type:'sendRequest',id:id}),
					success	: function(msg){
						if(msg == 'done'){
							$('#friends').find('span#'+id).html('Invitation Sent');
							$('#friends').find('input[type=checkbox]#'+id).attr('disabled', 'disabled');
							$('#friends').find('input[type=checkbox]#'+id).parent().css('background', '#e0fee1');	
						}else{
							$('#friends').find('span#'+id).html('Invitation not Sent');
						}						
					}
				});
		}else{
			// unchecked
			$.ajax({type	: 'POST', 
					url		: WWW + 'includes/invitations.php', 
					data	: ({type:'removeRequest',id:id}),
					success	: function(msg){
						$('#friends').find('span#'+id).html('Invitation cancel');
						$('#friends').find('span#'+id).css('background', '#565656');
					}
				});
		}
	});
	
	$(".req-frm").validationEngine();
	
});
</script>

<script src="http://connect.facebook.net/en_US/all.js"></script>
<script>
FB.init({
appId:'1660101807542186',
cookie:true,
status:true,
xfbml:true
});
 
function FacebookInviteFriends()
{
FB.ui({
method: 'apprequests',
message: 'Your Message diaolog'
});
}
</script>
<?php include('common/footer.php'); ?>