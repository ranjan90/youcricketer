<?php include('common/header.php');
$selected_country = getGeoLocationCountry(); 

if(isset($_GET['page']) && !empty($_GET['page'])){
	$page = str_replace('page','',trim($_GET['page']));
}else{
	$page = 1;
}

$records_per_page = 10;
$start_record  = ($page-1)*$records_per_page;
?>
<style>
.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
</style>
	<div class="middle">
		<h1> Tournaments </h1>
		<div class="white-box content">
			<div id="pagination-top"></div>
			<div class="list">
                <ul id="individual" class="content1">
                	<?php  $user_id = $_SESSION['ycdc_dbuid'];
					$sql = "SELECT t.*,count(tm.id) as matches_count FROM tournaments as t left join `tournament_matches` as tm on t.id=tm.tournament_id WHERE t.status=1 group by t.id ORDER BY t.start_time DESC LIMIT $start_record,$records_per_page";
                	$rs = mysqli_query($conn,$sql);
					
					$sql = "SELECT count(*) as record_count FROM tournaments as t  WHERE t.status=1 ";
                	$rs_total = mysqli_query($conn,$sql);
					$row_total = mysqli_fetch_assoc($rs_total);
					$records_count = $row_total['record_count'];
					
					while($row = mysqli_fetch_assoc($rs)){
		        		    ?>
		        		    
						<dl>
									<dt>
									<a href="javascript:;" title="<?php echo ucwords($row['title']); ?>"><img src="<?php echo WWW;?><?php echo 'images/no-photo.jpg'; ?>" width="120" height="128" /></a>
									</dt>
								<dd><div class="details">
									<h3><?php echo ucwords($row['title']); ?></h3>
									<p><?php echo date('d F, Y',strtotime($row['start_time']));?> - <? echo date('d F, Y',strtotime($row['end_time']));?><br /></p>
									<p><?php echo $row['matches_count'];?> Matches Played<br /></p>
									<a class="submit-login margin-top-5" href="<?=WWW?>tournament-matches/<?php echo $row['id'] ?>" >Matches</a></div>
									<?php /* ?><div class="flag"><img title="<?=$row_country['name']?>" alt="<?=$row_country['name']?>" src="<?php echo WWW;?>countries/<?=$row_country['flag']?>"  width="25"/></div> <?php */ ?>
									<div class="video">
										<?php 
										$video = '<img src="'.WWW.'images/no-video.jpg" width="168" height="130">';
										echo $video;?>									
									</div>
								</dd>
						</dl>
						<?php
					      $i++;
					      $count++;
					      $x++;
					  } 
					  ?>
			</ul>
			</div>
			<div id="pagination-bottom">
				<?php  $paging_str = getPaging('tournaments',$records_count,$page,$records_per_page);
				echo $paging_str;
				?>
		        <div class="clear"></div>
		    </div>  
		    <div class="clear"></div>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>

<?php 


include('common/footer.php'); ?>