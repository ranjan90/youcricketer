<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>

	<?
		if(isset($_POST) && !empty($_POST)){
				$user_id	= $_SESSION['ycdc_dbuid'];
				$old	= $_POST['old_password'];
				$new	= $_POST['new_password'];

				$row	= get_record_on_id('users',$user_id);
				if(md5($old) == $row['password']){
					$query = "update users set password = '".md5($new)."' where id = '$user_id'";
					if(mysqli_query($conn,$query)){

						$userid = $_SESSION['ycdc_dbuid'];
						$date = date('m/d/Y h:i:s a', time());
						$newTime = date('l, jS F Y', strtotime($date));
						$sql=mysqli_fetch_assoc(mysqli_query($conn,"select email,f_name,last_name from users where id = '$userid'"));
						$email_template = get_record_on_id('cms', 20);
						$mail_title		= $email_template['title'];
		                $mail_content	= $email_template['content'];
		                $mail_content 	= str_replace('{name}',$sql['f_name'], $mail_content);
					    $mail_content 	= str_replace('{last_name}',$sql['last_name'], $mail_content);
						$mail_content 	= str_replace('{date}',$newTime, $mail_content);
						 $mail_content 	= str_replace('{login_link}','<a href="'.WWW.'login.html" title="Contact Us">HERE</a>', $mail_content);
		               $mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);


							$headers = "Content-type:text/html;charset=UTF-8" . "\r\n";
							$headers .= "From: YouCricketer@no-reply.com\r\n";



						mail($sql['email'],$mail_title,$mail_content,$headers);

						echo '<div id="success" class="alert alert-success">Information Updated ... !</div>';

						?>

						<?
					}
				}else{
					echo '<div id="error" class="alert alert-danger">Please enter correct old password ... !</div>';
				}
			}
		?>

<div class="page-container">
	<?php include('common/user-left-panel.php');?>

	<!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">

            <div class="white-box">
				<div class="row">
				  <div class="col-sm-12">
					<h2> Change Password </h2>
				  </div>
				</div>

				<form method="post" action="" enctype="multipart/form-data" class="form-horizontal req-frm">
              <input name="action" value="submit-activity" type="hidden">
              <div class="form-group">
                <label class="col-sm-4 control-label"> Current Password: </label>
                <div class="col-sm-4">
                  <input name="old_password" class="form-control validate[required]" type="password">
                </div>
                <div class="col-sm-4">

                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label"> New Password: </label>
                <div class="col-sm-4">
                  <input name="new_password" id="new_password" class="form-control validate[required,minSize[8]]"  type="password">
                  <span class="help-block">Atleast One digit, One alphabet and minimum 8 characters</span>
                </div>
                <div class="col-sm-4">
                  <div id="password-result"></div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label"> Confirm Password: </label>
                <div class="col-sm-4">
									<div class="left">
                  	<input name="confirm_password" id="confirm_password" class="form-control validate[required, equals[new_password]]" type="password">
									</div>
                </div>
                <div class="col-sm-4">
                  <div id="confirm-password-result"></div>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                  <input class="btn orange hvr-float-shadow" value="Submit" type="submit">
                </div>
              </div>
            </form>

			</div>


        </div>
      </div>
      <!-- END CONTENT-->

</div>


<script>
$(document).ready(function(){
	$('input[name=new_password]').keyup(function(){
		var password = this.value;
		if(!(password.match(/^.*(?=.{7,})(?=.*[a-z])(?=\S*?[0-9]).*$/))){
	    	$('#password-result').html('Atleast One digit, One alphabet and minimum 8 characters');
	    	$('#password-result').css('background','red');
		}

		var score = 0;

		if (password.length >= 8)
	        score++;
	    if (password.length >= 9)
	        score++;
	    if (password.match(/^.*(?=.{7,})(?=.*[a-z])(?=\S*?[0-9]).*$/))
	        score++;

	    if(score == '3'){
	    	$('#password-result').html('Strong Password');
	    	$('#password-result').css('background','green');
	    }else if(score == '1'){
	    	$('#password-result').html('Not Strong Password');
	    	$('#password-result').css('background','orange');
	    }
	    else if(score == '2'){
	    	$('#password-result').html('Not Strong Password');
	    	$('#password-result').css('background','orange');
	    }
	});
	$('input[name=confirm_password]').keyup(function(){
		var password2 = this.value;
		var password1 = $("#new_password").val();
		  if(password1 == password2){
				$('#confirm-password-result').html('Password Matched');
	    	$('#confirm-password-result').addClass('err-green');
	    }else{
				$('#confirm-password-result').html('Password Not Matched');
	    	$('#confirm-password-result').addClass('err-red')
	    }
	});
	/*$('input[name=confirm_password]').keyup(function(){
		if($('input[name=confirm_password]').val() === $('input[name=new_password]').val()){
			$('#confirm-password-result').css('background', 'green');
			$('#confirm-password-result').html('Password Matched');
		}
	});*/

	$(".req-frm").validationEngine();
});
</script>
<?php include('common/footer.php'); ?>
