<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	
		<? 	if(isset($_POST) && !empty($_POST)){ 
				$title 	= $_POST['title'];
				$content= $_POST['content'];
				$date 		= date('Y-m-d H:i:s');
				$user_id 	= $_SESSION['ycdc_dbuid'];
				$isGlobal = ($_POST['is_global'] == 'on')?'1':'0';

				if(!isset($user_id) || empty($user_id)){
					echo '<div id="error" class="alert alert-danger">Please login first ... !</div>';
				}else{
					if(!empty($title) && !empty($content)){
						/*if($_POST['category']){
							$rs_cat = mysqli_query($conn,"select * from news_categories where name = '".$_POST['category']."'");
							if(mysqli_num_rows($rs_cat) == 0){
								mysqli_query($conn,"insert into news_categories (name, status) values ('".$_POST['category']."','1') ");
								$category_id = mysqli_insert_id($conn);
							}else{
								$row_cat = mysqli_fetch_assoc($rs_cat);
								$category_id  = $row_cat['id'];
							}
						}else{
							$category_id = $_POST['category_id'];
						}*/

						$rs_news = mysqli_query($conn,"select * from news where title = '$title'");
						if(mysqli_num_rows($rs_news) == 0){
							$query = "insert into news (title, content, status, news_date, user_id, is_global,record_count,`type`) 
							values ('$title','$content','1','$date','$user_id','$isGlobal','0','')";
							//echo $query;exit;
							if(mysqli_query($conn,$query)){
								$news_id = mysqli_insert_id($conn);

								if(!is_dir('news/'.$news_id)){
									mkdir('news/'.$news_id,0777);
								}
								chmod('news/'.$news_id,0777);

								if(!empty($_FILES['photo']['name'])){
									$filename 	= friendlyURL($title).'.jpg';
									$image 		= new SimpleImage();
									$image->load($_FILES["photo"]["tmp_name"]);
									$image->save('news/'.$news_id.'/'.$filename);
									chmod('news/'.$news_id.'/'.$filename,0777);
								
									mysqli_query($conn,"insert into photos (file_name, title, entity_id, entity_type,is_default,is_thumb) values ('$filename','$title','$news_id','news','0','0')");
									
								}
							}
							
			                echo '<div id="success" class="alert alert-success">News Information added successfully ... !</div>';
			                ?>
			                <script>
			                window.location = '<?=WWW?>my-news.html';
			                </script>
			                <?
						}else{
							echo '<div id="error" class="alert alert-danger">Information already exists ... !</div>';
						}
					}else{
						echo '<div id="error" class="alert alert-danger">Please fill all fields ... !</div>';
					}
				}
				
			}
		?>
		
<script src="<?=WWW;?>assets/js/ckeditor/ckeditor.js"></script>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1> Add News </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <form method="post" action="" enctype="multipart/form-data" class="form-horizontal" id="form-add-news">
            
            <div class="white-box">
              <div class="form-group">
                <div class="col-sm-12 text-center">
                  <h2>News Information</h2>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Title</label>
                <div class="col-sm-9">
                  <input name="title" value="" id="title" class="form-control validate[required]" type="text">
                </div>
              </div>
              
              <div class="form-group">
                <label class="col-sm-3 control-label">Is Global</label>
                <div class="col-sm-9">
                  <input type="checkbox" name="is_global" <?php echo ($row['is_global'] == '1')?'checked="checked"':'';?>>
                  <span class="help-block">News will be Regional News If checked</span>
                </div>
              </div>
              
              
              <div class="form-group">
                <label class="col-sm-3 control-label">Content</label>
                <div class="col-sm-9">
                  <div id="editor">
					<textarea name="content" id="content"  class="ckeditor" class="validate[required]"></textarea>
				  </div>
                </div>
              </div>
              
              <div class="form-group">
                <label class="col-sm-3 control-label">News Photo</label>
                <div class="col-sm-9">
                  <input accept="image/*" name="photo" type="file">
                  <span class="help-block">Max Photo size : 2MB</span>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12 text-center">
                  <input value=" Save " class="btn orange hvr-float-shadow" type="submit">
                  <a id="cancel" class="btn blue hvr-float-shadow" href="<?=WWW?>news.html">Cancel</a>
                </div>
              </div>
            </div>
		  </form>
        </div>
        
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
              <?php include('common/right-panel.php');?>
            </div>
          </div>
          
        </div>
      </div>
    </div><!-- /.container -->
	
<script type="text/javascript">
$(document).ready(function(){
	$('#form-add-news').validationEngine();
});
</script>	

<?php include('common/footer.php'); ?>