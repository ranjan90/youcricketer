<?php include('common/header.php'); ?>
<style>
.matches{}
.match{border: 1px solid #ccc; border-radius:5px; margin: 4px; padding: 8px;}
.match p{width:48%; float: left;}
.content1{width:100%;}
.box{margin:10px 0px;}
h3{margin:5px;}
h3 a{color: #515151;}
h3 div a{font-size:14px; color: blue;}
</style>
	<div class="middle">
		<h1>
			World Cup Cricket 2015
			<a href="<?php echo WWW;?>YC-WorldCup2015.pdf" class="right" title"Click to Download Schedule" target="_blank" style="font-size:13px;">
				Download and Print Entire World Cup Schedule <img src="<?php echo WWW;?>images/pdf.png">
			</a>
			<div class="clear"></div>
		</h1>

		<div style="margin-left:150px; height:150px; margin-top:15px; margin-bottom:30px;">
			<a class="popup-link" href="#blogging-contest-rules" >
				<img src="<?php echo WWW;?>images/world-cup-cricket-blogging-contest-100-doller.jpg">
			</a>
		</div>

		<div class="white-box content detail" style="width:100%;">
			<div id="schedule" class="content1" >
				<div class="box">
					<select name="pool">
						<option <?php echo (!isset($_GET['pool']))?'selected="selected"':'';?> value="">Select Pool</option>
						<option value="A" <?php echo (isset($_GET['pool']) && $_GET['pool'] == 'A')?'selected="selected"':'';?>>Pool A</option>
						<option value="B" <?php echo (isset($_GET['pool']) && $_GET['pool'] == 'B')?'selected="selected"':'';?>>Pool B</option>
					</select>
					<select name="venue">
						<option <?php echo (!isset($_GET['venue']))?'selected="selected"':'';?> value="">Select Venue</option>
						<option value="Australia" <?php echo (isset($_GET['venue']) && $_GET['venue'] == 'Australia')?'selected="selected"':'';?>>Australia</option>
						<option value="New Zealand" <?php echo (isset($_GET['venue']) && $_GET['venue'] == 'New Zealand')?'selected="selected"':'';?>>New Zealand</option>
					</select>
					<select name="country">
						<option <?php echo (!isset($_GET['country']))?'selected="selected"':'';?> value="">Select Country</option>
						<?php $rsCountries = mysqli_query($conn,"select distinct c.name from countries c join series_matches sm on sm.country_1 = c.id or sm.country_2 = c.id order by c.name");
							while($rowC = mysqli_fetch_assoc($rsCountries)){ ?>
							<option value="<?php echo $rowC['name']?>" <?php echo (isset($_GET['country']) && $_GET['country'] == str_replace('+', ' ', $rowC['name']))?'selected="selected"':'';?>><?php echo $rowC['name']?></option>
						<?php } ?>
					</select>
					<a href="<?php echo WWW;?>world-cup-cricket-2015.html" class="right">View All <br>Matches</a>
				</div>
				<div id="matches">
					<?php $select = "select m.* from series_matches m";
						  $join   = '';
						  $where  = " where m.series_id = '1'";
			        	  if(isset($_GET['pool']) && $_GET['pool'] != 'all'){
			        	  	$where .= " and m.pool = '".$_GET['pool']."'";
			        	  }
			        	  if(isset($_GET['venue']) && $_GET['venue'] != 'all'){
			        	  	$join .= " join countries cv on cv.id = m.match_country_id and cv.name = '".str_replace('+', ' ', $_GET['venue'])."'";
			        	  }
			        	  if(isset($_GET['country']) && $_GET['country'] != 'all'){
			        	  	$join .= " join countries cc on (cc.id = m.country_1 or cc.id = m.country_2) and cc.name = '".str_replace('+', ' ', $_GET['country'])."'";
			        	  }
			        	  $query = $select . $join . $where . " order by match_date;";
			        	   // echo $query;
			        ?>
			        <?php $matches = mysqli_query($conn,$query);?>
					<h2>Total Matches : <?php echo (mysqli_num_rows($matches) > 0)?mysqli_num_rows($matches):'0'?></h2>
					<?php while($rowM = mysqli_fetch_assoc($matches)){ ?>
					<div class="match">
						<h3>
							<?php if(empty($rowM['title'])) {
				        		$row1 = get_record_on_id('countries', $rowM['country_1']);
				        		$row2 = get_record_on_id('countries', $rowM['country_2']);
				        		$title = '<a href="'.WWW.'world-cup-cricket-2015-country-profile-'.$row1['id'].'-'.friendlyURL($row1['name']).'.html" title="Click here to view Country Profile">'.$row1['name'].'</a> vs <a href="'.WWW.'world-cup-cricket-2015-country-profile-'.$row2['id'].'-'.friendlyURL($row2['name']).'.html" title="Click here to view Country Profile">'.$row2['name'].'</a> - Pool '.$rowM['pool'].' ('.date('d-M-Y', strtotime($rowM['match_date'])).')';
				        		?>
				        	<?php }else{ ?>
				        	<?php $title = $rowM['title'].' ('.date('d-M-Y', strtotime($rowM['match_date'])).')';;?>
				        	<?php } ?>
				        	<?php echo $title;?>
				        	<div class="right">
				        		<?php
				        		$blogRow = mysqli_fetch_assoc(mysqli_query($conn,"select * from blog_articles where series_match_id = '".$rowM['id']."'"));
								if(empty($blogRow)){
									$query = "insert into blog_articles (category_id, title, series_match_id, create_date, status, user_id) values ('5','".strip_tags($title)."','".$rowM['id']."','".date('Y-m-d H:i:s')."','1','".$_SESSION['ycdc_dbuid']."');";
									mysqli_query($conn,$query);
									$blogId = mysqli_insert_id($conn);
								}else{
									$blogId = $blogRow['id'];
								}
				        		?>
				        		<a class="right" href="<?php echo WWW;?>blog-detail-<?php echo $blogId;?>-<?php echo friendlyURL(strip_tags($title));?>.html">Word Cup Blogs/Discussion Contest</a>				        		
				        	</div>
						</h3>
						<p>Match Date : <?php echo date('d-M-Y', strtotime($rowM['match_date']));?></p>
						<p>GMT Time : <?php echo $rowM['gmt_time'];?></p>
						<p>IST Time : <?php echo $rowM['ist_time'];?></p>
						<p>Venue : 
							<?php
					        	$venue = $rowM['match_ground'].', '.$rowM['match_city'].', ';
		          				$row_country= get_record_on_id('countries', $rowM['match_country_id']);
		          				echo $venue.$row_country['name'];
				        	?>
						</p>
						<div class="clear"></div>
					</div>
			        <?php } ?>
			    </div>
			</div>
		</div>		
		<div class="clear"></div>
	</div>
<div class="popup hide" style="width:600px;" id="blogging-contest-rules">
	<h1 style="font-size: 14px;">Blogging Contest Rules
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h1>
		<? $row = get_record_on_id('cms',42);?>
		<div>
			<?=$row['content']?>
		    <div class="clear"></div>
		</div>
</div>
<script type="text/javascript" src="<?php echo WWW;?>js/jquery-1.8.0.js"></script>
<script>
    $(document).ready(function(){

        $('select').change(function(){
            var pool = $('select[name=pool]').val();
            var venu = $('select[name=venue]').val();
            var count= $('select[name=country]').val();

            var page = '<?php echo WWW;?>world-cup-cricket-2015';

            if(pool != ''){
            	page = page + '-pool-'+ pool;
            }else{
            	page = page + '-pool-all';
            }
            if(venu.length > 0){
             	page = page + '-venue-'+venu.replace(' ', '+');
            }else{
            	page = page + '-venue-all';
            }
            if(count.length > 0){
             	page = page + '-country-'+count.replace(' ', '+');
            }else{
            	page = page + '-country-all';
            }
            window.location = page + '.html';
        });

        $('.close-popup').live('click',function(){
			var id = this.parentNode.parentNode.id;
			if(id){
				$('#' + id).addClass('hide');	
			}else{
				$('.popup').addClass('hide');
			}
		});

		$('a.popup-link').live('click',function(){
			// alert("laskjdf")
			var link = $(this).attr('href');
			var height = $(window).scrollTop();
			$(link).css('top',height-150);

			$(link).removeClass('hide');
		});
    });
</script>
<?php include('common/footer.php'); ?>