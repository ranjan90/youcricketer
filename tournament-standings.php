<?php include_once('includes/configuration.php');
$page = 'tournament-standings.html';
$selected_country = getGeoLocationCountry();

$league_clubs = array();
$league_info = array();
$teams = array();
$league_points = array();
$error = '';

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);
}

$tournament_id = trim($_GET['tour_id']);
$tournament_info = get_record_on_id('tournaments', $tournament_id);

$page_title = ucwords($tournament_info['title']).' Standings';

if(isset($_POST['submit_btn']) && !empty($_POST['submit_btn'])){
	extract($_POST);
	if(empty($penality_points)) $penality_points = 0;
	$sql = "SELECT id from league_points WHERE league_id = ".$tournament_info['league_id']." AND tournament_id=".$tournament_info['id'];
	if(mysqli_num_rows(mysqli_query($conn,$sql)) == 0){
		$sql = "INSERT INTO league_points SET win_points = $win_points,loss_points = $loss_points,tie_points = $tie_points,washout_points = $washout_points,league_id=".$tournament_info['league_id'].",tournament_id=".$tournament_info['id'];
	}else{
		$sql = "UPDATE league_points SET win_points = $win_points,loss_points = $loss_points,tie_points = $tie_points,washout_points = $washout_points WHERE league_id=".$tournament_info['league_id']." AND tournament_id=".$tournament_info['id'];
	}
	if(mysqli_query($conn,$sql)){
		$_SESSION['league_rules_updated'] = 1;
		header("Location:".WWW."tournament/standings/".$tournament_id);
		exit();
	}else{
		$error = '<p class="alert alert-danger">Error in updating League Standing Rules. Try again later</p>';
	}
}

$sql = "SELECT * from league_points WHERE league_id = ".$tournament_info['league_id']." AND tournament_id=".$tournament_info['id'];
$rs = mysqli_query($conn,$sql);
if(mysqli_num_rows($rs) > 0){
	$league_points = mysqli_fetch_assoc($rs);
}

$sql = "SELECT c.id,c.company_name FROM companies as c inner join tournament_teams as t on c.id = t.team_id
 WHERE  t.tournament_id = $tournament_id ORDER BY c.company_name ";
$rs_teams = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_teams)){
	$teams[] = $row;
}

if(isset($_POST['update_penalty_points']) && !empty($_POST['update_penalty_points'])){
	for($i=0;$i<count($teams);$i++){
		if(isset($_POST['penalty_points_'.$teams[$i]['id']])  ){
			if($_POST['penalty_points_'.$teams[$i]['id']] == '') $_POST['penalty_points_'.$teams[$i]['id']] = 0;
			$sql = "UPDATE tournament_teams SET penalty_points = ".$_POST['penalty_points_'.$teams[$i]['id']]." WHERE team_id=".$teams[$i]['id']." AND tournament_id=".$tournament_info['id'];
			mysqli_query($conn,$sql);
		}
	}

	if(!mysqli_error($conn)){
		$_SESSION['penalty_points_updated'] = 1;
		header("Location:".WWW."tournament/standings/".$tournament_id);
		exit();
	}else{
		$error = '<p class="alert alert-danger">Error in updating Penalty Points. Try again later</p>';
	}
}

$sql = "SELECT id from tournament_matches WHERE tournament_id = $tournament_id  ";
$rs_tournament_matches = mysqli_query($conn,$sql);

if(isset($_SESSION['league_rules_updated']) && $_SESSION['league_rules_updated']==1) {
	$league_rules_updated = 1;
	unset($_SESSION['league_rules_updated']);
}

if(isset($_SESSION['penalty_points_updated']) && $_SESSION['penalty_points_updated']==1) {
	$penalty_points_updated = 1;
	unset($_SESSION['penalty_points_updated']);
}

?>
            <div class="row">
              <div class="col-sm-12">
                <h2>  Tournaments Standings - <?php echo $tournament_info['title']; ?> </h2>

              </div>
            </div>

			<div class="row">
            <div class="col-sm-12">
				<?php if(!empty($error)): ?>
					<?php echo $error; ?>
				<?php endif; ?>
				<?php if(isset($league_rules_updated) && $league_rules_updated == 1): ?>
					<div id="information" class="alert alert-success">League Rules updated Successfully... !</div>
				<?php endif; ?>
				<?php if(isset($penalty_points_updated) && $penalty_points_updated == 1): ?>
					<div id="information" class="alert alert-success">Penalty Points updated Successfully... !</div>
				<?php endif; ?>
			</div>
            </div>

			<form method="post">
            <div class="row">
              <div class="col-sm-12">
                <div class="table-responsive">
                  <table class="table table-bordered table-condensed table-hover table-striped" id="table-list">
                    <thead>
                      <tr>
                        <th>Group</th>
                        <th>Team</th>
                        <th>Played</th>
                        <th>Won</th>
                        <th>Lost</th>
                        <th>Tied</th>
                        <th>Wash</th>
						<th>Win<br/>Points</th>
						<th>Loss<br/>Points</th>
						<th>Tie<br/>Points</th>
						<th>Washout<br/>Points</th>
						<th>Penalty<br/>Points</th>
                        <th>Final<br/>Points</th>
                        <th>Win %</th>
                        <th>NRR</th>
                        <th>Detail</th>
                      </tr>
                    </thead>
                    <tbody>
					<?php if(mysqli_num_rows($rs_tournament_matches)>0): ?>
					<?php for($i=0;$i<count($teams);$i++): ?>
					<?php $sql = "SELECT count(id) as matches_played FROM tournament_matches WHERE tournament_id=$tournament_id and (winning_team_id >0 OR winning_team_id = -1 OR winning_team_id = -2) and ( team1=".$teams[$i]['id'].' OR team2 = '.$teams[$i]['id'].')';
					$row = mysqli_fetch_assoc(mysqli_query($conn,$sql));
					$matches_played = $row['matches_played'];

					$sql = "SELECT count(id) as matches_won FROM tournament_matches WHERE tournament_id=$tournament_id and  winning_team_id=".$teams[$i]['id'];

					$row = mysqli_fetch_assoc(mysqli_query($conn,$sql));
					$matches_won = $row['matches_won'];

					$sql = "SELECT count(id) as matches_lost FROM tournament_matches WHERE tournament_id=$tournament_id and ( team1=".$teams[$i]['id'].' OR team2 = '.$teams[$i]['id'].") and winning_team_id!=".$teams[$i]['id']." and winning_team_id>0";
					$row = mysqli_fetch_assoc(mysqli_query($conn,$sql));
					$matches_lost = $row['matches_lost'];

					$sql = "SELECT count(id) as matches_tied FROM tournament_matches WHERE tournament_id=$tournament_id and ( team1=".$teams[$i]['id'].' OR team2 = '.$teams[$i]['id'].")  and winning_team_id = -1";
					$row = mysqli_fetch_assoc(mysqli_query($conn,$sql));
					$matches_tied = $row['matches_tied'];

					$sql = "SELECT count(id) as matches_washed FROM tournament_matches WHERE tournament_id=$tournament_id and ( team1=".$teams[$i]['id'].' OR team2 = '.$teams[$i]['id'].")  and winning_team_id = -2";
					$row = mysqli_fetch_assoc(mysqli_query($conn,$sql));
					$matches_washed = $row['matches_washed'];

					$win_percent = round(($matches_won/($matches_played-$matches_washed))*100,2);
					if($win_percent > 100){
						$win_percent = 100;
					}

					$total_overs_faced = 0;

					$sql = 'SELECT sum(team1_score) as total_runs_scored,sum(team1_overs) as total_overs_faced,
					sum(team2_score) as total_runs_conceded,sum(team2_overs) as total_overs_bowled FROM tournament_matches
					WHERE tournament_id='.$tournament_id.' and (winning_team_id >0 OR winning_team_id = -1 OR winning_team_id = -2) and  batting_team1='.$teams[$i]['id'];
					$row1 = mysqli_fetch_assoc(mysqli_query($conn,$sql)); //var_dump($row1);

					$sql = 'SELECT team1_overs,maximum_overs,winning_team_id  FROM tournament_matches
					WHERE tournament_id='.$tournament_id.' and (winning_team_id >0 OR winning_team_id = -1 OR winning_team_id = -2) and  batting_team1='.$teams[$i]['id'];
					$rs1 = mysqli_query($conn,$sql);
					while($row_data = mysqli_fetch_assoc($rs1)){
						if($row_data['winning_team_id'] == $teams[$i]['id']){
							$total_overs_faced+=$row_data['team1_overs'];
						}else{
							$total_overs_faced+=$row_data['maximum_overs'];
						}
					}

					$sql = 'SELECT sum(team2_score) as total_runs_scored,sum(team2_overs) as total_overs_faced ,
					sum(team1_score) as total_runs_conceded,sum(team1_overs) as total_overs_bowled FROM tournament_matches
					WHERE tournament_id='.$tournament_id.' and (winning_team_id >0 OR winning_team_id = -1 OR winning_team_id = -2) and  batting_team2='.$teams[$i]['id'];
					$row2 = mysqli_fetch_assoc(mysqli_query($conn,$sql));//var_dump($row2);

					$sql = 'SELECT team2_overs ,maximum_overs,winning_team_id  FROM tournament_matches
					WHERE tournament_id='.$tournament_id.' and (winning_team_id >0 OR winning_team_id = -1 OR winning_team_id = -2) and  batting_team2='.$teams[$i]['id'];
					$rs1 = mysqli_query($conn,$sql);
					while($row_data = mysqli_fetch_assoc($rs1)){
						if($row_data['winning_team_id'] == $teams[$i]['id']){
							$total_overs_faced+=$row_data['team1_overs'];
						}else{
							$total_overs_faced+=$row_data['maximum_overs'];
						}
					}


					$sql = "SELECT group_name,penalty_points FROM tournament_teams WHERE tournament_id=$tournament_id and team_id = ".$teams[$i]['id'];
					$row = mysqli_fetch_assoc(mysqli_query($conn,$sql));
					$group_name = $row['group_name'];
					$penalty_points = $row['penalty_points'];

					//$data1 = ($row1['total_runs_scored']+$row2['total_runs_scored'])/($row1['total_overs_faced']+$row2['total_overs_faced']);
					$balls_arr = explode('.',$total_overs_faced);
					$overs_faced_converted = $balls_arr[0]+($balls_arr[1]/6);

					$balls_arr = explode('.',$row1['total_overs_bowled']+$row2['total_overs_bowled']);
					$overs_bowled_converted = $balls_arr[0]+($balls_arr[1]/6);

					$data1 = ($row1['total_runs_scored']+$row2['total_runs_scored'])/($overs_faced_converted);
					$data2 = ($row1['total_runs_conceded']+$row2['total_runs_conceded'])/($overs_bowled_converted);

					//$data1 = ($row1['total_runs_scored']+$row2['total_runs_scored'])/($total_overs_faced);
					//$data2 = ($row1['total_runs_conceded']+$row2['total_runs_conceded'])/($row1['total_overs_bowled']+$row2['total_overs_bowled']);

					if(is_nan($data1)) $data1 = 0;
					if(is_nan($data2)) $data2 = 0;

					$net_run_rate = $data1-$data2;
					?>
                      <tr>
							<td><?php echo $group_name; ?></td>
							<td><?php echo $teams[$i]['company_name']; ?></td>
							<td><?php echo $matches_played; ?></td>
							<td><?php echo $matches_won; ?></td>
							<td><?php echo $matches_lost; ?></td>
							<td><?php echo $matches_tied; ?></td>
							<td><?php echo $matches_washed; ?></td>
							<td><?php if($league_points['win_points'] >0) $win_points = $league_points['win_points']*$matches_won;else $win_points = 0; if($league_points['win_points']>=0) echo $win_points;else echo 'N/A'; ?></td>
							<td><?php if($league_points['loss_points'] <0) $loss_points = $league_points['loss_points']*$matches_lost;else $loss_points = 0; if($league_points['loss_points']<=0) echo $loss_points;else echo 'N/A'; ?></td>
							<td><?php if($league_points['tie_points'] >0) $tie_points = $league_points['tie_points']*$matches_tied;else $tie_points = 0;if($league_points['tie_points']>=0) echo $tie_points;else echo 'N/A'; ?></td>
							<td><?php if($league_points['washout_points'] >0) $washout_points = $league_points['washout_points']*$matches_washed;else $washout_points = 0;  if($league_points['washout_points']>=0) echo $washout_points;else echo 'N/A'; ?></td>
							<td <?php if($_SESSION['ycdc_dbuid'] == $tournament_info['user_id']): ?> title="Click to Edit Penalty Points" onclick="$(this).find('input').show();$(this).find('.penalty_pts').hide();$('#update_penalty_points').show();" <?php endif; ?>><span class="penalty_pts"><?php echo $penalty_points; ?></span><input type="text" name="penalty_points_<?php echo $teams[$i]['id']; ?>" value="<?php echo $penalty_points; ?>" id="penalty_points_<?php echo $teams[$i]['id']; ?>" style="width:50px;display:none;" ></td>
							<td><?php $total_points = ($win_points+$tie_points+$washout_points+$loss_points) +$penalty_points; echo $total_points; ?></td>
							<td><?php if(is_nan($win_percent) || is_infinite($win_percent)) echo 0;else echo $win_percent; ?> %</td>
							<td><?php if(is_nan($net_run_rate) || is_infinite($net_run_rate)) echo 0;else echo round($net_run_rate,3); ?></td>
							<td><a class="team-standings" href1="<?php echo WWW; ?>tournament/team-standings/<?php echo $tournament_id; ?>/<?php echo $teams[$i]['id']; ?>" title="View Details"><i class="fa fa-exclamation-circle"></i></a></td>
						</tr>
					<?php endfor; ?>
					<?php endif; ?>


                    </tbody>
                  </table>
                </div>

				 <div class="form-group">
						<div class="col-sm-offset-5 col-sm-7">
							<?php if($_SESSION['ycdc_dbuid'] == $tournament_info['user_id']): ?><p style="float:right;font-size:14px;">Click on Penalty points to edit them</p><?php endif; ?>
							<input name="update_penalty_points" id="update_penalty_points" value="Update Points" class="btn orange hvr-float-shadow" type="submit" style="display:none;">
						</div>
				</div>

              </div>
            </div>
			</form>

			<?php if($_SESSION['ycdc_dbuid'] == $tournament_info['user_id']): ?>
			<div class="row">
				<div class="col-sm-12">
					<h3>Define Rules for Points System here</h3>
				</div>
            </div>

			<div class="row">
				<div class="col-sm-12">
					<form method="post" action="" class="form-horizontal">

					  <div class="form-group">
						<label class="col-sm-5 control-label">Win Points</label>
						<div class="col-sm-7">
						  <select name="win_points" id="win_points" class="form-control">
							<option value="-1" <?php if($league_points['win_points'] == -1) echo 'selected'; ?>>N/A</option>
							<?php for($i=0;$i<=10;$i++): ?>
							<?php  if(isset($_POST['win_points']) && $_POST['win_points'] == $i) $sel =  'selected'; elseif($league_points['win_points'] == $i) $sel =  'selected'; else $sel = ''; ?>
							<option <?php echo $sel; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
							<?php endfor; ?>
						  </select>
						</div>
					  </div>
					  <div class="form-group">
						<label class="col-sm-5 control-label">Loss Points</label>
						<div class="col-sm-7">
						  <select name="loss_points" id="loss_points" class="form-control">
							<option value="1" <?php if($league_points['loss_points'] == 1) echo 'selected'; ?>>N/A</option>
							<option value="0" <?php if($league_points['loss_points'] == 0) echo 'selected'; ?>>0</option>
							<?php for($i=1;$i<=10;$i++): ?>
							<?php  if(isset($_POST['loss_points']) && $_POST['loss_points'] == -$i) $sel =  'selected'; elseif($league_points['loss_points'] == -$i) $sel =  'selected';else $sel = ''; ?>
							<option <?php echo $sel; ?> value="<?php echo -$i; ?>"><?php echo -$i; ?></option>
							<?php endfor; ?>
						  </select>
						</div>
					  </div>
					  <div class="form-group">
						<label class="col-sm-5 control-label">Tie Points</label>
						<div class="col-sm-7">
						  <select name="tie_points" id="tie_points" class="form-control">
							<option value="-1" <?php if($league_points['tie_points'] == -1) echo 'selected'; ?>>N/A</option>
							<?php for($i=0;$i<=10;$i++): ?>
							<?php  if(isset($_POST['tie_points']) && $_POST['tie_points'] == $i) $sel =  'selected'; elseif($league_points['tie_points'] == $i) $sel =  'selected';else $sel = ''; ?>
							<option <?php echo $sel; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
							<?php endfor; ?>
						  </select>
						</div>
					  </div>
					  <div class="form-group">
						<label class="col-sm-5 control-label">Washout Points</label>
						<div class="col-sm-7">
						  <select name="washout_points" id="washout_points" class="form-control">
							<option value="-1" <?php if($league_points['washout_points'] == -1) echo 'selected'; ?>>N/A</option>
							<?php for($i=0;$i<=10;$i++): ?>
							<?php  if(isset($_POST['washout_points']) && $_POST['washout_points'] == $i) $sel =  'selected'; elseif($league_points['washout_points'] == $i) $sel =  'selected';else $sel = ''; ?>
							<option <?php echo $sel; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
							<?php endfor; ?>
						  </select>
						</div>
					  </div>
					  <!--<div class="form-group">
						<label class="col-sm-5 control-label">Penality Points</label>
						<div class="col-sm-7">
						  <input  name="penality_points" id="penality_points" class="form-control datepicker" value="<?php if(isset($_POST['penality_points'])) echo $_POST['penality_points'];elseif(!empty($league_points['penality_points'])) echo $league_points['penality_points']; ?>" type="text">
						</div>
					  </div>-->
					  <div class="form-group">
						<div class="col-sm-offset-5 col-sm-7">
							<input name="submit_btn" value=" Submit " class="btn orange hvr-float-shadow" type="submit" >
						</div>
					  </div>
					</form>
				</div>
			</div>
			<?php endif; ?>

          </div>

<script type="text/javascript" src="<?php echo WWW; ?>js/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	//$("#table-list").tablesorter( {sortList: [[13,1], [14,1]]} );

});
</script>
