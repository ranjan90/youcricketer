<?php include_once('includes/configuration.php');
$page = 'tournament-edit.html';

$selected_country = getGeoLocationCountry(); 

$tournament_overs = array('Ten10'=>'Ten10','Fifteen15'=>'Fifteen15','Twenty20'=>'Twenty20','Thirty30'=>'Thirty30','One Day'=>'One Day');
$overs = array('10'=>'10','15'=>'15','20'=>'20','30'=>'30','35'=>'35','40'=>'40','45'=>'45','50'=>'50','55'=>'55','60'=>'60');
$ball_type = array('hard_ball'=>'Hard Ball','rubber_ball'=>'Rubber Ball','tape_ball'=>'Tape Ball','tennis_ball'=>'Tennis Ball');

$sql = "select id,name from countries where status=1";
$rs_countries = mysqli_query($conn,$sql); 

$tournament_id = trim($_GET['id']);
$tournament_info = get_record_on_id('tournaments', $tournament_id);	

$page_title = 'Edit Tournament - '.ucwords($tournament_info['title']);

$tournament_teams = array();

$sql = 'select * from tournament_teams WHERE  tournament_id = '.$tournament_id;
$rs_teams = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_teams)){
	$tournament_teams[] = $row['team_id'];
}

$league_clubs = array();
$league_info = array();
$error = '';

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
	if($user_info['is_company'] == 1){
		$sql = "select * from companies where user_id = $user_id and company_type_id = 2 and status=1";
		$rs_company = mysqli_query($conn,$sql);
		if(mysqli_num_rows($rs_company)){
			$league_info = mysqli_fetch_assoc($rs_company);
			$sql = 'select c.* from league_clubs as lc inner join companies as c on lc.club_id=c.id where lc.league_id = '.$league_info['id'];
			$rs_club = mysqli_query($conn,$sql);
			while($row = mysqli_fetch_assoc($rs_club)){
				$league_clubs[] = $row;
			}
		}else{
			$error = '<p class="alert alert-danger">You must be logged as League Owner</p>';
		}
	}
}

if(isset($_POST['submit_btn']) && !empty($_POST['submit_btn'])){
	validate();
	if(empty($error)){
		$date = date('Y/m/d H:i:s')	;
		if(empty($_POST['state_id'])) $_POST['state_id'] = 0;
		$sql = " UPDATE tournaments SET  year='".trim($_POST['year'])."', title='".trim($_POST['title'])."',league_id='".trim($league_info['id'])."',
		user_id='".trim($user_id)."',country_id='".trim($_POST['country_id'])."',state_id='".trim($_POST['state_id'])."',city='".trim($_POST['city'])."'
		,overs_type='".trim($_POST['overs_type'])."',overs_count='".trim($_POST['overs_count'])."',ball_type='".trim($_POST['ball_type'])."'
		,end_time='".trim($_POST['end_time'])."',updated_on='".$date."' WHERE id=".$tournament_id;
		
		if(mysqli_query($conn,$sql)){
			
			$_SESSION['tournament_updated'] = 1;
			header("Location:".WWW."tournament/list");
			
		}else{
			$error = '<p class="alert alert-danger">Error in updating Tournament. Try again later</p>';
		}
	}
}

function validate(){
	global $error;
	
	if(empty($_POST['year'])){
		$error.= '<p class="alert alert-danger">Year is required field</p>';
	}
	if(empty($_POST['title'])){
		$error.= '<p class="alert alert-danger">Title is required field</p>';
	}
	if(empty($_POST['country_id'])){
		$error.= '<p class="alert alert-danger">Country is required field</p>';
	}
	/*if(empty($_POST['state_id'])){
		$error.= '<p class="alert alert-danger">State is required field</p>';
	}
	if(empty($_POST['city'])){
		$error.= '<p class="alert alert-danger">City is required field</p>';
	}*/
	if(empty($_POST['overs_type'])){
		$error.= '<p class="alert alert-danger">Game Type is required field</p>';
	}
	if(empty($_POST['overs_count'])){
		$error.= '<p class="alert alert-danger">Game Overs is required field</p>';
	}
	if(empty($_POST['ball_type'])){
		$error.= '<p class="alert alert-danger">Preferred Ball Type is required field</p>';
	}
	if(empty($_POST['start_time'])){
		$error.= '<p class="alert alert-danger">Start Time is required field</p>';
	}
	if(empty($_POST['end_time'])){
		$error.= '<p class="alert alert-danger">End Time is required field</p>';
	}
	if(!empty($_POST['start_time']) && !empty($_POST['end_time']) && strtotime($_POST['end_time']) < strtotime($_POST['start_time'])){
		$error.= '<p class="alert alert-danger">Start Time must be Less than End Time</p>';
	}
}

?>
<?php include('common/header.php'); ?>

<div class="page-container"> 
		<?php  include('common/user-left-panel.php');?>
      <!-- END SIDEBAR -->
      
      <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
		<div class="page-content">
		<?php  include('common/breadcrumbs.php');?>
		
		<div class="white-box">
            <div class="row">
              <div class="col-sm-8">
                <h2> Edit Tournament </h2>
              </div>
            </div>
           
            <div class="row">
              <div class="col-sm-12">
                <h3>Tournament Details</h3>
				<?php if(empty($user_info)): ?>
					<div id="error" class="alert alert-danger">You are not logged... !</div>
				<?php endif; ?>
				
				<?php if(!empty($error)): ?>
					<?php echo $error; ?>
				<?php endif; ?>
			
              </div>
            </div>
			<?php if($tournament_info['user_id'] == $user_id){ ?>
            <div class="row">
              <div class="col-sm-12">
                <form method="post" action="" class="form-horizontal">
                  <input name="action" value="submit" type="hidden">
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Year*</label>
                    <div class="col-sm-7">
                      <select name="year" id="year" class="form-control">
                       <option value="">Select One</option>
						<?php for($i=date('Y')-1;$i<=date('Y')+1;$i++): ?>
						<?php if(isset($_POST['year']) && $_POST['year'] == $i) $sel =  'selected';elseif($tournament_info['year'] == $i && empty($sel)) $sel = 'selected'; else $sel = '';  ?>
						<option <?php echo $sel; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>	
						<?php endfor; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Title*</label>
                    <div class="col-sm-7">
                      <input name="title" id="title" class="form-control"   value="<?php if(isset($_POST['title'])) echo $_POST['title']; else echo $tournament_info['title']; ?>" type="text">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Country</label>
                    <div class="col-sm-7">
                      <select name="country_id" id="country_id" class="form-control" onchange="getStates(this.value,'')">
                        <option value="">Select One</option>
						<?php while($row = mysqli_fetch_assoc($rs_countries)): if(isset($_POST['country_id']) && $_POST['country_id'] == $row['id']) $sel =  'selected';elseif($tournament_info['country_id'] == $row['id']) $sel = 'selected'; else $sel = '';  ?>
							<option <?php echo $sel; ?> value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
						<?php endwhile; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">State</label>
                    <div class="col-sm-7">
                      <select name="state_id" id="state_id" class="form-control">
                        <option value="0">Select One</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">City</label>
                    <div class="col-sm-7">
                      <input name="city" id="city" class="form-control" value="<?php if(!empty($_POST['city'])) echo $_POST['city']; else  echo $tournament_info['city'] ?>" type="text">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Game Type*</label>
                    <div class="col-sm-7">
                      <select name="overs_type" id="overs_type" class="form-control">
                       <option value="">Select One</option>
						<?php foreach($tournament_overs as $key=>$val): if(isset($_POST['overs_type']) && $_POST['overs_type'] == $key) $sel =  'selected';elseif($tournament_info['overs_type'] == $key) $sel =  'selected'; else $sel = '';  ?>
							<option <?php echo $sel; ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
						<?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Overs*</label>
                    <div class="col-sm-7">
                      <select name="overs_count" id="overs_count" class="form-control">
                        <option value="">Select One</option>
						<?php foreach($overs as $key=>$val): if(isset($_POST['overs_count']) && $_POST['overs_count'] == $key) $sel =  'selected';elseif($tournament_info['overs_count'] == $key) $sel =  'selected'; else $sel = '';  ?>
							<option <?php echo $sel; ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
						<?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Preferred Ball Type*</label>
                    <div class="col-sm-7">
                      <select name="ball_type" id="ball_type" class="form-control">
                        <option value="">Select One</option>
						<?php foreach($ball_type as $key=>$val): if(isset($_POST['ball_type']) && $_POST['ball_type'] == $key) $sel =  'selected';elseif($tournament_info['ball_type'] == $key) $sel =  'selected';else $sel = '';  ?>
							<option <?php echo $sel; ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
						<?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Start Date*</label>
                    <div class="col-sm-7">
                      <input autocomplete="off" name="start_time" id="start_time" class="form-control datepicker" value="<?php if(isset($_POST['start_time'])) echo $_POST['start_time']; else echo $tournament_info['start_time']; ?>" type="text">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">End Date*</label>
                    <div class="col-sm-7">
                      <input autocomplete="off" name="end_time" id="end_time" class="form-control datepicker" value="<?php if(isset($_POST['end_time'])) echo $_POST['end_time'];else echo $tournament_info['end_time']; ?>" type="text">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-5 col-sm-7">
						<input name="submit_btn" value=" Submit " class="btn orange hvr-float-shadow" type="submit" <?php if(empty($user_info) || empty($league_info)): ?> disabled <?php endif; ?>>
                      <input name="cancel_btn" value=" Cancel " class="btn blue hvr-float-shadow" onclick="window.location.href='<?php echo WWW; ?>tournament/list';" type="button">
                      
                    </div>
                  </div>
                </form>
              </div>
            </div>
			<?php }else{ ?>
				<div id="error" class="alert alert-danger">You are not owner of this tournament... !</div>
			<?php } ?>
          </div>
		
		
		</div>
	</div>
</div>		


<script src="<?php echo WWW; ?>assets/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo WWW; ?>assets/css/jquery.datetimepicker.css"/>
<script>
$('#start_time').datetimepicker();
$('#end_time').datetimepicker();

function getStates(id,sel){
	var data = "sel="+sel;
 $.ajax({
			url: "<?php echo WWW; ?>get_states.php?id="+id,
			type:"POST",
			data:data,
			success: function(data) {
				$("#state_id").html(data);
			}
		});
}		
<?php if(isset($_POST) && !empty($_POST)){
if(!empty($error) && !empty($_POST['country_id'])){ ?>
	<?php if(!empty($_POST['state_id'])) $state = $_POST['state_id'];else $state = '0'; ?>
	getStates(<?php echo $_POST['country_id']; ?>,<?php echo $state; ?>);
<?php }
}elseif(!empty($tournament_info['country_id']) && !empty($tournament_info['state_id'])){ ?>
	getStates(<?php echo $tournament_info['country_id']; ?>,<?php echo $tournament_info['state_id']; ?>);
<?php } ?>

</script>
<?php include('common/footer.php'); ?>