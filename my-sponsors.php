<?php include_once('includes/configuration.php');
$page = 'my-sponsors.html';
$page_title = 'My Sponsors'; ?>
<?php include('common/header.php');
$user_id = $_SESSION['ycdc_dbuid'];
$payment = mysqli_fetch_assoc(mysqli_query($conn,"select * from user_sponsor_payment WHERE user_id = $user_id  AND status = 1 ORDER BY user_sponsor_id DESC"));

if(!empty($payment)) {


    $date   = date('Ymd');
    $end    =  date('Ymd',strtotime($payment['date_end']));

    if($date <= $end ) {

        $days = dateTimeDaysLeft($date,$end);
    } else {
        $days = 0;
    }
}

/*if(isset($_POST['action']) && $_POST['action'] == 'update_sort')
{
    $sp_id = (int)$_POST['sponsor_id'];
    $sort_order = (int) $_POST['sort'];
    $sql   = "UPDATE my_sponsors SET sort = $sort WHERE sponsor_id = $sp_id";
    if( mysqli_query($conn,$sql) ) {
        echo '<p id="error" class="alert alert-success">Sort Order Updated Successfully.</p>';
    }else{
        echo '<p id="error" class="alert alert-danger">Error in Updated sort order. Try again later</p>';
    }
}*/

?>

<div class="page-container">
	<?php include('common/user-left-panel.php');?>

	<!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="white-box">
				<div class="row">
					<div class="col-sm-12">
                        <h2> My Sponsors -
                            <?php if(!empty($days)){ ?>
                            <a href="<?php echo WWW?>global-market-place.html" style="font-size:15px; font-weight: normal; color:#1743ac">
                                <?php echo "Your package is active till <b> ".date('d', strtotime($end)).' Of '.date('M', strtotime($end)) .', '.date('Y', strtotime($end))." ".$days.' day(s) left.</b>' ?>
                            </a> </h2>
                            <?php } else {?>
                            <a href="#" data-toggle="modal" data-target="#sponsorModal" style="font-size:15px; font-weight: normal; color:#1743ac"> <?php echo get_record_on_id('cms', 48)['title'] ?></a> </h2>
					        <?php } ?>
					</div>
				</div>
				<br/>
				<?php if(empty($payment) || empty($days)){ ?>

	<?} else {?>
<?php
                    if(isset($_POST['submit_btn']) && !empty($_POST['submit_btn'])){


                          if($_POST['album_photo']!='' || $_FILES['my_sponsors']['name']!=''){

                             $title = $_POST['title'];
                            // $sort = $_POST['sort'];
                             $sorder = mysqli_fetch_assoc(mysqli_query($conn,"select sort from my_sponsors where user_id=".$userid." ORDER BY sort DESC LIMIT 0,1"));
                             $sort=$sorder["sort"]+1;
                             $url = $_POST['url'];

                              if(isset($_POST['album_photo']) && $_POST['album_photo']!=''){

                                  $getImage=mysqli_fetch_assoc(mysqli_query($conn,'select * from photos where id="'.$_POST['album_photo'].'"'));

                                  $img="users/".$getImage['entity_id']."/photos/".$getImage['file_name'];

                                  $filename=$getImage['file_name'];

                                  copy($img,'my-sponsors/'.$filename);

                                  $sql = "INSERT INTO my_sponsors (`user_id`,`title`,`sort`,`file_type`,`url`,`file_info`) VALUES
                                          ('$user_id','$title','$sort','image','$url','$filename')";
                                  if( mysqli_query($conn,$sql) ) {
                                     echo '<p id="error" class="alert alert-success">Uploaded Successfully.</p>';
                                  }else{
                                      echo '<p id="error" class="alert alert-danger">Error in adding File. Try again later</p>';
                                  }


                              }else{

                                $targetFolder = 'my-sponsors';
                                $tempFile = $_FILES['my_sponsors']['tmp_name'];
                                $targetPath = $targetFolder;
                                $fileParts = pathinfo($_FILES['my_sponsors']['name']);
                                $fileName = $fileParts['filename'].'_'.rand(99,9999).time().$user_id.'.'.$fileParts['extension'];
                                $targetFile = $targetPath . '/' .$fileName ;

                                $file_orig = $fileParts['filename'].'.'.$fileParts['extension'];

                                if(!move_uploaded_file($tempFile,$targetFile)){
                                    $error= 'Error in uploading File';
                                }else{

                                    $sql = "INSERT INTO my_sponsors (`user_id`,`title`,`sort`,`file_type`,`url`,`file_info`) VALUES
                                            ('$user_id','$title','$sort','image','$url','$fileName')";
                                    if( mysqli_query($conn,$sql) ) {
                                       echo '<p id="error" class="alert alert-success">Uploaded Successfully.</p>';
                                    }else{
                                        echo '<p id="error" class="alert alert-danger">Error in adding File. Try again later</p>';
                                    }
                                }


                               }

                              }else{
                                echo '<p id="error" class="alert alert-danger">Please Upload Image or Choose form album.</p>';
                              }
                            //}


                           // $sql = " UPDATE companies SET by_laws_file =  '$file1',by_laws_file_orig = '$file2' ,tournament_data_added = 'yes' WHERE user_id='".$user_id."'";




                    }

                    if(isset($_GET['id']) && !empty($_GET['id'])) {
                        $sponsor_id = (int) $_GET['id'];
                        $rs_imgs = mysqli_query($conn,"select * from my_sponsors where sponsor_id = $sponsor_id ");
                        $row  = mysqli_fetch_assoc($rs_imgs);
                        if(!empty($row))
                        {
                            $sql = "DELETE FROM my_sponsors WHERE sponsor_id = $sponsor_id";
                            if (mysqli_query($conn, $sql)) {
                                @unlink('my-sponsors/'.$row['file_info']);
                                echo '<p id="error" class="alert alert-success">Deleted Successfully.</p>';
                            } else {
                                echo '<p id="error" class="alert alert-danger">Error in Deleting File. Try again later</p>';
                            }
                        }
                    }

                    unset($_POST);
                    ?>

                <div class="row">

                   <form method="post" action="<?=$_SERVER['PHP_SELF']?>" enctype="multipart/form-data" class="form-horizontal pull-left" id="myform">
						<div class="form-fields">
                          <?php /*endif; */?>
                          <?php /*endif; */?>
                          <div class="col-md-3 form-group">
                             <label class="col-md-12">Title</label>
                              <div class="col-md-12">
                                  <input class="form-control" type="text" name="title" placeholder="Title" required="required">
                              </div>
                          </div>
                       <!--<div class="form-group">
                           <label class="col-sm-5 control-label">Display Order</label>
                           <div class="col-sm-7">
                               <input type="text" name="sort" placeholder="i.e 1,2,3" required="required">
                           </div>
                       </div>-->
                        <div class="col-md-3 form-group">
                           <label class="col-md-12">Sponsor Url/Link</label>
                           <div class="col-md-12">
                               <input  class="form-control" type="url" name="url" placeholder="Proper Url/Link ">
                           </div>
                       </div>
                          <div class="col-md-5 form-group">
                            <label class="col-md-12">Upload Sponsor Ad Image</label>
                            <div class="col-md-6">
                              <input accept="image/*" name="my_sponsors" class="valid-aimage" id="my_sponsers" type="file"  onchange="loadFile(event)">
                              <span class="help-block">

                                Allowed File Types: jpg,jpeg,png <br> Max size is 10mb.
                              </span>
                              <img id="output" style="width:150px;"/>
							  <a style="display:none; cursor:pointer;" id="output-delete" title="Delete Photo">X</a>

                              <script>
                                var loadFile = function(event) {
                                  var reader = new FileReader();
                                  reader.onload = function(){

                                    output.src = reader.result;

									document.getElementById('output-delete').style.display='block';
                                  };
                                  reader.readAsDataURL(event.target.files[0]);
                                };
                              </script>
                            </div>
							<div class="col-md-6">
                              <p class="form-control-static"> <a id="from-album" href="javascript:;" onclick="$('#photos').toggle();" class="btn orange full hvr-float-shadow"> From Album</a> </p>
                            </div>
                          </div>
                           <div class="col-md-1 form-group">
                            <div class="col-md-12" style="margin-top:16px;">
                              <input name="submit_btn" type="hidden">
                              <input name="submit_btn" id="submit_btn" value="Upload" class="btn orange hvr-float-shadow" type="submit">
                            </div>
                          </div>
						  </div>
	<div class="clearfix"></div>
                          <div id="photos" class="" style="display:none;">
                  					<div class="row">
                  						<? 	$rs_photo = mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$user_id."' and is_default <> '1'");
                  							if(mysqli_num_rows($rs_photo) > 0){
                  								while($row_photos = mysqli_fetch_assoc($rs_photo)){
                  								?>
                  								<div id="photo" class="col-sm-2 text-center">
                  									<img src="<?=WWW?>users/<?=$user_id?>/photos/<?=$row_photos['file_name']?>" style="max-height:180px;">
                  									<br>
                  									<center>
                  										<input type="radio" name="album_photo" value="<?=$row_photos['id']?>">
                  									</center>
                  								</div>
                  								<?
                  								}
                  							}else{
                  								echo '<div class="alert alert-warning" style="margin:15px;"  id="information"> <i class="fa fa-exclamation-triangle"> </i>Photo album is empty</div>';
                  							}
                  						?>
                  					</div>
                  				</div>

                   </form>

                </div>


			<div class="clearfix"></div><br/>
            <div id="gallery" class="">
                <div class="row drag ">
                    <? $rs_imgs = mysqli_query($conn,"select * from my_sponsors where user_id = '$user_id' ORDER BY sort ASC ");
                    $i=1;
                    while($row_img = mysqli_fetch_assoc($rs_imgs)){
                      ?>
                        <!---put html here --->
            						<div class="col-sm-2">
            							<span class="drag_img_bx" id="<?php echo $row_img['sponsor_id'];?>">
          		                <img src="<?=WWW?>my-sponsors/<?=$row_img['file_info']?>"/>

                               <a href="<?=WWW?>my-sponsors.html?id=<?=$row_img['sponsor_id']?>" title="Delete Photo" onclick="return confirm('Are you sure to delete Photo');"  class="cross_ico">X</a>

                               <h4><?=$row_img['title']?></h4>

                               <?php if($row_img['url']!=''){?>
          						                 <a href="<?=$row_img['url']?>" class="btn blue hvr-float-shadow" target="_blank">Visit Link</a>
                             <?php } ?>
            						  </a>
                        </span>
                        </div>
                        <!---put html end here --->
                    <?
                    $i++;
                  } ?>
                </div>
            </div>

		          </div>
            <?php }?>
      </div>
      <!-- END CONTENT-->
</div>


    <!-- Modal -->
    <div id="sponsorModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><?php $contnt = get_record_on_id('cms', 48); echo $contnt['title']?></h4>
                </div>
                <div class="modal-body">
                    <p><?php echo $contnt['content'] ?> </p>
                </div>
                <!-- <div class="modal-footer">
                     <button type="button"  class="btn btn-success click-to-submit">Pay Now</button>
                     <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                 </div>-->
            </div>

        </div>
    </div>
drag

    <!--<script src="assets/js/jquery.gridly.js" type="text/javascript"></script>
    <link href="assets/css/jquery.gridly.css" rel="stylesheet" type="text/css" />-->


    <script>
       $(document).ready(function(){
       $("#output-delete").click(function(){
         $('#output').removeAttr('src');
            $('#output-delete').hide();
            $("#my_sponsers").val('');
      });
            $("#submit_btn").bind('click',function(){

                if($("#my_sponsors").val() == ''){
                    alert('File is Required Field');
                    return false;

                }
                if($("#my_sponsors").val() != ''){

                    var file_size = $("#my_sponsors")[0].files[0].size/(1024*1024);
                    if(file_size>10){
                        alert('File size is greater than 10MB');
                        return false;
                    }

                    var res_field = $("#my_sponsors").val();
                    var extension = res_field.substr(res_field.lastIndexOf('.') + 1).toLowerCase();
                    var allowedExtensions = ['jpg', 'png'];
                    if (res_field.length > 0)
                    {
                        if (allowedExtensions.indexOf(extension) === -1)
                        {
                            alert('Invalid file Format. Only ' + allowedExtensions.join(', ') + ' are allowed.');
                            return false;
                        }
                    }
                }
                return true;
            });

            var adjustment;
            $( ".drag" ).sortable({
                containment : 'parent',
            		placeholder	: "ui-state-highlight",
            		update		: function(event, ui){
            			var page_id_array = new Array();
            			$('.drag .drag_img_bx').each(function(){
            				page_id_array.push($(this).attr("id"));
            			});
            			$.ajax({
            				url:"ajax/update_sponsors.php",
            				method:"POST",
            				data:{order:page_id_array},
            				success:function(data){
            				}
            			});
            		}
            });



            /*var width=$( window ).width();
            var col=12;
            if(width>=640 && width<=800)
              var col=8;
            else if(width>=200 && width<=640)
              var col=4;
            else if(width>=1300)
              var col=16;

            $('.gridly').gridly({
              base: 60,
              columns: col
            });
            $('.gridly').gridly({gridly
              callbacks: { reordering: reordering , reordered: reordered }
            });

            var reordering = function($elements) {
                alert($elements);
              };

              var reordered = function($elements) {
                alert($elements);
              };*/


            $( "#myform" ).validate({
              messages: {
                title: {
                  required: "Please Enter Your Title"
                },
                my_sponsors:{
                  required: "Please Upload Image ",
                  accept: "Please Upload Image Only"
                }
              }
            });

        });
    </script>
<!-- <script src="js/drag.js" type="text/javascript"></script> -->

<?php include('common/footer.php'); ?>
