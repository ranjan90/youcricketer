<?php include('common/header.php'); ?>

<? $u_id = $_SESSION['ycdc_dbuid'];
 $rpp = PRODUCT_LIMIT_FRONT; // results per page
            			$ppage = intval($_GET["page"]);
      					if($ppage<=0) $ppage = 1;
      					$select = "select * from events e ";
      					$where = " where e.user_id = '$u_id' AND e.status=1 ";
				      	//=======================================
					    if(isset($_GET['keywords']) && $_GET['keywords'] != 'Search Here'){
					        $where  .= "and e.title like '%".str_replace('-','%',$_GET['keywords'])."%'";
					    }
					    if(isset($_GET['type']) && $_GET['type'] == 'my'){
					    	$where .= " and e.user_id = '".$_SESSION['ycdc_dbuid']."'";
					    }
					    if(isset($_GET['type']) && $_GET['type'] == 'your_region'){
					    	$ipData 	= file_get_contents('http://api.hostip.info/get_html.php?ip='.$user_ip);
							preg_match('/Country: (.*) /',$ipData, $expression);
							$selected_country = trim($expression[1]);
							$row_country = mysqli_fetch_assoc(mysqli_query($conn,"select * from countries where name = '$selected_country'"));
							$join .= " join users u on u.id = e.user_id and u.country_id = '".$row_country['id']."'";
					    }
					    $query = $select.$join.$where." order by e.id desc ";
				     //echo $query;
				      //=======================================
				      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
				        echo '<div id="error" class="alert alert-danger">No Events Found !</div>';
				      }
				      $rs   = mysqli_query($conn,$query);
				      $tcount = mysqli_num_rows($rs);
				      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
				      $count = 0;
				      $i = ($ppage-1)* $rpp;
          			  $x = 0;
          			  //======================================= ?>


<div class="page-container"> 
		<?php  include('common/user-left-panel.php');?>
      <!-- END SIDEBAR -->
      
      <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
		<div class="page-content">
		
		
		    <div class="white-box">
            <div class="row">
              <div class="col-md-12">
                <h2> YouCricketer Events </h2>
              </div>
            </div>
            
            <div id="pagination-top">
             <div class="row">
               
                <div class="col-sm-6">
                 <? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_FRONT){ ?>
					<?php
						$reload = 'events.html?';
						echo paginate_one($reload, $ppage, $tpages);
					?>
					<input type="hidden" name="pagination-page" value="events.html">
				 <? } ?>    
                </div>
                <div class="col-sm-3" id="search-div1">
                  <form id="list-search" method="post" action="">
                  <div class="input-group">
                    <input class="form-control validate[required] input-login" name="txtsearch" placeholder="Search Here" type="text">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit">Go!</button>
                    </span>
                  </div>
				  </form>
                </div>
                <div class="col-sm-3">
                  <a href="<?=WWW;?>add-event.html" class="btn orange full hvr-float-shadow">Create Event</a>
                </div>
              </div>
            </div>
            
            <div class="list">
              <ul>
			   <?php while(($count<$rpp) && ($i<$tcount)){
                		mysqli_data_seek($rs,$i);
                		$row_g 	= mysqli_fetch_assoc($rs);
                		$row_img= mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'events' and entity_id = '".$row_g['id']."' order by id desc limit 1"));
                		$row_u  = get_record_on_id('users', $row_g['user_id']);
      					?>
                <li>
                  <div class="row">
                    <div class="col-sm-3">
                      <a href="<?=WWW?>event-detail-<?=$row_g['id']?>-<?=friendlyURL($row_g['title'])?>.html" title="Read more"> 
					  <img src="<?=($row_img)?WWW.'events/'.$row_g['id'].'/'.$row_img['file_name']:WWW.'images/community-groups.png';?>" alt="<?=$row_g['title']?>" title="<?=$row_g['title']?>" class="img-responsive"> </a>
                    </div>
                    <div class="col-sm-6 sep-right">
                       <h3 onclick="window.location='<?=WWW?>event-detail-<?=$row_g['id']?>-<?=friendlyURL($row_g['title'])?>.html'"><?=$row_g['title']?></h3>
	                     <p><b>Start Date</b> : <?=date_converter($row_g['start_date'])?>
	                            	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	                            	<b>End Date</b> : <?=date_converter($row_g['end_date'])?>
	                     </p>
	                     <p><b>Location : </b><?=$row_g['city_name']?> > <?=get_combo('states','name',$row_g['state_id'],'','text')?> > <?=get_combo('countries','name',$row_u['country_id'],'','text')?></p>
                      <p> <!-- AddThis Button BEGIN -->
						        <div class="addthis_toolbox addthis_default_style ">
						        <a class="addthis_button_preferred_1"></a>
						        <a class="addthis_button_preferred_2"></a>
						        <a class="addthis_button_preferred_3"></a>
						        <a class="addthis_button_preferred_4"></a>
						        <a class="addthis_button_compact"></a>
						        <a class="addthis_counter addthis_bubble_style"></a>
						        </div>
						        <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
						        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5073efa669740139"></script>
						        <!-- AddThis Button END --> </p>
                    </div>
                    <div class="col-sm-3 text-center sep-top">
                      <a  href="<?=WWW?>event-detail-<?=$row_g['id']?>-<?=friendlyURL($row_g['title'])?>.html" title="Read more" class="btn orange full hvr-float-shadow">Read More</a><br><br>
                      
                      <a href="<?=WWW?>edit-event-<?=$row_g['id']?>.html" class="text-warning"><i class="fa fa-edit"></i></a>
                      <a href="<?=WWW?>delete-event-<?=$row_g['id']?>.html" onclick="return confirm('Are you sure to delete Record');" class="text-danger"><i class="fa fa-times"></i></a>
                    </div>
                  </div>
                </li>
                <?
					      $i++;
					      $count++;
					      $x++;
					  } 
					  ?>
                
              </ul>
            </div>
            
            <div id="pagination-bottom">
               <div class="row">
               
                <div class="col-sm-6">
                 <? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_FRONT){ ?>
					<?php
						$reload = 'events.html?';
						echo paginate_one($reload, $ppage, $tpages);
					?>
					<input type="hidden" name="pagination-page" value="events.html">
				 <? } ?>    
                </div>
                <div class="col-sm-3" id="search-div2">
                  <form id="list-search" method="post" action="">
                  <div class="input-group">
                    <input class="form-control validate[required] input-login" name="txtsearch" placeholder="Search Here" type="text">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit">Go!</button>
                    </span>
                  </div>
				  </form>
                </div>
                <div class="col-sm-3">
                  <a href="<?=WWW;?>add-event.html" class="btn orange full hvr-float-shadow">Create Event</a>
                </div>
              </div>
            </div>
          </div>
		
		
		</div>
	</div>
</div>	

 <script type="text/javascript">
		        
				$('form#list-search').submit(function(e){
					var parentId = $(this).parent().attr('id');
					var string = $('#'+parentId +' form input[name=txtsearch]').val();
					if(string != '' && string != 'Search Here'){
						string = string.replace(/[ ]+/g,'_');
						string = string.replace(/[^a-zA-Z0-9_]+/g,'').toLowerCase();
						if(string.length > 0){
							$('form#list-search').attr('action','<?=WWW;?>events-' + string + '.html');
						}
					}
				});
</script>
<?php include('common/footer.php'); ?>