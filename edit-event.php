<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	
		<? 	if(isset($_GET['action']) && $_GET['action'] == 'deletephoto'){
				$pid 	= $_GET['pid'];
				$row_img= get_record_on_id('photos',$pid);
				mysqli_query($conn,"delete from photos where id = '$pid'");
				unlink('events/'.$_GET['id'].'/'.$row_img['file_name']);
				?>
				<script>
				window.location = '<?=WWW?>edit-event-<?=$_GET['id']?>.html';
				</script>
				<?
			}
			if(isset($_POST) && !empty($_POST)){ 
				$title 	= $_POST['title'];
				$content= $_POST['content'];
				$state_id=$_POST['state_id'];
				$city_name= $_POST['city_name'];
				$isGlobal = ($_POST['is_global'] == 'on')?'1':'0';
				$from_date = $_POST['start_date'];
				$to_date   = $_POST['end_date'];
				
				$date_from1 = strtotime($from_date);
				$date_to    = strtotime($to_date);
				
				if($date_from1 > $date_to) {
				
					echo "<div id='error' class='alert alert-danger'><b>Error : </b>Please Select Correct Date ... Start Date should be less than to End date  ... !</div><br><br>"; 
				}
				
				else {
				
				$start_date = date_converter($_POST['start_date'],'Y-m-d');
				$end_date 	= date_converter($_POST['end_date'],'Y-m-d');
				$user_id 	= $_SESSION['ycdc_dbuid'];
				$id 		= $_GET['id'];
				if(!isset($user_id) || empty($user_id)){
					echo '<div id="error" class="alert alert-danger">Please login first ... !</div>';
				}else{
					if(!empty($title) && !empty($content)){
						$query = "update events set is_global = '$isGlobal', title = '$title', content = '$content', start_date = '$start_date', end_date = '$end_date', state_id = '$state_id', city_name = '$city_name' where id = '$id'";
						if(mysqli_query($conn,$query)){
						
							if(!is_dir('events/'.$id)){
								mkdir('events/'.$id,0777);
							}
							chmod('events/'.$id,0777);
							if(!empty($_FILES['photo']['name'])){
								$filename 	= friendlyURL($title).'.jpg';
								$image 		= new SimpleImage();
								$image->load($_FILES["photo"]["tmp_name"]);
								$image->save('events/'.$id.'/'.$filename);
								chmod('events/'.$id.'/'.$filename,0777);
							
								if(!empty($photo_id)){
									mysqli_query($conn,"update photos set file_name = '$filename' where id = '$photo_id'");	
								}else{
									mysqli_query($conn,"insert into photos (file_name, entity_type, entity_id) values ('$filename','events','$id');");
								}
							}
						}
		                echo '<div id="success" class="alert alert-success">Event Information updated successfully ... !</div>';
		                ?>
		                <script>
		                window.location = '<?=WWW?>my-events.html';
		                </script>
		                <?
		            }else{
						echo '<div id="error" class="alert alert-danger">Please fill all fields ... !</div>';
					}
				}
				
			}

		}
			$id 	= $_GET['id'];
			$row 	= get_record_on_id('events', $id);
			$row_img= mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'events' and entity_id = '$id' order by id desc limit 1"));
		?>
		

<script src="<?=WWW;?>assets/js/ckeditor/ckeditor.js"></script>

<div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1> Update Event </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <form method="post" action="" enctype="multipart/form-data" class="form-horizontal" id="form-add-event">
            
            <div class="white-box">
              <div class="form-group">
                <div class="col-sm-12 text-center">
                  <h2>Event Information</h2>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Event Title</label>
                <div class="col-sm-9">
					<input type="text" name="title" value="<?=$row['title']?>" class="form-control validate[required]">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Is Global</label>
                <div class="col-sm-9">
                  <input name="is_global" type="checkbox" <?php echo ($row['is_global'] == '1')?'checked="checked"':'';?>>
                  <span class="help-block">Event will be Regional Event If checked</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Content</label>
                <div class="col-sm-9">
                  <textarea name="content" id="content"  class="ckeditor" class="validate[required]"><?=$row['content']?></textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Start Date</label>
                <div class="col-sm-3">
                  <input name="start_date" value="<?=date_converter($row['start_date'],'m/d/Y')?>" class="form-control datepicker validate[required]" type="text" autocomplete="off">
                </div>
                <label class="col-sm-3 control-label">End Date</label>
                <div class="col-sm-3">
                  <input name="end_date" value="<?=date_converter($row['end_date'],'m/d/Y')?>" class="form-control datepicker validate[required]" type="text" autocomplete="off">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">State</label>
                <div class="col-sm-3">
                  <select name="state_id" class="form-control validate[required]">
						<option selected="selected" value=""></option>
							<? 	$rs_states = mysqli_query($conn,"select * from states where country_id = '".$row_user['country_id']."'"); 
								while($row_state_l = mysqli_fetch_assoc($rs_states)){ ?>
								<option <?=($row['state_id'] == $row_state_l['id'])?'selected="selected"':'';?> value="<?=$row_state_l['id']?>"><?=$row_state_l['name']?></option>
							<?  } ?>
                  </select>
                </div>
                <label class="col-sm-3 control-label">City</label>
                <div class="col-sm-3">
                  <input name="city_name" value="<?=$row['city_name']?>"  class="form-control validate[required]" type="text">
                </div>
              </div>
             
              <div class="form-group">
                <label class="col-sm-3 control-label">Event Photo</label>
                <div class="col-sm-9">
					<? 	if($row_img){ ?>
						<img src="<?=WWW?>events/<?=$id?>/<?=$row_img['file_name']?>" width="100">
						<a onclick="return confirm('Are you sure to delete Photo');" href="<?=WWW?>edit-event-<?=$_GET['id']?>-deletephoto-<?=$row_img['id']?>.html"><img  src="<?=WWW?>images/icons/delete.png"></a>
						<br>
						
						<input type="hidden" name="photo_id" value="<?=$row_img['id']?>">
						<?	} ?>
                  <input accept="image/*" name="photo" type="file">
                  <span class="help-block">Max Photo size : 2MB</span>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12 text-center">
                  <input value=" Save " class="btn orange hvr-float-shadow" type="submit">
                  <a id="cancel" class="btn blue hvr-float-shadow" href="<?=WWW?>events.html">Cancel</a>
                </div>
              </div>
            </div>
		  </form>
        </div>
        
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
              <?php include('common/right-panel.php');?>
            </div>
          </div>
          
        </div>
      </div>
    </div><!-- /.container -->
	
<script type="text/javascript">
$(document).ready(function(){
	$('#form-add-event').validationEngine();
	$('.datepicker').datepicker();
});
</script>	
	
<?php include('common/footer.php'); ?>