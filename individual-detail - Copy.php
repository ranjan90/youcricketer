<? include('common/header.php'); ?>
<style>
p{margin:0;}
h2{text-transform: capitalize;}
.menu2 li a{padding:0 5px;}
.profile-img{overflow: hidden; height:310px;}
#social{position:relative; top: -25px;}
.testimonial td {
    vertical-align: middle;
}
.table-list {width:100%;border:1px solid #ccc;}
.table-list tr{border:1px solid #ccc;}
.table-list th{padding:5px;background-color:#473E3D;color:#FB7008;font-size:14px;}
.table-list td{padding:5px;text-align:center;}
.table-list a{color:#000;}
.statistics-filters{float:left;margin:6px 15px 0px 0px;}
#statistics-data{float:left;margin:10px 0px 0px 0px;width:100%;}
</style>
<?  if(isset($_GET['id'])){ 
		$id 	= $_GET['id']; 
 
		$row 	= get_record_on_id('users', $id);
		$user_data = $row;
	}else if(isset($_GET['permalink'])){
		$permalink 	= $_GET['permalink'];
		$row 		= mysqli_fetch_assoc(mysqli_query($conn,"select * from users where permalink = '$permalink'"));
	}

?>
<ul id="menu6" class="menu2" style="position:relative; top:-1px;">
	<li><a href="#activity">Activity</a></li>
	<li><a href="#images">Photos</a></li>
	<li><a href="#videos">Videos</a></li>
	<li><a href="#traits">Traits</a></li>
	<li><a href="#affiliations">Affiliations</a></li>
	<li><a href="#experiences">Experience</a></li>
	<?php  if($row['user_type_id'] == 2){ ?>
		<li><a href="#statistics" class="player-stats-link">Statistics</a></li>
	<?php } ?>
	<li><a href="#testimonials">Testimonials</a></li>
	<li><a href="#sponsors">Sponsors</a></li>
	<li><a href="#opportunities">Seeking Opportunities</a></li>
	<li><a href="#travel-requirements">Travel Details</a></li>
	<li><a href="#personal-data">Personality</a></li>
	<li><a href="#contact">Contact</a></li>
	<div class="clear"></div>
</ul>

<? $row_i= mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_id = '$id' and entity_type = 'users' and is_default = '1'"));?>
<? $row_v= mysqli_fetch_assoc(mysqli_query($conn,"select * from videos where entity_id = '$id' and entity_type = 'users' and is_default = '1'"));?>
<? 


$row_city = get_record_on_id('cities', $row['city_id']); ?>
<? $row_states = get_record_on_id('states', $row['state_id']); ?>
<? $row_c= get_record_on_id('countries',$row['country_id']);?>
<? $fields_privacy = explode(',', $row['privacy_settings']); ?>

<?		$location   =  $row['city_name'].' > '.$row_states['name'].' > '.$row_c['name'];
	
?>
		<? 	$fromUserId = $_SESSION['ycdc_dbuid'];
			$toUserId 	= $id;
			if(isset($_POST) && $_POST['action'] == 'invite'){
				$date 		= date('Y-m-d H:i:s');

				$rsChk  = mysqli_query($conn,"select * from friendship where (from_user_id = '$toUserId' and to_user_id = '$fromUserId') or (from_user_id = '$fromUserId' and to_user_id = '$toUserId');");
				if(mysqli_num_rows($rsChk) == 0){
					$query 	= "insert into friendship (from_user_id, to_user_id, create_date, status) 
								values ('$fromUserId','$toUserId','$date','0');";
					if(mysqli_query($conn,$query)){
						echo '<div id="success">Invitation sent ... !</div>';
					}else{
						echo '<div id="error">Invitation cannot be sent ... !</div>';
					}	
				}
			}
			if(isset($_POST) && $_POST['action'] == 'message'){
				$subject 	= $_POST['subject'];
				$message 	= $_POST['comments'];
				$from_user  = $_SESSION['ycdc_dbuid'];
				$to_user 	= $id;
				$date 		= date('Y-m-d H:i:s');

				if($from_user == $to_user){
					echo '<div id="error">You are not allowed to send message to your self ... !</div>';
				}else{
					if(!empty($from_user)){
						$rs_chk = mysqli_query($conn,"select * from messages where from_user_id = '$from_user' and to_user_id = '$to_user' and subject = '$subject' and message = '$message'");
						if(mysqli_num_rows($rs_chk) == 0){
							$query = "insert into messages (from_user_id, to_user_id, subject, message, status, message_date) 
							values ('$from_user','$to_user','$subject','$message','0','$date')";
							mysqli_query($conn,$query);
							echo '<div id="success">Message sent ... !</div>';
						}
					}	
				}
			}
		?>
		<?php if($fields_privacy){ $fields = $fields_privacy; } else { $fields[] = '';}?>
		<div class="profile" style="height: 340px;">
			<div class="profile-img" style="height: 340px;">
				<img src="<?=WWW?><?=($row_i)?'users/'.$id.'/photos/'.$row_i['file_name']:'images/no-photo.png'?>" width="280" height= "340" alt="<?=$row['f_name'].' '.$row['m_name'].' '.$row['last_name']?>">
			</div>
			<div class="profile-info" style="height: 350px;">
				
				<div class="country"><img src="<?=WWW?>countries/<?=$row_c['flag']?>" title="<?=$row_c['name']?>" width="50"></div>
				
				<h2>
					<?=$row['f_name']?>&nbsp;<?=$row['last_name'];?>

				</h2>
				<?
					$p 	= "background-color: #FFFFFF;"; 
					$p1 =  "border-bottom: 1px solid #DDDDDD";
			?>
				
				<p style="<?=$p?>"><strong>Location:</strong> <?=$location?></p>
				
				
				<p style="<?=$p1?>"><strong>Timezone: </strong><?=get_combo('timezones','title',$row['timezone'],'','text')?></p>
				
				
				<p style="background-color: #FFFFFF;border-bottom: 1px solid #DDDDDD;"><strong>Gender: </strong><?=($row['gender'] == '1')?'Male':'Female';?></p>
				
				<?
					$dob = date_converter($row['date_of_birth'],'M')." ".date_converter($row['date_of_birth'],'d');
					 if(in_array('year_of_birth', $fields)){ 
						
						$dob.=", ".	date_converter($row['date_of_birth'],'Y');
							 }?>
				<p style="border-bottom: 1px solid #DDDDDD;"><strong>Birthday:</strong> <?=$dob?></p>
					
			
				<? /* if(in_array('year_of_birth', $fields)){ ?>
				<p style="border-bottom: 1px solid #DDDDDD;"><strong>Birthday:</strong> <?=date_converter($row['date_of_birth'],'Y')?></p>
				<? } */ ?>
				<p style="background-color: #FFFFFF;border-bottom: 1px solid #DDDDDD;"><strong><b>Favorited By: </b></strong> 
				<?php
			
			    $fav = mysqli_query($conn,"SELECT * FROM favorite_users WHERE player_id = '$id'");
				$count = mysqli_num_rows($fav);
				echo $count;
			
				?>
				</p>
				<?php $rol = mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM users WHERE id = '$id'")); 
				$type_id  = $rol['user_type_id'];
				$user_rol = mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM user_types WHERE id = '$type_id'"));

	             ?>
	
				
				<p style="border-bottom: 1px solid #DDDDDD;"><strong>Individual Role: </strong> 
				  <?=$user_rol['name']?> </p> 	

				 <? if($user_rol['name'] == 'Player' ){  ?>
							<? if($rol['type'] == 'Batsman'){
						   $usr_type = "Batsman";
						 }?>
						<?   if($rol['type'] == 'Bowler'){ 
									$usr_type = "Bowler";
										}
							 if($rol['type'] == 'All Rounder'){ 
									$usr_type = "All Rounder";
										}
							 if($rol['type'] == 'Specialist Wicketkeeper'){ 
									$usr_type = "Specialist Wicketkeeper";
										}
					 ?>
			
				<p style="background-color: #FFFFFF;border-bottom: 1px solid #DDDDDD;"><strong>Type of Player:</strong>
					<?   echo $usr_type; ?>
				 </p>
					<? if($rol['type'] == 'Batsman'){ ?>
						<p style="border-bottom: 1px solid #DDDDDD;"> <b>Batting Hand: </b> <?=$rol['type_of_batsman']?></p>
						<p style="background-color: #FFFFFF;"> <b>Batting Order:</b><?=$rol['batting_order']?></p>
					<? }else if($rol['type'] == 'Bowler'){ ?>
						<p style="border-bottom: 1px solid #DDDDDD;"><b>Bowler Type: </b> <?=$rol['type_of_bowler']?></p>
						<p style="background-color: #FFFFFF;"><b>Bowler Arm: </b> <?=$rol['which_arm_bowler']?></p>
						
					<? } ?>
				
				
				<p style="border-bottom: 1px solid #DDDDDD;"> <strong>Preferred Match Type:</strong> <?=$rol['match_types']?></p>
				
				
				<? } ?>
						<? if($rol['first_class_player'] == 1){ ?> <?  $p3 ="border-bottom: 1px solid #DDDDDD";  
						$p4  = "background-color: #FFFFFF";
						 ?>
					<p style="<?=$p?>"><strong>First Class Category:</strong> Yes</p>
					<? } else { $p3 = "background-color: #FFFFFF"; $p4 = "border-bottom: 1px solid #DDDDDD"; } ?>
				<?php
			
   

				/* $today = new DateTime();
				$birthdate = new DateTime($row['date_of_birth']);
				$interval = $today->diff($birthdate);
				echo $interval->format('%y years'); */
			    $opp = mysqli_query($conn,"SELECT * FROM opportunities WHERE user_id = '$id' AND type='domestic'");
				$rowO = mysqli_num_rows($opp);
				if($rowO > 0) {?> 

						<p style="<?=$p3?>"><strong>Actively Seeking Domestic Opportunity:</strong> 
			
					<? echo "Yes"; } 
				?>
				</p>
					<?php
			
			    $opp = mysqli_query($conn,"SELECT * FROM opportunities WHERE user_id = '$id' AND type='international'");
				$rowO = mysqli_num_rows($opp);
				if($rowO > 0) {?>    
				<p style="<?=$p4?>"><strong>Actively Seeking International Opportunity:</strong> 
			
				<? echo "Yes"; } 
				?>
				</p>
				<div style="min-height:30px;">
					<?php if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){?>
					<div class="left">
					<? if($fromUserId != $toUserId){ ?>
						<? 	$rsInvite = mysqli_query($conn,"select * from friendship where from_user_id = '$fromUserId' and to_user_id = '$toUserId'");
							if(mysqli_num_rows($rsInvite) == 0){ ?>
						<form method="post">
							<input type="hidden" name="action" value="invite">
							<input style="margin-top:5px;float:left;" type="submit" value="Invite" id="contact-btn" title="Invite to increase your network" class="submit-login">
						</form>
						<? }else{
							$rowInvite = mysqli_fetch_assoc($rsInvite);
							if($rowInvite['status'] = '1'){
								echo '<span><b>Already Friend</b></span>';	
							}else{
								echo '<span><b>Invitation Sent</b></span>';
							}
						} ?>
					<? } ?>
					</div>
					<?php }else{ ?>
						<div class="left">
							<button style="position:relative; top:10px;" id="contact-btn1" class="submit-login popup-link">Invite Friend</button>
						</div>
					<?php } ?>
					
					<? $row_user = mysqli_fetch_assoc(mysqli_query($conn,"select * from users where id = '$id'")); ?>
					<div style="float:left;margin-left:25px;margin-top:7px;">
						<? if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){ $user_id = $_SESSION['ycdc_dbuid'];?>
						<? 	$row_fav = mysqli_fetch_assoc(mysqli_query($conn,"select * from favorite_users where player_id = '$id' AND user_id = '$user_id'"));
						if($row_fav){
							?><a class="right star" style="cursor:pointer" id="<?=$_SESSION['ycdc_dbuid'];?>-<?=$id?>-<?php echo "remove";?>"  title="Remove Favorite"><img src="<?=WWW?>images/golden-star.png"></a><?
							}else{
							?><a class="right star" style="cursor:pointer" id="<?=$_SESSION['ycdc_dbuid'];?>-<?=$id?>-<?php echo "add";?>" title="Add to Favorite"><img src="<?=WWW?>images/silver-star.png"></a><?
							}
						  }
						?>
					</div>
				
					<div class="right">
						<p style="<?=(!isset($_SESSION['ycdc_dbuid']))?'position:relative; top:-15px;':'';?>">
						<? if(isset($_SESSION['ycdc_dbuid']) && $_SESSION['ycdc_dbuid'] != $_GET['id']){ ?>
						<a style="position:relative; top:5px;" id="contact-btn" href="<?=(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid']))?'#contact-tab':'#contact-message';?>" class="submit-login popup-link">Contact</a>
						<? }else if(!isset($_SESSION['ycdc_dbuid'])){ ?>
						<button style="position:relative; top:25px;" id="contact-btn1" class="submit-login popup-link">Contact</button>
						<? } ?>
					</div>
				</div>
				
				

				</p>
				<div class="clear"></div>
				
			</div>
			<div class="profile-contact" style="height: 320px;" >
				<div id="last-login" style="display:none;">
				<?php 	$rowActivity = mysqli_fetch_assoc(mysqli_query($conn,"select * from activity where activity like '%Logged in%' and user_id = '".$row['id']."'"));
						if($rowActivity){
							echo 'Last '.str_replace('_', ' ', $rowActivity['activity']).' on '.date_converter($rowActivity['activity_date']);
						}
				?>
				</div>
				<? 
					$video = $row_v['file_name']; 
				

	if(!empty($video)){
							if(preg_match('/<iframe(.*)<\/iframe>/', $row_v['file_name'])){ 
									
											preg_match('/src="(.*?)"/',$row_v['file_name'] , $src);
											$src = $src[1];
											$video = "<iframe width='365' height='320' allowfullscreen src='$src'></iframe>";
									
											
												}else{
						$video_path = WWW.'videos/'.$row_v['id'].'/'.$row_v['file_name'];
							$video = '<video width="365" height="320" controls>
									  <source src="'.$video_path.'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
									  <source src="'.$video_path.'" type=\'video/ogg; codecs="theora, vorbis"\'>
									  <source src="'.$video_path.'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
									  <source src="'.$video_path.'" type=\'video/webm; codecs="vp8, vorbis"\'>
									</video>';
						}
					}else{
						$video = '<img src="'.WWW.'images/no-video.jpg" width="365" height="340">';
					}
					echo $video; 
					?>
			</div>
		</div>


		
	<div class="middle">
		<ul id="menu6_1" class="menu2">
					<li><a href="#activity">Activity</a></li>
					<li><a href="#images">Photos</a></li>
					<li><a href="#videos">Videos</a></li>
					<li><a href="#traits">Traits</a></li>
					<li><a href="#affiliations">Affiliations</a></li>
					<li><a href="#experiences">Experience</a></li>
					<?php  if($row['user_type_id'] == 2){ ?>
						<li><a href="#statistics" class="player-stats-link">Statistics</a></li>
					<?php } ?>
					<li><a href="#testimonials">Testimonials</a></li>
					<li><a href="#sponsors">Sponsors</a></li>
					<li><a href="#opportunities">Seeking Opportunities</a></li>
					<li><a href="#travel-requirements">Travel Details</a></li>
					<li><a href="#personal-data">Personality</a></li>
					<li><a href="#contact">Contact</a></li>
					<div class="clear"></div>
			  	</ul>
		<div class="content max-width-grid-list">
			<div class="white-box">
			
				
			<? if(!isset($_SESSION['ycdc_user_name'])
		   || empty($_SESSION['ycdc_user_name'])){
			?>
			<script>
			$('#menu6 li a,#menu6_1 li a').live('click', function(){
				$('#contact-message').removeClass('hide');
			});
			
			</script>
			<script>
			$('#contact-btn1').live('click', function(){
				$('#contact-message').removeClass('hide');
			});
			</script>
			<div id="contact-message" class="hide">
			<div id="success"><b>Please Login or Register!</b></div>
			<p><?php $rowWhoopsMsg = get_record_on_id('cms', 23); echo $rowWhoopsMsg['content'];?></p>
			<?
			include('common/login-box.php');
			?></div><?
		   }else{ ?>			  	
		   <? if(in_array('Status/Activity', $fields)){ ?>
				<div id="activity" class="content2" style="width:98%;">
				   <? if(in_array('Status/Activity', $fields)){ ?>
				   <style>
					
					#dashboard-activity{position:relative;border: 1px solid orange; border-radius: 4px; margin: 3px 0px; padding: 4px;}
					#dashboard-activity #dashboard-activity-owner{float:left; width:90%;}
					#dashboard-activity #dashboard-activity-owner img{width:40px; height: 55px; float:left;}
					#dashboard-activity #dashboard-activity-owner h2{float:left; margin-left:10px; width:90%;}
					#dashboard-activity #dashboard-activity-owner #dashboard-datetime{display: block; margin:10px 50px;}
					#dashboard-activity #dashboard-activity-options{width:150px;position:absolute; top: 5px; right: 5px;}
					#dashboard-activity #dashboard-activity-options a{float:right;}
					#dashboard-activity #dashboard-activity-like{line-height: 22px;}
					#dashboard-activity #dashboard-activity-like a{padding:0px 4px;}
					#dashboard-activity-content{max-height: 100px;}
					#dashboard-activity-content{}
					.middle{min-height: 700px;}
					#comment img, #sharer img, #lover img{width:40px; height: 45px; float:left;}
					#comment h2, #sharer h2, #lover h2{float:left; margin-left:10px; width:80%;}
					#comment, #sharer, #lover{clear:both; height:50px;}
					#comments, #sharers, #lovers{max-height:500; overflow-y: scroll;}
					#comment h2{margin:0px; padding:0px 5px; font-size: 13px;}
					#comment p{padding:0px 5px; line-height: 22px;}
					</style>
						<? /*	$rs_activities = mysqli_query($conn,"select * from activity where user_id = '".$row_u['id']."' and status = '1' order by id desc");
						if(mysqli_num_rows($rs_activities) == 0){
							echo '<div id="error">Not Recently Updated</div>';
						}else{
							while($row_act = mysqli_fetch_assoc($rs_activities)){ 
							?>
							<dl>
								<dt><span><?=date_converter($row_act['activity_date'])?></span></dt>
								<dd><?=$row_act['activity']?></dd>
							</dl>
							<?
							}	
						}*/
						?>
						
						<?php $rs_msg = mysqli_query($conn,"select * from activity where user_id =".$row['id']." and activity not like '%Logged in%' order by id desc limit 15"); 
			        		if(mysqli_num_rows($rs_msg) == 0){
			        			?>
			        			<div id="error">No Activity</div>
			        			<?
			        		}else{
				        		while($row_msg = mysqli_fetch_assoc($rs_msg)){ 
				        			$rowUserActivity= get_record_on_id('users', $row_msg['user_id']);
				        			$row_img 		= mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$rowUserActivity['id']."' and is_default = '1' "));
				        			$rowStatusLike	= mysqli_query($conn,"select * from status_likes where activity_id = '".$row_msg['id']."' and user_id = '$userId'");
				        			$rsStatusLike 	= mysqli_query($conn,"select * from status_likes where activity_id = '".$row_msg['id']."'");
				        			$rsStatusShare 	= mysqli_query($conn,"select * from status_shares where activity_id = '".$row_msg['id']."'");
				        			$rsStatusComment= mysqli_query($conn,"select * from activity_comments where activity_id = '".$row_msg['id']."'");
				        			if($rowStatusLike){
				        				$imgTitle 	= 'Loved It';
				        			}else{
				        				$imgTitle 	= 'Love It';
				        			}
				        			$loversData 	= array();
				        			$sharersData 	= array();
				        			$commentData 	= array();
				        			if(mysqli_num_rows($rsStatusComment) > 0){
				        				while($rowStatusComment = mysqli_fetch_assoc($rsStatusComment)){
				        					$commentData[]    = $rowStatusComment['user_id'];
				        				}
				        				$commentCounter = '('.mysqli_num_rows($rsStatusComment).')';
				        			}else{
				        				$commentCounter = '';
				        			}
				        			if(mysqli_num_rows($rsStatusLike) > 0){
				        				while($rowStatusLike = mysqli_fetch_assoc($rsStatusLike)){
				        					$loversData[]    = $rowStatusLike['user_id'];
				        				}
				        				$loveCounter = '('.mysqli_num_rows($rsStatusLike).')';
				        			}else{
				        				$loveCounter = '';
				        			}
				        			if(mysqli_num_rows($rsStatusShare) > 0){
				        				while($rowStatusShare = mysqli_fetch_assoc($rsStatusShare)){
				        					$sharersData[]    = $rowStatusShare['user_id'];
				        				}
				        				$sharersCounter = '('.mysqli_num_rows($rsStatusShare).')';
				        			}else{
				        				$sharersCounter = '';
				        			}
				        			?>
				        			<div id="dashboard-activity">
				        				<div id="dashboard-activity-owner">
				        					<img src="<?php echo WWW;?><?=($row_img && !empty($row_img['file_name']))?'users/'.$rowUserActivity['id'].'/photos/'.$row_img['file_name']:'images/no-photo.jpg'?>" width="90" height="100" />
				        					<h2><?php echo $rowUserActivity['f_name'].' '.$rowUserActivity['last_name'];?></h2>
				        					<div id="dashboard-datetime"><?=date_converter($row_msg['activity_date'])." ".date("H:i:s",strtotime($row_msg['activity_date']))?></div>
				        				</div>
				        				<div id="dashboard-activity-options">
				        					<?php if($row_msg['user_id'] == $userId){ ?>
						        				<a onclick="return confirm('Are you sure that you want to delete this record ?')" href="<?=WWW?>dashboard.html?action=delete&type=activity&id=<?=$row_msg['id']?>"><img src="<?=WWW?>images/icons/delete.png"></a>
						        				<a class="popup-link" href="#edit-activity" id="<?=$row_msg['id']?>"><img src="<?=WWW?>images/icons/edit.png"></a>
						        			<?php } ?>
				        				</div>
				        				<div class="clear"></div>
				        				<div id="dashboard-activity-content">
				        					<div class="right" id="gallery" >
				        					<? 	$rsPhotos = mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_msg['user_id']."' and title = '".$row_msg['activity']."'");
				        						$rsVideos = mysqli_query($conn,"select * from videos where entity_type = 'users' and entity_id = '".$row_msg['user_id']."' and title = '".$row_msg['activity']."'");
				        						if(mysqli_num_rows($rsPhotos) > 0){
				        							$rowPhoto = mysqli_fetch_assoc($rsPhotos);
				        						?>
				        						<a href="<?=WWW?>users/<?=$row_msg['user_id']?>/photos/<?=$rowPhoto['file_name']?>" class="photo">
				        							<img style="border-radius:4px; border:1px solid #ccc;" src="<?=WWW?>users/<?=$row_msg['user_id']?>/photos/<?=$rowPhoto['file_name']?>" width="100">
				        						</a><?
				        						}
				        						if(mysqli_num_rows($rsVideos) > 0){
				        							$rowVideo = mysqli_fetch_assoc($rsVideos);
				        							if(!empty($rowVideo['file_name'])){
				        								$filename = explode('.',$rowVideo['file_name']);
														$filename1= $filename[0];
														$video = '<video width="220" height="130" controls>
																  <source src="'.WWW.'videos/'.$rowVideo['id'].'/'.$filename1.'.mp4" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
																  <source src="'.WWW.'videos/'.$rowVideo['id'].'/'.$filename1.'.ogg" type=\'video/ogg; codecs="theora, vorbis"\'>
																  <source src="'.WWW.'videos/'.$rowVideo['id'].'/'.$filename1.'.mov" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
																  <source src="'.WWW.'videos/'.$rowVideo['id'].'/'.$filename1.'.webm" type=\'video/webm; codecs="vp8, vorbis"\'>
																</video>';
														echo $video;
				        							}
				        						}
				        					?>
				        					</div>
				        					<div class="left"><?=$row_msg['activity']?></div>
				        					<div class="clear"></div>
				        				</div>
				        				<div class="clear"></div>
				        				<?php /* ?><div id="dashboard-activity-like">
				        					<a href="#clicktoloveit" class="love-it" id="<?=$row_msg['id']?>">
				        						<img title="<?=$imgTitle?>"  src="<?=WWW?>images/icons/<?=(in_array($_SESSION['ycdc_dbuid'], $loversData))?'love-it.png':'love-it-gray.png';?>">
				        					</a>
				        					<a class="popup-link love-it-counter" href="#activity-likes" title="Who Loved it" id="<?=$row_msg['id']?>"><?=$loveCounter;?></a>
				        					<a href="#clicktoshareit" title="Share It" class="share-it" id="<?=$row_msg['id']?>">
				        						<img src="<?=WWW?>images/icons/share-it.png">
				        					</a>
				        					<a title="Who Shared it" class="popup-link share-it-counter" href="#activity-shares" id="<?=$row_msg['id']?>"><?=$sharersCounter?></a>
				        					<a class="popup-link" href="#activity-comments" title="Comment on It" class="comment-it" id="<?=$row_msg['id']?>">
				        						<img src="<?=WWW?>images/icons/comments.png"><?=$commentCounter?>
				        					</a>
				        					<a title="Who Commented it" class="popup-link comment-it-counter" href="#activity-comments" id="<?=$row_msg['id']?>"><?=$commentsCounter?></a>
				        				</div><?php */ ?>
				        			</div>
				        			<div class="clear"></div>
				        	<?  } } ?>
							
				<? }else{ ?>
					<? 	$rowMsg = get_record_on_id('cms', 21); 
						echo '<div id="information">'.$rowMsg['content'].'</div>';
					?>
				<? } ?>
				</div> 
				<? } ?>
				<? if(in_array('photos_Albums', $fields)){ ?>
				<div id="images" class="content1">
					<div id="gallery" class="content1">
					<? 	$rs_imgs = mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '$id' and is_default = ''");
						while($row_img = mysqli_fetch_assoc($rs_imgs)){ ?>
						<div style="height:175px;width:150px;float:left; margin:5px;">
							<a class="photo" href="<?=WWW?>users/<?=$id?>/photos/<?=$row_img['file_name']?>">	
								<img style=" border-radius:4px;" src="<?=WWW?>users/<?=$id?>/photos/<?=$row_img['file_name']?>" alt="<?=$_SESSION['ycdc_user_name']?>" width="150" height="150"/>
							</a>
						</div>
					<?  } ?>
					</div>
				</div>
				<? } ?>
				<? if(in_array('videos_Albums', $fields)){ ?>
				<div id="videos" class="content1">
					<? 	$rs_imgs = mysqli_query($conn,"select * from videos where entity_type = 'users' and entity_id = '$id' and is_default = '0' ");
						if(mysqli_num_rows($rs_imgs) > 0){
							while($row_img = mysqli_fetch_assoc($rs_imgs)){ 
								$video = $row_img['file_name']; 
				

	if(!empty($video)){
							if(preg_match('/<iframe(.*)<\/iframe>/', $row_img['file_name'])){ 
									
											preg_match('/src="(.*?)"/',$row_img['file_name'] , $src);
											$src = $src[1];
											$video = "<iframe width='240' height='200' allowfullscreen src='$src'></iframe>";
									
											
												}else{
						$video_path = WWW.'videos/'.$row_img['id'].'/'.$row_img['file_name'];
							$video = '<video width="240" height="200" controls>
									  <source src="'.$video_path.'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
									  <source src="'.$video_path.'" type=\'video/ogg; codecs="theora, vorbis"\'>
									  <source src="'.$video_path.'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
									  <source src="'.$video_path.'" type=\'video/webm; codecs="vp8, vorbis"\'>
									</video>';
						}
					}else{
						$video = '<img src="'.WWW.'images/no-video.jpg" width="365" height="340">';
					}
					
								?> 
								<div style="height:240px;width:320px;float:left; margin:2px;" id="<?php echo $row_img['id'];?>" class='video'>
									<? echo $video;?>
								</div>
						<?php }//while ?>
					<? }else{ ?>
					<p>No Video .... !</p>
					<? } ?>
				</div>
				<? } ?>
				
				<div id="traits" class="content1">
<? if($rol['first_class_player'] == 1){ ?>
					<p><strong>First Class</strong> (<a target="_blank" href="<?=$row['espncrickinfo']?>">ESPN Profile</a>) </p>
<? } ?>
<? /*
					<p><strong>Playing Role</strong> <?=get_player_type_combo($row['type'],'text')?></p>

					<?  if($row['type'] == 'Batsman'){ ?>
					<p><strong>Type of Batsman:</strong> <?=$row['type_of_batsman']?></p>
					<p><strong>Preferred Batting Order:</strong> <?=$row['batting_order']?></p>
					<? } */?>
					<? if($row['type'] == 'Bowler'){ ?>
				<? /*	<p><strong>Type of Bowler:</strong> <?=$row['type_of_bowler']?></p>
					<p><strong>With Which Arm do you Bowl?:</strong> <?=$row['which_arm_bowler']?></p>  */ ?>
					<p><strong>Bowling Preference in One Day Games:</strong> <?=$row['bowl_between_overs']?></p>
					<? } ?>
				<? /*	<p><strong>Match Types:</strong> <?=$row['match_types']?></p> */ ?>
					<p><strong>Games Types:</strong> <?=$row['game_types']?></p>

				</div>
				
				
				<div id="affiliations" class="content1">
					<h3>Clubs</h3>
					<? $rsClubs = mysqli_query($conn,"select c.title,utc.* from users_to_clubs utc join clubs c on c.id = utc.club_id where utc.user_id = '".$row['id']."' order by utc.sort asc");?>
					<table style="width:650px;" cellpadding="6" cellspacing="4">
						<tr><tr><td><b>Name</b></td><td><b>Preference</b></td><td><b> Affiliation Date</b></td><td><b>Youcricketer Date & Time</b></td></tr></tr>
						<? while($rowClub = mysqli_fetch_assoc($rsClubs)){ ?>
							<tr><td width="40%"><a href="advance-search.html?type=All&country=All&city=&companyname=<?=$rowClub['title']?>&yearofservice=All&company=Search#companies-tab"><?=$rowClub['title']?></a> </td>
							<td width="15%" style="padding-left:20px;"><?=$rowClub['sort']?> </td>
							<td width="15%"><?=$rowClub['affiliation_date']?> </td>
							<td width="30%"><?=$rowClub['date_added']?> </td></tr>
						<? } ?>
					</table>
					<br/>
					<h3>Leagues</h3>
					<? $rsClubs = mysqli_query($conn,"select c.title, utc.* from users_to_leagues utc join leagues c on c.id = utc.league_id where utc.user_id = '".$row['id']."' order by utc.sort asc");?>
					<table style="width:650px;" cellpadding="6" cellspacing="4">
						<tr><tr><td><b>Name</b></td><td><b>Preference</b></td><td><b> Affiliation Date</b></td><td><b>Youcricketer Date & Time</b></td></tr></tr>
						<? while($rowClub = mysqli_fetch_assoc($rsClubs)){ ?>
							<tr><td width="40%"><a href="advance-search.html?type=All&country=All&city=&companyname=<?=$rowClub['title']?>&yearofservice=All&company=Search#companies-tab"><?=$rowClub['title']?></a> </td>
							<td width="15%" style="padding-left:20px;"><?=$rowClub['sort']?> </td>
							<td width="15%"><?=$rowClub['affiliation_date']?> </td>
							<td width="30%"><?=$rowClub['date_added']?> </td></tr>
						<? } ?>
					</table>
					<br/>
						<h3>Companies</h3>
					<? $rsAgent = mysqli_query($conn,"select * from agents  where user_id = '".$row['id']."' order by sort asc");?>
					<table style="width:650px;" cellpadding="6" cellspacing="4">
						<tr><tr><td><b>Name</b></td><td><b>Preference</b></td><td><b> Affiliation Date</b></td><td><b>Youcricketer Date & Time</b></td></tr></tr>
						<? while($rowClub = mysqli_fetch_assoc($rsAgent)){ ?>
							
							<tr><td width="40%"><a href="advance-search.html?type=All&country=All&city=&companyname=<?=$rowClub['name']?>&yearofservice=All&company=Search#companies-tab"><?=$rowClub['name']?></a> </td>
							<td width="15%" style="padding-left:20px;"><?=$rowClub['sort']?> </td>
							<td width="15%"><?=$rowClub['affiliation_date']?> </td>
							<td width="30%"><?=$rowClub['date_added']?> </td></tr>
						<? } ?>
					</table>
					
					
				</div>
				
				<? if(in_array('experiences', $fields)){ ?>
				<div id="experiences" class="content1 max-width-grid-list">
					<p><strong>Currently Working:</strong> <?=($row['currently_playing'] == '1')?'Yes':'No';?></p>
					<p><strong>Years of Experience:</strong> <?=$row['no_of_playing'];?></p>
					<p><strong>Overall experience:</strong><?=$row['other_information']?></p>

					<table width="100%" border="1" style="border-collapse:collapse;" id="">
				        	<tr>
				        		<th class="tleft">Country</th>
				        		<th class="tleft">Organization</th>
				        		<th class="tleft">Job Title</th>
				        		<th class="tleft">From Date</th>
				        		<th class="tleft">To Date</th>
				        	</tr>
				        <? 	$rs_msg = mysqli_query($conn,"select * from experiences where user_id = '".$row_user['id']."' order by to_date desc "); 
				        	if(mysqli_num_rows($rs_msg) == 0){
				        		?>
				        		<tr>
				        			<td colspan="3"><td>
				        		</tr>
				        		<?
				        	}else{
					       		while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
					       		<tr title="Click for detail">
					       			<td><?=get_combo('countries','name',$row_msg['country_id'],'','text')?></td>
					       			<td><?=$row_msg['organization']?></td>
					       			<td><?=$row_msg['job_title']?></td>
					       			<td><?=date_converter($row_msg['from_date'])?></td>
					       			<td><?=date_converter($row_msg['to_date'])?></td>
					       		</tr>
					       	<?  }
					       	} ?>
				        </table>
				</div>
				<? } ?>
				
				<?php  if($row['user_type_id'] == 2){ ?>
				
				<div id="statistics" class="content1" style="width:98%;">
					<h1 style="margin:10px 0px 10px 15px;"> Statistics </h1>
					<?php $game_type = array('Ten10'=>'Ten10','Fifteen15'=>'Fifteen15','Twenty20'=>'Twenty20','Thirty30'=>'Thirty30','One Day'=>'One Day');
						$overs_type = array('10'=>'10','15'=>'15','20'=>'20','30'=>'30','35'=>'35','40'=>'40','45'=>'45','50'=>'50','55'=>'55','60'=>'60');
						$ball_type = array('hard_ball'=>'Hard Ball','rubber_ball'=>'Rubber Ball','tape_ball'=>'Tape Ball','tennis_ball'=>'Tennis Ball');
					?>
					<div class="statistics-filters">
						
						<?php $rs_leagues = mysqli_query($conn,'Select DISTINCT c.id,c.company_name from companies as c inner join tournaments as t on c.id=t.league_id ORDER BY c.company_name'); ?>
						<select name="league_select_stats" id="league_select_stats" style="width:200px;">
						<option value="0">All Leagues</option>
						<?php while($row1 = mysqli_fetch_assoc($rs_leagues)): ?>
							<option value="<?php echo $row1['id']; ?>"><?php echo $row1['company_name']; ?></option>
						<?php endwhile; ?>
						</select>
						
						<?php //$rs_teams = mysqli_query($conn,"Select max(year) as max_year, min(year) as min_year FROM tournaments as t inner join tournament_teams as tt on t.id=tt.tournament_id inner join tournament_bat_scorecard as tbs on tbs.team_id=tt.team_id WHERE tbs.batsman_id = ".$row['id']);
						$rs_teams = mysqli_query($conn,"Select max(year) as max_year, min(year) as min_year FROM tournaments as t inner join tournament_teams as tt on t.id=tt.tournament_id "); // for player who have not played any tournament
						$year_row = mysqli_fetch_assoc($rs_teams);?>
						<select name="year_select_stats" id="year_select_stats" style="width:100px;">
							<option value="0">All Years</option>
							<?php for($i=$year_row['min_year'];$i<=$year_row['max_year'];$i++): ?>
								<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
							<?php endfor; ?>
						</select>
						
						<?php /*$rs_tournaments = mysqli_query($conn,"Select t.id,t.title from tournaments as t inner join tournament_teams as tt on t.id=tt.tournament_id 
						inner join tournament_bat_scorecard as tbs on tbs.team_id=tt.team_id 
						WHERE tbs.batsman_id = ".$row['id'].' ORDER BY title');*/
						$rs_tournaments = mysqli_query($conn,"Select distinct t.id,t.title from tournaments as t inner join tournament_teams as tt on t.id=tt.tournament_id ORDER BY t.title");
						?>
						<select name="tournament_select_stats" id="tournament_select_stats" style="width:180px;">
						<option value="0">All Tournaments</option>
						<?php while($row1 = mysqli_fetch_assoc($rs_tournaments)): ?>
							<option value="<?php echo $row1['id']; ?>"><?php echo $row1['title']; ?></option>
						<?php endwhile; ?>
						</select>
						
						<select name="game_types_select_stats" id="game_types_select_stats" style="width:120px;">
							<option value="0">Game Type</option>
							<?php foreach($game_type as $key=>$val):  ?>
								<option <?php echo $sel; ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
							<?php endforeach; ?>
						</select>
						<select name="overs_type_select_stats" id="overs_type_select_stats" style="width:100px;">
							<option value="0">Overs</option>
							<?php foreach($overs_type as $key=>$val):  ?>
								<option <?php echo $sel; ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
							<?php endforeach; ?>
						</select>
						<select name="ball_type_select_stats" id="ball_type_select_stats" style="width:100px;">
							<option value="0">Ball Type</option>
							<?php foreach($ball_type as $key=>$val):  ?>
								<option <?php echo $sel; ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
							<?php endforeach; ?>
						</select>
						
						<div class="clear"><br/></div>
						<input type="button" name="player_stats_search" id="player_stats_search" class="submit-login" value="Search" style="float:none;margin-left:20px;">
					</div>
					<div class="clear"><br/></div>
					<div id="player-statistics-data"></div>
				</div>
				<?php } ?>
				
				<div id="sponsors" class="content1  max-width-grid-list" style="width:600px;">
				Page Under Construction
				</div>
				
				<div id="testimonials" class="content1" style="width:900px;">
				
                <ul>
                	<?  $rpp = PRODUCT_LIMIT_FRONT; // results per page
            			$ppage = intval($_GET["page"]);
      					if($ppage<=0) $ppage = 1;
      					$query = "select * from testimonials where status=1 and entity_id =".$row_user['id'];
				      	$query .= " order by id desc ";
				     
				      $rs   = mysqli_query($conn,$query);
				      $tcount = mysqli_num_rows($rs);
				      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
				      $count = 0;
				      $i = ($ppage-1)* $rpp;
          			  $x = 0;
          			  //=======================================
						while(($count<$rpp) && ($i<$tcount)){
                		mysqli_data_seek($rs,$i);
                		$row_t 	= mysqli_fetch_array($rs);
                		$row_i  = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_t['user_id']."' and is_default = '1'"));
                		?>
      					<div class="testi-box">
						<table class="testimonial"><tr><td>  <?php if(!empty($row_i['file_name']) && file_exists('users/'.$row_t['user_id'].'/photos/'.$row_i['file_name'])){?>
							<img style="margin:4px" src="<?=WWW?><?='users/'.$row_t['user_id'].'/photos/'.$row_i['file_name']?>" alt="<?=get_combo('users','concat(f_name," ",last_name)',$row_t['user_id'],'','text')?>" width="40">
							<?php }else{ ?>
								<img  src="<?=WWW?>images/no-photo.png" width="70" height="65">
							<?php } ?></td>
							<td valign="top"><h2><?=get_combo('users','concat(f_name," ",last_name)',$row_t['user_id'],'','text')?> for </h2></td>
							
							<td><?php $row_i  = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_t['entity_id']."' and is_default = '1'")); ?>
							 <?php if($row_i['file_name'] && file_exists('users/'.$row_t['entity_id'].'/photos/'.$row_i['file_name'])){?>
							<img style="margin:4px" src="<?=WWW?><?='users/'.$row_t['entity_id'].'/photos/'.$row_i['file_name']?>" alt="<?=get_combo('users','concat(f_name," ",last_name)',$row_t['entity_id'],'','text')?>" width="40">
							<?php } else { ?>
								<img  src="<?=WWW?>images/no-photo.png" width="70" height="65">
							<?php } ?></td>
							
							<td><h2><?=get_combo('users','concat(f_name," ",last_name)',$row_t['entity_id'],'','text')?> on <?=date_converter($row_t['testimonial_date'],'M d,Y H:i')?></h2></td>
							<td><div class="stars" style="position:relative;margin-left:50px;">
								 
	                           
	                           
							<? 	$rating = $row_t['rating'];
								for($x = 0; $x < 5; $x++){
									if($x < $rating){
									?><img src="<?=WWW?>images/star-fill-gray.jpg" title="<?=$rating?>/5"><?
									}else{
									?><img src="<?=WWW?>images/star-gray.jpg" title="<?=$rating?>/5"><?
									}
								}
							?>
							</div></td>
						
						</tr>
						<tr><td></td><td colspan="6" style="padding-left:1px;"><p><?=$row_t['content']?></p></td></tr>
						</table>
						
						</div>
					    <?
					      $i++;
					      $count++;
					      $x++;
					  } 
					  ?>
			</ul>
			</div>
				
				
				<div id="opportunities" class="content1 max-width-grid-list">
					<? $rs_msg = mysqli_query($conn,"select * from opportunities where user_id = '".$row_user['id']."' and type='domestic' order by to_date desc ");  ?>
					<? if(mysqli_num_rows($rs_msg) > 0){ ?>
					<p><strong>Domestic Opportunities:</strong> Yes</p>
					<table width="100%" border="1" style="border-collapse:collapse;" id="inbox">
					        	<tr>
					        		<th class="tleft">State</th>
					        		<th class="tleft">City</th>
					        		<th class="tleft">From Date</th>
					        		<th class="tleft">To Date</th>
					        	</tr>
					        <? 	if(mysqli_num_rows($rs_msg) > 0){
					        		while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
						       		<tr title="Click for detail">
						       			<td><?=get_combo('states','name',$row_msg['state_id'],'','text')?></td>
						       			<td><?=$row_msg['city_name']?></td>
						       			<td><?=date_converter($row_msg['from_date'])?></td>
						       			<td><?=date_converter($row_msg['to_date'])?></td>
						       		</tr>
						       	<?  }
						       	} ?>
					        </table>
					<? } ?>
					<? $rs_msg = mysqli_query($conn,"select * from opportunities where user_id = '".$row_user['id']."' and type='international' order by to_date desc ");  ?>
					<? if(mysqli_num_rows($rs_msg) > 0){ ?>
					<p><strong>International Opportunities:</strong> Yes</p>
					<table width="100%" border="1" style="border-collapse:collapse;" id="inbox">
					        	<tr>
					        		<th class="tleft">Country</th>
					        		<th class="tleft">State</th>
					        		<th class="tleft">City</th>
					        		<th class="tleft">From Date</th>
					        		<th class="tleft">To Date</th>
					        	</tr>
					        <? 	if(mysqli_num_rows($rs_msg) > 0){
					        		while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
						       		<tr title="Click for detail">
						       			<td><?=get_combo('countries','name',$row_msg['country_id'],'','text')?></td>
						       			<td><?=get_combo('states','name',$row_msg['state_id'],'','text')?></td>
						       			<td><?=$row_msg['city_name']?></td>
						       			<td><?=date_converter($row_msg['from_date'])?></td>
						       			<td><?=date_converter($row_msg['to_date'])?></td>
						       		</tr>
						       	<?  }
						       	} ?>
					        </table>
					<? } ?>
				</div>
				
				
				<div id="travel-requirements" class="content1  max-width-grid-list">
					<p><strong>Citizenship:</strong>
						<? 	$countries 	= explode(',',$row['citizen']); 
							$cName 		= array();
							foreach($countries as $c){ 
								$rowCountry = get_record_on_id('countries', $c);
								$cName[] = $rowCountry['name'];
							}
							echo implode(',', $cName);
						?>
					</p>
					<p><strong>International Driving License:</strong><?=($row_user['driving_license'] == '1')?'Yes':'No';?></p>
					<p><strong>Valid Passport:</strong><?=($row_user['passport'] == '1')?'Yes':'No';?></p>
					<p><strong>Passport Expiration Date:</strong><?=$row_user['passport_expiry']?></p>
					<p><strong>Passport Regular Processing time in my Country:</strong><?=$row_user['passport_weeks']?> Weeks</p>
					<p><strong>Travel Visa Available For:</strong></p>
					<table width="100%" border="1" style="border-collapse:collapse;" id="inbox">
				        	<tr>
				        		<th class="tleft">Country</th>
				        		<th class="tleft">From Date</th>
				        		<th class="tleft">To Date</th>
				        	</tr>
				        <? 	$rs_msg = mysqli_query($conn,"select * from travel_visas where user_id = '".$row_user['id']."' order by to_date desc"); 
				        	if(mysqli_num_rows($rs_msg) == 0){
				        		?>
				        		<?
				        	}else{
					       		while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
					       		<tr title="Click for detail">
					       			<td><?=get_combo('countries','name',$row_msg['country_id'],'','text')?></td>
					       			<td><?=date_converter($row_msg['from_date'])?></td>
					       			<td><?=date_converter($row_msg['to_date'])?></td>
					       		</tr>
					       	<?  }
					       	} ?>
				        </table>
				</div>
				
				
				<div id="personal-data" class="content1 max-width-grid-list">
					<? if(in_array('about_myself', $fields)){ ?>
					<p><strong>About Myself:</strong><?=$row['education']?></p>
					<? } ?>
					
					<p><strong>Education:</strong></p>
					<table width="100%" border="1" style="border-collapse:collapse;" id="inbox">
					<tr>
						<th class="tleft">Degree</th>
						<th class="tleft">Institute</th>
						<th class="tleft">Passing Year</th>

					</tr>
			        <? 	$rs_msg = mysqli_query($conn,"select e.* from educations e where e.user_id = '".$row_user['id']."' order by passing_year desc "); 
			        	if(mysqli_num_rows($rs_msg) == 0){
			        		?>
			        		
			        		<?
			        	}else{
				       		while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
				       		<tr title="Click for detail">
				       			<td><?=$row_msg['degree']?></td>
				       			<td><?=$row_msg['institute']?></td>
				       			<td><?=$row_msg['passing_year']?></td>
				       		</tr>
				       	<?  }
				       	} ?>
			        </table>
					
					
					<p><strong>Languages:</strong></p>
					<table width="100%" border="1" style="border-collapse:collapse;" id="inbox">
					<tr>
						<th class="tleft">Language</th>
						<th class="tleft">Speaking</th>
						<th class="tleft">Reading</th>
						<th class="tleft">Writing</th>
						<th class="tleft">Preference</th>
					</tr>
			        <? 	$rs_msg = mysqli_query($conn,"select e.* from languages e where e.user_id = '".$row_user['id']."' order by sort desc"); 
			        	if(mysqli_num_rows($rs_msg) == 0){
			        		?>
			        		<?
			        	}else{
				       		while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
				       		<tr title="Click for detail">
				       			<td><?=$row_msg['language']?></td>
				       			<td><?=($row_msg['speaking'] == '1')?'Yes':'No';?></td>
				       			<td><?=($row_msg['reading'] == '1')?'Yes':'No';?></td>
				       			<td><?=($row_msg['writing'] == '1')?'Yes':'No';?></td>
								<td><?=$row_msg['sort']?></td>
				       		</tr>
				       	<?  }
				       	} ?>
			        </table>
					
					
					<p><strong>Favorite Cricketers:</strong></p>
					<table width="100%" border="1" style="border-collapse:collapse;" id="inbox">
			        <? 	$rs_msg = mysqli_query($conn,"select e.* from favorite_cricketers e where e.user_id = '".$row_user['id']."' order by sort desc"); 
			        	if(mysqli_num_rows($rs_msg) == 0){
			        		?>
			        		
			        		<?
			        	}else{
				       		while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
									<tr> <th class="tleft"width="22%"></th>
						<th class="tleft">Name</th>
						
						<th class="tleft">Preference</th>
					</tr>
				       		<tr title="Click for detail">
				       			<td width="22%"> &nbsp;</td>
				       			<td ><?=$row_msg['name']?></td>
<td><?=$row_msg['sort']?></td>
				       		</tr>
				       	<?  }
				       	} ?>
			        </table>
					
					
					<p><strong>Favorite Cricket Merchandise Brand(s) :</strong></p>
					<table width="100%" border="1" style="border-collapse:collapse;" id="inbox">
			        <? 	$rs_msg = mysqli_query($conn,"select e.* from favorite_brands e where e.user_id = '".$row_user['id']."' order by sort desc"); 
			        	if(mysqli_num_rows($rs_msg) == 0){
			        		?>
			        		
			        		<?
			        	}else{
				       		while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
							<tr>
						<th class="tleft"width="22%"></th>
<th class="tleft">Name</th>
						
						<th class="tleft">Preference</th>
					</tr>
				       		<tr title="Click for detail">
				       			<td width="22%"> &nbsp;</td>
				       			<td><?=$row_msg['name']?></td>
<td><?=$row_msg['sort']?></td>
				       		</tr>
				       	<?  }
				       	} ?>
			        </table>
					
				</div>
				
				<div id="contact" class="content1" style="width:50%;">
					<br>
					<? include('common/login-box.php');?>
			<? if(isset($_SESSION['ycdc_user_name']) && !empty($_SESSION['ycdc_user_name'])){ ?>
				<?php $isContact = 1; ?>
				<?php if(in_array('only_friends_can_contact', $fields_privacy)){ ?>
					<?php $friends = mysqli_fetch_assoc(mysqli_query($conn,"select * from friendship where (from_user_id = '".$_SESSION['ycdc_dbuid']."' or to_user_id = '".$_SESSION['ycdc_dbuid']."') and (from_user_id = '".$row['id']."' or to_user_id = '".$row['id']."')  and status = '1'")); ?>
					<?php if(!$friends){ ?>
						<?php $isContact = 0; ?>
					<?php } ?>
				<?php } ?>
				<?php if($isContact == 1){ ?>
				    <form method="post" action="">
				    	<input type="hidden" name="action" value="message">
						<fieldset>
							<div class="form-box">
								<div style="padding:15px 15px 0px 15px;">
									To : <?=get_combo('users','f_name',$_GET['id'],'','text')." "?><?=get_combo('users','last_name',$_GET['id'],'','text')?>
								</div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input style="width:500px; margin-left:600px;" type="text" name="subject" placeholder="Subject" class="input-login <?=(!isset($_SESSION['ycdc_user_name']) || empty($_SESSION['ycdc_user_name']))?'':'validate[required]'?>">
								
								<textarea placeholder="Message here" name="comments" style="width:500px; margin-top:10px;" class="<?=(!isset($_SESSION['ycdc_user_name']) || empty($_SESSION['ycdc_user_name']))?'':'validate[required]'?>"></textarea>
							</div>
							<div class="clear"></div>
							
							<div class="form-box">
								<input type="submit" value="Send"  class="submit-login">
							</div>
						</fieldset>
					</form>
				<?php }else{ ?>
				<div id="error">This user have privacy setting on contact ...!</div>
				<?php } ?>
			<? }else{ ?>
				<div id="error">Please login first for post your reply ... !</div>
			<? } ?>
				</div>
				<div class="clear"></div>
			</div>
		<? } ?>
		</div>
		<div class="clear"></div>
	</div>
<script src="<?=WWW?>js/jquery-1.8.0.js" type="text/javascript"></script>
<script src="<?=WWW?>js/jquery.lightbox-0.5.js" type="text/javascript"></script>
<script src="<?=WWW?>js/jquery.lightbox-0.5.min.js" type="text/javascript"></script>
<script src="<?=WWW?>js/jquery.lightbox-0.5.pack.js" type="text/javascript"></script>
<script src="<?=WWW?>js/smoothscroll.js" type="text/javascript"></script>
<script src="<?=WWW?>js/jquery.tabify.js" type="text/javascript" charset="utf-8"></script>
<link href="<?=WWW?>css/jquery.lightbox-0.5.css" type="text/css" rel="stylesheet">
<script src="<?php echo WWW; ?>js/jquery-ui.min.js"></script>
<script>
$(document).ready(function(){
	$('#menu6_1').tabify();
	$('#gallery a.photo').lightBox();

	$('a.love-it').click(function(){
		var id = this.id;
		$.ajax({type	: 'GET', 
		   	url			: WWW + 'includes/put-love-it.php?aid=' + id, 
			success		: function(msg){
				if(msg != 'false'){
					$('a.love-it#' + id).html('<img src="<?=WWW?>images/icons/love-it.png">');
					$('a.love-it-counter#' + id).html('(' + msg + ')');
				}
			}
    	});
	});
	$('a.share-it').click(function(){
		var id = this.id;
		$.ajax({type	: 'GET', 
		   	url			: WWW + 'includes/put-share-it.php?aid=' + id, 
			success		: function(msg){
				if(msg != 'false'){
					$('a.share-it-counter#' + id).html('(' + msg + ')');
				}
			}
    	});
	});

	$('a#contact-btn').click(function(){
		if($(this).attr('href') == '#contact-tab'){
			$(document).scrollTop($(document).height());
		}
	});
	$('.close-popup').click(function(){
		$('.popup').addClass('hide');
	});
	$('a.popup-link').live('click',function(){
		var link = $(this).attr('href');
		if(link == '#edit-activity'){
			var id = this.id;
			$.ajax({type	: 'POST', 
				url		: WWW + 'includes/get-record.php', 
				data	: ({table:'activity',field:'activity',id:id}),
				success	: function(msg){
					$(link+' textarea').val(msg);
					$(link+' #recordId').val(id);
				}
			});
		}else if(link == '#activity-likes'){
			var id = this.id;
			$.ajax({type	: 'POST', 
				url		: WWW + 'includes/get-activity-info.php', 
				data	: ({type:'loves',id:id}),
				success	: function(msg){
					$('#lovers').html(msg);
				}
			});
		}else if(link == '#activity-shares'){
			var id = this.id;
			$.ajax({type	: 'POST', 
				url		: WWW + 'includes/get-activity-info.php', 
				data	: ({type:'shares',id:id}),
				success	: function(msg){
					$('#sharers').html(msg);
				}
			});
		}else if(link == '#activity-comments'){
			var id = this.id;
			$.ajax({type	: 'POST', 
				url		: WWW + 'includes/get-activity-info.php', 
				data	: ({type:'comments',id:id}),
				success	: function(msg){
					$('#comments').html(msg);
				}
			});
			$('input[name=activity_id]').val(id);
		}
		var height = $(window).scrollTop();
		$(link).css('top',height-150);

		$(link).removeClass('hide');
	});
	$('a.popup-link').click(function(){
		var link = $(this).attr('href');
		if(link == '#edit-activity'){
			var id = this.id;
			$.ajax({type	: 'POST', 
				url		: WWW + 'includes/get-record.php', 
				data	: ({table:'activity',field:'activity',id:id}),
				success	: function(msg){
					$(link+' textarea').val(msg);
					$(link+' #recordId').val(id);
				}
		});
		}
		$(link).removeClass('hide');
	});
	
	<?php if($user_data['user_type_id'] == 2){ ?>
	displayPlayerStatistics = function(){
		var data = "player_id=<?php echo $user_data['id'] ?>&league_id="+$("#league_select_stats").val()+"&year="+$("#year_select_stats").val()+"&game_type="+$("#game_types_select_stats").val()+"&overs_type="+$("#overs_type_select_stats").val()+"&ball_type="+$("#ball_type_select_stats").val()+"&tour_id="+$("#tournament_select_stats").val();;
		$.ajax({
			url: "<?php echo WWW; ?>player-statistics-ajax.php",
			type:"POST",
			data:data,
			success: function(data) {
				$("#player-statistics-data").html(data);
			}
		});
	}
	
	$("#player_stats_search").bind('click',function(){
		displayPlayerStatistics();
	});
	$(".player-stats-link").bind('click',function(){
		displayPlayerStatistics();
	});
	
	displayPlayerStatistics();
	<?php } ?>
	
});	
</script>
<div id="activity-comments" class="popup hide">
	<h1>
		Activity Comments
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h1>
	<div id="comments"></div>
	<div id="new-comment">
		<form method="post">
			<input type="hidden" name="activity_id" value="">
			<input type="hidden" name="action" value="comment">
			<textarea cols="30" rows="2" name="comments"></textarea>
			<input type="submit" class="submit-login" value="Post">
		</form>
	</div>
</div>
<div id="activity-shares" class="popup hide">
	<h1>
		Your Activity Shared
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h1>
	<div id="sharers"></div>
</div>
<div id="activity-likes" class="popup hide">
	<h1>
		Your Activity Lovers
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h1>
	<div id="lovers"></div>
</div>
<div id="edit-activity" class="popup hide">
	<h1>
		Update your activity
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h1>
	<form method="post" action="">
		<input type="hidden" name="action" value="activity-update">
		<input type="hidden" name="recordId" id="recordId" value="">
		<fieldset>
			<div class="form-box">
				<textarea style="width:300px;" name="activity" class="validate[required]" placeholder="Type your activity here"></textarea>
				<input type="submit" class="submit-login margin-top-5" value="Save">
			</div>
		</fieldset>
	</form>
</div>
<? include('common/footer.php'); ?>