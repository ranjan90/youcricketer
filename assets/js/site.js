$(document).ready(function(){
	/*if($(window).width() < '768' && $(window).width() > '320'){
		$('.content').css('width', '450px');
	}

	$('a#checkAll').click(function(){
		$('form#multiple input[type=checkbox]').attr('checked', 'checked');
	});
	$('a#uncheckAll').click(function(){
		$('form#multiple input[type=checkbox]').removeAttr('checked', 'checked');
	});
	
	if($('#menu').length){
		$('#menu').tabify();
	}
	if($('#menu2').length){
		$('#menu2').tabify();
	}
	if($('#menu3').length){
		$('#menu3').tabify();
	}
	if($('#menu4').length){
		$('#menu4').tabify();
	}
	if($('#menu5').length){
		$('#menu5').tabify();
	}
	if($('#menu6').length){
		$('#menu6').tabify();
	}
	if($('#menu7').length){
		$('#menu7').tabify();
	}*/
	
	/*$('#menu2').tabify();
	$('#menu3').tabify();
	$('#menu4').tabify();
	$('#menu5').tabify();
	$('#menu6').tabify();
	$('#menu7').tabify();*/

	$('textarea').keyup(function(){
		var content = $(this).val();
		var remain  = 1000 - content.length;
		$("#left").html(remain);
//		$(this).after('<p class="red">Character left : '+ remain +'</p>');
	});

	$('.close-popup').on('click',function(){
		var id = this.parentNode.parentNode.id;
		if(id){
			$('#' + id).addClass('hide');	
		}else{
			$('.popup').addClass('hide');
		}
	});

	$('a.popup-link').on('click',function(){
		var link = $(this).attr('href');
		if(link == '#edit-activity'){
			var id = this.id;
			$.ajax({type	: 'POST', 
				url		: WWW + 'includes/get-record.php', 
				data	: ({table:'activity',field:'activity',id:id}),
				success	: function(msg){
					$(link+' textarea').val(msg);
					$(link+' #recordId').val(id);
				}
			});
		}

		//var height = $(window).scrollTop();
		//$(link).css('top',height-150);

		$(link).removeClass('hide');
	});

	$('a.register-popup-link').click(function(){
		$('#login.popup').addClass('hide');
		$('#register.popup').removeClass('hide');
	})

	$('input[name=photo]').attr('accept','image/*');
	$('input[name=video]').attr('accept','video/*');
	
	//$('#pagination-top').html($('#pagination-bottom').html());

	$('select#pagination-combo').change(function(){
		var x = this.value;
		var page = $('input[name=pagination-page]').val();
		window.location = WWW + page+'?page='+x;
	});

	//$('form').validationEngine();

	$('#dashboard .black-box h2').click(function(){
		$('#dashboard .black-box ul').css('display','none');
		var id = this.id;
		$('#'+id+'-box ul').css('display','block');
	});

	
	$('input[name=password]').change(function(){
		if($('input[name=password]').val().length == 0){
			$('#password-result').html('Weak Password');
		}		
	})

	/*if($('input[name=password]').val().length == 0){
		$('#password-result').html('Enter Password');
	}*/

	$('input[name=password]').blur(function(){
		var password = this.value;
		var score = 0;

		if (password.length >= 8)
	        score++;
	    if (password.length >= 10)
	        score++;
	    if (password.match(/^.*(?=.{8,})(?=.*[a-z])(?=.*[A-Z]).*$/))
	        score++;

	    if(score == '3'){
	    	$('#password-result').html('Strong Password');
	    	$('#password-result').css('background','green');
	    }else if(score == '2'){
	    	$('#password-result').html('Not Strong Password');
	    	$('#password-result').css('background','orange');
	    }
	});

	

	$('input[name=photo], .valid-aimage').change(function(){
		var val = this.value;
		var filesize = Math.round((this.files[0].size)/1024,2);
		if(filesize > 10000){
			$(this).val('');
            alert("Image is too large ... !");
		}
	});
	
	$('.valid-image').change(function(){
		var val = this.value;
		
		var type=this.files[0].type;
		
		var filesize = Math.round((this.files[0].size)/1024,2);
		
		if(type=='image/jpeg' || type=='image/png' || type=='image/jpg' ||  type=='image/gif'){
			if(filesize > 10000){
				$(this).val('');
				alert("Image is too large ... !");
			}	
		}else{
			if(filesize > 20000){
				$(this).val('');
				alert("File is too large ... !");
			}	
		}
		
	});
	
	
	
	$('input[name=video_file], input[name=videos]').change(function(){
		var val = this.value;
		var filesize = Math.round((this.files[0].size)/1024,2);
		if(filesize > 20000){
			$(this).val('');
            alert("Video is too large ... !");
		}
	});
	
	

	
	if($('.editor').length){
		$('.editor').kendoEditor();
	}
});
function popup(url) {
	window.open(url,'Social Login','width=500,height=500');
	return false;
 }