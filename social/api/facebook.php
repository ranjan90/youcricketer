<?php
ob_start();
session_start();
require_once('config.php');
require_once('code/facebook/facebook.php');
require_once('../../includes/path.php');

$facebook = new Facebook(array(
            'appId' => APP_ID,
            'secret' => APP_SECRET,
			'cookie'=>true
            ));

$user = $facebook->getUser();

$params = array(
    "redirect_uri" => WWW.'social/api/facebook.php',
		'scope' => 'email,read_stream,publish_stream,user_photos,user_birthday',
		'fbconnect' =>  1/*,
		"scope" => "email,read_stream,publish_stream,user_photos"*/
);

	
if ($user)
{
	$accessToken = $_GET['code'];
  try
  {
	if(!empty($accessToken))
	{
		$user_profile = $facebook->api('/me');
	}else
	{
		header('Location:'.$facebook->getLoginUrl($params)); 
	}
  }
  catch (FacebookApiException $e)
  {
    error_log($e);
    $user = null;
  }
  
	if (!empty($user_profile ))
	{
		require_once('../../includes/connection.php');
		require_once('../../includes/functions.php');

		$res 		= mysqli_query($conn,"SELECT id FROM social_users WHERE social_id = '".$user_profile['id']."' AND type = 'Facebook'");
		if(mysqli_num_rows($res) == 0)
		{
			if(!empty($user_profile['email'])){
				$sql 	= "INSERT INTO social_users (social_id, type, name, email, screen_name) VALUES (
				'".$user_profile['id']."',
				'Facebook',
				'".$user_profile['name']."',
				'".$user_profile['email']."',
				'".$user_profile['username']."'
				)";
				mysqli_query($conn,$sql);
				$id = mysqli_insert_id();
				if(isset($_SESSION['ref_invitation']) && !empty($_SESSION['ref_invitation'])){
					$ref_row	= mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM invitations WHERE id  ='".$_SESSION['ref_invitation']."'"));
					$from_user_id=$ref_row['from_user_id'];
					mysqli_query($conn,"update invitations set accept_status = '1', status = 'Invitation Accepted' where id ='".$_SESSION['ref_invitation']."'");
					mysqli_query($conn,"insert into friendship (from_user_id, to_user_id, create_date, status) values ('$from_user_id','$id','$date', '0'); ");
				}
				$_SESSION['social_api_id'] 		= $id;
				$_SESSION['social_display_name']= $user_profile['name'];
				$_SESSION['social_type'] 		= "Facebook";
				
				$rsUsers = mysqli_query($conn,"SELECT * FROM users WHERE email = '".$user_profile['email']."'");
				if(mysqli_num_rows($rsUsers) == 0){
					if(strpos(':', $user_profile['timezone'])){
						if(strlen($user_profile['timezone']) == 5){
							$timezone = $user_profile['timezone'];
						}else{
							$timezone = '0'.$user_profile['timezone'];
						}
					}else{
						$timezone = '0'.$user_profile['timezone'].':00';
					}
					if($user_profile['gender'] == 'male'){
						$gender = '1';
					}else{
						$gender = '0';
					}
					$rowTimezone = mysqli_fetch_assoc(mysqli_query($conn,"select * from timezones where time_difference = '+".$timezone."'"));
					$query 	= "insert into users (is_company, f_name, last_name, gender, user_type_id, email, status, create_date, timezone) 
					values ('0','".$user_profile['first_name']."','".$user_profile['last_name']."','".$gender."','18','".$user_profile['email']."','1','".date('Y-m-d H:i:s')."','".$rowTimezone['id']."');";
					mysqli_query($conn,$query);
					$userId = mysqli_insert_id();
					$_SESSION['ycdc_dbuid']		= $userId;
					$_SESSION['ycdc_user_email']= $user_profile['email'];
					$_SESSION['ycdc_user_name'] = $user_profile['first_name'].' '.$user_profile['last_name'];
					$_SESSION['ycdc_desig_id'] 	= '18';
					$_SESSION['timezone']       = $rowTimezone['id'];
					?>
					<script type="text/javascript">
						window.opener.location.reload(false);
						window.close();
					</script>
					<?php
				}else{
					$rowUser = mysqli_fetch_assoc($rsUsers);
					// session
					$_SESSION['ycdc_dbuid']		= $rowUser['id'];
					$_SESSION['ycdc_user_email']= $rowUser['email'];
					$_SESSION['ycdc_user_name'] = $rowUser['f_name'].' '.$row_user['last_name'];
					$_SESSION['ycdc_desig_id'] 	= $rowUser['user_type_id'];
					$_SESSION['timezone']       = $rowUser['timezone'];
					?>
						<script type="text/javascript">
							window.opener.location.reload(false);
							window.close();
						</script>
						<?php
				}
			}
		}else{
			// here typing users checking code and then session and redirect.
			$rsUsers = mysqli_query($conn,"SELECT * FROM users WHERE email = '".$user_profile['email']."'");
			if(mysqli_num_rows($rsUsers) == 0){
				if(strpos(':', $user_profile['timezone'])){
					if(strlen($user_profile['timezone']) == 5){
						$timezone = $user_profile['timezone'];
					}else{
						$timezone = '0'.$user_profile['timezone'];
					}
				}else{
					$timezone = '0'.$user_profile['timezone'].':00';
				}
				if($user_profile['gender'] == 'male'){
					$gender = '1';
				}else{
					$gender = '0';
				}
				$rowTimezone = mysqli_fetch_assoc(mysqli_query($conn,"select * from timezones where time_difference = '+".$timezone."'"));
				$query 	= "insert into users (is_company, f_name, last_name, gender, user_type_id, email, status, create_date, timezone) 
				values ('0','".$user_profile['first_name']."','".$user_profile['last_name']."','".$gender."','18','".$user_profile['email']."','1','".date('Y-m-d H:i:s')."','".$rowTimezone['id']."');";
				mysqli_query($conn,$query);
				$userId = mysqli_insert_id();
				$_SESSION['ycdc_dbuid']		= $userId;
				$_SESSION['ycdc_user_email']= $user_profile['email'];
				$_SESSION['ycdc_user_name'] = $user_profile['first_name'].' '.$user_profile['last_name'];
				$_SESSION['ycdc_desig_id'] 	= '18';
				$_SESSION['timezone']       = $rowTimezone['id'];
				?>
				<script type="text/javascript">
					window.opener.location.reload(false);
					window.close();
				</script>
				<?php
			}else{
				$rowUser = mysqli_fetch_assoc($rsUsers);
				// session
				$_SESSION['ycdc_dbuid']		= $rowUser['id'];
				$_SESSION['ycdc_user_email']= $rowUser['email'];
				$_SESSION['ycdc_user_name'] = $rowUser['f_name'].' '.$row_user['last_name'];
				$_SESSION['ycdc_desig_id'] 	= $rowUser['user_type_id'];
				$_SESSION['timezone']       = $rowUser['timezone'];
				session_start();
				?>
					<script type="text/javascript">
						window.opener.location.reload(false);
						window.close();
					</script>
					<?php
			}
		}

		$select = "SELECT * FROM social_users WHERE social_id = '".$user_profile['id']."' AND type = 'Facebook' LIMIT 1 ";
		$res = mysqli_query($conn,$select);
		$obj = mysqli_fetch_object($res);			
		
		$_SESSION['ycdc_dbuid'] 		= $obj->user_id;
		$_SESSION['ycdc_user_name'] 	= $obj->name;
		$_SESSION['ycdc_user_email'] 	= $obj->email;
		$_SESSION['loggedInFROM'] 		= 'Facebook';
		?>
		<script type="text/javascript">
			window.opener.location.reload(false);
			window.close();
		</script>
		<?php
	}
	else 
	{
		include('../includes/path.php');
		include('../includes/connection.php');
		session_start();
		$user_id 		= $_SESSION['fft_user_id'] ;
		mysqli_query($conn,"DELETE FROM online_sessions WHERE user_id='$user_id'");
		$_SESSION = array();
		session_destroy();
		if (isset($_SERVER['HTTP_COOKIE'])) {
		$cookies = explode(';', $_SERVER['HTTP_COOKIE']);
		foreach($cookies as $cookie) {
			$parts = explode('=', $cookie);
			$name = trim($parts[0]);
			setcookie($name, '', time()-1000);
			setcookie($name, '', time()-1000, '/');
		}
	}
		//header('location:http://www.profileindex.com');
		header('Location:'.$facebook->getLoginUrl($params)); 
        # For testing purposes, if there was an error, let's kill the script
        //die("Connection error please try again <a href='logout.php'>Click here</a>");
    }
} else {
    # There's no active session, let's generate one
	$login_url = $facebook->getLoginUrl(array( 'scope' => 'email'));
    header("Location:" . $login_url);
}
?>
