<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();
require_once('/home/profilei/public_html/social/api/config.php');
require_once('code/twitter/twitteroauth.php');
$connection = new TwitterOAuth(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET);
$request_token = $connection->getRequestToken(TWITTER_OAUTH_CALLBACK);
$_SESSION['oauth_token'] = $token = $request_token['oauth_token'];
$_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];
switch ($connection->http_code) {
  case 200:    
    $url = $connection->getAuthorizeURL($token);
    header('Location: ' . $url); 
    break;
  default:    
    echo 'Could not connect to Twitter. Refresh the page or try again later.';
}
?>