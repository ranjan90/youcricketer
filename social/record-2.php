<?php
ob_start();
session_start();
/*if(isset($_SESSION['social_api_id']) && $_SESSION['social_api_id'] != '' && isset($_SESSION['social_display_name']) && $_SESSION['social_display_name'] != '') {*/
	require_once('/home/profilei/public_html/includes/connection.php');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title></title>
		<script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
		<script type="text/javascript" src="/js/bday-picker.js"></script>
	</head>
<body> 
	<form method="post" id="frmgPlus" action="/social/frm.php">
		<input type="hidden" name="id" value="<?php echo $_SESSION['social_api_id'];?>" />
		<input type="hidden" name="type" value="<?php echo $_SESSION['type'];?>" />
		<div id="error"></div>
		<table width="98%" cellpadding="0" cellspacing="0">
			<tr>
				<td>Name:</td>
				<td><input type="text" name="name" id="name" value="<?php echo $_SESSION['social_display_name'];?>" /></td>
			</tr>
			<tr>
				<td>Email:</td>
				<td><input type="text" name="email" id="email" value="" /></td>
			</tr>			
			<tr>
				<td>Date of Birth:</td>
				<td><div class="picker" id="picker2"></div></td>
			</tr>
			<tr>
				<td>Country:</td>
				<td>
					<?php
					$sql = "SELECT id, name FROM countries";
					$result = mysqli_query($conn,$sql);								
					?>
					<select id="country" name="country">
						<option>Country</option>
						<?php
						while ($country = mysqli_fetch_object($result)) {
							echo '<option value="'.$country->id.'">'.$country->name.'</option>';
						}
						?>
					</select>
				</td>
			</tr>
			<tr id="changeData" style="display:none;">
				<td colspan="2">Do you want change fields?&nbsp; Yes: <input type="radio" id="change_data_1" name="change_data" value="1" /> No: <input type="radio" id="change_data_2" name="change_data" value="2" /></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" name="submit" value="Add Field" id="submit" /></td>
			</tr>
		</table>
	</form>
	<script type="text/javascript">
	$(document).ready(function(){
		$('#frmgPlus #submit').click(function() {
			var change = 0;
			if($(this).attr('value') == 'Re: Add Field') {
				change = $('input[type=radio]:checked').val();				
			}			
			$.post("/social/frm.php?change="+change, $("#frmgPlus").serialize(), function(data) {
					var obj = jQuery.parseJSON(data);
					if(obj.response == 'success') {
						window.opener.location.reload(false);
						window.close();
					} else if(obj.response == 'record_exist') {
						$('#frmgPlus #submit').attr('value', 'Re: Add Field');
						$('#frmgPlus #changeData').show();
					} else if (obj.response == 'error') {
						$.each(obj.msg, function(key, val) {
							$("#error").append(val);
						});
					}
			});
			return false;
		});
		$("#picker2").birthdaypicker({
		  futureDates: true,
		  maxYear: 2000,
		  maxAge: 75,
		  defaultDate: "10-17-1980"
		});
	});
	</script>
</body>
</html>
<?php
//}
?>