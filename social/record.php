<?php
ob_start();
session_start();
if(isset($_SESSION['social_api_id']) && $_SESSION['social_api_id'] != '' && isset($_SESSION['social_display_name']) && $_SESSION['social_display_name'] != '') {
	require_once('/home/profilei/public_html/includes/connection.php');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title></title>
		<script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
		<script type="text/javascript" src="/js/bday-picker.js"></script>
		<script type="text/javascript" src="/js/jquery-ui.min.js" /></script>
		<link href="/css/jquery-ui.css" type="text/css" rel="stylesheet" />
<link type="text/css" rel="stylesheet" href="/css/style.css" />
		<link type="text/css" rel="stylesheet" href="/css/signup.css" />
<style>
		body{ width:auto;}
		#error{display:none;}
		#submit{background-color:#62990A; border-radius:5px; cursor:pointer;border:0;height:34px;width:87px;color:#FFFFFF;}
		.input{
			background: none repeat scroll 0 0 #F4F4F4;
			border: 0 solid;
			border-radius: 2px 2px 2px 2px;
			box-shadow: 0 0 3px rgba(0, 0, 0, 0.1) inset;
			font-family: Arial,Helvetica,sans-serif;
			font-size: 13px;
			font-weight: normal;
			height: 25px;
			margin-bottom: 10px;
			padding: 3px;
		}
		.select{
			background: none repeat scroll 0 0 #F4F4F4;
			border: 0 solid;
			border-radius: 2px 2px 2px 2px;
			box-shadow: 0 0 3px rgba(0, 0, 0, 0.1) inset;
			font-family: Arial,Helvetica,sans-serif;
			font-size: 13px;
			font-weight: normal;
			height: 30px;
			margin-bottom: 10px;
			padding: 4px;
		}
		</style>
		<script type="text/javascript">

$(function(){
    $.ajax({type	: 'POST', 
		   	url		: '/includes/get-categories.php?type=professional', 
			dataType: 'json',
			success	: function(msg){
				$('#p_category_id').autocomplete({source: msg});
			}
    });
	
	$.ajax({type	: 'POST', 
		   	url		: '/includes/get-categories.php?type=company', 
			dataType: 'json',
			success	: function(msg){
				$('#c_category_id').autocomplete({source: msg});
			}
    });
});

$(document).ready(function(){
	$("form#frm-add").validationEngine();
	$('input[type=radio]').change(function(){
		if($(this).val() == '3'){
			$('input#c_category_id').fadeOut(0);
			$('input#p_category_id').fadeIn(500);
		}else{
			$('input#p_category_id').fadeOut(0);
			$('input#c_category_id').fadeIn(500);
		}
	});
});
</script>
<style>
.ui-autocomplete{margin: 310px 535px; min-height:200px; z-index:10000;}
</style>
	</head>
<body> 
<div class="top-navigation"></div>
<div style="margin-top:20px;margin-left:30px;">	
	<form method="post" id="frmgPlus" action="/social/frm.php">
		<input type="hidden" name="id" value="<?php echo $_SESSION['social_api_id'];?>" />
<input type="hidden" name="type" value="<?php echo $_SESSION['social_type'];?>" />
		<!--<input type="hidden" name="type" value="gPlus" />-->
		<div id="error"></div>
		<table width="98%" cellpadding="0" cellspacing="0">
			<tr>
				<td class="label">Name:</td>
				<td><input class="input" type="text" name="name" id="name" value="<?php echo $_SESSION['social_display_name'];?>" /></td>
			</tr>
			<tr>
				<td class="label">Email:</td>
				<td><input class="input" type="text" name="email" id="email" value="" /></td>
			</tr>			
			<tr>
				<td class="label">Date of Birth:</td>
				<td><div class="picker" id="picker2"></div></td>
			</tr>
			<tr>
				<td class="label">Profile Type : </td>
				<td>
					<input type="radio" name="type" id="type" value="2"/> &nbsp; Company Profile<br>
					<input type="radio" name="type" id="type" value="3" /> &nbsp; Personal Profile
				</td>
			</tr>
			<tr>
				<td class="label">Profile Feild</td>
				<td>
				<p id="pdepartments">
	                <input type="text" name="c_category_id" id="c_category_id" class="validate[required]" />
	                <input type="text" name="p_category_id" style="display:none;" id="p_category_id" class="validate[required]" />
				</p>
				</td>
			</tr>
			<tr>
				<td class="label">Country:</td>
				<td>
					<select name="country_id" id="country_id" class="validate[required]">
                <option selected="selected" value="">Select One</option>
                <?	$rs_countries = mysqli_query($conn,"select * from countries order by name ");
					while($row_country = mysqli_fetch_assoc($rs_countries)){ ?>
                    <option value="<?=$row_country['id']?>"><?=$row_country['name']?></option>
                 <? } ?>
                </select>
				</td>
			</tr>
			<tr id="changeData" style="display:none;">
				<td colspan="2">Do you want change fields?&nbsp; Yes: <input type="radio" id="change_data_1" name="change_data" value="1" /> No: <input type="radio" id="change_data_2" name="change_data" value="2" /></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" name="submit" value="Add Field" id="submit" /></td>
			</tr>
		</table>
	</form>
</div>
<div class="footer">
		<div class="copyright">
                	<p> &copy; 2013 Profile index  Intellectual Property LLC. All rights reserved. </p>
                </div>
	</div>
	<script type="text/javascript">
	$(document).ready(function(){
		$('#frmgPlus #submit').click(function() {
			var change = 0;
			if($(this).attr('value') == 'Re: Add Field') {
				change = $('input[type=radio]:checked').val();				
			}			
			$.post("/social/frm.php?change="+change, $("#frmgPlus").serialize(), function(data) {
					var obj = jQuery.parseJSON(data);
					if(obj.response == 'success') {
						window.opener.location.reload(false);
						window.close();
					} else if(obj.response == 'record_exist') {
						$('#frmgPlus #submit').attr('value', 'Re: Add Field');
						$('#frmgPlus #changeData').show();
					} else if (obj.response == 'error') {
						$.each(obj.msg, function(key, val) {
							$("#error").append(val);
						});
					}
			});
			return false;
		});
		$("#picker2").birthdaypicker({
		  futureDates: true,
		  maxYear: 2000,
		  maxAge: 75,
		  defaultDate: "10-17-1980"
		});
	});
	</script>
</body>
</html>
<?php
}
?>