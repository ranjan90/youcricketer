<?php include('common/header.php'); ?>

<div class="container-fluid">
    <div class="row">
		<div class="col-md-12">
			<h1> Fixtures </h1>
        </div>
    </div>
	<div class="row">
        <div class="col-md-10">
          <div class="white-box">
		  
	    <div class="row">
              <div class="col-sm-12">
                <div class="list">
                  <ul>
				    <?php include('espn/espn_fixtures.php');?>
                   
                  </ul>
                </div>
              </div>
        </div>
		</div>
        </div>
        <div class="col-md-2">
         
          <div class="row">
            <div class="col-sm-12">
				<?php include('common/right-panel.php');?>
            </div>
          </div>
        </div>
      </div>
      
</div><!-- /.container -->
<?php include('common/footer.php'); ?>