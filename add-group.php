<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	
		<? 	if(isset($_POST) && !empty($_POST)){ 
				$title 	= addslashes($_POST['title']);
				$content= addslashes($_POST['content']);
				$date 		= date('Y-m-d H:i:s');
				$user_id 	= $_SESSION['ycdc_dbuid'];
				$admin_id 	= $user_id;
				$isGlobal = ($_POST['is_global'] == 'on')?'1':'0';
				if(!isset($user_id) || empty($user_id)){
					echo '<div id="error" class="alert alert-danger">Please login first ... !</div>';
				}else{
					if(!empty($title) && !empty($content)){
						$rs_check = mysqli_query($conn,"select * from groups where user_id = '$user_id' and title = '$title'");
						if(mysqli_num_rows($rs_check) == 0){
							$query = "insert into groups (title, description, user_id, admin_id, status, create_date, is_global) 
							values ('$title','$content','$user_id','$admin_id','1','$date','$isGlobal')";

							if(mysqli_query($conn,$query)){
								$group_id = mysqli_insert_id($conn);
							mysqli_query($conn,"insert into users_to_groups (user_from_id, group_id, status ,invitation_to) values ('$user_id','$group_id','1','$user_id');");

								if(!is_dir('groups/'.$group_id)){
									mkdir('groups/'.$group_id,0777);
								}
								chmod('groups/'.$group_id,0777);

								if(!empty($_FILES['photo']['name'])){
									$filename 	= friendlyURL($title).'.jpg';
									$image 		= new SimpleImage();
									$image->load($_FILES["photo"]["tmp_name"]);
									$image->save('groups/'.$group_id.'/'.$filename);
									chmod('groups/'.$group_id.'/'.$filename,0777);
								
									mysqli_query($conn,"insert into photos (file_name, title, entity_id, entity_type) values ('$filename','$title','$group_id','groups')");
								}
							}

			                echo '<div id="success" class="alert alert-success">Groups Information added successfully ... !</div>';
			                ?>
			                <script>
			                window.location = '<?=WWW?>my-groups.html';
			                </script>
			                <?
						}else{
							echo '<div id="error" class="alert alert-danger">Information already exists ... !</div>';
						}
					}else{
						echo '<div id="error" class="alert alert-danger">Please fill all fields ... !</div>';
					}
				}
				
			}
		?>
		

<script src="<?=WWW;?>assets/js/ckeditor/ckeditor.js"></script>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1> Add Group </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <form method="post" action="" enctype="multipart/form-data" class="form-horizontal" id="form-add-forum">
            
            <div class="white-box">
              <div class="form-group">
                <div class="col-sm-12 text-center">
                  <h2>Group Information</h2>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Title</label>
                <div class="col-sm-9">
                  <input name="title" value="" class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Is Global</label>
                <div class="col-sm-9">
                  <input type="checkbox" name="is_global" <?php echo ($row['is_global'] == '1')?'checked="checked"':'';?>>
                  <span class="help-block">Group will be Regional Group If checked</span>
                </div>
              </div>
              
              
              <div class="form-group">
                <label class="col-sm-3 control-label">Content</label>
                <div class="col-sm-9">
                  <div id="editor">
					<textarea name="content" id="content"  class="ckeditor" class="validate[required]"></textarea>
				  </div>
                </div>
              </div>
              
              <div class="form-group">
                <label class="col-sm-3 control-label">Group Photo</label>
                <div class="col-sm-9">
                  <input accept="image/*" name="photo" type="file">
                  <span class="help-block">Max Photo size : 2MB</span>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12 text-center">
                  <input value=" Save " class="btn orange hvr-float-shadow" type="submit">
                  <a id="cancel" class="btn blue hvr-float-shadow" href="<?=WWW?>groups.html">Cancel</a>
                </div>
              </div>
            </div>
		  </form>
        </div>
        
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
              <?php include('common/right-panel.php');?>
            </div>
          </div>
          
        </div>
      </div>
    </div><!-- /.container -->
	
<script type="text/javascript">
$(document).ready(function(){
	$('#form-add-forum').validationEngine();
});
</script>	

<?php include('common/footer.php'); ?>