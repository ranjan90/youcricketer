<?	include('configuration.php');
	$query	 	= $_POST['query'];
	$type	 	= $_POST['type'];
	$offset	 	= $_POST['offset'];
	
	$query		= "$query limit $offset,1";

	if($type == 'individuals'){
		$row = mysqli_fetch_assoc(mysqli_query($conn,$query));

		if($row){
			if($row_img && !empty($row_img['file_name'])){
				$photo = 'users/'.$row['id'].'/photos/'.$row_img['file_name'];
			}else{
				$photo = 'images/no-photo.jpg';
			}
			if($row['first_class_player'] == '1'){
				$firstClassPlayer = 'First Class Player';
			}else{
				$firstClassPlayer = '';
			}
			$video = $row_vid['file_name'];
			if(!empty($video)){
				if(preg_match('/<iframe(.*)><\/iframe>/', $video)){
					if(!preg_match('/height="(.*)"/', $video)){
						$video = preg_replace('/height="(.*)"/','height="130"',$video);	
					}else{
						$video = preg_replace('/width="(.*)"/','width="168" height="130"',$video);	
					}
				}else{
					$filename = explode('.',$video);
					$filename1= $filename[0];
					$video = '<video width="168" height="130" controls>
								  <source src="'.WWW.'videos/'.$row['id'].'/'.$filename1.'.mp4" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
								  <source src="'.WWW.'videos/'.$row['id'].'/'.$filename1.'.ogg" type=\'video/ogg; codecs="theora, vorbis"\'>
								  <source src="'.WWW.'videos/'.$row['id'].'/'.$filename1.'.mov" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
								  <source src="'.WWW.'videos/'.$row['id'].'/'.$filename1.'.webm" type=\'video/webm; codecs="vp8, vorbis"\'>
							</video>';
				}
			}else{
				$video = '<img src="'.WWW.'images/no-video.jpg" width="168" height="130">';
			}
			$rowRole = get_record_on_id('user_types', $row['user_type_id']);
			$row_country= get_record_on_id('countries', $row['country_id']);
			$output = '<dl>
										<dt>
											<a href="'.WWW.'?>individual-detail-'.$row['id'].'-'.friendlyURL($row['f_name'].' '.$row['m_name'].' '.$row['last_name']).'.html" title="'.$row['f_name'].' '.$row['m_name'].' '.$row['last_name'].' - '.$site_title.'"><img src="'.WWW.$photo.'" width="120" height="128" /></a></dt>
										<dd><div class="details">
											<h3>'.$row['f_name'].' '.$row['last_name'].'</h3>
											<p>'.$location.'<br />
											'.$rowRole['name'].'<br />
											'.$firstClassPlayer.'
											'.$row['type'].'<br />
											</p>
											<a class="submit-login margin-top-5" href="'.WWW.'individual-detail-'.$row['id'].'-'.friendlyURL($row['f_name'].' '.$row['m_name'].' '.$row['last_name']).'.html" title="'.$row['f_name'].' '.$row['m_name'].' '.$row['last_name'].' - '.$site_title.'">View Profile</a></div>
											<div class="flag"><img title="'.$row_country['name'].'" alt="'.$row_country['name'].'" src="'.WWW.'countries/'.$row_country['flag'].'"  width="25"/></div>
											<div class="video">'.$video.'</div>
										</dd>
								</dl>';
		}else{
			$output = '';
		}
	}


	echo $output;

?>