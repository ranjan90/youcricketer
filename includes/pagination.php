<?php

/*************************************************************************

php easy :: pagination scripts set - Version One

==========================================================================

Author:      php easy code, www.phpeasycode.com

Web Site:    http://www.phpeasycode.com

Contact:     webmaster@phpeasycode.com

*************************************************************************/

function paginate_one($reload, $page, $tpages ) {

	

	$firstlabel = 'First';

	$prevlabel  = 'Prev';

	$nextlabel  = 'Next';

	$lastlabel  = 'Last';

	

	$out = "<div class=\"pagin\">\n";

	
	// first

	if($page>1) {

		$out.= "<a class=\"frist\" href=\"" . $reload . "page=1\">" . $firstlabel . "</a>\n";

	}

	else {

		$out.= "<span class=\"frist-disable\">" . $firstlabel . "</span>\n";

	}

	

	// previous

	if($page==1) {

		$out.= "<span class=\"prev-disable\">" . $prevlabel . "</span>\n";

	}

	else if($page==2) {

		$out.= "<a class=\"prev\" href=\"" . $reload . "\">" . $prevlabel . "</a>\n";

	}

	else {

		$out.= "<a class=\"prev\" href=\"" . $reload . "page=" . ($page-1) . "\">" . $prevlabel . "</a>\n";

	}

	

	// current

	$out.= "<span class=\"current\"> ".Page ." " . $page . " ".of." " . $tpages . "</span>\n";
/*
	$out .= "<select name=\"pagination-combo\" id=\"pagination-combo\">";
			
			for($x = 1; $x<=$tpages;$x++){
				if($x == $page){
					$selected = " selected=\"selected\"";
				}else{
					$selected = "";
				}
				$out .= "<option value=".$x." ".$selected.">".Page." ".$x."</option>";
			}
			
	$out .= "<select>";
*/
	// next



	if($page<$tpages) {

		$out.= "<a class=\"next\" href=\"" . $reload . "page=" .($page+1) ."\">" . $nextlabel . "</a>\n";

	}

	else {

		$out.= "<span class=\"next-disable\" >" . $nextlabel . "</span>\n";

	}

	

	// last

	if($page<$tpages) {

		$out.= "<a class=\"last\" href=\"" . $reload . "page=" . $tpages . "\">" . $lastlabel . "</a>\n";

	}

	else {

		$out.= "<span class=\"last-disable\">" . $lastlabel . "</span>\n";

	}

	

	$out.= "</div>";

	

	return $out;


}



?>