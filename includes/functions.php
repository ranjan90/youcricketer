<?php

	//define match type arrays
	$matchArray = array('0' 	=> 'Ten10',
						'1' 	=> 'Fifteen15',
						'2'		=> 'Twenty20',
						'3' 	=> 'One Day',
						'4' 	=> 'Test Match',
						);

	//preferred game types
	$prefferedGameType = array( '0' 	=> 'Hard Ball',
								'1' 	=> 'Rubber Ball',
								'2' 	=> 'Tape Ball',
								'3' 	=> 'Tennis Ball',
								);

	//user fields list
	$userFields 		= array( 	'Year of Birth', 	'experiences',  'about_myself', 'recent_activities', 'Photo Album','Video Album'
								);

	$legalStatus 		= array(
									'Domestic Corporation', 'Exempt Corporation', 'Foreign Corporation',
									'Limited Partnership', 'Limited Liability Company', 'Limited Liability Partnership',
									'Partnership', 'Limited Liability Limited Partnership', 'Sole Proprietor',
									'Private Limited Company', 'Private Company', 'Non Profit', 'Other', 'None'
							);

	$statusLevel 	 	= array(
									'Professional', 'Semi Professional', 'Competitive Amateur', 'Amateur', 'Social'
							);

	//get record on id
	function get_record_on_id($tablename, $id, $fields = ''){
		global $conn;
		if(empty($fields)){
			$fields = '*';
		}
		$query = 'select '.$fields.' from '.$tablename.' where id = '.$id;
		$rs = mysqli_query($conn,$query);
		$row = mysqli_fetch_assoc($rs);
		return $row;
	}


	function date_converter($date, $format = 'M d, Y'){
		return date($format,strtotime($date));
	}
	function pr($a){
		echo "<pre>";
		print_r($a);
		echo "</pre>";
	}



	function createMsg($msg,$type="success"){

		if($type=="error"){

			$_SESSION['msg']='<div id="information" class="msg-box alert alert-danger">'.$msg.'</div>';

		}elseif($type=="success"){

			$_SESSION['msg']='<div id="information" class="msg-box alert alert-success">'.$msg.'</div>';

		}



	}

	function my_pagination($page,$total,$url){

		$html='<ul class="pagination">';

		if($page>1){

			$ur=$url."?page=".$page-1;

			$html.='<li><a href="'.$ur.'"></a>Prev</li>';

		}

		for($i=1;$i<=$total;$i++){

			$ur=$url."?page=".$i;

			$html.='<li><a href="'.$ur.'">Page '.$i.'</></li>';
		}


		$html.='</ul>';

		return $html;

	}

	function displayMsg(){

		$msg='';

		if(isset($_SESSION['msg'])){

				$msg=$_SESSION['msg'];
				unset($_SESSION['msg']);
		}

		return $msg;

	}

	function my_redirect($url){

		echo "<script>window.location.href='".$url."'</script>";

	}


	//truncate string
	function truncate_string($i,$l){
		if(strlen($i) >= $l){
			$trim_i = trim($i);
			$o = "";
			for($a=0;$a<=$l;$a++){
				$o = $o.$trim_i[$a];
			}
			return $o.'...';
		}else{
			return $i;
		}
	}

	//get combo box
	function get_combo($table, $field, $selected='', $rename='',$r='',$p='',$show='', $class=''){
		global $conn;
		$output = "";
		if($selected){
			$query = "select id,".$field." from ".$table." order by ".$field;
		}else{
			$query = "select id,".$field." from ".$table." where status=1 order by ".$field;
		}

		if($rename == 'generic_id'){
			$required = '';
		}else{
			$required = 'validate[required]';
		}

		$rs_combo = mysqli_query($conn,$query);
		if(empty($r)){
			if($rename){
				echo '<select name="'.$rename.'" id="'.$rename.'" class="'.$required.' dd1 form-control '.$class.'">';
			}else{
				echo '<select name="'.$field.'" id="'.$field.'" class="'.$required.' dd1 form-control '.$class.'">';
			}

			if(!empty($selected)){
				while($row_combo = mysqli_fetch_assoc($rs_combo)){
					if($selected == $row_combo['id']){
						echo '<option selected="selected" value="'.$row_combo['id'].'">'.$row_combo[$field].'</option>';
					}else{
						echo '<option value="'.$row_combo['id'].'">'.$row_combo[$field].'</option>';
					}
				}
			}else{
				if(!empty($show)){
					$front = $show;
				}else{
					$front = "Select One";
				}

				echo '<option selected="selected" value="">'.$front.'</option>';
				while($row_combo = mysqli_fetch_assoc($rs_combo)){
					echo '<option value="'.$row_combo['id'].'">'.$row_combo[$field].'</option>';
				}
			}
			echo '</select>';
		}else{
			if($selected){
				$q = "select id,".$field." from ".$table." where id='".$selected."'";
				$rs_combo = mysqli_query($conn,"select id,".$field." from ".$table." where id='".$selected."'");
				$row_combo = mysqli_fetch_assoc($rs_combo);
				echo  $row_combo[$field];
			}
		}
	}


	//get combo box
	function get_combo_signup($table, $field, $selected='', $rename='',$r='',$p='',$show='', $class=''){
		$output = "";
		global $conn;
		if($selected){
			$query = "select id,".$field." from ".$table." order by ".$field;
		}else{
			$query = "select id,".$field." from ".$table." where status=1 order by ".$field;
		}

		if($rename == 'generic_id'){
			$required = '';
		}else{
			$required = 'validate[required]';
		}

		$rs_combo = mysqli_query($conn,$query);
		if(empty($r)){
			if($rename){
				echo '<select name="'.$rename.'" id="'.$rename.'"  class="'.$required.' form-control dd1 '.$class.'">';
			}else{
				echo '<select name="'.$field.'" id="'.$field.'" class="'.$required.' form-control dd1 '.$class.'">';
			}

			if(!empty($selected)){
				while($row_combo = mysqli_fetch_assoc($rs_combo)){
					if($selected == $row_combo['id']){
						echo '<option selected="selected" value="'.$row_combo['id'].'">'.$row_combo[$field].'</option>';
					}else{
						echo '<option value="'.$row_combo['id'].'">'.$row_combo[$field].'</option>';
					}
				}
			}else{
				if(!empty($show)){
					$front = $show;
				}else{
					$front = "Select One";
				}

				echo '<option selected="selected" value="">'.$front.'</option>';
				while($row_combo = mysqli_fetch_assoc($rs_combo)){
					echo '<option value="'.$row_combo['id'].'">'.$row_combo[$field].'</option>';
				}
			}
			echo '</select>';
		}else{
			if($selected){
				$q = "select id,".$field." from ".$table." where id='".$selected."'";
				$rs_combo = mysqli_query($conn,"select id,".$field." from ".$table." where id='".$selected."'");
				$row_combo = mysqli_fetch_assoc($rs_combo);
				echo  $row_combo[$field];
			}
		}
	}

	function get_status_combo_selected($val=''){
		if($val == ''){
			$val3 = 'selected';
		}elseif($val==1){
			$val1 = 'selected';
		}else{
			$val2 = 'selected';
		}
		$output = '<select name="status" id="status" class="dd1 form-control">';
		$output .='<option value="" '.$val3.'>Select One</option><option value="0" '.$val2.'>Inactive</option>
		<option value="1" '.$val1.'>Active</option>';
		$output .='</select>';
		return $output;
	}
	//get status combo box
	function get_status_combo($s='', $r='', $class=''){
		global $conn;
		$task_status_arr = array(
								0 => 'Inactive',
								1 => 'Active');

		if(isset($class) && !empty($class)){
			$classstr = $class;
		}else{
			$classstr = '';
		}

		if(empty($r)){
			echo '<select name="status" id="status" class="validate[required] dd1 form-control '.$classstr.'">';
			if($s === ''){
				echo '<option selected="selected" value="">Select One</option>';
				for($i=0;$i<count($task_status_arr);$i++){
					echo '<option value="'.$i.'">'.$task_status_arr[$i].'</option>';
				}
			}else{
				for($i=0;$i<count($task_status_arr);$i++){
					if($i == $s){
						echo '<option selected value="'.$i.'">'.$task_status_arr[$i].'</option>';
					}else{
						echo '<option value="'.$i.'">'.$task_status_arr[$i].'</option>';
					}
				}
			}
			echo '</select>';
		}else{
			for($i=0;$i<count($task_status_arr);$i++){
				if($i == $s){
					$output = $task_status_arr[$i];
				}
			}
			return $output;
		}
	}

	//get gender combo box
	function get_gender_combo($s='', $r='', $class=''){
		global $conn;
		$task_status_arr = array(
								0 => 'Female',
								1 => 'Male');

		if(isset($class) && !empty($class)){
			$classstr = $class;
		}else{
			$classstr = '';
		}

		if(empty($r)){
			echo '<select name="gender" id="gender" class="dd1 validate[required] form-control '.$classstr.'">';
			if($s === ''){
				echo '<option selected="selected" value="">Select One</option>';
				for($i=0;$i<count($task_status_arr);$i++){
					echo '<option value="'.$i.'">'.$task_status_arr[$i].'</option>';
				}
			}else{
				for($i=0;$i<count($task_status_arr);$i++){
					if($i == $s){
						echo '<option selected value="'.$i.'">'.$task_status_arr[$i].'</option>';
					}else{
						echo '<option value="'.$i.'">'.$task_status_arr[$i].'</option>';
					}
				}
			}
			echo '</select>';
		}else{
			for($i=0;$i<count($task_status_arr);$i++){
				if($i == $s){
					$output = $task_status_arr[$i];
				}
			}
			return $output;
		}
	}

	//get first class player combo box
	function get_first_class_player_combo($s='', $r='', $class=''){
		global $conn;
		$task_status_arr = array(
								0 => 'No',
								1 => 'Yes');

		if(isset($class) && !empty($class)){
			$classstr = $class;
		}else{
			$classstr = '';
		}

		if(empty($r)){
			echo '<select name="first_class_player" id="first_class_player" class="validate[required] form-control '.$classstr.'">';
			if($s === ''){
				echo '<option selected="selected" value="">Select One</option>';
				for($i=0;$i<count($task_status_arr);$i++){
					echo '<option value="'.$i.'">'.$task_status_arr[$i].'</option>';
				}
			}else{
				for($i=0;$i<count($task_status_arr);$i++){
					if($i == $s){
						echo '<option selected value="'.$i.'">'.$task_status_arr[$i].'</option>';
					}else{
						echo '<option value="'.$i.'">'.$task_status_arr[$i].'</option>';
					}
				}
			}
			echo '</select>';
		}else{
			for($i=0;$i<count($task_status_arr);$i++){
				if($i == $s){
					$output = $task_status_arr[$i];
				}
			}
			return $output;
		}
	}

	//get player type combo box
	function get_player_type_combo($s='', $r='', $class=''){
		global $conn;
		$task_status_arr = array(
								'Batsman',
								'Bowler',
								'All Rounder',
								'Specialist Wicketkeeper');

		if(isset($class) && !empty($class)){
			$classstr = $class;
		}else{
			$classstr = '';
		}

		if(empty($r)){
			echo '<select name="player_type" id="player_type" class="validate[required] form-control '.$classstr.'">';
			if($s === ''){
				echo '<option selected="selected" value="">Select One</option>';
				for($i=0;$i<count($task_status_arr);$i++){
					echo '<option value="'.$i.'">'.$task_status_arr[$i].'</option>';
				}
			}else{
				for($i=0;$i<count($task_status_arr);$i++){
					if($i == $s){
						echo '<option selected value="'.$i.'">'.$task_status_arr[$i].'</option>';
					}else{
						echo '<option value="'.$i.'">'.$task_status_arr[$i].'</option>';
					}
				}
			}
			echo '</select>';
		}else{
			for($i=0;$i<count($task_status_arr);$i++){
				if($i == $s){
					$output = $task_status_arr[$i];
				}
			}
			return $output;
		}
	}

	//convert string to permalink
	function friendlyURL($string){
		$output	= strtolower($string);
		$output = str_replace('`','',$output);
		$output = str_replace('~','',$output);
		$output = str_replace('@','',$output);
		$output = str_replace('#','',$output);
		$output = str_replace('$','',$output);
		$output = str_replace('%','',$output);
		$output = str_replace('^','',$output);
		$output = str_replace('&','',$output);
		$output = str_replace('*','',$output);
		$output = str_replace('(','',$output);
		$output = str_replace(')','',$output);
		$output = str_replace('+','',$output);
		$output = str_replace('=','',$output);
		$output = str_replace(']','',$output);
		$output = str_replace('}','',$output);
		$output = str_replace('[','',$output);
		$output = str_replace('{','',$output);
		$output = str_replace('\'','',$output);
		$output = str_replace('/','',$output);
		$output = str_replace('"','',$output);
		$output = str_replace(';','',$output);
		$output = str_replace(':','',$output);
		$output = str_replace('?','',$output);
		$output = str_replace('.','',$output);
		$output = str_replace('!','',$output);
		$output = str_replace('>','',$output);
		$output = str_replace(',','',$output);
		$output = str_replace('<','',$output);

		return urlencode($output);
	}

	//get combo box
	function get_categories_combo($table, $field, $selected='', $rename='',$parent_field=''){
		global $conn;
		$query = "select id,".$field." from ".$table." where ".$parent_field." = '0'" ;
		echo '<select name="'.$rename.'" id="'.$rename.'" class="validate[required] '.$class.'">';
		$rs_combo = mysqli_query($conn,$query);
		while($row_p = mysqli_fetch_assoc($rs_combo)){
			if(!empty($selected) && $row_p['id'] == $selected){
				$select_value = ' selected="selected"';
			}else{
				$select_value = '';
			}
			echo '<option '.$select_value.' value="'.$row_p['id'].'">'.$row_p['name'].'</option>';
			$rs_child = mysqli_query($conn,"select * from $table where parent_id = ".$row_p['id']);
			while($row_c = mysqli_fetch_assoc($rs_child)){
				if(!empty($selected) && $row_c['id'] == $selected){
					$cselect_value = ' selected="selected"';
				}else{
					$cselect_value = '';
				}
				echo '<option '.$cselect_value.' value="'.$row_c['id'].'">&nbsp;&nbsp;&#45;&nbsp;'.$row_c['name'].'</option>';
			}
		}

		echo '</select>';
	}

	function debug($var){
		echo '<pre>';
		print_r($var);
		echo '</pre>';
	}

	function getRandomWord($len) {
	    $word = array_merge(range('a', 'z'), range('A', 'Z'), range('1', '0'));
	    shuffle($word);
	    return substr(implode($word), 0, $len);
	}

	function cleanForBreadcrumb($var){
		$string 	= str_replace('.html','',$var);
		$string 	= str_replace('-',' ',$string);


		return ucwords($string);
	}
	function getBrowser()
	{
	    $u_agent = $_SERVER['HTTP_USER_AGENT'];
	    $bname = 'Unknown';
	    $platform = 'Unknown';
	    $version= "";

	    //First get the platform?
	    if (preg_match('/linux/i', $u_agent)) {
	        $platform = 'linux';
	    }
	    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
	        $platform = 'mac';
	    }
	    elseif (preg_match('/windows|win32/i', $u_agent)) {
	        $platform = 'windows';
	    }

	    // Next get the name of the useragent yes seperately and for good reason
	    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
	    {
	        $bname = 'Internet Explorer';
	        $ub = "MSIE";
	    }
	    elseif(preg_match('/Firefox/i',$u_agent))
	    {
	        $bname = 'Mozilla Firefox';
	        $ub = "Firefox";
	    }
	    elseif(preg_match('/Chrome/i',$u_agent))
	    {
	        $bname = 'Google Chrome';
	        $ub = "Chrome";
	    }
	    elseif(preg_match('/Safari/i',$u_agent))
	    {
	        $bname = 'Apple Safari';
	        $ub = "Safari";
	    }
	    elseif(preg_match('/Opera/i',$u_agent))
	    {
	        $bname = 'Opera';
	        $ub = "Opera";
	    }
	    elseif(preg_match('/Netscape/i',$u_agent))
	    {
	        $bname = 'Netscape';
	        $ub = "Netscape";
	    }

	    // finally get the correct version number
	    $known = array('Version', $ub, 'other');
	    $pattern = '#(?<browser>' . join('|', $known) .
	    ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
	    if (!preg_match_all($pattern, $u_agent, $matches)) {
	        // we have no matching number just continue
	    }

	    // see how many we have
	    $i = count($matches['browser']);
	    if ($i != 1) {
	        //we will have two since we are not using 'other' argument yet
	        //see if version is before or after the name
	        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
	            $version= $matches['version'][0];
	        }
	        else {
	            $version= $matches['version'][1];
	        }
	    }
	    else {
	        $version= $matches['version'][0];
	    }

	    // check if we have a number
	    if ($version==null || $version=="") {$version="?";}

	    return array(
	        'userAgent' => $u_agent,
	        'name'      => $bname,
	        'version'   => $version,
	        'platform'  => $platform,
	        'pattern'    => $pattern
	    );
	}

	function getSlug($str, $replace=array(), $delimiter='-') {
		if( !empty($replace) ) {
			$str = str_replace((array)$replace, ' ', $str);
		}

		$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
		$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
		$clean = strtolower(trim($clean, '-'));
		$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

		return $clean;
	}

	function getPaging($url,$total_records,$current_page,$records_per_page = 10,$show_pages = 10){
		global $conn;
		$paging_str = '<nav><ul class="pagination" style="margin:0;">';
		$total_pages = ceil($total_records/$records_per_page);
		if($total_pages<$show_pages){
			$show_pages = $total_pages;
		}
		for($i=1;$i<=$show_pages;$i++){
			if($i == $current_page){
				$paging_str.= '<li><a href="#" style="background-color:#eee;">'.$i.'</a></li>';
			}else{
				$paging_str.='<li><a href="'.WWW.$url.'/page'.$i.'">'.$i.'</a></li> ';
			}
		}

		if($total_pages>$show_pages){
			$paging_str.='<li class="current">...</li> <li><a href="'.WWW.$url.'/page'.$total_pages.'">'.$total_pages.'</a></li> ';
		}
		$paging_str.='</ul></nav>';

		return $paging_str;
	}

	function getGeoLocationCountry(){
		if(isset($_SESSION['selected_country']) && !empty($_SESSION['selected_country'])){
			 $selected_country = $_SESSION['selected_country'];
		}else{
			$location_data 	= unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR']));
			$_SESSION['selected_country'] = $location_data['geoplugin_countryName'];
			$selected_country = $_SESSION['selected_country'];
		}
		return $selected_country;
	}

	function getRightSideAd()
	{
		global $conn;
		$query = mysqli_query($conn,"SELECT * FROM advertisement WHERE CURDATE() <= expiry_date AND status = 1 ORDER BY sort ASC");
		while( $row = mysqli_fetch_assoc($query))
		{
 			echo '<div class="row"><div class="col-sm-12">
					<a title="'.$row['ad_name'].'" href="'.$row['ad_link'].'" target="_BLANK" id="'.$row['advertise_id'].'" class="add-stat">
						<img src="'.WWW.'ads/right_side/'.$row['ad_image'].'" alt="'.$row['ad_name'].'" style="display:block;width:100%" id="'.$row['advertise_id'].'"  class="img-app"/>
					</a>
					</div></div>';
		}
	}

	function mySponsorOnPayment($payment_id,$user_id,$payment_status,$qty)
    {
        global $conn;
        $quantity = 1;
        $status   = 1;

        $start_date   = date('Y-m-d');
        $getData      = getDaysLeft($start_date);
        $total        = $getData['total'];
        $expire_date  = $getData['expire_date'];
        $date_created = $start_date;
        if (isset($qty) && !empty($qty) && $qty > 0) {
            $quantity = $qty;
        }
        $totalDays = $total * $quantity;
        $sponsor_row = checkUserSponsor($user_id);
        if( $sponsor_row )
        {
            $totalDays   =  adjustUserSponsor($sponsor_row,$qty);
            $start_date  = $sponsor_row['date_start'];
            $expire_date = getNewExpiryDate($start_date,$totalDays);
        }
        $sql = "INSERT INTO user_sponsor_payment (`payment_id`,`user_id`,`quantity`,`date_created`,`date_start`,`date_end`,`total_days`,`days_left`,`status`,`payment_status`)
                    VALUES ('$payment_id','$user_id','$quantity','$date_created','$start_date','$expire_date','$totalDays','$totalDays','$status','$payment_status') ";

        mysqli_query($conn, $sql);
    }

    function checkUserSponsor($user_id) // Check if user have already sponsor
    {
        global $conn;
        $sql  = "SELECT * FROM user_sponsor_payment WHERE user_id = '$user_id' AND status = 1 ORDER BY user_sponsor_id DESC ";
        $row = mysqli_fetch_assoc(mysqli_query($conn, $sql));
        if($row){
            return $row;
        } else {
            return false;
        }
    }

    function adjustUserSponsor($row,$qty)
    {
        global $conn;
        $start = date("Y-m-d");
        $end   = $row['date_end'];
        $id    = $row['user_sponsor_id'];
        $quantity = 1;
        $daysLeft   = dateTimeDaysLeft($start,$end);
        $days       = getDaysLeft($start);
        $daysToAdd  = $days['total'];
        $sql = " UPDATE user_sponsor_payment SET status = '0' WHERE user_sponsor_id = $id";
        mysqli_query($conn, $sql);

        if (isset($qty) && !empty($qty) && $qty > 0) {
            $quantity = $qty;
        }
        $daysToAdd   = $daysToAdd * $quantity;
        $total_days = $daysLeft + $daysToAdd;
        return $total_days;
    }

    function getNewExpiryDate($date,$days)
    {
         $end_date = date('Y-m-d', strtotime("+$days days $date"));
         return $end_date;
    }

    function dateTimeDaysLeft($start,$end)
    {
        $dateOBj = new DateTime();
        $start_date = $dateOBj->format($start);
        $end_date   = $dateOBj->format($end);

        $dateObj1 = new DateTime($start_date);
        $dateObj2 = new DateTime($end_date);
        $total = $dateObj2->diff($dateObj1)->format("%a");
        return $total;
    }

    function getDaysLeft($start_date)
    {
        $return_data = array();
        $expire_date = date('Y-m-d', strtotime("+12 months $start_date"));
        $dateOBj = new DateTime();
        $start_date = $dateOBj->format($start_date);
        $end_date = $dateOBj->format($expire_date);

        $dateObj1 = new DateTime($start_date);
        $dateObj2 = new DateTime($end_date);
        $total    = $dateObj2->diff($dateObj1)->format("%a");
        $return_data['total']       = $total;
        $return_data['expire_date'] = $expire_date;
        return $return_data;
    }

    function dd($array)
    {
        echo "<pre>";
        print_r($array);
        echo "</pre>";
        die;
    }
