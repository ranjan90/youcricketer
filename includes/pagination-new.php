<?php
/*************************************************************************
php easy :: pagination scripts set - Version One
==========================================================================
Author:      php easy code, www.phpeasycode.com
Web Site:    http://www.phpeasycode.com
Contact:     webmaster@phpeasycode.com
Modified By : Naveed Ramzan
*************************************************************************/
function paginate_one($reload, $page, $tpages ) {
	$firstlabel = "First";
	$prevlabel  = "Prev";
	$nextlabel  = "Next";
	$lastlabel  = "Last";

	$pUrl 		= explode('.html', $reload);
	
	//$out = "<div class=\"pagin\">\n";
	$out = '<nav><ul class="pagination" style="margin-top:0">';
	
	// first
	if($page==2) {
		$reload 	= $pUrl[0]	.'-'.($page-1).'.html';
		//$out.= "<a class=\"frist\" href=\"" . $reload . "\">" . $firstlabel . "</a>\n";
		$out.='<li><a href="'.$reload.'">'.$firstlabel.'</a></li>';
	}else if($page>2) {
		$reload 	= $pUrl[0]	.'-1.html';
		//$out.= "<a class=\"frist\" href=\"" . $reload . "\">" . $firstlabel . "</a>\n";
		$out.='<li><a href="'.$reload.'">'.$firstlabel.'</a></li>';
	}else if($page==1){
		//$out.= "<span class=\"frist-disable\">" . $firstlabel . "</span>\n";
		$out.='<li><a href="#" aria-label="Previous"><span aria-hidden="true">'.$firstlabel.'</span></a></li>';
	}

	// previous
	if($page==1) {
		//$out.= "<span class=\"prev-disable\">" . $prevlabel . "</span>\n";
		$out.='<li><a href="#" aria-label="Previous"><span aria-hidden="true">'.$prevlabel.'</span></a></li>';
	}else if($page==2) {
		$reload 	= $pUrl[0]	.'-'.($page-1).'.html';
		//$out.= "<a class=\"prev\" href=\"" . $reload . "\">" . $prevlabel . "</a>\n";
		$out.='<li><a href="'.$reload.'">'.$prevlabel.'</a></li>';
	} else {
		$reload 	= $pUrl[0]	.'-'.($page-1).'.html';
		//$out.= "<a class=\"prev\" href=\"" . $reload . "\">" . $prevlabel . "</a>\n";
		$out.='<li><a href="'.$reload.'">'.$prevlabel.'</a></li>';
	}

	// current
	$out.= '<li><a href="#" aria-label="Previous"><span aria-hidden="true">Page ' . $page . " of " . $tpages . "</span></a></li>";
	/*$out .= "<select name=\"pagination-combo\" id=\"pagination-combo\">";
			for($x = 1; $x<=$tpages;$x++){
				if($x == $page){
					$selected = " selected=\"selected\"";
				}else{
					$selected = "";
				}
				$out .= "<option value=".$x." ".$selected.">".Page." ".$x."</option>";
			}
	$out .= "<select>";*/

	// next
	if($page<$tpages) {
		$reload 	= $pUrl[0]	.'-'.($page+1).'.html';
		//$out.= "<a class=\"next\" href=\"" . $reload . "\">" . $nextlabel . "</a>\n";
		$out.='<li><a href="'.$reload.'">'.$nextlabel.'</a></li>';
	} else {
		//$out.= "<span class=\"next-disable\" >" . $nextlabel . "</span>\n";
		$out.='<li><a href="#" aria-label="Previous"><span aria-hidden="true">'.$nextlabel.'</span></a></li>';
	}

	// last
	if($page<$tpages) {
		$reload 	= $pUrl[0]	.'-'.($tpages).'.html';
		//$out.= "<a class=\"last\" href=\"" . $reload . "\">" . $lastlabel . "</a>\n";
		$out.='<li><a href="'.$reload.'">'.$lastlabel.'</a></li>';
	} else {
		//$out.= "<span class=\"last-disable\">" . $lastlabel . "</span>\n";
		$out.='<li><a href="#" aria-label="Previous"><span aria-hidden="true">'.$lastlabel.'</span></a></li>';
	}

	$out.= "</ul></nav>";

	return $out;
}
?>