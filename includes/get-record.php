<?	include('configuration.php');
	
	$table 	= $_POST['table'];
	$recId  = $_POST['id'];
	$field  = $_POST['field'];
	if($table == 'clubs'){
		$row    = mysqli_fetch_assoc(mysqli_query($conn,"select utc.*, c.title from users_to_clubs utc join clubs c on c.id = utc.club_id where utc.id = '".$recId."'"));
		echo $row['sort'].'___'.$row['title'].'___'.$row['affiliation_date'];
	}else if($table == 'leagues'){
		$row    = mysqli_fetch_assoc(mysqli_query($conn,"select utc.*, c.title from users_to_leagues utc join leagues c on c.id = utc.league_id where utc.id = '".$recId."'"));
		echo $row['sort'].'___'.$row['title'].'___'.$row['affiliation_date'];
	}else if($table == 'agents'){
		$row    = mysqli_fetch_assoc(mysqli_query($conn,"select * from agents where id = '".$recId."'"));
		echo $row['sort'].'___'.$row['name'].'___'.$row['affiliation_date'];
	}else if($table == 'educations'){
		$row    = get_record_on_id($table, $recId);
		$edu 	= $row['degree'].'___'.$row['institute'].'___'.$row['passing_year'];
		echo $edu;
	}else if($table == 'experiences'){
		$row    = get_record_on_id($table, $recId);
		$row_c  = get_record_on_id('countries',$row['country_id']);
		$edu 	= $row_c['name'].'___'.$row['organization'].'___'.$row['job_title'].'___'.$row['from_date'].'___'.$row['to_date'];
		echo $edu;
	}else if($table == 'travel_visas'){
		$row    = get_record_on_id($table, $recId);
		$row_c  = get_record_on_id('countries',$row['country_id']);
		$travel	= $row_c['name'].'___'.$row['from_date'].'___'.$row['to_date'];
		echo $travel;
	}else if($table == 'languages'){
		$row    = get_record_on_id($table, $recId);
		$travel	= $row['language'].'___'.$row['speaking'].'___'.$row['reading'].'___'.$row['writing'].'___'.$row['sort'];
		echo $travel;
	}else if($table == 'favorite_cricketers'){
		$row    = get_record_on_id($table, $recId);
		$travel	= $row['name'].'___'.$row['sort'];
		echo $travel;
	}else if($table == 'favorite_brands'){
		$row    = get_record_on_id($table, $recId);
		$travel	= $row['name'].'___'.$row['sort'];
		echo $travel;
	}else if($table == 'company_staff'){
		$row    = get_record_on_id($table, $recId);
		$travel	= $row['title'].'___'.$row['first_name'].'___'.$row['last_name'];
		echo $travel;
	}else if($table == 'opportunities'){
		$row    = get_record_on_id($table, $recId);
		if($row['type'] == 'domestic'){
			$row_city = get_record_on_id('opportunities',$recId);
			$opp = $row_city['city_name'].'___'.$row['from_date'].'___'.$row['to_date'];
		}else{
			$row_country = get_record_on_id('countries', $row['country_id']);
			$row_state   = get_record_on_id('states', $row['state_id']);
			$row_city    = get_record_on_id('opportunities',$recId);
			$opp = $row_city['city_name'].'___'.$row['from_date'].'___'.$row['to_date'];
		}
		echo $opp;
	}else{
		$row    = get_record_on_id($table, $recId);	
		echo $row[$field];
	}
?>