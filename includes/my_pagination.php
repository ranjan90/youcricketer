<?php /*
<style>

.paginp{ 
	margin: 7px 0;
    overflow: hidden;    
    width:450px;
}

.pagin form{float:right; }


.pagin a.last{margin:0 2px;}
.pagin span:hover{}

.current{
	background: none;
    border: 0px solid #b3b3b3; 
    color: #000000;
    display: block;
    float: left;
    font-size: 12px;
    height: 26px;
    line-height: 26px;
    margin: 0 2px;
    text-align: center;
    padding:0 15px 0 15px;
	opacity:1;
}
.pagin select[name=pagination-combo]{width:90px;}

span.disable{
	background: url("../images/pagination-bg.png") repeat-x;
    border: 1px solid #FFFFFF; 
    color: #000000;
    display: block;
    float: left;
    font-size: 12px;
    height: 26px;
    line-height: 26px;
    margin: 0 10px;
    text-align: center;
    padding:0 5px 0 5px;
	opacity:1;
}

span.current:hover{	background: none;}



.prevp, .nextp {
    background: url("../images/pagination-bg.png") repeat-x;
    border: 1px solid #b3b3b3; 
    color: #000000;
    display: block;
    float: left;
    font-size: 12px;
    height: 26px;
    line-height: 26px;
    margin: 0 10px;
    text-align: center;
    padding:0 5px 0 5px;
	opacity:1;
}
.prevp:hover, .nextp:hover {
   background: url("../images/pagination-bg.png") repeat-x;
    border: 1px solid black; 
    
}
.prevp {
    float:left;
}
.nextp {
    float:left
}

.last{
	
	 background: url("../images/pagination-bg.png") repeat-x;
border: 1px solid #b3b3b3;
color: #000000;
display: block;
float: left;
font-size: 12px;
height: 26px;
line-height: 26px;
margin: 0 10px;
text-align: center;
padding: 0 5px 0 5px;
opacity: 1;
}

.last:hover{
	
	 background: url("../images/pagination-bg.png") repeat-x;
border: 1px solid black;

}
.disablep {
    background: url("../images/pagination-bg.png") repeat-x;
    border: 1px solid #b3b3b3; 
    color: silver;
    display: block;
    float: left;
    font-size: 12px;
    height: 26px;
    line-height: 26px;
    margin: 0 10px;
    text-align: center;
    padding:0 5px 0 5px;
	background: transparent;
}</style>
*/ ?>
<?php 


function pagination($rows,$sql,$keyword,$link){
$returnData[] = '';

// This is the number of results we want displayed per page
$page_rows = 10;
// This tells us the page number of our last page
$last = ceil($rows/$page_rows);
// This makes sure $last cannot be less than 1
if($last < 1){
	$last = 1;
}
// Establish the $pagenum variable
$pagenum = 1;
// Get pagenum from URL vars if it is present, else it is = 1
if(isset($_GET['pn'])){
	$pagenum = preg_replace('#[^0-9]#', '', $_GET['pn']);
}
// This makes sure the page number isn't below 1, or more than our $last page
if ($pagenum < 1) { 
    $pagenum = 1; 
} else if ($pagenum > $last) { 
    $pagenum = $last; 
}
// This sets the range of rows to query for the chosen $pagenum
$limit = 'LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;
// This is your query again, it is for grabbing just one page worth of rows by applying $limit

$returnData['limit'] = $limit;

// Establish the $paginationCtrls variable
$paginationCtrls = '<div class="row"><div class="col-sm-3 col-md-2"><span class="current-page">Page '.$pagenum.'  of '.$last.' </span></div>
<div class="col-sm-9 col-md-10"><nav><ul class="pagination">';

// If there is more than 1 page worth of results
if($last != 1){
	/* First we check if we are on page one. If we are then we don't need a link to 
	   the previous page or the first page so we do nothing. If we aren't then we
	   generate links to the first page, and to the previous page. */
	
	if ($pagenum == 1) {
		//$paginationCtrls .= "<span class='disablep'>First </span>  ";
		$paginationCtrls .= '<li><a href="#" aria-label="First"><span aria-hidden="true">First</span></a></li>';
		 
	}else {
		//$paginationCtrls .= '<a href="'.$link.''.$keyword.'&pn='.$previous.'"> <span class="prevp">First </span></a>  ';
		$paginationCtrls .= '<li><a href="'.$link.''.$keyword.'&pn='.$previous.'" aria-label="First"><span aria-hidden="true">First</span></a></li>';
	}
	
	if ($pagenum > 1) {
        $previous = $pagenum - 1;
		//$paginationCtrls .= '<a href="'.$link.''.$keyword.'&pn='.$previous.'"> <span class="prevp">Prev </span></a> ';
		$ne = '<a href="'.$link.''.$keyword.'"&pn="'.$previous.'">Previous</a>  &nbsp; ';
		// Render clickable number links that should appear on the left of the target page number
		$paginationCtrls .= '<li><a href="'.$link.''.$keyword.'"&pn="'.$previous.'" aria-label="Previous"><span aria-hidden="true">Prev</span></a></li>';
			    
    }else {
	    	//$paginationCtrls .= "<span class='disablep'>Prev </span>  ";
			$paginationCtrls .= '<li><a href="#" aria-label="Previous"><span aria-hidden="true">Prev</span></a></li>';
	    }
	// Render the target page number, but without it being a link
	//$paginationCtrls .= '<span class="current">Page '.$pagenum.'  of '.$last.' </span> ';
	// Render clickable number links that should appear on the right of the target page number
	
	// This does the same as above, only checking if we are on the last page, and then generating the "Next"
    if ($pagenum != $last) {
        $next = $pagenum + 1;
		
        //$paginationCtrls .= '<a href="'.$link.''.$keyword.'&pn='.$next.'"> <span class="nextp">Next </span></a> ';
		$paginationCtrls .= '<li><a href="'.$link.''.$keyword.'&pn='.$next.'" aria-label="Next"><span aria-hidden="true">Next</span></a></li>';
    }
    
    else {
    	//$paginationCtrls .= "<span class='disablep'> Next </span>";
		$paginationCtrls .= '<li><a href="#" aria-label="Next"><span aria-hidden="true">Next</span></a></li>';
    }
    
    
    if ($pagenum == $last) {
    	
    	//$paginationCtrls .= "<span class='disablep'>Last </span>";
		$paginationCtrls .= '<li><a href="#" aria-label="Last"><span aria-hidden="true">Last</span></a></li>';
    }
    
    else {
    	
    	//$paginationCtrls .= '<a href="'.$link.''.$keyword.'&pn='.$last.'"><span class="last">Last </span></a> ';
		$paginationCtrls .= '<li><a href="'.$link.''.$keyword.'&pn='.$last.'" aria-label="Last"><span aria-hidden="true">Last</span></a></li>';
    }
    
}
$paginationCtrls .= '</ul></nav></div></div>';
	$returnData['pagination'] = trim($paginationCtrls);
	return $returnData;
}






function com_pagination($rows,$sql,$keyword,$link){
	$returnData[] = '';
	
	// This is the number of results we want displayed per page
	$page_rows = 10;
	// This tells us the page number of our last page
	$last = ceil($rows/$page_rows);
	// This makes sure $last cannot be less than 1
	if($last < 1){
		$last = 1;
	}
	// Establish the $pagenum variable
	$pagenum = 1;
	// Get pagenum from URL vars if it is present, else it is = 1
	if(isset($_GET['pn'])){
		$pagenum = preg_replace('#[^0-9]#', '', $_GET['pn']);
	}
	// This makes sure the page number isn't below 1, or more than our $last page
	if ($pagenum < 1) {
		$pagenum = 1;
	} else if ($pagenum > $last) {
		$pagenum = $last;
	}
	// This sets the range of rows to query for the chosen $pagenum
	$limit = 'LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;
	// This is your query again, it is for grabbing just one page worth of rows by applying $limit
	
	$returnData['limit'] = $limit;
	
	// Establish the $paginationCtrls variable
	$paginationCtrls = '<div class="paginp">';
	// If there is more than 1 page worth of results
	if($last != 1){
		/* First we check if we are on page one. If we are then we don't need a link to
		 the previous page or the first page so we do nothing. If we aren't then we
		generate links to the first page, and to the previous page. */
	
		if ($pagenum == 1) {
			$paginationCtrls .= "<span class='disablep'>First </span>  ";
	
				
		}else {
			$paginationCtrls .= '<a href="'.$link.''.$keyword.'&pn='.$previous.'#companies-tab"> <span class="prevp">First </span></a>  ';
	
		}
	
		if ($pagenum > 1) {
			$previous = $pagenum - 1;
			$paginationCtrls .= '<a href="'.$link.''.$keyword.'&pn='.$previous.'#companies-tab"> <span class="prevp">Prev </span></a> ';
			$ne = '<a href="'.$link.''.$keyword.'"&pn="'.$previous.'">Previous</a>  &nbsp; ';
			// Render clickable number links that should appear on the left of the target page number
	
			 
		}else {
			$paginationCtrls .= "<span class='disablep'>Prev </span>  ";
		}
		// Render the target page number, but without it being a link
		$paginationCtrls .= '<span class="current">Page '.$pagenum.'  of '.$last.' </span> ';
		// Render clickable number links that should appear on the right of the target page number
	
		// This does the same as above, only checking if we are on the last page, and then generating the "Next"
		if ($pagenum != $last) {
			$next = $pagenum + 1;
			$paginationCtrls .= '<a href="'.$link.''.$keyword.'&pn='.$next.'#companies-tab"> <span class="nextp">Next </span></a> ';
		}
	
		else {
			$paginationCtrls .= "<span class='disablep'> Next </span>";
		}
	
	
		if ($pagenum == $last) {
			 
			$paginationCtrls .= "<span class='disablep'>Last </span>";
		}
	
		else {
			 
			$paginationCtrls .= '<a href="'.$link.''.$keyword.'&pn='.$last.'#companies-tab"><span class="last">Last </span></a> ';
		}
	
	}
	$returnData['pagination'] = trim($paginationCtrls);
	return $returnData;
	}
	
	
	
	
	
	
	
/*	$returnData[] = '';
    
	// This is the number of results we want displayed per page
	$page_rows = 2;
	// This tells us the page number of our last page
	$last = ceil($rows/$page_rows);
	// This makes sure $last cannot be less than 1
	if($last < 1){
		$last = 1;
	}
	// Establish the $pagenum variable
	$pagenum = 1;
	// Get pagenum from URL vars if it is present, else it is = 1
	if(isset($_GET['pn'])){
		$pagenum = preg_replace('#[^0-9]#', '', $_GET['pn']);
	}
	// This makes sure the page number isn't below 1, or more than our $last page
	if ($pagenum < 1) {
		$pagenum = 1;
	} else if ($pagenum > $last) {
		$pagenum = $last;
	}
	// This sets the range of rows to query for the chosen $pagenum
	$limit = 'LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;
	// This is your query again, it is for grabbing just one page worth of rows by applying $limit

	$returnData['limit'] = $limit;

	// Establish the $paginationCtrls variable
	$paginationCtrls = '';
	// If there is more than 1 page worth of results
	if($last != 1){
		/* First we check if we are on page one. If we are then we don't need a link to
		 the previous page or the first page so we do nothing. If we aren't then we
		generate links to the first page, and to the previous page. 
	
		if ($pagenum > 1) {
			$previous = $pagenum - 1;
			$paginationCtrls .= '<a href="'.$link.''.$keyword.'&pn='.$previous.'#companies-tab";">Previous</a> &nbsp; &nbsp; ';
			$ne = '<a href="'.$link.''.$keyword.'"&pn="'.$previous.'#companies-tab">Previous</a>  &nbsp; ';
			// Render clickable number links that should appear on the left of the target page number
			for($i = $pagenum-4; $i < $pagenum; $i++){
				if($i > 0){
					$paginationCtrls .= '<a href="'.$link.''.$keyword.'&pn='.$i.'#companies-tab">'.$i.'</a> &nbsp; ';
				}
			}
			 
		}else {
			$paginationCtrls = "Previous &nbsp;&nbsp;  ";
		}
		// Render the target page number, but without it being a link
		$paginationCtrls .= ''.$pagenum.' &nbsp; ';
		// Render clickable number links that should appear on the right of the target page number
		for($i = $pagenum+1; $i <= $last; $i++){
			$paginationCtrls .= '<a href="'.$link.''.$keyword.'&pn='.$i.'#companies-tab">'.$i.'</a> &nbsp; ';
			if($i >= $pagenum+4){
				break;
			}
		}
		// This does the same as above, only checking if we are on the last page, and then generating the "Next"
		if ($pagenum != $last) {
			$next = $pagenum + 1;
			$paginationCtrls .= ' &nbsp;  <a href="'.$link.''.$keyword.'&pn='.$next.'#companies-tab">Next</a> ';
		}

		else {
			$paginationCtrls .= "&nbsp; Next";
		}

	}
	
	$paginationCtrls = '</div>';
	$returnData['pagination'] = trim($paginationCtrls);
	return $returnData;
}



?>