<?php
 
/*
* File: SimpleImage.php
* Author: Simon Jarvis
* Copyright: 2006 Simon Jarvis
* Date: 08/11/06
* Link: http://www.white-hat-web-design.co.uk/articles/php-image-resizing.php
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details:
* http://www.gnu.org/licenses/gpl.html
*
*/

class SimpleImage
{
   var $image;
   var $image_type;
   var $m_blnIsImage;
   public static $FILE_SIZE_IMAGE =  2000;
   public static $FILE_SIZE_VIDEO = 10000;

   function __construct($blnIsImage = TRUE)
   {
      $this->m_blnIsImage = $m_blnIsImage;
   }
   
   function load($filename)
   {
      if( $m_blnIsImage )
      {
	 $this->loadImage($filename);
      }
      else
      {
	 $this->loadVideo($filename);
      }
   }
   
   function save($filename, $image_type=IMAGETYPE_JPEG, $compression=75, $permissions=null)
   {
      if( $m_blnIsImage )
      {
	 $this->saveImage($filename, $image_type, $compression, $permissions);
      }
      else
      {
	 $this->saveVideo($filename);
      }
   }
   
   function output($image_type=IMAGETYPE_JPEG)
   {
      if( $m_blnIsImage )
      {
	 $this->outputImage($image_type);
      }
      else
      {
      }
   }

   function getWidth()
   {
      if( $m_blnIsImage )
      {
	 $this->getImageWidth();
      }
      else
      {
      }
   }

   function getHeight()
   {
      if( $m_blnIsImage )
      {
	 $this->getImageHeight();
      }
      else
      {
      }
   }

   function resizeToHeight($height)
   {
      if( $m_blnIsImage )
      {
	 $this->resizeImageToHeight($height);
      }
      else
      {
      }
   }

   function resizeToWidth($width)
   {
      if( $m_blnIsImage )
      {
	 $this->resizeImageToWidth($width);
      }
      else
      {
      }
   }

   function scale($scale)
   {
      if( $m_blnIsImage )
      {
	 $this->scaleImage($scale);
      }
      else
      {
      }
   }

   function resize($width,$height)
   {
      if( $m_blnIsImage )
      {
	 $this->resizeImage($width, $height);
      }
      else
      {
      }
   }

   function loadVideo($filename)
   {
      $this->image = $filename;
   }

   function saveVideo($filename, $permissions=null)
   {
      if( !move_uploaded_file($this->image, $filename) )
      {
	 return false;
      }

      $this->image = $filename;

      if( $permissions != null)
      {
         chmod($filename,$permissions);
      }

      return true;
   }

   function loadImage($filename)
   {
      $image_info = getimagesize($filename);

      $this->image_type = $image_info[2];

      if( $this->image_type == IMAGETYPE_JPEG )
      {
         $this->image = imagecreatefromjpeg($filename);
      }
      elseif( $this->image_type == IMAGETYPE_GIF )
      {
         $this->image = imagecreatefromgif($filename);
      }
      elseif( $this->image_type == IMAGETYPE_PNG )
      {
         $this->image = imagecreatefrompng($filename);
      }
   }

   function saveImage($filename, $image_type=IMAGETYPE_JPEG, $compression=75, $permissions=null)
   {
      if( $image_type == IMAGETYPE_JPEG )
      {
         imagejpeg($this->image,$filename,$compression);
      }
      elseif( $image_type == IMAGETYPE_GIF )
      {
         imagegif($this->image,$filename);
      }
      elseif( $image_type == IMAGETYPE_PNG )
      {
      	  imagepng($this->image,$filename);
      }

      if( $permissions != null)
      {
         chmod($filename,$permissions);
      }

      return true;
   }

   function outputImage($image_type=IMAGETYPE_JPEG)
   {
      if( $image_type == IMAGETYPE_JPEG )
      {
         imagejpeg($this->image);
      }
      elseif( $image_type == IMAGETYPE_GIF )
      {
         imagegif($this->image);
      }
      elseif( $image_type == IMAGETYPE_PNG )
      {
      	imagepng($this->image);
      }
   }

   function getImageWidth()
   {
      return imagesx($this->image);
   }

   function getImageHeight()
   {
      return imagesy($this->image);
   }

   function resizeImageToHeight($height)
   {
      $ratio = $height / $this->getImageHeight();
      $width = $this->getImageWidth() * $ratio;
      $this->resizeImage($width,$height);
   }
 
   function resizeImageToWidth($width)
   {
      $ratio = $width / $this->getImageWidth();
      $height = $this->getImageHeight() * $ratio;
      $this->resizeImage($width,$height);
   }
 
   function scaleImage($scale)
   {
      $width = $this->getWidth() * $scale/100;
      $height = $this->getheight() * $scale/100;
      $this->resizeImage($width,$height);
   }
 
   function resizeImage($width,$height)
   {
      $new_image = imagecreatetruecolor($width, $height);
      imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getImageWidth(), $this->getImageHeight());
      $this->image = $new_image;
   }      
 
}
?>