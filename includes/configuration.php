<?php
if(!@session_start()){
	@session_start();
}
	$user_ip= $_SERVER['REMOTE_ADDR'];

	error_reporting(0);
	ini_set('display_errors',0);

	include('connection.php');
	include('functions.php');
	include('DX_Mailer.php');
	include('path.php');
	include('SimpleImage.php');
	include('timezone.php');
	include('email-setting.php');
	include('pagination-new.php');

	extract($_POST);


	$row_site_title				= get_record_on_id('settings',1);
	$row_site_meta_keywords		= get_record_on_id('settings',2);
	$row_site_meta_description	= get_record_on_id('settings',3);
	$records_per_page_front		= get_record_on_id('settings',4);
	$facebook_page_link 		= get_record_on_id('settings',7);
	$twitter_page_link 			= get_record_on_id('settings',8);
	$pinterest_page_link 		= get_record_on_id('settings',15);
	$tumblr_page_link 			= get_record_on_id('settings',16);
	$google_plus_page_link		= get_record_on_id('settings',9);
	$homepage_users_list		= get_record_on_id('settings',10);
	$search_results_limit 		= get_record_on_id('settings',11);
	$show_our_team 				= get_record_on_id('settings',17);
	$show_how_we_growing 	= get_record_on_id('settings',18);
	


	$reload = '';
	//=====================
	$url_p	= $_SERVER['REQUEST_URI'];
	$url_p	= preg_split('/',$url_p);
	if(isset($_GET['slug']) && preg_match('/how-to/', $_SERVER['REQUEST_URI'])){
		$page 	= trim($url_p[2]);
	}elseif(isset($_GET['slug']) && preg_match('/cricket-club/', $_SERVER['REQUEST_URI'])){
		$page 	= trim($url_p[2]);
	}
	else{
		$page 	= trim($url_p[PAGE_TOKEN]);
	}
	if(count($url_p) > 0 && $page == ''){
		$page = trim($url_p[count($url_p)]);
	}
	/****************************************/
	define('PRODUCT_LIMIT_FRONT',$records_per_page_front['value'],true);
	define('HOMEPAGE_USERS_LIMIT', $homepage_users_list['value'],true);
	define('DELETED_STATUS', '8', true);
	define('SEARCH_RESULTS_LIMIT', $search_results_limit['value'],true);

  /* paypal details */

	define('PAYPAL_URL', 'https://www.sandbox.paypal.com/cgi-bin/webscr');

	define('PAYPAL_EMAIL', 'shujakhan2000-facilitator@hotmail.com');//

	define('SITE_URL', 'http://dev1.youcricketer.com');

	/****************************************/
	/* get title and other information from dynamic records */
	/****************************************/
	//form submit for meta information
	if(isset($_POST['action']) && $_POST['action'] == 'metasubmit'){

		$meta_post_title		= $_POST['page_title'];
		$meta_post_keywords		= $_POST['page_meta_keywords'];
		$meta_post_description	= $_POST['page_meta_description'];
		$canonical_tag 			= $_POST['canonical_tag'];

		if(empty($page)){
			$meta_page 			= 'index';
		}else{
			$meta_page			= $page;
		}
		$rs_chk = mysqli_query($conn,"select * from seotags where page_link = '$meta_page' ");
		$logged_in_user = $_SESSION['fft_user_id'];
		if(mysqli_num_rows($rs_chk) > 0){
			$row_meta = mysqli_fetch_assoc($rs_chk);
			$query 	= "update seotags set canonical_tag = '$canonical_tag', page_title = '$meta_post_title', meta_keywords = '$meta_post_keywords', meta_description = '$meta_post_description' where id = '".$row_meta['id']."'";
		}else{
			$query 	= "insert into seotags (created_by, page_link, page_title, meta_keywords, meta_description, canonical_tag)
			values ('$logged_in_user','$meta_page','$meta_post_title','$meta_post_keywords','$meta_post_description', '$canonical_tag')";
		}
		if(mysqli_query($conn,$query)){
			$meta_msg = '<div id="success">Meta information updated ... !</div>';
		}else{
			$meta_msg = '<div id="error">Meta information cannot be updated ... !</div>';
		}

	}
	/****************************************/
	//get info from meta info table
	if(empty($page)){
		$meta_page 			= 'index';
	}else{
		$meta_page			= $page;
	}
	$rs_chk 	= mysqli_query($conn,"select * from seotags where page_link = '$meta_page'");
	if(mysqli_num_rows($rs_chk) > 0){
		$row_meta_info 		= mysqli_fetch_assoc($rs_chk);
		$pageDescription 	= $row_meta_info['meta_description'];
		$pageKeywords		= $row_meta_info['meta_keywords'];
		$pageTitle			= $row_meta_info['page_title'];
		$canonical_tag 		= $row_meta_info['canonical_tag'];
	}else{
		if(!$_GET){
			$pageTitle 		= 	cleanForBreadcrumb($page);
			$pageKeywords 	= 	cleanForBreadcrumb($page);
			$pageDescription= 	cleanForBreadcrumb($page);
		}else if(isset($_GET['entity'])){
			$pageTitle 		= ucwords($_GET['entity']).' (Companies)';
			$pageKeywords 	= ucwords($_GET['entity']).' (Companies)';
			$pageDescription= ucwords($_GET['entity']).' (Companies)';
		}else if(isset($_GET['id']) && preg_match('/detail/', $page)){
			if(preg_match('/individual/', $page)){
				$row 			= get_record_on_id('users', $_GET['id']);
				$pageTitle 		= ucwords($row['f_name']).' '.ucwords($row['last_name']).' (Individuals)';
				$pageKeywords 	= ucwords($row['f_name']).' '.ucwords($row['last_name']).' (Individuals)';
				$pageDescription= ucwords($row['f_name']).' '.ucwords($row['last_name']).' (Individuals)';
			}
			if(preg_match('/company/', $page)){
				$row 			= get_record_on_id('companies', $_GET['id']);
				$rowCompanyType = get_record_on_id('company_types', $row['company_type_id']);
				$pageTitle 		= $row['company_name'].' - '.ucwords($rowCompanyType['name']).' (Companies)';
				$pageKeywords 	= $row['company_name'].' - '.ucwords($rowCompanyType['name']).' (Companies)';
				$pageDescription= $row['company_name'].' - '.ucwords($rowCompanyType['name']).' (Companies)';
			}
			if(preg_match('/press-release/', $page)){
				$row 			= get_record_on_id('press_releases', $_GET['id']);
				$pageTitle 		= $row['title'];
				$pageKeywords 	= $row['title'];
				$pageDescription= $row['title'].'.'.$row['content'];
			}
			if(preg_match('/blog/', $page)){
				$row 			= get_record_on_id('blog_articles', $_GET['id']);
				$pageTitle 		= $row['title'].' - Blog (Community)';
				$pageKeywords 	= $row['title'].' - Blog (Community)';
				$pageDescription= $row['title'].' - Blog (Community).'.$row['content'];
			}
			if(preg_match('/forum/', $page)){
				$row 			= get_record_on_id('forum_topics', $_GET['id']);
				$pageTitle 		= $row['title'].' - Forum (Community)';
				$pageKeywords 	= $row['title'].' - Forum (Community)';
				$pageDescription= $row['title'].' - Forum (Community).'.$row['content'];
			}
			if(preg_match('/group/', $page)){
				$row 			= get_record_on_id('groups', $_GET['id']);
				$pageTitle 		= $row['title'].' - Groups (Community)';
				$pageKeywords 	= $row['title'].' - Groups (Community)';
				$pageDescription= $row['title'].' - Groups (Community).'.$row['content'];
			}
			if(preg_match('/news/', $page)){
				$row 			= get_record_on_id('news', $_GET['id']);
				$pageTitle 		= $row['title'].' - News (Community)';
				$pageKeywords 	= $row['title'].' - News (Community)';
				$pageDescription= $row['title'].' - News (Community).'.$row['content'];
			}
			if(preg_match('/event/', $page)){
				$row 			= get_record_on_id('events', $_GET['id']);
				$pageTitle 		= $row['title'].' - Events (Community)';
				$pageKeywords 	= $row['title'].' - Events (Community)';
				$pageDescription= $row['title'].' - Events (Community)';//.$row['content']
			}
		}else if(isset($_GET['id']) && preg_match('/category/', $page)){
			if(preg_match('/blog-category/', $page)){
				$row 			= get_record_on_id('blog_categories', $_GET['id']);
				$pageTitle 		= $row['name'].' - Blog (Community)';
				$pageKeywords 	= $row['name'].' - Blog (Community)';
				$pageDescription= $row['name'].' - Blog (Community).'.$row['description'];
			}
			if(preg_match('/forum-category/', $page)){
				$row 			= get_record_on_id('forum_categories', $_GET['id']);
				$pageTitle 		= $row['name'].' - Forum (Community)';
				$pageKeywords 	= $row['name'].' - Forum (Community)';
				$pageDescription= $row['name'].' - Forum (Community).'.$row['description'];
			}
		}else if(preg_match('/(.*).html(.*)/', $page, $matches)){
			$pageTitle 		= ucwords(str_replace('-', ' ', $matches[1]));
			$pageKeywords 	= ucwords(str_replace('-', ' ', $matches[1]));
			$pageDescription= ucwords(str_replace('-', ' ', $matches[1]));
		}
		if(isset($_GET['type'])
		&& $_GET['type'] == 'your_region'){
			preg_match('/your-region-(.*).html/', $page, $matches);
			$pageTitle 		= 'In Your Region - '.ucwords($matches[1]);
			$pageKeywords 	= 'In Your Region - '.ucwords($matches[1]);
			$pageDescription= 'In Your Region - '.ucwords($matches[1]);
			if($matches[1] == 'clubs'
			|| $matches[1] == 'leagues'){
				$pageTitle 		.= ' (Companies)';
				$pageKeywords 	.= ' (Companies)';
				$pageDescription.= ' (Companies)';
			}
		}
	}
	/****************************************/

	if(!empty($pageTitle)){
		$site_title			= $pageTitle 		/*.' - '. $row_site_title['value']*/;
		$meta_keywords		= $pageKeywords 	/*.' - '. $row_site_meta_keywords['value']*/;
		$meta_description	= $pageDescription  /*.' - '. $row_site_meta_description['value']*/;
	}else{
		$site_title			= $row_site_title['value'];
		$meta_keywords		= $row_site_meta_keywords['value'];
		$meta_description	= $row_site_meta_description['value'];
	}

	/****************************************/
	if($user_ip == '127.0.0.1'){
		$user_ip = '223.29.230.234';
	}
	/****************************************/

	if($page == ''){
		$top_menu['home'] = 'nav_selected';
	}else if(preg_match('/about-us|who-we-are|our-mission|our-team|our-philosphy/', $page)){
		$top_menu['about_us'] = 'nav_selected';
	}else if(preg_match('/press-releases/', $page)){
		$top_menu['press_releases'] = 'nav_selected';
	}else if(preg_match('/how-to/', $page)){
		$top_menu['how_to'] = 'nav_selected';
	}else if(preg_match('/your-region-clubs|your-region-blog|your-region-forum|your-region-groups|your-region-testimonials|your-region-news|your-region-leagues|your-region-events/', $page)){
		$top_menu['in_your_region'] = 'nav_selected';
	}else if(preg_match('/clubs|blog|community|forum|groups|testimonials|news|leagues|events/', $page)){
		$top_menu['community'] = 'nav_selected';
	}else if(preg_match('/top-players/', $page)){
		$top_menu['top_players'] = 'nav_selected';
	}else if(preg_match('/in-your-region/', $page)){
		$top_menu['in_your_region'] = 'nav_selected';
	}else if(preg_match('/faqs/', $page)){
		$top_menu['faqs'] = 'nav_selected';
	}else if(preg_match('/contact-us/', $page)){
		$top_menu['contact_us'] = 'nav_selected';
	}else if(preg_match('/Login/', $page)){
		$top_menu['login'] = 'nav_selected';
	}
?>
