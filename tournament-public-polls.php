<?php include_once('includes/configuration.php');
$page = 'tournament-public-polls.html';
$selected_country = getGeoLocationCountry();

$league_clubs = array();
$league_info = array();
$teams = array();
$error = '';

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);
}

$tournament_id = trim($_GET['tour_id']);
$tournament_info = get_record_on_id('tournaments', $tournament_id);
$poll_str = '';

$page_title = ucwords($tournament_info['title']).' Polls';

$sql = "SELECT * FROM tournament_polls WHERE tournament_id=$tournament_id and is_active=1 ORDER BY id DESC ";
$rs_polls = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_polls)){
	$poll_str.='<div class="poll-div" id="poll_div_'.$row['id'].'"><div class="poll_question">'.$row['poll_question'].'</div><div class="clear"></div>';
	for($i=1;$i<=6;$i++){
		if(!empty($row['poll_option_'.$i])){
			$poll_str.='<div class="poll_option"><input type="radio" value="'.$i.'" name="poll_option_'.$row['id'].'" id="poll_option_'.$row['id'].'_'.$i.'">'.$row['poll_option_'.$i].'</div><div class="clear"></div>';
		}
	}

	$poll_str.='<div class="clear" style="height:5px;"></div><input type="button" onClick="submitPoll('.$row['id'].')" value="Submit" class="submit-login" name="poll_submit_'.$row['id'].'" id="poll_submit_'.$row['id'].'">';
	$poll_str.='<div class="clear"></div> </div>';
	$poll_str.='<div class="clear"><br/></div>';
}
?>
            <div class="row">
              <div class="col-sm-12">
                <h2> Tournaments Polls - <?php echo $tournament_info['title']; ?> </h2>

              </div>
            </div>

            <div class="row">
              <div class="col-sm-12">
                <?php echo $poll_str; ?>
				 <?php if(empty($poll_str)): ?>
					<div id="information" class="alert alert-info">No Records</div>
				 <?php endif; ?>
              </div>
            </div>


          </div>



<script type="text/javascript">
$(document).ready(function(){
	submitPoll = function(id){
		if(!$("input[name=poll_option_"+id+"]").is(":checked")){
			alert('Select one poll option');return;
		}

		var option_id = $('input[name=poll_option_'+id+']:checked').val();
		$.ajax({
			url: "<?php echo WWW; ?>get_polls.php?t="+new Date().getTime(),
			type:"POST",
			data:"poll_id="+id+"&option_id="+option_id+"&action=submit",
			success: function(data) {
				data = $.parseJSON(data);
				if(data.error == ''){
					$("#poll_div_"+id).html(data.str);
				}
			}
		});
	}

});
</script>
