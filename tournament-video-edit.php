<?php include_once('includes/configuration.php');
$page = 'tournament-video-add.html';
$selected_country = getGeoLocationCountry(); 

$error = '';
$tournament_permission = 0;
$match_info = array();
$match_id = trim($_GET['m_id']);
$match_info = get_record_on_id('tournament_matches', $match_id);	
$tournament_id = $match_info['tournament_id'];
$tournament_info = get_record_on_id('tournaments', $tournament_id);	
$video_id = trim($_GET['v_id']);
$video_info = get_record_on_id('tournament_match_videos', $video_id);	

$page_title = 'Edit Video - '.ucwords($video_info['video_title']);

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
}

if(isset($user_id) && !empty($user_id)){
	//$sql = "select * from tournament_permissions where tournament_id = $tournament_id and member_id = $user_id";
	//$rs_permission = mysqli_query($conn,$sql);
	if($tournament_info['user_id'] == $user_id){
		$tournament_permission = 1;
	}else{
		$tournament_permission = 0;
	}
}

if(isset($_POST['submit_btn']) && !empty($_POST['submit_btn'])){
	validate();
	if(empty($error)){
		$sql = " UPDATE tournament_match_videos SET video_title='".trim($_POST['video_title'])."' WHERE id=".$video_id;
		
		if(mysqli_query($conn,$sql)){
			$_SESSION['video_added'] = 1;
			header("Location:".WWW."tournament/matches/videos/list/".$match_id);
			exit();
		}else{
			$error = '<p id="error">Error in updating Video. Try again later</p>';
		}
	}
}

function validate(){
	global $error;
	if(empty($_POST['video_title'])){
		$error.= '<p id="error">Video Title is required field</p>';
	}
	
}

?>
<?php include('common/header.php'); ?>
<style>
.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
dl {min-height:145px;}
.content {width:1010px;}
.large-column {width:790px;}
.form-box {width:70%;}
.chk_left {float:left !important; }
#video_title,#video_url {width:410px;}
</style>
	<div class="middle">
		<h1> Edit Video </h1>
		<h2><?php echo ucwords($tournament_info['title']); ?></h2>
		
		<div class="white-box content" id="dashboard">
			<?php if(empty($user_info)): ?>
				<div id="error">You are not logged... !</div>
			<?php endif; ?>
			
			<?php if(!empty($error)): ?>
				<?php echo $error; ?>
			<?php endif; ?>
		
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
				
				<?php if($tournament_permission){ ?>
				<form method="post"  enctype="multipart/form-data">
					<input type="hidden" name="action" value="submit">
					<fieldset>
						<h2>Video Details</h2>
						<p></p>
						<div class="form-box">
							<label>Video Title</label>
							<div class="text"><input type="text" name="video_title" id="video_title" class="input-login" value="<?php if(!empty($_POST['video_title'])) echo $_POST['video_title'];else echo $video_info['video_title']; ?>"></div>
						</div>
						
						<div class="form-box">
							<?php if($video_info['video_type'] == 'embed'): ?>
								<?php $video_id = substr($video_info['video_url'],strpos($video_info['video_url'],'?v=')+3);?>
								<iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $video_id; ?>" frameborder="0" allowfullscreen></iframe>
							<?php else: ?>
							<?php $video_path = WWW.'video-uploads/'.$video_info['video_file']; ?>
									<video width="540" height="300" controls>
									  <source src="<?php echo $video_path; ?>" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
									  <source src="<?php echo $video_path; ?>" type='video/ogg; codecs="theora, vorbis"'>
									  <source src="<?php echo $video_path; ?>" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
									  <source src="<?php echo $video_path; ?>" type='video/webm; codecs="vp8, vorbis"'>
									</video>
							<?php endif; ?>
						</div>
						
						<div class="clear"></div>
						<div class="form-box">
							<input type="button" name="cancel_btn" value=" Cancel " class="submit-login" onclick="window.location.href='<?php echo WWW; ?>tournament/matches/videos/list/<?php echo $match_id; ?>';">
							<input type="submit" name="submit_btn" value=" Submit " class="submit-login" >
						</div>
					</fieldset>
				</form>
				<?php }else{ ?>
				<div id="error">You do not have Permission for this Tournament... !</div>
				<?php } ?>
			</div>  
		    <div class="clear"></div>
			</div>
		</div>
		<!--<div class="rightbar"><?php //include('common/right-panel.php');?></div>-->
		<div class="clear"></div>
	</div>
	


<?php include('common/footer.php'); ?>