<?php include_once('includes/configuration.php');
$page = 'club-members-search.html';
$selected_country = getGeoLocationCountry(); 

$countries = array();
$rs_countries = mysqli_query($conn,"select id,name from countries where status='1'");
while($row = mysqli_fetch_assoc($rs_countries)){
	$countries[] = $row;
}

/*$leagues = array();
$rs_clubs = mysqli_query($conn,"select id,title from leagues where status='1' order by title");
while($row = mysqli_fetch_assoc($rs_clubs)){
	$leagues[] = $row;
}*/

$ppage = intval($_GET["page"]);
if($ppage<=0) $ppage = 1;

$league_info = array();
$user_info  = array();
$league_clubs = array();

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
	if($user_info['is_company'] == 0){
		$sql = 'select * from club_members where member_id = '.$user_id;
		$rs_club = mysqli_query($conn,$sql);
		while($row = mysqli_fetch_assoc($rs_club)){
			$member_clubs[] = $row['club_id'];
		}
	}
}



if(isset($_POST['clubs_add_submit']) && !empty($_POST['clubs_add_submit'])){
	$clubs = $_POST['club_ids'];
	if(!empty($clubs)){
		for($i=0;$i<count($clubs);$i++){
			$sql = "INSERT into club_members SET member_id=".$user_id.", club_id=".$clubs[$i];
			mysqli_query($conn,$sql);
		}
	}
	
	$_SESSION['member_club_added'] = 1;
	
	if(isset($_GET['keywords']) && !empty($_GET['keywords'])){
		$url_q.='-'.$_GET['keywords'];
	}else{
		$url_q.='-all';
	}
	if(isset($_GET['league_id']) && !empty($_GET['league_id']) ){
		$url_q.='-'.$_GET['league_id'];
	}else{
		$url_q.='-0';
	}
	if(isset($_GET['country_id']) && !empty($_GET['country_id']) ){
		$url_q.='-'.$_GET['country_id'];
	}else{
		$url_q.='-0';
	}
	
	header("Location:".WWW."member-add-club{$url_q}-".$ppage.'.html');
	exit;
	//echo '<script>window.location.href="'.WWW.'club-members-search-all-'.$ppage.'.html"</script>';
}

if(isset($_SESSION['member_club_added']) && $_SESSION['member_club_added']==1) {
	$club_added = 1;
	unset($_SESSION['member_club_added']);
}

if(isset($_GET['league_id']) && !empty($_GET['league_id'])){
	
	$query = "select title from leagues where id =".$_GET['league_id'];	
	$rs   = mysqli_query($conn,$query);
	if(mysqli_num_rows($rs)){
		$league_arr = mysqli_fetch_assoc($rs);
		$league_name = $league_arr['title'];
	}
}

?>
<?php include('common/header.php'); ?>
<style>
.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
dl {min-height:145px;}
.content {width:1010px;}
.large-column {width:790px;}
</style>
	<div class="middle">
		<h1> Add Clubs </h1>
		<?php if(isset($club_added) && $club_added == 1): ?>
			<div id="information">Clubs added Successfully... !</div>
		<?php  endif; ?>
		
		<?php if(empty($user_info)): ?>
			<div id="error">You are not logged... !</div>
		<?php endif; ?>
		<?php /*if(!empty($user_info) && empty($league_info) ): ?>
			<div id="error">You are not logged as League Owner.. !</div>
		<?php endif;*/ ?>
		<div class="white-box content" id="dashboard">
			<div class="small-column">
				<? include('common/user-left-panel.php'); ?>
			</div>
			<div class="large-column">
			<div id="pagination-top"></div>
			
			<div id="adv_search">
			<div class="clear"><br></div>
			<h2> Advanced Search </h2>
			
			<form id="advance_search_form" method="post" action=""> 
			
			<div id="auto_suggest" style="float:left;">
				<div class="header_search"><input type="text" class="search_box" id="search_box" name="league_name" <?php if(isset($league_name) && !empty($league_name)){?> value="<?php echo $league_name; ?>" <?php } else{ ?> placeholder="League Name" <?php } ?> autocomplete="off"></div>
				<div style="clear:both"></div>
				<div id="searchres" class="searchres"></div>
			</div>	
			
			<div id="country_dropdopwn" style="float:left;">
				<select name="country_id_srch" id="country_id_srch" style="font-size:14px;">
					<option value="0">Country</option>
					<?php for($i=0;$i<count($countries);$i++): ?>
						<?php if(isset($_GET['country_id']) && $_GET['country_id'] == $countries[$i]['id']) $sel = 'selected';else $sel = ''; ?>
						<option <?php echo $sel; ?> value="<?php echo $countries[$i]['id']; ?>"><?php echo $countries[$i]['name']; ?></option>
					<?php endfor; ?>
				</select>	
			</div>	
			
			<div id="club_name_div" style="float:left;margin-left:10px;">
			<input type="text" name="club_name_srch" id="club_name_srch" <?php if(isset($_GET['keywords']) && !empty($_GET['keywords'])): ?> value="<?php echo $_GET['keywords']; ?>" <?php else: ?>  placeholder="Club Name" <?php endif; ?> style="height:30px;">
			</div>	
			<input type="hidden" name="league_id" id="league_id"  value="<?php if(isset($_GET['league_id']) && !empty($_GET['league_id'])) echo $_GET['league_id']; ?>" >
			<div style="clear:both"><br></div>
			
			<input type="submit" name="submit_adv_search" id="submit_adv_search" value="Search">
			
			</form>
			
			<?php /* ?>
			<form id="advance_search_frm" method="POST" action=""> 
				
				<input type="text" name="club_name_srch" id="club_name_srch" <?php if(isset($_GET['keywords']) && !empty($_GET['keywords'])): ?> value="<?php echo $_GET['keywords']; ?>" <?php else: ?>  placeholder="Club Name" <?php endif; ?> style="height:30px;">
				<select name="country_id_srch" id="country_id_srch" style="font-size:14px;">
					<option value="0">Country</option>
					<?php for($i=0;$i<count($countries);$i++): ?>
						<?php if(isset($_GET['country_id']) && $_GET['country_id'] == $countries[$i]['id']) $sel = 'selected';else $sel = ''; ?>
						<option <?php echo $sel; ?> value="<?php echo $countries[$i]['id']; ?>"><?php echo $countries[$i]['name']; ?></option>
					<?php endfor; ?>
				</select>
				<select name="league_id_srch" id="league_id_srch" style="width:235px;font-size:14px;">
					<option value="0">League</option>
					<?php for($i=0;$i<count($leagues);$i++): ?>
						<?php if(isset($_GET['league_id']) && $_GET['league_id'] == $leagues[$i]['id']) $sel = 'selected';else $sel = ''; ?>
						<option <?php echo $sel; ?> value="<?php echo $leagues[$i]['id']; ?>"><?php echo $leagues[$i]['title']; ?></option>
					<?php endfor; ?>
				</select>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="submit" name="submit_adv_search" id="submit_adv_search" value="Search">
			</form>
			
			<?php */ ?>
			<div class="clear"><br></div>
			</div>
			
			<?php if(!empty($_GET['keywords']) || !empty($_GET['league_id']) || !empty($_GET['country_id'])){ ?>
			
			<form method="post">
			<div class="list">
                <ul id="individual" class="content1">
                	<?  
						
                		$rpp = PRODUCT_LIMIT_FRONT; // results per page
						$league_id = 0;
                		$country_id = 0;
						
      					$query = "select distinct u.*,c.company_name,c.years_in_business,c.id as company_id from users u inner join companies as c on u.id=c.user_id ";
						$query_count = "select count(*) as users_count from users u inner join companies as c on u.id=c.user_id";
				      	//=======================================
						$where = " where c.company_type_id = '4'  and u.status = 1";
						if(isset($_GET['league_id']) && !empty($_GET['league_id'])){
							$league_id = trim($_GET['league_id']);
							$join  = " INNER JOIN users_to_leagues as l ON u.id=l.user_id ";
							$query.=$join;
							$query_count.=$join;
							$where.=" AND l.league_id = ".$league_id;
						}
						
						if(isset($_GET['country_id']) && !empty($_GET['country_id'])){
							$country_id = trim($_GET['country_id']);
							$where.=" AND u.country_id = ".$country_id;
						}
						
					    if(isset($_GET['keywords']) && $_GET['keywords'] != 'Search Here' && $_GET['keywords'] != 'all' && !empty($_GET['keywords'])){
							$keywords = trim($_GET['keywords']);
							$where  .= " and (c.company_name like '%{$keywords}%' ) ";
						}else if(isset($_GET['club_name_srch']) && $_GET['club_name_srch'] != 'Club Name' && $_GET['club_name_srch'] != 'all' && !empty($_GET['club_name_srch']) ){
							$keywords = trim($_GET['club_name_srch']);
							$where  .= " and (c.company_name like '%{$keywords}%' ) ";
						}else{
							$keywords = '';
						}
						
						$query.=$where;
						$query_count.=$where;
					    $query .= " order by u.id desc ";
				    
						$rs_count   = mysqli_query($conn,$query_count);
						$row_count  = mysqli_fetch_assoc($rs_count);
						$tcount = $row_count['users_count'];
					  
						$tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
						$count = 0;
						$start = ($ppage-1)* $rpp;
						$x = 0;
					  
						$query .= " LIMIT $start,$rpp ";
						//echo $query;
						$rs   = mysqli_query($conn,$query);
						if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
							echo '<div id="information">No record found ... !</div>';
						}
          			 
						$i= 0 ;
						while($row 	= mysqli_fetch_assoc($rs)){
                		

                		//$row_img = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row['id']."' and is_default = '1' "));
			  				$row_country= get_record_on_id('countries', $row['country_id']);	
			  				$location 	= $row_country['name'];
			  				//$row_vid = mysqli_fetch_assoc(mysqli_query($conn,"select * from videos where entity_id = '".$row['id']."' and entity_type = 'users' and is_default = '1' and file_name != ''"));
							
							$str_league = '';
							$sql = "select c.company_name from league_clubs as lc  inner join companies as c on c.id=lc.league_id WHERE lc.league_id != ".$league_info['id']." and lc.club_id=".$row['id']." and c.company_type_id = 2 and c.status=1";
							$rs_league = mysqli_query($conn,$sql);
							if(mysqli_num_rows($rs_league)){
								while($row_league = mysqli_fetch_assoc($rs_league)){
									$str_league.=" <br/>Member of ".$row_league['company_name'];
								}
								
							}
		        		    ?>
		        		    
						<dl>
								<dt>
									
									<?php /* ?><a href="<?=WWW?>individual-detail-<?=$row['id']?>-<?=friendlyURL($row['f_name'].' '.$row['m_name'].' '.$row['last_name'])?>.html#activity-tab" title="<?=$row['f_name'].' '.$row['m_name'].' '.$row['last_name']?>"><img src="<?php echo WWW;?><?=($row_img && !empty($row_img['file_name']))?'users/'.$row['id'].'/photos/'.$row_img['file_name']:'images/no-photo.jpg'?>" width="120" height="128" /></a><?php */ ?></dt>
								<dd><div class="details">
									<h3><?=truncate_string($row['company_name'],20)?></h3>
									<p><? echo $location;?><br />
									Club<br/>
										
									
									<?php if(!empty($league_info)) { ?>
										<div style="float:left;">
										<?php if(!in_array($row['company_id'],$league_clubs)){ ?>
											<input type="checkbox" name="club_ids[]" id="club_id_<?php echo $i; ?>" value="<?php echo $row['company_id']; ?>"> Add to League
										<?php } else{ ?>
											Already in League
										<?php } ?>		 	
										<?php echo $str_league; ?>
										</div>
									<?php } ?>		 
								
									</p>
									<a class="submit-login margin-top-5" href="<?=WWW?>cricket-club/<?php echo getSlug($row_country['name']).'-'.getSlug($row['company_name']); ?>/<?php echo $row['company_id']; ?>"  title="<?=$row['company_name'].' - '.$site_title?>">View Profile</a></div>
									<div class="flag"><img title="<?=$row_country['name']?>" alt="<?=$row_country['name']?>" src="<?php echo WWW;?>countries/<?=$row_country['flag']?>"  width="25"/></div>
									<div class="video">
										<? /*
										$video = $row_vid['file_name'];
											if(!empty($video)){
						if(preg_match('/<iframe(.*)<\/iframe>/', $row_vid['file_name'])){ 
									
									preg_match('/src="(.*?)"/',$row_vid['file_name'] , $src);
									$src = $src[1];
									$video = "<iframe width='205' height='130' frameborder='0' allowfullscreen 
												src='$src'>
																	</iframe>";
									
									if(!preg_match('/height="(.*)"/', $video)){
										$video = preg_replace('/height="(.*)"/','height="130"',$video);	
									}else{
										$video = preg_replace('/width="(.*)"/','width="220" height="150"',$video);	
									}
						}else{	
												$filename = explode('.',$video);
												$filename1= $filename[0];
												$video = '<video width="220" height="130" controls>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mp4" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.ogg" type=\'video/ogg; codecs="theora, vorbis"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mov" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.webm" type=\'video/webm; codecs="vp8, vorbis"\'>
												</video>';
											}
										}else{
											$video = '<img src="'.WWW.'images/no-video.jpg" width="168" height="130">';
										}
										echo $video; */?>									
									</div>
								</dd>
						</dl>
						<?php
                		
					      $i++;
					      $count++;
					      $x++;
					  } 
					  ?>
			</ul>
			</div><br/>
			<div style="float: left; clear: both; padding: 10px;">
			<input type="submit" name="clubs_add_submit" id="clubs_add_submit" value="Add Clubs"> 
			</div>
			</form>
			
			<?php } ?>
			
			<p>&nbsp;</p><p>&nbsp;</p>
			<div id="pagination-bottom">
				<? if($tcount != 0 && $tcount > PRODUCT_LIMIT_FRONT){ ?>
		      	<?php
		        	$q_url = '';
					if(isset($keywords) && !empty($keywords)){
						$q_url.="-{$keywords}";
					}else{
						$q_url.="-all";
					}
					if(isset($league_id) ){
						$q_url.="-{$league_id}";
					}
					if(isset($country_id) ){
						$q_url.="-{$country_id}";
					}
					
					if(!empty($q_url)){
						$reload = "league-clubs-search{$q_url}.html?";
					}else{
						$reload = "league-clubs-search-all.html?";
					}
		        	echo paginate_one($reload, $ppage, $tpages);
		      	?>
		      	<input type="hidden" name="pagination-page" value="league-clubs-search.html">
		        <? } ?>    
		        <form id="list-search" method="post" action="">
		        	<input type="text" name="txtsearch" placeholder="Search Here" class="validate[required] input-login">
		        	<input type="submit" value="">
		        </form>
				<div class="clear"></div>
		        <script type="text/javascript">
		        $('form#list-search').submit(function(e){
					var parentId = $(this).parent().attr('id');
					var string = $('#'+parentId +' form input[name=txtsearch]').val();
					if(string != '' && string != 'Search Here'){
						string = string.replace(/[^a-zA-Z0-9]+/g,'').toLowerCase();
						if(string.length > 0){
							$('form#list-search').attr('action','<?=WWW;?>league-clubs-search-' + string + '.html');
						}
					}
				});
				
				 $('#submit_adv_search').click(function(e){
				
					var string = $('#club_name_srch').val();
					if(string == ''){
						string = 'all';
					}
					//var league_id = $('#league_id_srch').val();
					if($("#search_box").val()!=''){
						var league_id = $('#league_id').val();
					}else{
						var league_id = 0;
					}
					var country_id = $('#country_id_srch').val();
					//if(string != '' && string != 'Member Name'){
						string = string.replace(/[^a-zA-Z0-9]+/g,'').toLowerCase();
						//if(string.length > 0){
							$('form#advance_search_form').attr('action','<?=WWW;?>league-clubs-search-' + string +"-"+league_id+"-"+country_id+ '.html');
						//}
					//}
				});
		        </script>
				
				<script type="text/javascript">
				$(document).ready(function(){
				$("#search_box").keyup(function(){var search_string = $("#search_box").val();
				if(search_string == ''){$("#searchres").html('');}else{postdata = {'string' : search_string}
				$.post("get_clubs.php?type=league",postdata,function(data){	$("#searchres").html(data);	});}});});
				function fillme(name,id){$("#search_box").val(name);$("#searchres").html(''); $("#league_id").val(id);}
				</script>
				
		        <div class="clear"></div>
		    </div>  
		    <div class="clear"></div>
		</div>
		</div>
		
		<div class="clear"></div>
	</div>
	
<style type="text/css">
.header_search{background-color:none;padding:20 0 5 200px;width:200px;float:left;}
.searchres{margin-left:2px;width:200px;border: #f0f0f0 1px solid;border-radius:5px;position:absolute;z-index:100;background-color:#fff;}.img{float:left;margin: 5 5 5 5px;float:left;}
.search_box{width: 180px;height: 20px;border-radius: 5px;padding: 10px;font-family: verdana;color:black; font-size: 15px;}
.name{margin-top: 14px;}
.user_div{clear:both;font-family: verdana;border-top: #f0f0f0 1px solid;color:black; font-size: 13px;height:20px;margin:1px;padding:7px;width:180px;border-radius:4px;vertical-align: top;  }
.user_div:hover{background:#F6F6F6;color:#fff;cursor:pointer}
.no_data{height: 40px;background-color: #F0F0F0;padding: 10 0 0 10px;width: 180px;border-radius: 5px;font-family: verdana;color:black; font-size: 15px;}
.name{float:left;margin:0 0 0 10px;}.cntry{margin:0 0 0 50px; font-size:13px}
</style>
<?php include('common/footer.php'); ?>