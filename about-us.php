<?php include('common/header.php'); ?>

    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1> About Us </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <div class="white-box">
            <h2>Who We Are</h2>
			<?php $row = get_record_on_id('cms', 39);?>
			<?php echo $row['content']?>
		    <div class="clear"></div>
		    <h2>Our Mission</h2>
			<?php $row = get_record_on_id('cms', 40);?>
			<?php echo $row['content']?>
			<div class="clear"></div>
			<h2>Our Philosphy</h2>
			<?php $row = get_record_on_id('cms', 41);?>
			<?php echo $row['content']?>
			<div class="clear"></div>
			<?php /* ?><h2>Our Team</h2><?php */ ?>
			<?php //include('common/our-team.php');?>
			<div class="clear"></div>
		  </div>
        </div>
        
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
             <?php include('common/right-panel.php');?>
            </div>
          </div>
          
        </div>
      </div>
    </div><!-- /.container -->
	
	
<?php include('common/footer.php'); ?>