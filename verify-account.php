<?php include('common/header.php'); ?>
<?php  if(isset($_SESSION['ycdc_user_name'])
	&& !empty($_SESSION['ycdc_user_name'])){ ?>
		<script>
		window.location = '<?=WWW?>dashboard.html';
		</script>
<?php  }?>


		<?php 	if(isset($_POST) && !empty($_POST) && $_POST['action'] == 'verify-account'){
				$email 	= $_POST['email'];
				$code 	= $_POST['verification_code'];

				$rs_chk	= mysqli_query($conn,"select * from users where email = '$email'");

				if(mysqli_num_rows($rs_chk) == 0){
					echo '<div id="error" class="alert alert-danger">Invalid Email Address ... !</div><br><br>';
				}else{
					$row = mysqli_fetch_assoc($rs_chk);
					$rcount = mysqli_num_rows(mysqli_query($conn, "select status from varifications where user_id=".$row['id']));
					$otstatus = 1;
					if($rcount > 0){
						$rw = get_record_on_id("varifications",$row['id']);
						$otstatus = $rw['status'];
					}
					if($row['status'] == '1' && $otstatus==1){
						echo '<div id="error" class="alert alert-danger">Your Account already activiated ... !</div><br><br>';
					}else if($row['status'] == '0'){
						echo '<div id="error" class="alert alert-danger">Your Account has been deactivated by Administrator ... !</div><br><br>';
					}elseif($row['status'] == '2' || ($row['status'] == '1' && $otstatus==0)){
						if($row['verification_code'] == $_POST['verification_code']){
							$query = "update users set status = '1' where email = '$email'";
							mysqli_query($conn,$query);
							if($rcount > 0){
								mysqli_query($conn, "UPDATE varifications SET status=1 where user_id=".$row['id']);
							}
							$query = "update companies set status = '1' where user_id = ".$row['id'];
							mysqli_query($conn,$query);
						$result =	mysqli_query($conn,"UPDATE invitations SET status = 'Invitation Accepted' WHERE to_emails = '$email' AND accept_status = 1");
						if($result) {
								mysqli_query($conn,"UPDATE invitations SET status = 'Already Joined'   WHERE to_emails = '$email' AND accept_status = 0 ");
						}
							?>
							<script>
							window.location = '<?=WWW?>login.html?status=verified';
							</script>
							<?
						}else{
							echo '<div id="error" class="alert alert-danger">Verification code is not correct ... !</div><br><br>';
						}
					}
				}
			}
			if(isset($_POST) && !empty($_POST) && $_POST['action'] == 'resend-verification'){
				$email 	= $_POST['email'];
				$rs_chk	= mysqli_query($conn,"select * from users where email = '$email'");
				if(mysqli_num_rows($rs_chk) == 0){
					echo '<div id="error" class="alert alert-danger">Invalid Email Address ... !</div><br><br>';
				}else{
					$row_u = mysqli_fetch_assoc($rs_chk);
					$verf = mysqli_fetch_assoc(mysqli_query($conn, "select status from varifications where user_id=".$row_u['id']));
					$versts = 1;
					if(isset($verf['status']) && $verf['status']!=""){
							$versts = $verf['status'];
					}
					if($row_u['status'] == '0'){
						echo '<div id="error" class="alert alert-danger">Your account is disabled by Administrator ... !</div><br><br>';
					}elseif($row_u['status'] == '1' && $versts==1){
						echo '<div id="error" class="alert alert-danger">Your account is already activated, Please use forget password if do not remember your password ... !</div><br><br>';
					}else{
						$verification_code 	= getRandomWord(12);
						mysqli_query($conn,"update users set verification_code = '$verification_code' where email = '$email'");
						// sending email
						$email_template = get_record_on_id('cms', 11);
						$mail_title		= $email_template['title'];
		                $mail_content	= $email_template['content'];
		                $mail_content 	= str_replace('{name}',$row_u['f_name'], $mail_content);
		                $mail_content 	= str_replace('{verification_code}',$verification_code, $mail_content);
		                $mail_content 	= str_replace('{verification_link}','<a href="'.WWW.'verify-account.html?id='.$row_u['id'].'" title="Verify your account">HERE</a>', $mail_content);
		                $mail_content 	= str_replace('{email}',$email, $mail_content);
		                $mail_content 	= str_replace('{contact_link}','<a href="'.WWW.'contact-us.html" title="Contact Us">HERE</a>', $mail_content);
		                $mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);


						$headers = "Content-type:text/html;charset=UTF-8" . "\r\n";
	               		$headers .= "From: no-reply@youcricketer.com" . "\r\n";

		                mail($_POST['email'],$mail_title,$mail_content,$headers);
		                echo '<div id="success" class="alert alert-success">Verification Code sent, Please check your email.... !</div><br><br>';
					}
				}
			}
		?>

		<?php function q(){ ?>

		<div class="white-box content">
			<form method="post" action="">
				<input type="hidden" name="action" value="verify-account">
				<fieldset>
					<h2>Enter Verification Code</h2>
					<div class="form-box">
						<label>Email Address </label>
						<input type="text" name="email" class="input-login validate[required, custom[email]]">
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Code </label>
						<input type="text" name="verification_code" class="input-login validate[required]">
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<a href="#" id="resend" class="popup-link" style="position:relative; top;10px;">Re-send Verification Code</a>
						<input type="submit" value=" Submit " class="submit-login">
					</div>
				</fieldset>
			</form>
			<div id="resend-verification-code" class="hide topline">
						<h1>
							Re-Send Verification Code
							<img src="<?=WWW?>images/icons/delete.png" id="close-resend"class="close-popup">
						</h1>
						<form method="post" action="">
							<input type="hidden" name="action" value="resend-verification">
							<fieldset>
								<div class="form-box">
									<label>Email Address</label>
									<input type="text" name="email" class="input-login validate[required, custom[email]]">
									<input type="submit" class="submit-login margin-top-5" value="Submit">
								</div>



							</fieldset>
						</form>
					</div>
					<div class="clear"></div>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>
<script>

$(window).load(function(){
	$("#resend").click(function(){
    $("#resend-verification-code").show();
		});
	$("#close-resend").click(function(){
		$("#resend-verification-code").hide();
		});

});
</script>

		<?php } ?>



<div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1> Verify Account  </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <div class="white-box">
            <div class="row">
              <div class="col-md-8">
                <h2>Enter Verification Code</h2>
                <form method="post" action="" class="form-horizontal" id="verify-account-frm">

                  <div class="form-group">
                    <label class="col-sm-3 control-label">Email Address </label>
                    <div class="col-sm-9">
                      <input name="email" value="<?php echo $_POST['email'] ?>" class="form-control validate[required, custom[email]]" type="text">
                    </div>
                  </div>

				  <div class="form-group">
                    <label class="col-sm-3 control-label">Code </label>
                    <div class="col-sm-9">
                      <input name="verification_code" value="<?php echo $_POST['verification_code'] ?>" class="form-control validate[required]" type="text">
					  <a  href="#" onClick="javascript:void(0)" data-toggle="modal" data-target="#loginView">Re-Send Verification Code</a>
                    </div>
                  </div>

                 <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                      <input value="Submit" class="btn orange hvr-float-shadow" type="submit">
					  <input type="hidden" name="action" value="verify-account">
                    </div>
                  </div>
                </form>
              </div>

            </div>
		  </div>
        </div>

        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
				<?php include('common/right-panel.php');?>
            </div>
          </div>

        </div>
      </div>
    </div><!-- /.container -->


	<div class="modal fade" id="loginView" tabindex="-1" role="dialog" aria-labelledby="loginViewModel" aria-hidden="true">
      <div class="modal-dialog" data-mcs-theme="minimal" style="max-height:800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="settingLabel">Re-Send Verification Code</h4>
          </div>
          <div class="modal-body mCustomScrollbar" data-mcs-theme="minimal" style="max-height:700px">


            <form class="form-horizontal" method="post" id="resend-verification-frm">
              <input name="action" value="login" type="hidden">
              <div class="form-group">
                <label class="col-sm-5 control-label">Email Address: </label>
                <div class="col-sm-7">
                  <input name="email" value="<?php echo $_POST['email'] ?>" class="form-control validate[required, custom[email]]" type="text">
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-6">
                  <input value=" Submit " class="btn orange full hvr-float-shadow" type="submit">
				  <input type="hidden" name="action" value="resend-verification">
                </div>

              </div>
            </form>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.searchView-Model -->

<script type="text/javascript">
$(document).ready(function(){
	$('#verify-account-frm,#resend-verification-frm').validationEngine();
});
</script>

<?php include('common/footer.php'); ?>
