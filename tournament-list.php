<?php include_once('includes/configuration.php');
$page = 'tournament-list.html';

$selected_country = getGeoLocationCountry(); 

$league_clubs = array();
$league_info = array();
$tournaments = array();
$permitted_tournaments = array();
$error = '';

if(isset($_GET['page']) && !empty($_GET['page'])){
	$current_page = str_replace('page','',trim($_GET['page']));
}else{
	$current_page = 1;
}

$records_per_page = 10;
$start_record  = ($current_page-1)*$records_per_page;

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
}

$sql = "SELECT  *  FROM companies  WHERE user_id = $user_id ";
$rs_league = mysqli_query($conn,$sql);
$league_info = mysqli_fetch_assoc($rs_league);
if(!empty($league_info) && in_array($league_info['company_type_id'],array(1,2,3,35))){
	$league_permission = 1;
}else{
	$league_permission = 0;
}

$sql = "select * from tournament_permissions where member_id = $user_id";
$rs_permission = mysqli_query($conn,$sql);
if(mysqli_num_rows($rs_permission)>0){
	$member_permission = 1;
	while($row = mysqli_fetch_assoc($rs_permission)){
		$permitted_tournaments[] = $row['tournament_id'];
	}
}else{
	$member_permission = 0;
}

if($league_permission || $member_permission){
	$tournament_permission = 1;
}else{
	$tournament_permission = 0;
}

if(empty($error) && isset($_GET['id']) && $_GET['action'] == 'delete'){
	$id = trim($_GET['id']);
	$tournament_info = get_record_on_id('tournaments', $id);	
	if($tournament_info['user_id'] == $user_id){
		$sql = "DELETE FROM tournaments WHERE id=$id ";
		mysqli_query($conn,$sql);
		$sql = "DELETE FROM tournament_teams WHERE tournament_id=$id ";
		mysqli_query($conn,$sql);
	
		$sql = "DELETE FROM tournament_bat_scorecard WHERE match_id IN(Select match_id from tournament_matches WHERE tournament_id=$id) ";
		mysqli_query($conn,$sql);
		$sql = "DELETE FROM tournament_bowl_scorecard WHERE match_id IN(Select match_id from tournament_matches WHERE tournament_id=$id) ";
		mysqli_query($conn,$sql);
		$sql = "DELETE FROM tournament_fow_scorecard WHERE match_id IN(Select match_id from tournament_matches WHERE tournament_id=$id) ";
		mysqli_query($conn,$sql);
		$sql = "DELETE FROM tournament_matches WHERE tournament_id=$id ";
		mysqli_query($conn,$sql);
		$sql = "DELETE FROM tournament_permissions WHERE tournament_id=$id ";
		mysqli_query($conn,$sql);
		
		$_SESSION['tournament_deleted'] = 1;
	}
	header("Location:".WWW."tournament/list");
	exit();
}

if(isset($_SESSION['tournament_deleted']) && $_SESSION['tournament_deleted']==1) {
	$tournament_deleted = 1;
	unset($_SESSION['tournament_deleted']);
}
if(isset($_SESSION['tournament_updated']) && $_SESSION['tournament_updated']==1) {
	$tournament_updated = 1;
	unset($_SESSION['tournament_updated']);
}
if(isset($_SESSION['tournament_added']) && $_SESSION['tournament_added']==1) {
	$tournament_added = 1;
	unset($_SESSION['tournament_added']);
}


if(!empty($permitted_tournaments)){
	$str = implode(',',$permitted_tournaments);
	//$perms_sql = " OR t.id IN($str) ";
	$perms_sql = '';
}else{
	$perms_sql = '';
}

 $sql = "SELECT  count(*) as record_count FROM tournaments as t WHERE (t.user_id = $user_id $perms_sql) and t.status = 1";
$rs_total = mysqli_query($conn,$sql);
$row_total = mysqli_fetch_assoc($rs_total);
$records_count = $row_total['record_count'];
					

 
 $sql = "SELECT t.*,c.name as country_name FROM tournaments as t 
 left join countries as c on .t.country_id = c.id
 WHERE (t.user_id = $user_id $perms_sql) and t.status = 1  ORDER BY t.id DESC LIMIT $start_record,$records_per_page";
$rs_tournaments = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_tournaments)){
	$tournaments[] = $row;
}

$page_title = 'League Tournaments List - '.$league_info['company_name'];
?>
<?php include('common/header.php'); ?>

<div class="page-container"> 
		<?php include('common/user-left-panel.php');?>
      <!-- END SIDEBAR -->
      
      <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
		<div class="page-content">
		<?php  include('common/breadcrumbs.php');?>
			<div class="white-box">
				<div class="row">
				  <div class="col-sm-8">
					<h2> League Tournaments <?php if($league_permission): ?> - <?php echo $league_info['company_name']; ?><?php endif; ?></h2>
				  </div>
				  <?php if($tournament_permission && $league_permission): ?>
				  <div class="col-sm-4">
					<a  class="btn orange full hvr-float-shadow margin-top-10 margin-bottom-10"  href="<?php echo WWW; ?>tournament/add"  title="Add New Tournament"> <i class="fa fa-plus"></i> Add New Tournament </a>
				  </div>
				  <?php endif; ?>
				  
					<div class="col-sm-12">
						<?php if(isset($tournament_deleted) && $tournament_deleted == 1): ?>
							<div id="information" class="alert alert-success">Tournament deleted Successfully... !</div>
						<?php endif; ?>
						<?php if(isset($tournament_updated) && $tournament_updated == 1): ?>
							<div id="information" class="alert alert-success">Tournament updated Successfully... !</div>
						<?php endif; ?>
						<?php if(isset($tournament_added) && $tournament_added == 1): ?>
							<div id="information" class="alert alert-success">Tournament added Successfully... !</div>
						<?php endif; ?>
					
						<?php if(empty($user_info)): ?>
							<div id="error" class="alert alert-danger">You are not logged... !</div>
						<?php endif; ?>
						
						<?php if(!empty($error)): ?>
							<?php echo $error; ?>
						<?php endif; ?>
					</div>
			
				</div>
			
			<?php if($tournament_permission): ?>	
			<div class="row">
              <div class="col-sm-12">
                <div class="table-responsive">
                  <table class="table table-bordered table-condensed table-hover table-striped">
                    <thead>
                      <tr>
                        <th>Tournament Title</th>
                        <th>Game Type</th>
                        <th>Overs</th>
                        <th>Ball Type</th>
                        <th>Country</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
					<?php for($i=0;$i<count($tournaments);$i++): ?>	
                      <tr>
                        <td><?php echo ucwords($tournaments[$i]['title']); ?></td>
						<td><?php echo $tournaments[$i]['overs_type']; ?></td>
						<td><?php echo $tournaments[$i]['overs_count']; ?> overs</td>
						<td><?php echo ucwords(str_replace('_',' ',$tournaments[$i]['ball_type'])); ?></td>
						<td><?php echo ucwords($tournaments[$i]['country_name']); ?></td>
						<td><?php echo  date('d M, Y',strtotime($tournaments[$i]['start_time'])); ?></td>
						<td><?php echo date('d M, Y',strtotime($tournaments[$i]['end_time'])); ?></td>
                       <td>
						<?php if($tournaments[$i]['user_id'] == $user_id): ?>
							<a href="<?php echo WWW; ?>tournament/clubs/<?php echo $tournaments[$i]['id']; ?>" class="text-info" title="Tournament Clubs & Groups"><i class="fa fa-group"></i></a>  
							<a href="<?php echo WWW; ?>tournament/tournament-permissions-<?php echo $tournaments[$i]['id']; ?>.html" class="text-info" title="Umpires/Referees/Scorers Permission"><i class="fa fa-cogs"></i></a> 
							<a href="<?php echo WWW; ?>tournament/standings/<?php echo $tournaments[$i]['id']; ?>" class="text-info" title="Tournament Standings"><i class="fa fa-star-half-o"></i></a>  
							<a href="<?php echo WWW; ?>tournament/polls/list/<?php echo $tournaments[$i]['id']; ?>" class="text-info" title="Tournament Polls"><i class="fa fa-tasks"></i></a>  
							
						<?php endif; ?>
						
						<a href="<?php echo WWW; ?>tournament/matches/list/<?php echo $tournaments[$i]['id']; ?>" class="text-success" title="Schedule & Score Book"><i class="fa fa-calendar-plus-o"></i></a> 
						<?php if($tournaments[$i]['user_id'] == $user_id): ?>			
							<a href="<?php echo WWW; ?>tournament/edit/<?php echo $tournaments[$i]['id']; ?>" class="text-warning" title="Edit Tournament"><i class="fa fa-edit"></i></a>  
							<a href="<?php echo WWW; ?>tournament-list.php?id=<?php echo $tournaments[$i]['id']; ?>&action=delete" class="text-danger" onclick="return confirm('Are you sure to delete tournament.\nAll matches and other data will be deleted.');" title="Delete Tournament"><i class="fa fa-times"></i></a>
						<?php endif; ?>	 
						</td>
                      </tr>
                    <?php endfor; ?>  
						<?php if(empty($tournaments)): ?>
						<tr><td colspan="4">No Records</td></tr>
						<?php endif; ?>
					</table>
                    </tbody>
                  </table>
                </div>
				<?php  $paging_str = getPaging('tournament/list',$records_count,$current_page,$records_per_page);
				echo $paging_str; ?>
              </div>
            </div>
			<?php else: ?>
				<p id="error" class="alert alert-danger">You do not have Permission for this page.</p>
			<?php endif; ?>
				
            </div>
		</div>
	</div>	
</div>		
	
<?php include('common/footer.php'); ?>