<?php include_once('includes/configuration.php');
$page = 'testimonials-about-me.html';
$page_title = 'Testimonials About Me';
if(isset($_GET['action']) && $_GET['action'] == 'delete'){
	$id = $_GET['id'];
	$rs_t = mysqli_query($conn,"select * from testimonials where id = $id and entity_id = '".$_SESSION['ycdc_dbuid']."'");
	if(mysqli_num_rows($rs_t)>0){
		mysqli_query($conn,"DELETE from testimonials where id = '$id' LIMIT 1");
		header("Location:testimonials-about-me.html");
		exit();
	}
}
include('common/header.php'); 
?>

<div class="page-container"> 
	<?php include('common/user-left-panel.php');?>
	
	<? $msg = '';
	$rpp = PRODUCT_LIMIT_FRONT; // results per page
            			$ppage = intval($_GET["page"]);
      					if($ppage<=0) $ppage = 1;
      					$query = "select * from testimonials where status=1 and entity_id = '".$_SESSION['ycdc_dbuid']."'";
				      	//=======================================
					    if(isset($_GET['keywords']) && $_GET['keywords'] != 'Search Here'){
					        $query  .= "and content like '%".str_replace('-','%',$_GET['keywords'])."%'";
					    }
					    
					    $query .= " order by id desc ";
				      //echo $query;
				      //=======================================
				      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
				        $msg = '<div id="information" class="alert alert-info"><i class="fa fa-exclamation-triangle"></i> There are no Testimonial for this Individual</div>';
				      }
				      $rs   = mysqli_query($conn,$query);
				      $tcount = mysqli_num_rows($rs);
				      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
				      $count = 0;
				      $i = ($ppage-1)* $rpp;
          			  $x = 0;
	?>
	
	<!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
          
            <div class="white-box">
				<div class="row">
				  <div class="col-sm-12">
					<h2> Testimonials About Me </h2>
				  </div>
				</div>
            
		
          
            <div class="row">
              <div class="col-sm-5">
                <? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_FRONT){ ?>
		      	<?php
		        	//$reload = 'testimonials.html?';
					if(!empty($keywords)){
							$reload = "testimonials-about-me-{$keywords}.html?";
						}else{
							$reload = "testimonials-about-me.html?";
					}
		        	echo paginate_one($reload, $ppage, $tpages);
		      	?>
		      	<input type="hidden" name="pagination-page" value="testimonials.html">
		        <? } ?>    
              </div>
              <div class="col-sm-4" id="search-div-1">
                <form id="list-search" method="post" action="">
                  <div class="input-group">
                    <input type="text" class="form-control validate[required] input-login" name="txtsearch" placeholder="Search Here">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit">Go!</button>
                    </span>
                  </div>
                </form>
              </div>
              
            </div>
            <div id="pagination-top"> </div>
            <div class="row">
              <div class="col-sm-12">
                <div class="list testimonials">
				<?php if(mysqli_num_rows($rs) == 0) echo $msg; ?>
                  <ul>
				  <?php 
				    while(($count<$rpp) && ($i<$tcount)){
                		mysqli_data_seek($rs,$i);
                		$row_t 	= mysqli_fetch_array($rs);
                		$row_i  = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_t['user_id']."' and is_default = '1'"));
                	?>
                    <li>
                      <div class="row">
                      	<div class="col-sm-9 sep-right">
						<h3>
						<?php if(!empty($row_i['file_name']) && file_exists('users/'.$row_t['user_id'].'/photos/'.$row_i['file_name'])){?>
							<img style="margin:4px" src="<?=WWW?><?='users/'.$row_t['user_id'].'/photos/'.$row_i['file_name']?>" alt="<?=get_combo('users','concat(f_name," ",last_name)',$row_t['user_id'],'','text')?>" width="40">
							<?php }else{ ?>
								<img  src="<?=WWW?>images/no-photo.png" width="70" height="65">
							<?php } ?></td>
							<?=get_combo('users','concat(f_name," ",last_name)',$row_t['user_id'],'','text')?> for 
							
							<?php $row_i  = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_t['entity_id']."' and is_default = '1'")); ?>
							 <?php if($row_i['file_name'] && file_exists('users/'.$row_t['entity_id'].'/photos/'.$row_i['file_name'])){?>
							<img style="margin:4px" src="<?=WWW?><?='users/'.$row_t['entity_id'].'/photos/'.$row_i['file_name']?>" alt="<?=get_combo('users','concat(f_name," ",last_name)',$row_t['entity_id'],'','text')?>" width="40">
							<?php } else { ?>
								<img  src="<?=WWW?>images/no-photo.png" width="70" height="65">
							<?php } ?>
							
							<?=get_combo('users','concat(f_name," ",last_name)',$row_t['entity_id'],'','text')?> on <?=date_converter($row_t['testimonial_date'],'M d,Y H:i')?>
						</h3>	
                          
                          <p><?=$row_t['content']?></p>
                        </div>
                        <div class="col-sm-3 text-center sep-top">
                           <? 	$rating = $row_t['rating'];
								for($x = 0; $x < 5; $x++){
									if($x < $rating){
									?><img src="<?=WWW?>images/star-fill-gray.jpg" title="<?=$rating?>/5"><?
									}else{
									?><img src="<?=WWW?>images/star-gray.jpg" title="<?=$rating?>/5"><?
									}
								}
							?>
                        </div>
                      </div>
                    </li>
                    <?
					      $i++;
					      $count++;
					      $x++;
					  } 
					  ?>
                    
                  </ul>
                </div>
              </div>
            </div>
            <div id="pagination-bottom"> </div>
            <div class="row">
              <div class="col-sm-5">
                <? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_FRONT){ ?>
		      	<?php
		        	//$reload = 'testimonials.html?';
					if(!empty($keywords)){
							$reload = "testimonials-about-me-{$keywords}.html?";
						}else{
							$reload = "testimonials-about-me.html?";
					}
		        	echo paginate_one($reload, $ppage, $tpages);
		      	?>
		      	<input type="hidden" name="pagination-page" value="testimonials.html">
		        <? } ?>    
              </div>
              <div class="col-sm-4" id="search-div-2">
                <form id="list-search" method="post" action="">
                  <div class="input-group">
                    <input type="text" class="form-control validate[required] input-login" name="txtsearch" placeholder="Search Here">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit">Go!</button>
                    </span>
                  </div>
                </form>
              </div>
              
            </div>
         
       
				
				
			</div>
		  
		  
        </div>
      </div>
      <!-- END CONTENT-->
		
</div>

<script type="text/javascript">
		        $('form#list-search').submit(function(e){
					var parentId = $(this).parent().attr('id');
					var string = $('#'+parentId +' form input[name=txtsearch]').val();
					if(string != '' && string != 'Search Here'){
						string = string.replace(/[^a-zA-Z0-9]+/g,'-').toLowerCase();
						if(string.length > 0){
							$('form#list-search').attr('action','<?=WWW;?>testimonials-about-me-' + string + '.html');
						}
					}
				});
</script>
<?php include('common/footer.php'); ?>