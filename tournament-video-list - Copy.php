<?php include_once('includes/configuration.php');
$page = 'tournament-venue-list.html';
$selected_country = getGeoLocationCountry(); 

$videos = array();
$error = '';

if(isset($_GET['page']) && !empty($_GET['page'])){
	$current_page = str_replace('page','',trim($_GET['page']));
}else{
	$current_page = 1;
}

$records_per_page = 10;
$start_record  = ($current_page-1)*$records_per_page;

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
}

$tournament_permission = 0;

$match_id = trim($_GET['m_id']);
$match_info = get_record_on_id('tournament_matches', $match_id);	
$tournament_id = $match_info['tournament_id'];
$tournament_info = get_record_on_id('tournaments', $tournament_id);		

$page_title = 'Videos List - '.ucwords($tournament_info['title']);

if(isset($user_id) && !empty($user_id)){
	//$sql = "select * from tournament_permissions where tournament_id = $tournament_id and member_id = $user_id";
	//$rs_permission = mysqli_query($conn,$sql);
	if($tournament_info['user_id'] == $user_id){
		$tournament_permission = 1;
	}else{
		$tournament_permission = 0;
	}
}

if(empty($error) && isset($_GET['v_id']) && $_GET['action'] == 'delete'){
	$video_id = trim($_GET['v_id']);
	if($tournament_permission){
		$sql = "DELETE FROM tournament_match_videos WHERE id = $video_id ";
		mysqli_query($conn,$sql);
		$_SESSION['video_deleted'] = 1;
	}
	header("Location:".WWW."tournament/matches/videos/list/".$match_id);
	exit();
}

if(isset($_SESSION['video_deleted']) && $_SESSION['video_deleted']==1) {
	$video_deleted = 1;
	unset($_SESSION['video_deleted']);
}
if(isset($_SESSION['video_updated']) && $_SESSION['video_updated']==1) {
	$video_updated = 1;
	unset($_SESSION['video_updated']);
}
if(isset($_SESSION['video_added']) && $_SESSION['video_added']==1) {
	$video_added = 1;
	unset($_SESSION['video_added']);
}


$sql = "SELECT count(*) as record_count FROM tournament_match_videos WHERE match_id=$match_id ";
$rs_total = mysqli_query($conn,$sql);
$row_total = mysqli_fetch_assoc($rs_total);
$records_count = $row_total['record_count'];

$sql = "SELECT * FROM tournament_match_videos WHERE match_id = $match_id ORDER BY id LIMIT $start_record,$records_per_page";
$rs_venues = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_venues)){
	$videos[] = $row;
}

?>
<?php include('common/header.php'); ?>
<style>
.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
dl {min-height:145px;}
.content {width:1010px;}
.large-column {width:790px;}
#table-list {width:100%;border:1px solid #ccc;}
#table-list tr{border:1px solid #ccc;}
#table-list th{padding:5px;background-color:#000;color:#FB7008;font-size:14px;}
#table-list td{padding:5px;text-align:center;}
#table-list a{color:#000;}
.add_tournament{font-size:14px;color:#000;}
</style>
	<div class="middle">
		<h1>Match Videos </h1>
		<h2><?php echo ucwords($tournament_info['title']); ?></h2>
		<div class="white-box content" id="dashboard">
			<?php if(isset($video_deleted) && $video_deleted == 1): ?>
				<div id="information">Video deleted Successfully... !</div>
			<?php endif; ?>
			<?php if(isset($video_updated) && $video_updated == 1): ?>
				<div id="information">Video updated Successfully... !</div>
			<?php endif; ?>
			<?php if(isset($video_added) && $video_added == 1): ?>
				<div id="information">Video added Successfully... !</div>
			<?php endif; ?>
		
			<?php if(empty($user_info)): ?>
				<div id="error">You are not logged... !</div>
			<?php endif; ?>
			
			<?php if(!empty($error)): ?>
				<?php echo $error; ?>
			<?php endif; ?>
		
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
			<?php if($tournament_permission){ ?>
			<p style="float:right;"><a href="<?php echo WWW; ?>tournament/matches/videos/add/<?php echo $match_id; ?>" class="add_tournament" title="Add New Video"><img alt="Add" src="<?php echo WWW; ?>images/icons/add.png" border="0"> Add New Video</a></p>
			 <div class="clear"></div>
				<table id="table-list" class="white-box">
				<tr><th>Title</th><th>Type</th><th>Action</th></tr>
				<?php for($i=0;$i<count($videos);$i++): ?>
					<tr>
						<td><?php echo $videos[$i]['video_title']; ?></td>
						<td><?php echo ucfirst($videos[$i]['video_type']); ?></td>
						<td>
							<a href="<?php echo WWW; ?>tournament/matches/videos/edit/<?php echo $videos[$i]['id']; ?>/<?php echo $match_id; ?>" title="Edit Video"><img alt="Edit" src="<?php echo WWW; ?>images/icons/edit.png" border="0"></a>  &nbsp;&nbsp;
							<a href="<?php echo WWW; ?>tournament-video-list.php?v_id=<?php echo $videos[$i]['id']; ?>&m_id=<?php echo $match_id; ?>&action=delete" onclick="return confirm('Are you sure to delete Video.');" title="Delete Video"><img alt="Delete Video" src="<?php echo WWW; ?>images/icons/delete.png" border="0"></a>
						</td>
					</tr>
				<?php endfor; ?>
				<?php if(empty($videos)): ?>
				<tr><td colspan="4">No Records</td></tr>
				<?php endif; ?>
				</table>
				<?php }else{ ?>
				<div id="error">You do not have Permission for this Tournament... !</div>
				<?php } ?>
				
				<div id="pagination-bottom">
				<?php  $paging_str = getPaging('tournament/matches/videos/list/'.$match_id,$records_count,$current_page,$records_per_page);
				echo $paging_str;
				?>
		        <div class="clear"></div>
				</div>  
			</div>  
		    <div class="clear"></div>
			</div>
		</div>
		<!--<div class="rightbar"><?php //include('common/right-panel.php');?></div>-->
		<div class="clear"></div>
	</div>
	


<?php include('common/footer.php'); ?>