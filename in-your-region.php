<?php include('common/header.php'); ?>
<?	$selected_country = getGeoLocationCountry();
		 
	$row_country = mysqli_fetch_assoc(mysqli_query($conn,"select * from countries where name = '$selected_country'"));
?>

<div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1> In Your Region - <?=$selected_country?> </h1>
        </div>
      </div>
      <div class="row">
        
		<?php if(!isset($_SESSION['ycdc_user_email']) || empty($_SESSION['ycdc_user_email'])): ?>
		<div class="col-md-10">
          <div class="white-box">
            <div class="row">
              <div class="col-md-4 col-sm-6">
                <div class="community-box" onclick="window.location='<?php echo WWW; ?>sign-up-community.html?page=blogs&region=true';">
					<h2>Blogs</h2>
					<img src="<?php echo WWW; ?>assets/img/community-blog.png" alt="Community - Blog - <?=$site_title?>">
				</div>	
              </div>
              <div class="col-md-4 col-sm-6">
                <div class="community-box" onclick="window.location='<?php echo WWW; ?>sign-up-community.html?page=forums&region=true';">
					<h2>Forums</h2>
					<img src="<?php echo WWW; ?>assets/img/community-forum.png" alt="Community - Forum - <?=$site_title?>">
				</div>
              </div>
              <div class="col-md-4 col-sm-6">
               <div class="community-box" onclick="window.location='<?php echo WWW; ?>sign-up-community.html?page=groups&region=true';">
					<h2>Groups</h2>
					<img src="<?php echo WWW; ?>assets/img/community-groups.png" alt="Community - Groups - <?=$site_title?>">
				</div>
              </div>
              <div class="col-md-4 col-sm-6">
                <div class="community-box" onclick="window.location='<?php echo WWW; ?>sign-up-community.html?page=testimonials&region=true';">
					<h2>Testimonials</h2>
					<img src="<?php echo WWW; ?>assets/img/community-testimonials.png" alt="Community - Testimonials - <?=$site_title?>">
				</div>
              </div>
              <div class="col-md-4 col-sm-6">
               <div class="community-box" onclick="window.location='<?php echo WWW; ?>sign-up-community.html?page=member-news&region=true';">
					<h2>Whats Happening</h2>
					<img src="<?php echo WWW; ?>assets/img/community-news.png" alt="Community - News - <?=$site_title?>">
				</div>
              </div>
              <div class="col-md-4 col-sm-6">
                <div class="community-box" onclick="window.location='<?php echo WWW; ?>sign-up-community.html?page=events&region=true';">
					<h2>Events</h2>
					<img src="<?php echo WWW; ?>assets/img/community-events.png" alt="Community - Events - <?=$site_title?>">
				</div>
              </div>
            </div>
		  </div>
        </div>
		<?php else: ?>
		
			<div class="col-md-10">
			  <div class="white-box">
				<div class="row">
				  <div class="col-md-4 col-sm-6">
					<div class="community-box" onclick="window.location='<?=WWW?>your-region-blogs.html';">
						<h2>Blogs</h2>
						<img src="<?php echo WWW; ?>assets/img/community-blog.png" alt="Community - Blog - <?=$site_title?>">
					</div>	
				  </div>
				  <div class="col-md-4 col-sm-6">
					<div class="community-box" onclick="window.location='<?=WWW?>your-region-forums.html';">
						<h2>Forums</h2>
						<img src="<?php echo WWW; ?>assets/img/community-forum.png" alt="Community - Forum - <?=$site_title?>">
					</div>
				  </div>
				  <div class="col-md-4 col-sm-6">
				   <div class="community-box" onclick="window.location='<?=WWW?>your-region-groups.html';">
						<h2>Groups</h2>
						<img src="<?php echo WWW; ?>assets/img/community-groups.png" alt="Community - Groups - <?=$site_title?>">
					</div>
				  </div>
				  <div class="col-md-4 col-sm-6">
					<div class="community-box" onclick="window.location='<?=WWW?>your-region-testimonials.html';">
						<h2>Testimonials</h2>
						<img src="<?php echo WWW; ?>assets/img/community-testimonials.png" alt="Community - Testimonials - <?=$site_title?>">
					</div>
				  </div>
				  <div class="col-md-4 col-sm-6">
				   <div class="community-box" onclick="window.location='<?=WWW?>your-region-news.html';">
						<h2>Whats Happening</h2>
						<img src="<?php echo WWW; ?>assets/img/community-news.png" alt="Community - News - <?=$site_title?>">
					</div>
				  </div>
				  <div class="col-md-4 col-sm-6">
					<div class="community-box" onclick="window.location='<?=WWW?>your-region-events.html';">
						<h2>Events</h2>
						<img src="<?php echo WWW; ?>assets/img/community-events.png" alt="Community - Events - <?=$site_title?>">
					</div>
				  </div>
				</div>
			  </div>
			</div>
		
		<?php endif; ?>
        
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
				<?php include('common/right-panel.php');?>
            </div>
          </div>
          
        </div>
      </div>
</div><!-- /.container -->	
	
<style>.community-box{cursor:pointer;}</style>
<?php include('common/footer.php'); ?>