<?php include('common/header.php'); ?>

<? 	$keyword= $_GET['keyword'];

?>
<?

	$sql = "select users.*,count(favorite_users.player_id) as fav from users INNER JOIN favorite_users ON users.id = favorite_users.player_id  GROUP BY favorite_users.player_id  ORDER BY fav DESC ";

	$com_sql ="select users.*,count(invitations.from_user_id) as inv from users INNER JOIN invitations ON users.id = invitations.from_user_id  where  invitations.status = 'Invitation Accepted' GROUP BY invitations.from_user_id ORDER BY inv DESC  ";

	$query = mysqli_query($conn,$sql);
	$com_row = mysqli_query($conn,$com_sql);
	$rw = mysqli_num_rows($query);
	$com = mysqli_num_rows($com_row);



			// Here we have the total row count
				$rows = $rw;

				$page_rows = 10;

				$last = ceil($rows/$page_rows);

				if($last < 1){
							$last = 1;
							}

				$pagenum = 1;

						if(isset($_GET['pn'])){
								$pagenum = $_GET['pn'];
								}

					if ($pagenum < 1) {
								$pagenum = 1;
						} else if ($pagenum > $last) {
								$pagenum = $last;
									}

	$limit = 'LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;

	$sql .=$limit;

	$firstp = 1;

?>

	<div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1> Most Favourites Individuals </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <div class="white-box">
            <fieldset>
                <?php $rs_users = mysqli_query($conn,$sql);

			  		   $total_record = mysqli_num_rows($rs_users);

			  		   if($total_record > 0 ){

			  			while($row_g= mysqli_fetch_assoc($rs_users)){
								if($row_g['permalink']!=''){
								  $pLink=WWW."yci/".$row_g['permalink'];
								}else{
								  $pLink=WWW."individual-detail-".$row_g['id']."-".friendlyURL($row_g['f_name']." ".$row_g['m_name']." ".$row_g['last_name']).".html";
								}
						$row_img = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_g['id']."' and is_default = '1' "));
                		$row_vid = mysqli_fetch_assoc(mysqli_query($conn,"select * from videos where entity_type = 'users' and entity_id = '".$row_g['id']."' and is_default = '1' "));
			  			if(!empty($row_g['city_id'])){
			  				$row_city   = get_record_on_id('cities', $row_g['city_id']);
				           	$row_states = get_record_on_id('states', $row_city['state_id']);
				           	$row_country= get_record_on_id('countries', $row_states['country_id']);
				           	if(!$row_country){
				           		$row_country= get_record_on_id('countries', $row_g['country_id']);
				           	}
				           	$location   =  $row_g['city_name'].' > '.$row_states['name'].' > '.$row_country['name'];
			  			}else{
			  				$row_country= get_record_on_id('countries', $row_g['country_id']);
			  				$location 	= $row_country['name'];
			  			}
      					?>

                <dl>
                  <dt> <a href="<?php echo $pLink; ?>" title="<?=$row_g['f_name'].' '.$row_g['m_name'].' '.$row_g['last_name']?>"><img src="<?php echo WWW;?><?=($row_img && !empty($row_img['file_name']))?'users/'.$row_g['id'].'/photos/'.$row_img['file_name']:'images/no-photo.jpg'?>"  /></a></dt>
                  <dd>
                    <div class="details">
                      <h3><a href="<?php echo $pLink; ?>" title="<?=$row_g['f_name'].' '.$row_g['m_name'].' '.$row_g['last_name']?>"><?=$row_g['f_name'].'<br>'.$row_g['last_name']?></a></h3>
						<p><? echo $location;?><br />
							<?=get_combo('user_types','name',$row_g['user_type_id'],'','text');?><br />
							<?=$row_g['type']?><br />
							<b> Total Favorites </b> <?php echo $row_g['fav'];?>
						</p>
                      <a href="<?php echo $pLink; ?>" title="<?=$row_g['f_name'].' '.$row_g['m_name'].' '.$row_g['last_name'].' - '.$site_title?>">View Profile</a>
                      <span class="flag"><img title="<?=$row_country['name']?>" alt="<?=$row_country['name']?>" src="<?php echo WWW;?>countries/<?=$row_country['flag']?>"></span>
                    </div>
                    <div class="video"> <?php
							$video = $row_vid['file_name'];
							if(!empty($video)){
								if(preg_match('/<iframe(.*)<\/iframe>/', $row_vid['file_name'])){
										preg_match('/src="(.*?)"/',$row_vid['file_name'] , $src);
										$src = $src[1];
										$video = "<iframe width='215' height='130' allowfullscreen src='$src'></iframe>";
								}else{
										$filename = explode('.',$video);
										$filename1= $filename[0];
										$video = '<video width="215" height="130" controls>
										<source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mp4" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
										<source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.ogg" type=\'video/ogg; codecs="theora, vorbis"\'>
										<source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mov" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
										<source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.webm" type=\'video/webm; codecs="vp8, vorbis"\'>
										</video>';
								}
							}else{
								$video = '<img src="'.WWW.'images/no-video.jpg" >';
							}
							echo $video;?>
					</div>
                  </dd>
                </dl>
                <?php }
			  	} ?>

              </fieldset>

			  <div class="col-sm-6">
						<?php if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){

							if($rows > PRODUCT_LIMIT_FRONT){

								if(!empty($keywords)){
									$reload = "events-{$keywords}.html?";
								}else{
									$reload = "most-favorite.html?";
								}

		        		//echo paginate_one($reload, $pagenum, $last);
		      		?>
		      		<input type="hidden" name="pagination-page" value="events.html">
		        <? }
						}
						?>
        	</div>

            <div class="view-btn">
              <a class="btn orange hvr-float-shadow" title="View all Individuals" href="<?php echo WWW;?>individuals.html">View All</a>
            </div>
          </div>
        </div>
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
              <?php include('common/right-panel.php');?>
            </div>
          </div>

        </div>
      </div>
    </div><!-- /.container -->

<?php include('common/footer.php'); ?>
