<?php include_once('includes/configuration.php');
$page = 'tournament-clubs.html';

$selected_country = getGeoLocationCountry(); 

$tournament_id = trim($_GET['t_id']);
$tournament_info = get_record_on_id('tournaments', $tournament_id);	

$page_title = 'Tournament Clubs- '.ucwords($tournament_info['title']);

$tournament_teams = array();
$tournament_teams_groups = array();

$sql = 'select * from tournament_teams WHERE  tournament_id = '.$tournament_id.' order by id asc';
$rs_teams = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_teams)){
	$tournament_teams[] = $row['team_id'];
	$tournament_teams_groups[$row['team_id']] = $row['group_name'];
}

$league_clubs = array();
$league_info = array();
$error = '';

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
	if($user_info['is_company'] == 1){
		$sql = "select * from companies where user_id = $user_id and company_type_id = 2 and status=1";
		$rs_company = mysqli_query($conn,$sql);
		if(mysqli_num_rows($rs_company)){
			$league_info = mysqli_fetch_assoc($rs_company);
			$sql = 'select c.* from league_clubs as lc inner join companies as c on lc.club_id=c.id where lc.league_id = '.$league_info['id'];
			$rs_club = mysqli_query($conn,$sql);
			while($row = mysqli_fetch_assoc($rs_club)){
				$league_clubs[] = $row;
			}
		}else{
			$error = '<p id="error" class="alert alert-danger">You must be logged as League Owner</p>';
		}
	}
}

if(isset($_SESSION['tournament_clubs_updated']) && $_SESSION['tournament_clubs_updated']==1) {
	$tournament_clubs_updated = 1;
	unset($_SESSION['tournament_clubs_updated']);
}

if(isset($_POST['submit_btn']) && !empty($_POST['submit_btn'])){
	validate();
	if(empty($error)){
		
		$tournament_teams = $_POST['part_clubs'];
		$sql = "DELETE FROM tournament_teams WHERE tournament_id = $tournament_id";
		mysqli_query($conn,$sql);
			
		for($i=0;$i<count($tournament_teams);$i++){
			$group_name = trim($_POST['group_name_'.$tournament_teams[$i]]);	
			$sql = "INSERT INTO tournament_teams SET tournament_id = $tournament_id, team_id = ".$tournament_teams[$i].", group_name = '$group_name' ";
			mysqli_query($conn,$sql);
		}
			
		$_SESSION['tournament_clubs_updated'] = 1;
		header("Location:".WWW."tournament/clubs/".$tournament_id);
	}
}

function validate(){
	global $error;
	
	if(empty($_POST['part_clubs']) || count($_POST['part_clubs'])<2){
		$error.= '<p id="error" class="alert alert-danger">Minimum Two Participating Clubs are Required</p>';
	}
}

?>
<?php include('common/header.php'); ?>

<div class="page-container"> 
		<?php  include('common/user-left-panel.php');?>
      <!-- END SIDEBAR -->
      
      <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
		<div class="page-content">
		<?php  include('common/breadcrumbs.php');?>
			<div class="white-box">
            <div class="row">
              <div class="col-sm-12">
                <h2> Tournament Clubs - <?php echo $tournament_info['title']; ?> </h2>
              </div>
			  
				<div class="col-sm-12">
				<?php if(empty($user_info)): ?>
					<div id="error" class="alert alert-danger">You are not logged... !</div>
				<?php endif; ?>
				
				<?php if(!empty($error)): ?>
					<?php echo $error; ?>
				<?php endif; ?>
				
				<?php if(isset($tournament_clubs_updated) && $tournament_clubs_updated == 1): ?>
					<div id="information" class="alert alert-success">Tournament clubs updated Successfully... !</div>
				<?php endif; ?>
				</div>
			</div>
			
			<div class="row">
              <div class="col-sm-12">
                <h3> Tournament Clubs </h3>
                <p class="text-warning">Please Check Mark the Clubs Participating in this Specific Tournament and Click Update at the bottom. To Remove from Tournament please uncheck and Click Update. Group can have only Alphanumeric Characters</p>
              </div>
            </div>
			<?php if($tournament_info['user_id'] == $user_id){ ?>
            <form method="post" enctype="multipart/form-data" class="form-horizontal">
              <input name="action" value="submit" type="hidden">
              <div class="row">
                <div class="col-sm-12">
                  <h3> Participating Clubs: </h3>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6 text-center">
                  <h5> Club Name </h5>
                </div>
                <div class="col-sm-6 text-center">
                  <h5> Group Name </h5>
                </div>
              </div>
			  
			  <?php for($i=0;$i<count($league_clubs);$i++): ?>
              <div class="form-group">
                <div class="col-sm-6">
                  <input class="chk_left" <?php if(!empty($_POST['part_clubs']) && in_array($league_clubs[$i]['id'],$_POST['part_clubs'])) echo 'checked';elseif(empty($_POST) && in_array($league_clubs[$i]['id'],$tournament_teams)) echo 'checked'; ?> name="part_clubs[]" value="<?php echo $league_clubs[$i]['id']; ?>" type="checkbox">
                  <a href="<?php echo WWW.$league_clubs[$i]['company_permalink']; ?>" target="_blank" title="<?php echo $league_clubs[$i]['company_name']; ?>">
                  	<?php echo $league_clubs[$i]['company_name']; ?>
                  	<?php $rowCheckUser = get_record_on_id('users', $league_clubs[$i]['user_id']);
                  		  if($rowCheckUser['status'] != '1'){ ?>
                  		  (Its not active)
                  		<?php } ?>
                  </a>
                </div>
                <div class="col-sm-6">
                  <input maxlength="20" name="group_name_<?php echo $league_clubs[$i]['id']; ?>" id="group_name_<?php echo $league_clubs[$i]['id']; ?>" value="<?php if(!empty($_POST['group_name'])) echo $_POST['group_name'];elseif(!empty($tournament_teams_groups[$league_clubs[$i]['id']])) echo $tournament_teams_groups[$league_clubs[$i]['id']];else echo 'N/A'; ?>" class="form-control"  type="text">
                </div>
              </div>
              <?php endfor; ?>
			  
             <div class="form-group">
                <div class="col-sm-12 text-center">
                  <input name="submit_btn" value=" Update " class="btn orange hvr-float-shadow" type="submit" <?php if(empty($user_info) || empty($league_info)): ?> disabled <?php endif; ?>>
                  <input name="cancel_btn" value=" Cancel " class="btn blue hvr-float-shadow" onclick="window.location.href='<?php echo WWW; ?>tournament/list';" type="button">
                </div>
              </div>
            </form>
			<?php }else{ ?>
				<div id="error" class="alert alert-danger">You are not owner of this tournament... !</div>
			<?php } ?>
          </div>
		
		
		</div>
	</div>
</div>	
	
<?php include('common/footer.php'); ?>