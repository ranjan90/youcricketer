<?php include('includes/configuration.php'); 
extract($_POST);
set_time_limit(120);ini_set('max_execution_time',120);
$match_info = array();
$error = '';
$match_info = get_record_on_id('tournament_matches', $match_id);	
$tournament_id = $match_info['tournament_id'];
$tournament_info = get_record_on_id('tournaments', $tournament_id);	

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
}

if(isset($user_id) && !empty($user_id)){
	$sql = "select id from tournament_permissions where tournament_id = $tournament_id and member_id = $user_id";
	$rs_permission = mysqli_query($conn,$sql);
	if($tournament_info['user_id'] == $user_id || mysqli_num_rows($rs_permission)){
		$tournament_permission = 1;
	}else{
		$tournament_permission = 0;
	}
}

if(isset($_POST['action']) && $_POST['action'] == 'add'){
	if($tournament_permission){
		$sql = "INSERT into tournament_match_gallery SET match_id=$match_id,image='$img' ";
		if(!mysqli_query($conn,$sql)){
			$error = 'Error in adding image. Try again later'; 
		}
	}else{
		$error = 'You do not have Permission for this Tournament'; 
	}
	
	$output_arr = array('error'=>$error,'str'=>'');
}

if(isset($_POST['action']) && $_POST['action'] == 'get'){
	$gallery_str = '';
	$count = 0;
	$sql = "SELECT * FROM tournament_match_gallery WHERE match_id=$match_id ORDER BY id DESC";
	$rs = mysqli_query($conn,$sql);
	while($row = mysqli_fetch_assoc($rs)){
		$gallery_str.='<div class="col-sm-3"><a class="match-gallery-link cboxElement" href="'.WWW.'images/gallery/files/'.$row['image'].'"><img class="img-responsive" src="'.WWW.'images/gallery/files/thumbnail/'.$row['image'].'"></a>';
		if(!isset($_POST['scoresheet']) || empty($_POST['scoresheet'])){
			$gallery_str.='<br><a class="delete_img" href="javascript:;" onClick="deleteGalleryImage('.$row['id'].')">Delete</a>';
		}
		$gallery_str.='</div>';
		$count++;
		if($count%4 == 0){
			//$gallery_str.='<div class="clear"></div>';	
		}
	}
	
	$output_arr = array('error'=>$error,'str'=>$gallery_str);
}

if(isset($_POST['action']) && $_POST['action'] == 'delete'){
	if($tournament_permission){
		$sql = "SELECT image FROM tournament_match_gallery WHERE id = $id";
		$rs = mysqli_query($conn,$sql);
		$row = mysqli_fetch_assoc($rs);
		$image = $row['image'];
		unlink('images/gallery/files/'.$image);
		unlink('images/gallery/files/thumbnail/'.$image);
		
		$sql = "DELETE FROM tournament_match_gallery WHERE id=$id LIMIT 1 ";
		if(!mysqli_query($conn,$sql)){
			$error = 'Error in deleting image. Try again later'; 
		}
	}else{
		$error = 'You do not have Permission for this Tournament'; 
	}
	
	$output_arr = array('error'=>$error,'str'=>'');
}

echo json_encode($output_arr);

exit();