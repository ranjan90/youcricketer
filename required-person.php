<?php include_once('includes/configuration.php');
$page = 'required-person.html';
 include('common/header.php');

 $uQuery=mysqli_query($conn,"select * from user_types where id!='1' and status='1'");

 if(isset($_POST['submit'])){

   $sql=mysqli_query($conn,"select * from users where user_type_id='".$role."' and status='1'");

   mysqli_query($conn,"insert into person_requests(role,experience,amount,message,status,date_added)values('$role','$experience','$amount','$message','1',Now())");

   $request_id=mysqli_insert_id($conn);

   $headers = "Content-type:text/html;charset=UTF-8" . "\r\n";
   $headers .= "From: no-reply@youcricketer.com\r\n";

   while($user=mysqli_fetch_assoc($sql)){

     $mgs='';

     $username=ucwords($user['f_name'].' '.$user['last_name']);

     $mgs="Hi $username<br/><br/>
     A new Request generated on our site for you. Please see the details below : -<br/><br/>
     Experience : $experience<br/>
     Amount Can pay : $$amount<br/>
     Message : $message<br>";

     $msg="New Request";

     mysqli_query($conn,"insert into user_notifications(user_id,message,request_id,read_status,date_added)values('".$user['id']."','$msg','$request_id','0',Now())");

     //mail($user['email'],'Apologize',$mgs,$headers);*/

   }

   createMsg("Your Request Posted Successfully!..");

   my_redirect('required-person.html');

   die();

 }

?>
<div class="page-container">
       <?php include('common/user-left-panel.php');?>
       <!-- END SIDEBAR -->

       <!-- BEGIN CONTENT -->
       <div class="page-content-wrapper">
           <div class="page-content">
				<div class="white-box">
          <?php echo displayMsg();?>
          <form action="" method="post" id="myform">
					<div class="row">
						<div class="col-md-12">
							<h2>Required Person</h2>
						</div>
					</div>
					<div class="row" id="Required_Person_Form">
						<div class="col-md-4">
							<div class="form-group">
							<label>Role</label>
							<select class="form-control" name="role" required>
								<option value="">Select One</option>
                <?php while($record=mysqli_fetch_assoc($uQuery)){ ?>
								        <option value="<?php echo $record['id']?>"><?php echo $record['name']?></option>
                <?php } ?>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
							<label>Experience</label>
							<select class="form-control" name="experience" required="">
								<option value="5+">5+</option>
								<option value="4">4</option>
								<option value="3">3</option>
								<option value="2">2</option>
								<option value="1">1</option>
								<option value="0">No Experience</option>
							</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
							<label>I can Pay </label>
							<input type="text" class="form-control" name="amount" required placeholder="$2500 Per Month" />
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Message</label>
								<textarea class="form-control" id="editor" col="40" rows="5" name="message" required></textarea>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="col-md-2">
              <input type="submit" name="submit" value="Submit"  class="btn orange full hvr-float-shadow">
        		</div>
		        </div>
          </form>
           </div>
       </div>
</div>

<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
  $(document).ready(function(){

      $("#myform").validate({
        messages: {
            role: {
                required: "Please Choose Role"
            },
            experience: {
                required: "Please Choose Experience"
            },
            amount: {
                required: "Please fill amount"
            },
            message: {
                required: "Please fill message"
            }
        }

   });

  });
</script>
<script src="//cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>
<script>
CKEDITOR.replace('editor');
</script>

<?php include('common/footer.php'); ?>
