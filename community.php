<?php include('common/header.php');  ?>

	<div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1> <?=(isset($_GET['type']) && $_GET['type'] == 'my')?'My':'YouCricketer';?> Community </h1>
        </div>
      </div>
      <div class="row">
        
		<?php if(!isset($_SESSION['ycdc_user_email']) || empty($_SESSION['ycdc_user_email'])): ?>
		<div class="col-md-10">
          <div class="white-box">
            <div class="row">
              <div class="col-md-4 col-sm-6">
                <div class="community-box" onclick="window.location='<?php echo WWW; ?>sign-up-community.html?page=blogs';">
					<h2>Blogs</h2>
					<img src="<?php echo WWW; ?>assets/img/community-blog.png" alt="Community - Blog - <?=$site_title?>">
				</div>	
              </div>
              <div class="col-md-4 col-sm-6">
                <div class="community-box" onclick="window.location='<?php echo WWW; ?>sign-up-community.html?page=forums';">
					<h2>Forums</h2>
					<img src="<?php echo WWW; ?>assets/img/community-forum.png" alt="Community - Forum - <?=$site_title?>">
				</div>
              </div>
              <div class="col-md-4 col-sm-6">
               <div class="community-box" onclick="window.location='<?php echo WWW; ?>sign-up-community.html?page=groups';">
					<h2>Groups</h2>
					<img src="<?php echo WWW; ?>assets/img/community-groups.png" alt="Community - Groups - <?=$site_title?>">
				</div>
              </div>
              <div class="col-md-4 col-sm-6">
                <div class="community-box" onclick="window.location='<?php echo WWW; ?>sign-up-community.html?page=testimonials';">
					<h2>Testimonials</h2>
					<img src="<?php echo WWW; ?>assets/img/community-testimonials.png" alt="Community - Testimonials - <?=$site_title?>">
				</div>
              </div>
              <div class="col-md-4 col-sm-6">
               <div class="community-box" onclick="window.location='<?php echo WWW; ?>sign-up-community.html?page=member-news';">
					<h2>Whats Happening</h2>
					<img src="<?php echo WWW; ?>assets/img/community-news.png" alt="Community - News - <?=$site_title?>">
				</div>
              </div>
              <div class="col-md-4 col-sm-6">
                <div class="community-box" onclick="window.location='<?php echo WWW; ?>sign-up-community.html?page=events';">
					<h2>Events</h2>
					<img src="<?php echo WWW; ?>assets/img/community-events.png" alt="Community - Events - <?=$site_title?>">
				</div>
              </div>
            </div>
		  </div>
        </div>
		<?php else: ?>
			<? if(!isset($_GET['type']) || $_GET['type'] != 'my'){ ?>
			<div class="col-md-10">
			  <div class="white-box">
				<div class="row">
				  <div class="col-md-4 col-sm-6">
					<div class="community-box" onclick="window.location='<?php echo WWW; ?>blogs.html';">
						<h2>Blogs</h2>
						<img src="<?php echo WWW; ?>assets/img/community-blog.png" alt="Community - Blog - <?=$site_title?>">
					</div>	
				  </div>
				  <div class="col-md-4 col-sm-6">
					<div class="community-box" onclick="window.location='<?php echo WWW; ?>forums.html';">
						<h2>Forums</h2>
						<img src="<?php echo WWW; ?>assets/img/community-forum.png" alt="Community - Forum - <?=$site_title?>">
					</div>
				  </div>
				  <div class="col-md-4 col-sm-6">
					<div class="community-box" onclick="window.location='<?php echo WWW; ?>groups.html';">
						<h2>Groups</h2>
						<img src="<?php echo WWW; ?>assets/img/community-groups.png" alt="Community - Groups - <?=$site_title?>">
					</div>
				  </div>
				  <div class="col-md-4 col-sm-6">
					<div class="community-box" onclick="window.location='<?php echo WWW; ?>testimonials.html';">
						<h2>Testimonials</h2>
						<img src="<?php echo WWW; ?>assets/img/community-testimonials.png" alt="Community - Testimonials - <?=$site_title?>">
					</div>
				  </div>
				  <div class="col-md-4 col-sm-6">
				   <div class="community-box" onclick="window.location='<?php echo WWW; ?>news.html';">
						<h2>Whats Happening</h2>
						<img src="<?php echo WWW; ?>assets/img/community-news.png" alt="Community - News - <?=$site_title?>">
					</div>
				  </div>
				  <div class="col-md-4 col-sm-6">
					<div class="community-box" onclick="window.location='<?php echo WWW; ?>events.html';">
						<h2>Events</h2>
						<img src="<?php echo WWW; ?>assets/img/community-events.png" alt="Community - Events - <?=$site_title?>">
					</div>
				  </div>
				</div>
			  </div>
			</div>
			<?php } ?>
			
			<? if(isset($_GET['type']) && $_GET['type'] == 'my'){ ?>
				
				<div class="col-md-10">
				  <div class="white-box">
					<div class="row">
					  <div class="col-md-4 col-sm-6">
						<div class="community-box" onclick="window.location='<?php echo WWW; ?>my-blog.html';">
							<h2>Blogs</h2>
							<img src="<?php echo WWW; ?>assets/img/community-blog.png" alt="Community - Blog - <?=$site_title?>">
						</div>	
					  </div>
					  <div class="col-md-4 col-sm-6">
						<div class="community-box" onclick="window.location='<?php echo WWW; ?>my-forum.html';">
							<h2>Forums</h2>
							<img src="<?php echo WWW; ?>assets/img/community-forum.png" alt="Community - Forum - <?=$site_title?>">
						</div>
					  </div>
					  <div class="col-md-4 col-sm-6">
						<div class="community-box" onclick="window.location='<?php echo WWW; ?>my-groups.html';">
							<h2>Groups</h2>
							<img src="<?php echo WWW; ?>assets/img/community-groups.png" alt="Community - Groups - <?=$site_title?>">
						</div>
					  </div>
					  <?php if($row_user['is_company'] == '1'): ?>
						<div class="col-md-4 col-sm-6">
							<div  class="community-box" onclick="window.location='<?php echo WWW; ?>my-testimonials.html';">
								<h2>Testimonials</h2>
								<img src="<?php echo WWW; ?>assets/img/community-testimonials.png" alt="Community - Testimonials - <?=$site_title?>">
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
						   <div class="community-box" onclick="window.location='<?php echo WWW; ?>my-news.html';">
								<h2>Whats Happening</h2>
								<img src="<?php echo WWW; ?>assets/img/community-news.png" alt="Community - News - <?=$site_title?>">
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="community-box" onclick="window.location='<?php echo WWW; ?>my-events.html';">
								<h2>Events</h2>
								<img src="<?php echo WWW; ?>assets/img/community-events.png" alt="Community - Events - <?=$site_title?>">
							</div>
						</div>
					  <?php else: ?>
						<div class="col-md-4 col-sm-6">
							<div  class="community-box" onclick="window.location='<?php echo WWW; ?>my-testimonials.html';">
								<h2>Testimonials</h2>
								<img src="<?php echo WWW; ?>assets/img/community-testimonials.png" alt="Community - Testimonials - <?=$site_title?>">
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="community-box" onclick="window.location='<?php echo WWW; ?>my-events.html';">
								<h2>Events</h2>
								<img src="<?php echo WWW; ?>assets/img/community-events.png" alt="Community - Events - <?=$site_title?>">
							</div>
						</div>
					  <?php endif; ?>
					  
					</div>
				  </div>
				</div>
				
			<?php } ?>
		<?php endif; ?>
        
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
				<?php include('common/right-panel.php');?>
            </div>
          </div>
          
        </div>
      </div>
    </div><!-- /.container -->
	
<style>.community-box{cursor:pointer;}</style>

<?php include('common/footer.php'); ?>