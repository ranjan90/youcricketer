<?php include('common/header.php'); ?>
<?
if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid']))
{
?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<?
}

if($_SESSION['delete_photo'] == "Del"){

	$msg =  '<div id="success" class="alert alert-success"><b>Success : </b> Video Deleted ... !</div><br><br>';
	unset($_SESSION['delete_photo']);
}

 $user = $_SESSION['ycdc_dbuid'];
?>

<? 	
					if(isset($_POST['embed'])){
					//echo $_SESSION['ycdc_dbuid']; exit;
						extract($_POST);
						if(preg_match('/<iframe(.*)<\/iframe>/', $embed_code)){
						$rs_chk 	= mysqli_query($conn,"select * from videos where entity_id = '$user' and entity_type = 'users' and file_name = '$embed_code' and is_default = 0");

									if(mysqli_num_rows($rs_chk) == 0)
									{
										$query = mysqli_query($conn,"insert into videos (file_name,description, status, entity_id, entity_type, is_default)
												values ('$embed_code','$description','1','$user','users','0');");
										
									}
										else {
											$msg = '<div id="error" class="alert alert-danger"><b>Failure : </b>This Embed Code Already Exists Please Chose Another
											!</div><br><br>';
										}
										if($query!=''){
											$msg = '<div id="success" class="alert alert-success">Videos uploaded ... !</div>';
										}
						}
						else {
							$msg = '<div id="error" class="alert alert-danger"><b>Failure : </b>This is not a Valid Embed Code	!</div><br><br>';
						}
					}else {
						if(isset($_FILES['videos']['name']) && !empty($_FILES['videos']['name'])){
							extract($_POST);
							$user_id=$_SESSION['ycdc_dbuid'];
							$filename 	= strtolower(str_replace(' ','-',$_FILES['videos']['name']));
							$rs_chk 	= mysqli_query($conn,"select * from videos where entity_id = '$user_id' and entity_type = 'users' and file_name = '$filename'");
							
							if(mysqli_num_rows($rs_chk) == 0){
								$query = mysqli_query($conn,"insert into videos (file_name,description, status, entity_id, entity_type, is_default) 
											values ('$filename','$description','1','$user_id','users','0');");
								$video_id = mysqli_insert_id($conn);

								$user_id=$_SESSION['ycdc_dbuid'];
								
								chmod('videos/',0777);

								if(!is_dir('videos/'.$video_id)){
									mkdir('videos/'.$video_id,0777);
								}

								chmod('videos/'.$video_id,0777);

								move_uploaded_file($_FILES['videos']['tmp_name'], 'videos/'.$video_id.'/'.$filename);
								chmod('videos/'.$video_id.'/'.$filename,0777);

								$row_image 			= get_record_on_id('settings', 12);
								$row_size 			= get_record_on_id('settings', 13);

								$ffmpeg 			= "/usr/bin/ffmpeg";
								$image_source_path 	= "videos/$video_id/";
								$dest_image_path    = "videos/$video_id/";
								$second 			= "-".$row_image['value'];
								$size 				= $row_size['value'];
								$image_name 		= "videos/$video_id/$filename.jpg";
								$video_name 		= $filename;
								$command 			= "ffmpeg  -itsoffset $second  -i $video_name -vcodec mjpeg -vframes 1 -an -f rawvideo -s $size $image_name";

								shell_exec($command);
								shell_exec("chmod 0777 $image_name");
							}else{
								$msg = '<div id="error" class="alert alert-danger"><b>Failure : </b>This Video Already Exists Please Chose Another  
								 !</div><br><br>';
							}
						}
					}
					if($query!=''){
						$msg =  '<div id="success" class="alert alert-success">Videos uploaded ... !</div>';
					}
					
					$user_id=$_SESSION['ycdc_dbuid'];
					?>


<div class="page-container"> 
		<?php  include('common/user-left-panel.php');?>
      <!-- END SIDEBAR -->
      
      <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
		<div class="page-content">
		
		
			<div class="white-box">
            <div class="row">
              <div class="col-md-12">
                <h2> My Videos - <?=$_SESSION['ycdc_user_name']?> </h2>
				<?php echo $msg; ?>
              </div>
            </div>
            <div id="upload-area">
				<form method="post" enctype="multipart/form-data" class="form-horizontal" >
                <div class="form-group">
                  <label class="col-sm-5 control-label"> Upload Video Or Embed Code: </label>
                  <div class="col-sm-7">
                    <select id="select_type" class="form-control">
                      <option value="video"> Video </option>
                      <option value="embed"> Embed </option>
                    </select>
                  </div>
                </div>
				</form>
				<form method="post" enctype="multipart/form-data" class="form-horizontal req-frm" id="form_upload" >
                <div class="form-group">
                  <label class="col-sm-5 control-label"> Description: </label>
                  <div class="col-sm-7">
                    <textarea name="description" id="description" class="form-control description" maxlength="40"></textarea>
                    <span class="help-block countdown">40 characters remaining.</span>
                  </div>
                </div>
                <div class="form-group" id="add_video">
                  <label class="col-sm-5 control-label"> Upload Video: </label>
                  <div class="col-sm-7">
                    <input name="videos" class="validate[required]" id="video_upload" accept="video/*" type="file">
                    <span class="help-block">Allowed types are MP4,MOV,OGG,QUICKTIME,WEBM</span>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-5 col-sm-7">
                    <input  value="Upload" name="video" class="btn orange hvr-float-shadow" type="submit">
                  </div>
                </div>
				</form>
				
				<form method="post" enctype="multipart/form-data" class="form-horizontal req-frm" id="form_embed" style="display:none;">
				<div class="form-group">
                  <label class="col-sm-5 control-label"> Description: </label>
                  <div class="col-sm-7">
                    <textarea name="description" id="description" class="form-control description" maxlength="40"></textarea>
                    <span class="help-block countdown">40 characters remaining.</span>
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="col-sm-5 control-label"> Embed Code: </label>
                  <div class="col-sm-7">
                    <textarea name="embed_code" id="upload_embed_code" class="form-control validate[required]"  rows="3"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-5 col-sm-7">
                    <input value="Upload" name="embed" class="btn orange hvr-float-shadow" type="submit">
                  </div>
                </div>
				</form>
              
            </div>
			
			<div class="clear"><br/></div>
            
            <div id="gallery">
              <div class="row">
			  
			  <? 	$rs_vid = mysqli_query($conn,"select * from videos where entity_type = 'users' and entity_id = '$user_id' and is_default = '0'");
					$i= 0;
					while($row_img = mysqli_fetch_assoc($rs_vid)){
						
						$video = '';
						$video = $row_img['file_name'];
						if(!empty($video)){
							if(preg_match('/<iframe(.*)<\/iframe>/', $row_img['file_name'])){ 
										preg_match('/src="(.*?)"/',$row_img['file_name'] , $src);
										$src = $src[1];
										$video = "<iframe width='205' height='130' frameborder='0' allowfullscreen 
													src='$src'>
																		</iframe>";
										
										if(!preg_match('/height="(.*)"/', $video)){
											$video = preg_replace('/height="(.*)"/','height="130"',$video);	
										}else{
											$video = preg_replace('/width="(.*)"/','width="220" height="150"',$video);	
										}
							}else{
										$filename = explode('.',$video);
										$filename1= $filename[0];
										$video = '<video width="220" height="130" controls>
													  <source src="'.WWW.'videos/'.$row_img['id'].'/'.$filename1.'.mp4" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
													  <source src="'.WWW.'videos/'.$row_img['id'].'/'.$filename1.'.ogg" type=\'video/ogg; codecs="theora, vorbis"\'>
													  <source src="'.WWW.'videos/'.$row_img['id'].'/'.$filename1.'.mov" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
													  <source src="'.WWW.'videos/'.$row_img['id'].'/'.$filename1.'.webm" type=\'video/webm; codecs="vp8, vorbis"\'>
													</video>';
							}
						}else{
							$video = '<img src="'.WWW.'images/no-video.jpg" width="220" height="130">';
						}
						?>
						
                <div class="col-sm-3">
                  <?php echo $video; ?>
                  <a onclick="return confirm('Are you sure you want to delete this video ?');" href="<?=WWW?>delete-user-video.php?id=<?php echo $row_img['id'];?>" title="Delete Photo" class="btn blue full hvr-float-shadow"> <i class="fa fa-times"></i> </a>
                </div>
				<?php } ?>
                
              </div>
            </div>
          </div>
		
		
		</div>
	</div>
</div>	


<script>

function updateCountdown(ele) {
    // 140 is the max message length
    var remaining = 40 - $(ele).val().length;//alert(remaining);
    $(ele).parents('form').find('.countdown').text(remaining + ' characters remaining.');
}

$(document).ready(function() {
   
	$('.description').bind('change',function(){
		updateCountdown(this)
	});
	$('.description').bind('keyup',function(){
		updateCountdown(this)
	});
});

$(document).ready(function(){
	$('.req-frm').validationEngine();
	$("#select_type").bind('change',function(){
		if($(this).val() == 'embed'){
			$("#form_embed").show('slow');
			$("#form_upload").hide('slow');
		}else{
			$("#form_embed").hide('slow');
			$("#form_upload").show('slow');
		}	
	});	
});	
</script>
<?php include('common/footer.php'); ?>