<?php include_once('includes/configuration.php');
$page = 'match-add.html';
$selected_country = getGeoLocationCountry(); 

$error = '';
$umpires = array();
$venues = array();
$tournament_permission = 0;
$tournament_id = trim($_GET['t_id']);
$tournament_info = get_record_on_id('tournaments', $tournament_id);	

$page_title = 'Add New Match - '.ucwords($tournament_info['title']);

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
	if($user_id){
		$sql = 'select c.* from tournament_teams as t inner join companies as c on t.team_id=c.id where t.tournament_id = '.$tournament_id;
		$rs_club = mysqli_query($conn,$sql);
		while($row = mysqli_fetch_assoc($rs_club)){
			$tournament_clubs[] = $row;
		}
	}
}

if(isset($user_id) && !empty($user_id)){
	$sql = "select * from tournament_permissions where tournament_id = $tournament_id and member_id = $user_id";
	$rs_permission = mysqli_query($conn,$sql);
	if(mysqli_num_rows($rs_permission)){
		$tournament_permission = 1;
	}else{
		$tournament_permission = 0;
	}
}

$sql = "SELECT id,umpire_name FROM tournament_umpires WHERE tournament_id = $tournament_id ORDER BY id ";
$rs_umpires = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_umpires)){
	$umpires[] = $row;
}

$sql = "SELECT id,venue FROM tournament_venues WHERE tournament_id = $tournament_id ORDER BY id ";
$rs_venues = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_venues)){
	$venues[] = $row;
}

if(isset($_POST['submit_btn']) && !empty($_POST['submit_btn'])){
	validate();
	if(empty($error)){
		$date = date('Y/m/d H:i:s')	;
		if(empty($_POST['umpire_id'])) $_POST['umpire_id'] = 0;
		if(empty($_POST['venue_id'])) $_POST['venue_id'] = 0;
		if(empty($_POST['toss_won_team_id'])) $_POST['toss_won_team_id'] = 0;
		
		$sql = " INSERT INTO tournament_matches SET tournament_id='".trim($tournament_id)."',team1='".trim($_POST['team1'])."',
		team2='".trim($_POST['team2'])."',start_time='".trim($_POST['start_time'])."',umpire_id='".trim($_POST['umpire_id'])."',added_on='".$date."'
		,maximum_overs='".trim($_POST['overs_count'])."',toss_won_team_id='".trim($_POST['toss_won_team_id'])."'
		,toss_won_decision='".trim($_POST['toss_won_decision'])."',venue_id='".trim($_POST['venue_id'])."',last_updated_by = $user_id
		,updated_on='".$date."',status=1";
		
		if(mysqli_query($conn,$sql)){
			$match_id = mysqli_insert_id($conn);
			if(!empty($_POST['toss_won_team_id']) && !empty($_POST['toss_won_decision'])){
				if($_POST['toss_won_decision'] == 'bat'){
					$batting_team1 = $_POST['toss_won_team_id'];
					if($_POST['toss_won_team_id'] == $_POST['team1']){
						$batting_team2 = $_POST['team2'];
					}else{
						$batting_team2 = $_POST['team1'];
					}
				}else{
					$batting_team2 = $_POST['toss_won_team_id'];
					if($_POST['toss_won_team_id'] == $_POST['team1']){
						$batting_team1 = $_POST['team2'];
					}else{
						$batting_team1 = $_POST['team1'];
					}
				}
			
			
				$sql = "Update tournament_matches SET batting_team1 = $batting_team1,batting_team2 = $batting_team2 WHERE id=".$match_id;
				mysqli_query($conn,$sql);
			}
			
			$_SESSION['match_added'] = 1;
			header("Location:".WWW."tournament/matches/list/".$tournament_id);
		}else{
			$error = '<p id="error">Error in adding Match. Try again later</p>';
		}
	}
}

function validate(){
	global $error;
	
	if(empty($_POST['team1'])){
		$error.= '<p id="error">Team1 is required field</p>';
	}
	if(empty($_POST['team2'])){
		$error.= '<p id="error">Team2 is required field</p>';
	}
	if($_POST['team1'] == $_POST['team2']){
		$error.= '<p id="error">Team1 is same as Team2 </p>';
	}
	if(empty($_POST['start_time'])){
		$error.= '<p id="error">Start Time is required field</p>';
	}
	if(empty($_POST['overs_count'])){
		$error.= '<p id="error">Overs is required field</p>';
	}
	/*if(empty($_POST['maximum_overs'])){
		$error.= '<p id="error">Overs is required field</p>';
	}
	if(empty($_POST['toss_won_team_id'])){
		$error.= '<p id="error">Toss Won By is required field</p>';
	}
	if(!empty($_POST['toss_won_team_id']) && ($_POST['toss_won_team_id'] != $_POST['team1'] && $_POST['toss_won_team_id'] != $_POST['team2']) ){
		$error.= '<p id="error">Toss Won By should be Team1 or Team2 </p>';
	}
	if(empty($_POST['toss_won_decision'])){
		$error.= '<p id="error">Toss Won Decision is required field</p>';
	}
	if(empty($_POST['umpire_id'])){
		$error.= '<p id="error">Umpire is required field</p>';
	}
	if(empty($_POST['venue_id'])){
		$error.= '<p id="error">Venue is required field</p>';
	}*/
}

?>
<?php include('common/header.php'); ?>
<style>
.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
dl {min-height:145px;}
.content {width:1010px;}
.large-column {width:790px;}
.form-box {width:50%;}
.chk_left {float:left !important; }
</style>
	<div class="middle">
		<h1> Add New Match </h1>
		<h2>Tournament: <?php echo $tournament_info['title'] ?></h2>
		<div class="white-box content" id="dashboard">
			<?php if(empty($user_info)): ?>
				<div id="error">You are not logged... !</div>
			<?php endif; ?>
			
			<?php if(!empty($error)): ?>
				<?php echo $error; ?>
			<?php endif; ?>
		
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
				<?php if($tournament_permission){ ?>
				<form method="post"  enctype="multipart/form-data">
					<input type="hidden" name="action" value="submit">
					<fieldset>
						<h2>Match Details</h2>
						<p>Fields with * are Required</p>
						
						<div class="form-box">
							<label>Date/Time*</label>
							<div class="text"><input type="text" name="start_time"  autocomplete="off" id="start_time" class="input-login" value="<?php if(!empty($_POST['start_time'])) echo $_POST['start_time']; ?>"></div>
						</div>
						
						<div class="clear"></div>
						
						<div class="form-box">
							<label>Team1*</label>
							<div class="text">
							<select name="team1" id="team1"> 
							<option value="">Select One</option>
							<?php for($i=0;$i<count($tournament_clubs);$i++): if(isset($_POST['team1']) && $_POST['team1'] == $tournament_clubs[$i]['id']) $sel =  'selected';else $sel = '';  ?>
								<option <?php echo $sel; ?> value="<?php echo $tournament_clubs[$i]['id']; ?>"><?php echo $tournament_clubs[$i]['company_name']; ?></option>
							<?php endfor; ?>
							</select>
							</div>
						</div>
						<div class="clear"></div>
						<div class="form-box">
							<label>Team2*</label>
							<div class="text">
							<select name="team2" id="team2"> 
							<option value="">Select One</option>
							<?php for($i=0;$i<count($tournament_clubs);$i++): if(isset($_POST['team2']) && $_POST['team2'] == $tournament_clubs[$i]['id']) $sel =  'selected';else $sel = '';  ?>
								<option <?php echo $sel; ?> value="<?php echo $tournament_clubs[$i]['id']; ?>"><?php echo $tournament_clubs[$i]['company_name']; ?></option>
							<?php endfor; ?>
							</select>
							</div>
						</div>
						
						<div class="clear"></div>
						
						<div class="form-box">
							<label>Overs*</label>
							<div class="text"><input type="text" name="overs_count"  id="overs_count" class="input-login" value="<?php if(!empty($_POST['overs_count'])) echo $_POST['overs_count']; else echo $tournament_info['overs_count']; ?>"></div>
						</div>
						
						<div class="clear"></div>
						
						<div class="form-box">
							<label>Toss Won By</label>
							<div class="text">
							<select name="toss_won_team_id" id="toss_won_team_id"> 
							<option value="">Select One</option>
							<?php for($i=0;$i<count($tournament_clubs);$i++): if(isset($_POST['toss_won_team_id']) && $_POST['toss_won_team_id'] == $tournament_clubs[$i]['id']) $sel =  'selected';else $sel = '';  ?>
								<option <?php echo $sel; ?> value="<?php echo $tournament_clubs[$i]['id']; ?>"><?php echo $tournament_clubs[$i]['company_name']; ?></option>
							<?php endfor; ?>
							</select>
							</div>
						</div>
						<div class="clear"></div>
						
						<div class="form-box">
							<label>Elected To</label>
							<div class="text">
							<select name="toss_won_decision" id="toss_won_decision"> 
							<option value="">Select One</option>
							<option value="bat" <?php if(isset($_POST['toss_won_decision']) && $_POST['toss_won_decision'] == 'bat') echo 'selected'; ?>>Bat</option>
							<option value="bowl" <?php if(isset($_POST['toss_won_decision']) && $_POST['toss_won_decision'] == 'bowl') echo 'selected'; ?>>Bowl</option>
							</select>
							</div>
						</div>
						<div class="clear"></div>
						
						<div class="form-box">
							<label>Umpire</label>
							<div class="text">
								<select name="umpire_id" id="umpire_id"> 
								<option value="">Select One</option>
								<?php for($i=0;$i<count($umpires);$i++): if(isset($_POST['umpire_id']) && $_POST['umpire_id'] == $umpires[$i]['id']) $sel =  'selected';else $sel = '';  ?>
									<option <?php echo $sel; ?> value="<?php echo $umpires[$i]['id']; ?>"><?php echo $umpires[$i]['umpire_name']; ?></option>
								<?php endfor; ?>
								</select>
								<a style="float:right;background:none;" target="_blank" href="<?php echo WWW; ?>tournament/umpires/add/<?php echo $tournament_id; ?>">Add New Umpire</a>
							</div>
						</div>
						<div class="clear"></div>
						
						<div class="form-box">
							<label>Venue</label>
							<div class="text">
								<select name="venue_id" id="venue_id"> 
								<option value="">Select One</option>
								<?php for($i=0;$i<count($venues);$i++): if(isset($_POST['venue_id']) && $_POST['venue_id'] == $venues[$i]['id']) $sel =  'selected';else $sel = '';  ?>
									<option <?php echo $sel; ?> value="<?php echo $venues[$i]['id']; ?>"><?php echo $venues[$i]['venue']; ?></option>
								<?php endfor; ?>
								</select>
								<a style="float:right;background:none;" target="_blank" href="<?php echo WWW; ?>tournament/venues/add/<?php echo $tournament_id; ?>">Add New Venue</a>
							</div>
						</div>
						<div class="clear"></div>
						
						
						<div class="form-box">
							<input type="button" name="cancel_btn" value=" Cancel " class="submit-login" onclick="window.location.href='<?php echo WWW; ?>tournament/matches/list/<?php echo $tournament_id; ?>';">
							<input type="submit" name="submit_btn" value=" Submit " class="submit-login" >
						</div>
					</fieldset>
				</form>
				<?php }else{ ?>
				<div id="error">You do not have Permission for this Tournament... !</div>
				<?php } ?>
			</div>  
		    <div class="clear"></div>
			</div>
		</div>
		<!--<div class="rightbar"><?php //include('common/right-panel.php');?></div>-->
		<div class="clear"></div>
	</div>
	

<script src="<?php echo WWW; ?>js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo WWW; ?>css/jquery.datetimepicker.css"/>
<script>
$('#start_time').datetimepicker();
</script>
<?php include('common/footer.php'); ?>