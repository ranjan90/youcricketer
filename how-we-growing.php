<?php include('common/header.php'); ?>

<div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1>How We Growing !</h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <div class="white-box">
            <ul class="nav nav-pills">
              <?php for($x = date('Y'); $x >= 2013; $x--) { ?>
                <li style="position:static !important;" <?php echo ($x == date('Y'))?'class="active"':'';?>><a data-toggle="pill" href="#<?php echo $x; ?>"><?php echo $x; ?></a></li>
              <?php } ?>
            </ul>
            <hr>
            <div class="tab-content">
              <?php for($x = date('Y'); $x >= 2013; $x--) { ?>
                  <div id="<?php echo $x;?>" class="tab-pane fade <?php echo ($x == date('Y'))?'in active':'';?>">
                    <?php $query = "select * from growing where status = '1' and tag='$x' order by sort_order desc";
                          $team = mysqli_query($conn, $query);
                          while($row = mysqli_fetch_object($team)){ ?>
                            <div class="story col-md-5">
                              <h3><?php echo $row->tag .' - '. $row->title?></h3>
                              <p><?php echo $row->description?></p>
                              <img src="<?php echo WWW;?>growing/<?php echo $row->photo?>" alt="<?php echo $row->story?>">
                            </div>
                     <?php } ?>
                     <div class="clear"></div>
                  </div>
              <?php } ?>
            </div>
		      </div>
        </div>
        
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
             <?php include('common/right-panel.php');?>
            </div>
          </div>
          
        </div>
      </div>
</div><!-- /.container -->

<?php include('common/footer.php'); ?>
<style>
.story{border:1px solid #ccc; border-radius: 5px; padding:15px; margin:10px;}
.story img{max-width: 75%; margin:auto;}
</style>