<?php include_once('includes/configuration.php');
$page = 'club-members-search.html';
$selected_country = getGeoLocationCountry();

$countries = array();
$rs_countries = mysqli_query($conn,"select id,name from countries where status='1'");
while($row = mysqli_fetch_assoc($rs_countries)){
	$countries[] = $row;
}

$leagues = array();
$rs_clubs = mysqli_query($conn,"select id,title from leagues where status='1' order by title");
while($row = mysqli_fetch_assoc($rs_clubs)){
	$leagues[] = $row;
}

$ppage = intval($_GET["page"]);
if($ppage<=0) $ppage = 1;

$league_info = array();
$user_info  = array();
$league_clubs = array();

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);
	if($user_info['is_company'] == 1){
		$sql = "select * from companies where user_id = $user_id and company_type_id = 2 and status=1";
		$rs_company = mysqli_query($conn,$sql);
		if(mysqli_num_rows($rs_company)){
			$league_info = mysqli_fetch_assoc($rs_company);
			$sql = 'select * from league_clubs where league_id = '.$league_info['id'];
			$rs_club = mysqli_query($conn,$sql);
			while($row = mysqli_fetch_assoc($rs_club)){
				$league_clubs[] = $row['club_id'];
			}
		}
	}
}


if(isset($_POST['clubs_add_submit']) && !empty($_POST['clubs_add_submit'])){
	$clubs = $_POST['club_ids'];
	if(!empty($clubs)){
		for($i=0;$i<count($clubs);$i++){
			$sql = "INSERT into league_clubs SET league_id=".$league_info['id'].", club_id=".$clubs[$i];
			mysqli_query($conn,$sql);
		}
		if(!mysqli_error($conn)){
			$sql = "UPDATE companies SET tournament_data_added = 'yes' WHERE id = ".$league_info['id'];
			mysqli_query($conn,$sql);
		}
	}

	$_SESSION['league_club_added'] = 1;

	if(isset($_GET['keywords']) && !empty($_GET['keywords'])){
		$url_q.='-'.$_GET['keywords'];
	}else{
		$url_q.='-all';
	}
	if(isset($_GET['league_id']) && !empty($_GET['league_id']) ){
		$url_q.='-'.$_GET['league_id'];
	}else{
		$url_q.='-0';
	}
	if(isset($_GET['country_id']) && !empty($_GET['country_id']) ){
		$url_q.='-'.$_GET['country_id'];
	}else{
		$url_q.='-0';
	}

	header("Location:".WWW."league-clubs-search{$url_q}-".$ppage.'.html');
	exit;
	//echo '<script>window.location.href="'.WWW.'club-members-search-all-'.$ppage.'.html"</script>';
}

if(isset($_SESSION['league_club_added']) && $_SESSION['league_club_added']==1) {
	$club_added = 1;
	unset($_SESSION['league_club_added']);
}

if(isset($_GET['league_id']) && !empty($_GET['league_id'])){

	$query = "select comapny_name from companies where id =".$_GET['league_id'];
	$rs   = mysqli_query($conn,$query);
	if(mysqli_num_rows($rs)){
		$league_arr = mysqli_fetch_assoc($rs);
		$league_name = $league_arr['company_name'];
	}
}

?>
<?php include('common/header.php'); ?>

<?php

                		$rpp = PRODUCT_LIMIT_FRONT; // results per page
						//$league_id = 0;
                		$country_id = 0;
      					$query = "select distinct u.*,c.company_name,c.years_in_business,c.id as company_id,c.company_permalink from users u inner join companies as c on u.id=c.user_id ";
						$query_count = "select count(*) as users_count from users u inner join companies as c on u.id=c.user_id";
				      	//=======================================
						$where = " where c.company_type_id = '4'  and u.status = 1";
						if(isset($_GET['league_id']) && !empty($_GET['league_id'])){
							$league_id = trim($_GET['league_id']);
							$join  = " inner join league_clubs as l on l.club_id=c.id ";
							$query.=$join;
							$query_count.=$join;
							$where.=" AND l.league_id = ".$league_id;
						}

						if(isset($_GET['country_id']) && !empty($_GET['country_id'])){
							$country_id = trim($_GET['country_id']);
							$where.=" AND u.country_id = ".$country_id;
						}

					    if(isset($_GET['keywords']) && $_GET['keywords'] != 'Search Here' && $_GET['keywords'] != 'all' && !empty($_GET['keywords'])){
							$keywords = str_replace('_',' ',trim($_GET['keywords']));
							$where  .= " and (c.company_name like '%{$keywords}%' ) ";
						}else if(isset($_GET['club_name_srch']) && $_GET['club_name_srch'] != 'Club Name' && $_GET['club_name_srch'] != 'all' && !empty($_GET['club_name_srch']) ){
							$keywords = str_replace('_',' ',trim($_GET['club_name_srch']));
							$where  .= " and (c.company_name like '%{$keywords}%' ) ";
						}else{
							$keywords = '';
						}

						$query.=$where;
						$query_count.=$where;
					    $query .= " order by u.id desc ";


						// $query = "select distinct u.*,c.*,l.* from users u, companies c, league_clubs l where u.id=c.user_id and c.id=l.league_id and u.status = 1 and c.company_type_id=4";
						// if(isset($_GET['league_id']) && !empty($_GET['league_id'])){
						// 	$league_id = trim($_GET['league_id']);
						// 	$query .= " AND l.league_id =".$league_id;
						// }
						// if(isset($_GET['keywords']) && $_GET['keywords'] != 'Search Here' && $_GET['keywords'] != 'all' && !empty($_GET['keywords'])){
						// 	$keywords = str_replace('_',' ',trim($_GET['keywords']));
						// 	$where  .= " and (c.company_name like '%{$keywords}%' ) ";
						// }elseif(isset($_GET['club_name_srch']) && $_GET['club_name_srch'] != 'Club Name' && $_GET['club_name_srch'] != 'all' && !empty($_GET['club_name_srch']) ){
						// 	$keywords = str_replace('_',' ',trim($_GET['club_name_srch']));
						// 	$where  .= " and (c.company_name like '%{$keywords}%' ) ";
						// }else{
						// 	$keywords = '';
						// }
						// if(isset($_GET['country_id']) && !empty($_GET['country_id'])){
						// 	$country_id = trim($_GET['country_id']);
						// 	$where.=" AND u.country_id = ".$country_id;
						// }
						// $query .= " order by u.id desc";

						$rs_count   = mysqli_query($conn,$query_count);
						$row_count  = mysqli_fetch_assoc($rs_count);
						$tcount = $row_count['users_count'];

						// $rs_count   = mysqli_query($conn,$query);
						// $tcount = mysqli_num_rows($rs_count);
						// $row_count  = mysqli_fetch_assoc($rs_count);

						$tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
						$count = 0;
						$start = ($ppage-1)* $rpp;
						$x = 0;
						//echo $query;

						?>





<div class="page-container">
		<?php include('common/user-left-panel.php');?>
      <!-- END SIDEBAR -->

      <!-- BEGIN CONTENT -->
      <div class="page-content-wrapper">
        <div class="page-content">

          <div class="white-box">
            <div class="row">
              <div class="col-md-12">
                <h2>  Add League Clubs </h2>

				<?php if(isset($club_added) && $club_added == 1): ?>
					<div id="information" class="alert alert-success">Clubs added Successfully... !</div>
				<?php  endif; ?>

				<?php if(empty($user_info)): ?>
					<div id="error" class="alert alert-danger">You are not logged... !</div>
				<?php endif; ?>

				<?php if(!empty($user_info) && empty($league_info) ): ?>
					<div id="error" class="alert alert-danger">You are not logged as League Owner.. !</div>
				<?php endif; ?>
              </div>
            </div>

			<div id="adv_search" class="panel panel-default">
              <div class="panel-body">
                <div class="form-group">
                  <div class="col-sm-12">
                    <h3> Advanced Search </h3>
                  </div>
                </div>
                <form class="form-horizontal" id="advance_search_form" method="post" action="">
                  <div class="form-group">
                    <label class="col-sm-5 control-label">League Name</label>
                    <div class="col-sm-7">
											<input type="hidden" id="league_id" value="<?php if(isset($league_id) and $league_id>=0){ echo $league_id; }else{
												echo '-1'; } ?>">
						<input class="form-control" id="search_box" name="league_name" <?php if(isset($league_name) && !empty($league_name)){?> value="<?php echo $league_name; ?>" <?php } else{ ?> placeholder="League Name" <?php } ?> autocomplete="off" type="text">
						<div style="clear:both"></div>
						<div id="searchres" class="searchres"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Country</label>
                    <div class="col-sm-7">
                      <select name="country_id_srch" id="country_id_srch" class="form-control" >
						<option value="0">Country</option>
						<?php for($i=0;$i<count($countries);$i++): ?>
							<?php if(isset($_GET['country_id']) && $_GET['country_id'] == $countries[$i]['id']) $sel = 'selected';else $sel = ''; ?>
							<option <?php echo $sel; ?> value="<?php echo $countries[$i]['id']; ?>"><?php echo $countries[$i]['name']; ?></option>
						<?php endfor; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Club Name</label>
                    <div class="col-sm-7">
                      <input class="form-control" name="club_name_srch"  id="club_name_srch" <?php if(isset($_GET['keywords']) && !empty($_GET['keywords'])): ?> value="<?php echo str_replace('_',' ',$_GET['keywords']); ?>" <?php else: ?>  placeholder="Club Name" <?php endif; ?> type="text">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-5 col-sm-7">
                      <input name="club_id" id="club_id" value="" type="hidden">
                      <input name="submit_adv_search" id="submit_adv_search" value="Search" class="btn orange hvr-float-shadow" type="submit">
                    </div>
                  </div>
                </form>
              </div>
            </div>

			<?php if(!empty($_GET['keywords']) || !empty($_GET['league_id']) || !empty($_GET['country_id'])){ ?>
            <div id="pagination-top">
              <div class="row">
                <div class="col-sm-6">
                <? if($tcount != 0 && $tcount > PRODUCT_LIMIT_FRONT){ ?>
		      	<?php
		        	$q_url = '';
					if(isset($keywords) && !empty($keywords)){
						$q_url.="-{$keywords}";
					}else{
						$q_url.="-all";
					}
					if(!isset($league_id)){
						$league_id=0;
					}
					if(isset($league_id) ){
						if($league_id==-1){
							$league_id = 0;
						}
						$q_url.="-{$league_id}";
					}
					if(isset($country_id) ){
						$q_url.="-{$country_id}";
					}

					if(!empty($q_url)){
						$reload = "league-clubs-search{$q_url}.html?";
					}else{
						$reload = "league-clubs-search-all.html?";
					}
		        	echo paginate_one($reload, $ppage, $tpages);
		      	?>
		      	<input type="hidden" name="pagination-page" value="league-clubs-search.html">
		        <? } ?>
				</div>
                <div class="col-sm-3" id="search-div1">
					<!-- <form id="list-search" method="post" action="">
                  <div class="input-group" >
                    <input class="form-control validate[required] input-login" name="txtsearch" id="txtsearch" placeholder="Search Here" type="text">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit">Go!</button>
                    </span>
                  </div>
				  </form> -->
                </div>
              </div>
            </div>

			<form method="post">
            <div id="individual" class="content1">
				<?php
						$query .= " LIMIT $start,$rpp ";
						//echo $query;
						$rs   = mysqli_query($conn,$query);
						$rcount = mysqli_num_rows(mysqli_query($conn,$query));
						if($rcount == 0){
							echo '<div id="information" class="alert alert-danger">No record found ... !</div>';
						}

						$i= 0 ;
						while($row 	= mysqli_fetch_assoc($rs)){


							$row_img = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row['id']."' and is_default = '1' "));
			  				$row_country= get_record_on_id('countries', $row['country_id']);
			  				$location 	= $row_country['name'];
			  				$row_vid = mysqli_fetch_assoc(mysqli_query($conn,"select * from videos where entity_id = '".$row['id']."' and entity_type = 'users' and is_default = '1' and file_name != ''"));

							$str_league = '';
							$sql = "select c.company_name from league_clubs as lc  inner join companies as c on c.id=lc.league_id WHERE lc.league_id != ".$league_info['id']." and lc.club_id=".$row['id']." and c.company_type_id = 2 and c.status=1";

							$rs_league = mysqli_query($conn,$sql);
							if(mysqli_num_rows($rs_league)){
								while($row_league = mysqli_fetch_assoc($rs_league)){
									$str_league.=" <br/>Member of ".$row_league['company_name'];
								}

							}
		        ?>
              <dl>
                <dt> <a href="<?=WWW?><?php echo $row['company_permalink']; ?>" title="<?=$row['company_name']; ?>"><img src="<?php echo WWW;?><?=($row_img && !empty($row_img['file_name']))?'users/'.$row['id'].'/photos/'.$row_img['file_name']:'images/no-photo.jpg'?>"></a></dt>
                <dd>
                  <div class="details">
                    <h3><a href="<?=WWW?><?php echo $row['company_permalink']; ?>" title="<?=$row['company_name'].' - '.$site_title?>"><?=truncate_string($row['company_name'],20)?></a></h3>
                    <p><? echo $location;?><br>

					<?php if(!empty($league_info)) { ?>

						<?php if(!in_array($row['company_id'],$league_clubs)){ ?>
							<p><input type="checkbox" name="club_ids[]" id="club_id_<?php echo $i; ?>" value="<?php echo $row['company_id']; ?>"> Add to League</p>
						<?php } else{ ?>
							<p>Already in your League</p>
						<?php } ?>
							<?php echo $str_league; ?>
					<?php } ?>

                    <a href="<?=WWW?><?php echo $row['company_permalink']; ?>" title="<?=$row['company_name'].' - '.$site_title?>">View Profile</a>
                    <span class="flag"><img title="<?=$row_country['name']?>" alt="<?=$row_country['name']?>" src="<?php echo WWW;?>countries/<?=$row_country['flag']?>"></span>
                  </div>
                  <div class="video">
									<?php
										$video = $row_vid['file_name'];
											if(!empty($video)){
											if(preg_match('/<iframe(.*)<\/iframe>/', $row_vid['file_name'])){

												preg_match('/src="(.*?)"/',$row_vid['file_name'] , $src);
												$src = $src[1];
												$video = "<iframe width='205' height='130' frameborder='0' allowfullscreen src='$src'></iframe>";

											}else{
												$filename = explode('.',$video);
												$filename1= $filename[0];
												$video = '<video width="220" height="130" controls>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mp4" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.ogg" type=\'video/ogg; codecs="theora, vorbis"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mov" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.webm" type=\'video/webm; codecs="vp8, vorbis"\'>
												</video>';
											}
										}else{
											$video = '<img src="'.WWW.'images/no-video.jpg" >';
										}
										echo $video; ?>
					</div>
                </dd>
              </dl>
			  <?php

				$i++;
				$count++;
				$x++;
				}
				?>

<div class="clearfix"></div>
            </div>
			<div class="clearfix"></div>
			<?php if($rcount > 0){ ?>
            <div class="row">
              <div class="col-sm-12">
                <input name="clubs_add_submit" id="clubs_add_submit" value="Add Clubs" type="submit" class="btn orange hvr-float-shadow" style="margin:15px 0">
              </div>
            </div>
			<?php } ?>
			</form>
<div class="clearfix"></div>
            <div id="pagination-bottom">
              <div class="row">
                <div class="col-sm-6">
                <? if($tcount != 0 && $tcount > PRODUCT_LIMIT_FRONT){ ?>
		      	<?php
		        	$q_url = '';
					if(isset($keywords) && !empty($keywords)){
						$q_url.="-{$keywords}";
					}else{
						$q_url.="-all";
					}
					if(isset($league_id) ){
						$q_url.="-{$league_id}";
					}
					if(isset($country_id) ){
						$q_url.="-{$country_id}";
					}

					if(!empty($q_url)){
						$reload = "league-clubs-search{$q_url}.html?";
					}else{
						$reload = "league-clubs-search-all.html?";
					}
		        	echo paginate_one($reload, $ppage, $tpages);
		      	?>
		      	<input type="hidden" name="pagination-page" value="league-clubs-search.html">
		        <? } ?>
				</div>
                <div class="col-sm-3" id="search-div2">
					<!-- <form id="list-search" method="post" action="">
                  <div class="input-group">
                    <input class="form-control validate[required] input-login" name="txtsearch" id="txtsearch" placeholder="Search Here" type="text">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit">Go!</button>
                    </span>
                  </div>
				  </form> -->
                </div>
              </div>
            </div>

			<?php } ?>
          </div>
        </div>
      </div>
      <!-- END CONTENT-->
    </div><!-- /.container -->

		        <script type="text/javascript">
		        $('form#list-search').submit(function(e){
					var parentId = $(this).parent().attr('id');
					var string = $('#'+parentId +' form input[name=txtsearch]').val();
					if(string != '' && string != 'Search Here'){
						string = string.replace(/[ ]+/g,'_');
						string = string.replace(/[^a-zA-Z0-9_]+/g,'').toLowerCase();
						if(string.length > 0){
							$('form#list-search').attr('action','<?=WWW;?>league-clubs-search-' + string + '.html');
						}
					}
				});

				 $('#submit_adv_search').click(function(e){

					var string = $('#club_name_srch').val();
					if(string == ''){
						string = 'all';
					}
					//var league_id = $('#league_id_srch').val();
					if($("#search_box").val()!=''){
						var league_id = $('#league_id').val();
					}else{
						var league_id = 0;
					}
					var country_id = $('#country_id_srch').val();
					//if(string != '' && string != 'Member Name'){
						string = string.replace(/[ ]+/g,'_');
						string = string.replace(/[^a-zA-Z0-9_]+/g,'').toLowerCase();
						//if(string.length > 0){
							$('form#advance_search_form').attr('action','<?=WWW;?>league-clubs-search-' + string +"-"+league_id+"-"+country_id+ '.html');
						//}
					//}
				});
		        </script>

				<script type="text/javascript">
				$(document).ready(function(){
					$("#search_box").keyup(function(){
						var search_string = $("#search_box").val();
						$(".searchres").css({"display":"block"});
						if(search_string == ''){$("#searchres").html('');}else{postdata = {'string' : search_string}
						$.post("get_clubs.php?type=league",postdata,function(data){
								if(data==0){
									var name = $("#search_box").val();
									fillme(name,-1);
									data = '<div class="no_data">No Result Found !</div>';
								}
								$("#searchres").html(data);
						}); }
					});
				});
				function fillme(name,id){
					$("#search_box").val(name);$("#searchres").html(''); $("#league_id").val(id);
				}
				$(".searchres").click(function(){
					$(".searchres").hide('fast');
				});
				$("#search_box").focusout(function(){
					$(".searchres").hide('fast');
				});
				</script>

<style type="text/css">
.searchres{margin-left:2px;width:97%;border: #f0f0f0 1px solid;border-radius:5px;position:absolute;z-index:100;background-color:#fff; height: 285px; overflow-y: scroll; display: none;}
.img{float:left;margin: 5 5 5 5px;float:left;}
.user_div{clear:both;font-family: verdana;border-top: #f0f0f0 1px solid;color:black; font-size: 13px;height:20px;margin:1px;padding:13px;width:95%;border-radius:4px;vertical-align: top;  }
.user_div:hover{background:#F6F6F6;color:#fff;cursor:pointer}
.no_data{height: 40px;background-color: #F0F0F0;padding: 10 0 0 10px;width: 180px;border-radius: 5px;font-family: verdana;color:black; font-size: 15px;}
</style>
<?php include('common/footer.php'); ?>
