<?php include_once('includes/configuration.php');
$page = 'venues/edit';
$selected_country = getGeoLocationCountry(); 

$error = '';
if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
}

$sql = 'select * from companies where user_id = '.$user_id;
$rs_league = mysqli_query($conn,$sql);
$league_info = mysqli_fetch_assoc($rs_league);
$league_id = $league_info['id'];

$venue_id = trim($_GET['v_id']);
$venue_info = get_record_on_id('league_venues', $venue_id);	

$page_title = 'Edit Venue - '.ucwords($venue_info['title']);

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($user_id) && !empty($user_id)){
	if(!empty($league_info)){
		$venue_permission = 1;
	}else{
		$venue_permission = 0;
	}
}

$sql = "SELECT id,name FROM countries  ORDER BY id ";
$rs_countries = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_countries)){
	$countries[] = $row;
}

if(isset($_POST['submit_btn']) && !empty($_POST['submit_btn'])){
	validate();
	if(empty($error)){
		if(empty($_POST['country'])) $_POST['country'] = 0;
		
		$sql = " UPDATE league_venues SET venue='".trim($_POST['venue'])."',
		address='".trim($_POST['address'])."',country='".trim($_POST['country'])."',state='".trim($_POST['state'])."',
		city='".trim($_POST['city'])."', zip_code='".trim($_POST['zip_code'])."',contact_name='".trim($_POST['contact_name'])."',
		directions='".trim($_POST['directions'])."' WHERE id=$venue_id";
		
		if(mysqli_query($conn,$sql)){
			$_SESSION['venue_updated'] = 1;
			header("Location:".WWW."venues/list");
			exit();
		}else{
			$error = '<p id="error" class="alert alert-danger">Error in updating Venue. Try again later</p>';
		}
	}
}

function validate(){
	global $error;
	if(empty($_POST['venue'])){
		$error.= '<p id="error" class="alert alert-danger">Venue is required field</p>';
	}
	if(empty($_POST['address'])){
		$error.= '<p id="error" class="alert alert-danger">Address is required field</p>';
	}
}

?>
<?php include('common/header.php'); ?>

<div class="page-container"> 
		<?php  include('common/user-left-panel.php');?>
      <!-- END SIDEBAR -->
      
      <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
		<div class="page-content">
		<?php  include('common/breadcrumbs.php');?>
		
		<div class="white-box">
            <div class="row">
              <div class="col-sm-12">
                <h2> Edit Venues  </h2>
				<h3><?php echo ucwords($league_info['company_name']); ?></h3>
				<?php if(empty($user_info)): ?>
					<div id="error" class="alert alert-danger">You are not logged... !</div>
				<?php endif; ?>
				
				<?php if(!empty($error)): ?>
					<?php echo $error; ?>
				<?php endif; ?>
              </div>
            </div>
            
			<?php if($venue_permission){ ?>
            <div class="row">
              <div class="col-sm-12">
                <form method="post" action="" class="form-horizontal">
                  <input name="action" value="submit" type="hidden">
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Venue</label>
                    <div class="col-sm-7">
                      <input name="venue" id="venue" class="form-control"  value="<?php if(!empty($_POST['venue'])) echo $_POST['venue'];else echo $venue_info['venue']; ?>" type="text">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Address</label>
                    <div class="col-sm-7">
                      <textarea name="address" id="address" class="form-control" rows="3"><?php if(!empty($_POST['address'])) echo $_POST['address'];else echo $venue_info['address']; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Country</label>
                    <div class="col-sm-7">
                      <select name="country" id="country" class="form-control">
                        <option value="0">Select One</option>
						<?php for($i=0;$i<count($countries);$i++): if(isset($_POST['country']) && $_POST['country'] == $countries[$i]['id']) $sel =  'selected'; elseif($venue_info['country'] == $countries[$i]['id']) $sel =  'selected'; else $sel = '';  ?>
							<option <?php echo $sel; ?> value="<?php echo $countries[$i]['id']; ?>"><?php echo $countries[$i]['name']; ?></option>
						<?php endfor; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">State</label>
                    <div class="col-sm-7">
                      <input name="state" id="state" class="form-control" value="<?php if(!empty($_POST['state'])) echo $_POST['state'];else echo $venue_info['state']; ?>" type="text">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">City</label>
                    <div class="col-sm-7">
                      <input name="city" id="city" class="form-control" value="<?php if(!empty($_POST['city'])) echo $_POST['city'];else echo $venue_info['city']; ?>" type="text">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Zip Code</label>
                    <div class="col-sm-7">
                      <input name="zip_code" id="zip_code" class="form-control" value="<?php if(!empty($_POST['zip_code'])) echo $_POST['zip_code'];else echo $venue_info['zip_code']; ?>" type="text">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Contact Name</label>
                    <div class="col-sm-7">
                      <input name="contact_name" id="contact_name" class="form-control" value="<?php if(!empty($_POST['contact_name'])) echo $_POST['contact_name'];else echo $venue_info['contact_name']; ?>" type="text">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Directions</label>
                    <div class="col-sm-7">
                      <textarea name="directions" id="directions" class="form-control" rows="3"><?php if(!empty($_POST['directions'])) echo $_POST['directions'];else echo $venue_info['directions']; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-5 col-sm-7">
                      <input name="submit_btn" value=" Submit " class="btn blue hvr-float-shadow" type="submit">
					  <input name="cancel_btn" value=" Cancel " class="btn orange hvr-float-shadow" onclick="window.location.href='<?php echo WWW; ?>venues/list';" type="button">
                    </div>
                  </div>
                </form>
              </div>
            </div>
			<?php }else{ ?>
				<div id="error"  class="alert alert-danger">You do not have Permission for Venues... !</div>
			<?php } ?>
          </div>

		</div>
	</div>
</div>		
	


<?php include('common/footer.php'); ?>