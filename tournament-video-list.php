<?php include_once('includes/configuration.php');
$page = 'tournament-video-list.html';
set_time_limit(120);ini_set('max_execution_time',120);
$selected_country = getGeoLocationCountry(); 

$error = '';
$tournament_permission = 0;
$match_info = array();
$match_videos = array();
$match_id = trim($_GET['m_id']);
$sql = "SELECT tm.*,c1.company_name as team1_name,c2.company_name as team2_name
FROM tournament_matches as tm 
inner join companies as c1 on tm.team1=c1.id  inner join companies as c2 on tm.team2=c2.id
WHERE tm.id = $match_id and tm.status = 1 ";

$rs = mysqli_query($conn,$sql);
$match_info  = mysqli_fetch_assoc($rs);	

$tournament_id = $match_info['tournament_id'];

$tournament_info = get_record_on_id('tournaments', $tournament_id);	

$page_title = 'Match Videos - '.ucwords($tournament_info['title']);

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
}

if(isset($user_id) && !empty($user_id)){
	$sql = "select * from tournament_permissions where tournament_id = $tournament_id and member_id = $user_id LIMIT 1";
	$rs_permission = mysqli_query($conn,$sql);
	if($tournament_info['user_id'] == $user_id || mysqli_num_rows($rs_permission)>0 ){
		$tournament_permission = 1;
	}else{
		$tournament_permission = 0;
	}
}else{
	$tournament_permission = 0;
}

if(empty($error) && isset($_GET['v_id']) && $_GET['action'] == 'delete'){
	$video_id = trim($_GET['v_id']);
	if($tournament_permission){
		$sql = "DELETE FROM tournament_match_videos WHERE id = $video_id ";
		mysqli_query($conn,$sql);
		$_SESSION['video_deleted'] = 1;
	}
	header("Location:".WWW."tournament/matches/videos/list/".$match_id);
	exit();
}

if(isset($_POST['submit_btn']) && !empty($_POST['submit_btn'])){
	validate();
	if(empty($error)){
		if($_POST['video_type'] == 'upload'){
			$targetFolder = 'video-uploads'; 
			$tempFile = $_FILES['video_file']['tmp_name'];
			$targetPath = $targetFolder;
			$fileParts = pathinfo($_FILES['video_file']['name']);
			$fileName = rand(99,9999).time().'.'.$fileParts['extension'];
			$targetFile = $targetPath . '/' .$fileName ;
			if(!move_uploaded_file($tempFile,$targetFile)){
				$error= 'Error in uploading video';
			}
			$sql = " INSERT INTO tournament_match_videos SET video_file='".trim($fileName)."', video_type = '".trim($_POST['video_type'])."',
			match_id='".$match_id."'";
		}else{
			$sql = " INSERT INTO tournament_match_videos SET embed_code='".mysqli_real_escape_string($conn,trim($_POST['embed_code']))."', video_type = '".trim($_POST['video_type'])."',
			match_id='".$match_id."'";
		}
		
		
		if(empty($error) && mysqli_query($conn,$sql)){
			$_SESSION['video_added'] = 1;
			header("Location:".WWW."tournament/matches/videos/list/".$match_id);
			exit();
		}else{
			$error = '<p id="error" class="alert alert-danger">Error in adding Video. Try again later</p>';
		}
	}
}

function validate(){
	global $error;
	
	if($_POST['video_type'] == 'embed' && empty($_POST['embed_code'])){
		$error.= '<p id="error" class="alert alert-danger">Embed Code is required field</p>';
	}
	if($_POST['video_type'] == 'upload' && empty($_FILES['video_file']['name'])){
		$error.= '<p id="error" class="alert alert-danger">Video File is required field</p>';
	}
	
	$file_types = array('video/mp4','video/webm','video/ogg');
	if(!empty($_FILES['video_file']['name']) && !in_array($_FILES['video_file']['type'],$file_types)){
		$error.= '<p id="error" class="alert alert-danger">These Video File extensions are allowed: mp4, webm, ogg </p>';
	}
	
	$max_size = 1024*1024*10;		//10MB
	if(!empty($_FILES['video_file']['name']) && $_FILES['video_file']['size']>$max_size ){
		$error.= '<p id="error" class="alert alert-danger">Video File of maximum 10MB is allowed </p>';
	}
}

if(isset($_SESSION['video_added']) && $_SESSION['video_added'] ==1){
	$video_added = 1;
	unset($_SESSION['video_added']);
}

if(isset($_SESSION['video_deleted']) && $_SESSION['video_deleted'] ==1){
	$video_deleted = 1;
	unset($_SESSION['video_deleted']);
}

$sql = "SELECT * FROM tournament_match_videos WHERE match_id = $match_id ORDER BY id DESC";
$rs_venues = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_venues)){
	$match_videos[] = $row;
}

?>
<?php include('common/header.php'); ?>

<div class="page-container"> 
		<?php  include('common/user-left-panel.php');?>
      <!-- END SIDEBAR -->
      
      <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
		<div class="page-content">
		<?php  include('common/breadcrumbs.php');?>
		
			<div class="white-box">
            <div class="row">
              <div class="col-md-12">
                
				<h2> Match Videos </h2>
				<h3><?php echo $match_info['team1_name'] ?> vs <?php echo $match_info['team2_name'] ?> - <?php echo date('d F Y',strtotime($match_info['start_time'])); ?></h3>
				<h3><?php echo ucwords($tournament_info['title']); ?></h3>
				
				<?php if(!empty($error)): ?>
					<?php echo $error; ?>
				<?php endif; ?>
				
				<?php if(isset($video_added) && $video_added == 1): ?>
					<div id="information" class="alert alert-success">Video added Successfully... !</div>
				<?php endif; ?>
				
				<?php if(isset($video_deleted) && $video_deleted == 1): ?>
					<div id="information" class="alert alert-success">Video deleted Successfully... !</div>
				<?php endif; ?>
              </div>
            </div>
			
			<div class="clear">&nbsp;</div>
			
			<?php if($tournament_permission){ ?>
            <div id="upload-area">
				<form method="post" enctype="multipart/form-data" class="form-horizontal" >
                <div class="form-group">
                  <label class="col-sm-5 control-label"> Upload Video Or Embed Code: </label>
                  <div class="col-sm-7">
                    <select id="select_type" class="form-control">
                      <option value="video"> Video </option>
                      <option value="embed"> Embed </option>
                    </select>
                  </div>
                </div>
				</form>
				<form method="post" enctype="multipart/form-data" class="form-horizontal req-frm" id="form_upload" >
                
                <div class="form-group" id="add_video">
                  <label class="col-sm-5 control-label"> Upload Video: </label>
                  <div class="col-sm-7">
                    <input name="video_file" id="video_file" class="validate[required]" id="video_upload" accept="video/*" type="file">
                    <span class="help-block">Allowed types are MP4,MOV,OGG,QUICKTIME,WEBM</span>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-5 col-sm-7">
                    <input  value="Upload" name="submit_btn" id="submit_btn" class="btn orange hvr-float-shadow" type="submit">
                  </div>
                </div>
				<input type="hidden" name="video_type" value="upload">
				</form>
				
				<form method="post" enctype="multipart/form-data" class="form-horizontal req-frm" id="form_embed" style="display:none;">
				
                
                <div class="form-group">
                  <label class="col-sm-5 control-label"> Embed Code: </label>
                  <div class="col-sm-7">
                    <textarea name="embed_code" id="upload_embed_code" class="form-control validate[required]"  rows="3"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-5 col-sm-7">
                    <input value="Upload" name="submit_btn" class="btn orange hvr-float-shadow" type="submit">
                  </div>
                </div>
				<input type="hidden" name="video_type" value="embed">
				</form>
              
            </div>
			
			<div class="clear"><br/></div>
            
            <div id="gallery">
              <div class="row">
			  
			   	<?php for($i=0;$i<count($match_videos);$i++){ 
						echo ' <div class="col-sm-3"><div class="embed-responsive embed-responsive-4by3">';
						
						if($match_videos[$i]['video_type'] == 'embed'){
							echo $match_videos[$i]['embed_code'];
						}else{?>
							<?php $video_path = WWW.'video-uploads/'.$match_videos[$i]['video_file']; ?>
							<video width="200" height="130" controls>
							 <source src="<?php echo $video_path; ?>" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
							<source src="<?php echo $video_path; ?>" type='video/ogg; codecs="theora, vorbis"'>
							<source src="<?php echo $video_path; ?>" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
							<source src="<?php echo $video_path; ?>" type='video/webm; codecs="vp8, vorbis"'>
							</video>
						<?php } ?>
						</div>
					<a href="<?php echo WWW; ?>tournament-video-list.php?v_id=<?php echo $match_videos[$i]['id']; ?>&m_id=<?php echo $match_id; ?>&action=delete" onclick="return confirm('Are you sure to delete Video.');" title="Delete Video" class="btn blue full hvr-float-shadow"> <i class="fa fa-times"></i> </a>
                </div>
				<?php } ?>
                
              </div>
            </div>
			
			<?php }else{ ?>
				<div id="error" class="alert alert-danger">You do not have Permission for this Tournament... !</div>
			<?php } ?>
				
          </div>
		
		
		</div>
	</div>
</div>	


<script>



$(document).ready(function(){
	$('.req-frm').validationEngine();
	$("#select_type").bind('change',function(){
		if($(this).val() == 'embed'){
			$("#form_embed").show('slow');
			$("#form_upload").hide('slow');
		}else{
			$("#form_embed").hide('slow');
			$("#form_upload").show('slow');
		}	
	});	
	
	$("#submit_btn").bind('click',function(){
		if($("#select_type").val() == 'video'){
			var file_size = $("#video_file")[0].files[0].size/(1024*1024);
			if(file_size>10){
				alert('File size is greater than 10MB');
				return false;
			}
		}
		
		
		return true;
	});
	
});	
</script>

<?php include('common/footer.php'); ?>