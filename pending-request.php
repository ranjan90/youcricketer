<?php include('common/header.php'); ?>
<? 	if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>

<div class="page-container"> 
		<?php  include('common/user-left-panel.php');?>
      <!-- END SIDEBAR -->
      
      <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
		<div class="page-content">
		
		
		<div class="white-box">
            <div class="row">
              <div class="col-sm-12">
                <h2> Pending Friendship Requests </h2>
              </div>
            </div>
            
            <form method="post" action="" enctype="multipart/form-data" class="form-horizontal">
              <div class="form-group" id="friends">
                
				<?		$userId 	= $_SESSION['ycdc_dbuid'];
			        		$rsFriends 	= mysqli_query($conn,"select * from friendship where to_user_id = '$userId' and status = '0' "); 
			        		if(mysqli_num_rows($rsFriends) > 0){
			        			//echo '<h3>Pending Requests</h3>';
			        		}else{
			        			echo '<div id="error" class="alert alert-info"><i class="fa fa-exclamation-triangle"> </i> No Pending Friendship Requests ... !</div>';
			        		}
			        		while($rowF = mysqli_fetch_assoc($rsFriends)){ 
			        			if($rowF['from_user_id'] == $userId){
			        				$friendId 	= $rowF['to_user_id'];
			        			}else{
			        				$friendId 	= $rowF['from_user_id'];
			        			}
			        			$rowUser = get_record_on_id('users', $friendId);
			        			$row_img = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$friendId."' and is_default = '1' "));
			    ?>
				
				<div class="col-sm-3">
                  <div id="friend">
                    <a href="<?=WWW?>individual-detail-<?=$rowUser['id']?>-<?=friendlyURL($rowUser['f_name'].' '.$rowUser['last_name'])?>.html#activity-tab" title="<?=$rowUser['f_name'].' '.$rowUser['last_name']?>">
                      <img style="max-height:120px;" src="<?php echo WWW;?><?=($row_img && !empty($row_img['file_name']))?'users/'.$rowUser['id'].'/photos/'.$row_img['file_name']:'images/no-photo.jpg'?>" >
                    </a>
                    <p><?php echo $rowUser['f_name'].' '.$rowUser['last_name'];?></p>
                    <input type="checkbox" name="friend" id="<?=$rowF['id']?>" class="accept-fr"> Accept <input type="checkbox" name="friend" id="<?=$rowF['id']?>" class="deny-fr"> Deny 
                  </div>
                </div>
                
                <?php } ?>
                
              </div>
			  
            </form>
        </div>
		
		</div>
	</div>
</div>	


<script>
	var WWW = "<?=WWW?>";
	$(document).ready(function(){
		//$('#friend input[type=checkbox]').click(function(){
		$('#friend .accept-fr').click(function(){
			var id = $(this).attr('id');
			if($('input[type=checkbox]#'+id+':checked').length == '1'){
				// checked
				$.ajax({type	: 'POST', 
						url		: WWW + 'includes/update-friendship.php', 
						data	: ({type:'makeFriend',id:id}),
						success	: function(msg){
							window.location = WWW + 'pending-request.html';
						}
					});
			}else{
				// unchecked
				$.ajax({type	: 'POST', 
						url		: WWW + 'includes/update-friendship.php', 
						data	: ({type:'makeUnfriend',id:id}),
						success	: function(msg){
							window.location = WWW + 'pending-request.html';
						}
					});
			}
		});
		
		$('#friend .deny-fr').click(function(){
			var id = $(this).attr('id');
			$.ajax({type	: 'POST', 
					url		: WWW + 'includes/update-friendship.php', 
					data	: ({type:'makeUnfriend',id:id}),
					success	: function(msg){
						window.location = WWW + 'pending-request.html';
					}
			});
		});
		
		
	});
</script>

<?php include('common/footer.php'); ?>