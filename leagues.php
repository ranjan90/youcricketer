<?php include('common/header.php'); ?>
	<div class="middle">
		<h1> Leagues <?=(isset($_GET['type']) && $_GET['type'] == 'your_region')?'In Your Region':'';?></h1>
		<div class="white-box content">
			<div id="pagination-top"></div>
			<div class="list">
                <ul>
                	<?  $rpp = PRODUCT_LIMIT_FRONT; // results per page
            			$ppage = intval($_GET["page"]);
      					if($ppage<=0) $ppage = 1;
      					$select = "select * from leagues l";
      					$where  = " where l.status=1 and l.id not in (select league_id from users_to_leagues)";
				      	//=======================================
					    if(isset($_GET['keywords']) && $_GET['keywords'] != 'Search Here'){
					        $where  .= "and title like '%".$_GET['keywords']."%'";
					    }
					    if(isset($_GET['type']) && $_GET['type'] == 'your_region'){
					    	$selected_country = getGeoLocationCountry();
								
						}
							$row_country = mysqli_fetch_assoc(mysqli_query($conn,"select * from countries where name = '$selected_country'"));
							$join .= " join users u on u.id = l.user_id and u.country_id = '".$row_country['id']."'";
					    }
					    $query = $select.$join.$where ." order by l.id desc ";
				      //echo $query;
				      //=======================================
				      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
				        echo '<div id="error">Your search returned no results</div>';
				      }
				      $rs   = mysqli_query($conn,$query);
				      $tcount = mysqli_num_rows($rs);
				      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
				      $count = 0;
				      $i = ($ppage-1)* $rpp;
          			  $x = 0;
          			  //=======================================
      				  while(($count<$rpp) && ($i<$tcount)){
                		mysqli_data_seek($rs,$i);
                		$row_g 	= mysqli_fetch_assoc($rs);
                		$row_img= mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'leagues' and entity_id = '".$row_g['id']."'"));
      					?>
      					<li>
							<a href="<?=WWW?>league-detail-<?=$row_g['id']?>-<?=friendlyURL($row_g['title'])?>.html" title="Read more" />
								<img src="<?=($row_img)?WWW.'leagues/'.$row_g['id'].'/'.$row_img['file_name']:WWW.'images/community-groups.png';?>" alt="<?=$row_g['title']?>" title="<?=$row_g['title']?>" width="114" />
							</a>
	                        <span class="list-text">
	                            <h3 onclick="window.location='<?=WWW?>league-detail-<?=$row_g['id']?>-<?=friendlyURL($row_g['title'])?>.html'"><?=$row_g['title']?></h3>
	                            <p><?=truncate_string($row_g['content'], 140)?></p>
	                            <a href="<?=WWW?>league-detail-<?=$row_g['id']?>-<?=friendlyURL($row_g['title'])?>.html" title="Read more" class="submit-login" />Read More</a>
	                        </span>
	                        <span class="list-social">
	                         	<!-- AddThis Button BEGIN -->
						        <div class="addthis_toolbox addthis_default_style ">
						        <a class="addthis_button_preferred_1"></a>
						        <a class="addthis_button_preferred_2"></a>
						        <a class="addthis_button_preferred_3"></a>
						        <a class="addthis_button_preferred_4"></a>
						        <a class="addthis_button_compact"></a>
						        <a class="addthis_counter addthis_bubble_style"></a>
						        </div>
						        <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
						        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5073efa669740139"></script>
						        <!-- AddThis Button END -->
	                         	<br> <br> <br>
	                            <?=date_converter($row_g['create_date'])?>
	                        </span>
						</li>
					    <?
					      $i++;
					      $count++;
					      $x++;
					  } 
					  ?>
			</ul>
			</div>
			<div id="pagination-bottom">
				<? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_FRONT){ ?>
		      	<?php
		        	$reload = 'leagues.html?';
		        	echo paginate_one($reload, $ppage, $tpages);
		      	?>
		      	<input type="hidden" name="pagination-page" value="leagues.html">
		        <? } ?>    
		        <form id="list-search" method="post" action="">
		        	<input type="text" name="txtsearch" placeholder="Search Here" class="validate[required] input-login">
		        	<input type="submit" value="">
		        </form>
		        <script type="text/javascript">
		        $('form#list-search').submit(function(e){
					var parentId = $(this).parent().attr('id');
					var string = $('#'+parentId +' form input[name=txtsearch]').val();
					if(string != '' && string != 'Search Here'){
						string = string.replace(/[^a-zA-Z0-9]+/g,'').toLowerCase();
						if(string.length > 0){
							$('form#list-search').attr('action','<?=WWW;?>leagues-' + string + '.html');
						}
					}
				});
		        </script>
		        <div class="clear"></div>
		    </div>  
		    <div class="clear"></div>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>
<?php include('common/footer.php'); ?>