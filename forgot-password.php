<?php include('common/header.php'); ?>
<?php $site_key = '6LdcVRETAAAAAD91vVdDOQLo_bhqq-3MUyHBi2nS';
$secret_key = '6LdcVRETAAAAAJgLM7q9C6SqsXHWwt6B2ccePxHo';
?>

<?php 	if(isset($_POST) && !empty($_POST)){
				$captcha_error = false;
				if(isset($_POST['captcha_added']) && !empty($_POST['captcha_added'])){
					if(isset($_POST['g-recaptcha-response'])){
						$captcha=$_POST['g-recaptcha-response'];
						$response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret_key&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
						$response = json_decode($response,true);
						if($response['success'] == false){
							$captcha_error = true;
							echo '<div class="alert alert-danger">Invalid Human Verification</div>';
						}
					}else{
						echo '<div  class="alert alert-danger">Invalid Human Verification</div>';
						$captcha_error = true;
					}
				}
				
				if(!$captcha_error){
					$username 	= $_POST['email'];
					$rs_user	= mysqli_query($conn,"select * from users where email = '$username'");
					
					if(mysqli_num_rows($rs_user) == 0){
						echo '<div id="error" class="alert alert-danger">Invalid email address ... !</div><br><br>';	
					}else{
						$row_u = mysqli_fetch_assoc($rs_user);
						$password = getRandomWord(8);

						$email_template = get_record_on_id('cms', 2);
						$mail_title		= $email_template['title'];
						$mail_content	= $email_template['content'];
						$mail_content 	= str_replace('{name}',$row_u['f_name'], $mail_content);
						$mail_content 	= str_replace('{email}',$username, $mail_content);
						$mail_content 	= str_replace('{password}',$password, $mail_content);
						$mail_content 	= str_replace('{login_link}','<a href="'.WWW.'login.html" title="Login Here">HERE</a>', $mail_content);
						$mail_content 	= str_replace('{contact_link}','<a href="'.WWW.'contact-us.html" title="Contact Us">HERE</a>', $mail_content);
						$mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);

						$headers = "Content-type:text/html;charset=UTF-8" . "\r\n";
						$headers .= "From: no-reply@youcricketer.com" . "\r\n";


						$query = "update users set password = '".md5($password)."' where email = '$username'";
						mysqli_query($conn,$query);
						mail($username,$mail_title,$mail_content,$headers);
						echo '<div id="success" class="alert alert-success"><b>Success : </b>Password Reset email has been sent. Please check your email and follow the instructions in it. If u do not see the email in your inbox check your Spam/Junk folder and mark it safe.. !</div><br><br>';
					}
				}
			}
		?>
		
<div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1> Forgot Password </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <div class="white-box">
            <div class="row">
              <div class="col-md-8">
                <h2>Enter Information</h2>
                <form method="post" action="" class="form-horizontal" id="forgot-password-frm">
                  <input name="timezone" value="America/Los_Angeles" type="hidden">
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Email Address </label>
                    <div class="col-sm-9">
                      <input name="email" value="<?php echo $_POST['email'] ?>" class="form-control validate[required, custom[email]]" type="text">
                    </div>
                  </div>
                 
				 
				  <div class="form-group">
                    <label class="col-sm-3 control-label">Human Verification  </label>
                    <div class="col-sm-9">
						<script src="https://www.google.com/recaptcha/api.js"></script>
						<div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
						<input type="hidden" name="captcha_added" value="1">
                    </div>
                  </div>
				  
                 
                  <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                      <input value="Submit" class="btn orange hvr-float-shadow" type="submit">
                    </div>
                  </div>
                </form>
              </div>
             
            </div>
		  </div>
        </div>
        
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
				<?php include('common/right-panel.php');?>
            </div>
          </div>
          
        </div>
      </div>
    </div><!-- /.container -->

<script type="text/javascript">
$(document).ready(function(){
	$('#forgot-password-frm').validationEngine();
});
</script>
<?php include('common/footer.php'); ?>