<?php include('common/header.php');?>

	<div class="container-fluid">

		<div class="row" >
			<div class="col-md-12">
			  <div id="activity-slider">
				<p>Latest Greatest </p>
				<a href="<?php echo WWW ?>latest-greatest.html" ><?php $rowWhoopsMsg = get_record_on_id('cms', 45); echo $rowWhoopsMsg['content'];?></a>
			  </div>
			</div>
		</div>

      <div class="row">
        <div class="col-md-3">
          <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
              <div class="item active">
                <img src="<?php echo WWW;?>assets/img/top-right/map.jpg" alt="Map" />
              </div>
              <div class="item">
                <img src="<?php echo WWW;?>assets/img/top-right/express.jpg" alt="Express" />
              </div>
              <div class="item">
                <img src="<?php echo WWW;?>assets/img/top-right/connected.jpg" alt="Connected" />
              </div>
              <div class="item">
                <img src="<?php echo WWW;?>assets/img/top-right/shine.jpg" alt="Shine" />
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
			<div id="myCarousel2" class="carousel slide" data-ride="carousel">

			<div class="carousel-inner" role="listbox">
				<?php $rs = mysqli_query($conn,"select * from sliding_banners where status = '1' order by sort_order");
					$count = 0;
					while($row_banner = mysqli_fetch_assoc($rs)){ ?>
						<div class="item <?php if($count == 0) echo 'active'; ?>">
						<a target="_blank" href="<?php echo $row_banner['web_url']?>" title="<?php echo $row_banner['title']?>">
							<img src="<?php echo WWW;?>banners/<?php echo $row_banner['content']?>" alt="<?php echo $row_banner['title']?>" />
						</a>
						</div>
				<?php $count++; } ?>
             </div>
			<ol class="carousel-indicators">
				<?php for($i=0;$i<$count;$i++): ?>
					<li data-target="#myCarousel2" data-slide-to="<?php echo $i; ?>" <?php if($i==0): ?> class="active" <?php endif; ?>></li>
				<?php endfor; ?>
            </ol>
			</div>
        </div>
		<div class="col-md-3">
          <div id="myCarousel3" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
              <?php $rs = mysqli_query($conn,"select * from small_sliding_banners where status = '1' order by sort_order");
					$count = 0;
					while($row_banner = mysqli_fetch_assoc($rs)){ ?>
						<div class="item <?php if($count == 0) echo 'active'; ?>">
						<a target="_blank" href="<?php echo $row_banner['web_url']?>" title="<?php echo $row_banner['title']?>">
							<img src="<?php echo WWW;?>small-banners/<?php echo $row_banner['content']?>" alt="<?php echo $row_banner['title']?>" />
						</a>
						</div>
				<?php $count++; } ?>
            </div>
          </div>
        </div>

      </div>


      <div class="row" id="home_page_panel">
        <div class="col-md-7">
          <div class="white-box_listting">
            <ul class="menu" id="menu">
              <li><a href="#" id="individual-tab" class="active">Individuals</a></li>
              <li><a href="#" id="companies-tab">Clubs/Leagues/Organizations</a></li>
			</ul>
			<div class="content1" id="individual">
              <fieldset style="height:1288x; overflow:hidden;">
				<?php
			  			$query 	= "select distinct id,f_name,m_name,last_name,user_type_id,type,country_id,permalink from users u where is_company = '0' and u.status = 1 order by rand() limit ".HOMEPAGE_USERS_LIMIT; //create_date >= '$oneMonthAgo' and
			  			$rs_users = mysqli_query($conn,$query);
			  			while($row= mysqli_fetch_assoc($rs_users)){

								if($row['permalink']!=''){
								  $pLink=WWW."yci/".$row['permalink'];
								}else{
								  $pLink=WWW."individual-detail-".$row['id']."-".friendlyURL($row['f_name']." ".$row['m_name']." ".$row['last_name']).".html";
								}
			  				$row_img = mysqli_fetch_assoc(mysqli_query($conn,"select id,file_name from photos where entity_type = 'users' and entity_id = '".$row['id']."' and is_default = '1' "));
			  				$row_country= get_record_on_id('countries', $row['country_id']);
			  				$location 	= $row_country['name'];
			  				$row_vid = mysqli_fetch_assoc(mysqli_query($conn,"select id,file_name from videos where entity_id = '".$row['id']."' and entity_type = 'users' and is_default = '1' and file_name != ''"));
				?>
                <dl>
                  <dt><a href="<?php echo $pLink; ?>" title="<?php echo $row['f_name'].' '.$row['m_name'].' '.$row['last_name']?>"><img src="<?php echo WWW;?><?php echo ($row_img && !empty($row_img['file_name']))?'users/'.$row['id'].'/photos/'.$row_img['file_name']:'images/no-photo.jpg'?>" alt="no photo" width="120" height="128" /></a></dt>
                  <dd>
                    <div class="details">
						<h3 style="cursor:pointer;" onclick="window.location='<?php echo $pLink; ?>'">
							<?php echo truncate_string($row['f_name'],20).'&nbsp;<br>'.truncate_string($row['last_name'], 20)?>
						</h3>
						<p><? echo $location;?><br />
						<?if($row['user_type_id'] != 1){
							echo get_combo('user_types','name',$row['user_type_id'],'','text');
						?><br />
						<?   if($row['user_type_id'] == 2){ echo $row['type'];} }?> <br/>
						</p>

						<a class="submit-login margin-top-5" href="<?php echo $pLink; ?>" title="<?php echo $row['f_name'].' '.$row['m_name'].' '.$row['last_name'].' - '.$site_title?>">View Profile</a>
                      <span class="flag"><img width="25" title="<?php echo $row_country['name']?>" alt="<?php echo $row_country['name']?>" src="<?php echo WWW;?>countries/<?php echo $row_country['flag']?>" ></span>
                    </div>
                    <div class="video">
					<?
						$video = $row_vid['file_name'];
						if(!empty($video)){
							if(preg_match('/<iframe(.*)<\/iframe>/', $row_vid['file_name'])){
								preg_match('/src="(.*?)"/',$row_vid['file_name'] , $src);
								$src = $src[1];
								$video = "<iframe width='205' height='130' frameborder='0' allowfullscreen src='$src'></iframe>";
							}else{
								$filename = explode('.',$video);
								$filename1= $filename[0];
								$video = '<video width="220" height="130" controls>
										<source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mp4" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
										<source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.ogg" type=\'video/ogg; codecs="theora, vorbis"\'>
										<source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mov" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
										<source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.webm" type=\'video/webm; codecs="vp8, vorbis"\'>
										</video>';
							}
						}else{
								$video = '<img src="'.WWW.'images/no-video.jpg" alt="no video" width="168" height="130">';
						}
						echo $video;?>
					</div>
                  </dd>
                </dl>

               <?	} ?>
              </fieldset>
              <div class="view-btn">
                <a class="btn orange hvr-float-shadow" title="View all Individuals" href="<?php echo WWW?>individuals.html">View All</a>
              </div>
			</div>
			<div style="display: none;" class="content1" id="companies">
              <fieldset style="height:1288x; overflow:hidden;">
				<?php $rs_users = mysqli_query($conn,"select c.*,co.name as country_name from companies c left join countries as co on c.country_id=co.id where c.status = 1 order by c.id desc limit ".HOMEPAGE_USERS_LIMIT);
			  			while($row= mysqli_fetch_assoc($rs_users)){
			  				$row_img = mysqli_fetch_assoc(mysqli_query($conn,"select id,file_name from photos where entity_type = 'users' and entity_id = '".$row['user_id']."' and is_default = '1' "));
			  				$row_u   = mysqli_fetch_assoc(mysqli_query($conn,"select city_id,country_id from users where id = '".$row['user_id']."'"));
			  				$row_vid = mysqli_fetch_assoc(mysqli_query($conn,"select id,file_name from videos where entity_id = '".$row['user_id']."' and entity_type = 'users' and is_default = '1' "));
			  				if(!empty($row_u['city_id'])){
			  					$row_city   = get_record_on_id('cities', $row_u['city_id']);
				            	$row_states = get_record_on_id('states', $row_city['state_id']);
				            	$row_country= get_record_on_id('countries', $row['country_id']);
				            	if(!$row_country){
				            		$row_country= get_record_on_id('countries', $row_u['country_id']);
				            	}
				            	$location   =  $row_city['name'].' > '.$row_states['name'].' > '.$row_country['name'];
			  				}else{
			  					$row_country= get_record_on_id('countries', $row['country_id']);
			  					$location   = $row_country['name']	;
			  				}
			  				$location   = $row_country['name']	;
				?>
                <dl>
                  <dt><a href="<?php echo WWW?><?php echo $row['company_permalink']?>" title="<?php echo $row['company_name']?>"><img src="<?php echo WWW;?><?php echo ($row_img && !empty($row_img['file_name']))?'users/'.$row['user_id'].'/photos/'.$row_img['file_name']:'images/no-photo.png'?>" alt="no photo" width="120" height="128" /></a> </dt>
                  <dd>
                    <div class="details">
						<h3 style="height:22px; cursor:pointer;" onclick="window.location='<?php echo WWW?><?php echo $row['company_permalink']?>'"><?php echo $row['company_name']?></h3>
						<p><?php echo get_combo('company_types','name',$row['company_type_id'],'','text')?></p>
						<p><? echo $location;?></p>
						<a  href="<?php echo WWW?><?php echo $row['company_permalink']?>" title="<?php echo $row['company_name'].' - '.$site_title?>">View Profile</a>
                      <span class="flag"><img width="25" alt="<?php echo $row_country['name']?>" title="<?php echo $row_country['name']?>" src="<?php echo WWW;?>countries/<?php echo $row_country['flag']?>"></span>
                    </div>
                    <div class="video">
						<?php $video = $row_vid['file_name'];
							if(!empty($video)){
								if(preg_match('/<iframe(.*)><\/iframe>/', $video)){
							        preg_match('/src="(.*?)"/',$row_vid['file_name'] , $src);
									$src = $src[1];
									$video = "<iframe width='205' height='130' frameborder='0' allowfullscreen src='$src'></iframe>";
								}else{
									$video_path = WWW.'videos/'.$row_vid['id'].'/'.$row_vid['file_name'];
									$video = '<video width="150" height="130" controls>
									<source src="'.$video_path.'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
									<source src="'.$video_path.'" type=\'video/ogg; codecs="theora, vorbis"\'>
									<source src="'.$video_path.'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
									<source src="'.$video_path.'" type=\'video/webm; codecs="vp8, vorbis"\'>
									</video>';
								}
							}else{
								$video = '<img src="'.WWW.'images/no-video.jpg" alt="no video" width="168" height="130">';
							}
						echo $video;
						?>
					</div>
                    <div class="clear"></div>
                  </dd>
                </dl>
				<?php } ?>

              </fieldset>
              <div class="view-btn">
                <?php if(isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])): ?>
						<a href="<?php echo WWW?>advance-search.html#companies-tab" title="View all Companies" class="btn orange hvr-float-shadow">View All</a>
				<?php else: ?>
						<a href="<?php echo WWW?>companies-sign-up.php" title="View all Companies" class="btn orange hvr-float-shadow">View All</a>
				<?php endif; ?>
              </div>
            </div>
			<div class="clear"></div>
		  </div>
		  <div class="clearfix"></div>
        </div>
        <div class="col-md-3">
          <div class="row">
            <div class="col-sm-12">
              <div class="white-box_listting">
                <ul id="menu2" class="menu">
				  <li><a href="#" id="news-tab" class="active">News</a></li>
				  <li><a href="#" id="scoreboard-tab">Score</a></li>
				</ul>

				<div id="news" class="content1" style="min-height:330px;">
					<div id="news_content" style="min-height:400px;"><?php //include 'espn/news.php';?></div>
				<a href="<?php echo WWW;?>espn-news.html" class="btn orange hvr-float-shadow"> View All </a>
				</div>

				<div id="scoreboard" class="content1 " style="min-height:240px;display:none;">
					<div id="score_content"><?php //include 'espn/score.php';?></div>
				</div>

				<div class="clear"></div>

              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
				<div class="white-box_listting">
					<ul id="menu3" class="menu">
					  <li><a href="#" id="result-tab" class="active">Results</a></li>
					  <li><a href="#" id="blog-tab">Fixtures</a></li>
					</ul>

					<div id="result" class="content1 ">
						<div class="slide10" id="slide10"> <img id="minus10" alt="International" src="<?php echo WWW?>images/icons/minus-small-white-icon.png">International </div>
						<div id="results_content" style="min-height:300px;"><?php //include('espn/espn_results.php');?></div>
						<a href="<?php echo WWW?>espn-results.php" class="btn orange hvr-float-shadow"> View All </a>
						<?php //echo results($text);?>
					</div>
					<div id="blog" class="content1" style="min-height:240px; display:none;">
						<div class="slide10" id="slide11"> <img id="minus11" alt="International" src="<?php echo WWW?>images/icons/minus-small-white-icon.png">International </div>
						<div id="fixtures_content"><?php //include('espn/espn_fixtures.php');?></div>
						<a href="<?php echo WWW?>espn-fixtures.php" class="btn orange hvr-float-shadow"> View All </a>
					</div>

				</div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <div class="white-box_listting">
                <ul id="menu4" class="menu">
                  <li><a href="#" id="facebook-tab" class="active">Facebook</a></li>
                  <li><a href="#" id="twitter-tab">Twitter</a></li>
			    </ul>
				<div id="facebook" class="content1" style="min-height:400px; #min-height:400px;">
                  <!--<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2FYouCricketer&amp;width=250&amp;height=400&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=true&amp;show_border=true"   style="border:none; overflow:hidden; width:100%; height:400px;"></iframe>-->
				  <div id="fb-root"></div>
					<script>(function(d, s, id) {
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id)) return;
					js = d.createElement(s); js.id = id;
					js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.12&appId=1927636410858320&autoLogAppEvents=1';
					fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));</script>
					<div class="fb-page" data-href="https://www.facebook.com/YouCricketer/" data-tabs="timeline" data-width="400" data-height="400" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/YouCricketer/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/YouCricketer/">YouCricketer</a></blockquote></div>
                </div>
				<div id="twitter" class="content1" style="min-height:400px; display:none;">
				  <h2>Twitter</h2>
				  <a class="twitter-timeline" href="https://twitter.com/YouCricketer" data-widget-id="409331659427700736">Tweets by @YouCricketer</a>
                  <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				</div>
              </div>
            </div>
          </div>
		   <div class="clearfix"></div>
        </div>
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
				<?php include('common/right-panel.php');?>
            </div>
          </div>
 <div class="clearfix"></div>
        </div>
      </div>
    </div><!-- /.container -->

<script type="text/javascript">
	$(document).ready(function(){
		$("#news_content").html('<p>Loading News... </p>');
		$("#score_content").html('<p>Loading Scores... </p>');
		$("#results_content").html('<p>Loading Results... </p>');
		$("#fixtures_content").html('<p>Loading Fixtures... </p>');

        $.ajax({
			url: "<?php echo WWW; ?>espn/news.php?t=<?php echo time(); ?>",
			type:"GET",
			success: function(data) {
				$("#news_content").html(data);
			}
		});

		$.ajax({
			url: "<?php echo WWW; ?>espn/score.php?t=<?php echo time(); ?>",
			type:"GET",
			success: function(data) {
				$("#score_content").html(data);
			}
		});

		$.ajax({
			url: "<?php echo WWW; ?>espn/espn_results.php?t=<?php echo time(); ?>",
			type:"GET",
			success: function(data) {
				$("#results_content").html(data);
			}
		});

		$.ajax({
			url: "<?php echo WWW; ?>espn/espn_fixtures.php?t=<?php echo time(); ?>",
			type:"GET",
			success: function(data) {
				$("#fixtures_content").html(data);
			}
		});
		$(".add-stat").on('click',function(e){
			e.preventDefault();
			var id = this.id;
			var location = $(this).attr('href');
			window.open(location);
			var url = '<?=WWW?>add-stats.php?ad_id='+id+'&type=<?=RIGHT_SIDE_AD?>';

			$.ajax({
				url: url,
				type:"GET",
				success: function(data) {
					console.log($data);
				}
			});
		});

	});
</script>

<?php include('common/footer.php'); ?>
