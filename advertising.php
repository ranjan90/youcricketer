<?php include('common/header.php'); ?>

<div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1>Advertising</h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <div class="white-box">
           
			<?php if(!isset($_SESSION['ycdc_dbuid'])){ ?>
				<?php $row = get_record_on_id('cms',17);?>
				<?php echo $row['content']?>
				<div class="clear"></div>
				<?php include('common/login-box.php');?>
			<?php } ?>
			<?php if(isset($_SESSION['ycdc_dbuid'])){ ?>
				<?php $row = get_record_on_id('cms',25); echo $row['content']?>
				<p> Please contact our Advertising Department through the <a href="<?php echo WWW?>contact-us.html" title="Click here for Contact Page">Contact Page.</a></p>
			<?php } ?>
		    
		  </div>
        </div>
        
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
             <?php include('common/right-panel.php');?>
            </div>
          </div>
          
        </div>
      </div>
</div><!-- /.container -->
	
<?php include('common/footer.php'); ?>