<?php include('common/header.php'); ?>

<div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1>Our Team </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <div class="white-box">
            <?php $team = mysqli_query($conn,"select * from team where status = '1' order by sort_order");
                  while($row = mysqli_fetch_object($team)){ ?>
                  <div class="member col-md-3">
                    <img src="<?php echo WWW;?>team/<?php echo $row->photo?>" width="100">
                  <h3><?php echo $row->name?></h3>
                  <p><?php echo $row->designation?></p>
                  </div>
              <?php } ?>
              <div class="clear"></div>
              <p align="center"><a href="<?php echo WWW;?>careers.html" title="Click to see Careers">Join our team</a></p>
              <style>
              .team{}
              .member{border:1px solid #ccc; border-radius:4px; padding:5px; margin:5px; float:left; width:32%; height:135px;}
              .member img{float:left; margin:5px; max-height: 120px;}
              .member .rightbox{float: left; margin-top:10px;}
              </style>
		      </div>
        </div>
        
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
             <?php include('common/right-panel.php');?>
            </div>
          </div>
          
        </div>
      </div>
</div><!-- /.container -->

<?php include('common/footer.php'); ?>