<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>

<div class="page-container"> 
	<?php include('common/user-left-panel.php');?>
	
	<!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
          
            <div class="white-box">
				<div class="row">
				  <div class="col-sm-12">
					<h2> Identity Card (<?=$_SESSION['ycdc_user_name']?>)  </h2>
				  </div>
				</div>
				<?php $row_type = get_record_on_id('user_types', $row_user['user_type_id']); ?>
				<?php if($row_type['name'] == 'Company'){ 
						$row_company = mysqli_fetch_assoc(mysqli_query($conn,"select * from companies where user_id = '".$row_user['id']."'"));
				}?>
				<? 	if($row_company){
						$row_country = get_record_on_id('countries', $row_company['country_id']);
					}else{
						$row_country = get_record_on_id('countries', $row_user['country_id']);
					}
				?>
				<? $row_state 	= get_record_on_id('states', $row_user['state_id']);?>
				<div id="identity-card">
					<div class="row">
						<div class="col-sm-6 identity-card-logo">
						  <div class="">
							<img alt="Logo" src="<?=WWW?>assets/img/logo.png">
							<p>Connect  &amp; Discover Cricket Opportunities Globally </p>
						  </div>
						</div>
						<div class="col-sm-6 identity-card-country">
						  <img src="<?=WWW?>countries/<?=$row_country['flag']?>">
						  <?=$row_country['name'];?><br>
						  <?=$row_state['name'];?><br>
						  <?=$row_user['city_name'];?>
						</div>
					</div>
					<div class="row">
					<div class="col-sm-6 identity-card-name">
					<?php if($row_type['name'] == 'Company'){ 
						$row_company = mysqli_fetch_assoc(mysqli_query($conn,"select * from companies where user_id = '".$row_user['id']."'")); ?>
						<?php echo $row_company['company_name']?><br>
						<?php echo $row_user['f_name'] . ' ' . $row_user['last_name']?><br>
						<?php echo WWW.$row_company['company_permalink']; ?>
					<?php }else{ ?>  
						<?=$row_user['f_name']." ".$row_user['last_name']?> (<?=get_combo('user_types', 'name', $row_user['user_type_id'],'','text')?>)
						<br><?php echo WWW?>yci/<?php echo $row_user['permalink']; ?>
					<?php } ?>
					  
					</div>
					<div class="col-sm-6 identity-card-img">
					<? 	$row_photo = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_id = '".$row_user['id']."' and entity_type = 'users' and is_default = '1'"));
								if($row_photo){
									$image = 'users/'.$row_user['id'].'/photos/'.$row_photo['file_name'];
								}else{
									$image = 'images/no-photo.png';
								}
					?>
					  <img src="<?=WWW?><?=$image?>">
					</div>
					</div>
            </div>
				
			</div>
		  
		  
        </div>
      </div>
      <!-- END CONTENT-->
		
</div>
<?php include('common/footer.php'); ?>