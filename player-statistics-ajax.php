<?php include_once('includes/configuration.php');

$league_info = array();
$matches = array();
$user_tournaments = array();
$error = '';
$start_record = 0;
$records_per_page = 200;


if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

$where_sql = '';
$where_sql_sub = '';
$where_sql_bat = '';
$where_sql_bowl = '';

$where_sql = " AND  tm.winning_team_id != '' ";
$where_sql_sub.=" AND  tournament_matches.winning_team_id != '' ";

if(isset($_POST['league_id']) && !empty($_POST['league_id'])){
	$where_sql.=" AND t.league_id = ".$_POST['league_id'];
	$where_sql_sub.=" AND tournaments.league_id = ".$_POST['league_id'];
}
if(isset($_POST['year']) && !empty($_POST['year'])){
	$where_sql.=" AND t.year = ".$_POST['year'];
	$where_sql_sub.=" AND tournaments.year = ".$_POST['year'];
}
if(isset($_POST['tour_id']) && !empty($_POST['tour_id'])){
	$where_sql.=" AND tm.tournament_id = ".$_POST['tour_id'];
	$where_sql_sub.=" AND tournament_matches.tournament_id = ".$_POST['tour_id'];
}
if(isset($_POST['game_type']) && !empty($_POST['game_type'])){
	$where_sql.=" AND t.overs_type = '".$_POST['game_type']."'";
	$where_sql_sub.=" AND tournaments.overs_type = '".$_POST['game_type']."'";
}
if(isset($_POST['overs_type']) && !empty($_POST['overs_type'])){
	$where_sql.=" AND t.overs_count = ".$_POST['overs_type'];
	$where_sql_sub.=" AND tournaments.overs_count = ".$_POST['overs_type'];
}
if(isset($_POST['ball_type']) && !empty($_POST['ball_type'])){
	$where_sql.=" AND t.ball_type = '".$_POST['ball_type']."'";
	$where_sql_sub.=" AND tournaments.ball_type = '".$_POST['ball_type']."'";
}
if(isset($_POST['player_id']) && !empty($_POST['player_id'])){
	$where_sql.=" AND tbs.batsman_id = ".$_POST['player_id'];
	$where_sql_bat.=" AND tournament_bat_scorecard.batsman_id = ".$_POST['player_id'];
	$where_sql_bowl.=" AND tournament_bowl_scorecard.bowler_id = ".$_POST['player_id'];
}


$sql = "SELECT u.f_name, u.m_name,u.last_name,c1.company_name as team_name,c1.company_permalink, tbs.batsman_id,COUNT(DISTINCT tbs.match_id) AS matches_played,c1.id as company_id,
SUM(DISTINCT tbs.runs_scored) as total_runs_scored,SUM(DISTINCT tbs.balls_played) as total_balls_played,MAX(runs_scored) as highest_runs,p.file_name,
SUM(DISTINCT tbs.fours_scored) as total_fours_scored,SUM(DISTINCT tbs.sixes_scored) as total_sixes_scored,
(SELECT COUNT(tournament_bat_scorecard.id)  FROM  tournament_bat_scorecard INNER JOIN tournament_matches on tournament_bat_scorecard.match_id=tournament_matches.id INNER JOIN tournaments on tournament_matches.tournament_id=tournaments.id WHERE batsman_id = tbs.batsman_id AND (runs_scored>=50 AND runs_scored<100) $where_sql_sub $where_sql_bat) AS fiftys,
(SELECT COUNT(tournament_bat_scorecard.id)  FROM  tournament_bat_scorecard INNER JOIN tournament_matches on tournament_bat_scorecard.match_id=tournament_matches.id INNER JOIN tournaments on tournament_matches.tournament_id=tournaments.id WHERE batsman_id = tbs.batsman_id AND runs_scored>=100 $where_sql_sub $where_sql_bat) AS hundreads,
(SELECT COUNT(tournament_bat_scorecard.id)  FROM  tournament_bat_scorecard INNER JOIN tournament_matches on tournament_bat_scorecard.match_id=tournament_matches.id INNER JOIN tournaments on tournament_matches.tournament_id=tournaments.id WHERE batsman_id = tbs.batsman_id AND how_out != 'DNB' $where_sql_sub $where_sql_bat) AS innings,
(SELECT COUNT(tournament_bat_scorecard.id)  FROM  tournament_bat_scorecard INNER JOIN tournament_matches on tournament_bat_scorecard.match_id=tournament_matches.id INNER JOIN tournaments on tournament_matches.tournament_id=tournaments.id WHERE batsman_id = tbs.batsman_id AND how_out = 'Not Out' $where_sql_sub $where_sql_bat) AS not_outs,
(SELECT COUNT(tournament_bat_scorecard.id)  FROM  tournament_bat_scorecard INNER JOIN tournament_matches on tournament_bat_scorecard.match_id=tournament_matches.id INNER JOIN tournaments on tournament_matches.tournament_id=tournaments.id WHERE fielder_id = tbs.batsman_id AND (how_out = 'Caught' OR how_out = 'Caught & Bowled' OR how_out = 'Caught Behind') $where_sql_sub $where_sql_bat) AS total_catches,
(SELECT COUNT(tournament_bat_scorecard.id)  FROM  tournament_bat_scorecard INNER JOIN tournament_matches on tournament_bat_scorecard.match_id=tournament_matches.id INNER JOIN tournaments on tournament_matches.tournament_id=tournaments.id WHERE fielder_id = tbs.batsman_id AND how_out = 'Stumped' $where_sql_sub $where_sql_bat) AS total_stumped,
(SELECT COUNT(tournament_bat_scorecard.id)  FROM  tournament_bat_scorecard INNER JOIN tournament_matches on tournament_bat_scorecard.match_id=tournament_matches.id INNER JOIN tournaments on tournament_matches.tournament_id=tournaments.id WHERE fielder_id = tbs.batsman_id AND how_out = 'Run Out' $where_sql_sub $where_sql_bat) AS total_run_outs,
(SELECT COUNT(tournament_bowl_scorecard.id)  FROM  tournament_bowl_scorecard INNER JOIN tournament_matches on tournament_bowl_scorecard.match_id=tournament_matches.id INNER JOIN tournaments on tournament_matches.tournament_id=tournaments.id WHERE bowler_id = tbs.batsman_id AND wickets_taken>=5 AND wickets_taken<10 $where_sql_sub $where_sql_bowl) AS five_wickets,
(SELECT COUNT(tournament_bowl_scorecard.id)  FROM  tournament_bowl_scorecard INNER JOIN tournament_matches on tournament_bowl_scorecard.match_id=tournament_matches.id INNER JOIN tournaments on tournament_matches.tournament_id=tournaments.id WHERE bowler_id = tbs.batsman_id AND wickets_taken>=10  $where_sql_sub $where_sql_bowl) AS ten_wickets,
(SELECT SUM(tournament_bowl_scorecard.overs_bowled)  FROM  tournament_bowl_scorecard INNER JOIN tournament_matches on tournament_bowl_scorecard.match_id=tournament_matches.id INNER JOIN tournaments on tournament_matches.tournament_id=tournaments.id WHERE bowler_id = tbs.batsman_id   $where_sql_sub $where_sql_bowl) AS total_overs_bowled,
(SELECT SUM(tournament_bowl_scorecard.runs_conceded)  FROM  tournament_bowl_scorecard INNER JOIN tournament_matches on tournament_bowl_scorecard.match_id=tournament_matches.id INNER JOIN tournaments on tournament_matches.tournament_id=tournaments.id WHERE bowler_id = tbs.batsman_id   $where_sql_sub $where_sql_bowl) AS total_runs_conceded,
(SELECT SUM(tournament_bowl_scorecard.wickets_taken)  FROM  tournament_bowl_scorecard INNER JOIN tournament_matches on tournament_bowl_scorecard.match_id=tournament_matches.id INNER JOIN tournaments on tournament_matches.tournament_id=tournaments.id WHERE bowler_id = tbs.batsman_id   $where_sql_sub $where_sql_bowl) AS total_wickets_taken,
(SELECT SUM(tournament_bowl_scorecard.wide_balls_bowled)  FROM  tournament_bowl_scorecard INNER JOIN tournament_matches on tournament_bowl_scorecard.match_id=tournament_matches.id INNER JOIN tournaments on tournament_matches.tournament_id=tournaments.id WHERE bowler_id = tbs.batsman_id   $where_sql_sub $where_sql_bowl) AS total_wide_balls_bowled,
(SELECT SUM(tournament_bowl_scorecard.no_balls_bowled)  FROM  tournament_bowl_scorecard INNER JOIN tournament_matches on tournament_bowl_scorecard.match_id=tournament_matches.id INNER JOIN tournaments on tournament_matches.tournament_id=tournaments.id WHERE bowler_id = tbs.batsman_id   $where_sql_sub $where_sql_bowl) AS total_no_balls_bowled,
(SELECT SUM(tournament_bowl_scorecard.maidens_bowled)  FROM  tournament_bowl_scorecard INNER JOIN tournament_matches on tournament_bowl_scorecard.match_id=tournament_matches.id INNER JOIN tournaments on tournament_matches.tournament_id=tournaments.id WHERE bowler_id = tbs.batsman_id   $where_sql_sub $where_sql_bowl) AS total_maidens_bowled
FROM tournament_bat_scorecard AS tbs 
INNER JOIN tournament_matches AS tm ON tbs.match_id = tm.id 
INNER JOIN tournaments AS t ON t.id = tm.tournament_id
INNER JOIN users AS u ON u.id = tbs.batsman_id 
INNER JOIN companies AS c1 ON tbs.team_id = c1.id
LEFT JOIN photos as p ON p.entity_id=tbs.batsman_id AND p.entity_type = 'users' AND p.is_default = 1
WHERE 1  ".$where_sql;

$sql.=' GROUP BY tbs.batsman_id ORDER BY total_runs_scored DESC';
$rs_matches = mysqli_query($conn,$sql);//echo $sql;
while($row = mysqli_fetch_assoc($rs_matches)){
	$statistics[] = $row;
}
?>
			
			<?php if(!empty($error)): ?>
				<?php echo $error; ?>
			<?php endif; ?>
		
			 <div class="clear"></div>
				<div class="row">
                <div class="col-sm-12">
                <div id="player-statistics-data" class="table-responsive">
				<table id="table-list" class="table table-bordered table-condensed table-hover table-striped">
				<thead><tr><th >Player</th><th >Team</th><th>Mat</th><th>Inns</th><th>NO</th><th>Runs</th><th>Balls <br>Played</th><th>Avg</th><th>SR</th>
				<th>HS</th><th>50's</th><th>100's</th><th>4's</th><th>6's</th><th>Overs<br/>Bowled</th><th>Maidens</th><th>Runs</th><th>Wkts</th><th>Econ</th><th>Bowling<br>Average</th><th>5 Wkts</th><th>10 Wkts</th><th>Wides</th><th>Nb</th><th>Catches</th><th>Stumps</th><th>Run outs</th></tr></thead>
				 <tbody>
				<?php for($i=0;$i<count($statistics);$i++): ?>
				<?php $overs_played = floor($statistics[$i]['total_balls_played']/6).'.'.$statistics[$i]['total_balls_played']%6; ?>
				<?php if(!empty($statistics[$i]['file_name']) && file_exists('users/'.$statistics[$i]['batsman_id'].'/photos/'.$statistics[$i]['file_name'])) $user_photo = 'users/'.$statistics[$i]['batsman_id'].'/photos/'.$statistics[$i]['file_name'];else $user_photo = 'images/no-photo.jpg'; ?>					
					<tr>
						<td ><a style="color:#D25E06;" <?php if(isset($_SESSION['ycdc_user_name']) && !empty($_SESSION['ycdc_user_name'])){  ?> href="<?=WWW?>individual-detail-<?php echo $statistics[$i]['batsman_id'];?>-<?=friendlyURL($statistics[$i]['f_name'].' '.$statistics[$i]['last_name'])?>.html#activity-tab" <?php  }else{ ?> href="javascript:;" onclick="hideStatisticsByPlayer();" <?php }  ?> title="<?php echo $statistics[$i]['f_name']; ?> <?php echo $statistics[$i]['last_name'] ?> Profile"><img style="width:35px;padding:2px;" src="<?php echo $user_photo; ?>"><br/><?php echo $statistics[$i]['f_name']; ?> <?php echo $statistics[$i]['last_name'] ?></a></td>
						<td ><a  style="color:#D25E06;" href="<?=WWW?><?=$statistics[$i]['company_permalink'];?>" title="<?php echo $statistics[$i]['team_name']; ?> Profile"><?php echo $statistics[$i]['team_name']; ?></a> </td>
						<td><?php echo $statistics[$i]['matches_played']; ?></td>
						<td><?php echo $statistics[$i]['innings']; ?></td>
						<td><?php echo $statistics[$i]['not_outs']; ?></td>
						<td><?php echo $statistics[$i]['total_runs_scored']; ?></td>
						<td><?php echo $statistics[$i]['total_balls_played']; ?></td>
						<td><?php if($statistics[$i]['innings'] == $statistics[$i]['not_outs']) echo $statistics[$i]['total_runs_scored']; elseif(is_nan(round($statistics[$i]['total_runs_scored']/($statistics[$i]['innings']-$statistics[$i]['not_outs']),1))) echo 0;else echo round($statistics[$i]['total_runs_scored']/($statistics[$i]['innings']-$statistics[$i]['not_outs']),1); ?></td>
						<td><?php if($statistics[$i]['total_balls_played']>0) echo round(($statistics[$i]['total_runs_scored']/$statistics[$i]['total_balls_played'])*100,1); else echo 0; ?></td>
						<td><?php echo $statistics[$i]['highest_runs']; ?></td>
						<td><?php echo $statistics[$i]['fiftys']; ?></td>
						<td><?php echo $statistics[$i]['hundreads']; ?></td>
						<td><?php echo $statistics[$i]['total_fours_scored']; ?></td>
						<td><?php echo $statistics[$i]['total_sixes_scored']; ?></td>
						<td><?php if(!empty($statistics[$i]['total_overs_bowled'])) echo $statistics[$i]['total_overs_bowled']; else echo '0.0'; ?></td>
						<td><?php if(!empty($statistics[$i]['total_maidens_bowled'])) echo $statistics[$i]['total_maidens_bowled']; else echo '0.0'; ?></td>
						<td><?php if(!empty($statistics[$i]['total_runs_conceded'])) echo $statistics[$i]['total_runs_conceded']; else echo 0; ?></td>
						<td><?php if(!empty($statistics[$i]['total_wickets_taken'])) echo $statistics[$i]['total_wickets_taken']; else echo 0; ?></td>
						<td><?php if($statistics[$i]['total_overs_bowled']>0) echo round($statistics[$i]['total_runs_conceded']/$statistics[$i]['total_overs_bowled'],1);else echo 0; ?></td>
						<td><?php if(is_nan(round($statistics[$i]['total_runs_conceded']/$statistics[$i]['total_wickets_taken'],1)) || is_infinite(round($statistics[$i]['total_runs_conceded']/$statistics[$i]['total_wickets_taken'],1)) ) echo 0;else echo round($statistics[$i]['total_runs_conceded']/$statistics[$i]['total_wickets_taken'],1); ?></td>
						<td><?php echo $statistics[$i]['five_wickets']; ?></td>
						<td><?php echo $statistics[$i]['ten_wickets']; ?></td>
						<td><?php if(!empty($statistics[$i]['total_wide_balls_bowled'])) echo $statistics[$i]['total_wide_balls_bowled'];else echo 0; ?></td>
						<td><?php if(!empty($statistics[$i]['total_no_balls_bowled'])) echo $statistics[$i]['total_no_balls_bowled']; else echo 0; ?></td>
						<td><?php echo $statistics[$i]['total_catches']; ?></td>
						<td><?php echo $statistics[$i]['total_stumped']; ?></td>
						<td><?php echo $statistics[$i]['total_run_outs']; ?></td>
					</tr>
					<?php if($i && ($i+1)%9 ==0): ?>
						<thead><tr><th>Player</th><th >Team</th><th>Mat</th><th>Inns</th><th>NO</th><th>Runs</th><th>Overs <br>Played</th><th>Avg</th><th>SR</th>
						<th>HS</th><th>50's</th><th>100's</th><th>4's</th><th>6's</th><th>Overs<br/>Bowled</th><th>Maidens</th><th>Runs</th><th>Wkts</th><th>Econ</th><th>Bowling<br>Average</th><th>5 Wkts</th><th>10 Wkts</th><th>Wides</th><th>Nb</th><th>Catches</th><th>Stumps</th><th>Run outs</th></tr></thead>
					<?php endif; ?>
				<?php endfor; ?>
				<?php if(empty($statistics)): ?>
				<tr><td colspan="25">No Records</td></tr>
				<?php endif; ?>
				 <tbody>
				</table>
				</div></div></div>
				
<div class="clear"><br/></div>