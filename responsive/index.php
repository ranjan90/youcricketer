<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/img/favicon.png">

    <title>Youcricketer Overseas Cricket Agency</title>

    <!-- Bootstrap core CSS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="assets/js/jquery.min.js"></script>
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#" title="Home Page - "><img alt="Logo" src="assets/img/logo.png" /></a>
		  <p>Connect  & Discover Cricket Opportunities Globally </p>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <div class="header-top">
            <div class="row">
              <div class="col-sm-6">
                <form method="post"  id="frm-search">
                  <div class="input-group">
                    <input id='ja' name="keywords" type="text" class="form-control" placeholder="Search Individuals/Clubs/Leagues/Companies"/>
                    <div class="input-group-addon"><button type="button" id="search-btn"><i class="fa fa-search"></i></button></div>
                  </div>
                </form>
              </div>
              <div class="col-sm-3 col-xs-6">
                <a href="<?=WWW?>advance-search.html"> Advance Search </a>
              </div>
              <div class="col-sm-3 col-xs-6">
                <ul class="social"><!--<?php $social_title = 'Youcricketer Sports Social Media' ?>-->
                  <li><a href="<?php echo WWW;?>rss.html"><img alt="RSS" height="25" width="30" src="assets/img/rss.png" /></a></li>
                  <li><a href="<?=$twitter_page_link['value']?>" title="Twitter Page - <?=$social_title?>" target="_blank"><img alt="Twitter Page - <?=$social_title?>" height="25" width="30" src="assets/img/twitter.png" /></a></li>
                  <li><a href="<?=$facebook_page_link['value']?>" title="Facebook Page - <?=$social_title?>" target="_blank"><img alt="Facebook Page - <?=$social_title?>" height="25" width="30" src="assets/img/facebook.png" /></a></li>
                  <!-- <?php /* ?><li><a href="<?=$google_plus_page_link['value']?>" title="Google Plus Page - <?=$social_title?>" target="_blank"><img alt="Google Plus Page - <?=$social_title?>" height="25" width="30" src="<?php echo WWW;?>images/google.png" /></a></li>
                  <li><a href="<?=$pinterest_page_link['value']?>" title="Pinterest Plus Page - <?=$social_title?>" target="_blank"><img alt="Pinterest Plus Page - <?=$social_title?>" height="25" width="30" src="<?php echo WWW;?>images/pinterest.png" /></a></li>
                  <li><a href="<?=$tumblr_page_link['value']?>" title="Tumblr Plus Page - <?=$social_title?>" target="_blank"><img alt="Tumblr Plus Page - <?=$social_title?>" height="25" width="30" src="<?php echo WWW;?>images/tumblr.png" /></a></li><?php */ ?>
                  <li><a href="#"><img height="25" width="20" src="<?php echo WWW;?>images/youtube.png" /></a></li> -->
                </ul>
              </div>
            </div>
          </div>
          <div class="signup-buttons">
            <div class="row">
              <div class="col-sm-4 col-xs-12">
                <a title="Login - <?=$site_title?>" href="<?=WWW?>login.html" class="btn blue full hvr-float-shadow">Login</a>
              </div>
              <div class="col-sm-4 col-xs-12">
                <a title="Individual Sign up - <?=$site_title?>" href="<?=WWW?>sign-up-individual.html" class="btn orange full hvr-float-shadow">Sign up as Individual</a>
              </div>
              <div class="col-sm-4 col-xs-12">
                <a title="Company Sign up - <?=$site_title?>" href="<?=WWW?>sign-up-company.html" class="btn orange full hvr-float-shadow">Sign up as Club/League/Company</a>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">About Us</a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Who We Are</a></li>
                        <li><a href="#">Our Mission</a></li>
                        <li><a href="#">Our Philosphy</a></li>
                    </ul>
                </li>
                <li><a href="#">Press Releases</a></li>
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Community</a>
                    <ul class="dropdown-menu" role="menu">
                    	<li><a href="#">Blogs</a></li>
                        <li><a href="#">Forums</a></li>
                        <li><a href="#">Groups</a></li>
                        <li><a href="#">Testimonials</a></li>
                        <li><a href="#">Whats Happening</a></li>
                        <li><a href="#">Events</a></li>
                    </ul>
                </li>
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Your Region</a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Blogs</a></li>
                        <li><a href="#">Forums</a></li>
                        <li><a href="#">Groups</a></li>
                        <li><a href="#">Testimonials</a></li>
                        <li><a href="#">Whats Happening</a></li>
                        <li><a href="#">Events</a></li>
                    </ul>
                </li>
                <li><a href="#">Most Favorite</a></li>
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">FAQs</a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">FAQs</a></li>
                        <li><a href="#">How To</a></li>
                    </ul>
                </li>
                <li><a href="">Contact Us</a></li>
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> My Account</a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">My Dashboard</a></li>
                        <li><a href="#">My Network</a></li>
                        <li><a href="#">My Messages</a></li>
                        <li><a href="#">My Community</a></li>
                        <li><a href="#">My Albums</a></li>
                        <li><a href="#">My Following</a></li>
                    </ul>
                </li>
              </ul>
            </div>
          </div>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div id="activity-slider">
            <p>Latest Greatest </p>
            <a href="#">Free Online Scoring feature is now available. Use it from anywhere in the World for your League's Tournaments. Register for free account.</a>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
              <div class="item active">
                <img src="assets/img/top-right/map.jpg" alt="Map" />
              </div>
              <div class="item">
                <img src="assets/img/top-right/express.jpg" alt="Express" />
              </div>
              <div class="item">
                <img src="assets/img/top-right/connected.jpg" alt="Connected" />
              </div>
              <div class="item">
                <img src="assets/img/top-right/shine.jpg" alt="Shine" />
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-8">
          <div id="myCarousel2" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
              <li data-target="#myCarousel2" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel2" data-slide-to="1"></li>
              <li data-target="#myCarousel2" data-slide-to="2"></li>
              <li data-target="#myCarousel2" data-slide-to="3"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
              <div class="item active">
                <a title="Whats New" href="#" target="_blank"><img src="assets/img/banners/youcricketer-features.jpg" alt="Whats New" /></a>
              </div>
              <div class="item">
                <a title="Opportiniti" href="#" target="_blank"><img src="assets/img/banners/fire-banner.jpg" alt="Opportiniti" /></a>
              </div>
              <div class="item">
                <a title="Its not jus" href="#" target="_blank"><img src="assets/img/banners/banner-1.jpg" alt="Its not jus" />
              </div>
              <div class="item">
                <a title="New Features" href="#" target="_blank"><img src="assets/img/banners/banner-5.jpg" alt="New Features" /></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div><!-- /.container -->
    
    <div style="height:15px"></div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-7">
          <div class="white-box">
            <ul class="menu" id="menu">
              <li><a href="#" id="individual-tab" class="active">Individuals</a></li>
              <li><a href="#" id="companies-tab">Clubs/Leagues/Companies</a></li>
			</ul>
			<div class="content1" id="individual">
              <fieldset style="height:1288x; overflow:hidden;">
                <dl>
                  <dt> <a title="rakesh  goit" href="http://youcricketer.com/individual-detail-135-rakesh++goit.html#activity-tab"><img width="120" height="128" alt="no photo" src="assets/img/no-photo.jpg"></a></dt>
                  <dd>
                    <div class="details">
                      <h3>rakesh<br>
                        goit</h3>
                      <p>Nepal<br>
                        Player<br>
                        All Rounder <br>
                      </p>
                      <a title="rakesh  goit - Youcricketer Overseas Cricket Agency" href="http://youcricketer.com/individual-detail-135-rakesh++goit.html#activity-tab">View Profile</a>
                      <span class="flag"><img width="25" src="assets/img/countries/nepal.png" alt="Nepal" title="Nepal"></span>
                    </div>
                    <div class="video"> <img width="168" height="130" alt="no video" src="assets/img/no-video.jpg"> </div>
                  </dd>
                </dl>
                <dl>
                  <dt> <a title="Rishabh  Choudhary" href="http://youcricketer.com/individual-detail-613-rishabh++choudhary.html#activity-tab"><img width="120" height="128" alt="no photo" src="assets/img/no-photo.jpg"></a></dt>
                  <dd>
                    <div class="details">
                      <h3>Rishabh<br>
                        Choudhary</h3>
                      <p>India<br>
                        Player<br>
                        <br>
                      </p>
                      <a title="Rishabh  Choudhary - Youcricketer Overseas Cricket Agency" href="http://youcricketer.com/individual-detail-613-rishabh++choudhary.html#activity-tab">View Profile</a>
                      <span class="flag"><img width="25" src="assets/img/countries/india.png" alt="India" title="India"></span>  
                    </div>
                    <div class="video"> <img width="168" height="130" alt="no video" src="assets/img/no-video.jpg"> </div>
                  </dd>
                </dl>
                <dl>
                  <dt> <a title="Goldy  Singh" href="http://youcricketer.com/individual-detail-1078-goldy++singh.html#activity-tab"><img width="120" height="128" alt="no photo" src="assets/img/no-photo.jpg"></a></dt>
                  <dd>
                    <div class="details">
                      <h3>Goldy<br>
                        Singh</h3>
                      <p>United States<br>
                        Player<br>
                        <br>
                      </p>
                      <a title="Goldy  Singh - Youcricketer Overseas Cricket Agency" href="http://youcricketer.com/individual-detail-1078-goldy++singh.html#activity-tab">View Profile</a>
                      <span class="flag"><img width="25" src="assets/img/countries/united-states.png" alt="United States" title="United States"></span>
                    </div>
                    <div class="video"> <img width="168" height="130" alt="no video" src="assets/img/no-video.jpg"> </div>
                  </dd>
                </dl>
                <dl>
                  <dt> <a title="Terence  Fernandes" href="http://youcricketer.com/individual-detail-1014-terence++fernandes.html#activity-tab"><img width="120" height="128" alt="no photo" src="http://youcricketer.com/users/1014/photos/terence.jpg"></a></dt>
                  <dd>
                    <div class="details">
                      <h3>Terence<br>
                        Fernandes</h3>
                      <p>United States<br>
                        Player<br>
                        <br>
                      </p>
                      <a title="Terence  Fernandes - Youcricketer Overseas Cricket Agency" href="http://youcricketer.com/individual-detail-1014-terence++fernandes.html#activity-tab">View Profile</a>
                      <span class="flag"><img width="25" src="assets/img/countries/united-states.png" alt="United States" title="United States"></span>
                    </div>
                    <div class="video"> <img width="168" height="130" alt="no video" src="assets/img/no-video.jpg"> </div>
                  </dd>
                </dl>
                <dl>
                  <dt> <a title="Sachin  Gunathilaka" href="http://youcricketer.com/individual-detail-1295-sachin++gunathilaka.html#activity-tab"><img width="120" height="128" alt="no photo" src="http://youcricketer.com/users/1295/photos/sachin.jpg"></a></dt>
                  <dd>
                    <div class="details">
                      <h3>Sachin<br>
                        Gunathilaka</h3>
                      <p>Sri Lanka<br>
                        Player<br>
                        <br>
                      </p>
                      <a title="Sachin  Gunathilaka - Youcricketer Overseas Cricket Agency" href="http://youcricketer.com/individual-detail-1295-sachin++gunathilaka.html#activity-tab">View Profile</a>
                      <span class="flag"><img width="25" src="assets/img/countries/sri-lanka.png" alt="Sri Lanka" title="Sri Lanka"></span>
                    </div>
                    <div class="video"> <img width="168" height="130" alt="no video" src="assets/img/no-video.jpg"> </div>
                  </dd>
                </dl>
                <dl>
                  <dt> <a title="Shariq  Syed" href="http://youcricketer.com/individual-detail-537-shariq++syed.html#activity-tab"><img width="120" height="128" alt="no photo" src="assets/img/no-photo.jpg"></a></dt>
                  <dd>
                    <div class="details">
                      <h3>Shariq<br>
                        Syed</h3>
                      <p>Pakistan<br>
                        A Cricket Lover<br>
                        <br>
                      </p>
                      <a title="Shariq  Syed - Youcricketer Overseas Cricket Agency" href="http://youcricketer.com/individual-detail-537-shariq++syed.html#activity-tab">View Profile</a>
                      <span class="flag"><img width="25" src="assets/img/countries/pakistan.png" alt="Pakistan" title="Pakistan"></span>
                    </div>
                    <div class="video"> <img width="168" height="130" alt="no video" src="assets/img/no-video.jpg"> </div>
                  </dd>
                </dl>
                <dl>
                  <dt> <a title="Pranav  Ambati" href="http://youcricketer.com/individual-detail-1516-pranav++ambati.html#activity-tab"><img width="120" height="128" alt="no photo" src="assets/img/no-photo.jpg"></a></dt>
                  <dd>
                    <div class="details">
                      <h3>Pranav<br>
                        Ambati</h3>
                      <p>United States<br>
                        Player<br>
                        <br>
                      </p>
                      <a title="Pranav  Ambati - Youcricketer Overseas Cricket Agency" href="http://youcricketer.com/individual-detail-1516-pranav++ambati.html#activity-tab">View Profile</a>
                      <span class="flag"><img width="25" src="assets/img/countries/united-states.png" alt="United States" title="United States"></span>
                    </div>
                    <div class="video"> <img width="168" height="130" alt="no video" src="assets/img/no-video.jpg"> </div>
                  </dd>
                </dl>
                <dl>
                  <dt> <a title="krishna  madabooshi" href="http://youcricketer.com/individual-detail-1506-krishna++madabooshi.html#activity-tab"><img width="120" height="128" alt="no photo" src="assets/img/no-photo.jpg"></a></dt>
                  <dd>
                    <div class="details">
                      <h3>krishna<br>
                        madabooshi</h3>
                      <p>United States<br>
                        Player<br>
                        <br>
                      </p>
                      <a title="krishna  madabooshi - Youcricketer Overseas Cricket Agency" href="http://youcricketer.com/individual-detail-1506-krishna++madabooshi.html#activity-tab">View Profile</a>
                      <span class="flag"><img width="25" src="assets/img/countries/united-states.png" alt="United States" title="United States"></span>
                    </div>
                    <div class="video"> <img width="168" height="130" alt="no video" src="assets/img/no-video.jpg"> </div>
                  </dd>
                </dl>
                <dl>
                  <dt> <a title="Srinath  Sankar" href="http://youcricketer.com/individual-detail-942-srinath++sankar.html#activity-tab"><img width="120" height="128" alt="no photo" src="assets/img/no-photo.jpg"></a></dt>
                  <dd>
                    <div class="details">
                      <h3>Srinath<br>
                        Sankar</h3>
                      <p>United States<br>
                        Player<br>
                        <br>
                      </p>
                      <a title="Srinath  Sankar - Youcricketer Overseas Cricket Agency" href="http://youcricketer.com/individual-detail-942-srinath++sankar.html#activity-tab">View Profile</a>
                      <span class="flag"><img width="25" src="assets/img/countries/united-states.png" alt="United States" title="United States"></span>
                    </div>
                    <div class="video"> <img width="168" height="130" alt="no video" src="assets/img/no-video.jpg"> </div>
                  </dd>
                </dl>
                <dl>
                  <dt> <a title="Navin  Chatlani" href="http://youcricketer.com/individual-detail-1214-navin++chatlani.html#activity-tab"><img width="120" height="128" alt="no photo" src="assets/img/no-photo.jpg"></a></dt>
                  <dd>
                    <div class="details">
                      <h3>Navin<br>
                        Chatlani</h3>
                      <p>United States<br>
                        Player<br>
                        <br>
                      </p>
                      <a title="Navin  Chatlani - Youcricketer Overseas Cricket Agency" href="http://youcricketer.com/individual-detail-1214-navin++chatlani.html#activity-tab">View Profile</a>
                      <span class="flag"><img width="25" src="assets/img/countries/united-states.png" alt="United States" title="United States"></span>
                    </div>
                    <div class="video"> <img width="168" height="130" alt="no video" src="assets/img/no-video.jpg"> </div>
                  </dd>
                </dl>
                <dl>
                  <dt> <a title="Devang  Vasani" href="http://youcricketer.com/individual-detail-1220-devang++vasani.html#activity-tab"><img width="120" height="128" alt="no photo" src="assets/img/no-photo.jpg"></a></dt>
                  <dd>
                    <div class="details">
                      <h3>Devang<br>
                        Vasani</h3>
                      <p>India<br>
                        Player<br>
                        <br>
                      </p>
                      <a title="Devang  Vasani - Youcricketer Overseas Cricket Agency" href="http://youcricketer.com/individual-detail-1220-devang++vasani.html#activity-tab">View Profile</a>
                      <span class="flag"><img width="25" src="assets/img/countries/india.png" alt="India" title="India"></span>
                    </div>
                    <div class="video"> <img width="168" height="130" alt="no video" src="assets/img/no-video.jpg"> </div>
                  </dd>
                </dl>
              </fieldset>
              <div class="view-btn">
                <a class="btn orange hvr-float-shadow" title="View all Individuals" href="http://youcricketer.com/individuals.html">View All</a>
              </div>
			</div>
			<div style="display: none;" class="content1" id="companies">
              <fieldset style="height:1288x; overflow:hidden;">
                <dl>
                  <dt><a title="UMCC" href="http://youcricketer.com/cricket-club/united-states-umcc/98"> <img width="120" height="128" alt="no photo" src="assets/img/no-photo.png"> </a> </dt>
                  <dd>
                    <div class="details">
                      <h3 style="height:22px;">UMCC</h3>
                      <p>Club</p>
                      <p>United States</p>
                      <a title="UMCC - Youcricketer Overseas Cricket Agency" href="http://youcricketer.com/cricket-club/united-states-umcc/98">View Profile</a>
                      <span class="flag"><img width="25" src="assets/img/countries/united-states.png" title="United States" alt="United States"></span>
                    </div>
                    <div class="video"> <img width="168" height="130" alt="no video" src="assets/img/no-video.jpg"> </div>
                    <div class="clear"></div>
                  </dd>
                </dl>
                <dl>
                  <dt><a title="UMCC" href="http://youcricketer.com/cricket-club/united-states-umcc/97"> <img width="120" height="128" alt="no photo" src="assets/img/no-photo.png"> </a> </dt>
                  <dd>
                    <div class="details">
                      <h3 style="height:22px;">UMCC</h3>
                      <p>Club</p>
                      <p>United States</p>
                      <a title="UMCC - Youcricketer Overseas Cricket Agency" href="http://youcricketer.com/cricket-club/united-states-umcc/97">View Profile</a>
                      <span class="flag"><img width="25" src="assets/img/countries/united-states.png" title="United States" alt="United States"></span>
                    </div>
                    <div class="video"> <img width="168" height="130" alt="no video" src="assets/img/no-video.jpg"> </div>
                    <div class="clear"></div>
                  </dd>
                </dl>
                <dl>
                  <dt><a title="cricket club" href="http://youcricketer.com/cricket-club/india-cricket-club/96"> <img width="120" height="128" alt="no photo" src="assets/img/no-photo.png"> </a> </dt>
                  <dd>
                    <div class="details">
                      <h3 style="height:22px;">cricket club</h3>
                      <p>Club</p>
                      <p>India</p>
                      <a title="cricket club - Youcricketer Overseas Cricket Agency" href="http://youcricketer.com/cricket-club/india-cricket-club/96">View Profile</a>
                      <span class="flag"><img width="25" src="assets/img/countries/india.png" title="India" alt="India"></span>
                    </div>
                    <div class="video"> <img width="168" height="130" alt="no video" src="assets/img/no-video.jpg"> </div>
                    <div class="clear"></div>
                  </dd>
                </dl>
                <dl>
                  <dt><a title="AAA" href="http://youcricketer.com/cricket-club/united-states-aaa/95"> <img width="120" height="128" alt="no photo" src="assets/img/no-photo.png"> </a> </dt>
                  <dd>
                    <div class="details">
                      <h3 style="height:22px;">AAA</h3>
                      <p>Club</p>
                      <p>United States</p>
                      <a title="AAA - Youcricketer Overseas Cricket Agency" href="http://youcricketer.com/cricket-club/united-states-aaa/95">View Profile</a>
                      <span class="flag"><img width="25" src="assets/img/countries/united-states.png" title="United States" alt="United States"></span>
                    </div>
                    <div class="video"> <img width="168" height="130" alt="no video" src="assets/img/no-video.jpg"> </div>
                    <div class="clear"></div>
                  </dd>
                </dl>
                <dl>
                  <dt><a title="Spartans Cricket Club" href="http://youcricketer.com/cricket-club/united-states-spartans-cricket-club/93"> <img width="120" height="128" alt="no photo" src="http://youcricketer.com/users/1162/photos/venkata+.jpg"> </a> </dt>
                  <dd>
                    <div class="details">
                      <h3 style="height:22px;">Spartans Cricket Club</h3>
                      <p>Club</p>
                      <p>United States</p>
                      <a title="Spartans Cricket Club - Youcricketer Overseas Cricket Agency" href="http://youcricketer.com/cricket-club/united-states-spartans-cricket-club/93">View Profile</a>
                      <span class="flag"><img width="25" src="assets/img/countries/united-states.png" title="United States" alt="United States"></span>
                    </div>
                    <div class="video"> <img width="168" height="130" alt="no video" src="assets/img/no-video.jpg"> </div>
                    <div class="clear"></div>
                  </dd>
                </dl>
                <dl>
                  <dt><a title="Club" href="http://youcricketer.com/cricket-club/united-states-club/92"> <img width="120" height="128" alt="no photo" src="assets/img/no-photo.png"> </a> </dt>
                  <dd>
                    <div class="details">
                      <h3 style="height:22px;">Club</h3>
                      <p>Club</p>
                      <p>United States</p>
                      <a title="Club - Youcricketer Overseas Cricket Agency" href="http://youcricketer.com/cricket-club/united-states-club/92">View Profile</a>
                      <span class="flag"><img width="25" src="assets/img/countries/united-states.png" title="United States" alt="United States"></span>
                    </div>
                    <div class="video"> <img width="168" height="130" alt="no video" src="assets/img/no-video.jpg"> </div>
                    <div class="clear"></div>
                  </dd>
                </dl>
                <dl>
                  <dt><a title="" href="http://youcricketer.com/cricket-club/united-states-/91"> <img width="120" height="128" alt="no photo" src="assets/img/no-photo.png"> </a> </dt>
                  <dd>
                    <div class="details">
                      <h3 style="height:22px;"></h3>
                      <p>Club</p>
                      <p>United States</p>
                      <a title=" - Youcricketer Overseas Cricket Agency" href="http://youcricketer.com/cricket-club/united-states-/91">View Profile</a>
                      <span class="flag"><img width="25" src="assets/img/countries/united-states.png" title="United States" alt="United States"></span>
                    </div>
                    <div class="video"> <img width="168" height="130" alt="no video" src="assets/img/no-video.jpg"> </div>
                    <div class="clear"></div>
                  </dd>
                </dl>
                <dl>
                  <dt><a title="British Officers Cricket Club" href="http://youcricketer.com/cricket-club/united-states-british-officers-cricket-club/90"> <img width="120" height="128" alt="no photo" src="http://youcricketer.com/users/1148/photos/parth.jpg"> </a> </dt>
                  <dd>
                    <div class="details">
                      <h3 style="height:22px;">British Officers Cricket Club</h3>
                      <p>Club</p>
                      <p>United States</p>
                      <a title="British Officers Cricket Club - Youcricketer Overseas Cricket Agency" href="http://youcricketer.com/cricket-club/united-states-british-officers-cricket-club/90">View Profile</a>
                      <span class="flag"><img width="25" src="assets/img/countries/united-states.png" title="United States" alt="United States"></span>
                    </div>
                    <div class="video"> <img width="168" height="130" alt="no video" src="assets/img/no-video.jpg"> </div>
                    <div class="clear"></div>
                  </dd>
                </dl>
                <dl>
                  <dt><a title="Pakhtoonkhwa CC" href="http://youcricketer.com/cricket-club/united-states-pakhtoonkhwa-cc/89"> <img width="120" height="128" alt="no photo" src="http://youcricketer.com/users/1145/photos/tariq.jpg"> </a> </dt>
                  <dd>
                    <div class="details">
                      <h3 style="height:22px;">Pakhtoonkhwa CC</h3>
                      <p>Club</p>
                      <p>United States</p>
                      <a title="Pakhtoonkhwa CC - Youcricketer Overseas Cricket Agency" href="http://youcricketer.com/cricket-club/united-states-pakhtoonkhwa-cc/89">View Profile</a>
                      <span class="flag"><img width="25" src="assets/img/countries/united-states.png" title="United States" alt="United States"></span>
                    </div>
                    <div class="video"> <img width="168" height="130" alt="no video" src="assets/img/no-video.jpg"> </div>
                    <div class="clear"></div>
                  </dd>
                </dl>
                <dl>
                  <dt><a title="UMCC" href="http://youcricketer.com/cricket-club/united-states-umcc/88"> <img width="120" height="128" alt="no photo" src="assets/img/no-photo.png"> </a> </dt>
                  <dd>
                    <div class="details">
                      <h3 style="height:22px;">UMCC</h3>
                      <p>Club</p>
                      <p>United States</p>
                      <a title="UMCC - Youcricketer Overseas Cricket Agency" href="http://youcricketer.com/cricket-club/united-states-umcc/88">View Profile</a>
                      <span class="flag"><img width="25" src="assets/img/countries/united-states.png" title="United States" alt="United States"></span>
                    </div>
                    <div class="video"> <img width="168" height="130" alt="no video" src="assets/img/no-video.jpg"> </div>
                    <div class="clear"></div>
                  </dd>
                </dl>
                <dl>
                  <dt><a title="Kirkwood Cricket Club" href="http://youcricketer.com/cricket-club/united-states-kirkwood-cricket-club/87"> <img width="120" height="128" alt="no photo" src="assets/img/no-photo.png"> </a> </dt>
                  <dd>
                    <div class="details">
                      <h3 style="height:22px;">Kirkwood Cricket Club</h3>
                      <p>Club</p>
                      <p>United States</p>
                      <a title="Kirkwood Cricket Club - Youcricketer Overseas Cricket Agency" href="http://youcricketer.com/cricket-club/united-states-kirkwood-cricket-club/87">View Profile</a>
                      <span class="flag"><img width="25" src="assets/img/countries/united-states.png" title="United States" alt="United States"></span>  
                    </div>
                    <div class="video"> <img width="168" height="130" alt="no video" src="assets/img/no-video.jpg"> </div>
                    <div class="clear"></div>
                  </dd>
                </dl>
              </fieldset>
              <div class="view-btn">
                <a class="btn orange hvr-float-shadow" title="View all Companies" href="http://youcricketer.com/companies-sign-up.php">View All</a>
              </div>
            </div>
			<div class="clear"></div>
		  </div>
        </div>
        <div class="col-md-3">
          <div class="row">
            <div class="col-sm-12">
              <div class="white-box">
                <ul id="menu2" class="menu">
				  <li><a href="#" id="news-tab" class="active">News</a></li>
				  <li><a href="#" id="scoreboard-tab">Score</a></li>
				</ul>
                <div id="news" class="content1" style="min-height:330px;">
				  <div id="news_content" style="min-height:400px;">
                    <div class="slide10" id="slide10"> <i class="fa fa-minus-circle"></i> International </div>
                    <div class="target10" id="target10">
                      <ul>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/usa/content/story/1029291.html" target="_blank">Former WI batsman Powell to chair new USA selection panel</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/ireland/content/story/1029271.html" target="_blank">Adair, Terry called up to Ireland squad</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/tri-nation-west-indies-2016/content/story/1029223.html" target="_blank">Unchanged South Africa bowl in knockout clash; Holder fit</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/county-cricket-2016/content/story/1029183.html" target="_blank">Rossington onslaught leaves Sussex tough survival bid</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/england-v-sri-lanka-2016/content/story/1029169.html" target="_blank">Umpire Oxenford pioneers shield during Edgbaston ODI</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/usa/content/story/1029161.html" target="_blank">USA wicketkeeper Dodson would prefer Olympic T20 over World T20</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/india/content/story/1029081.html" target="_blank">Decision on pink ball will depend on players' feedback - Thakur</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/england-v-sri-lanka-2016/content/story/1029069.html" target="_blank">England's fielding helps keep Sri Lanka to 254</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/india/content/story/1029065.html" target="_blank">BCCI scraps Syed Mushtaq Ali Trophy for new inter-zonal T20 league</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/ci/content/story/1029059.html" target="_blank">Stokes and Wood set for Durham comebacks</a></li>
                      </ul>
                    </div>
                  </div>
				  <div class="view-btn">
                    <a class="btn orange hvr-float-shadow" href="<?=WWW;?>espn-news.html">View All</a>
                  </div>
				</div>
                <div id="scoreboard" class="content1" style="min-height:240px; display:none;">
                  <div id="score_content">
                    <div class="slide10" id="slide10"> <i class="fa fa-minus-circle"></i> International </div>
                    <div style="display: block;" class="target10" id="target10">
                      <ul>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=score&amp;url=http://www.espncricinfo.com/england-v-sri-lanka-2016/engine/match/913625.html" target="_blank"> Sri Lanka    254/7 (50 ov)
                          vs 
                          England    256/0 (34.1/50 ov)
                          (
                          
                          Jun 24, 2016
                          -
                          
                          
                          2nd ODI at Edgbaston, Birmingham (day/night)
                          
                          
                          )</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=score&amp;url=http://www.espncricinfo.com/england-v-sri-lanka-2016/content/story/1029069.html" target="_blank"> West Indies    158/4 (28/50 ov)
                          vs 
                          South Africa    
                          (
                          
                          Jun 24, 2016
                          -
                          
                          
                          9th Match at Kensington Oval, Bridgetown, Barbados (day/night)
                          
                          
                          )</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=score&amp;url=http://www.espncricinfo.com/england-v-sri-lanka-2016/content/series/913581.html" target="_blank"> Sussex    178 &amp; 70/0 (24 ov)
                          vs 
                          Northamptonshire    478/5d
                          (
                          
                          Jun 22-25, 2016
                          -
                          
                          
                          at Arundel Castle Cricket Club Ground
                          
                          
                          )</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=score&amp;url=http://www.espncricinfo.com/tri-nation-west-indies-2016/engine/match/932865.html" target="_blank"> Yorkshire    134 (19.3/20 ov)
                          vs 
                          Durham    68/3 (9.5/20 ov)
                          (
                          
                          Jun 24, 2016
                          -
                          
                          
                          North Group at Riverside Ground, Chester-le-Street
                          
                          
                          )</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=score&amp;url=http://www.espncricinfo.com/tri-nation-west-indies-2016/content/story/1029223.html" target="_blank"> Surrey    110 (19.2/20 ov)
                          vs 
                          Glamorgan    68/1 (9.5/20 ov)
                          (
                          
                          Jun 24, 2016
                          -
                          
                          
                          South Group at Sophia Gardens, Cardiff
                          
                          
                          )</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=score&amp;url=http://www.espncricinfo.com/tri-nation-west-indies-2016/content/series/932847.html" target="_blank"> Lancashire    149/5 (20 ov)
                          vs 
                          Worcestershire    26/5 (7/20 ov)
                          (
                          
                          Jun 24, 2016
                          -
                          
                          
                          North Group at Old Trafford, Manchester
                          
                          
                          )</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=score&amp;url=http://www.espncricinfo.com/ci/engine/match/947013.html" target="_blank"> Leicestershire    125/7 (20 ov)
                          vs 
                          Warwickshire    5/0 (1/20 ov)
                          (
                          
                          Jun 24, 2016
                          -
                          
                          
                          North Group at Grace Road, Leicester
                          
                          
                          )</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=score&amp;url=http://www.espncricinfo.com/county-cricket-2016/content/story/1029183.html" target="_blank"> Derbyshire    114 (19.3/20 ov)
                          vs 
                          Nottinghamshire    17/0 (2.2/20 ov)
                          (
                          
                          Jun 24, 2016
                          -
                          
                          
                          North Group at County Ground, Derby
                          
                          
                          )</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=score&amp;url=http://www.espncricinfo.com/ci/content/series/946651.html" target="_blank"> Hampshire    135/8 (20 ov)
                          vs 
                          Essex    12/0 (1.3/20 ov)
                          (
                          
                          Jun 24, 2016
                          -
                          
                          
                          South Group at County Ground, Chelmsford
                          
                          
                          )</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=score&amp;url=http://www.espncricinfo.com/ci/engine/match/947213.html" target="_blank"> Middlesex    210/6 (20 ov)
                          vs 
                          Kent    
                          (
                          
                          Jun 24, 2016
                          -
                          
                          
                          South Group at St Lawrence Ground, Canterbury
                          
                          
                          )</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=score&amp;url=http://www.espncricinfo.com/ci/content/series/946653.html" target="_blank"> South Africa Under-19s    206 &amp; 119/2 (55 ov)
                          vs 
                          Sri Lanka Under-19s    180
                          (
                          
                          Jun 23-25, 2016
                          -
                          
                          
                          1st Youth Test at P Saravanamuttu Stadium, Colombo
                          
                          
                          )</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=score&amp;url=http://www.espncricinfo.com/ci/engine/match/947209.html" target="_blank"> Leinster Lightning    173/8 (20 ov)
                          vs 
                          North-West Warriors    99 (15/20 ov)
                          (
                          
                          Jun 24, 2016
                          -
                          
                          
                          at Woodvale Road, Eglinton (day/night)
                          
                          
                          )</a></li>
                      </ul>
                    </div>
                  </div>
				</div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <div class="white-box">
                <ul id="menu3" class="menu">
                  <li><a href="#" id="result-tab" class="active">Results</a></li>
				  <li><a href="#" id="blog-tab">Fixtures</a></li>
                </ul>
				<div id="result" class="content1 ">
				  <div class="slide10" id="slide10"> <i class="fa fa-minus-circle"></i> International </div>
                  <div id="results_content" style="min-height:300px;">
                    <div style="display: block;" class="target10" id="target10">
                      <ul>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/tri-nation-west-indies-2016/engine/match/932863.html" target="_blank">West Indies vs Australia</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/england-v-sri-lanka-2016/engine/match/913623.html" target="_blank">Sri Lanka vs England</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/tri-nation-west-indies-2016/engine/match/932861.html" target="_blank">South Africa vs Australia</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/england-v-sri-lanka-2016/engine/match/913621.html" target="_blank">Sri Lanka vs Ireland</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/zimbabwe-v-india-2016/engine/match/1007659.html" target="_blank">India vs Zimbabwe</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/zimbabwe-v-india-2016/engine/match/1007657.html" target="_blank">Zimbabwe vs India</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/zimbabwe-v-india-2016/engine/match/1007655.html" target="_blank">Zimbabwe vs India</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/icc-womens-championship-2014-16/engine/match/946547.html" target="_blank">England Women vs Pakistan Women</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/icc-womens-championship-2014-16/engine/match/946545.html" target="_blank">Pakistan Women vs England Women</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/ci/engine/match/946865.html" target="_blank">Durham vs Yorkshire</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/ci/engine/match/946863.html" target="_blank">Lancashire vs Warwickshire</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="view-btn">
                    <a class="btn orange hvr-float-shadow" href="<?=WWW;?>espn-results.html">View All</a>
                  </div>
				</div> 
				<div id="blog" class="content1" style="min-height:240px; display:none;">
				  <div class="slide10" id="slide10"> <i class="fa fa-minus-circle"></i> International </div>
                  <div id="fixtures_content">
                    <div style="display: block;" class="target10" id="target10">
                      <ul>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/ci/engine/series/913591.html" target="_blank">Sri Lanka in England Test Series</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/ci/engine/series/913601.html" target="_blank">Pakistan in England Test Series</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/ci/engine/series/1022585.html" target="_blank">India in West Indies Test Series</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/ci/engine/series/995435.html" target="_blank">Warne-Muralitharan Trophy</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/ci/engine/series/1024047.html" target="_blank">New Zealand in Zimbabwe Test Series</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/ci/engine/series/936107.html" target="_blank">New Zealand in South Africa Test Series</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/wcl-championship-2015-17/engine/series/870869.html" target="_blank">ICC World Cricket League Championship</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/tri-nation-west-indies-2016/engine/series/932847.html" target="_blank">West Indies Tri-Nation Series</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/ci/engine/series/1007663.html" target="_blank">India in Zimbabwe ODI Series</a></li>
                        <li><a href="http://dev1.youcricketer.com/redirection.php?type=news&amp;url=http://www.espncricinfo.com/ci/engine/series/913597.html" target="_blank">Sri Lanka in Ireland ODI Series</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="view-btn">
                    <a class="btn orange hvr-float-shadow" href="<?=WWW;?>espn-fixtures.html">View All</a>
                  </div>
				</div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <div class="white-box">
                <ul id="menu4" class="menu">
                  <li><a href="#" id="facebook-tab" class="active">Facebook</a></li>
                  <li><a href="#" id="twitter-tab">Twitter</a></li>
			    </ul>
				<div id="facebook" class="content1" style="min-height:400px; #min-height:400px;">
                  <iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2FYouCricketer&amp;width=250&amp;height=400&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=true&amp;show_border=true"   style="border:none; overflow:hidden; width:250px; height:400px;"></iframe>
                </div>
				<div id="twitter" class="content1" style="min-height:400px; display:none;">
				  <h2>Twitter</h2>
				  <a class="twitter-timeline" href="https://twitter.com/YouCricketer" data-widget-id="409331659427700736">Tweets by @YouCricketer</a>
                  <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				</div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
              <div class="right_updates">
                <h3>Latest Updates</h3>
                <p>Online Scoring feature is now available</p>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <a href="<?=WWW?>mobile-apps.html"> <img src="assets/img/ios-android.jpg" alt="Mobile Apps" class="img-app" /> </a>
            </div>
          </div>
        </div>
      </div>
    </div><!-- /.container -->
    
    <footer class="footer">
      <div class="container">
        <div class="end-links">
          <a href="#">Home</a> - 
          <a href="#">Quick Tour</a> - 
          <a href="#">About Us</a> - 
          <a href="#">Multimedia Policies</a> - 
          <a href="#">Privacy Policy and Cookie Use</a> - 
          <a href="#">Terms of Use</a> - 
          <a href="#">Advertising</a> - 
          <a href="#">Careers</a> - 
          <a href="#">Contact Us</a>
        </div>
		<div class="copyright">Copyright &copy; 2016- All Rights Reserved By <span class="orange"><strong>YouCricketer Inc.</strong></span></div>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
    <!--<script src="assets/js/jquery.tabify.js" type="text/javascript" charset="utf-8"></script>-->
    
    <script type="text/javascript">
		$('ul.nav li.dropdown').hover(function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
		
		$( document ).ready(function() {
			$("#companies-tab").click(function(event){
				event.preventDefault(); 
				event.stopPropagation();
				$("#individual").hide();
				$("#companies").show();
				$("#companies-tab").addClass("active");
				$("#individual-tab").removeClass("active");
			});
			$("#individual-tab").click(function(event){
				event.preventDefault(); 
				event.stopPropagation();
				$("#companies").hide();
				$("#individual").show();
				$("#individual-tab").addClass("active");
				$("#companies-tab").removeClass("active");
			});
			
			$("#news-tab").click(function(event){
				event.preventDefault(); 
				event.stopPropagation();
				$("#scoreboard").hide();
				$("#news").show();
				$("#news-tab").addClass("active");
				$("#scoreboard-tab").removeClass("active");
			});
			$("#scoreboard-tab").click(function(event){
				event.preventDefault(); 
				event.stopPropagation();
				$("#news").hide();
				$("#scoreboard").show();
				$("#scoreboard-tab").addClass("active");
				$("#news-tab").removeClass("active");
			});
			
			$("#result-tab").click(function(event){
				event.preventDefault(); 
				event.stopPropagation();
				$("#blog").hide();
				$("#result").show();
				$("#result-tab").addClass("active");
				$("#blog-tab").removeClass("active");
			});
			$("#blog-tab").click(function(event){
				event.preventDefault(); 
				event.stopPropagation();
				$("#result").hide();
				$("#blog").show();
				$("#blog-tab").addClass("active");
				$("#result-tab").removeClass("active");
			});
			
			$("#facebook-tab").click(function(event){
				event.preventDefault(); 
				event.stopPropagation();
				$("#twitter").hide();
				$("#facebook").show();
				$("#facebook-tab").addClass("active");
				$("#twitter-tab").removeClass("active");
			});
			$("#twitter-tab").click(function(event){
				event.preventDefault(); 
				event.stopPropagation();
				$("#facebook").hide();
				$("#twitter").show();
				$("#twitter-tab").addClass("active");
				$("#facebook-tab").removeClass("active");
			});
		});
		
    </script>
  </body>
</html>