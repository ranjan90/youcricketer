<?php include_once('includes/configuration.php');
$page = 'match-basic-edit.html';
if(isset($_SESSION['selected_country']) && !empty($_SESSION['selected_country'])){
	$selected_country = $_SESSION['selected_country'];
}else{
	$ipData 	= file_get_contents('http://api.hostip.info/get_html.php?ip='.$user_ip);
	preg_match('/Country: (.*) /',$ipData, $expression);
	$selected_country = trim($expression[1]);
	if(stripos($selected_country,'unknown') !== false){
		$url = 'http://www.telize.com/geoip';
		$ipData = json_decode(file_get_contents($url.'/'.$user_ip),true);
		$selected_country = $ipData['country'];
	}
	$_SESSION['selected_country'] = $selected_country;
} 

$error = '';
$umpires = array();
$venues = array();
$tournament_permission = 0;
$tournament_id = trim($_GET['t_id']);
$tournament_info = get_record_on_id('tournaments', $tournament_id);	
$match_id = trim($_GET['m_id']);
$match_info = get_record_on_id('tournament_matches', $match_id);	

$page_title = 'Edit Match - '.ucwords($tournament_info['title']);

$sql = "SELECT tm.*,c1.company_name as batting_team1_name,c2.company_name as batting_team2_name,c3.company_name as toss_won_team_name FROM tournament_matches as tm inner join companies as c1 on tm.batting_team1=c1.id  inner join companies as c2 on tm.batting_team2=c2.id
inner join companies as c3 on tm.toss_won_team_id=c3.id WHERE tm.id = $match_id ";
$rs_match = mysql_query($sql);
$match_teams = mysql_fetch_assoc($rs_match);

$match_team_ids = array($match_info['team1'],$match_info['team2']);
	
if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysql_fetch_assoc(mysql_query("select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
	if($user_id){
		$sql = 'select c.* from tournament_teams as t inner join companies as c on t.team_id=c.id where t.tournament_id = '.$tournament_id;
		$rs_club = mysql_query($sql);
		while($row = mysql_fetch_assoc($rs_club)){
			$tournament_clubs[] = $row;
		}
		
		$sql = 'select * from companies where user_id = '.$tournament_info['user_id'];
		$rs_league = mysql_query($sql);
		$league_info = mysql_fetch_assoc($rs_league);
		
		$sql = 'select c.* from league_clubs as t inner join companies as c on t.club_id=c.id where t.league_id = '.$league_info['id'];
		$rs_club = mysql_query($sql);
		while($row = mysql_fetch_assoc($rs_club)){
			$league_clubs[] = $row;
		}
	}
}

if(isset($user_id) && !empty($user_id)){
	$sql = "select * from tournament_permissions where tournament_id = $tournament_id and member_id = $user_id LIMIT 1";
	$rs_permission = mysql_query($sql);
	if($tournament_info['user_id'] == $user_id || mysql_num_rows($rs_permission)>0 ){
		$tournament_permission = 1;
	}else{
		$tournament_permission = 0;
	}
}else{
	$tournament_permission = 0;
}

$sql = "select u.f_name,u.m_name,u.last_name,u.id as user_id from users as u inner join tournament_permissions as tm on u.id=tm.member_id
 where tm.tournament_id = $tournament_id ";
$rs_umpires = mysql_query($sql);
while($row = mysql_fetch_assoc($rs_umpires)){
	$umpires[] = $row;
}

$sql = "SELECT id,venue FROM league_venues WHERE league_id = ".$league_info['id']." ORDER BY id ";
$rs_venues = mysql_query($sql);
while($row = mysql_fetch_assoc($rs_venues)){
	$venues[] = $row;
}

if(isset($_POST['submit_btn']) && !empty($_POST['submit_btn'])){
	validate1();
	if(empty($error)){
		$date = date('Y/m/d H:i:s')	;
		
		$sql = " UPDATE tournament_matches SET start_time='".trim($_POST['start_time'])."',
		umpire1_id='".trim($_POST['umpire1_id'])."',umpire2_id='".trim($_POST['umpire2_id'])."',umpire3_id='".trim($_POST['umpire3_id'])."',
		umpire1_from='".trim($_POST['umpire1_from'])."',umpire2_from='".trim($_POST['umpire2_from'])."',umpire3_from='".trim($_POST['umpire3_from'])."',
		referee_id='".trim($_POST['referee_id'])."',venue_id='".trim($_POST['venue_id'])."',updated_on='".$date."',last_updated_by = $user_id ,
		scorer_1='".trim($_POST['scorer_1'])."',scorer_2='".trim($_POST['scorer_2'])."' WHERE id=".$match_id;
		
		if(mysql_query($sql)){
			$_SESSION['match_updated'] = 1;
			header("Location:".WWW."tournament/matches/list/$tournament_id");
		}else{
			$error = '<p id="error">Error in updating Match. Try again later</p>'.mysql_error().$sql;
		}
	}
}

function validate1(){
	global $error;
	
	if(empty($_POST['start_time'])){
		$error.= '<p id="error">Start Date/Time is required field</p>';
	}
	/*if(empty($_POST['umpire1_id'])){
		$error.= '<p id="error">1st Umpire is required field</p>';
	}*/
	if(empty($_POST['venue_id'])){
		$error.= '<p id="error">Venue is required field</p>';
	}
}

?>
<?php include('common/header.php'); ?>
<style>
.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
dl {min-height:145px;}
.content {width:1010px;}
.large-column {width:790px;}
.form-box {width:60%;}
.chk_left {float:left !important; }
</style>
	<div class="middle">
		<h1>Edit Match Schedule</h1>
		<h2>Tournament: <?php echo $tournament_info['title'] ?></h2>
		<div class="white-box content" id="dashboard">
			<?php if(empty($user_info)): ?>
				<div id="error">You are not logged... !</div>
			<?php endif; ?>
			
			<?php if(!empty($error)): ?>
				<?php echo $error; ?>
			<?php endif; ?>
			
			
		
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
			<?php if($tournament_permission){ ?>
				
					<form method="post"  enctype="multipart/form-data">
						<input type="hidden" name="action" value="submit">
						<fieldset>
							<h2>Match Highlights</h2>
							<!--<p>All Fields are Required</p> -->
							
							<div class="clear"></div>
							<div class="form-box">
								<label>Start Date/Time*</label>
								<div class="text">
									<input type="text" name="start_time" autocomplete="off"   id="start_time" class="input-login" value="<?php if(isset($_POST['start_time'])) echo $_POST['start_time'];else echo $match_info['start_time']; ?>">
									<span style="font-weight:normal;float:right;">System time appears above. Change it accordingly</span>
								</div>
							</div>
							
							<div class="form-box">
								<label>1st Umpire<?php ?></label>
								<div class="text">
									<select name="umpire1_id" id="umpire1_id"> 
									<option value="0">N/A</option>
									<?php for($i=0;$i<count($umpires);$i++): if(isset($_POST['umpire1_id']) && $_POST['umpire1_id'] == $umpires[$i]['user_id']) $sel =  'selected';elseif($match_info['umpire1_id'] == $umpires[$i]['user_id']) $sel = 'selected';else $sel = ''; ?>
										<option <?php echo $sel; ?> value="<?php echo $umpires[$i]['user_id']; ?>"><?php echo $umpires[$i]['f_name']; ?> <?php echo $umpires[$i]['m_name']; ?> <?php echo $umpires[$i]['last_name']; ?></option>
									<?php endfor; ?>
									</select>
								</div>
							</div>
							<div class="clear"></div>
							
							<div class="form-box">
								<label>1st Umpire From</label>
								<div class="text">
								<select name="umpire1_from" id="umpire1_from"> 
								<option value="0">N/A</option>
								<option value="-1" <?php if(isset($_POST['umpire1_from']) && $_POST['umpire1_from'] == -1) echo 'selected';elseif($match_info['umpire1_from'] == -1) echo 'selected'; ?> >Independent</option>
								<?php for($i=0;$i<count($league_clubs);$i++): if(isset($_POST['umpire1_from']) && $_POST['umpire1_from'] == $league_clubs[$i]['id']) $sel =  'selected';elseif($match_info['umpire1_from'] == $league_clubs[$i]['id']) $sel = 'selected';else $sel = '';  ?>
									<option <?php echo $sel; ?> value="<?php echo $league_clubs[$i]['id']; ?>"><?php echo $league_clubs[$i]['company_name']; ?></option>
								<?php endfor; ?>
								</select>
								</div>
							</div>
							<div class="clear"></div>
							
							<div class="form-box">
								<label>2nd Umpire</label>
								<div class="text">
									<select name="umpire2_id" id="umpire2_id"> 
									<option value="0">N/A</option>
									<?php for($i=0;$i<count($umpires);$i++): if(isset($_POST['umpire2_id']) && $_POST['umpire2_id'] == $umpires[$i]['user_id']) $sel =  'selected';elseif($match_info['umpire2_id'] == $umpires[$i]['user_id']) $sel = 'selected';else $sel = ''; ?>
										<option <?php echo $sel; ?> value="<?php echo $umpires[$i]['user_id']; ?>"><?php echo $umpires[$i]['f_name']; ?> <?php echo $umpires[$i]['m_name']; ?> <?php echo $umpires[$i]['last_name']; ?></option>
									<?php endfor; ?>
									</select>
								</div>
							</div>
							<div class="clear"></div>
							
							<div class="form-box">
								<label>2nd Umpire From</label>
								<div class="text">
								<select name="umpire2_from" id="umpire2_from"> 
								<option value="0">N/A</option>
								<option value="-1" <?php if(isset($_POST['umpire2_from']) && $_POST['umpire2_from'] == -1) echo 'selected';elseif($match_info['umpire2_from'] == -1) echo 'selected'; ?> >Independent</option>
								<?php for($i=0;$i<count($league_clubs);$i++): if(isset($_POST['umpire2_from']) && $_POST['umpire2_from'] == $league_clubs[$i]['id']) $sel =  'selected';elseif($match_info['umpire2_from'] == $league_clubs[$i]['id']) $sel = 'selected';else $sel = '';  ?>
									<option <?php echo $sel; ?> value="<?php echo $league_clubs[$i]['id']; ?>"><?php echo $league_clubs[$i]['company_name']; ?></option>
								<?php endfor; ?>
								</select>
								</div>
							</div>
							<div class="clear"></div>
							
							<div class="form-box">
							<label>Ist Scorer</label>
							<div class="text">
								<select name="scorer_1" id="scorer_1"> 
								<option value="0">N/A</option>
								<?php for($i=0;$i<count($umpires);$i++): if(isset($_POST['scorer_1']) && $_POST['scorer_1'] == $umpires[$i]['user_id']) $sel =  'selected';elseif($match_info['scorer_1'] == $umpires[$i]['user_id']) $sel = 'selected';else $sel = '';  ?>
									<option <?php echo $sel; ?> value="<?php echo $umpires[$i]['user_id']; ?>"><?php echo $umpires[$i]['f_name']; ?> <?php echo $umpires[$i]['m_name']; ?> <?php echo $umpires[$i]['last_name']; ?></option>
								<?php endfor; ?>
								</select>
							</div>
							</div>
							<div class="clear"></div>
							
							<div class="form-box">
								<label>2nd Scorer</label>
								<div class="text">
									<select name="scorer_2" id="scorer_2"> 
									<option value="0">N/A</option>
									<?php for($i=0;$i<count($umpires);$i++): if(isset($_POST['scorer_2']) && $_POST['scorer_2'] == $umpires[$i]['user_id']) $sel =  'selected';elseif($match_info['scorer_2'] == $umpires[$i]['user_id']) $sel = 'selected';else $sel = '';  ?>
										<option <?php echo $sel; ?> value="<?php echo $umpires[$i]['user_id']; ?>"><?php echo $umpires[$i]['f_name']; ?> <?php echo $umpires[$i]['m_name']; ?> <?php echo $umpires[$i]['last_name']; ?></option>
									<?php endfor; ?>
									</select>
								</div>
							</div>
							<div class="clear"></div>
							
							<div class="form-box">
								<label>3rd Umpire</label>
								<div class="text">
									<select name="umpire3_id" id="umpire3_id"> 
									<option value="0">N/A</option>
									<?php for($i=0;$i<count($umpires);$i++): if(isset($_POST['umpire2_id']) && $_POST['umpire3_id'] == $umpires[$i]['user_id']) $sel =  'selected';elseif($match_info['umpire3_id'] == $umpires[$i]['user_id']) $sel = 'selected';else $sel = ''; ?>
										<option <?php echo $sel; ?> value="<?php echo $umpires[$i]['user_id']; ?>"><?php echo $umpires[$i]['f_name']; ?> <?php echo $umpires[$i]['m_name']; ?> <?php echo $umpires[$i]['last_name']; ?></option>
									<?php endfor; ?>
									</select>
								</div>
							</div>
							<div class="clear"></div>
							
							<div class="form-box">
								<label>3rd Umpire From</label>
								<div class="text">
								<select name="umpire3_from" id="umpire3_from"> 
								<option value="0">N/A</option>
								<option value="-1" <?php if(isset($_POST['umpire3_from']) && $_POST['umpire3_from'] == -1) echo 'selected';elseif($match_info['umpire3_from'] == -1) echo 'selected'; ?> >Independent</option>
								<?php for($i=0;$i<count($league_clubs);$i++): if(isset($_POST['umpire3_from']) && $_POST['umpire3_from'] == $league_clubs[$i]['id']) $sel =  'selected';elseif($match_info['umpire3_from'] == $league_clubs[$i]['id']) $sel = 'selected';else $sel = '';  ?>
									<option <?php echo $sel; ?> value="<?php echo $league_clubs[$i]['id']; ?>"><?php echo $league_clubs[$i]['company_name']; ?></option>
								<?php endfor; ?>
								</select>
								</div>
							</div>
							<div class="clear"></div>
							
							<div class="form-box">
								<label>Match Referee</label>
								<div class="text">
									<select name="referee_id" id="referee_id"> 
									<option value="0">N/A</option>
									<?php for($i=0;$i<count($umpires);$i++): if(isset($_POST['referee_id']) && $_POST['referee_id'] == $umpires[$i]['id']) $sel =  'selected';elseif($match_info['referee_id'] == $umpires[$i]['id']) $sel = 'selected';else $sel = '';  ?>
										<option <?php echo $sel; ?> value="<?php echo $umpires[$i]['user_id']; ?>"><?php echo $umpires[$i]['f_name']; ?> <?php echo $umpires[$i]['m_name']; ?> <?php echo $umpires[$i]['last_name']; ?></option>
									<?php endfor; ?>
									</select>
									
								</div>
							</div>
							<div class="clear"></div>
							
							<div class="form-box">
								<label>Venue*</label>
								<div class="text">
									<select name="venue_id" id="venue_id"> 
									<option value="0">Select One</option>
									<?php for($i=0;$i<count($venues);$i++): if(isset($_POST['venue_id']) && $_POST['venue_id'] == $venues[$i]['id']) $sel =  'selected';elseif($match_info['venue_id'] == $venues[$i]['id']) $sel = 'selected';else $sel = '';  ?>
										<option <?php echo $sel; ?> value="<?php echo $venues[$i]['id']; ?>"><?php echo $venues[$i]['venue']; ?></option>
									<?php endfor; ?>
									</select>
								</div>
							</div>
							<div class="clear"></div>
							
							<div class="form-box">
								<label>Last Updated</label>
								<div class="text" style="width:auto;"><?php echo date('d F, Y H:i:s', strtotime($match_info['updated_on']));  ?> </div>
							</div>
							
							<?php $last_updated_by = get_record_on_id('users', $match_info['last_updated_by']);	 ?>
							<div class="clear"></div>
							<div class="form-box">
								<label>Last Updated by</label>
								<div class="text" style="width:auto;"><?php echo $last_updated_by['f_name'].' '.$last_updated_by['m_name'].' '.$last_updated_by['last_name'] ;  ?> </div>
							</div>
							
							<div class="clear"></div>
							<div class="form-box">
								<input type="button" name="cancel_btn" value=" Cancel " class="submit-login" onclick="window.location.href='<?php echo WWW; ?>tournament/matches/list/<?php echo $tournament_id; ?>';">
								<input type="submit" name="submit_btn" value=" Update " class="submit-login" >
							</div>
						</fieldset>
					</form>
				
			<?php }else{ ?>
				<div id="error">You do not have Permission for this Tournament... !</div>
			<?php } ?>
			</div>  
		    <div class="clear"></div>
			</div>
		</div>
		<!--<div class="rightbar"><?php //include('common/right-panel.php');?></div>-->
		<div class="clear"></div>
	</div>
	

<script src="<?php echo WWW; ?>js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo WWW; ?>css/jquery.datetimepicker.css"/>
<script>
$('#start_time').datetimepicker();
</script>
<?php include('common/footer.php'); ?>