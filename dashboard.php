<?php include('common/header.php'); ?>
<? 	if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>

<?php

if(empty($row_user['country_id'])){
		?>
		<script>
			window.location = '<?=WWW?>account-information.html';
		</script>
		<?
		}
		$browser = getBrowser();
		if($browser['name'] == 'MSIE' && $browser['version'] < 11){
			?><div id="information">Your browser is out dated, Please install latest version <a href="http://www.microsoft.com/en-pk/download/details.aspx?id=40902">HERE</a></div><?
		}

			            			$width = 9;
									if($row_user['is_company'] == '0'){
										$row_img = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_user['id']."' and is_default = '1'"));
										$row_vid = mysqli_fetch_assoc(mysqli_query($conn,"select * from videos where entity_type = 'users' and entity_id = '".$row_user['id']."' and is_default = '1'"));
										$row_club= mysqli_fetch_assoc(mysqli_query($conn,"select * from users_to_clubs where user_id = '".$row_user['id']."'"));
										$row_leag= mysqli_fetch_assoc(mysqli_query($conn,"select * from users_to_leagues where user_id = '".$row_user['id']."'"));
										$rs_exp  = mysqli_query($conn,"select * from experiences where user_id = '".$row_user['id']."'");
										$rs_dom  = mysqli_query($conn,"select * from opportunities where user_id = '".$row_user['id']."' and type = 'domestic'");
										$rs_int  = mysqli_query($conn,"select * from opportunities where user_id = '".$row_user['id']."' and type = 'international'");
										$rs_trav = mysqli_query($conn,"select * from travel_visas where user_id = '".$row_user['id']."' ");
										$rs_cric = mysqli_query($conn,"select * from favorite_cricketers where user_id = '".$row_user['id']."' ");
										$rs_brand= mysqli_query($conn,"select * from favorite_brands where user_id = '".$row_user['id']."' ");
										$rs_lang = mysqli_query($conn,"select * from languages where user_id = '".$row_user['id']."' ");

										if($row_img){
											$width += 9;
										}
										if(!empty($row_user['city_id'])){
											$width += 9;
										}
										if(!empty($row_user['type'])
										&&(!empty($row_user['type_of_batsman']) || !empty($row_user['type_of_bowler']))
										&& !empty($row_user['match_types'])
										&& !empty($row_user['game_types'])){
											$width += 9;
										}
										if($row_club
										&& $row_leag
										&& !empty($row_user['agent'])){
											$width += 9;
										}
										if($row_vid){
											$width += 9;
										}
										if(!empty($row_user['currently_playing'])
										&& !empty($row_user['no_of_playing'])
										&& mysqli_num_rows($rs_exp) > 0){
											$width += 9;
										}
										if(mysqli_num_rows($rs_dom) > 0
										|| mysqli_num_rows($rs_int) > 0){
											$width += 9;
										}
										if(mysqli_num_rows($rs_trav) > 0
										&& !empty($row_user['citizen'])
										&& $row_user['passport'] != ''){
											$width += 9;
										}
										if((mysqli_num_rows($rs_cric) > 0 || mysqli_num_rows($rs_brand) > 0)
										&& mysqli_num_rows($rs_lang) > 0
										&& !empty($row_user['phone'])){
											$width += 9;
										}
										if(!empty($row_user['facebook'])
										&& !empty($row_user['twitter'])){
											$width += 10;
										}

										if($width <= 40){
											$bg = 'red';
										}else if($width <= 90){
											$bg = 'orange';
										}else{
											$bg = 'green';
										}

									  $profile_link1 = 'profile-information-step-1.html';

									}else{

										if(empty($row_comp['office_phone'])){
											$bg   = 'red';
											$width= '50';
											$number=2;
										}else if(!empty($row_comp['office_phone'])){
											$bg   = 'orange';
											$width= '75';
										}
										$profile_link1 = 'update-company-profile-1.html';
									}

?>
<? 	 	if(isset($_POST['action']) && !empty($_POST) && $_POST['action'] == 'comment'){
			$activity_id 	= $_POST['activity_id'];
			$comments 		= mysqli_real_escape_string($conn,$_POST['comments']);
			$user_id 		= $_SESSION['ycdc_dbuid'];
			$datetime 		= date('Y-m-d H:i:s');
			$rsCheck 		= mysqli_query($conn,"select * from activity_comments where activity_id = '$activity_id' and user_id = '$user_id' and comments = '$comments' ");
			if(mysqli_num_rows($rsCheck) == 0){
				$query 		= "insert into activity_comments (activity_id, user_id, create_date, comments)
								values ('$activity_id','$user_id','$datetime','$comments');";
				if(mysqli_query($conn,$query)){
					echo '<div id="success" class="alert alert-success">Comments saved... !</div>';
				}else{
					echo '<div id="error" class="alert alert-danger">Comments cannot be saved... !</div>';
				}
			}else{
				echo '<div id="error" class="alert alert-danger">Comments already exists ... !</div>';
			}
		}
		if(isset($_POST['action']) && $_POST['action'] == 'submit-activity'){


			$activity 		= mysqli_real_escape_string($conn,$_POST['activity']);
			if(!empty($activity)){
				$activity_date 	= date('Y-m-d H:i:s');
				$user_id 		= $row_user['id'];
				$chk_date 		= date('Y-m-d');
				/*********************************************/
				if(isset($_FILES) && count($_FILES) > 0){
					preg_match('/image/', $_FILES['file']['type'], $matches);
					if($matches[0] == 'image'){
						$type = 'photos';
					}
					preg_match('/video/', $_FILES['file']['type'], $matches);
					if($matches[0] == 'video'){
						$type = 'videos';
					}
					$user_id = $_SESSION['ycdc_dbuid'];
					if(!is_dir('users/'.$user_id)){
						mkdir('users/'.$user_id,0777);
					}
					chmod('users/'.$user_id,0777);

					if($type == 'photos'){
						if(!is_dir('users/'.$user_id.'/photos')){
							mkdir('users/'.$user_id.'/photos',0777);
						}
						chmod('users/'.$user_id.'/photos',0777);
						if(!empty($_FILES['file']['name'])){
							$filename 	= friendlyURL($_SESSION['ycdc_user_name']).'-'.friendlyURL($_FILES['file']['name']).'.jpg';
							$rs_chk 	= mysqli_query($conn,"select * from photos where entity_id = '$user_id' and entity_type = 'users' and file_name = '$filename'");
							if(mysqli_num_rows($rs_chk) == 0){
								$image 		= new SimpleImage();
								$image->load($_FILES["file"]["tmp_name"]);
								if($image->getWidth() > 800){
									$image->resizeToWidth(800);
								}
								$image->save('users/'.$user_id.'/photos/'.$filename);
								chmod('users/'.$user_id.'/photos/'.$filename,0777);
								mysqli_query($conn,"insert into photos (file_name, entity_id, entity_type, title)
									values ('$filename','$user_id','users','$activity')");
								//echo '<div id="success" class="alert alert-success"><b>Success : </b>Activity updated ... !</div>';
							}
						}
					}else{
						if($_FILES['file']['type'] == 'video/mp4'
				  		|| $_FILES['file']['type'] == 'video/webm'
				  		|| $_FILES['file']['type'] == 'video/ogg'
				  		|| $_FILES['file']['type'] == 'video/mov'
				  		|| $_FILES['file']['type'] == 'video/quicktime'){
				  			$exte = explode('.', $_FILES['file']['name']);
				  			$ext  = $exte[count($exte)-1];
				  			$filename 	= friendlyURL($_SESSION['ycdc_user_name']).'-'.friendlyURL($_FILES['file']['name']).'.'.$ext;
							$rs_check   = mysqli_query($conn,"select * from videos where entity_type = 'users' and entity_id = '".$user_id."' and file_name = '".$filename."'");
							mysqli_query($conn,"insert into videos (file_name, status, entity_id, entity_type, title)
							values ('".$filename."','1','".$user_id."','users','$activity');");
							$video_id = mysqli_insert_id($conn);
							mkdir('videos/'.$video_id,0777);
							chmod('videos/'.$video_id,0777);
							move_uploaded_file($_FILES['file']['tmp_name'], 'videos/'.$video_id.'/'.$filename);
							chmod('videos/'.$video_id.'/'.$filename,0777);
							//echo '<div id="success"><b>Success : </b>Information updated ... !</div><br><br>';
				  		}
					}
				}
				/*********************************************/
				$rs_chk = mysqli_query($conn,"select * from activity where user_id = '$user_id' and activity = '$activity' and activity_date like '$chk_date%'");
				if(mysqli_num_rows($rs_chk) == 0){
					$query = "insert into activity (user_id, activity_date, activity, status) values ('$user_id','$activity_date','$activity','1');";
					mysqli_query($conn,$query);
					echo '<div id="success" class="alert alert-success">Activity Added successfully ... !</div>';
				}else {
					echo '<div id="error" class="alert alert-danger">Activity already exists ... !</div>';
				}
			}else{
				echo '<div id="error" class="alert alert-danger">Write something
				 ... !</div>';
			}
		}
		if(isset($_GET['action']) && $_GET['action'] == 'delete' && $_GET['type'] == 'activity'){
			$id 		= $_GET['id'];
		  $result =	mysqli_query($conn,"delete from activity where id = '$id'");

		if($result){
			echo '<div id="success" class="alert alert-success">Activity Deleted successfully ... !</div>';
               ?>
			<script>
					window.location.href = "<?php echo WWW.'dashboard.html' ?>";
			</script>
      <?
				}
		}
		if(isset($_POST['action']) && $_POST['action'] == 'activity-update'){
			$activity   = mysqli_real_escape_string($conn,$_POST['activity']);
			$id 		= $_POST['recordId'];
			/***/
			$activityRow= get_record_on_id('activity', $id);
			$rowPhoto 	= mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$_SESSION['ycdc_dbuid']."' and title = '".$activityRow['activity']."'"));
			$rowVideo 	= mysqli_fetch_assoc(mysqli_query($conn,"select * from videos where entity_type = 'users' and entity_id = '".$_SESSION['ycdc_dbuid']."' and title = '".$activityRow['activity']."'"));

			if($rowPhoto){
				mysqli_query($conn,"update photos set title = '$activity' where id = '".$rowPhoto['id']."'");
			}
			if($rowVideo){
				mysqli_query($conn,"update videos set title = '$activity' where id = '".$rowVideo['id']."'");
			}
			/***/
			$query 		= "update activity set activity = '$activity' where id = '$id'";
			$results = mysqli_query($conn,$query);
		if($results) {
			echo '<div id="success" class="alert alert-success">Activity updated successfully ... !</div>';	}
		}
?>
<div class="page-container">
	<?php include('common/user-left-panel.php');?>

	<!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">

          <div class="white-box">
			<?php if($width <= 50){ ?>
            <div class="row">
              <div class="col-md-12">
                <div class="alert alert-info"> <i class="fa fa-exclamation-circle"></i> <a href="<?php echo $profile_link1;?>">Your profile is <?php echo $width; ?>% complete</a>
                </div>
              </div>
            </div>
			<?php } ?>
            <div class="progress">
              <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="19" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $width; ?>%"> <?php echo $width; ?>% Complete </div>
            </div>
            <div class="row">
              <div class="col-sm-9 col-lg-10">
                <h2> My Status / Activity </h2>
              </div>
              <div class="col-sm-3 col-lg-2">
                <a href="<?php echo WWW;?>invite-friends.html" class="btn orange full hvr-float-shadow margin-top-10">Invite Friends</a>
              </div>
            </div>

            <form method="post" action="" enctype="multipart/form-data" class="form-horizontal"  id="add-activity-frm">
              <input name="action" value="submit-activity" type="hidden">
              <div class="form-group">
                <!--<label class="col-sm-5 control-label"> Whats on your mind or heart or both?: </label>-->
								<div class="col-sm-1">
									<?php $row_img 	= mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$_SESSION['ycdc_dbuid']."' and is_default = '1' ")); ?>
                  <img src="<?php echo WWW;?><?=($row_img && !empty($row_img['file_name']))?'users/'.$_SESSION['ycdc_dbuid'].'/photos/'.$row_img['file_name']:'images/no-photo.jpg'?>" class="img-responsive" style="width:100%; height:46px;">
                </div>
								<div class="col-sm-11">
                  <textarea name="activity" class="form-control " required rows="5" placeholder="Whats on your mind or heart or both?"></textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"> Attach Photo/Video: </label>
                <div class="col-sm-7">
                  <input accept="image/*, video/*" name="file" type="file" class="valid-image">
				  <span class="help-block">
                      Max Photo/Video size : 10MB/20MB &nbsp; &nbsp; &nbsp; <a href="multimedia-policy.html" target="_blank">Multimedia Policy</a>
                    </span>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-5 col-sm-7">
                  <input class="btn orange hvr-float-shadow" value="Post" type="submit">
                  <a class="btn blue hvr-float-shadow" href="<?php echo WWW;?>dashboard.html">Cancel</a>
                </div>
              </div>
            </form>




			        	<? 	$userId 	= $_SESSION['ycdc_dbuid'];
			        		$rsFriends 	= mysqli_query($conn,"select * from friendship where (from_user_id = '$userId' or to_user_id = '$userId') and status = '1'");
			        		$friends 	= array();
			        		while($rowF = mysqli_fetch_assoc($rsFriends)){
			        			if($rowF['from_user_id'] == $userId){
			        				$friends[] 	= $rowF['to_user_id'];
			        			}else{
			        				$friends[] 	= $rowF['from_user_id'];
			        			}
			        		}
			        		$friends[] 			= $userId;

			        		$rs_msg = mysqli_query($conn,"select * from activity where user_id in (".implode(',',$friends).") and activity not like '%Logged in%' order by id desc limit 15");
			        		if(mysqli_num_rows($rs_msg) == 0){
			        			?>
			        			<div class="row">
								  <div class="col-sm-12">
									<div class="alert alert-danger"> <i class="fa fa-exclamation-circle"></i> No Activity </div>
								  </div>
								</div>
			        			<?
			        		}else{
				        		while($row_msg = mysqli_fetch_assoc($rs_msg)){
				        			$rowUserActivity= get_record_on_id('users', $row_msg['user_id']);
				        			$row_img 		= mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$rowUserActivity['id']."' and is_default = '1' "));
				        			$rowStatusLike	= mysqli_query($conn,"select * from status_likes where activity_id = '".$row_msg['id']."' and user_id = '$userId'");
				        			$rsStatusLike 	= mysqli_query($conn,"select * from status_likes where activity_id = '".$row_msg['id']."'");
				        			$rsStatusShare 	= mysqli_query($conn,"select * from status_shares where activity_id = '".$row_msg['id']."'");
				        			$rsStatusComment= mysqli_query($conn,"select * from activity_comments where activity_id = '".$row_msg['id']."'");
				        			if($rowStatusLike){
				        				$imgTitle 	= 'Loved It';
				        			}else{
				        				$imgTitle 	= 'Love It';
				        			}
				        			$loversData 	= array();
				        			$sharersData 	= array();
				        			$commentData 	= array();
				        			if(mysqli_num_rows($rsStatusComment) > 0){
				        				while($rowStatusComment = mysqli_fetch_assoc($rsStatusComment)){
				        					$commentData[]    = $rowStatusComment['user_id'];
				        				}
				        				$commentCounter = '('.mysqli_num_rows($rsStatusComment).')';
				        			}else{
				        				$commentCounter = '';
				        			}
				        			if(mysqli_num_rows($rsStatusLike) > 0){
				        				while($rowStatusLike = mysqli_fetch_assoc($rsStatusLike)){
				        					$loversData[]    = $rowStatusLike['user_id'];
				        				}
				        				$loveCounter = '('.mysqli_num_rows($rsStatusLike).')';
				        			}else{
				        				$loveCounter = '';
				        			}
				        			if(mysqli_num_rows($rsStatusShare) > 0){
				        				while($rowStatusShare = mysqli_fetch_assoc($rsStatusShare)){
				        					$sharersData[]    = $rowStatusShare['user_id'];
				        				}
				        				$sharersCounter = '('.mysqli_num_rows($rsStatusShare).')';
				        			}else{
				        				$sharersCounter = '';
				        			}
				        			?>




            <div class="panel panel-default" id="dashboard-activity">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-sm-1">
					<img src="<?php echo WWW;?><?=($row_img && !empty($row_img['file_name']))?'users/'.$rowUserActivity['id'].'/photos/'.$row_img['file_name']:'images/no-photo.jpg'?>" class="img-responsive" style="max-width:40px;"/>
				  </div>
                  <div class="col-sm-7">
                    <h3 style="font-size:20px;margin-bottom:8px;"><?php echo $rowUserActivity['f_name'].' '.$rowUserActivity['last_name'];?></h3>
                    <span id="dashboard-datetime"><?=date_converter($row_msg['activity_date'])." ".date("H:i:s",strtotime($row_msg['activity_date']))?></span>
                  </div>
				  <?php if($row_msg['user_id'] == $userId){ ?>
                  <div class="col-sm-2">
                    <a class="btn blue full hvr-float-shadow" onclick="return confirm('Are you sure that you want to delete this record ?')" href="<?=WWW?>dashboard.html?action=delete&type=activity&id=<?=$row_msg['id']?>"><i class="fa fa-times"></i></a>
                  </div>
                  <div class="col-sm-2">
                    <a class="btn orange full hvr-float-shadow" href="#" onClick="editActivity(<?=$row_msg['id']?>);" id="<?=$row_msg['id']?>" data-toggle="modal" data-target="#edit-activity"><i class="fa fa-edit"></i></a>
                  </div>
				  <?php } ?>
                </div>
              </div>
              <div class="panel-body">
                <div class="row">
					<div class="col-sm-6">
						<?=$row_msg['activity']?>
					</div>
					<div class="col-sm-6" id="gallery">
					<? 	$rsPhotos = mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_msg['user_id']."' and title = '".$row_msg['activity']."'");
				        	$rsVideos = mysqli_query($conn,"select * from videos where entity_type = 'users' and entity_id = '".$row_msg['user_id']."' and title = '".$row_msg['activity']."'");
				        	if(mysqli_num_rows($rsPhotos) > 0){
								$rowPhoto = mysqli_fetch_assoc($rsPhotos);
				        	?>
				        	<a href="<?=WWW?>users/<?=$row_msg['user_id']?>/photos/<?=$rowPhoto['file_name']?>" class="photo">
				        		<img style="border-radius:4px; border:1px solid #ccc;" src="<?=WWW?>users/<?=$row_msg['user_id']?>/photos/<?=$rowPhoto['file_name']?>" width="100">
				        	</a><?
				        	}
				        	if(mysqli_num_rows($rsVideos) > 0){
				        		$rowVideo = mysqli_fetch_assoc($rsVideos);
				        		if(!empty($rowVideo['file_name'])){
									$filename = explode('.',$rowVideo['file_name']);
									$filename1= $filename[0];
									$video = '<video width="220" height="130" controls>
									<source src="'.WWW.'videos/'.$rowVideo['id'].'/'.$filename1.'.mp4" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
									<source src="'.WWW.'videos/'.$rowVideo['id'].'/'.$filename1.'.ogg" type=\'video/ogg; codecs="theora, vorbis"\'>
									<source src="'.WWW.'videos/'.$rowVideo['id'].'/'.$filename1.'.mov" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
									<source src="'.WWW.'videos/'.$rowVideo['id'].'/'.$filename1.'.webm" type=\'video/webm; codecs="vp8, vorbis"\'>
									</video>';
									echo $video;
				        		}
							}
				       ?>
					</div>
                </div>
              </div>
              <div class="panel-footer">
                <div class="row">
                  <div class="col-sm-12">
                    <a href="javascript:;" class="love-it"  onClick="javascript:void(0)"  id="<?=$row_msg['id']?>"> <i <?php if(in_array($_SESSION['ycdc_dbuid'], $loversData)): ?>style="color:#ff0000;" <?php endif; ?> class="fa fa-heart-o"></i> </a>
                    <a class="love-it-counter"  href="javascript:;" id="<?=$row_msg['id']?>" onClick="getLikes(<?=$row_msg['id']?>)" title="Who Loved it" id="<?=$row_msg['id']?>"   data-toggle="modal" data-target="#activity-likes"><?=$loveCounter;?></a>

                    <a href="javascript:;" title="Comment on It"  onClick="getComments(<?=$row_msg['id']?>)" data-toggle="modal" data-target="#activity-comments" id="<?=$row_msg['id']?>"> <i class="fa fa-comments"></i> </a>
                    <a title="Who Commented it" class="comment-it-counter" href="javascript:;" id="<?=$row_msg['id']?>" onClick="getComments(<?=$row_msg['id']?>)" data-toggle="modal" data-target="#activity-comments"><?=$commentCounter?></a>

										<a href="javascript:;" title="Share It" class="share-it"  onClick="javascript:void(0)" id="<?=$row_msg['id']?>"> <i class="fa fa-share"></i> </a>
                    <a title="Who Shared it" class="share-it-counter" href="javascript:;" id="<?=$row_msg['id']?>" onClick="getShares(<?=$row_msg['id']?>)" data-toggle="modal" data-target="#activity-shares"><?=$sharersCounter;?></a>
                  </div>
                </div>
              </div>
            </div>

			<?  }
			} ?>

          </div>
        </div>
      </div>
      <!-- END CONTENT-->

</div>



    <div class="modal fade" id="edit-activity" tabindex="-1" role="dialog" aria-labelledby="edit-activityModel" aria-hidden="true">
      <div class="modal-dialog" style="max-height:1800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="settingLabel">Update your activity</h4>
          </div>
          <div class="modal-body" style="max-height:1700px">
            <form class="form-horizontal" method="post" id="edit-activity-frm">
              <input name="action" value="activity-update" type="hidden">
              <input name="recordId" id="recordId" value="" type="hidden">
              <div class="form-group">
                <label class="col-sm-5 control-label">Type your activity here</label>
                <div class="col-sm-7">
                  <textarea name="activity" id="edit_activity" class="form-control validate[required]" rows="5"></textarea>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-5 col-sm-7">
                  <input value="Save" class="btn orange hvr-float-shadow" type="submit">
                </div>
              </div>
            </form>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.edit-activity-Model -->

    <div class="modal fade" id="activity-likes" tabindex="-1" role="dialog" aria-labelledby="activity-likesModel" aria-hidden="true">
      <div class="modal-dialog" style="max-height:1800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="settingLabel">Your Activity Lovers</h4>
          </div>
          <div class="modal-body" style="max-height:1700px" id="lovers">

          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.activity-likes-Model -->

    <div class="modal fade" id="activity-shares" tabindex="-1" role="dialog" aria-labelledby="activity-sharesModel" aria-hidden="true">
      <div class="modal-dialog" style="max-height:1800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="settingLabel">Your Activity Shared</h4>
          </div>
          <div class="modal-body" style="max-height:1700px" id="sharers">

          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.activity-likes-Model -->

    <div class="modal fade" id="activity-comments" tabindex="-1" role="dialog" aria-labelledby="activity-commentsModel" aria-hidden="true">
      <div class="modal-dialog" style="max-height:1800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="settingLabel">Activity Comments</h4>
          </div>
          <div class="modal-body" style="max-height:1700px">
            <div id="comments">

            </div>
            <form class="form-horizontal" method="post" id="comment-add-frm">
              <input name="activity_id" id="activity_id" value="" type="hidden">
              <input name="action" value="comment" type="hidden">
              <div class="form-group">
                <label class="col-sm-1 control-label"><strong>Comment:</strong></label>
                <div class="col-sm-7">
                  <textarea name="comments" class="form-control validate[required]" rows="5"></textarea>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-1 col-sm-1">
                  <input value="Post" class="btn orange hvr-float-shadow" type="submit">
                </div>
              </div>
            </form>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.activity-comments-Model -->

<script src="<?=WWW?>js/jquery.lightbox-0.5.min.js" type="text/javascript"></script>
<link href="<?=WWW?>css/jquery.lightbox-0.5.css" type="text/css" rel="stylesheet">
<script>
$(document).ready(function(){
	var WWW = "<?php echo WWW; ?>"

	$("#add-activity-frm").validate();

	editActivity = function(id){

			$.ajax({type	: 'POST',
				url		: WWW + 'includes/get-record.php',
				data	: ({table:'activity',field:'activity',id:id}),
				success	: function(msg){
					$("#edit_activity").val(msg);
					$('#recordId').val(id);
				}
			});
	}

	$('a.love-it').click(function(){
		var id = this.id;
		var ele = this;
		$.ajax({type	: 'GET',
		   	url			: WWW + 'includes/put-love-it.php?aid=' + id,
			success		: function(msg){
				if(msg != 'false'){//alert($(ele).html());
					if(msg == '0'){
						$(ele).html('<i class="fa fa-heart-o" ></i>');
						$(ele).parents('.col-sm-12').find('.love-it-counter').html('(' + msg + ')');
					}else{
						$(ele).html('<i class="fa fa-heart-o" style="color:#ff0000;"></i>');
						$(ele).parents('.col-sm-12').find('.love-it-counter').html('(' + msg + ')');
					}
				}
			}
    	});
	});

	$('a.share-it').click(function(){
		var id = this.id;
		var ele = this;
		$.ajax({type	: 'GET',
		   	url			: WWW + 'includes/put-share-it.php?aid=' + id,
			success		: function(msg){
				if(msg != 'false'){
					$(ele).parents('.col-sm-12').find('.share-it-counter').html('(' + msg + ')');
				}
			}
    	});
	});

	getShares = function(id){
		$.ajax({type	: 'POST',
				url		: WWW + 'includes/get-activity-info.php',
				data	: ({type:'shares',id:id}),
				success	: function(msg){
					$('#sharers').html(msg);
				}
		});
	}

	getLikes = function(id){
		$.ajax({type	: 'POST',
				url		: WWW + 'includes/get-activity-info.php',
				data	: ({type:'loves',id:id}),
				success	: function(msg){
					$('#lovers').html(msg);
				}
			});
	}

	getComments = function(id){
		$.ajax({type	: 'POST',
			url		: WWW + 'includes/get-activity-info.php',
			data	: ({type:'comments',id:id}),
			success	: function(msg){
			$('#comments').html(msg);
		}
		});
		$('#activity_id').val(id);
	}

	$('#add-activity-frm,#edit-activity-frm,#comment-add-frm').validationEngine();
	$('#gallery a.match-gallery-link,a.photo').lightBox();
});
</script>



<?php include('common/footer.php'); ?>
