<?php include_once('includes/configuration.php');
$page = 'match-header.html';
$selected_country = getGeoLocationCountry(); 

$error = '';
$umpires = array();
$venues = array();
$tournament_permission = 0;
$tournament_id = trim($_GET['t_id']);
$tournament_info = get_record_on_id('tournaments', $tournament_id);	
$match_id = trim($_GET['m_id']);
$match_info = get_record_on_id('tournament_matches', $match_id);	

$page_title = 'Edit Match - '.ucwords($tournament_info['title']);

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
	if($user_id){
		$sql = 'select c.* from tournament_teams as t inner join companies as c on t.team_id=c.id where t.tournament_id = '.$tournament_id;
		$rs_club = mysqli_query($conn,$sql);
		while($row = mysqli_fetch_assoc($rs_club)){
			$tournament_clubs[] = $row;
		}
		
		$sql = 'select * from companies where user_id = '.$tournament_info['user_id'];
		$rs_league = mysqli_query($conn,$sql);
		$league_info = mysqli_fetch_assoc($rs_league);
		
		$sql = 'select c.* from league_clubs as t inner join companies as c on t.club_id=c.id where t.league_id = '.$league_info['id'];
		$rs_club = mysqli_query($conn,$sql);
		while($row = mysqli_fetch_assoc($rs_club)){
			$league_clubs[] = $row;
		}
	}
}

if(isset($user_id) && !empty($user_id)){
	$sql = "select * from tournament_permissions where tournament_id = $tournament_id and member_id = $user_id LIMIT 1";
	$rs_permission = mysqli_query($conn,$sql);
	if($tournament_info['user_id'] == $user_id || mysqli_num_rows($rs_permission)>0 ){
		$tournament_permission = 1;
	}else{
		$tournament_permission = 0;
	}
}else{
	$tournament_permission = 0;
}

$sql = "select u.f_name,u.m_name,u.last_name,u.id as user_id from users as u inner join tournament_permissions as tm on u.id=tm.member_id
 where tm.tournament_id = $tournament_id ";
$rs_umpires = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_umpires)){
	$umpires[] = $row;
}

$sql = "SELECT id,venue FROM league_venues WHERE league_id = ".$league_info['id']." ORDER BY id ";
$rs_venues = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_venues)){
	$venues[] = $row;
}

if(isset($_POST['submit_btn']) && !empty($_POST['submit_btn'])){
	validate1();
	if(empty($error)){
		$date = date('Y/m/d H:i:s')	;
		if(isset($_POST['start_time'])){
			$start_time = trim($_POST['start_time']);
		}else{
			$start_time = date('Y/m/d',strtotime($match_info['start_time'])).' '.$_POST['start_hours'].':'.$_POST['start_minutes'];
		}
		
		$sql = " UPDATE tournament_matches SET maximum_overs='".trim($_POST['maximum_overs'])."',start_time='".trim($start_time)."',
		umpire1_id='".trim($_POST['umpire1_id'])."',umpire2_id='".trim($_POST['umpire2_id'])."',umpire3_id='".trim($_POST['umpire3_id'])."',
		umpire1_from='".trim($_POST['umpire1_from'])."',umpire2_from='".trim($_POST['umpire2_from'])."',umpire3_from='".trim($_POST['umpire3_from'])."',
		referee_id='".trim($_POST['referee_id'])."',venue_id='".trim($_POST['venue_id'])."',updated_on='".$date."',last_updated_by = $user_id ,
		scorer_1='".trim($_POST['scorer_1'])."',scorer_2='".trim($_POST['scorer_2'])."'";
		
		if(isset($_POST['team1'])){
			$sql.= ",team1=".$_POST['team1'].",team2=".$_POST['team2'];
		}
		$sql.=" WHERE id=".$match_id;
		
		if(mysqli_query($conn,$sql)){
			$_SESSION['match_updated'] = 1;
			header("Location:".WWW."tournament/matches/list/$tournament_id");
		}else{
			$error = '<p id="error" class="alert alert-danger">Error in updating Match. Try again later</p>';
		}
	}
}

function validate1(){
	global $error;
	
	if(empty($_POST['maximum_overs'])){
		$error.= '<p id="error" class="alert alert-danger">Overs is required field</p>';
	}
	if(empty($_POST['venue_id'])){
		$error.= '<p id="error" class="alert alert-danger">Venue is required field</p>';
	}
}

?>
<?php include('common/header.php'); ?>


<div class="page-container"> 
		<?php  include('common/user-left-panel.php');?>
      <!-- END SIDEBAR -->
      
      <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
		<div class="page-content">
		<?php  include('common/breadcrumbs.php');?>
		
		    <div class="white-box">
            <div class="row">
              <div class="col-sm-12">
                <h2> Edit Match Details </h2>
                <h3> Tournament: <?php echo $tournament_info['title'] ?> </h3>
              </div>
            </div>
            
            <div class="row">
              <div class="col-sm-12">
                <h3> Edit Match </h3>
                <p>Fields with * are Required</p>
				<?php if(empty($user_info)): ?>
					<div id="error" class="alert alert-danger">You are not logged... !</div>
				<?php endif; ?>
				
				<?php if(!empty($error)): ?>
					<?php echo $error; ?>
				<?php endif; ?>
              </div>
            </div>
			
			<?php if($tournament_permission){ ?>
            <div class="row">
              <div class="col-sm-12">
                <form method="post" enctype="multipart/form-data" class="form-horizontal">
                  <input name="action" value="submit" type="hidden">
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Date/Time*</label>
                    <div class="col-sm-7">
                      
					  <?php if($tournament_info['user_id'] == $user_id): ?>
									<input type="text" name="start_time" autocomplete="off"   id="start_time" class="form-control" value="<?php if(isset($_POST['start_time'])) echo $_POST['start_time'];else echo $match_info['start_time']; ?>">
						<?php else: ?>	
									<span style="margin-top:10px;float:left;"><?php echo date('d F, Y',strtotime($match_info['start_time'])); ?></span>
									
									<select name="start_minutes"  id="start_minutes" class="form-control1" style="width:90px;margin-left:5px;height:30px;border:1px solid #ccc;border-radius:3px;">
									<option value="0">Minutes</option>
									<?php for($i=0;$i<60;$i++): ?>
									<?php if(isset($_POST['start_minutes']) && $_POST['start_minutes'] == $i) $sel =  'selected';elseif(date('i',strtotime($match_info['start_time'])) == $i) $sel = 'selected';else $sel = ''; ?>
										<option <?php echo $sel; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
									<?php endfor; ?>
									</select>
									<select name="start_hours"  id="start_hours" class="form-control1" style="width:80px;height:30px;border:1px solid #ccc;border-radius:3px;">
									<option value="0">Hours</option>
									<?php for($i=0;$i<24;$i++): ?>
										<?php if(isset($_POST['start_hours']) && $_POST['start_hours'] == $i) $sel =  'selected';elseif(date('H',strtotime($match_info['start_time'])) == $i) $sel = 'selected';else $sel = ''; ?>
										<option <?php echo $sel; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
									<?php endfor; ?>
									</select>
						<?php endif; ?>
						<span class="help-block">Change the time to actual time game started</span>
                    </div>
                  </div>
                  
				  <div class="form-group">
                    <label class="col-sm-5 control-label">Overs*</label>
                    <div class="col-sm-7 text">
                      <input type="text" name="maximum_overs" id="maximum_overs" class="form-control" value="<?php if(isset($_POST['maximum_overs'])) echo $_POST['maximum_overs'];else echo $match_info['maximum_overs']; ?>">
					  
                    </div>
                  </div>
				  
				  <?php if(empty($match_info['toss_won_team_id']) || empty($match_info['toss_won_decision'])): ?>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Host Team*</label>
                    <div class="col-sm-7 text">
                      <select name="team1" id="team1" class="form-control">
                        <option value="">Select One</option>
							<?php for($i=0;$i<count($tournament_clubs);$i++): if(isset($_POST['team1']) && $_POST['team1'] == $tournament_clubs[$i]['id']) $sel =  'selected';elseif(isset($match_info['team1']) &&  $match_info['team1'] == $tournament_clubs[$i]['id']) $sel =  'selected';  else $sel = '';  ?>
								<option <?php echo $sel; ?> value="<?php echo $tournament_clubs[$i]['id']; ?>"><?php echo $tournament_clubs[$i]['company_name']; ?></option>
							<?php endfor; ?>
                      </select>
					  
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Guest Team*</label>
                    <div class="col-sm-7 text">
                      <select name="team2" id="team2" class="form-control">
                        <option value="">Select One</option>
							<?php for($i=0;$i<count($tournament_clubs);$i++): if(isset($_POST['team2']) && $_POST['team2'] == $tournament_clubs[$i]['id']) $sel =  'selected';elseif(isset($match_info['team2']) &&  $match_info['team2'] == $tournament_clubs[$i]['id']) $sel =  'selected';  else $sel = '';  ?>
							<option <?php echo $sel; ?> value="<?php echo $tournament_clubs[$i]['id']; ?>"><?php echo $tournament_clubs[$i]['company_name']; ?></option>
						<?php endfor; ?>
                      </select>
					  
                    </div>
                  </div>
				  <?php endif; ?>
				  
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Venue*</label>
                    <div class="col-sm-7">
                      <select name="venue_id" id="venue_id" class="form-control">
                        <option value="0">Select One</option>
							<?php for($i=0;$i<count($venues);$i++): if(isset($_POST['venue_id']) && $_POST['venue_id'] == $venues[$i]['id']) $sel =  'selected';elseif($match_info['venue_id'] == $venues[$i]['id']) $sel = 'selected';else $sel = '';  ?>
							<option <?php echo $sel; ?> value="<?php echo $venues[$i]['id']; ?>"><?php echo $venues[$i]['venue']; ?></option>
						<?php endfor; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">1st Umpire</label>
                    <div class="col-sm-7">
                      <select name="umpire1_id" id="umpire1_id" class="form-control display-phone">
                       <option value="0">N/A</option>
						<?php for($i=0;$i<count($umpires);$i++): if(isset($_POST['umpire1_id']) && $_POST['umpire1_id'] == $umpires[$i]['user_id']) $sel =  'selected';elseif($match_info['umpire1_id'] == $umpires[$i]['user_id']) $sel = 'selected';else $sel = ''; ?>
							<option <?php echo $sel; ?> value="<?php echo $umpires[$i]['user_id']; ?>"><?php echo $umpires[$i]['f_name']; ?> <?php echo $umpires[$i]['m_name']; ?> <?php echo $umpires[$i]['last_name']; ?></option>
						<?php endfor; ?>
                      </select>
					  
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">1st Umpire From</label>
                    <div class="col-sm-7">
                      <select name="umpire1_from" id="umpire1_from" class="form-control">
                       <option value="0">N/A</option>
								<option value="-1" <?php if(isset($_POST['umpire1_from']) && $_POST['umpire1_from'] == -1) echo 'selected';elseif($match_info['umpire1_from'] == -1) echo 'selected'; ?> >Independent</option>
								<?php for($i=0;$i<count($league_clubs);$i++): if(isset($_POST['umpire1_from']) && $_POST['umpire1_from'] == $league_clubs[$i]['id']) $sel =  'selected';elseif($match_info['umpire1_from'] == $league_clubs[$i]['id']) $sel = 'selected';else $sel = '';  ?>
									<option <?php echo $sel; ?> value="<?php echo $league_clubs[$i]['id']; ?>"><?php echo $league_clubs[$i]['company_name']; ?></option>
								<?php endfor; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">2nd Umpire</label>
                    <div class="col-sm-7">
                      <select name="umpire2_id" id="umpire2_id" class="form-control display-phone">
                        <option value="0">N/A</option>
							<?php for($i=0;$i<count($umpires);$i++): if(isset($_POST['umpire2_id']) && $_POST['umpire2_id'] == $umpires[$i]['user_id']) $sel =  'selected';elseif($match_info['umpire2_id'] == $umpires[$i]['user_id']) $sel = 'selected';else $sel = ''; ?>
								<option <?php echo $sel; ?> value="<?php echo $umpires[$i]['user_id']; ?>"><?php echo $umpires[$i]['f_name']; ?> <?php echo $umpires[$i]['m_name']; ?> <?php echo $umpires[$i]['last_name']; ?></option>
							<?php endfor; ?>
                      </select>
					  
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">2nd Umpire From</label>
                    <div class="col-sm-7">
                      <select name="umpire2_from" id="umpire2_from" class="form-control">
                        <option value="0">N/A</option>
						<option value="-1" <?php if(isset($_POST['umpire2_from']) && $_POST['umpire2_from'] == -1) echo 'selected';elseif($match_info['umpire2_from'] == -1) echo 'selected'; ?> >Independent</option>
						<?php for($i=0;$i<count($league_clubs);$i++): if(isset($_POST['umpire2_from']) && $_POST['umpire2_from'] == $league_clubs[$i]['id']) $sel =  'selected';elseif($match_info['umpire2_from'] == $league_clubs[$i]['id']) $sel = 'selected';else $sel = '';  ?>
							<option <?php echo $sel; ?> value="<?php echo $league_clubs[$i]['id']; ?>"><?php echo $league_clubs[$i]['company_name']; ?></option>
						<?php endfor; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Ist Scorer</label>
                    <div class="col-sm-7">
                      <select name="scorer_1" id="scorer_1" class="form-control display-phone">
                        <option value="0">N/A</option>
							<?php for($i=0;$i<count($umpires);$i++): if(isset($_POST['scorer_1']) && $_POST['scorer_1'] == $umpires[$i]['user_id']) $sel =  'selected';elseif($match_info['scorer_1'] == $umpires[$i]['user_id']) $sel = 'selected';else $sel = '';  ?>
								<option <?php echo $sel; ?> value="<?php echo $umpires[$i]['user_id']; ?>"><?php echo $umpires[$i]['f_name']; ?> <?php echo $umpires[$i]['m_name']; ?> <?php echo $umpires[$i]['last_name']; ?></option>
							<?php endfor; ?>
						</select>
						
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">2nd Scorer</label>
                    <div class="col-sm-7">
                      <select name="scorer_2" id="scorer_2" class="form-control display-phone">
                        <option value="0">N/A</option>
							<?php for($i=0;$i<count($umpires);$i++): if(isset($_POST['scorer_2']) && $_POST['scorer_2'] == $umpires[$i]['user_id']) $sel =  'selected';elseif($match_info['scorer_2'] == $umpires[$i]['user_id']) $sel = 'selected';else $sel = '';  ?>
								<option <?php echo $sel; ?> value="<?php echo $umpires[$i]['user_id']; ?>"><?php echo $umpires[$i]['f_name']; ?> <?php echo $umpires[$i]['m_name']; ?> <?php echo $umpires[$i]['last_name']; ?></option>
							<?php endfor; ?>
						</select>
						
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">3rd Umpire</label>
                    <div class="col-sm-7">
                      <select name="umpire3_id" id="umpire3_id" class="form-control display-phone">
                       <option value="0">N/A</option>
						<?php for($i=0;$i<count($umpires);$i++): if(isset($_POST['umpire2_id']) && $_POST['umpire3_id'] == $umpires[$i]['user_id']) $sel =  'selected';elseif($match_info['umpire3_id'] == $umpires[$i]['user_id']) $sel = 'selected';else $sel = ''; ?>
							<option <?php echo $sel; ?> value="<?php echo $umpires[$i]['user_id']; ?>"><?php echo $umpires[$i]['f_name']; ?> <?php echo $umpires[$i]['m_name']; ?> <?php echo $umpires[$i]['last_name']; ?></option>
						<?php endfor; ?>
						</select>
						
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">3rd Umpire From</label>
                    <div class="col-sm-7">
                      <select name="umpire3_from" id="umpire3_from" class="form-control">
							<option value="0">N/A</option>
							<option value="-1" <?php if(isset($_POST['umpire3_from']) && $_POST['umpire3_from'] == -1) echo 'selected';elseif($match_info['umpire3_from'] == -1) echo 'selected'; ?> >Independent</option>
							<?php for($i=0;$i<count($league_clubs);$i++): if(isset($_POST['umpire3_from']) && $_POST['umpire3_from'] == $league_clubs[$i]['id']) $sel =  'selected';elseif($match_info['umpire3_from'] == $league_clubs[$i]['id']) $sel = 'selected';else $sel = '';  ?>
								<option <?php echo $sel; ?> value="<?php echo $league_clubs[$i]['id']; ?>"><?php echo $league_clubs[$i]['company_name']; ?></option>
							<?php endfor; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Match Referee</label>
                    <div class="col-sm-7">
                      <select name="referee_id" id="referee_id" class="form-control">
                       <option value="0">N/A</option>
						<?php for($i=0;$i<count($umpires);$i++): if(isset($_POST['referee_id']) && $_POST['referee_id'] == $umpires[$i]['user_id']) $sel =  'selected';elseif($match_info['referee_id'] == $umpires[$i]['user_id']) $sel = 'selected';else $sel = '';  ?>
							<option <?php echo $sel; ?> value="<?php echo $umpires[$i]['user_id']; ?>"><?php echo $umpires[$i]['f_name']; ?> <?php echo $umpires[$i]['m_name']; ?> <?php echo $umpires[$i]['last_name']; ?></option>
						<?php endfor; ?>
						</select>
						
                    </div>
                  </div>
				  <div class="form-group">
                    <label class="col-sm-5 control-label">Last Updated</label>
                    <div class="col-sm-7">
                     <?php echo date('d F, Y H:i:s', strtotime($match_info['updated_on']));  ?>
                    </div>
                  </div>
				  <div class="form-group">
                    <label class="col-sm-5 control-label">Last Updated by</label>
                    <div class="col-sm-7">
                     <?php $last_updated_by = get_record_on_id('users', $match_info['last_updated_by']);?>
					 <?php echo $last_updated_by['f_name'].' '.$last_updated_by['m_name'].' '.$last_updated_by['last_name'] ;  ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-5 col-sm-7">
                      <input name="submit_btn" value=" Submit " class="btn orange hvr-float-shadow" type="submit">
                      <input name="cancel_btn" value=" Cancel " class="btn blue hvr-float-shadow" onclick="window.location.href='<?php echo WWW; ?>tournament/matches/list/<?php echo $tournament_id; ?>';" type="button">
                    </div>
                  </div>
                </form>
              </div>
            </div>
			<?php }else{ ?>
				<div id="error" class="alert alert-danger">You do not have Permission for this Tournament... !</div>
			<?php } ?>
          </div>
		
		
		</div>
	</div>
</div>		

<script src="<?php echo WWW; ?>assets/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo WWW; ?>assets/css/jquery.datetimepicker.css"/>
<script>
$('#start_time').datetimepicker();
</script>

<?php include('common/footer.php'); ?>