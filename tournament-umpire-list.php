<?php include_once('includes/configuration.php');
$page = 'tournament-umpire-list.html';
$selected_country = getGeoLocationCountry(); 

$umpires = array();
$error = '';
if(isset($_GET['page']) && !empty($_GET['page'])){
	$current_page = str_replace('page','',trim($_GET['page']));
}else{
	$current_page = 1;
}

$records_per_page = 10;
$start_record  = ($current_page-1)*$records_per_page;

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
}

$tournament_permission = 0;
$tournament_id = trim($_GET['t_id']);
$tournament_info = get_record_on_id('tournaments', $tournament_id);	

$page_title = ucwords($tournament_info['title']).' Umpires';

if(isset($user_id) && !empty($user_id)){
	//$sql = "select * from tournament_permissions where tournament_id = $tournament_id and member_id = $user_id";
	//$rs_permission = mysqli_query($conn,$sql);
	if($tournament_info['user_id'] == $user_id){
		$tournament_permission = 1;
	}else{
		$tournament_permission = 0;
	}
}

if(empty($error) && isset($_GET['u_id']) && $_GET['action'] == 'delete'){
	$umpire_id = trim($_GET['u_id']);
	if($tournament_permission){
		$sql = "DELETE FROM tournament_umpires WHERE id = $umpire_id ";
		mysqli_query($conn,$sql);
		$_SESSION['umpire_deleted'] = 1;
	}
	header("Location:".WWW."tournament/umpires/list/".$tournament_id);
	exit();
}

if(isset($_SESSION['umpire_deleted']) && $_SESSION['umpire_deleted']==1) {
	$umpire_deleted = 1;
	unset($_SESSION['umpire_deleted']);
}
if(isset($_SESSION['umpire_updated']) && $_SESSION['umpire_updated']==1) {
	$umpire_updated = 1;
	unset($_SESSION['umpire_updated']);
}
if(isset($_SESSION['umpire_added']) && $_SESSION['umpire_added']==1) {
	$umpire_added = 1;
	unset($_SESSION['umpire_added']);
}


$sql = "SELECT count(*) as record_count FROM tournament_umpires as u left join companies as c on u.club_id=c.id
 WHERE  u.tournament_id=$tournament_id  ";
$rs_total = mysqli_query($conn,$sql);
$row_total = mysqli_fetch_assoc($rs_total);
$records_count = $row_total['record_count'];

$sql = "SELECT u.*,c.company_name FROM tournament_umpires as u left join companies as c on u.club_id=c.id
 WHERE  u.tournament_id=$tournament_id ORDER BY u.id  LIMIT $start_record,$records_per_page";
$rs_umpires = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_umpires)){
	$umpires[] = $row;
}

?>
<?php include('common/header.php'); ?>
<style>
.list ul{width:100%;}
.list ul li{width:98%;}
dd{width:650px;}
.details{width:420px;}
.details a{left:330px;}
.content{width:820px;}
dl {min-height:145px;}
.content {width:1010px;}
.large-column {width:790px;}
#table-list {width:100%;border:1px solid #ccc;}
#table-list tr{border:1px solid #ccc;}
#table-list th{padding:5px;background-color:#000;color:#FB7008;font-size:14px;}
#table-list td{padding:5px;text-align:center;}
#table-list a{color:#000;}
.add_tournament{font-size:14px;color:#000;}
</style>
	<div class="middle">
		<h1>Tournament Umpires </h1>
		<h2><?php echo ucwords($tournament_info['title']); ?></h2>
		<div class="white-box content" id="dashboard">
			<?php if(isset($umpire_deleted) && $umpire_deleted == 1): ?>
				<div id="information">Umpire deleted Successfully... !</div>
			<?php endif; ?>
			<?php if(isset($umpire_updated) && $umpire_updated == 1): ?>
				<div id="information">Umpire updated Successfully... !</div>
			<?php endif; ?>
			<?php if(isset($umpire_added) && $umpire_added == 1): ?>
				<div id="information">Umpire added Successfully... !</div>
			<?php endif; ?>
		
			<?php if(empty($user_info)): ?>
				<div id="error">You are not logged... !</div>
			<?php endif; ?>
			
			<?php if(!empty($error)): ?>
				<?php echo $error; ?>
			<?php endif; ?>
		
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
			<?php if($tournament_permission){ ?>
			<p style="float:right;"><a href="<?php echo WWW; ?>tournament/umpires/add/<?php echo $tournament_id; ?>" class="add_tournament" title="Add New Umpire"><img alt="Add" src="<?php echo WWW; ?>images/icons/add.png" border="0"> Add New Umpire</a></p>
			 <div class="clear"></div>
				<table id="table-list" class="white-box">
				<tr><th>Name</th><th>Club</th><th>Action</th></tr>
				<?php for($i=0;$i<count($umpires);$i++): ?>
					<tr>
						<td><?php echo $umpires[$i]['umpire_name']; ?></td>
						<td><?php if(!empty($umpires[$i]['company_name'])) echo $umpires[$i]['company_name'];else echo 'Independent';  ?></td>
						<td>
							<a href="<?php echo WWW; ?>tournament/umpires/edit/<?php echo $umpires[$i]['id']; ?>" title="Edit" ><img alt="Edit" src="<?php echo WWW; ?>images/icons/edit.png" border="0"></a>  &nbsp;&nbsp;
							<a href="<?php echo WWW; ?>tournament-umpire-list.php?u_id=<?php echo $umpires[$i]['id']; ?>&t_id=<?php echo $umpires[$i]['tournament_id']; ?>&action=delete" onclick="return confirm('Are you sure to delete Umpire.');" title="Delete"><img alt="Delete" src="<?php echo WWW; ?>images/icons/delete.png" border="0"></a>
						</td>
					</tr>
				<?php endfor; ?>
				<?php if(empty($umpires)): ?>
				<tr><td colspan="4">No Records</td></tr>
				<?php endif; ?>
				</table>
				<?php }else{ ?>
				<div id="error">You do not have Permission for this Tournament... !</div>
				<?php } ?>
				
				<div id="pagination-bottom">
				<?php  $paging_str = getPaging('tournament/umpires/list/'.$tournament_id,$records_count,$current_page,$records_per_page);
				echo $paging_str;
				?>
		        <div class="clear"></div>
				</div>  
				
			</div>  
		    <div class="clear"></div>
			</div>
		</div>
		<!--<div class="rightbar"><?php //include('common/right-panel.php');?></div>-->
		<div class="clear"></div>
	</div>
	


<?php include('common/footer.php'); ?>