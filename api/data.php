<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
include_once("api.php");
$api	=	new Api();
//$api->domainName	=	"http://localhost/youcricket/web/";
$api->domainName	=	"http://dev1.youcricketer.com/";
//$api->getHomeUsers(5);
/*
if(sizeof($_POST)>0)
{
	$postrequest	=	$api->postParam('request');
	if($postrequest=='newActivity'){		
		//echo json_encode($_POST);
		//$activity =	$api->postParam('activity');
		//$userid =	$api->postParam('id');
		$api->AddnewActivity($_POST);	
	}	
}
*/
if(sizeof($_GET)>0){
	$request =	$api->getParam('request');
	
	//get users list to display at home page
	if($request=='homeUsers'){
			$offset =	$api->getParam('offset');
			$api->getHomeUsers($offset);
	}
	if($request=='homeSearch'){
			$search =	$api->getParam('search');
			$api->gethomeSearch($search);
	}
		
	//Load companies list to display at home page
	if($request=='homeCompanies'){
			$offset =	$api->getParam('offset');
			$api->getHomeCompanies($offset);
	}
	if($request=='registerUser'){
			
			$api->registerUser($_POST);
			
	}
	if($request=='userLogin'){
			
			$api->userLogin($_POST);
			
	}
	if($request=='checkLogin'){
			
			$api->checkLogin();
			
	}
	if($request=='Activities'){
		
			$offset =	$api->getParam('offset');
			$api->getActivities($offset);	
	}

	if($request=='newComment'){
		
		//echo json_encode($_POST);
		$comment		=	$api->postParam('comment');
		$activityid		=	$api->postParam('activityid');
		$userid			=	$api->postParam('id');
		$api->newComment($comment,$activityid,$userid);	
	}
	if($request=='myFriends'){
		$userid			=	$api->postParam('id');
		$api->getmyFriends($userid);	
	}
	if($request=='Penddingrequest'){
		$userid			=	$api->postParam('id');
		$api->getPenddingrequest($userid);	
	}
	if($request=='Approvedrequest'){
		$userid			=	$api->postParam('id');
		$rfid			=	$api->postParam('rfid');
		$api->updateApprovedrequest($userid,$rfid);	
	}
	if($request=='postInvite'){
		$email			=	$api->postParam('friendemail');
		$userid			=	$api->postParam('id');
		$api->postinvite($email,$userid);	
	}
	if($request=='forgottenPassword'){
		$email			=	$api->postParam('email');
		$api->forgottenpassword($email);	
	}
	if($request=='composeMessage'){
		$to			=	$api->postParam('to');
		$subject	=	$api->postParam('subject');
		$message	=	$api->postParam('message');
		$from_user_id	=	$api->postParam('id');
		$api->sendmessage($to,$subject,$message,$from_user_id);	
	}
	if($request=='myNews'){
		$user_id	=	$api->postParam('id');
		$api->getmyNews($user_id);	
	}
	if($request=='myEvents'){
		$user_id	=	$api->postParam('id');
		$api->getmyEnvents($user_id);	
	}
		if($request=='myTestimonials'){
		$user_id	=	$api->postParam('id');
		$api->getmyTestm($user_id);	
	}
	if($request=='inboxMessage'){
		$user_id	=	$api->postParam('id');
		$api->getInbox($user_id);	
	}
		if($request=='OutboxMessage'){
		$user_id	=	$api->postParam('id');
		$api->getOutbox($user_id);	
	}
		if($request=='myProfile'){
		$user_id	=	$api->postParam('id');
		$api->getProfile($user_id);	
	}
	if($request=='deleteActivity'){
		$user_id	=	$api->postParam('id');
		$activityid	=	$api->postParam('activityid');
		$api->deleteActivity($activityid,$user_id);	
	}
	if($request=='signupYear'){
		$api->getsignupYear();	
	}
	if($request=='shareit'){
		$user_id	=	$api->postParam('id');
		$activityid	=	$api->postParam('activityid');
		$api->shareit($activityid,$user_id);	
	}
	if($request=='loveit'){
		$userId	=	$api->postParam('id');
		$activityId	=	$api->postParam('activityid');
		$api->loveit($activityId,$userId);	
	}
}
?>