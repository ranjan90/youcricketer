<?php include('common/header.php'); ?>
<? if(isset($_GET['type']) && $_GET['type'] == 'my'){ ?>
	<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
	<script>
		window.location = '<?=WWW?>logout.php';
	</script>
	<? } ?>
<? } ?>

<div class="container-fluid">
    <div class="row">
		<div class="col-md-12">
			<h1> ESPN latest News </h1>
        </div>
    </div>
	<div class="row">
        <div class="col-md-10">
          <div class="white-box">
		  
	    <div class="row">
              <div class="col-sm-12">
                <div class="list">
                  <ul>
				    <?php include('espn/espn_parse_news.php'); ?>
					<?php 
						$news = $xpath->query("//h2/a[contains(@href,'content/story/')]");
						foreach($news as $l){ ?>  
                    <li>
                      <div class="row">
                      	<div class="col-sm-1 col-md-1">
                          <a href="<?php echo WWW;?>redirection.php?type=news&url=http://www.espncricinfo.com/<?php echo $l->getAttribute('href');?>" target="_blank" title="Read more"> <img src="<?=WWW?>images/espn.jpg" alt="<?php echo $l->textContent;?>" title="<?php echo $l->textContent;?>" /> </a>
                        </div>
                        <div class="col-sm-6 col-md-8 sep-right">
                          <a href="<?php echo WWW;?>redirection.php?type=news&url=http://www.espncricinfo.com/<?php echo $l->getAttribute('href');?>" target="_blank" title="Read more"><h3><?php echo $l->textContent;?></h3></a>
						  
                          <a href="<?php echo WWW;?>redirection.php?type=news&url=http://www.espncricinfo.com/<?php echo $l->getAttribute('href');?>"  title="Read more" class="btn orange hvr-float-shadow pull-right"> Read More </a>
                        </div>
                        <div class="col-sm-3 col-md-2 text-center sep-top">
							<!-- AddThis Button BEGIN -->
									<div class="addthis_toolbox addthis_default_style ">
									<a class="addthis_button_preferred_1"></a>
									<a class="addthis_button_preferred_2"></a>
									<a class="addthis_button_preferred_3"></a>
									<a class="addthis_button_preferred_4"></a>
									<a class="addthis_button_compact"></a>
									<a class="addthis_counter addthis_bubble_style"></a>
									</div>
									<script type="text/javascript">var addthis_config = {
									"data_track_addressbar":true};</script>
									<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5073efa669740139"></script>
								<!-- AddThis Button END -->
						</div>
                      </div>
                    </li>
                    <?php
					    
					}?>
                   
                  </ul>
                </div>
              </div>
        </div>
		</div>
        </div>
        <div class="col-md-2">
         
          <div class="row">
            <div class="col-sm-12">
				<?php include('common/right-panel.php');?>
            </div>
          </div>
        </div>
      </div>
      
</div><!-- /.container -->
<?php include('common/footer.php'); ?>