<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	
		<? 	if(isset($_GET['action']) && $_GET['action'] == 'deletephoto'){
				$pid 	= $_GET['pid'];
				$row_img= get_record_on_id('photos',$pid);
				mysqli_query($conn,"delete from photos where id = '$pid'");
				unlink('groups/'.$_GET['id'].'/'.$row_img['file_name']);
				?>
				<script>
				window.location = '<?=WWW?>edit-group-<?=$_GET['id']?>.html';
				</script>
				<?
			}
			if(isset($_POST) && !empty($_POST)){ 
				$title 	= $_POST['title'];
				$content= $_POST['content'];
				$date 		= date('Y-m-d');
				$user_id 	= $_SESSION['ycdc_dbuid'];
				$isGlobal = ($_POST['is_global'] == 'on')?'1':'0';
				$id 		= $_GET['id'];
				$photo_id 	= $_POST['photo_id'];

				if(!isset($user_id) || empty($user_id)){
					echo '<div id="error" class="alert alert-danger">Please login first ... !</div>';
				}else{
					if(!empty($title) && !empty($content)){
						$query = "update groups set is_global = '$isGlobal', title = '$title', description = '$content' where id = '$id'";
						if(mysqli_query($conn,$query)){
						
							if(!is_dir('groups/'.$id)){
								mkdir('groups/'.$id,0777);
							}
							chmod('groups/'.$id,0777);
							if(!empty($_FILES['photo']['name'])){
								$filename 	= friendlyURL($title).'.jpg';
								$image 		= new SimpleImage();
								$image->load($_FILES["photo"]["tmp_name"]);
								$image->save('groups/'.$id.'/'.$filename);
								chmod('groups/'.$id.'/'.$filename,0777);
							
								if(!empty($photo_id)){
									mysqli_query($conn,"update photos set file_name = '$filename' where id = '$photo_id'");	
								}else{
									mysqli_query($conn,"insert into photos (file_name, entity_type, entity_id) values ('$filename','groups','$id');");
								}
							}
						}
		                echo '<div id="success" class="alert alert-success">Group Information updated successfully ... !</div>';
		                ?>
		                <script>
		                window.location = '<?=WWW?>my-groups.html';
		                </script>
		                <?
		            }else{
						echo '<div id="error" class="alert alert-danger">Please fill all fields ... !</div>';
					}
				}
				
			}
			$id 	= $_GET['id'];
			$row 	= get_record_on_id('groups', $id);
			$row_img= mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'groups' and entity_id = '$id' order by id desc limit 1"));
			
		?>
		
		<?php function q1(){ ?>
		<div class="white-box content">
			<form method="post" enctype="multipart/form-data">
				<fieldset>
					<h2>News Information</h2>
					<div class="form-box">
						<label>Group Admin</label>
						<select class="validate[required]" name="admin_id">
						<? 	$rs_members = mysqli_query($conn,"select * from users_to_groups where group_id = '$id'");
							while($row_m= mysqli_fetch_assoc($rs_members)){ ?>
							<option value="<?=$row_m['user_from_id']?>" <?=($row_m['user_from_id'] == $row['admin_id'])?'selected="selected"':'';?>><?=get_combo('users','concat(f_name," ", last_name)',$row_m['user_from_id'],'admin_id','text');?></option>
						<? 	} ?>
						</select>
					</div>
					
					<div class="clear"></div>
					<div class="form-box">
						<label>Title</label>
						<input type="text" name="title" value="<?=$row['title']?>" class="input-login validate[required]">
					</div>
					<div class="clear"></div>
					<div class="form-box" style="width:100%;">
						<label>Is Global</label>
						<input style="float:left; margin-left:70px;" type="checkbox" name="is_global" <?php echo ($row['is_global'] == '1')?'checked="checked"':'';?>>
						<label>Group will be Regional Group If checked</label>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Content</label>
						<textarea maxlength="500" name="content" class="validate[required]"><?=$row['description']?></textarea>
					</div>
					<div class="clear"></div>
					<h2>News Photo</h2>
					<div class="form-box">
						<label>News Photo</label>
						<input type="file" name="photo">
						<br>
						<? 	if($row_img){ ?>
						<a href="<?=WWW?>edit-group-<?=$_GET['id']?>-deletephoto-<?=$row_img['id']?>.html"><img style="position:relative; top:20px; right:5px;" src="<?=WWW?>images/icons/delete.png"></a>
						<img src="<?=WWW?>groups/<?=$id?>/<?=$row_img['file_name']?>" width="100">
						<input type="hidden" name="photo_id" value="<?=$row_img['id']?>">
						<?	} ?>
					</div>
					<div class="form-box">
						<label>Max Photo size : 2MB</label>
					</div>
					<div class="clear"></div>
					
					<div class="form-box">
						<input type="submit" value=" Save " class="submit-login">
					</div>
					<div class="form-box">
						<a id="cancel" class="submit-login" href="<?=WWW?>groups.html">Cancel</a>
					</div>
				</fieldset>
			</form>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>
<style>
#cancel{background: url('<?php echo WWW;?>images/sign-in-button.png') no-repeat !important; color:#fff !important; padding-left:3px; top:0px !important;position:relative; left:-150px;}
</style>

		<?php } ?>






<script src="<?=WWW;?>assets/js/ckeditor/ckeditor.js"></script>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1> Update Group </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <form method="post" action="" enctype="multipart/form-data" class="form-horizontal" id="form-add-forum">
            
            <div class="white-box">
              <div class="form-group">
                <div class="col-sm-12 text-center">
                  <h2>Group Information</h2>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Title</label>
                <div class="col-sm-9">
                  <input name="title" value="<?=$row['title']?>" class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Is Global</label>
                <div class="col-sm-9">
                  <input type="checkbox" name="is_global" <?php echo ($row['is_global'] == '1')?'checked="checked"':'';?>>
                  <span class="help-block">Group will be Regional Group If checked</span>
                </div>
              </div>
              
              
              <div class="form-group">
                <label class="col-sm-3 control-label">Content</label>
                <div class="col-sm-9">
                  <div id="editor">
					<textarea name="content" id="content"  class="ckeditor" class="validate[required]"><?=$row['description']?></textarea>
				  </div>
                </div>
              </div>
              
              <div class="form-group">
                <label class="col-sm-3 control-label">Group Photo</label>
                <div class="col-sm-9">
                  <? 	if($row_img){ ?>
						
						<img src="<?=WWW?>groups/<?=$id?>/<?=$row_img['file_name']?>" width="100">&nbsp;
						<a href="<?=WWW?>edit-group-<?=$_GET['id']?>-deletephoto-<?=$row_img['id']?>.html" onClick="return confirm('Are you sure to Photo');"><img  src="<?=WWW?>images/icons/delete.png"></a>
						<input type="hidden" name="photo_id" value="<?=$row_img['id']?>"><br/>
					<?	} ?>
					<input accept="image/*" name="photo" type="file">
                  <span class="help-block">Max Photo size : 2MB</span>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12 text-center">
                  <input value=" Save " class="btn orange hvr-float-shadow" type="submit">
                  <a id="cancel" class="btn blue hvr-float-shadow" href="<?=WWW?>groups.html">Cancel</a>
                </div>
              </div>
            </div>
		  </form>
        </div>
        
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
              <?php include('common/right-panel.php');?>
            </div>
          </div>
          
        </div>
      </div>
    </div><!-- /.container -->
	
<script type="text/javascript">
$(document).ready(function(){
	$('#form-add-forum').validationEngine();
});
</script>	

<?php include('common/footer.php'); ?>