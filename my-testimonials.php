<?php include('common/header.php'); ?>

<?php  $rpp = PRODUCT_LIMIT_FRONT; // results per page
            			$ppage = intval($_GET["page"]);
      					if($ppage<=0) $ppage = 1;
      					$query = "select * from testimonials where status=1 ";
				      	//=======================================
					    if(isset($_GET['keywords']) && $_GET['keywords'] != 'Search Here'){
					        $query  .= "and content like '%".str_replace('-','%',$_GET['keywords'])."%'";
					    }
					    if(isset($_GET['type']) && $_GET['type'] == 'my'){
					    	$query  .= "and user_id = '".$_SESSION['ycdc_dbuid']."'";	
					    }
					    $query .= " order by id desc ";
				      //echo $query;
				      //=======================================
				      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
				        echo '<div id="information" class="alert alert-danger">No record ...!</div>';
				      }
				      $rs   = mysqli_query($conn,$query);
				      $tcount = mysqli_num_rows($rs);
				      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
				      $count = 0;
				      $i = ($ppage-1)* $rpp;
          			  $x = 0;
          			  //======================================= ?>


<div class="page-container"> 
		<?php  include('common/user-left-panel.php');?>
      <!-- END SIDEBAR -->
      
      <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
		<div class="page-content">
		
		
		
		          <div class="white-box">
            <div class="row">
              <div class="col-md-12">
                <h2> YouCricketer Testimonials </h2>
              </div>
            </div>
           
            <div id="pagination-top">
              <div class="row">
                  <div class="col-sm-6">
                <? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_FRONT){ ?>
		      	<?php
		        	$reload = 'testimonials.html?';
		        	echo paginate_one($reload, $ppage, $tpages);
		      	?>
		      	<input type="hidden" name="pagination-page" value="testimonials.html">
		        <? } ?>    
                </div>
                <div class="col-sm-3" id="search-div1">
                  <form id="list-search" method="post" action="">
                  <div class="input-group">
                    <input class="form-control validate[required] input-login" name="txtsearch" placeholder="Search Here" type="text">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit">Go!</button>
                    </span>
                  </div>
				  </form>
                </div>
                <div class="col-sm-3">
                  <a href="<?=WWW;?>add-testimonial.html" class="btn orange full hvr-float-shadow">Create Testimonial</a>
                </div>
              </div>
            </div>
            
            <div class="list testimonials">
              <ul>
			  <?php while(($count<$rpp) && ($i<$tcount)){
                		mysqli_data_seek($rs,$i);
                		$row_t 	= mysqli_fetch_array($rs);
                		$row_i  = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_t['user_id']."' and is_default = '1'"));
                		?>
                <li>
                  <div class="row">
                    <div class="col-sm-9 sep-right">
                      <h2>
                         <?php if($row_i['file_name'] && file_exists('users/'.$row_t['user_id'].'/photos/'.$row_i['file_name'])){?>
								<img style="margin:4px" src="<?=WWW?><?='users/'.$row_t['user_id'].'/photos/'.$row_i['file_name']?>" alt="<?=get_combo('users','concat(f_name," ",last_name)',$row_t['entity_id'],'','text')?>" width="40">
							<?php } else { ?>
								<img  src="<?=WWW?>images/no-photo.png" width="70" height="65">
							<?php } ?> 
							<?=get_combo('users','concat(f_name," ",last_name)',$row_t['user_id'],'','text')?> for
							<?php $row_i  = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_t['entity_id']."' and is_default = '1'")); ?>
							 <?php if($row_i['file_name'] && file_exists('users/'.$row_t['entity_id'].'/photos/'.$row_i['file_name'])){?>
							<img style="margin:4px" src="<?=WWW?><?='users/'.$row_t['entity_id'].'/photos/'.$row_i['file_name']?>" alt="<?=get_combo('users','concat(f_name," ",last_name)',$row_t['entity_id'],'','text')?>" width="40">
							<?php } else { ?>
								<img  src="<?=WWW?>images/no-photo.png" width="70" height="65">
							<?php } ?>
							<?=get_combo('users','concat(f_name," ",last_name)',$row_t['entity_id'],'','text')?> on <?=date_converter($row_t['testimonial_date'],'M d,Y H:i')?>
                      </h2>
                      <p><?=$row_t['content']?></p>
                    </div>
                    <div class="col-sm-3 text-center sep-top">
                      <input name="input-3" value="<?php echo $row_t['rating']; ?>" class="rating-loading input-3"><br><br>
					  <? 	/*$rating = $row_t['rating'];
								for($x = 0; $x < 5; $x++){
									if($x < $rating){
									?><img src="<?=WWW?>images/star-fill-gray.jpg" title="<?=$rating?>/5"><?
									}else{
									?><img src="<?=WWW?>images/star-gray.jpg" title="<?=$rating?>/5"><?
									}
								}*/
							?>
                      
                      <a href="<?=WWW?>edit-testimonial-<?=$row_t['id']?>.html" class="text-warning"><i class="fa fa-edit"></i></a>
                      <a href="<?=WWW?>delete-testimonial-<?=$row_t['id']?>.html" onclick="return confirm('Are you sure to delete Record');" class="text-danger"><i class="fa fa-times"></i></a>
                    </div>
                  </div>
                </li>
                <?
					      $i++;
					      $count++;
					      $x++;
					  } 
					  ?>
                
              </ul>
            </div>
            
            <div id="pagination-bottom">
              <div class="row">
                 <div class="col-sm-6">
                <? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_FRONT){ ?>
		      	<?php
		        	$reload = 'testimonials.html?';
		        	echo paginate_one($reload, $ppage, $tpages);
		      	?>
		      	<input type="hidden" name="pagination-page" value="testimonials.html">
		        <? } ?>    
                </div>
                <div class="col-sm-3" id="search-div2">
                  <form id="list-search" method="post" action="">
                  <div class="input-group">
                    <input class="form-control validate[required] input-login" name="txtsearch" placeholder="Search Here" type="text">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit">Go!</button>
                    </span>
                  </div>
				  </form>
                </div>
                <div class="col-sm-3">
                  <a href="<?=WWW;?>add-testimonial.html" class="btn orange full hvr-float-shadow">Create Testimonial</a>
                </div>
              </div>
            </div>
          </div>
		
		
		</div>
	</div>
</div>	

<script src="<?php echo WWW; ?>assets/js/star-rating.js"></script>
 <script type="text/javascript">
		        
				$('form#list-search').submit(function(e){
					var parentId = $(this).parent().attr('id');
					var string = $('#'+parentId +' form input[name=txtsearch]').val();
					if(string != '' && string != 'Search Here'){
						string = string.replace(/[ ]+/g,'_');
						string = string.replace(/[^a-zA-Z0-9_]+/g,'').toLowerCase();
						if(string.length > 0){
							$('form#list-search').attr('action','<?=WWW;?>testimonials-' + string + '.html');
						}
					}
				});
				
				$('.input-3').rating({displayOnly: true, step: 0.5});
</script>
<?php include('common/footer.php'); ?>