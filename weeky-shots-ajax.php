<?php include_once('includes/configuration.php');?>
<div class="clear"><br/></div>
<h3>Recent Results</h3>
<div class="clear"></div>
<?php 
$tour_id = trim($_POST['tour_id']);
$sql = "SELECT tm.*,c1.company_name AS team1, c2.company_name AS team2,c3.company_name AS winning_team_name FROM tournament_matches AS tm 
INNER JOIN companies AS c1 ON tm.team1 = c1.id INNER JOIN companies AS c2 ON  tm.team2 = c2.id LEFT JOIN companies AS c3 ON  tm.winning_team_id = c3.id 
WHERE tm.tournament_id = ".$tour_id." AND DATEDIFF(NOW(),start_time)>0 AND DATEDIFF(NOW(),start_time)<=8 ORDER BY start_time DESC";
//echo $sql;
$rs_matches = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_matches)) { ?>
<div class="row"><div class="col-sm-12">
<h5><a href="<?php echo WWW; ?>tournament/matches/scoresheet/<?php echo $row['id'];?>" class="match-link"><?php echo $row['team1'];?> vs <?php echo $row['team2'];?></a> </h5>
<div class="clear"></div>
<p class="match-details"><?php echo $row['maximum_overs'];?> Overs Match - <?php echo date('d M, Y H:i',strtotime($row['start_time']));?>  </p>
<p class="match-details"><b>Match Result:</b> 
<?php if(!empty($row['completely_abandon_due_to']) && $row['completely_abandon_due_to'] != 'N/A'): ?>
	Match Abandoned due to  <?php echo $row['completely_abandon_due_to'];?>
<?php endif; ?>
			
<?php if(!empty($row['winning_team_id'])){ ?>
<?php if($row['winning_team_id']>0){
	echo $row['winning_team_name'].' Wins ';
	if(!empty($row['completely_abandon_due_to']) && $row['completely_abandon_due_to']=='Forfeit by One Team') echo ' by Forfeit';
	if(!empty($row['completely_abandon_due_to']) && $row['completely_abandon_due_to']=='Late Arrival of One Team') echo ' by Late Arrival';	
					
}else{
	if($row['winning_team_id'] == -1) echo 'Match Tied';
	if($row['winning_team_id'] == -2) echo 'Match Abandoned';
}
			
} ?>
</p>
</div></div>
<?php } ?>
<?php if(mysqli_num_rows($rs_matches) == 0): ?>
	<b>No Records</b>
<?php else: ?>	
	<p><a href="javascript:;" onclick="$('#weekly_shots-tab').removeClass('active');$('#weekly_shots').hide();$('#schedule_scorebook').show();$('#schedule_scorebook-tab').addClass('active');displaySchedule(<?php echo $tour_id; ?>);" style="color:#fb8300;font-size:15px;font-weight:700;margin-left:5px;">View All &raquo;</a></p>
<?php endif; ?>

<div class="clear"><br/></div>

<h3>Upcoming Fixtures</h3>
<div class="clear"><br/></div>
 <div class="row"><div class="col-sm-12"><div class="table-responsive">
<table class="table table-bordered table-condensed table-hover table-striped" id="table-list" >
<thead><tr><th>Date/Time</th><th>Day</th><th>Host Team</th><th>Guest Team</th><th>Venue</th><th>1st Umpire</th><th>Phone No</th></tr></thead>
<tbody>
<?php 
$tour_id = trim($_POST['tour_id']);
$sql = "SELECT tm.*,c1.company_name AS team1, c2.company_name AS team2,v.venue,u1.f_name,u1.last_name,l.umpire_phone FROM tournament_matches AS tm 
INNER JOIN companies AS c1 ON tm.team1 = c1.id INNER JOIN companies AS c2 ON  tm.team2 = c2.id  INNER JOIN league_venues as v ON  v.id = tm.venue_id 
left join users as u1 on tm.umpire1_id = u1.id left join league_umpires as l on tm.umpire1_id = l.umpire_id 
WHERE tm.tournament_id = ".$tour_id." AND DATEDIFF(start_time,NOW())>=0 AND DATEDIFF(start_time,NOW())<=8 ORDER BY start_time ASC";
$rs_matches = mysqli_query($conn,$sql);//echo $sql;
while($row = mysqli_fetch_assoc($rs_matches)) { ?>
	<tr>
		<td><?php echo date('d M, Y H:i',strtotime($row['start_time']));?></td>
		<td><?php echo $row['start_day'];?></td>
		<td><?php echo $row['team1'];?></td>
		<td><?php echo $row['team2'];?></td>
		<td><?php echo $row['venue'];?></td>
		<td><?php echo $row['f_name'];?> <?php echo $row['last_name'];?></td>
		<td><?php echo $row['umpire_phone'];?></td>
	</tr>
<?php } ?>

<?php if(mysqli_num_rows($rs_matches) == 0): ?>
	<tr><td colspan="6"><b>No Records</b></td></tr>
	<?php else: ?>	
	<tr><td colspan="6"></td><td><a href="javascript:;" onclick="$('#weekly_shots-tab').removeClass('active');$('#weekly_shots').hide();$('#schedule_scorebook').show();$('#schedule_scorebook-tab').addClass('active');displaySchedule(<?php echo $tour_id; ?>);" style="color:#fb8300;font-size:15px;font-weight:700;margin-left:5px;">View All &raquo;</a></td></tr>
<?php endif; ?>
</tbody>
</table>
</div></div></div>