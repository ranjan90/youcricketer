<?php include('common/header.php'); ?>

<div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1> FAQs </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <div class="white-box">
            <div id="pagination-top">
              <div class="row">
                <div class="col-sm-4 col-sm-offset-8">
                  <form id="list-search" method="post" action="">
                    <div class="input-group">
                      <input type="text" class="form-control validate[required] input-login" name="keyword" placeholder="Search Here">
                      <span class="input-group-btn">
                        <button class="btn btn-default" type="submit">Go!</button>
                      </span>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            
			<?php if(isset($_POST['keyword']) && !empty($_POST['keyword'])){
				$keywords = $_POST['keyword'];
				$sql = "select * from faqs where question like'%$keywords%' or answer like '%$keywords%'  and status = '1'";
				$rs_ques = mysqli_query($conn,$sql);
				$rows   = mysqli_num_rows($rs_ques); ?>
				
			<div class="row">
              <div class="col-sm-12">
              	<h2> Search Result  for '<?php echo $keyword; ?>' (<?php echo $rows.' Results'; ?>) - <a href="<?=WWW?>faqs.html" rel="faqs" class="" >Close Search Results </a> </h2>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
              	<div id="accordion" class="panel-group">
				
					<?php $counter = 1;
					while($row_ques = mysqli_fetch_assoc($rs_ques)): ?>
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title"> <a href="#<?php echo $counter; ?>" data-parent="#accordion" data-toggle="collapse" class="collapsed"> <span class="sign"></span> <?php echo $row_ques['question']; ?> </a> </h4>
						</div>
						<div class="panel-collapse collapse" id="<?php echo $counter; ?>">
						  <div class="panel-body">
							<p><?php echo $row_ques['answer']; ?></p>
						  </div>
						</div>
					</div>
					<?php $counter++;
					endwhile; ?>
					
                </div>
              </div>
            </div>
			
			<?php } else { ?>	
			
            <div class="row">
              <div class="col-sm-12">
              	<h2> General Questions </h2>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
              	<div id="accordion" class="panel-group">
				
					<?php $sql = "select * from faqs where category_id = '1' and status = '1'";
					$counter = 1;
					$rs_ques = mysqli_query($conn,$sql);
					while($row_ques = mysqli_fetch_assoc($rs_ques)): ?>
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title"> <a href="#<?php echo $counter; ?>" data-parent="#accordion" data-toggle="collapse" class="collapsed"> <span class="sign"></span> <?php echo $row_ques['question']; ?> </a> </h4>
						</div>
						<div class="panel-collapse collapse" id="<?php echo $counter; ?>">
						  <div class="panel-body">
							<p><?php echo $row_ques['answer']; ?></p>
						  </div>
						</div>
					</div>
					<?php $counter++;
					endwhile; ?>
					
                </div>
              </div>
            </div>
            
            <div class="row">
              <div class="col-sm-12">
              	<h2> Account Questions </h2>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
              	<div id="accordion2" class="panel-group ">
				
                  <?php $sql = "select * from faqs where category_id = '2' and status = '1'";
					$rs_ques = mysqli_query($conn,$sql);
					while($row_ques = mysqli_fetch_assoc($rs_ques)): ?>
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title"> <a href="#<?php echo $counter; ?>" data-parent="#accordion" data-toggle="collapse" class="collapsed"> <span class="sign"></span> <?php echo $row_ques['question']; ?> </a> </h4>
						</div>
						<div class="panel-collapse collapse" id="<?php echo $counter; ?>">
						  <div class="panel-body">
							<p><?php echo $row_ques['answer']; ?></p>
						  </div>
						</div>
					</div>
					<?php $counter++;
					endwhile; ?>
					
                </div>
              </div>
            </div>
			
			<?php } ?>
			
		  </div>
        </div>
        
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
				<?php include('common/right-panel.php');?>
            </div>
          </div>
          
        </div>
      </div>
</div><!-- /.container -->
<?php include('common/footer.php'); ?>