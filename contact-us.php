<?php include('common/header.php'); ?>
	<? 	
		$user_id =  $_SESSION['ycdc_dbuid'];
		$row = mysqli_fetch_assoc(mysqli_query($conn,"SELECT email FROM users WHERE id = '$user_id'"));
		
		if(isset($_POST['action'])){
			
				$name 		= $_SESSION['ycdc_user_name'];
				$subject 	= $_POST['subject'];
				
				$email 		= $row['email'];
				$content	= $_POST['content'];
				$department = $_POST['department_id'];
           // Getting email address from admin emails table
			$get_email         = mysqli_query($conn,"SELECT email FROM contact_emails WHERE id = '$department'");
			$department_email  = mysqli_fetch_assoc($get_email);

			$message = $content."<br/><br/> FROM : ".$name."<br/>  Email ".$email;
			
			
			$headers = "Content-type:text/html;charset=UTF-8" . "\r\n";
			$headers .= "From: $email" . "\r\n";

		
		
			
             
				$rs_req	= mysqli_query($conn,"select * from contact_requests where email = '$email' and subject = '$subject' and content = '$content' and department_id = '$department_id'");
				
				if(mysqli_num_rows($rs_req) == 0){

						mail($department_email['email'],$subject,$message,$headers);
            
					$query = "insert into contact_requests (subject , name, email, content, status, department_id) values ('$subject','$name','$email','$content','0','$department_id');";
					mysqli_query($conn,$query);
					echo '<div class="alert alert-success">Contact Request successfully submitted</div>';					
				}else{
					echo '<div class="alert alert-danger">Contact Request already submitted</div>';
				}
			}
		?>

	<div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1> Contact Us  </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <div class="white-box">
		  
		  <? if(isset($_SESSION['ycdc_user_name']) && !empty($_SESSION['ycdc_user_name'])){ ?>
            <div class="alert alert-info"> Fill all fields </div>
            <form method="post" action="" class="form-horizontal" id="form-contact">
              <input name="action" value="contactsubmit" type="hidden">
              <div class="form-group">
                <label class="col-sm-4 control-label">Department</label>
                <div class="col-sm-8">
					<?php echo get_combo('contact_emails','concat(title)','','department_id')?>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Subject</label>
                <div class="col-sm-8">
                  <input name="subject" class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Content</label>
                <div class="col-sm-8">
                  <textarea name="content" class="form-control validate[required]" rows="5" cols="80"></textarea>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                  <button class="btn orange hvr-float-shadow" type="submit"> <i class="fa fa-chevron-right"></i> Submit </button>
                </div>
              </div>
            </form>
		  <?php }else{ ?>
			<p>You need to be a registered member to contact and/or view complete profile of the specific Individual or Company. Already a member click Login below or join us by clicking either of two Registration links depending upon the type for free and easy setup. </p>
			<? include('common/login-box.php');?>
		  <?php } ?>
		  </div>
        </div>
        
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
				<?php include('common/right-panel.php');?>
            </div>
          </div>
          
        </div>
      </div>
    </div><!-- /.container -->
	
<script type="text/javascript">
$(document).ready(function(){
	$('#form-contact').validationEngine();
});
</script>	
	
	
<?php include('common/footer.php'); ?>