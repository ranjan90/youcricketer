<?php include_once('includes/configuration.php');
$page = 'league-umpires.html';
$selected_country = getGeoLocationCountry();

$ppage = intval($_GET["page"]);
if($ppage<=0) $ppage = 1;

$league_info = array();
$user_info  = array();
$club_members = array();

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);
	if($user_info['is_company'] == 1){
		$sql = "select * from companies where user_id = $user_id and company_type_id IN(1,2,3,35) and status=1";
		$rs_company = mysqli_query($conn,$sql);
		if(mysqli_num_rows($rs_company)){
			$league_info = mysqli_fetch_assoc($rs_company);
		}
	}
}

if(isset($_POST['members_del_submit']) && !empty($_POST['members_del_submit'])){
	$members = $_POST['member_ids'];
	if(!empty($members)){
		for($i=0;$i<count($members);$i++){
			$sql = "DELETE FROM league_umpires WHERE league_id=".$league_info['id']." and umpire_id=".$members[$i] .' LIMIT 1';
			mysqli_query($conn,$sql);
		}
	}

	$_SESSION['league_umpire_deleted'] = 1;
	header("Location:".WWW.'league-umpires-all-'.$ppage.'.html');
	exit;
}

if(isset($_POST['umpire_update_submit']) && !empty($_POST['umpire_update_submit'])){
	$users = $_POST['user_ids'];
	if(!empty($users)){
		for($i=0;$i<count($users);$i++){
			$id = $users[$i];
			$sql = "Update league_umpires SET year_certified = '".$_POST['year_certified_'.$id]."',umpire_phone='".$_POST['umpire_phone_'.$id]."' WHERE league_id=".$league_info['id']." and umpire_id=".$id;
			mysqli_query($conn,$sql);
		}
	}

	$_SESSION['league_umpire_updated'] = 1;
	header("Location:".WWW.'league-umpires-all-'.$ppage.'.html');
	exit;
}

if(isset($_SESSION['league_umpire_deleted']) && $_SESSION['league_umpire_deleted']==1) {
	$member_deleted = 1;
	unset($_SESSION['league_umpire_deleted']);
}

if(isset($_SESSION['league_umpire_updated']) && $_SESSION['league_umpire_updated']==1) {
	$member_updated = 1;
	unset($_SESSION['league_umpire_updated']);
}

$page_title = 'League Umpires, Referees & Scorers - '.$league_info['company_name'];
?>
<?php include('common/header.php'); ?>

<?php

                		$rpp = PRODUCT_LIMIT_FRONT; // results per page

      					$query = "select distinct u.*,c.year_certified,c.umpire_phone from users u inner join league_umpires as c on u.id=c.umpire_id where u.status = 1 and c.league_id=".$league_info['id'];
						$query_count = "select count(*) as users_count from users u inner join league_umpires as c on u.id=c.umpire_id where u.status = 1 and c.league_id=".$league_info['id'];
				      	//=======================================
					    if(isset($_GET['keywords']) && $_GET['keywords'] != 'Search Here' && $_GET['keywords'] != 'all' && !empty($_GET['keywords'])){
							$keywords = trim($_GET['keywords']);
							if(strpos($keywords,'_') !== false){
								$keywords_arr = explode('_',$keywords);
								$where.= " and (u.f_name like '%".$keywords_arr[0]."%'  AND u.last_name like '%".$keywords_arr[1]."%') ";
							}else{
								$where  .= " and (u.f_name like '%{$keywords}%' OR u.m_name like '%{$keywords}%' OR u.last_name like '%{$keywords}%') ";
							}
						}else{
							$keywords = '';
							$where  = '';
						}

						$query.=$where;
						$query_count.=$where;
					    $query .= " order by u.f_name,u.last_name  ";

						$rs_count   = mysqli_query($conn,$query_count);
						$row_count  = mysqli_fetch_assoc($rs_count);
						$tcount = $row_count['users_count'];


						$tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
						$count = 0;
						$start = ($ppage-1)* $rpp;
						$x = 0;

						$query .= " LIMIT $start,$rpp "; ?>

<div class="page-container">
		<?php include('common/user-left-panel.php');?>
      <!-- END SIDEBAR -->

      <!-- BEGIN CONTENT -->
      <div class="page-content-wrapper">
        <div class="page-content">

          <div class="white-box">
            <div class="row">
              <div class="col-md-12">
                <h2> League Umpires, Referees & Scorers - <?php echo $league_info['company_name'];  ?> </h2>
				<?php if(isset($member_deleted) && $member_deleted == 1): ?>
					<div id="information" class="alert alert-success">Umpires deleted Successfully... !</div>
				<?php  endif; ?>

				<?php if(isset($member_updated) && $member_updated == 1): ?>
					<div id="information" class="alert alert-success">Umpires updated Successfully... !</div>
				<?php  endif; ?>

				<?php if(empty($user_info)): ?>
					<div id="error" class="alert alert-danger">You are not logged... !</div>
				<?php endif; ?>

				<?php if(!empty($user_info) && empty($league_info) ): ?>
					<div id="error" class="alert alert-danger">You are not logged as League Owner.. !</div>
				<?php endif; ?>
              </div>
            </div>

            <div id="pagination-top">
              <div class="row">
                <div class="col-sm-6">
                <? if($tcount != 0 && $tcount > PRODUCT_LIMIT_FRONT){ ?>
		      	<?php
		        	//$reload = 'club-members-search.html?';
					if(!empty($keywords)){
						$reload = "league-umpires-{$keywords}.html?";
					}else{
						$reload = "league-umpires-all.html?";
					}
		        	echo paginate_one($reload, $ppage, $tpages);
		      	?>
		      	<input type="hidden" name="pagination-page" value="league-umpires.html">
		        <? } ?>
				</div>
                <div class="col-sm-3" id="search-div1">
					<form id="list-search" method="post" action="">
                  <div class="input-group" >
                    <input class="form-control validate[required] input-login" name="txtsearch" id="txtsearch" placeholder="Search Here" type="text" value="<?=$_POST['txtsearch']?>">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit">Go!</button>
                    </span>
                  </div>
				  </form>
                </div>
              </div>
            </div>

			<form method="post">
            <div id="individual" class="content1">
				<?php
						$rs   = mysqli_query($conn,$query);
						$rcount = mysqli_num_rows(mysqli_query($conn,$query));
						if($rcount == 0){
							echo '<div id="information" class="alert alert-danger">No record found ... !</div>';
						}

						$i= 0 ;
						while($row 	= mysqli_fetch_assoc($rs)){
							if($row['permalink']!=''){
								$pLink=WWW."yci/".$row['permalink'];
							}else{
								$pLink=WWW."individual-detail-".$row['id']."-".friendlyURL($row['f_name']." ".$row['m_name']." ".$row['last_name']).".html";
							}

                		$row_img = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row['id']."' and is_default = '1' "));
			  			$row_country= get_record_on_id('countries', $row['country_id']);
			  			$location 	= $row_country['name'];
			  			$row_vid = mysqli_fetch_assoc(mysqli_query($conn,"select * from videos where entity_id = '".$row['id']."' and entity_type = 'users' and is_default = '1' and file_name != ''"));
		        ?>
              <dl>
                <dt> <a href="<?=$pLink?>" title="<?=$row['f_name'].' '.$row['m_name'].' '.$row['last_name']?>"><img src="<?php echo WWW;?><?=($row_img && !empty($row_img['file_name']))?'users/'.$row['id'].'/photos/'.$row_img['file_name']:'images/no-photo.jpg'?>"></a></dt>
                <dd>
                  <div class="details">
                    <h3><a href="<?=$pLink?>" title="<?=$row['f_name'].' '.$row['m_name'].' '.$row['last_name']?>"><?=truncate_string($row['f_name'],20).' '.truncate_string($row['last_name'], 20)?></a></h3>
                    <?if($row['user_type_id'] != 1){
						echo get_combo('user_types','name',$row['user_type_id'],'','text');
					?><br />
					<? if($row['user_type_id'] == 2){ echo $row['type'].'<br/>';} } ?>
					<?php if(!empty($league_info)): ?>

										<div style="float:left;">
											<div style="float:left;margin-bottom:5px;">
												<div style="float: left; width: 160px;">Date Certified
												<input type="text" name="year_certified_<?php echo $row['id']; ?>" id="year_certifiied" value="<?php if($row['year_certified']!='0000-00-00') echo $row['year_certified']; ?>" style="width:135px;margin-left:2px;" class="input-login year-certified datepicker" placeholder="Date Certified">&nbsp;
												</div>
												<div style="float:left;width:135px;">Phone No
												<input type="text" name="umpire_phone_<?php echo $row['id']; ?>" id="umpire_phone" value="<?php echo $row['umpire_phone']; ?>" style="width:150px;margin-left:2px;height:32px;"  class="input-login" placeholder="Umpire Phone">
												</div>
												<div class="clear"></div>
												<input type="hidden" name="user_ids[]"  value="<?php echo $row['id']; ?>">
											</div>
											<div class="clear"></div>

											<input type="checkbox" name="member_ids[]" id="member_id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>"> Remove from League
										</div>
					<?php endif; ?>

                    <a href="<?=$pLink?>" title="<?=$row['f_name'].' '.$row['m_name'].' '.$row['last_name'].' - '.$site_title?>">View Profile</a>
                    <span class="flag"><img title="<?=$row_country['name']?>" alt="<?=$row_country['name']?>" src="<?php echo WWW;?>countries/<?=$row_country['flag']?>" ></span>
                  </div>
                  <div class="video">
				  <?
										$video = $row_vid['file_name'];
											if(!empty($video)){
											if(preg_match('/<iframe(.*)<\/iframe>/', $row_vid['file_name'])){

														preg_match('/src="(.*?)"/',$row_vid['file_name'] , $src);
														$src = $src[1];
														$video = "<iframe width='205' height='130' frameborder='0' allowfullscreen src='$src'></iframe>";


											}else{
												$filename = explode('.',$video);
												$filename1= $filename[0];
												$video = '<video width="220" height="130" controls>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mp4" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.ogg" type=\'video/ogg; codecs="theora, vorbis"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mov" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.webm" type=\'video/webm; codecs="vp8, vorbis"\'>
												</video>';
											}
										}else{
											$video = '<img src="'.WWW.'images/no-video.jpg" >';
										}
										echo $video;?>
					</div>
                </dd>
              </dl>
			  <?php

				$i++;
				$count++;
				$x++;
				}
				?>

              <div class="clearfix"></div>
            </div>
			<div class="clearfix"></div>
						<?php if($rcount > 0){ ?>
						<div class="row">
              <div class="col-sm-12">
                <input name="members_del_submit" id="members_del_submit" value="Delete Umpires" onclick="$('#submit_action').val('delete');" type="submit" class="btn orange hvr-float-shadow" style="margin:15px 0">
				<input name="umpire_update_submit" id="umpire_update_submit" value="Update Umpires" onclick="$('#submit_action').val('update');" type="submit" class="btn orange hvr-float-shadow" style="margin:15px 0">
              </div>
            </div>
					<?php } ?>

			</form>
            <div class="clearfix"></div>
            <div id="pagination-bottom">
              <div class="row">
                <div class="col-sm-6">
                <? if($tcount != 0 && $tcount > PRODUCT_LIMIT_FRONT){ ?>
		      	<?php
		        	//$reload = 'club-members-search.html?';
					if(!empty($keywords)){
						$reload = "league-umpires-{$keywords}.html?";
					}else{
						$reload = "league-umpires-all.html?";
					}
		        	echo paginate_one($reload, $ppage, $tpages);
		      	?>
		      	<input type="hidden" name="pagination-page" value="league-umpires.html">
		        <? } ?>
				</div>
                <div class="col-sm-3" id="search-div2">
					<form id="list-search" method="post" action="">
                  <div class="input-group">
                    <input class="form-control validate[required] input-login" name="txtsearch" id="txtsearch" placeholder="Search Here" type="text" value="<?=$_POST['txtsearch']?>">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit">Go!</button>
                    </span>
                  </div>
				  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- END CONTENT-->
    </div><!-- /.container -->

	<script type="text/javascript">
		        $('form#list-search').submit(function(e){
					var parentId = $(this).parent().attr('id');
					var string = $('#'+parentId +' form input[name=txtsearch]').val();
					if(string==""){
						$('form#list-search').attr('action','<?=WWW;?>league-umpires.html');
					}
					if(string != '' && string != 'Search Here'){
						string = string.replace(/[ ]+/g,'_');
						string = string.replace(/[^a-zA-Z0-9_]+/g,'').toLowerCase();
						if(string.length > 0){
							$('form#list-search').attr('action','<?=WWW;?>league-umpires-' + string + '.html');
						}
					}
				});
				$(document).ready(function() {
					$('.datepicker').datepicker({'format':'yyyy-mm-dd'});
				});
	</script>

<?php include('common/footer.php'); ?>
