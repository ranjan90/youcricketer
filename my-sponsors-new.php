<?php include_once('includes/configuration.php');
$page = 'my-sponsors.html';
$page_title = 'My Sponsors'; ?>
<?php include('common/header.php');
$user_id = $_SESSION['ycdc_dbuid'];
$payment = mysqli_fetch_assoc(mysqli_query($conn,"select * from user_sponsor_payment WHERE user_id = $user_id  AND status = 1 ORDER BY user_sponsor_id DESC"));
if(!empty($payment)) {

    $date   = date('Y-m-d');
    $end    =  $payment['date_end'];
    if(strtotime($date) < strtotime($end)  ) {
        $days = dateTimeDaysLeft($start,$end);
    } else {
        $days = 0;
    }
}
if(isset($_POST['action']) && $_POST['action'] == 'update_sort')
{
    $sp_id = (int)$_POST['sponsor_id'];
    $sort_order = (int) $_POST['sort'];
    $sql   = "UPDATE my_sponsors SET sort = $sort WHERE sponsor_id = $sp_id";
    if( mysqli_query($conn,$sql) ) {
        echo '<p id="error" class="alert alert-success">Sort Order Updated Successfully.</p>';
    }else{
        echo '<p id="error" class="alert alert-danger">Error in Updated sort order. Try again later</p>';
    }
}
?>

<div class="page-container"> 
	<?php include('common/user-left-panel.php');?>
	
	<!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
          
            <div class="white-box">
				<div class="row">
					<div class="col-sm-12">
                        <h2> My Sponsors -
                            <?php if(!empty($days)){ ?>
                            <a href="javascript:void(0)" style="font-size:15px; font-weight: normal; color:#1743ac">
                                <?php echo "Your package is active till <b> ".date('d', strtotime($end)).' Of '.date('M', strtotime($end)) .', '.date('Y', strtotime($end))." ".$days.' day(s) left.</b>' ?>
                            </a> </h2>
                            <?php } else {?>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#sponsorModal" style="font-size:15px; font-weight: normal; color:#1743ac"> <?php echo get_record_on_id('cms', 48)['title'] ?></a> </h2>
					        <?php } ?>
					</div>
				</div>
				<br/>
				<?php if(empty($payment) || empty($days)){ ?>

	<?} else {?>
<?php
                    if(isset($_POST['submit_btn']) && !empty($_POST['submit_btn'])){

                           $title = $_POST['title'];
                           $sort = $_POST['sort'];
                        //   $detail = $_POST['detail'];
                           $url = $_POST['url'];

                            for($i=0;$i<count($_FILES['my_sponsors']['name']);$i++){
                                $targetFolder = 'my-sponsors';
                                $tempFile = $_FILES['my_sponsors']['tmp_name'][$i];
                                $targetPath = $targetFolder;
                                $fileParts = pathinfo($_FILES['my_sponsors']['name'][$i]);
                                $fileName = $fileParts['filename'].'_'.rand(99,9999).time().$user_id.'.'.$fileParts['extension'];
                                $targetFile = $targetPath . '/' .$fileName ;
                                $file_orig = $fileParts['filename'].'.'.$fileParts['extension'];
                                if(!move_uploaded_file($tempFile,$targetFile)){
                                    $error= 'Error in uploading File';
                                }else{

                                    $sql = "INSERT INTO my_sponsors (`user_id`,`title`,`sort`,`file_type`,`url`,`file_info`) VALUES 
                                            ('$user_id','$title','$sort','image','$url','$fileName')";
                                    if( mysqli_query($conn,$sql) ) {
                                       echo '<p id="error" class="alert alert-success">Uploaded Successfully.</p>';
                                    }else{
                                        echo '<p id="error" class="alert alert-danger">Error in adding File. Try again later</p>';
                                    }
                                }
                            }


                           // $sql = " UPDATE companies SET by_laws_file =  '$file1',by_laws_file_orig = '$file2' ,tournament_data_added = 'yes' WHERE user_id='".$user_id."'";




                    }

                    if(isset($_GET['id']) && !empty($_GET['id'])) {
                        $sponsor_id = (int) $_GET['id'];
                        $rs_imgs = mysqli_query($conn,"select * from my_sponsors where sponsor_id = $sponsor_id ");
                        $row  = mysqli_fetch_assoc($rs_imgs);
                        if(!empty($row))
                        {
                            $sql = "DELETE FROM my_sponsors WHERE sponsor_id = $sponsor_id";
                            if (mysqli_query($conn, $sql)) {
                                @unlink('my-sponsors/'.$row['file_info']);
                                echo '<p id="error" class="alert alert-success">Deleted Successfully.</p>';
                            } else {
                                echo '<p id="error" class="alert alert-danger">Error in Deleting File. Try again later</p>';
                            }
                        }
                    }

                    unset($_POST);
                    ?>

                <div class="row">

                   <form method="post" action="<?=$_SERVER['PHP_SELF']?>" enctype="multipart/form-data" class="form-horizontal pull-left">

                          <?php /*endif; */?>
                          <?php /*endif; */?>
                          <div class="form-group">
                            <label class="col-sm-5 control-label">Title</label>
                              <div class="col-sm-7">
                                  <input type="text" name="title" placeholder="Title" required="required">
                              </div>
                          </div>
                       <div class="form-group">
                           <label class="col-sm-5 control-label">Display Order</label>
                           <div class="col-sm-7">
                               <input type="text" name="sort" placeholder="i.e 1,2,3" required="required">
                           </div>
                       </div>
                       <div class="form-group">
                           <label class="col-sm-5 control-label">Sponsor Url/Link</label>
                           <div class="col-sm-7">
                               <input type="url" name="url" placeholder="Proper Url/Link " required="required">
                           </div>
                       </div>
                         <div class="form-group">
                            <label class="col-sm-5 control-label">Upload Sponsor Ad Image</label>
                            <div class="col-sm-7">
                              <input accept=".jpg,.png" name="my_sponsors[]" id="my_sponsers" type="file" required="required" >
                              <span class="help-block">

                                Allowed File Types: jpg,png <br> Max size is 10mb.
                              </span>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-sm-offset-5 col-sm-7">
                              <input name="submit_btn" type="hidden">
                              <input name="submit_btn" id="submit_btn" value=" Submit " class="btn orange hvr-float-shadow" type="submit">
                            </div>
                          </div>
                   </form>

                </div>


			<div class="clearfix"><br></div>
            <div id="gallery">
                <div class="row">
                    <? 	$rs_imgs = mysqli_query($conn,"select * from my_sponsors where user_id = '$user_id' ORDER BY sort ASC ");
                    while($row_img = mysqli_fetch_assoc($rs_imgs)){ ?>

                        <div class="col-sm-2">
                            <a href="<?=$row_img['url']?>" target="_blank" title="<?=$row_img['title']?>">
                                <img src="<?=WWW?>my-sponsors/<?=$row_img['file_info']?>" alt="<?=$row_img['url']?>" title="<?=$row_img['title']?>" alt="<?=$row_img['title']?>" class="img-responsive"> </a>
                                <span><?=$row_img['url']?></span> <br>
                                <div class="row" style="width: 98%; margin-left: 1px;">
                                    <span class="pull-left">
                                        Display Order
                                    </span>
                                    <span>
                                        <form method="POST" class="">
                                        <input type="hidden" name="sponsor_id" value="<?=$row_img['sponsor_id']?>">
                                        <input type="hidden" name="action" value="update_sort">
                                        <input type="text" style="width: 16%;" name="sort" value="<?=$row_img['sort']?>">
                                        <input type="submit" class=""  value="Update" style="width: 30%; font-size: 10px;font-weight: bold;" >
                                    </form>
                                    </span>
                                </div>
                            <a href="<?=WWW?>my-sponsors.html?id=<?=$row_img['sponsor_id']?>" title="Delete Photo" onclick="return confirm('Are you sure to delete Photo');" class="btn blue full hvr-float-shadow"> <i class="fa fa-times"></i> </a>
                        </div>
                    <?  } ?>
                </div>
            </div>
			<div id="show_images">
				<div class="row">
					<div class="col-md-12">
					  <h1>Drag and drop images...</h1>
					<div class="drag">
						<img rel="1" src="http://lorempixel.com/200/200/city/1" class="img-thumbnail">
						<img rel="2" src="http://lorempixel.com/200/200/city/2" class="img-thumbnail">
						<img rel="3" src="http://lorempixel.com/200/200/city/3" class="img-thumbnail">
					</div>
						<p id="output">1,2,3</p>
					</div>
				</div>
			</div>
		</div>
            <?php }?>
      </div>
      <!-- END CONTENT-->
</div>


    <!-- Modal -->
    <div id="sponsorModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><?php $contnt = get_record_on_id('cms', 48); echo $contnt['title']?></h4>
                </div>
                <div class="modal-body">
                    <p><?php echo $contnt['content'] ?> </p>
                </div>
                <!-- <div class="modal-footer">
                     <button type="button"  class="btn btn-success click-to-submit">Pay Now</button>
                     <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                 </div>-->
            </div>

        </div>
    </div>

    <script>
        $(document).ready(function(){

            $("#submit_btn").bind('click',function(){

                if($("#my_sponsors").val() == ''){
                    alert('File is Requied Field');
                    return false;

                }
                if($("#my_sponsors").val() != ''){

                    var file_size = $("#my_sponsors")[0].files[0].size/(1024*1024);
                    if(file_size>10){
                        alert('File size is greater than 10MB');
                        return false;
                    }

                    var res_field = $("#my_sponsors").val();
                    var extension = res_field.substr(res_field.lastIndexOf('.') + 1).toLowerCase();
                    var allowedExtensions = ['jpg', 'png'];
                    if (res_field.length > 0)
                    {
                        if (allowedExtensions.indexOf(extension) === -1)
                        {
                            alert('Invalid file Format. Only ' + allowedExtensions.join(', ') + ' are allowed.');
                            return false;
                        }
                    }
                }
                return true;
            });
        });
    </script>
	
<!-- SCRIPT START HERE -->
<script>
!function(e,x,h){function r(a,b){var c=Math.max(0,a[0]-b[0],b[0]-a[1]),d=Math.max(0,a[2]-b[1],b[1]-a[3]);return c+d}function s(a,b,c,d){for(var f=a.length,d=d?"offset":"position",c=c||0;f--;){var k=a[f].el?a[f].el:e(a[f]),i=k[d]();i.left+=parseInt(k.css("margin-left"),10);i.top+=parseInt(k.css("margin-top"),10);b[f]=[i.left-c,i.left+k.outerWidth()+c,i.top-c,i.top+k.outerHeight()+c]}}function l(a,b){var c=b.offset();return{left:a.left-c.left,top:a.top-c.top}}function t(a,b,c){for(var b=[b.left,b.top],
c=c&&[c.left,c.top],d,f=a.length,e=[];f--;)d=a[f],e[f]=[f,r(d,b),c&&r(d,c)];return e=e.sort(function(a,b){return b[1]-a[1]||b[2]-a[2]||b[0]-a[0]})}function m(a){this.options=e.extend({},j,a);this.containers=[];this.scrollProxy=e.proxy(this.scroll,this);this.dragProxy=e.proxy(this.drag,this);this.dropProxy=e.proxy(this.drop,this);this.options.parentContainer||(this.placeholder=e(this.options.placeholder),a.isValidTarget||(this.options.isValidTarget=h))}function n(a,b){this.el=a;this.options=e.extend({},
v,b);this.group=m.get(this.options);this.rootGroup=this.options.rootGroup=this.options.rootGroup||this.group;this.parentContainer=this.options.parentContainer;this.handle=this.rootGroup.options.handle||this.rootGroup.options.itemSelector;this.el.on(o.start,this.handle,e.proxy(this.dragInit,this));this.options.drop&&this.group.containers.push(this)}var o,v={drag:!0,drop:!0,exclude:"",nested:!0,vertical:!0},j={afterMove:function(){},containerPath:"",containerSelector:"ol, ul",distance:0,handle:"",itemPath:"",
itemSelector:"li",isValidTarget:function(){return!0},onCancel:function(){},onDrag:function(a,b){a.css(b)},onDragStart:function(a){a.css({height:a.height(),width:a.width()});a.addClass("dragged");e("body").addClass("dragging")},onDrop:function(a){a.removeClass("dragged").removeAttr("style");e("body").removeClass("dragging")},onMousedown:function(a,b){b.preventDefault()},placeholder:'<li class="placeholder"/>',pullPlaceholder:!0,serialize:function(a,b,c){a=e.extend({},a.data());if(c)return b;b[0]&&
(a.children=b,delete a.subContainer);delete a.sortable;return a},tolerance:0},p={},u=0,w={left:0,top:0,bottom:0,right:0};o={start:"touchstart.sortable mousedown.sortable",drop:"touchend.sortable touchcancel.sortable mouseup.sortable",drag:"touchmove.sortable mousemove.sortable",scroll:"scroll.sortable"};m.get=function(a){p[a.group]||(a.group||(a.group=u++),p[a.group]=new m(a));return p[a.group]};m.prototype={dragInit:function(a,b){this.$document=e(b.el[0].ownerDocument);b.enabled()?(this.toggleListeners("on"),
this.item=e(a.target).closest(this.options.itemSelector),this.itemContainer=b,this.setPointer(a),this.options.onMousedown(this.item,a,j.onMousedown)):this.toggleListeners("on",["drop"]);this.dragInitDone=!0},drag:function(a){if(!this.dragging){if(!this.distanceMet(a))return;this.options.onDragStart(this.item,this.itemContainer,j.onDragStart);this.item.before(this.placeholder);this.dragging=!0}this.setPointer(a);this.options.onDrag(this.item,l(this.pointer,this.item.offsetParent()),j.onDrag);var b=
a.pageX,a=a.pageY,c=this.sameResultBox,d=this.options.tolerance;if(!c||c.top-d>a||c.bottom+d<a||c.left-d>b||c.right+d<b)this.searchValidTarget()||this.placeholder.detach()},drop:function(){this.toggleListeners("off");this.dragInitDone=!1;if(this.dragging){if(this.placeholder.closest("html")[0])this.placeholder.before(this.item).detach();else this.options.onCancel(this.item,this.itemContainer,j.onCancel);this.options.onDrop(this.item,this.getContainer(this.item),j.onDrop);this.clearDimensions();this.clearOffsetParent();
this.lastAppendedItem=this.sameResultBox=h;this.dragging=!1}},searchValidTarget:function(a,b){a||(a=this.relativePointer||this.pointer,b=this.lastRelativePointer||this.lastPointer);for(var c=t(this.getContainerDimensions(),a,b),d=c.length;d--;){var f=c[d][0];if(!c[d][1]||this.options.pullPlaceholder)if(f=this.containers[f],!f.disabled){if(!this.$getOffsetParent())var e=f.getItemOffsetParent(),a=l(a,e),b=l(b,e);if(f.searchValidTarget(a,b))return!0}}this.sameResultBox&&(this.sameResultBox=h)},movePlaceholder:function(a,
b,c,d){var f=this.lastAppendedItem;if(d||!(f&&f[0]===b[0]))b[c](this.placeholder),this.lastAppendedItem=b,this.sameResultBox=d,this.options.afterMove(this.placeholder,a)},getContainerDimensions:function(){this.containerDimensions||s(this.containers,this.containerDimensions=[],this.options.tolerance,!this.$getOffsetParent());return this.containerDimensions},getContainer:function(a){return a.closest(this.options.containerSelector).data("sortable")},$getOffsetParent:function(){if(this.offsetParent===
h){var a=this.containers.length-1,b=this.containers[a].getItemOffsetParent();if(!this.options.parentContainer)for(;a--;)if(b[0]!=this.containers[a].getItemOffsetParent()[0]){b=!1;break}this.offsetParent=b}return this.offsetParent},setPointer:function(a){a={left:a.pageX,top:a.pageY};if(this.$getOffsetParent()){var b=l(a,this.$getOffsetParent());this.lastRelativePointer=this.relativePointer;this.relativePointer=b}this.lastPointer=this.pointer;this.pointer=a},distanceMet:function(a){return Math.max(Math.abs(this.pointer.left-
a.pageX),Math.abs(this.pointer.top-a.pageY))>=this.options.distance},scroll:function(){this.clearDimensions();this.clearOffsetParent()},toggleListeners:function(a,b){var c=this,b=b||["drag","drop","scroll"];e.each(b,function(b,f){c.$document[a](o[f],c[f+"Proxy"])})},clearOffsetParent:function(){this.offsetParent=h},clearDimensions:function(){this.containerDimensions=h;for(var a=this.containers.length;a--;)this.containers[a].clearDimensions()}};n.prototype={dragInit:function(a){var b=this.rootGroup;
!b.dragInitDone&&1===a.which&&this.options.drag&&!e(a.target).is(this.options.exclude)&&b.dragInit(a,this)},searchValidTarget:function(a,b){var c=t(this.getItemDimensions(),a,b),d=c.length,f=this.rootGroup,e=!f.options.isValidTarget||f.options.isValidTarget(f.item,this);if(!d&&e)return f.movePlaceholder(this,this.el,"append"),!0;for(;d--;)if(f=c[d][0],!c[d][1]&&this.hasChildGroup(f)){if(this.getContainerGroup(f).searchValidTarget(a,b))return!0}else if(e)return this.movePlaceholder(f,a),!0},movePlaceholder:function(a,
b){var c=e(this.items[a]),d=this.itemDimensions[a],f="after",h=c.outerWidth(),i=c.outerHeight(),g=c.offset(),g={left:g.left,right:g.left+h,top:g.top,bottom:g.top+i};this.options.vertical?b.top<=(d[2]+d[3])/2?(f="before",g.bottom-=i/2):g.top+=i/2:b.left<=(d[0]+d[1])/2?(f="before",g.right-=h/2):g.left+=h/2;this.hasChildGroup(a)&&(g=w);this.rootGroup.movePlaceholder(this,c,f,g)},getItemDimensions:function(){this.itemDimensions||(this.items=this.$getChildren(this.el,"item").filter(":not(.placeholder, .dragged)").get(),
s(this.items,this.itemDimensions=[],this.options.tolerance));return this.itemDimensions},getItemOffsetParent:function(){var a=this.el;return"relative"===a.css("position")||"absolute"===a.css("position")||"fixed"===a.css("position")?a:a.offsetParent()},hasChildGroup:function(a){return this.options.nested&&this.getContainerGroup(a)},getContainerGroup:function(a){var b=e.data(this.items[a],"subContainer");if(b===h){var c=this.$getChildren(this.items[a],"container"),b=!1;c[0]&&(b=e.extend({},this.options,
{parentContainer:this,group:u++}),b=c.sortable(b).data("sortable").group);e.data(this.items[a],"subContainer",b)}return b},enabled:function(){return!this.disabled&&(!this.parentContainer||this.parentContainer.enabled())},$getChildren:function(a,b){var c=this.rootGroup.options,d=c[b+"Path"],c=c[b+"Selector"],a=e(a);d&&(a=a.find(d));return a.children(c)},_serialize:function(a,b){var c=this,d=this.$getChildren(a,b?"item":"container").not(this.options.exclude).map(function(){return c._serialize(e(this),
!b)}).get();return this.rootGroup.options.serialize(a,d,b)},clearDimensions:function(){this.itemDimensions=h;if(this.items&&this.items[0])for(var a=this.items.length;a--;){var b=e.data(this.items[a],"subContainer");b&&b.clearDimensions()}}};var q={enable:function(){this.disabled=!1},disable:function(){this.disabled=!0},serialize:function(){return this._serialize(this.el,!0)}};e.extend(n.prototype,q);e.fn.sortable=function(a){var b=Array.prototype.slice.call(arguments,1);return this.map(function(){var c=
e(this),d=c.data("sortable");if(d&&q[a])return q[a].apply(d,b)||this;!d&&(a===h||"object"===typeof a)&&c.data("sortable",new n(c,a));return this})}}(jQuery,window);

$(document).ready(function()
{
    var adjustment

    var group = $("div.drag").sortable({
        group: 'drag',
        itemSelector: 'img',
        containerSelector: 'div',
        vertical: false,
        placeholder: '<div class="placeholder" />',
        pullPlaceholder: false,

        // set item relative to cursor position
        onDragStart: function ($item, container, _super) {
            var offset = $item.offset(),
            pointer = container.rootGroup.pointer

            adjustment = {
              left: pointer.left - offset.left,
              top: pointer.top - offset.top
            }

            _super($item, container)
        },
        onDrop: function (item, container, _super) {
            console.log(group.sortable("serialize").get());
            $('#output').text(group.sortable("serialize").get().join("\n"))
            _super(item, container)
        },
        onDrag: function ($item, position) {
        $item.css({
            width: 180,
            height: 180,
            left: position.left - adjustment.left,
            top: position.top - adjustment.top
        })
        },
        serialize: function (parent, children, isContainer) {
            return isContainer ? children.join() : parent.attr('rel')
        }
    })
});
</script>
<!-- Script END -->
	
<?php include('common/footer.php'); ?>