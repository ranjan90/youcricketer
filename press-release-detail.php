<?php include('common/header.php'); ?>
	<div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1>Press Release Detail </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <div class="white-box">
          <?php $row = get_record_on_id('press_releases',$_GET['id']);?>
			<h2><?php echo $row['title']?></h2>
			<!-- AddThis Button BEGIN -->
					<div class="addthis_toolbox addthis_default_style ">
					<a class="addthis_button_preferred_1"></a>
					<a class="addthis_button_preferred_2"></a>
					<a class="addthis_button_preferred_3"></a>
					<a class="addthis_button_preferred_4"></a>
					<a class="addthis_button_compact"></a>
					<a class="addthis_counter addthis_bubble_style"></a>
					</div>
					<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
					<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5073efa669740139"></script>
			<!-- AddThis Button END -->
		    <div class="clear"></div>
			<div><b>Released On : </b><?php echo date_converter($row['release_date'])?></div>
			<div class="clear" style="height:10px;"></div>
				<div class="space10"></div>
				<h3>Summary</h3>
				<?php echo $row['summary']?>
				<h3>Detail</h3>
				<?php echo $row['content']?>
		    
		  </div>
        </div>
        
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
             <?php include('common/right-panel.php');?>
            </div>
          </div>
          
        </div>
      </div>
</div><!-- /.container -->	
<?php include('common/footer.php'); ?>