<?php include('common/header.php'); ?>
<? 
				$slug = $_GET['slug'];
				$sql = "Select * from how_tos WHERE slug = '$slug'";
				$rs = mysqli_query($conn,$sql);
				$how_to_data = mysqli_fetch_assoc($rs);
					
?>

<div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1>
            How to Detail - <?php echo $how_to_data['title']; ?><br>
            <span> <strong>Created By :</strong> Administrator <strong>Created On :</strong> <?php echo date('d M, Y',strtotime($how_to_data['date_added'])); ?> </span>
          </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <div class="white-box">
            <?php echo  $how_to_data['content']?>
		  </div>
        </div>
        
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
              <?php include('common/right-panel.php');?>
            </div>
          </div>
          
        </div>
      </div>
</div><!-- /.container -->
	
<?php include('common/footer.php'); ?>