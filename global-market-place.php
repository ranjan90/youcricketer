<?php include('includes/configuration.php');
$page = 'global-market-place.html';
 include('common/header.php');
 $square_msg='';

    /* Square Process */
    if(isset($_POST['square'])){

      $ttlQty=array();
      $title=array();
      $ttlPrice=array();
      $line_items='';

      for($i=0;$i<=count($_POST['qty']);$i++){

          if(isset($_POST['ids'][$i])){

            $id=$_POST['ids'][$i];

            $item[]=$_POST['ids'][$i];
            $ttlQty[]=$_POST['qty'][$id];
            $title[]=$_POST['title'][$id];
            $ttlPrice[]=$_POST['price'][$id];

            $line_items.='{"name":"'.$_POST['title'][$id].'","quantity":"'.$_POST['qty'][$id].'","base_price_money":{"amount":'.($_POST['price'][$id]*100).',"currency":"USD"}},';

          }

      }



      $line_items=rtrim($line_items,',');

      $_SESSION['square']['user_id']=$_SESSION['ycdc_dbuid'];
      $_SESSION['square']['item']=$item;
      $_SESSION['square']['title']=$title;
      $_SESSION['square']['price']=$ttlPrice;
      $_SESSION['square']['quantity']=$ttlQty;


      /* client location id*/
      $location="CBASECUjDAZuTF_vJ0UVtWTJtJIgAQ";

      /* Post Url*/
      $url="https://connect.squareup.com/v2/locations/".$location."/checkouts";

      /* Authorization headers */
      $header[]="Authorization : Bearer sandbox-sq0atb-EUScP628qDebNAxU0oYJIg";
      $header[]="Content-Type : application/json";

      $ur=WWW."global-market-place.html?square_callback=yes";

      /* post data*/
      $var='{"redirect_url":"'.$ur.'","idempotency_key":"'.uniqid().'","ask_for_shipping_address":true,"merchant_support_email":"anujsetia@1wayit.com","order":{"reference_id":"Youcricketer","line_items":['.$line_items.']}}';

      /* curl */
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,$url);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,$var);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      /* response */
      $response =json_decode(curl_exec ($ch));
      curl_close ($ch);

      /* if return checkout url then it redirect to on that url else show error msg*/
      if(isset($response->checkout->checkout_page_url)){

          echo "<script>window.location.href='".$response->checkout->checkout_page_url."'</script>";

      }else{

        createMsg("The Amount must be greater than 100.",'error');
      }

    }


    /* paypal Process */
    if(isset($_POST['paypal'])){

      $url='';

      $ttlQty=array();
      $title=array();
      $ttlPrice=array();
      $tl='';
      $amount=0;
      $qty=0;

      for($i=0;$i<=count($_POST['qty']);$i++){

        if(isset($_POST['ids'][$i])){

            $id=$_POST['ids'][$i];

            $item[]=$_POST['ids'][$i];
            $ttlQty[]=$_POST['qty'][$id];
            $qty+=$_POST['qty'][$id];
            $title[]=$_POST['title'][$id];
            $ttlPrice[]=$_POST['price'][$id];
            $tl.=$_POST['title'][$id].', ';
            $amount+=$_POST['price'][$id];

          }

      }

      $_SESSION['paypal']['user_id']=$_SESSION['ycdc_dbuid'];
      $_SESSION['paypal']['item']=$item;
      $_SESSION['paypal']['title']=$title;
      $_SESSION['paypal']['price']=$ttlPrice;
      $_SESSION['paypal']['quantity']=$ttlQty;

      ?>
      <form action="<?php echo PAYPAL_URL;?>" method="post" id="paypal-sub">
          <input type="hidden" name="cmd" value="_cart">
          <input type="hidden" name="business" value="<?php echo PAYPAL_EMAIL;?>">

          <input type="hidden" name="upload" value="1">
          <input type="hidden" name="email" value="<?php echo $_SESSION['ycdc_user_email']?>">

          <?php for($i =0;$i< count($_SESSION['paypal']['item']);$i++){ ?>
            <input type="hidden" name="item_number_<?php echo $i+1;?>" value="<?php echo rand(1,1000000000);?>">
            <input type="hidden" name="item_name_<?php echo $i+1;?>" value="<?php echo $_SESSION['paypal']['title'][$i]; ?>">
            <input type="hidden" name="amount_<?php echo $i+1;?>" value="<?php echo $_SESSION['paypal']['price'][$i]; ?>">
            <input type="hidden" name="quantity_<?php echo $i+1;?>" value="<?php echo $_SESSION['paypal']['quantity'][$i];?>">
          <?php } ?>

          <input type="hidden" name="custom" value="<?php echo base64_encode(serialize($_SESSION['paypal']));?>" />
          <input type="hidden" name="currency_code" value="USD">
          <input type="hidden" name="return" value="<?=WWW."global-market-place.html?paypal=success"?>">
          <input type="hidden" name="cancel_return" value="<?=WWW."global-market-place.html?paypal=cancel"?>">
          <input type="hidden" name="notify_url" value="<?=WWW."paypal_notify.php"?>">
      </form>
      <script>
          document.getElementById("paypal-sub").submit();
      </script>

      <?php

    }

    /* square callback url */
    if(isset($_GET['square_callback']) && $_GET['square_callback']=='yes'){

      if(isset($_SESSION['square']) && count($_SESSION['square']['item'])>0){

          for($i=0;$i<count($_SESSION['square']['item']);$i++){

                mysqli_query($conn,"insert into user_payments (payment_type,is_sponsor,user_id,item_id,quantity,price,transaction_id,status,payment_status)values('square','1','".$_SESSION['ycdc_dbuid']."','".$_SESSION['square']['item'][$i]."','".$_SESSION['square']['quantity'][$i]."','".$_SESSION['square']['price'][$i]."','".$_GET['transactionId']."','VERIFIED','Completed')");

                $vars=$_SESSION['square'];


                if($vars['item'][$i]=='2'){

                    $payment_id=mysqli_insert_id();

                    $checkOld=mysqli_query($conn,"select * from user_sponsor_payment where user_id='".$vars['user_id']."' order by user_sponsor_id DESC");

                    if(mysqli_num_rows($checkOld)>0){

                        $record=mysqli_fetch_assoc($checkOld);

                        $endDate=$record['date_end'];

                        if(date("Ymd")<=date("Ymd",strtotime($record['date_end']))){

                          $end_date=date("Y-m-d",strtotime("+1 year",strtotime($record['date_end'])));

                          mysqli_query($conn,"update user_sponsor_payment set date_end='".$end_date."' where user_id='".$vars['user_id']."'");


                        }else{

                            $end_date=date("Y-m-d",strtotime("+1 year"));

                            mysqli_query($conn,"update user_sponsor_payment set date_end='".$end_date."' where user_id='".$vars['user_id']."'");
                        }


                    }else{

                      $end_date=date("Y-m-d",strtotime("+1 year"));

                      mysqli_query($conn,"insert into user_sponsor_payment(payment_id,user_id,quantity,date_start,date_end
                      ,status,payment_status)values('".$payment_id."','".$vars['user_id']."','1','".date("Y-m-d")."','".$end_date."','1','VERIFIED')");


                    }

                    $emailFormat=mysqli_fetch_assoc(mysqli_query($conn,"select * from cms where id='52'"));

                    $emailContent=$emailFormat['content'];

                    $headers = "Content-type:text/html;charset=UTF-8" . "\r\n";
                    $headers .= "From: no-reply@youcricketer.com\r\n";

                    $emailContent=str_replace('[name]',$_SESSION['ycdc_user_name'],$emailContent);

                    $emailContent=str_replace('[date]',$end_date,$emailContent);

                    mail($_SESSION['ycdc_user_email'],$emailFormat['title'],$emailContent,$headers);

                }

          }

          unset($_SESSION['square']);

          createMsg("Your Payment has been done.Please wait we respond you shortly..");

          my_redirect('global-market-place.html');

          die();


      }else{

          my_redirect('global-market-place.html');

      }

    }

    /* Paypal Callback*/
    if(isset($_GET['paypal'])){

      if($_GET['paypal']=='success'){

        createMsg("Your Payment has been completed.Please wait for verification we respond you shortly!...");

        my_redirect('global-market-place.html');

        die();

      }elseif($_GET['paypal']=='cancel'){

        createMsg("Your Payment has been cancelled","error");

        my_redirect('global-market-place.html');

        die();

      }

    }

?>
 <div class="page-container">

        <?php include('common/user-left-panel.php');?>
        <!-- END SIDEBAR -->
        <form action="" method="post" class="global-form">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="white-box" style="overflow: hidden !important;">
                  <?php echo displayMsg();?>


      				<div class="tit_head">
      					<div class="row">
      						  <div class="col-sm-12">
      							<h2>Global Products & Services</h2>
      						  </div>
                    <div style="clear:both;"></div><br/>

                    <div class="col-md-12">
                      <div class="alert alert-info">
                        <?php $rowMsg=get_record_on_id('cms', 53);  ?>
                        <?php echo $rowMsg['content'];?>
                      </div>
                    </div>
                    <div style="clear:both;"></div><br/>

                <div class="btns text-right">
				<p>Payment Methods : <a class="payment-btn" data-id="square"><img src="<?php echo WWW;?>images/square.png" alt="Square Payment" title="Square Payment"/></a>
				<a  class="payment-btn" data-id="paypal"><img src="<?php echo WWW;?>images/paypal.png" alt="Paypal Payment" title="Paypal Payment"/></a></p>

                <input type="submit" name="square" value="Square" class="btn square orange hvr-float-shadow" style="display:none;">
                <input type="submit" name="paypal" value="Paypal" class="btn paypal orange hvr-float-shadow"  style="display:none;">
              </div>

      						</div>
      				</div>
	            <div class="global_regional_panel">

                <?php 
				
				$tquery=mysqli_query($conn,"select * from marke_place_items where item_type='1'  and status='1' order by item_id asc");
				
				$rpp = PRODUCT_LIMIT_FRONT;
				
				$ppage = intval($_GET["page"]);
				
				if($ppage<=0){
					
					$ppage = 1;
					$start=0;
					$limit=$rpp;
					
				}else{
					$start=($ppage-1)*$rpp;
					$limit=$rpp;
				}
				
				$tcount = ceil(mysqli_num_rows($tquery)/$rpp);
				
				$query=mysqli_query($conn,"select * from marke_place_items where item_type='1'  and status='1' order by item_id asc limit $start,$limit");
				
				if(mysqli_num_rows($query)>0){
                      while($record=mysqli_fetch_assoc($query)){

                  ?>
                <input type="hidden" name="title[<?php echo $record['item_id']?>]" value="<?php echo $record['item_title'];?>">
                <input type="hidden" name="price[<?php echo $record['item_id']?>]" value="<?php echo $record['item_price'];?>">

      					<div class="global_regional">
      						<div class="row">
      							<div class="col-md-6">
      								<h4><input type="checkbox" class="chk-bx all_ids" name="ids[]" value="<?php echo $record['item_id'];?>"/> <?php echo $record['item_title'];?></h4>
      							</div>
      							<div class="col-md-3">
      								<h4>Price  <span>$ <?php echo number_format($record['item_price'],2);?></span></h4>
      							</div>
      							<div class="col-md-3">
      								<h4>Qty
                        <?php if($record['item_id']=='2'){?>
                          <span>1</span>
                          <input type="hidden" name="qty[<?php echo $record['item_id']?>]" value="1">
                        <?php }else{?>
                          <select class="form-control select_bx"  name="qty[<?php echo $record['item_id']?>]">
                          <?php for($i=1;$i<=20;$i++){?>
                            <option value="<?php echo $i;?>"><?php echo $i;?></option>
                          <?php } ?>
                        </select>
                        <?php } ?>

                      </h4>
      							</div>
      						</div>
      						<div class="row">
      							<div class="col-md-12">
      								<p><?php echo $record['item_detail'];?></p>
      							</div>
      						</div>
      					</div>
						
						

              <?php }?>
				<?php if($tcount>=1){?>
					<div id="pagination-bottom">
					  <?php
						$reload = 'global-market-place.html';
						echo paginate_one($reload, $ppage, $tcount);
					  ?>
					</div>
				<?php } ?>
					
               <?php  }else{

                    echo "<p>No Records found</p>";

                }
               ?>

      				  </div>
      				</div>
            </div>
        </div>
      </form>
  </div>
<script type="text/javascript">
  $(document).ready(function(){
    $(".payment-btn").click(function(){

        var id=$(this).attr('data-id');

        $("."+id).trigger('click');

    });

    $(".global-form").submit(function(){
        var check=$(".all_ids:checked").length;
        if(check>0)
          return true;
        else
          alert("Please Choose at least one item");

        return false;
    });
  });
</script>
<?php include('common/footer.php'); ?>
