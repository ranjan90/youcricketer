<?php include('common/header.php');

 if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
	<script>
		window.location = '<?=WWW?>logout.php';
	</script>
<? }

	if(isset($_FILES) && !empty($_FILES['photos']['name'][0])){

		$total = count($_FILES['photos']['name']);
		$user_id=$_SESSION['ycdc_dbuid'];

		if(!is_dir('users/'.$user_id)){
			mkdir('users/'.$user_id,0777);
		}
		chmod('users/'.$user_id,0777);
		if(!is_dir('users/'.$user_id.'/photos')){
			mkdir('users/'.$user_id.'/photos',0777);
		}
		chmod('users/'.$user_id.'/photos',0777);

		for($x = 0; $x < $total; $x++){
			if(!empty($_FILES['photos']['name'][$x])){
				$filename 	= friendlyURL($_FILES['photos']['name'][$x]).'.jpg';
				$rs_chk 	= mysqli_query($conn,"select * from photos where entity_id = '$user_id' and entity_type = 'users' and file_name = '$fileanme'");
				if(mysqli_num_rows($rs_chk) == 0){
					$image 		= new SimpleImage();
					$image->load($_FILES["photos"]["tmp_name"][$x]);
					if($image->getWidth() > 800){
						$image->resizeToWidth(800);
					}
					$image->save('users/'.$user_id.'/photos/'.$filename);
					chmod('users/'.$user_id.'/photos/'.$filename,0777);

					mysqli_query($conn,"insert into photos (file_name, entity_id, entity_type)
						values ('$filename','$user_id','users')");
				}
			}
		}
		$msg =  '<div id="success" class="alert alert-success">Photos uploaded ... !</div>';
	}

	$user_id=$_SESSION['ycdc_dbuid'];
?>



<div class="page-container">
		<?php  include('common/user-left-panel.php');?>
      <!-- END SIDEBAR -->

      <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
		<div class="page-content">

		<div class="white-box">
            <div class="row">
              <div class="col-md-12">
                <h2> My Photos - <?=$_SESSION['ycdc_user_name']?> </h2>
				<?php echo $msg; ?>
              </div>
            </div>
            <div id="upload-area">
              <form method="post" enctype="multipart/form-data" class="form-horizontal req-frm" id="photos-form">
                <div class="form-group">
                  <label class="col-sm-5 control-label"> Upload Photos : </label>
                  <div class="col-sm-4">
                    <input name="photos[]" multiple accept="image/*" type="file" class="">
					<span class="help-block">
                      Max Photo size : 10MB &nbsp; &nbsp; &nbsp; <a href="multimedia-policy.html" target="_blank">Multimedia Policy</a>
                    </span>
				   </div>

                </div>
				<div class="clearfix"></div>
			    <div class="clearfix"></div>
                <div class="form-group">
                  <div class="col-sm-offset-5 col-sm-7">
                    <input value="Upload" class="btn orange hvr-float-shadow" type="submit">
                  </div>
                </div>

              </form>
            </div>

            <div id="gallery">
			<div class="row">
			<? 	$rs_imgs = mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '$user_id' and is_default <> '1' ORDER BY id DESC");
					while($row_img = mysqli_fetch_assoc($rs_imgs)){ ?>

                <div class="col-sm-2">
                  <a href="<?=WWW?>users/<?=$user_id?>/photos/<?=$row_img['file_name']?>">
				  <img src="<?=WWW?>users/<?=$user_id?>/photos/<?=$row_img['file_name']?>" alt="<?=$_SESSION['ycdc_user_name']?>"  class="img-responsive"> </a>
                  <a href="<?=WWW?>delete-photo-<?=$row_img['id']?>.html" title="Delete Photo" onclick="return confirm('Are you sure to delete Photo');" class="btn blue full hvr-float-shadow"> <i class="fa fa-times"></i> </a>
                </div>
                <?  } ?>
              </div>
            </div>
          </div>


		</div>
	</div>
</div>



<script>
$(document).ready(function(){

	$("#photos-form").validate();

	$('input').change(function(){
		var val = this.value;
		var filesize = Math.round((this.files[0].size)/1024,2);
		if(filesize > 10000){
			$(this).val('');
            alert("Image is too large ... !");
		}
	});

	$('input[name=photo]').change(function(){
		var val = this.value;
		var filesize = Math.round((this.files[0].size)/1024,2);
		if(filesize > 10000){
			$(this).val('');
            alert("Image is too large ... !");
		}
	});

	$('.req-frm').validationEngine();
});
</script>

<?php include('common/footer.php'); ?>
