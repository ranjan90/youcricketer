<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	
		<? 	if(isset($_GET['action']) && $_GET['action'] == 'deletephoto'){
				$pid 	= $_GET['pid'];
				$row_img= get_record_on_id('photos',$pid);
				mysqli_query($conn,"delete from photos where id = '$pid'");
				unlink('forum/'.$_GET['id'].'/'.$row_img['file_name']);
				?>
				<script>
				window.location = '<?=WWW?>edit-forum-<?=$_GET['id']?>.html';
				</script>
				<?
			}
			if(isset($_POST) && !empty($_POST)){ 
				$title 	= $_POST['title'];
				$content= $_POST['content'];
				$date 		= date('Y-m-d');
				$isGlobal = ($_POST['is_global'] == 'on')?'1':'0';
				$user_id 	= $_SESSION['ycdc_dbuid'];
				$id 		= $_GET['id'];
				$photo_id 	= $_POST['photo_id'];

				if(!isset($user_id) || empty($user_id)){
					echo '<div id="error" class="alert alert-danger">Please login first ... !</div>';
				}else{
					if(!empty($title) && !empty($content)){
						$query = "update forum_topics set is_global = '$isGlobal', title = '$title', content = '$content',category_id=$category_id where id = '$id'";
						if(mysqli_query($conn,$query)){
							chmod('forum',0777);
							if(!is_dir('forum/'.$id)){
								mkdir('forum/'.$id,0777);
							}
							chmod('forum/'.$id,0777);
							if(!empty($_FILES['photo']['name'])){
								$filename 	= friendlyURL($title).'.jpg';
								$image 		= new SimpleImage();
								$image->load($_FILES["photo"]["tmp_name"]);
								$image->save('forum/'.$id.'/'.$filename);
								chmod('forum/'.$id.'/'.$filename,0777);
								if(!empty($photo_id)){
									mysqli_query($conn,"update photos set file_name = '$filename' where id = '$photo_id'");	
								}else{
									mysqli_query($conn,"insert into photos (file_name, entity_type, entity_id) values ('$filename','forum','$id');");
								}								
							}
						}
		                echo '<div id="success" class="alert alert-success">Forum Information updated successfully ... !</div>';
		                ?>
		                <script>
		                window.location = '<?=WWW?>my-forum.html';
		                </script>
		                <?
		            }else{
						echo '<div id="error" class="alert alert-danger">Please fill all fields ... !</div>';
					}
				}
				
			}
			$id 	= $_GET['id'];
			$row 	= get_record_on_id('forum_topics', $id);
			$row_img= mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'forum' and entity_id = '$id'"));
		?>
		
<script src="<?=WWW;?>assets/js/ckeditor/ckeditor.js"></script>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1> Update Forum </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <form method="post" action="" enctype="multipart/form-data" class="form-horizontal" id="form-add-forum">
            
            <div class="white-box">
              <div class="form-group">
                <div class="col-sm-12 text-center">
                  <h2>Forum Information</h2>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Forum Category</label>
                <div class="col-sm-9">
					<?=get_combo('forum_categories','name',$row['category_id'],'category_id')?>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Is Global</label>
                <div class="col-sm-9">
                  <input type="checkbox" name="is_global" <?php echo ($row['is_global'] == '1')?'checked="checked"':'';?>>
                  <span class="help-block">Forum will be Regional Forum If checked</span>
                </div>
              </div>
             
              <div class="form-group">
                <label class="col-sm-3 control-label">Title</label>
                <div class="col-sm-9">
                  <input name="title" value="<?=$row['title']?>" class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Content</label>
                <div class="col-sm-9">
                  <div id="editor">
					<textarea name="content" id="content"  class="ckeditor" class="validate[required]"><?=$row['content']?></textarea>
				  </div>
                </div>
              </div>
              
              <div class="form-group">
                <label class="col-sm-3 control-label">Forum Photo</label>
                <div class="col-sm-9">
                  
					<? 	if($row_img){ ?>
						
						<img src="<?=WWW?>forum/<?=$id?>/<?=$row_img['file_name']?>" width="100">&nbsp;
						<a href="<?=WWW?>edit-forum-<?=$_GET['id']?>-deletephoto-<?=$row_img['id']?>.html" onClick="return confirm('Are you sure to Photo');"><img  src="<?=WWW?>images/icons/delete.png"></a>
						<input type="hidden" name="photo_id" value="<?=$row_img['id']?>"><br/>
					<?	} ?>
					<input accept="image/*" name="photo" type="file">
                  <span class="help-block">Max Photo size : 2MB</span>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12 text-center">
                  <input value=" Save " class="btn orange hvr-float-shadow" type="submit">
                  <a id="cancel" class="btn blue hvr-float-shadow" href="<?=WWW?>forums.html">Cancel</a>
                </div>
              </div>
            </div>
		  </form>
        </div>
        
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
              <?php include('common/right-panel.php');?>
            </div>
          </div>
          
        </div>
      </div>
    </div><!-- /.container -->
	
<script type="text/javascript">
$(document).ready(function(){
	$('#form-add-forum').validationEngine();
});
</script>	
<?php include('common/footer.php'); ?>