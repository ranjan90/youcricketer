<?php include_once('includes/configuration.php');
$page = 'tournament-video-add.html';
$selected_country = getGeoLocationCountry(); 

$error = '';
$tournament_permission = 0;
$tournament_info = array();
$poll_info = array();

$poll_id = trim($_GET['p_id']);
$poll_info = get_record_on_id('tournament_polls', $poll_id);	

$tournament_id = $poll_info['tournament_id'];
$tournament_info = get_record_on_id('tournaments', $tournament_id);	

$page_title = 'Edit Poll - '.ucwords($tournament_info['title']);

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
}

if(isset($user_id) && !empty($user_id)){
	//$sql = "select * from tournament_permissions where tournament_id = $tournament_id and member_id = $user_id";
	//$rs_permission = mysqli_query($conn,$sql);
	if($tournament_info['user_id'] == $user_id){
		$tournament_permission = 1;
	}else{
		$tournament_permission = 0;
	}
}

if(isset($_POST['submit_btn']) && !empty($_POST['submit_btn'])){
	validate();
	if(empty($error)){
		$sql = "UPDATE tournament_polls SET poll_question='".trim($_POST['poll_question'])."',tournament_id=".$tournament_id.", is_active=".$_POST['is_active'].", ";
		for($i=1;$i<=6;$i++){
			$sql.="poll_option_$i='".$_POST['poll_option_'.$i]."', ";
		}
		
		$sql = rtrim($sql,", ");
		$sql.=" WHERE id = $poll_id ";
		
		if(mysqli_query($conn,$sql)){
			$_SESSION['poll_updated'] = 1;
			header("Location:".WWW."tournament/polls/list/".$tournament_id);
			exit();
		}else{
			$error = '<p id="error" class="alert alert-danger">Error in updating Poll. Try again later</p>';
		}
	}
}

function validate(){
	global $error;
	if(empty($_POST['poll_question'])){
		$error.= '<p id="error" class="alert alert-danger">Poll Question is required field</p>';
	}
	
	if(empty($_POST['poll_option_1']) || empty($_POST['poll_option_2'])){
		$error.= '<p id="error" class="alert alert-danger">At least two poll options are required</p>';
	}
}

?>
<?php include('common/header.php'); ?>


<div class="page-container"> 
		<?php  include('common/user-left-panel.php');?>
      <!-- END SIDEBAR -->
      
      <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
		<div class="page-content">
		<?php  include('common/breadcrumbs.php');?>
		
		    <div class="white-box">
            <div class="row">
              <div class="col-sm-12">
                <h2> Edit Poll </h2>
                <h3><?php echo ucwords($tournament_info['title']); ?></h3>
              </div>
            </div>
           
            <div class="row">
              <div class="col-sm-12">
                <h3>Poll Details</h3>
				<?php if(empty($user_info)): ?>
					<div id="error" class="alert alert-danger">You are not logged... !</div>
				<?php endif; ?>
				
				<?php if(!empty($error)): ?>
					<?php echo $error; ?>
				<?php endif; ?>
              </div>
            </div>
			
			<?php if($tournament_permission){ ?>
            <div class="row">
              <div class="col-sm-12">
                <form method="post" action="" class="form-horizontal">
                  <input name="action" value="submit" type="hidden">
                  
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Poll Question*</label>
                    <div class="col-sm-7">
                      <input class="form-control" name="poll_question" id="poll_question" value="<?php if(!empty($_POST['poll_question'])) echo $_POST['poll_question'];else echo $poll_info['poll_question']; ?>" type="text">
                    </div>
                  </div>
				  <?php for($i=1;$i<=6;$i++): ?>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Option <?php echo $i; ?></label>
                    <div class="col-sm-7">
                      <input class="form-control" name="poll_option_<?php echo $i; ?>" id="poll_option_<?php echo $i; ?>" value="<?php if(!empty($_POST['poll_option_'.$i])) echo $_POST['poll_option_'.$i];else echo $poll_info['poll_option_'.$i] ?>" type="text">
					  &nbsp; <?php if(!empty($poll_info['poll_option_'.$i])): ?> <?php echo $poll_info['votes_'.$i];  ?> Votes<?php endif; ?>
                    </div>
                  </div>
				  <?php endfor; ?>
				  
				  <div class="form-group">
                    <label class="col-sm-5 control-label">Status</label>
                    <div class="col-sm-7">
						<select name="is_active" id="is_active" class="control-label">
							<option value="1" <?php if($poll_info['is_active'] == 1) echo 'selected'; ?>>Active</option>
							<option value="0" <?php if($poll_info['is_active'] == 0) echo 'selected'; ?>>Inactive</option>
						</select>
                    </div>
                  </div>
                  
                  
                  <div class="form-group">
                    <div class="col-sm-offset-5 col-sm-7">
						<input name="submit_btn" value=" Submit " class="btn orange hvr-float-shadow" type="submit">
						<input name="cancel_btn" value=" Cancel " class="btn blue hvr-float-shadow" onclick="window.location.href='<?php echo WWW; ?>tournament/polls/list/<?php echo $tournament_id; ?>';" type="button">
                      
                    </div>
                  </div>
                </form>
              </div>
            </div>
			<?php }else{ ?>
				<div id="error" class="alert alert-danger">You do not have Permission for this Tournament... !</div>
			<?php } ?>
          </div>
		
		</div>
	</div>
</div>		
	
<?php include('common/footer.php'); ?>