<?php include('common/header.php'); ?>
<style>.white-box li{list-style:square;} .white-box ul li::before{content:'';}</style>
<div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1> Quick Tour </h1>
		  <? $row = get_record_on_id('cms',27);?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <div class="white-box">
		  
			<?=$row['content']?>
		    <div class="clear"><br/></div>
		    <h2>With Features including:</h2>
			<h3>Interactive Status Page</h3>
			<ul>
				<li>After Logged In -> <a title="Visit Now" href="<?=WWW;?>dashboard.html">Dashboard</a></li>
				<li>After Logged In -> Left Panel -> My Dashboard -> <a title="Visit Now" href="<?=WWW;?>dashboard.html">My Status/Activity</a></li>
			</ul>
			<img src="<?=WWW;?>whats-new/interactive-status-page.png" class="img-responsive">
			
			<div class="clear"><br/><br/></div>

			<h3>Invite Friends</h3>
			<ul>
				<li>After Logged In -> Dashboard -> <a title="Visit Now" href="<?=WWW;?>invite-friends.html">Invite Friends</a></li>
				<li>After Logged In -> Left Panel -> My Network -> <a title="Visit Now" href="<?=WWW;?>invite-friends.html">Invite Friends</a></li>
			</ul>
			<img class="img-responsive"  src="<?=WWW;?>whats-new/invite-friend.png" >

			<div class="clear"><br/><br/></div>
			
			<h3>Build Network of Friends</h3>
			<ul>
				<li>After Logged In -> Dashboard -> <a title="Visit Now" href="<?=WWW;?>invite-friends.html">Invite Friends</a></li>
				<li>After Logged In -> Left Panel -> My Network -> <a title="Visit Now" href="<?=WWW;?>invite-friends.html">Invite Friends</a></li>
				<li>After Logged In -> Left Panel -> My Network -> <a title="Visit Now" href="<?=WWW;?>my-network.html">My Friends</a></li>
				<li>After Logged In -> Left Panel -> My Network -> <a title="Visit Now" href="<?=WWW;?>pending-requests.html">Pending Requests</a></li>
				<li>After Logged In -> Left Panel -> My Network -> <a title="Visit Now" href="<?=WWW;?>network-notifications.html">Network Notifications</a></li>
			</ul>
			<img class="img-responsive"  src="<?=WWW;?>whats-new/build-network-of-friends.png" >

			<div class="clear"><br/><br/></div>
			
			<h3>Love it, Comment, Share Statuses</h3>
			<ul>
				<li>After Logged In -> <a title="Visit Now" href="<?=WWW;?>dashboard.html">Dashboard</a></li>
			</ul>
			<img class="img-responsive"  src="<?=WWW;?>whats-new/love-it-comment-share-statuses.png" >
			
			<div class="clear"><br/><br/></div>

			<h3>Attach Photos/Video in Status</h3>
			<ul>
				<li>After Logged In -> <a title="Visit Now" href="<?=WWW;?>dashboard.html">Dashboard</a></li>
			</ul>
			<img class="img-responsive"  src="<?=WWW;?>whats-new/attach-photos-video-in-status.png" >
			
			<div class="clear"><br/><br/></div>

			<h3>Photo Albums	</h3>
			<ul>
				<li>After Logged In -> Left Panel -> My Albums -> <a title="Visit Now" href="<?=WWW;?>my-photos.html">My Photos</a></li>
			</ul>
			<img class="img-responsive"  src="<?=WWW;?>whats-new/photo-album.png" >
			
			<div class="clear"><br/><br/></div>

			<h3>Video Albums</h3>	
			<ul>
				<li>After Logged In -> Left Panel -> My Albums -> <a title="Visit Now" href="<?=WWW;?>my-videos.html">My Videos</a></li>
			</ul>
			<img class="img-responsive"  src="<?=WWW;?>whats-new/video-album.png" >

			<div class="clear"><br/><br/></div>
			
			<h3>Referral Points</h3>
			<ul>
				<li>After Logged In -> Left Panel -> My Dashboard -> <a title="Visit Now" href="<?=WWW;?>account-information.html">Account Information</a></li>
			</ul>
			<img class="img-responsive"  src="<?=WWW;?>whats-new/referral-points.png" >

			<div class="clear"><br/><br/></div>
			
			<h3>Blogs, Forums & Groups</h3>
			<ul>
				<li>Top Menu -> <a title="Visit Now" href="<?=WWW;?>community.html">Community</a></li>
			</ul>
			<img class="img-responsive"  src="<?=WWW;?>whats-new/blog-forums-groups.png" >

			<div class="clear"><br/><br/></div>
			
			<h3>Events & Testimonials</h3>
			<ul>
				<li>Top Menu -> <a title="Visit Now" href="<?=WWW;?>community.html">Community</a></li>
			</ul>
			<img class="img-responsive"  src="<?=WWW;?>whats-new/events-testimonials.png" >

			<div class="clear"><br/><br/></div>
			
			<h3>In Your Region</h3>
			<ul>
				<li>Top Menu -> <a title="Visit Now" href="<?=WWW;?>in-your-region.html">In Your Region</a></li>
			</ul>
			<img class="img-responsive"  src="<?=WWW;?>whats-new/in-your-region.png" >

			<div class="clear"><br/><br/></div>
			
			<h3>Register/Login with Facebook</h3>
			<ul>
				<li>Header Part -> <a title="Visit Now" href="<?=WWW;?>login.html">Login</a></li>
			</ul>
			<img class="img-responsive"  src="<?=WWW;?>whats-new/register-login-with-facebook.png" >

			<div class="clear"><br/><br/></div>
			
			<h3>Register as Individual	</h3>
			<ul>
				<li>Header Part -> <a title="Visit Now" href="<?=WWW;?>sign-up-individual.html">Sign Up Individual</a></li>
			</ul>
			<img class="img-responsive"  src="<?=WWW;?>whats-new/register-as-individual.png" >

			<div class="clear"><br/><br/></div>
			
			<h3>Register as Company/Club/League</h3>
			<ul>
				<li>Header Part -> <a title="Visit Now" href="<?=WWW;?>sign-up-company.html">Sign Up Company</a></li>
			</ul>
			<img class="img-responsive"  src="<?=WWW;?>whats-new/register-as-company-club-league.png" >

			<div class="clear"><br/><br/></div>
			
			<h3>Messaging System	</h3>
			<ul>
				<li>After Logged In -> Left Panel -> My Messages -> <a title="Visit Now" href="<?=WWW;?>compose.html">Compose Message</a></li>
				<li>After Logged In -> Left Panel -> My Messages -> <a title="Visit Now" href="<?=WWW;?>inbox.html">Inbox</a></li>
				<li>After Logged In -> Left Panel -> My Messages -> <a title="Visit Now" href="<?=WWW;?>outbox.html">Outbox</a></li>
				<li>After Logged In -> Left Panel -> My Messages -> <a title="Visit Now" href="<?=WWW;?>deleted.html">Delete Messages</a></li>
			</ul>
			<img class="img-responsive"  src="<?=WWW;?>whats-new/messaging-system.png" >

			<div class="clear"><br/><br/></div>
			
			<h3>Make & Print your ID Card	</h3>
			<ul>
				<li>After Logged In -> Left Panel -> My Dashboard -> <a title="Visit Now" href="<?=WWW;?>identity-card.html">Identity Card</a></li>
			</ul>
			<img class="img-responsive"  src="<?=WWW;?>whats-new/make-print-your-id-card.png" >

			<div class="clear"><br/><br/></div>
			
			<h3>Privacy Settings</h3>
			<ul>
				<li>After Logged In -> Left Panel -> My Dashboard -> <a title="Visit Now" href="<?=WWW;?>privacy-settings.html">Privacy Settings</a></li>
			</ul>
			<img class="img-responsive"  src="<?=WWW;?>whats-new/privacy-settings.png" >
		  
			<div class="clear"><br/><br/></div>
		  
		  </div>
        </div>
        
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
				<?php include('common/right-panel.php');?>
            </div>
          </div>
          
        </div>
      </div>
</div><!-- /.container -->	

<?php include('common/footer.php'); ?>