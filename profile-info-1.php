<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? }

if($_SESSION['delete_photo'] == "Del"){

	echo '<div id="success" class="alert alert-success"><b>Success : </b>Photo Deleted ... !</div><br><br>';
	unset($_SESSION['delete_photo']);
}
?>
<?php 	/*	Profile Photo code			*/
		$user_id = $row_user['id'];
		$f_name  = $row_user['f_name'];

			if(isset($_POST) && !empty($_POST['update_profile_photo'])){
				$filename = $_FILES['photo']['name'];
				$ext = pathinfo($filename, PATHINFO_EXTENSION);
				if($ext=='jpg' || $ext=='png' || $ext=='jpeg'){
					if(!empty($_FILES['photo']['name'])){

						if(!is_dir('users/'.$user_id)){
							mkdir('users/'.$user_id,0777);
						}
						chmod('users/'.$user_id,0777);
						if(!is_dir('users/'.$user_id.'/photos')){
							mkdir('users/'.$user_id.'/photos',0777);
						}
						chmod('users/'.$user_id.'/photos',0777);

						$filename 	= friendlyURL($f_name).'.jpg';
						$image 		= new SimpleImage();
						$image->load($_FILES["photo"]["tmp_name"]);
						$image->save('users/'.$user_id.'/photos/'.$filename);
						chmod('users/'.$user_id.'/photos/'.$filename,0777);

						$rs_photos = mysqli_query($conn,"select * from photos where entity_id = '".$user_id."' and entity_type = 'users' and is_default = '1'");
						if(mysqli_num_rows($rs_photos) > 0){
							mysqli_query($conn,"update photos set file_name = '$filename' where entity_id = '$user_id' and entity_type = 'users' and is_default = '1'");
						}else{
							mysqli_query($conn,"insert into photos (file_name, title, entity_id, entity_type, is_default) values ('$filename','$f_name','$user_id','users','1')");
						}
					}else{
						$row_photo_user = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$user_id."' and is_default = '1'"));
						$row_photo_id   = $row_photo_user['id'];
						mysqli_query($conn,"update photos set is_default = '0' where id = '".$row_photo_id."'");
						mysqli_query($conn,"update photos set is_default = '1' where id = '".$_POST['album_photo']."'");
					}

					echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
					echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=1"</script>';
				}else{
					echo '<div id="error" class="alert alert-danger"><b>Error : </b>Invalid file type...!</div><br><br>';
				}

			}
			$row_photo = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$user_id."' and is_default = '1' order by id desc limit 1"));

			?>


			<? /*	Profile Video code			*/
			$user_id = $row_user['id'];//var_dump($_POST);exit;
			if($_POST['album_video'] && !empty($_POST['album_video'])){
				$vid_id = $_POST['album_video'];

				$sql = mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM videos WHERE entity_id = '$user_id'and entity_type = 'users' and is_default = '1' "));
				$current_id = $sql['id'];

				$profile_video = mysqli_query($conn,"update videos set is_default = '0',profile_video = '0' WHERE id = '$current_id' ");
					if($profile_video){
				mysqli_query($conn,"update videos set is_default = '1',profile_video = '1' WHERE id = '$vid_id' ");
					}

			}

			if(isset($_POST) && !empty($_POST['update_profile_video'])){
			  if($_POST['video'] == 'embed'){
			  if(!empty($_POST['video_code'])){
					$rs_check_code = mysqli_query($conn,"select * from videos where entity_type = 'users' and entity_id = '".$row_user['id']."' and is_default = '1'");
					if(mysqli_num_rows($rs_check_code) == 0){
						mysqli_query($conn,"insert into videos (file_name, status, entity_id, entity_type, is_default)
						values ('".$_POST['video_code']."','1','".$row_user['id']."','users','1');");
					}else{
						$row_video = mysqli_fetch_assoc($rs_check_code);
						mysqli_query($conn,"update videos set file_name = '".$_POST['video_code']."' where id = '".$row_video['id']."'");
						echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
					}
				}
			  }else{
			  	if(!empty($_FILES['video_file']['name'])){
			  		if($_FILES['video_file']['type'] == 'video/mp4'
			  		|| $_FILES['video_file']['type'] == 'video/webm'
			  		|| $_FILES['video_file']['type'] == 'video/ogg'
			  		|| $_FILES['video_file']['type'] == 'video/mov'
			  		|| $_FILES['video_file']['type'] == 'video/quicktime'){
			  			$exte = explode('.', $_FILES['video_file']['name']);
			  			$ext  = $exte[count($exte)-1];
			  			$filename 	= friendlyURL($_FILES['video_file']['name']).'.'.$ext;
						$rs_check   = mysqli_query($conn,"select * from videos where entity_type = 'users' and entity_id = '".$row_user['id']."' and file_name = '".$filename."'");
						mysqli_query($conn,"insert into videos (file_name, status, entity_id, entity_type, is_default)
						values ('".$filename."','1','".$row_user['id']."','users','1');");
						$video_id = mysqli_insert_id($conn);
						mysqli_query($conn,"update videos set is_default = '0' where entity_type = 'users' and entity_id = '".$row_user['id']."' and id <> '$video_id'");
						mkdir('videos/'.$video_id,0777);
						chmod('videos/'.$video_id,0777);
						move_uploaded_file($_FILES['video_file']['tmp_name'], 'videos/'.$video_id.'/'.$filename);
						chmod('videos/'.$video_id.'/'.$filename,0777);
						echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
			  		}else{
			  			echo '<div id="error" class="alert alert-danger"><b>Failure : </b>Invalid format ... !</div><br><br>';
			  		}
				}
			  }
			  echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=2"</script>';
			}
			$row_video = mysqli_fetch_assoc(mysqli_query($conn,"select * from videos where entity_id = '$user_id' and entity_type = 'users' and is_default = '1'"));
			if(preg_match('/<iframe(.*)<\/iframe>/', $row_video['file_name'])){
				$embed = 1;
			}else{
				$embed = 0;
			}

			/* Step 3 Location code		*/
			if(isset($_POST['update_location']) && !empty($_POST['update_location'])){
				//print_r($_POST); exit;
				$state_id 		= $_POST['state_id'];
				$city_name		= $_POST['city_name'];
				$zipcode 		= $_POST['zipcode'];
				$streetaddress 	= $_POST['streetaddress'];
				mysqli_query($conn,"update users set post_code = '$zipcode', state_id = '$state_id',city_name = '$city_name',street_address='$streetaddress'  where id = '$user_id'");
				echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br>';
				echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=3"</script>';
			}
			$row_country = get_record_on_id('countries', $row_user['country_id']);
			$row_city 	 = get_record_on_id('cities', $row_user['city_id']) ;
			$row_state 	 = get_record_on_id('states', $row_city['state_id']);

			// To Get User's State And City if he already has submited...
			$state          = mysqli_query($conn,"SELECT state_id,city_name FROM users WHERE id = '$user_id'");
			$user_state     = mysqli_fetch_assoc($state);
			$user_state_id  = $user_state['state_id'];
			$city_name      =$user_state['city_name'];

			/* Step 4 Individual traits code		*/

			if(isset($_POST['update_individual_traits']) && !empty($_POST['update_individual_traits'])){
				$set = array();
				$set[] = "type 		= '".$_POST['player_type']."'";
				$set[] = "first_class_player = '".$_POST['first_class_player']."'";
				$set[] = "match_types = '".implode(',',$_POST['matchType'])."'";
				$set[] = "game_types = '".implode(',',$_POST['gameType'])."'";

				if($user_type_id == '17'){
					$set[] = "is_company = '1'";
				}else{
					$set[] = "is_company = '0'";
				}

				if($player_type == 'Batsman'){ // batsman
					$batsman_type = $_POST['batsman_type'];
					$batting_order= $_POST['batting_order'];

					$set[] 	= "type_of_batsman= '".$batsman_type."'";
					$set[] 	= "batting_order= '".$batting_order."'";
				}else if($player_type == 'Bowler'){
					$bowler_type  = $_POST['bowler_type'];
					$bowling_arm  = $_POST['bowling_arm'];
					$bowling_prefer=$_POST['bowling_prefer'];

					$set[] 	= "type_of_bowler= '".$bowler_type."'";
					$set[] 	= "which_arm_bowler= '".$bowling_arm."'";
					$set[] 	= "bowl_between_overs= '".$bowling_prefer."'";
				}

				if($_POST['first_class_player'] == '1'){

					if(!empty($_POST['espn'])){
						$link = '';
						$pattern = "!^https?://(?:[a-zA-Z0-9-]+\.)*espncricinfo\.com(?:/[^#]*(?:#[^#]+)?)?$!"; //setting pattern
						preg_match($pattern, trim($_POST['espn']), $matches);
						// we need to get user link if he has already posted ....
						$user_espn_link = mysqli_query($conn,"SELECT espncrickinfo FROM users WHERE id = '".$row_user['id']."'");
						$user_link = mysqli_fetch_assoc($user_espn_link);
						preg_match($pattern, trim($user_link['espncrickinfo']), $user_link_match);
						if($user_link_match){
							$link = $user_link['espncrickinfo'];
						}
						///
						if($matches){
							$link = $_POST['espn'];
						}else {
							echo '<div id="error" class="alert alert-danger"><b> Error : </b> Not a Valid Espncricinfo link ... !</div><br><br>';
						}
					}
				}

				$set[] 	= "espncrickinfo= '".$link."'";

				$query  = "update users set ".implode(',',$set)." where id = '".$row_user['id']."'";
				mysqli_query($conn,$query);
				echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
				echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=4"</script>';
			}


			/*	Step 5 Current Affiliations Code	*/

			if(isset($_GET) && $_GET['action'] == 'delete' && $_GET['type'] == 'clubs'){
				mysqli_query($conn,"delete from users_to_clubs where id = '".$_GET['id']."'");
				echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
				echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=5"</script>';
			}
			if(isset($_POST) && $_POST['action'] == 'add-club'){
				$club 		= $_POST['club_name'];
				$sort 		= $_POST['sort'];
				$affiliation_date = trim($_POST['club_affiliation_time']);
				if(!empty($club)){
					$sql = "select * from users_to_clubs where user_id = '$user_id' and sort = '$sort'";
					if(mysqli_num_rows(mysqli_query($conn,$sql)) == 0){
						$date 		= date('Y-m-d H:i:s');
						$rs_clubs 	= mysqli_query($conn,"select * from clubs where title = '$club'");
						if(mysqli_num_rows($rs_clubs) == 0){
							mysqli_query($conn,"insert into clubs (title, status, create_date) values ('$club','1','$date');");
							$club_id = mysqli_insert_id($conn);
						}else{
							$row 	= mysqli_fetch_assoc($rs_clubs);
							$club_id= $row['id'];
						}
						$rs_chk = mysqli_query($conn,"select * from users_to_clubs where user_id = '$user_id' and club_id = '$club_id'");
						if(mysqli_num_rows($rs_chk) == 0){
							mysqli_query($conn,"insert into users_to_clubs (sort, user_id, club_id,affiliation_date,date_added) values ('$sort','$user_id','$club_id','$affiliation_date',now()); ");
							echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
							echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=5"</script>';
						}
					}else{
						echo '<div id="error" class="alert alert-danger"><b> Error : </b>This Preference already exists for clubs. Please chose a unique sort ... !</div><br><br>';
					}
				}
			}
			if(isset($_POST) && $_POST['action'] == 'club-update'){
				$club 		= $_POST['club_name'];
				$sort 		= $_POST['sort'];
				$affiliation_date = trim($_POST['club_affiliation_time']);
				if(!empty($club)){
					$usertoclub = $_POST['recordId'];
					$date 		= date('Y-m-d H:i:s');
					$sql = "select * from users_to_clubs where user_id = '$user_id' and sort = '$sort' and id!='$usertoclub' ";
					if(mysqli_num_rows(mysqli_query($conn,$sql)) == 0) {
						$rs_clubs 	= mysqli_query($conn,"select * from clubs where title = '$club'");
						if(mysqli_num_rows($rs_clubs) == 0){
							mysqli_query($conn,"insert into clubs (title, status, create_date) values ('$club','1','$date');");
							$club_id = mysqli_insert_id($conn);
						}else{
							$row 	= mysqli_fetch_assoc($rs_clubs);
							$club_id= $row['id'];
						}
						mysqli_query($conn,"update users_to_clubs set sort = '$sort', club_id = '$club_id',affiliation_date='$affiliation_date' where id = '$usertoclub'");
						echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
						echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=5"</script>';

					}else{
						echo '<div id="error" class="alert alert-danger"><b> Error : </b>This Preference already exists for clubs. Please chose a unique sort ... !</div><br><br>';
					}
				}

			}
			//leagues
			if(isset($_GET) && $_GET['action'] == 'delete' && $_GET['type'] == 'leagues'){
				mysqli_query($conn,"delete from users_to_leagues where id = '".$_GET['id']."'");
				echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
				echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=5"</script>';

			}
			if(isset($_POST) && $_POST['action'] == 'add-league'){
				$club 		= $_POST['league_name'];
				$sort 		= $_POST['sort'];
				$affiliation_date = trim($_POST['league_affiliation_time']);
				if(!empty($club)){
					$sql = "select * from users_to_leagues where user_id = '$user_id' and sort = '$sort'";
					if(mysqli_num_rows(mysqli_query($conn,$sql)) == 0){
						$date 		= date('Y-m-d H:i:s');
						$rs_clubs 	= mysqli_query($conn,"select * from leagues where title = '$club'");
						if(mysqli_num_rows($rs_clubs) == 0){
							mysqli_query($conn,"insert into leagues (title, status, create_date) values ('$club','1','$date');");
							$club_id = mysqli_insert_id($conn);
						}else{
							$row 	= mysqli_fetch_assoc($rs_clubs);
							$club_id= $row['id'];
						}
						$rs_chk = mysqli_query($conn,"select * from users_to_leagues where user_id = '$user_id' and league_id = '$club_id'");
						if(mysqli_num_rows($rs_chk) == 0){
							mysqli_query($conn,"insert into users_to_leagues (sort, user_id, league_id,affiliation_date,date_added) values ('$sort','$user_id','$club_id','$affiliation_date',now()); ");
							echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
							echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=5"</script>';
						}
					}else{
						echo '<div id="error" class="alert alert-danger"><b> Error : </b>This Preference already exists for Leagues. Please chose a unique sort ... !</div><br><br>';
					}
				}

			}
			if(isset($_POST) && $_POST['action'] == 'league-update'){
				$club 		= $_POST['league_name'];
				$sort 		= $_POST['sort'];
				$affiliation_date = trim($_POST['league_affiliation_time']);
				if(!empty($club)){
					$usertoclub = $_POST['edit_league_id'];
					$date 		= date('Y-m-d H:i:s');
					$sql = "select * from users_to_leagues where user_id = '$user_id' and sort = '$sort' and id!='$usertoclub' ";
					if(mysqli_num_rows(mysqli_query($conn,$sql)) == 0) {
						$rs_clubs 	= mysqli_query($conn,"select * from leagues where title = '$club'");
						if(mysqli_num_rows($rs_clubs) == 0){
							mysqli_query($conn,"insert into leagues (title, status, create_date) values ('$club','1','$date');");
							$club_id = mysqli_insert_id($conn);
						}else{
							$row 	= mysqli_fetch_assoc($rs_clubs);
							$club_id= $row['id'];
						}
						mysqli_query($conn,"update users_to_leagues set sort = '$sort', league_id = '$club_id',affiliation_date='$affiliation_date' where id = '$usertoclub'");
						echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
						echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=5"</script>';
					}else{
						echo '<div id="error" class="alert alert-danger"><b> Error : </b>This Preference already exists for Leagues. Please chose a unique sort ... !</div><br><br>'.$sql;
					}
				}

			}
			//agent
			if(isset($_GET) && $_GET['action'] == 'delete' && $_GET['type'] == 'agents'){
				$agent_id = $_GET['id'];

				mysqli_query($conn,"DELETE FROM agents WHERE id = '$agent_id'");
				echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
				echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=5"</script>';
			}
			if(isset($_POST) && $_POST['action'] == 'add-agent'){
				$club 		= $_POST['agent_name'];
				$sort 		= $_POST['sort'];
				$affiliation_date = trim($_POST['company_affiliation_time']);
			/*	if(!empty($club)){
					$agents = explode(',',$row_user['agent']);
					$agents[] = $club.'___'.$sort;
				}
				$agents = implode(',', $agents);
			*/
			//	mysqli_query($conn,"update users set agent = '$agents' where id = '".$row_user['id']."'");
				$check_sort = mysqli_query($conn,"SELECT * FROM agents WHERE sort = '$sort' AND user_id='".$row_user['id']."'");
				$sort_rows = mysqli_num_rows($check_sort);
				if($sort_rows > 0){
					echo '<div id="error" class="alert alert-danger"><b> Error : </b>This Sort is already exists Please chose a unique sort ... !</div><br><br>';
				}
				else  {
					mysqli_query($conn,"insert into agents (name, sort, user_id,affiliation_date,date_added) values ('$club',$sort,'".$row_user['id']."','$affiliation_date',now());");
					echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
					echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=5"</script>';
				}

			}
			if(isset($_POST) && $_POST['action'] == 'agent-update'){
				$agent 		= $_POST['agent_name'];
				$sort 		= $_POST['sort'];
				$agent_id   = $_POST['edit_agent_id'];
				$affiliation_date = trim($_POST['company_affiliation_time']);
				$sql = "select * from agents where user_id = '$user_id' and sort = '$sort' and id!='$agent_id' ";
				if(mysqli_num_rows(mysqli_query($conn,$sql)) == 0) {
					$query = "update agents set `name` = '$agent', `sort`='$sort',affiliation_date='$affiliation_date' where id = '$agent_id' AND user_id = '".$row_user['id']."'";
					mysqli_query($conn,$query);
					echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
					echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=5"</script>';
				}else{
					echo '<div id="error" class="alert alert-danger"><b> Error : </b>This Preference already exists for Company. Please chose a unique sort ... !</div><br><br>';
				}

			}


			if(isset($_POST) && !empty($_POST) && $_POST['action'] == 'update_overall_exp'){
				mysqli_query($conn,"update users set other_information = '".$_POST['overall_experience']."', currently_playing = '".$_POST['currently_playing']."', no_of_playing = '".$_POST['no_of_playing']."' where id = '$user_id'");
				echo '<div id="success"  class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
				echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=6"</script>';
			}
			if(isset($_GET) && $_GET['action'] == 'delete' && $_GET['type'] == 'experiences'){
				mysqli_query($conn,"delete from experiences where id = '".$_GET['id']."'");
				echo '<div id="success"  class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
				echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=6"</script>';
			}
			if(isset($_POST) && $_POST['action'] == 'add-exp'){
				$country_id 		= $_POST['country_id'];
				$organization 		= $_POST['organization'];
				$job_title 	 		= $_POST['job_title'];
				if(!empty($_POST['start_date'])){
				$from_date 	 		= $_POST['start_date'];
				}
				else {
					$from_date = date("Y-m-d");
				}

				if(!empty($_POST['end_date'])){
					$to_date 	 		= $_POST['end_date'];
				}
				else {
					$to_date = date("Y-m-d");
				}
				if($to_date < $from_date){
					echo '<div id="error"  class="alert alert-danger"><b>Error  : </b>To date should be less than From date ... !</div><br><br>';
				}
				else {
				if(!empty($country_id) && !empty($organization) && !empty($job_title)){
					$rs_chk = mysqli_query($conn,"select * from experiences where user_id = '$user_id' and country_id = '$country_id' and organization = '$organization' and job_title = '$job_title'");
						if(mysqli_num_rows($rs_chk) == 0){
							$query = "insert into experiences (user_id, country_id, organization, job_title, from_date, to_date)
								values ('$user_id','$country_id', '$organization', '$job_title','$from_date', '$to_date'); ";
							mysqli_query($conn,$query);
							echo '<div id="success"  class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
							echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=6"</script>';
						}
					}
				}
			}

			if(isset($_POST) && $_POST['action'] == 'edit-exp'){
				$organization 		= $_POST['organization'];
				$job_title 	 		= $_POST['job_title'];
				$from_date 	 		= $_POST['start_date'];
				$to_date 	 		= $_POST['end_date'];
				$id 				= $_POST['recordId'];
				if($to_date < $from_date){
						echo '<div id="error"  class="alert alert-danger"><b>Error  : </b>End/t date should be less than From date ... !</div><br><br>';
				}else {
					$query = "update experiences set organization = '$organization', job_title = '$job_title', from_date = '$from_date', to_date = '$to_date' where id = '$id'";
					mysqli_query($conn,$query);
					echo '<div id="success"  class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
					echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=6"</script>';
				}
			}


			if(isset($_POST) && $_POST['action'] == 'add-domestic'){
				$city_id		= $_POST['city_id'];
				$state_id   	= $_POST['state_id'];
				$city_name		= $_POST['city_name'];
				if(!empty($_POST['from_date'])){
					$from_date 	 		= $_POST['from_date'];
				}
				else {
					$from_date = date("Y-m-d");
				}

				if(!empty($_POST['to_date'])){
					$to_date 	 		= $_POST['to_date'];
				}
				else {
					$to_date = date("Y-m-d");
				}
				$date_from1 = strtotime($from_date);
				$date_to    = strtotime($to_date);

				if($date_from1 > $date_to) {

					echo "<div id='error' class='alert alert-danger'><b>Error : </b>Please Select Correct Date ... From Date should not be greater than to date  ... !</div><br><br>";
				}

				else {
				$rs_chk = mysqli_query($conn,"select * from opportunities where user_id = '$user_id' and type = 'domestic' and city_name = '$city_name' and state_id='$state_id'");
				if(mysqli_num_rows($rs_chk) == 0){
					$query = "insert into opportunities (user_id, type, country_id, city_name, from_date, to_date,state_id)
						values ('$user_id','domestic','".$row_user['country_id']."','$city_name','$from_date','$to_date','$state_id');";
					mysqli_query($conn,$query);
					echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
					echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=7"</script>';
				}

				}
			}
			if(isset($_POST) && $_POST['action'] == 'edit-domestic'){
				$state_id   	= $_POST['state_id'];
				$city_name      = $_POST['city_name'];
				$from_date		= $_POST['from_date'];
				$to_date		= $_POST['to_date'];			;
				$recordId 		= $_POST['recordId'];
				$date_from1 = strtotime($from_date);
				$date_to    = strtotime($to_date);

				if($date_from1 > $date_to) {

					echo "<div id='error' class='alert alert-danger'><b>Error : </b>Please Select Correct Date ... From Date should not be greater than to date  ... !</div><br><br>";
				}else {
					mysqli_query($conn,"update opportunities set city_name='$city_name',from_date = '$from_date', to_date = '$to_date',state_id='$state_id' where id = '$recordId'");
					echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
					echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=7"</script>';
				}
			}
			if(isset($_GET) && $_GET['action'] == 'delete' && $_GET['type'] == 'opportunities'){
				mysqli_query($conn,"delete from opportunities where id = '".$_GET['id']."'");
				echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
				echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=7"</script>';
			}
			if(isset($_POST) && $_POST['action'] == 'add-international'){

				$country_id		= $_POST['country_id'];
				$state_id		= $_POST['state']; //$_POST['state_id']
				$city_id		= $_POST['city_id'];
				$city_name		= $_POST['city_name'];
				if(!empty($_POST['from_date'])){
					$from_date 	 		= $_POST['from_date'];
				}
				else {
					$from_date = date("Y-m-d");
				}

				if(!empty($_POST['to_date'])){
					$to_date 	 		= $_POST['to_date'];
				}
				else {
					$to_date = date("Y-m-d");
				}

				$date_from1 = strtotime($from_date);
				$date_to    = strtotime($to_date);

				if($date_from1 > $date_to) {

					echo "<div id='error' class='alert alert-danger'><b>Error : </b>Please Select Correct Date ... From Date should not be greater than to date  ... !</div><br><br>";
				}else {
					//$rs_chk = mysqli_query($conn,"select * from opportunities where user_id = '$user_id' and type = 'international' and city_id = '$city_id'");

					$rs_chk = mysqli_query($conn,"select * from opportunities where user_id = '$user_id' and type = 'international' and city_name = '$city_name'");

					if(mysqli_num_rows($rs_chk) == 0){
						$query = "insert into opportunities (user_id, type, country_id, state_id, city_name, from_date, to_date)
							values ('$user_id','international','$country_id','$state_id','$city_name','$from_date','$to_date');";
						mysqli_query($conn,$query);
						echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
						echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=7"</script>';
					}
				}
			}

			if(isset($_POST) && $_POST['action'] == 'edit-international'){
				//print_r($_POST);
				$country_id		= $_POST['country_id'];
				$state_id		= $_POST['state']; //$_POST['state_id']

				$city_name		= $_POST['city_name'];
				$from_date		= $_POST['from_date'];
				$to_date		= $_POST['to_date'];			;
				$recordId 		= $_POST['recordId'];

				$date_from1 = strtotime($from_date);
				$date_to    = strtotime($to_date);

				if($date_from1 > $date_to) {
					echo "<div id='error' class='alert alert-success'><b>Error : </b>Please Select Correct Date ... From Date should not be greater than to date  ... !</div><br><br>";
				}else {
					mysqli_query($conn,"update opportunities set country_id='$country_id',state_id='$state_id',city_name='$city_name', from_date = '$from_date', to_date = '$to_date' where id = '$recordId'");
					echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
					echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=7"</script>';
				}
			}

		if(isset($_GET['cid']) && $_GET['action'] == 'delete_citizenship'){

			$cid = $_GET['cid'];
			if(!empty($cid)){
				$sql = mysqli_query($conn, "SELECT citizen FROM users WHERE id= '$user_id' ");
				$row = mysqli_fetch_assoc($sql);
				$countries = explode(',',$row['citizen']);
				for($i=0; $i< count($countries); $i++){
					if($countries[$i] == $cid){
						unset($countries[$i]);
					}
				}
			}

			$citizen = implode(',',$countries);
			$query  = "update users set citizen = '$citizen' where id = '$user_id'";
			$result = mysqli_query($conn,$query);
			if($result){
				echo '<div id="success" class="alert alert-success"><b>Success : </b>Deleted Successfuly ... !</div><br><br>';
				echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=8"</script>';
			}
		}

			if(isset($_GET) && $_GET['action'] == 'delete' && $_GET['type'] == 'travel_visas'){
				mysqli_query($conn,"delete from travel_visas where id = '".$_GET['id']."'");
				echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
				echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=8"</script>';
			}
			if(isset($_POST) && !empty($_POST['travel_details_submit']) ){
				$query = "update users set  passport = '".$_POST['passport']."', driving_license = '".$_POST['driving_license']."', passport_expiry = '".$_POST['passport_expiry']."', passport_weeks = '".$_POST['passport_weeks']."' where id = '$user_id'";
				mysqli_query($conn,$query);
				echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
				echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=8"</script>';
			}
			if(isset($_POST) && $_POST['action'] == 'city'){
				$query = "update users set citizen = '".implode(',',$_POST['citizen'])."'where id = '$user_id'";
				mysqli_query($conn,$query);
				echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
				echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=8"</script>';
			}
			if(isset($_POST) && $_POST['action'] == 'edit-travel'){
				$from_date		= $_POST['from_date'];
				$to_date		= $_POST['to_date'];			;
				$recordId 		= $_POST['recordId'];
				mysqli_query($conn,"update travel_visas set from_date = '$from_date', to_date = '$to_date' where id = '$recordId'");
				echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
				echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=8"</script>';
			}
			if(isset($_POST) && $_POST['action'] == 'add-travel'){
				$country_id		= $_POST['country_id'];
				if(!empty($_POST['from_date'])){
					$from_date 	 		= $_POST['from_date'];
				}
				else {
					$from_date = date("Y-m-d");
				}

				if(!empty($_POST['to_date'])){
					$to_date 	 		= $_POST['to_date'];
				}
				else {
					$to_date = date("Y-m-d");
				}

				if($to_date < $from_date) {
					echo '<div id="error" class="alert alert-danger"><b>Error : </b> To Date can not be less than from date ... !</div><br><br>';

				  }
				else {
					$rs_chk = mysqli_query($conn,"select * from travel_visas where user_id = '$user_id' and country_id = '$country_id'");
					if(mysqli_num_rows($rs_chk) == 0){
						mysqli_query($conn,"insert into travel_visas (user_id, country_id, from_date, to_date) values ('$user_id','$country_id','$from_date','$to_date');");
						echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
						echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=8"</script>';
					}
					else {
						$sql= mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM countries WHERE id = '$country_id'"));
						$contry_name = 	$sql['name'];
						echo "<div id='error' class='alert alert-danger'><b>Error : </b> Your Travel Visa information of ".$contry_name." already saved ... !</div><br><br>";
					}
				}
			}


			if(isset($_POST) && $_POST['action'] == 'update_social' ){
				$facebook 	= $_POST['facebook'];
				$twitter 	= $_POST['twitter'];
				$google_plus 	= $_POST['googleplus'];

				mysqli_query($conn,"update users set facebook = '$facebook', twitter = '$twitter',google_plus='$google_plus' where id = '".$user_id."'");
				echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
				echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=10"</script>';
			}

			if(isset($_POST) && $_POST['action'] == 'update_personality'){
				mysqli_query($conn,"update users set phone = '".$_POST['phone']."',  education = '".$_POST['education']."' where id = '$user_id'");
				echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
				echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=9"</script>';
			}

			$row_country = get_record_on_id('countries', $row_user['country_id']);

			if(isset($_POST) && $_POST['action'] == 'add-language'){
				$language 		= $_POST['langauge']				;
				$speaking 		= ($_POST['speaking'] == 'on')?'1':'0';
				$reading 		= ($_POST['reading'] == 'on')?'1':'0';
				$writing 		= ($_POST['writing'] == 'on')?'1':'0';
				$sort 			= $_POST['sort'];
				if(!empty($language)){
					$rs_chk = mysqli_query($conn,"select * from languages where user_id = '$user_id' and language = '$language'");
					if(mysqli_num_rows($rs_chk) == 0){
						mysqli_query($conn,"insert into languages (sort, user_id, language, speaking, reading, writing) values ('$sort','$user_id','$language','$speaking','$reading','$writing');");
						echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
						echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=9"</script>';
					}
				}
			}

			if(isset($_POST) && $_POST['action'] == 'edit-language'){
				$language 		= $_POST['langauge']				;
				$speaking 		= ($_POST['speaking'] == 'on')?'1':'0';
				$reading 		= ($_POST['reading'] == 'on')?'1':'0';
				$writing 		= ($_POST['writing'] == 'on')?'1':'0';
				$sort 			= $_POST['sort'];
				$recordId 		= $_POST['recordId'];
				if(!empty($language)){
					mysqli_query($conn,"update languages set language = '$language', speaking = '$speaking', reading = '$reading', writing = '$writing', sort = '$sort' where id = '$recordId'");
					echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
					echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=9"</script>';
				}
			}

			if(isset($_POST) && $_POST['action'] == 'add-cricketer'){

				$name 		= $_POST['name'];
				$sort		= $_POST['sort'];
				if(!empty($name)){
					$rs_chk = mysqli_query($conn,"select * from favorite_cricketers where user_id = '$user_id' and name = '$name'");
					if(mysqli_num_rows($rs_chk) == 0){
						mysqli_query($conn,"insert into favorite_cricketers (sort, user_id, name) values ('$sort','$user_id','$name');");
						echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
						echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=9"</script>';
					}
				}
			}

			if(isset($_POST) && $_POST['action'] == 'add-brand'){

				$name 		= $_POST['name'];
				$sort 		= $_POST['sort'];
				if(!empty($name)){
					$rs_chk = mysqli_query($conn,"select * from favorite_brands where user_id = '$user_id' and name = '$name'");
					if(mysqli_num_rows($rs_chk) == 0){
						mysqli_query($conn,"insert into favorite_brands (sort, user_id, name) values ('$sort','$user_id','$name');");
						echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
						echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=9"</script>';
					}
				}
			}

			if(isset($_GET) && $_GET['action'] == 'delete' && $_GET['type'] == 'educations'){
				mysqli_query($conn,"delete from educations where id = '".$_GET['id']."'");
				echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
				echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=9"</script>';
			}

			if(isset($_GET) && $_GET['action'] == 'delete' && $_GET['type'] == 'favorite_cricketers'){
				mysqli_query($conn,"delete from favorite_cricketers where id = '".$_GET['id']."'");
				echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
				echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=9"</script>';
			}
			if(isset($_GET) && $_GET['action'] == 'delete' && $_GET['type'] == 'favorite_brands'){
				mysqli_query($conn,"delete from favorite_brands where id = '".$_GET['id']."'");
				echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
				echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=9"</script>';
			}


			if(isset($_POST) && $_POST['action'] == 'add-education'){

				$degree 		= $_POST['degree']				;
				$institute 		= $_POST['institute'];
				$passing_year 	= $_POST['passing_year'];
				if(!empty($degree)){
					$rs_chk = mysqli_query($conn,"select * from educations where user_id = '$user_id' and degree = '$degree'");
					if(mysqli_num_rows($rs_chk) == 0){
						mysqli_query($conn,"insert into educations (user_id, degree, institute, passing_year) values ('$user_id','$degree','$institute','$passing_year');");
						echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
						echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=9"</script>';
					}
				}
			}

			if(isset($_POST) && $_POST['action'] == 'edit-cricketer'){
				$name 			= $_POST['name'];
				$sort 			= $_POST['sort'];
				$recordId 		= $_POST['recordId'];

				mysqli_query($conn,"update favorite_cricketers set sort = '$sort', name = '$name' where id = '$recordId'");
				echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
				echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=9"</script>';
			}

			if(isset($_POST) && $_POST['action'] == 'edit-brand'){
				$name 			= $_POST['name'];
				$sort 			= $_POST['sort'];
				$recordId 		= $_POST['recordId'];

				mysqli_query($conn,"update favorite_brands set sort = '$sort',  name = '$name' where id = '$recordId'");
				echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
				echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=9"</script>';
			}

			if(isset($_POST) && $_POST['action'] == 'edit-education'){
				$degree 		= $_POST['degree']				;
				$institute 		= $_POST['institute'];
				$passing_year 	= $_POST['passing_year'];
				$recordId 		= $_POST['recordId'];

				mysqli_query($conn,"update educations set degree = '$degree', institute = '$institute', passing_year = '$passing_year' where id = '$recordId'");
				echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
				echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=9"</script>';
			}
?>

 <style>
		.menu li {width:25%;}
		.menu li a{padding:0 10px;}
		@media only screen and (min-width:480px) {
			.menu li {width:20%;}
		}
		@media only screen and (min-width:768px) {
			.menu li {width:10%;}
		}
		.hide{display:none;}
		.datepicker{z-index:1151 !important;}
		.ui-autocomplete{z-index:1152 !important;}
	</style>

<div class="page-container">
	<?php include('common/user-left-panel.php');?>

	<!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">

        <div class="white-box_listting">

            <div class="row">
              <div class="col-sm-12">
                <h2 style="margin-top:0"> Profile Information </h2>
              </div>
            </div>
            <ul id="menu-individual-profile" class="menu" style="display:block; height: 30px;">
              <li><a href="#" id="step1-tab" >Step 1</a></li>
              <li><a href="#" id="step2-tab">Step 2</a></li>
              <li><a href="#" id="step3-tab">Step 3</a></li>
              <li><a href="#" id="step4-tab">Step 4</a></li>
              <li><a href="#" id="step5-tab">Step 5</a></li>
              <li><a href="#" id="step6-tab">Step 6</a></li>
              <li><a href="#" id="step7-tab">Step 7</a></li>
              <li><a href="#" id="step8-tab">Step 8</a></li>
              <li><a href="#" id="step9-tab">Step 9</a></li>
              <li><a href="#" id="step10-tab">Step 10</a></li>
            </ul>
            <div class="clearfix" style="height:10px;"></div>

            <div id="step1" class="content1" style="display:none;" >
              <h3> Profile Photo </h3>
              <form method="post" action="" enctype="multipart/form-data" id="profile-photo" class="form-horizontal">
                <input name="action" value="submit" type="hidden">
                <div class="form-group">
                  <label class="col-sm-4 control-label"> Current Photo </label>
                  <div class="col-sm-8">
                    <? if($row_photo){ ?>
						<div style="float:left;width:160px;">
						<a class="no-bg"  href="<?=WWW?>delete-user-profile-picture.php?id=<?php echo $row_photo['id'];?>">
						<img onclick="return confirm('Are you sure to delete Photo');" src="<?=WWW?>images/erase.png" border="0" style=" position:relative; left:170px;top:10px;z-index:1;" /></a>
						<img src="<?=WWW?>users/<?=$row_user['id']?>/photos/<?=$row_photo['file_name']?>" class="img-responsive">
						</div>
					<? }else{ ?>
						<img src="<?=WWW?>images/no-photo.png" class="img-responsive">
					<? } ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label"> Update Photo </label>
                  <div class="col-sm-4">
                    <input name="photo" accept="image/*" type="file">
                    <span class="help-block">
                      Max Photo size : 10MB &nbsp; &nbsp; &nbsp; <a target="_blank" href="multimedia-policy.html">Multimedia Policy</a>
                    </span>
                  </div>
                  <div class="col-sm-4">
                    <p class="form-control-static"> <a id="from-album" href="javascript:;" onclick="if($('#photos').hasClass('hide')) $('#photos').removeClass('hide'); else $('#photos').addClass('hide'); " class="btn orange full hvr-float-shadow"> From Album</a> </p>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-4 col-sm-8">
                    <input id="update" value="Update" name="update_profile_photo" class="btn orange hvr-float-shadow" type="submit">
                    <a id="cancel" class="btn blue hvr-float-shadow" href="dashboard.html">Cancel</a>
                  </div>
                </div>

				<div id="photos" class="hide">
					<div class="row">
						<? 	$rs_photo = mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$user_id."' and is_default <> '1'");
							if(mysqli_num_rows($rs_photo) > 0){
								while($row_photos = mysqli_fetch_assoc($rs_photo)){
								?>
								<div id="photo" class="col-sm-3 text-center">
									<img src="<?=WWW?>users/<?=$user_id?>/photos/<?=$row_photos['file_name']?>" style="max-height:180px;">
									<br>
									<center>
										<input type="radio" name="album_photo" value="<?=$row_photos['id']?>">
									</center>
								</div>
								<?
								}
							}else{
								echo '<div class="alert alert-warning" style="margin:15px;"  id="information"> <i class="fa fa-exclamation-triangle"> </i>Photo album is empty</div>';
							}
						?>
					</div>
				</div>

              </form>

            </div><!-- ./Step 1-->
            <div id="step2" class="content1" style="display:none;">
              <h3> Profile Video </h3>
              <form method="post" action="" enctype="multipart/form-data" class="form-horizontal">
                <input name="action" value="submit" type="hidden">
                <div class="form-group">
				 <label class="col-sm-2 control-label" style="line-height:40px;">Video Type</label>
                  <div class="col-sm-8">  <input type="radio" value="upload" name="video" <?=($embed == 0)?'checked="checked"':'';?>> Upload Video
                  <input type="radio" value="embed" name="video" <?=($embed == 1)?'checked="checked"':'';?>> Embed Video </div>

                </div>
				<div class="form-group">
                  <label class="col-sm-2 control-label"> Current Video <br/><br/>

				  <?php if($row_video['id']!=''){?>
					<a class="btn blue hvr-float-shadow" onclick='return confirm("Are you sure to delete 	Video");' href="<?=WWW?>delete-user-profile-video.php?id=<?php echo $row_video['id'];?>"><i class="fa fa-times"></i></a>
				  <?php } ?>

				  </label>
                  <div class="col-sm-8">
                    <?php $row_v = mysqli_fetch_assoc(mysqli_query($conn,"select * from videos where entity_type = 'users' and entity_id = '".$row_user['id']."' and is_default = '1'"));
						if($row_v){

								if(preg_match('/<iframe(.*)<\/iframe>/', $row_video['file_name'])){
									preg_match('/src="(.*?)"/',$row_video['file_name'] , $src);
									$src = $src[1];
									$video = "<iframe width='450' height='350' allowfullscreen src='$src'></iframe>";
								}else{
									$video_path = WWW.'videos/'.$row_v['id'].'/'.$row_v['file_name'];
									$video = '<video id="video1" width="450" height="350" controls preload="metadata">
										<source src="'.$video_path.'" type="video/mp4; codecs=\'avc1.42E01E, mp4a.40.2\'">
										<source src="'.$video_path.'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
									    <source src="'.$video_path.'" type=\'video/ogg; codecs="theora, vorbis"\'>
									    <source src="'.$video_path.'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
									    <source src="'.$video_path.'" type=\'video/webm; codecs="vp8, vorbis"\'>
										</video>';
								}
					?>

					<?php
					echo '<div class="embed-responsive embed-responsive-16by9">'.$video.'</div>';
					} else { ?>
						<img src="<?=WWW?>images/no-photo.png" class="img-responsive">
					<?php }?>

					</div>
                </div>
                <div class="form-group" id="upload-video">
                  <label class="col-sm-2 control-label"> Upload Video </label>
                  <div class="col-sm-4">
                    <input name="video_file" accept="video/*" type="file">
                    <span class="help-block">
                      Max Video size : 20MB &nbsp; &nbsp; &nbsp; <a target="_blank" href="<?=WWW?>multimedia-policy.html">Multimedia Policy</a>
                    </span>
                  </div>
                  <div class="col-sm-4">
                    <p class="form-control-static"> <a id="from-album" href="javascript:;" onclick="if($('#videos').hasClass('hide')) $('#videos').removeClass('hide'); else $('#videos').addClass('hide'); " class="btn orange full hvr-float-shadow"> From Album</a> </p>
                  </div>
                </div>
                <div class="form-group" id="embed-video">
                  <label class="col-sm-2 control-label"> Embed Video </label>
                  <div class="col-sm-8">
                    <textarea name="video_code" class="form-control" rows="5" placeholder="Please paste here YouTube/Vimeo/DailyMotion Embed Code"><?if(preg_match('/<iframe(.*)<\/iframe>/', $row_video['file_name'])){echo trim($row_video['file_name']);} ?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-2">
                    <input id="update" value="Update" name="update_profile_video" class="btn orange hvr-float-shadow" type="submit">
                    <a id="cancel" class="btn blue hvr-float-shadow" href="<?=WWW?>dashboard.html">Cancel</a>
                  </div>
                </div>


			  <div id="videos" class="hide">
				<div class="row">
						<? 	$rs_photo = mysqli_query($conn,"select * from videos where entity_type = 'users' and entity_id = '".$user_id."' and is_default  = '0' and id != '".$row_v['id']."'");
							if(mysqli_num_rows($rs_photo) > 0){
								while($row_photos = mysqli_fetch_assoc($rs_photo)){
								?>
								<div id="photo" class="col-sm-3 text-center">
									<?
											$video = $row_photos['file_name'];
											if(!empty($video)){
						if(preg_match('/<iframe(.*)<\/iframe>/', $row_photos['file_name'])){

									preg_match('/src="(.*?)"/',$row_photos['file_name'] , $src);
									$src = $src[1];
									$video = "<iframe width='205' height='130' frameborder='0' allowfullscreen
												src='$src'>
																	</iframe>";

									if(!preg_match('/height="(.*)"/', $video)){
										$video = preg_replace('/height="(.*)"/','height="130"',$video);
									}else{
										$video = preg_replace('/width="(.*)"/','width="220" height="150"',$video);
									}
						}else{

												$filename = explode('.',$video);
												$filename1= $filename[0];
												$video = '<video width="220" height="130" controls>
												  <source src="'.WWW.'videos/'.$row_photos['id'].'/'.$filename1.'.mp4" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_photos['id'].'/'.$filename1.'.ogg" type=\'video/ogg; codecs="theora, vorbis"\'>
												  <source src="'.WWW.'videos/'.$row_photos['id'].'/'.$filename1.'.mov" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_photos['id'].'/'.$filename1.'.webm" type=\'video/webm; codecs="vp8, vorbis"\'>
												</video>';
											}
										}else{
											$video = '<img src="'.WWW.'images/no-video.jpg" width="220" height="130">';
										}
										echo $video;?>
										<br/>
										<input type="radio" name="album_video" value="<?=$row_photos['id']?>">

								</div>
								<?
								}
							}else{
								echo '<div class="alert alert-warning" style="margin:15px;" id="information"> <i class="fa fa-exclamation-triangle"> </i>Video album is empty</div>';
							}
						?>
					</div>
					</div>
				</form>

            </div><!-- ./Step 2-->
            <div id="step3" class="content1" style="display:none;">
              <h3> Location Details </h3>
              <form method="post" action="" enctype="multipart/form-data" class="form-horizontal">
                <input name="action" value="submit" type="hidden">
                <div class="form-group">
                  <label class="col-sm-4 control-label"> Country </label>
                  <div class="col-sm-8">
                    <p class="form-control-static" style="line-height:34px;"><?=get_combo('countries','name',$row_user['country_id'],'','text')?></p>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label"> State </label>
                  <div class="col-sm-8">

					<select  name="state_id" class="form-control validate[required]">
						<option selected="selected" value=""></option>
						<? 	$rs_states = mysqli_query($conn,"select * from states where country_id = '".$row_user['country_id']."'");
							while($row_state_l = mysqli_fetch_assoc($rs_states)){ ?>
							<option <?=($user_state_id == $row_state_l['id'])?'selected="selected"':'';?> value="<?=$row_state_l['id']?>"><?=$row_state_l['name']?></option>
						<?  } ?>
					</select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label"> City </label>
                  <div class="col-sm-8">
                    <input  name="city_name" maxlength="25" value="<?php if($city_name){ echo $city_name;} ?>" class="form-control" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label"> Zip/Postal code </label>
                  <div class="col-sm-8">
                    <input name="zipcode" class="form-control <?=($row_user['country_id'] == '221' || $row_user['country_id'] == '222')?'validate[required]':'';?>"  maxlength="6" value="<?=$row_user['post_code']?>" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label"> Street Address </label>
                  <div class="col-sm-8">
					<? $stree =mysqli_fetch_assoc(mysqli_query($conn,"SELECT street_address FROM users WHERE id = '$user_id'"));?>
                    <textarea name="streetaddress" maxlength="170" class="form-control" rows="3"><?php if($row_user['street_address']){echo trim($row_user['street_address']);} ?></textarea>
                    <span class="help-block"><a href="#" onClick="javascript:void(0)" data-toggle="modal" data-target="#addressView">Why do we need your street address?</a></span>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-4 col-sm-8">
                    <input id="update" value="Update" name="update_location" class="btn orange hvr-float-shadow" type="submit">
                    <a id="cancel" class="btn blue hvr-float-shadow" href="<?=WWW?>dashboard.html">Cancel</a>
                  </div>
                </div>
              </form>
            </div><!-- ./Step 3-->
            <div id="step4" class="content1" style="display:none;">
              <h3> Individual Traits </h3>
              <form method="post" action="" enctype="multipart/form-data" class="form-horizontal">
                <input name="action" value="submit" type="hidden">
                <div class="form-group">
                  <label class="col-sm-4 control-label"> First Class Category </label>
                  <div class="col-sm-8">
					<?=get_first_class_player_combo($row_user['first_class_player'])?>
                  </div>
                </div>
                <div class="form-group" class="<?=($row_user['first_class_player'] != '1')?'hide':'';?>">
                  <label class="col-sm-4 control-label"> Paste your ESPNCRICINFO.com Link Here </label>
                  <div class="col-sm-8">
                    <textarea name="espn" class="form-control" rows="3"><?=(!empty($row_user['espncrickinfo']))?trim($row_user['espncrickinfo']):''?></textarea>
                  </div>
                </div>
				<?php $type = mysqli_query($conn,"SELECT user_type_id FROM users WHERE id = '$user_id'");
						 	$user_type_id = mysqli_fetch_assoc($type);
						 	$type_name = mysqli_query($conn,"SELECT *  FROM user_types WHERE id = '".$user_type_id['user_type_id']."'");
						 	$user_type = mysqli_fetch_assoc($type_name);
						 	if($user_type['name'] == "Player"){
				?>
                <div class="form-group">
                  <label class="col-sm-4 control-label"> Type of Player </label>
                  <div class="col-sm-8">

                    <select class="form-control validate[required]" name="player_type">
								<option value="" selected="selected">Select One</option>
								<option <?=($row_user['type'] == 'Batsman')?'selected="selected"':'';?> value="Batsman">Batsman</option>
								<option <?=($row_user['type'] == 'Bowler')?'selected="selected"':'';?> value="Bowler">Bowler</option>
								<option <?=($row_user['type'] == 'All Rounder')?'selected="selected"':'';?> value="All Rounder">All Rounder</option>
								<option <?=($row_user['type'] == 'Specialist Wicketkeeper')?'selected="selected"':'';?> value="Specialist Wicketkeeper">Specialist Wicketkeeper</option>
					</select>
                  </div>
                </div>
				<?php } ?>
                <div class="form-group <?=($row_user['type'] != 'Batsman')?'hide':'';?>" >
                  <label class="col-sm-4 control-label"> Type of Batsman </label>
                  <div class="col-sm-8">
					<select name="batsman_type" class="form-control" >
						<option <?=($row_user['type_of_batsman'] == 'Left-handed')?'selected="selected"':'';?> value="Left-handed">Left-handed</option>
						<option <?=($row_user['type_of_batsman'] == 'Right-handed')?'selected="selected"':'';?><?=(empty($row_user['type_of_batsman']))?'selected="selected"':'';?> value="Right-handed">Right-handed</option>
					</select>
                  </div>
                </div>
                <div class="form-group <?=($row_user['type'] != 'Batsman')?'hide':'';?>" >
                  <label class="col-sm-4 control-label"> Preferred Batting Order </label>
                  <div class="col-sm-8">
                    <select name="batting_order" class="form-control" >
						<option selected="selected" value="">- Select One -</option>
						<option <?=($row_user['batting_order'] == 'Top Order (1 - 3)')?'selected="selected"':'';?> value="Top Order (1 - 3)">Top Order (1 - 3)</option>
						<option <?=($row_user['batting_order'] == 'Middle Order (4 - 7)')?'selected="selected"':'';?> value="Middle Order (4 - 7)">Middle Order (4 - 7)</option>
						<option <?=($row_user['batting_order'] == 'Lower Order (8 - 11)')?'selected="selected"':'';?> value="Lower Order (8 - 11)">Lower Order (8 - 11)</option>
					</select>
                  </div>
                </div>
                <div class="form-group <?=($row_user['type'] != 'Bowler')?'hide':'';?>">
                  <label class="col-sm-4 control-label"> Type of Bowler </label>
                  <div class="col-sm-8">
                    <select name="bowler_type" class="form-control" >
						<option selected="selected" value="">- None -</option>
						<option <?=($row_user['type_of_bowler'] == 'Fast')?'selected="selected"':'';?> value="Fast">Fast</option>
						<option <?=($row_user['type_of_bowler'] == 'Medium-fast')?'selected="selected"':'';?> value="Medium-fast">Medium-fast</option>
						<option <?=($row_user['type_of_bowler'] == 'Leg Spinner')?'selected="selected"':'';?> value="Leg Spinner">Leg Spinner</option>
						<option <?=($row_user['type_of_bowler'] == 'Off Spinner')?'selected="selected"':'';?> value="Off Spinner">Off Spinner</option>
					</select>
                  </div>
                </div>
                <div class="form-group <?=($row_user['type'] != 'Bowler')?'hide':'';?>" >
                  <label class="col-sm-4 control-label"> With Which Arm do you Bowl? </label>
                  <div class="col-sm-8">
                    <select name="bowling_arm" class="form-control" >
						<option selected="selected" value="" >- None -</option>
						<option <?=($row_user['which_arm_bowler'] == 'Left')?'selected="selected"':'';?> value="Left">Left</option>
						<option <?=($row_user['which_arm_bowler'] == 'Right')?'selected="selected"':'';?> value="Right">Right</option>
					</select>
                  </div>
                </div>
                <div class="form-group <?=($row_user['type'] != 'Bowler')?'hide':'';?>" >
                  <label class="col-sm-4 control-label"> Your Bowling Overs Preference in One Day Games? </label>
                  <div class="col-sm-8">

					<select name="bowling_prefer" class="form-control" >
						<option selected="selected" value="">- None -</option>
						<option <?=($row_user['bowl_between_overs'] == '1 - 12 Overs')?'selected="selected"':'';?> value="1 - 12 Overs">1 - 12 Overs</option>
						<option <?=($row_user['bowl_between_overs'] == '13 - 20 Overs')?'selected="selected"':'';?> value="13 - 20 Overs">13 - 20 Overs</option>
						<option <?=($row_user['bowl_between_overs'] == '21 - 30 Overs')?'selected="selected"':'';?> value="21 - 30 Overs">21 - 30 Overs</option>
						<option <?=($row_user['bowl_between_overs'] == '31 - 40 Overs')?'selected="selected"':'';?> value="31 - 40 Overs">31 - 40 Overs</option>
						<option <?=($row_user['bowl_between_overs'] == '41 - 50 Overs')?'selected="selected"':'';?> value="41 - 50 Overs">41 - 50 Overs</option>
						<option <?=($row_user['bowl_between_overs'] == 'As Needed')?'selected="selected"':'';?> value="As Needed">As Needed</option>
					</select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label"> Preferred Game Type </label>
                  <div class="col-sm-8">
					<? $prefferedGameTypeData = explode(',',$row_user['game_types']);?>
					<? foreach($prefferedGameType as $ma){ ?>
                    <p><input style="float:left;" <?=(in_array($ma, $prefferedGameTypeData))?'checked="checked"':'';?> name="gameType[]" value="<?=$ma?>" type="checkbox">&nbsp; <?=$ma?></p>
                    <? } ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label"> Preferred Match Type </label>
                  <div class="col-sm-8">
					<? $matchArrayData = explode(',',$row_user['match_types']);?>
					<? foreach($matchArray as $ma){ ?>
                    <p><input style="float:left;" <?=(in_array($ma, $matchArrayData))?'checked="checked"':'';?> name="matchType[]" value="<?=$ma?>" type="checkbox">&nbsp; <?=$ma?></p>
                    <? } ?>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-4 col-sm-8">
                    <input id="update" value="Update" name="update_individual_traits" class="btn orange hvr-float-shadow" type="submit">
                    <a id="cancel" class="btn blue hvr-float-shadow" href="<?=WWW?>dashboard.html">Cancel</a>
                  </div>
                </div>
              </form>
            </div><!-- ./Step 4-->
            <div id="step5" class="content1" style="display:none;">
              <h3> Current Affiliations </h3>
              <form method="post" action="" enctype="multipart/form-data" class="form-horizontal">
                <input name="action" value="submit" type="hidden">
                <div class="row">
                  <div class="col-xs-10">
                    <h5>Your Club(s)</h5>
                  </div>
                  <div class="col-xs-2">
                    <a href="#" class="btn orange full hvr-float-shadow margin-bottom-10" onClick="javascript:void(0)"  data-toggle="modal" data-target="#addClub"> <i class="fa fa-plus"></i> </a>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="table-responsive">
                      <table class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Preference</th>
                            <th>Affiliation Date</th>
                            <th>Youcricketer Date &amp; Time</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
						<? 	$rs_msg = mysqli_query($conn,"select utc.*, c.title from users_to_clubs utc join clubs c on c.id = utc.club_id where utc.user_id = '".$row_user['id']."' order by utc.sort asc ");
			        	if(mysqli_num_rows($rs_msg) > 0){
						while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
                          <tr title="Click for detail">
                            <td><?=$row_msg['title']?></td>
				       			<td><?=$row_msg['sort']?></td>
								<td><?=$row_msg['affiliation_date']?></td>
								<td><?=$row_msg['date_added']?></td>
                            <td>
                              <a class="text-warning edit-affiliation" href="#edit-club" id="<?=$row_msg['id']?>" onClick="javascript:void(0)" data-toggle="modal" data-target="#editClub"><i class="fa fa-edit"></i></a>
                              <a class="text-danger" href="<?=WWW?>profile-information-step-1.html?action=delete&type=clubs&id=<?=$row_msg['id']?>"><i class="fa fa-times"></i></a>
                            </td>
                          </tr>
						<?  }
						} ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-xs-10">
                    <h5>Your League(s)</h5>
                  </div>
                  <div class="col-xs-2">
                    <a href="#" class="btn orange full hvr-float-shadow margin-bottom-10" onClick="javascript:void(0)" data-toggle="modal" data-target="#addLeague"> <i class="fa fa-plus"></i> </a>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="table-responsive">
                      <table class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Preference</th>
                            <th>Affiliation Date</th>
                            <th>Youcricketer Date &amp; Time</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
						<? 	$rs_msg = mysqli_query($conn,"select utc.*, c.title from users_to_leagues utc join leagues c on c.id = utc.league_id where utc.user_id = '".$row_user['id']."' order by utc.sort asc ");
			        	if(mysqli_num_rows($rs_msg) >0 ){
						while($row_msg = mysqli_fetch_assoc($rs_msg)){
			        		?>
                          <tr title="Click for detail">
							<td><?=$row_msg['title']?></td>
				       		<td><?=$row_msg['sort']?></td>
							<td><?=$row_msg['affiliation_date']?></td>
							<td><?=$row_msg['date_added']?></td>
                            <td>
                              <a class="text-warning edit-affiliation" href="#edit-league" id="<?=$row_msg['id']?>" onClick="javascript:void(0)" data-toggle="modal" data-target="#editLeague"><i class="fa fa-edit"></i></a>
                              <a class="text-danger" href="<?=WWW?>profile-information-step-1.html?action=delete&type=leagues&id=<?=$row_msg['id']?>"><i class="fa fa-times"></i></a>
                            </td>
                          </tr>
                        <?  } } ?>

                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-xs-10">
                    <h5>Your Company(s)</h5>
                  </div>
                  <div class="col-xs-2">
                    <a href="#" class="btn orange full hvr-float-shadow margin-bottom-10" onClick="javascript:void(0)" data-toggle="modal" data-target="#addCompany"> <i class="fa fa-plus"></i> </a>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="table-responsive">
                      <table class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Preference</th>
                            <th>Affiliation Date</th>
                            <th>Youcricketer Date &amp; Time</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
						<? 	$rs_msg = mysqli_query($conn,"SELECT * FROM agents WHERE user_id = '$user_id' ORDER by sort ASC ");
			        	if(mysqli_num_rows($rs_msg) > 0){
						while($row_agent  = mysqli_fetch_assoc($rs_msg)){ ?>
                          <tr title="Click for detail">
                            <td><?=$row_agent['name']?></td>
				       		<td><?=$row_agent['sort']?></td>
							<td><?=$row_agent['affiliation_date']?></td>
							<td><?=$row_agent['date_added']?></td>
                            <td>
                              <a class="text-warning edit-affiliation" href="#edit-agent" id="<?php echo $row_agent['id']."___".$row_agent['name']."___".$row_agent['sort'];?>" onClick="javascript:void(0)" data-toggle="modal" data-target="#editCompany"><i class="fa fa-edit"></i></a>
                              <a class="text-danger" href="<?=WWW?>profile-information-step-1.html?action=delete&type=agents&id=<?=$row_agent['id'] ?>"><i class="fa fa-times"></i></a>
                            </td>
                          </tr>
                        <?  }
				       	} ?>

                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12 text-center">
                    <a id="cancel" class="btn blue hvr-float-shadow" href="<?=WWW?>dashboard.html">Cancel</a>
                  </div>
                </div>
              </form>
            </div><!-- ./Step 5-->
            <div id="step6" class="content1" style="display:none;">
              <h3> Overall Experience </h3>
              <form method="post" action="" enctype="multipart/form-data" class="form-horizontal req-frm">
                <input name="action" value="update_overall_exp" type="hidden">
                <div class="form-group">
                  <label class="col-sm-4 control-label"> Currently Active in your individual role </label>
                  <div class="col-sm-8">
                    <select name="currently_playing" class="form-control">
						<option value="1" <?=($row_user['currently_playing'] == '1')?'selected="selected"':'';?>> Yes </option>
						<option value="0" <?=($row_user['currently_playing'] == '0')?'selected="selected"':'';?>> No </option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label"> Years of Experience </label>
                  <div class="col-sm-8">
                    <select name="no_of_playing" class="form-control validate[required]">
						<option <?=(empty($row_user['no_of_playing']))?'selected="selected"':'';?> value=""></option>
						<? for($x = 1; $x <= 100; $x++){ ?>
							<option <?=($x == $row_user['no_of_playing'])?'selected="selected"':'';?> value="<?=$x?>"><?=$x?></option>
						<? } ?>

                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label"> Describe your experience based on your listed individual role or any other roles you had </label>
                  <div class="col-sm-8">
                    <textarea name="overall_experience" class="form-control" rows="3"><?=$row_user['other_information']?></textarea>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-10">
                    <h5>Countries traveled related to your individual role or any type of work or education</h5>
                  </div>
                  <div class="col-xs-2">
                    <a href="#" class="btn orange full hvr-float-shadow margin-bottom-10" onClick="javascript:void(0)" data-toggle="modal" data-target="#addExp"> <i class="fa fa-plus"></i> </a>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="table-responsive">
                      <table class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                          <tr>
                            <th>Country</th>
                            <th>Organization</th>
                            <th>Job Title</th>
                            <th>From Date</th>
                            <th>To Date</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
							<? 	$rs_msg = mysqli_query($conn,"select * from experiences where user_id = '".$row_user['id']."' order by to_date desc ");
				        	if(mysqli_num_rows($rs_msg) > 0){
							while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
                          <tr title="Click for detail">
                            <td><?=get_combo('countries','name',$row_msg['country_id'],'','text')?></td>
					       	<td><?=$row_msg['organization']?></td>
					       	<td><?=$row_msg['job_title']?></td>
					       	<td><?=date_converter($row_msg['from_date'])?></td>
					       	<td><?=date_converter($row_msg['to_date'])?></td>
                            <td>
                              <a class="text-warning edit-experience" href="#edit-exp" id="<?=$row_msg['id']?>" onClick="javascript:void(0)" data-toggle="modal" data-target="#editExp"><i class="fa fa-edit"></i></a>
                              <a class="text-danger" onclick="return confirm('Are you sure to delete record');"  href="<?=WWW?>profile-information-step-1.html?action=delete&type=experiences&id=<?=$row_msg['id']?>"><i class="fa fa-times"></i></a>
                            </td>
                          </tr>
                          <?  }
					       	} ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-offset-4 col-sm-8">
                    <input id="update" value="Update " class="btn orange hvr-float-shadow" type="submit">
                    <a id="cancel" class="btn blue hvr-float-shadow" href="dashboard.html">Cancel</a>
                  </div>
                </div>
              </form>
            </div><!-- ./Step 6-->
            <div id="step7" class="content1" style="display:none;">
              <h3> Seeking Opportunities </h3>
              <form method="post" action="" enctype="multipart/form-data" class="form-horizontal">
                <input name="action" value="submit" type="hidden">
                <div class="form-group">
                  <label class="col-sm-4 control-label"> Actively Seeking Domestic Opportunity </label>
                  <div class="col-sm-8">
					<? $rs_msg = mysqli_query($conn,"select * from opportunities where user_id = '".$row_user['id']."' and type='domestic' order by to_date desc ");  ?>
                    <input name="domestic" <?=(mysqli_num_rows($rs_msg) > 0)?'checked="checked" disabled="disabled"':'';?> type="checkbox">
                  </div>
                </div>
                <div class="row domestic-detail <?=(mysqli_num_rows($rs_msg) > 0)?'':'hide';?>">
                  <div class="col-xs-10">
                    <h5>Domestic Availibility</h5>
                  </div>
                  <div class="col-xs-2">
                    <a href="#" class="btn orange full hvr-float-shadow margin-bottom-10" onClick="javascript:void(0)" data-toggle="modal" data-target="#add-domestic"> <i class="fa fa-plus"></i> </a>
                  </div>
                </div>
                <div class="row domestic-detail <?=(mysqli_num_rows($rs_msg) > 0)?'':'hide';?>">
                  <div class="col-sm-12">
                    <div class="table-responsive">
                      <table class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                          <tr>
                            <th>State</th>
                            <th>City</th>
                            <th>From Date</th>
                            <th>To Date</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
						<? 	if(mysqli_num_rows($rs_msg) > 0){
					        while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
                          <tr title="Click for detail">
                            <td><?=get_combo('states','name',$row_msg['state_id'],'','text')?> </td>
                            <td><?php echo $row_msg['city_name'];?></td>
                            <td><?=date_converter($row_msg['from_date'])?></td>
                            <td><?=date_converter($row_msg['to_date'])?></td>
                            <td>
                              <a class="text-warning edit-opportunities" href="#edit-domestic" id="<?=$row_msg['id']?>" onClick="javascript:void(0)" data-toggle="modal" data-target="#edit-domestic"><i class="fa fa-edit"></i></a>
                              <a class="text-danger" onclick="return confirm('Are you sure to delete record');"  href="<?=WWW?>profile-information-step-1.html?action=delete&type=opportunities&id=<?=$row_msg['id']?>"><i class="fa fa-times"></i></a>
                            </td>
                          </tr>
                          	<?  }
						       	} ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

				<? $rs_msg = mysqli_query($conn,"select * from opportunities where user_id = '".$row_user['id']."' and type = 'international' order by to_date desc");  ?>
                <div class="form-group">
                  <label class="col-sm-4 control-label"> Actively Seeking International Opportunity </label>
                  <div class="col-sm-8">
                    <input name="international" type="checkbox"  <?=(mysqli_num_rows($rs_msg) > 0)?'checked="checked" disabled="disabled"':'';?>>
                  </div>
                </div>
                <div class="row international-detail <?=(mysqli_num_rows($rs_msg) > 0)?'':'hide';?>">
                  <div class="col-xs-10">
                    <h5>International Availibility</h5>
                  </div>
                  <div class="col-xs-2">
                    <a href="#" class="btn orange full hvr-float-shadow margin-bottom-10" onClick="javascript:void(0)" data-toggle="modal" data-target="#add-international"> <i class="fa fa-plus"></i> </a>
                  </div>
                </div>

                <div class="row international-detail <?=(mysqli_num_rows($rs_msg) > 0)?'':'hide';?>">
                  <div class="col-sm-12">
                    <div class="table-responsive">
                      <table class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                          <tr>
                            <th>Country</th>
                            <th>State</th>
                            <th>City</th>
                            <th>From Date</th>
                            <th>To Date</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
						<? 	if(mysqli_num_rows($rs_msg) > 0){
					    while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
                          <tr title="Click for detail">
                            <td><?=get_combo('countries','name',$row_msg['country_id'],'','text')?></td>
                            <td><?=get_combo('states','name',$row_msg['state_id'],'','text')?> </td>
                            <td><?php echo  $row_msg['city_name']; ?></td>
                            <td><?=date_converter($row_msg['from_date'])?></td>
                            <td><?=date_converter($row_msg['to_date'])?></td>
                            <td>
                              <a class="text-warning edit-opportunities" href="#edit-international" id="<?=$row_msg['id']?>" onClick="javascript:void(0)" data-toggle="modal" data-target="#edit-international"><i class="fa fa-edit"></i></a>
                              <a class="text-danger" onclick="return confirm('Are you sure to delete record');" href="<?=WWW?>profile-information-step-1.html?action=delete&type=opportunities&id=<?=$row_msg['id']?>"><i class="fa fa-times"></i></a>
                            </td>
                          </tr>
                        <?  }
						} ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-offset-4 col-sm-8">
                    <!--<input id="update" value="Update " class="btn orange hvr-float-shadow" type="submit">-->
                    <a id="cancel" class="btn blue hvr-float-shadow" href="dashboard.html">Cancel</a>
                  </div>
                </div>
              </form>
            </div><!-- ./Step 7-->
            <div id="step8" class="content1" style="display:none;">
              <h3> Travel Details </h3>
              <form method="post" action="" enctype="multipart/form-data" class="form-horizontal">
                <input name="action" value="submit" type="hidden">
                <div class="row">
                  <div class="col-xs-10">
                    <h5>Your Citizenship</h5>
                  </div>
                  <div class="col-xs-2">
                    <a href="#" class="btn orange full hvr-float-shadow margin-bottom-10" onClick="javascript:void(0)" data-toggle="modal" data-target="#add-citizenship"> <i class="fa fa-plus"></i> </a>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="table-responsive">
                      <table class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                          <tr>
                            <th>Country</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
						<?php
				        $sql = mysqli_query($conn, "SELECT citizen FROM users WHERE id= '$user_id' ");
				        $row = mysqli_fetch_assoc($sql);
				        $countries = explode(',',$row['citizen']);
				        for($i = 0; $i< count($countries); $i++){
						$query    = mysqli_query($conn,"SELECT name FROM countries WHERE id = '$countries[$i]'");
						$country  = mysqli_fetch_assoc($query);
						if(!empty($country['name'])){
						?>
                          <tr title="Click for detail">
                            <td><?php echo $country['name']?></td>
                            <td> <a class="text-danger" onclick="return confirm('Are you sure to delete Record');" href="<?=WWW?>profile-information-step-1.html?cid=<?php echo $countries[$i];?>&action=delete_citizenship"><i class="fa fa-times"></i></a> </td>
                          </tr>
						<?  } }?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-4 control-label"> Do you have International Driving License </label>
                  <div class="col-sm-8">
                    <select name="driving_license" class="form-control">
						<option value="0" <?=($row_user['driving_license'] == '0' || empty($row_user['driving_license']))?'selected="selected"':'';?>>No</option>
						<option value="1" <?=($row_user['driving_license'] == '1')?'selected="selected"':'';?>>Yes</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label"> Do you have a valid Passport </label>
                  <div class="col-sm-8">
                    <select name="passport" id="passport_yes" class="form-control">
						<option value="0" <?=($row_user['passport'] == '0' || empty($row_user['passport']))?'selected="selected"':'';?>>No</option>
						<option value="1" <?=($row_user['passport'] == '1')?'selected="selected"':'';?>>Yes</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label"> Please enter Passport Expiry Date </label>
                  <div class="col-sm-8">
                    <input name="passport_expiry" value="<?php if($row_user['passport_expiry']){echo $row_user['passport_expiry'];}?>" id="dateSelect" class="form-control datepicker" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label"> Regular Passport Processing time in your Country </label>
                  <div class="col-sm-8">
                    <select name="passport_weeks" class="form-control">
						<? for($x = 1; $x <= 16; $x++){ ?>
							<option <?=($x == $row_user['passport_weeks'])?'selected="selected"':'';?> value="<?=$x?>"><?=$x?> Week<?=($x!=1)?'s':'';?></option>
						<? } ?>
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-10">
                    <h5>Travel Visa Available For</h5>
                  </div>
                  <div class="col-xs-2">
                    <a href="#" class="btn orange full hvr-float-shadow margin-bottom-10" onClick="javascript:void(0)" data-toggle="modal" data-target="#add-travel"> <i class="fa fa-plus"></i> </a>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="table-responsive">
                      <table class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                          <tr>
                            <th>Country</th>
                            <th>From Date</th>
                            <th>To Date</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
						<? 	$rs_msg = mysqli_query($conn,"select * from travel_visas where user_id = '".$row_user['id']."' order by to_date desc");
				        	if(mysqli_num_rows($rs_msg) >0){
							while($row_msg = mysqli_fetch_assoc($rs_msg)){
				        ?>
                          <tr title="Click for detail">
                            <td><?=get_combo('countries','name',$row_msg['country_id'],'','text')?></td>
					       	<td><?=date_converter($row_msg['from_date'])?></td>
					       	<td><?=date_converter($row_msg['to_date'])?></td>
                            <td>
                              <a class="text-warning edit-travel-details" href="#edit-travel" id="<?=$row_msg['id']?>" onClick="javascript:void(0)" data-toggle="modal" data-target="#edit-travel"><i class="fa fa-edit"></i></a>
                              <a class="text-danger" onclick="return confirm('Are you sure to delete record');" href="<?=WWW?>profile-information-step-1.html?action=delete&type=travel_visas&id=<?=$row_msg['id']?>"><i class="fa fa-times"></i></a>
                            </td>
                          </tr>
                        <?  }
					       	} ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-offset-4 col-sm-8">
                    <input id="update" value="Update" name="travel_details_submit" class="btn orange hvr-float-shadow" type="submit">
                    <a id="cancel" class="btn blue hvr-float-shadow" href="<?=WWW?>dashboard.html">Cancel</a>
                  </div>
                </div>
              </form>
            </div><!-- ./Step 8-->
            <div id="step9" class="content1" style="display:none;">
              <h3> Personality </h3>
              <form method="post" action="" id="step-9-form"  enctype="multipart/form-data" class="form-horizontal req-frm">
                <input name="action" value="update_personality" type="hidden">
                <div class="form-group">
                  <label class="col-sm-4 control-label"> Phone Number <a href="#" onClick="javascript:void(0)" data-toggle="modal" data-target="#phoneNumberView"><span class="tooltip_show" data-toggle="tooltip" title="Why we need your phone number?"><i class="fa fa-question"></i></span></a></label>
                  <div class="col-sm-8">
                    <input value="<?=$row_user['phone']?>" name="phone" class="form-control " type="text" placeholder="(Country Code)-5555555555" required number1='true'>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label"> Write About Yourself </label>
                  <div class="col-sm-8">
                    <textarea name="education" maxlength="1000" id="msg" class="form-control" required><?=$row_user['education']?></textarea>
                    <span class="help-block countdown" >1000 characters remaining.</span>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-10">
                    <h5>Education Details</h5>
                  </div>
                  <div class="col-xs-2">
                    <a href="#" class="btn orange full hvr-float-shadow margin-bottom-10" onClick="javascript:void(0)" data-toggle="modal" data-target="#add-education"> <i class="fa fa-plus"></i> </a>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="table-responsive">
                      <table class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                          <tr>
                            <th>Degree</th>
                            <th>Institute</th>
                            <th>Passing Year</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
						<? 	$rs_msg = mysqli_query($conn,"select e.* from educations e where e.user_id = '".$row_user['id']."' order by passing_year desc ");
							if(mysqli_num_rows($rs_msg) >0){
							while($row_msg = mysqli_fetch_assoc($rs_msg)){	?>
                          <tr title="Click for detail">
                            <td><?=$row_msg['degree']?></td>
				       		<td><?=$row_msg['institute']?></td>
				       		<td><?=$row_msg['passing_year']?></td>
                            <td>
                              <a class="text-warning personality-education" href="#edit-education" id="<?=$row_msg['id']?>" onClick="javascript:void(0)" data-toggle="modal" data-target="#edit-education"><i class="fa fa-edit"></i></a>
                              <a class="text-danger" onclick="return confirm('Are you sure to delete record');" href="<?=WWW?>profile-information-step-1.html?action=delete&type=educations&id=<?=$row_msg['id']?>"><i class="fa fa-times"></i></a>
                            </td>
                          </tr>
                        <? } } ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-10">
                    <h5>Language Familiarity</h5>
                  </div>
                  <div class="col-xs-2">
                    <a href="#" class="btn orange full hvr-float-shadow margin-bottom-10" onClick="javascript:void(0)" data-toggle="modal" data-target="#add-language"> <i class="fa fa-plus"></i> </a>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="table-responsive">
                      <table class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                          <tr>
                            <th>Language</th>
                            <th>Speaking</th>
                            <th>Reading</th>
                            <th>Writing</th>
                            <th>Preference </th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
						<? 	$rs_msg = mysqli_query($conn,"select e.* from languages e where e.user_id = '".$row_user['id']."' order by sort desc");
			        	if(mysqli_num_rows($rs_msg) > 0){
						while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
			        		<tr title="Click for detail">
                           <td><?=$row_msg['language']?></td>
				       		<td><?=($row_msg['speaking'] == '1')?'Yes':'No';?></td>
				       		<td><?=($row_msg['reading'] == '1')?'Yes':'No';?></td>
				       		<td><?=($row_msg['writing'] == '1')?'Yes':'No';?></td>
				       		<td><?=$row_msg['sort']?></td>
                            <td>
                              <a class="text-warning personality-education" href="#edit-language" id="<?=$row_msg['id']?>" onClick="javascript:void(0)" data-toggle="modal" data-target="#edit-language"><i class="fa fa-edit"></i></a>
                              <a class="text-danger" onclick="return confirm('Are you sure to delete record');" href="<?=WWW?>profile-information-step-1.html?action=delete&type=langauges&id=<?=$row_msg['id']?>"><i class="fa fa-times"></i></a>
                            </td>
                          </tr>
                        <?  } } ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-10">
                    <h5>Favorite Cricketer(s)</h5>
                  </div>
                  <div class="col-xs-2">
                    <a href="#" class="btn orange full hvr-float-shadow margin-bottom-10" onClick="javascript:void(0)" data-toggle="modal" data-target="#add-cricketer"> <i class="fa fa-plus"></i> </a>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="table-responsive">
                      <table class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Preference</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
						<? 	$rs_msg = mysqli_query($conn,"select e.* from favorite_cricketers e where e.user_id = '".$row_user['id']."' order by sort desc");
			        	if(mysqli_num_rows($rs_msg) > 0){
						while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
                          <tr title="Click for detail">
                            <td><?=$row_msg['name']?></td>
                            <td><?=$row_msg['sort']?></td>
                            <td>
                              <a class="text-warning personality-education" href="#edit-cricketer" id="<?=$row_msg['id']?>" onClick="javascript:void(0)" data-toggle="modal" data-target="#edit-cricketer"><i class="fa fa-edit"></i></a>
                              <a class="text-danger" onclick="return confirm('Are you sure to delete record');" href="<?=WWW?>profile-information-step-1.html?action=delete&type=favorite_cricketers&id=<?=$row_msg['id']?>"><i class="fa fa-times"></i></a>
                            </td>
                          </tr>
						  <?  } } ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-10">
                    <h5>Favorite Cricket Merchandise Brand(s)</h5>
                  </div>
                  <div class="col-xs-2">
                    <a href="#" class="btn orange full hvr-float-shadow margin-bottom-10" onClick="javascript:void(0)" data-toggle="modal" data-target="#add-brand"> <i class="fa fa-plus"></i> </a>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="table-responsive">
                      <table class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Preference</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
						<? 	$rs_msg = mysqli_query($conn,"select e.* from favorite_brands e where e.user_id = '".$row_user['id']."' order by sort desc");
			        	if(mysqli_num_rows($rs_msg) > 0){
			        	while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
                          <tr title="Click for detail">
                            <td><?=$row_msg['name']?></td>
                            <td><?=$row_msg['sort']?></td>
                            <td>
                              <a class="text-warning personality-education" href="#edit-brand" id="<?=$row_msg['id']?>" onClick="javascript:void(0)" data-toggle="modal" data-target="#edit-brand"><i class="fa fa-edit"></i></a>
                              <a class="text-danger" onclick="return confirm('Are you sure to delete record');" href="<?=WWW?>profile-information-step-1.html?action=delete&type=favorite_brands&id=<?=$row_msg['id']?>"><i class="fa fa-times"></i></a>
                            </td>
                          </tr>
                          <?  } } ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-offset-4 col-sm-8">
                    <input id="update" value="Update " class="btn orange hvr-float-shadow" type="submit">
                    <a id="cancel" class="btn blue hvr-float-shadow" href="dashboard.html">Cancel</a>
                  </div>
                </div>
              </form>
            </div><!-- ./Step 9-->
            <div id="step10" class="content1" style="display:none;">
              <h3> Social Media Details </h3>
              <form method="post" action="" enctype="multipart/form-data" class="form-horizontal req-frm">
                <input name="action" value="update_social" type="hidden">
                <div class="form-group">
                  <label class="col-sm-4 control-label"> Your Facebook Page Link </label>
                  <div class="col-sm-8">
                    <input name="facebook" value="<?=$row_user['facebook']?>" class="form-control validate[custom[url]]" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label"> Your Twitter Page Link </label>
                  <div class="col-sm-8">
                    <input name="twitter" value="<?=$row_user['twitter']?>" class="form-control validate[custom[url]]" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label"> Your Google Plus Link </label>
                  <div class="col-sm-8">
                    <input name="googleplus" value="<?=$row_user['google_plus']?>" class="form-control validate[custom[url]]" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-4 col-sm-8">
                    <input id="update" value="Update " class="btn orange hvr-float-shadow" type="submit">
                    <a id="cancel" class="btn blue hvr-float-shadow" href="<?=WWW?>dashboard.html">Cancel</a>
                  </div>
                </div>
              </form>
            </div><!-- ./Step 10-->
          </div>


        </div>
      </div>
      <!-- END CONTENT-->

</div>



 <div class="modal fade" id="addressView" tabindex="-1" role="dialog" aria-labelledby="addressViewModel" aria-hidden="true">
      <div class="modal-dialog" style="max-height:800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Why do we need your street Address </h4>
          </div>
          <div class="modal-body" style="max-height:700px">
            <div class="row">
              <div class="col-sm-9">
				<? $row = get_record_on_id('cms',19);?>
                <p><?=$row['content']?></p>
              </div>
            </div>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.addressView-Model -->

    <div class="modal fade" id="addClub" tabindex="-1" role="dialog" aria-labelledby="addClubModel" aria-hidden="true">
      <div class="modal-dialog" style="max-height:800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Add your Club</h4>
          </div>
          <div class="modal-body" style="max-height:700px">
            <form method="post" action="" class="form-horizontal req-frm">
              <input name="action" value="add-club" type="hidden">
              <div class="form-group">
                <label class="col-xs-5 control-label">Club Name</label>
                <div class="col-xs-7">
                  <input autocomplete="off" name="club_name" id="club_name_add" maxlength="54" class="form-control validate[required] ui-autocomplete-input" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Preferred Order</label>
                <div class="col-xs-7">
                  <input name="sort" class="form-control validate[required,[custom[integer]]" type="text">
                  <span class="help-block">1 is Highest Ranked</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Affiliation Date</label>
                <div class="col-xs-7">
                  <input name="club_affiliation_time" id="club_affiliation_time_add" class="form-control datepicker validate[required]" autocomplete="off" type="text">
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-offset-5 col-xs-7">
                  <input class="btn orange hvr-float-shadow" value="Save" type="submit">
                </div>
              </div>
            </form>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.addClub-Model -->

	<div class="modal fade" id="editClub" tabindex="-1" role="dialog" aria-labelledby="editClubModel" aria-hidden="true">
      <div class="modal-dialog" style="max-height:800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Edit your Club</h4>
          </div>
          <div class="modal-body" style="max-height:700px">
            <form method="post" action="" class="form-horizontal req-frm">
              <input type="hidden" name="action" value="club-update">
              <div class="form-group">
                <label class="col-xs-5 control-label">Club Name</label>
                <div class="col-xs-7">
                  <input autocomplete="off" name="club_name" id="club_name_edit" maxlength="54" class="form-control validate[required] ui-autocomplete-input" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Preferred Order</label>
                <div class="col-xs-7">
                  <input name="sort" id="club_sort_edit" class="form-control validate[required,[custom[integer]]" type="text">
                  <span class="help-block">1 is Highest Ranked</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Affiliation Date</label>
                <div class="col-xs-7">
                  <input name="club_affiliation_time" id="club_affiliation_time_edit" class="form-control datepicker validate[required]" autocomplete="off" type="text">
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-offset-5 col-xs-7">
                  <input class="btn orange hvr-float-shadow" value="Save" type="submit">
				  <input type="hidden" name="recordId" id="recordId" value="">
                </div>
              </div>
            </form>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.addClub-Model -->

    <div class="modal fade" id="addLeague" tabindex="-1" role="dialog" aria-labelledby="addLeagueModel" aria-hidden="true">
      <div class="modal-dialog" style="max-height:800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Add your League</h4>
          </div>
          <div class="modal-body" style="max-height:700px">
            <form method="post" action="" class="form-horizontal req-frm">
              <input name="action" value="add-league" type="hidden">
              <div class="form-group">
                <label class="col-xs-5 control-label">League Name</label>
                <div class="col-xs-7">
                  <input autocomplete="off" name="league_name" id="league_name_add" maxlength="54" class="form-control validate[required] ui-autocomplete-input" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Preferred Order</label>
                <div class="col-xs-7">
                  <input name="sort" class="form-control validate[required,[custom[integer]]" type="text">
                  <span class="help-block">1 is Highest Ranked</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Affiliation Date</label>
                <div class="col-xs-7">
                  <input name="league_affiliation_time" id="league_affiliation_time" class="form-control datepicker validate[required]" autocomplete="off" type="text">
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-offset-5 col-xs-7">
                  <input class="btn orange hvr-float-shadow" value="Save" type="submit">
                </div>
              </div>
            </form>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.addLeague-Model -->

	<div class="modal fade" id="editLeague" tabindex="-1" role="dialog" aria-labelledby="editLeagueModel" aria-hidden="true">
      <div class="modal-dialog" style="max-height:800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Edit your League</h4>
          </div>
          <div class="modal-body" style="max-height:700px">
            <form method="post" action="" class="form-horizontal req-frm">
              <<input type="hidden" name="action" value="league-update">
              <div class="form-group">
                <label class="col-xs-5 control-label">League Name</label>
                <div class="col-xs-7">
                  <input autocomplete="off" name="league_name" id="league_name_edit" maxlength="54" class="form-control validate[required] ui-autocomplete-input" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Preferred Order</label>
                <div class="col-xs-7">
                  <input name="sort" id="league_sort_edit" class="form-control validate[required,[custom[integer]]" type="text">
                  <span class="help-block">1 is Highest Ranked</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Affiliation Date</label>
                <div class="col-xs-7">
                  <input name="league_affiliation_time" id="league_affiliation_time_edit" class="form-control datepicker validate[required]" autocomplete="off" type="text">
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-offset-5 col-xs-7">
                  <input class="btn orange hvr-float-shadow" value="Save" type="submit">
				  <input type="hidden" name="edit_league_id" id="edit_league_id" value="">
                </div>
              </div>
            </form>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.addLeague-Model -->


    <div class="modal fade" id="addCompany" tabindex="-1" role="dialog" aria-labelledby="addCompanyModel" aria-hidden="true">
      <div class="modal-dialog" style="max-height:800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Add your Company</h4>
          </div>
          <div class="modal-body" style="max-height:700px">
            <form method="post" action="" class="form-horizontal req-frm">
              <input name="action" value="add-agent" type="hidden">
              <div class="form-group">
                <label class="col-xs-5 control-label">Company Name</label>
                <div class="col-xs-7">
                  <input autocomplete="off" name="agent_name" id="company_name_add" maxlength="54" class="form-control validate[required] ui-autocomplete-input" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Preferred Order</label>
                <div class="col-xs-7">
                  <input name="sort" class="form-control validate[required,[custom[integer]]" type="text">
                  <span class="help-block">1 is Highest Ranked</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Affiliation Date</label>
                <div class="col-xs-7">
                  <input name="company_affiliation_time" id="company_affiliation_time" class="form-control datepicker validate[required]" autocomplete="off" type="text">
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-offset-5 col-xs-7">
                  <input class="btn orange hvr-float-shadow" value="Save" type="submit">
                </div>
              </div>
            </form>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.addCompany-Model -->

	<div class="modal fade" id="editCompany" tabindex="-1" role="dialog" aria-labelledby="editCompanyModel" aria-hidden="true">
      <div class="modal-dialog" style="max-height:800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Edit your Company</h4>
          </div>
          <div class="modal-body" style="max-height:700px">
            <form method="post" action="" class="form-horizontal req-frm">
              <input type="hidden" name="action" value="agent-update">
              <div class="form-group">
                <label class="col-xs-5 control-label">Company Name</label>
                <div class="col-xs-7">
                  <input autocomplete="off" name="agent_name" id="company_name_edit" maxlength="54" class="form-control validate[required] ui-autocomplete-input" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Preferred Order</label>
                <div class="col-xs-7">
                  <input name="sort" id="company_sort_edit" class="form-control validate[required,[custom[integer]]" type="text">
                  <span class="help-block">1 is Highest Ranked</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Affiliation Date</label>
                <div class="col-xs-7">
                  <input name="company_affiliation_time" id="company_affiliation_time_edit" class="form-control datepicker validate[required]" autocomplete="off" type="text">
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-offset-5 col-xs-7">
                  <input class="btn orange hvr-float-shadow" value="Save" type="submit">
				  <input type="hidden" name="edit_agent_id" id="edit_agent_id" value="">
                </div>
              </div>
            </form>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.addCompany-Model -->

    <div class="modal fade" id="addExp" tabindex="-1" role="dialog" aria-labelledby="addExpModel" aria-hidden="true">
      <div class="modal-dialog" style="max-height:800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Add your Experience</h4>
          </div>
          <div class="modal-body" style="max-height:700px">
            <form method="post" action="" class="form-horizontal req-frm">
              <input name="action" value="add-exp" type="hidden">
              <div class="form-group">
                <label class="col-xs-5 control-label">Select Country</label>
                <div class="col-xs-7">
					<?=get_combo('countries','name','','country_id','','','Select Country')?>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Organization</label>
                <div class="col-xs-7">
                  <input name="organization" class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Job Title</label>
                <div class="col-xs-7">
                  <input name="job_title" class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Start Date</label>
                <div class="col-xs-7">
                  <input name="start_date" id="start_date_add" class="form-control datepicker validate[required]" autocomplete="off" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">End Date</label>
                <div class="col-xs-7">
                  <input name="end_date" id="end_date_add" class="form-control datepicker validate[required]" autocomplete="off" type="text">
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-offset-5 col-xs-7">
                  <input class="btn orange hvr-float-shadow" value="Save" type="submit">
                </div>
              </div>
            </form>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.addExp-Model -->

	    <div class="modal fade" id="editExp" tabindex="-1" role="dialog" aria-labelledby="addExpModel" aria-hidden="true">
      <div class="modal-dialog" style="max-height:800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Update your Experience</h4>
          </div>
          <div class="modal-body" style="max-height:700px">
            <form method="post" action="" class="form-horizontal req-frm">
              <input type="hidden" name="action" value="edit-exp">
              <div class="form-group">
                <label class="col-xs-5 control-label">Select Country</label>
                <div class="col-xs-7">
					<label id="country"></label>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Organization</label>
                <div class="col-xs-7">
                  <input id="organization" name="organization" class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Job Title</label>
                <div class="col-xs-7">
                  <input name="job_title" id="job_title" class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Start Date</label>
                <div class="col-xs-7">
                  <input name="start_date" id="start_date_edit" class="form-control datepicker validate[required]" autocomplete="off" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">End Date</label>
                <div class="col-xs-7">
                  <input name="end_date" id="end_date_edit" class="form-control datepicker validate[required]" autocomplete="off" type="text">
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-offset-5 col-xs-7">
					<input type="hidden" name="recordId" id="recordId" value="">
                  <input class="btn orange hvr-float-shadow" value="Save" type="submit">
                </div>
              </div>
            </form>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.addExp-Model -->

    <div class="modal fade" id="add-domestic" tabindex="-1" role="dialog" aria-labelledby="add-domesticModel" aria-hidden="true">
      <div class="modal-dialog" style="max-height:800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Add Domestic Opportunity</h4>
          </div>
          <div class="modal-body" style="max-height:700px">
            <form method="post" action="" class="form-horizontal" class="req-frm">
              <input name="action" value="add-domestic" type="hidden">
              <div class="form-group">
                <label class="col-xs-5 control-label">Select Domestic States</label>
                <div class="col-xs-7" id="states_domestic">
                  <select name="state_id" class="form-control validate[required]">
                   <option selected="selected" value="">-- Select --</option>
					<? 	$rs_states = mysqli_query($conn,"select * from states where country_id = '".$row_user['country_id']."'");
						while($row_state_l = mysqli_fetch_assoc($rs_states)){ ?>
						<option  value="<?=$row_state_l['id']?>"><?=$row_state_l['name']?></option>
					<?  } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Add city Name</label>
                <div class="col-xs-7">
                  <input name="city_name" id="city_name"  class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Start Date</label>
                <div class="col-xs-7">
                  <input name="from_date" id="domestic_start_date_add" class="form-control datepicker validate[required]" autocomplete="off" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">End Date</label>
                <div class="col-xs-7">
                  <input name="to_date" id="domestic_start_date_add" class="form-control datepicker validate[required]" autocomplete="off" type="text">
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-offset-5 col-xs-7">
                  <input class="btn orange hvr-float-shadow" value="Save" type="submit">
                </div>
              </div>
            </form>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.add-domestic-Model -->

	<div class="modal fade" id="edit-domestic" tabindex="-1" role="dialog" aria-labelledby="edit-domesticModel" aria-hidden="true">
      <div class="modal-dialog" style="max-height:800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Edit Domestic Opportunity</h4>
          </div>
          <div class="modal-body" style="max-height:700px">
            <form method="post" action="" class="form-horizontal" class="req-frm">
              <input name="action" value="edit-domestic" type="hidden">
			  <input type="hidden" name="recordId" id="recordId" value="">
              <div class="form-group">
                <label class="col-xs-5 control-label">Select Domestic States</label>
                <div id="user_state_domestic">
					<img src='images/ajax-loader.gif' />
				</div>

              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Add city Name</label>
                <div class="col-xs-7">
                  <input name="city_name" id="city_name" class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Start Date</label>
                <div class="col-xs-7">
                  <input name="from_date" id="domestic_start_date_edit" class="form-control datepicker validate[required]" autocomplete="off" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">End Date</label>
                <div class="col-xs-7">
                  <input name="to_date" id="domestic_end_date_edit" class="form-control datepicker validate[required]" autocomplete="off" type="text">
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-offset-5 col-xs-7">
                  <input class="btn orange hvr-float-shadow" value="Save" type="submit">
                </div>
              </div>
            </form>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.add-domestic-Model -->

    <div class="modal fade" id="add-international" tabindex="-1" role="dialog" aria-labelledby="add-internationalModel" aria-hidden="true">
      <div class="modal-dialog" style="max-height:800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Add International Opportunity</h4>
          </div>
          <div class="modal-body" style="max-height:700px">
            <form method="post" action="" class="form-horizontal req-frm">
              <input type="hidden" name="action" value="add-international">
              <div class="form-group">
                <label class="col-xs-5 control-label">Select Country</label>
                <div class="col-xs-7" id="states_domestic">
				<?php $sql = mysqli_query($conn,"SELECT * FROM countries");?>
                  <select name="country_id" id="c_id" onchange="states();" class="form-control validate[required]">
                   <option>--Any--</option>
			  		<?php while($row = mysqli_fetch_assoc($sql)){?>
			  		<option value="<?php echo $row['id'];?>"> <?php echo $row['name'];?></option>
			  		<?php }?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Select State</label>
                <div class="col-xs-7" id="states_domestic">
					<div id="state">
						<div id="state_data">
							State values appear here
						</div>
					</div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Add city Name</label>
                <div class="col-xs-7">
                  <input name="city_name" id="city_name" class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Start Date</label>
                <div class="col-xs-7">
                  <input name="from_date" id="from_date_internation_add" class="form-control datepicker validate[required]" autocomplete="off" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">End Date</label>
                <div class="col-xs-7">
                  <input name="to_date" id="to_date_internation_add" class="form-control datepicker validate[required]" autocomplete="off" type="text">
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-offset-5 col-xs-7">
                  <input class="btn orange hvr-float-shadow" value="Save" type="submit">
                </div>
              </div>
            </form>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.add-international-Model -->

	<div class="modal fade" id="edit-international" tabindex="-1" role="dialog" aria-labelledby="add-internationalModel" aria-hidden="true">
      <div class="modal-dialog" style="max-height:800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Update International Opportunity</h4>
          </div>
          <div class="modal-body" style="max-height:700px">
            <form method="post" action="" class="form-horizontal req-frm">
            <input type="hidden" name="action" value="edit-international">
			<input type="hidden" name="recordId" id="recordId" value="">
              <div class="form-group">
                <label class="col-xs-5 control-label">Select Country</label>
                <div class="col-xs-7" id="states_domestic">
					<div id="country"> <img src='images/ajax-loader.gif' /></div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Select State</label>
                <div class="col-xs-7" id="states_domestic">
					<div id="state">
						<div id="states_data">
			  		  <div id='state_edit'> <img src='images/ajax-loader.gif' /> </div>
			  		</div>
					</div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">City Name</label>
                <div class="col-xs-7">
                  <input name="city_name" id="city_name" class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Start Date</label>
                <div class="col-xs-7">
                  <input  name="from_date" id="from_date_internation_edit" class="form-control datepicker validate[required]" autocomplete="off" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">End Date</label>
                <div class="col-xs-7">
                  <input name="to_date" id="to_date_internation_edit" class="form-control datepicker validate[required]" autocomplete="off" type="text">
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-offset-5 col-xs-7">
                  <input class="btn orange hvr-float-shadow" value="Save" type="submit">
                </div>
              </div>
            </form>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.add-international-Model -->

    <div class="modal fade" id="add-citizenship" tabindex="-1" role="dialog" aria-labelledby="add-citizenshipModel" aria-hidden="true">
      <div class="modal-dialog" style="max-height:800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Add citizenship</h4>
          </div>
          <div class="modal-body" style="max-height:700px">
            <form method="post" action="" class="form-horizontal req-frm">
              <input name="action" value="city" type="hidden">
              <div class="form-group" id="country-citizen">
                <label class="col-xs-5 control-label">Select Country</label>
                <div class="col-xs-7">
                  <select name="citizen[]" id="citizen" class="form-control validate[required]">
                    <?php
						$rs_c = mysqli_query($conn,"select * from countries");
						while($row_c = mysqli_fetch_assoc($rs_c)){ ?>
						<option value='<?=$row_c['id']?>'> <?php echo $row_c['name'];?> </option>
					<?  } ?>
                  </select>
                </div>
              </div>
			  <div id="new-citizen"></div>
              <div class="form-group">
                <div class="col-xs-10">
                  <h5>Add More</h5>
                </div>
                <div class="col-xs-2">
                  <a href="#" onclick="$('#country-citizen').clone().appendTo('#new-citizen');" class="btn orange full hvr-float-shadow margin-bottom-10" id="add-row"> <i class="fa fa-plus"></i> </a>
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-offset-5 col-xs-7">
                  <input class="btn orange hvr-float-shadow" value="Save" type="submit">
                </div>
              </div>
            </form>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.add-citizenship-Model -->

    <div class="modal fade" id="add-travel" tabindex="-1" role="dialog" aria-labelledby="add-travelModel" aria-hidden="true">
      <div class="modal-dialog" style="max-height:800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Add Travel Visa Details</h4>
          </div>
          <div class="modal-body" style="max-height:700px">
            <form method="post" action="" class="form-horizontal req-frm">
              <input type="hidden" name="action" value="add-travel">
              <div class="form-group">
                <label class="col-xs-5 control-label">Select Country</label>
                <div class="col-xs-7">
                  <?=get_combo('countries','name','','country_id','','','Select Country');?>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Start Date</label>
                <div class="col-xs-7">
                  <input name="from_date" id="dateFrom" class="form-control datepicker validate[required]" autocomplete="off" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">End Date</label>
                <div class="col-xs-7">
                  <input name="to_date" id="dateTo" class="form-control datepicker validate[required]" autocomplete="off" type="text">
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-offset-5 col-xs-7">
                  <input class="btn orange hvr-float-shadow" value="Save" type="submit">
                </div>
              </div>
            </form>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.add-travel-Model -->

	<div class="modal fade" id="edit-travel" tabindex="-1" role="dialog" aria-labelledby="edit-travelModel" aria-hidden="true">
      <div class="modal-dialog" style="max-height:800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Update Travel Visa Details</h4>
          </div>
          <div class="modal-body" style="max-height:700px">
            <form method="post" action="" class="form-horizontal req-frm">
				<input type="hidden" name="action" value="edit-travel">
				<input type="hidden" name="recordId" id="recordId" value="">
              <div class="form-group">
                <label class="col-xs-5 control-label"> Country</label>
                <div class="col-xs-7">
                 <label id="name"></label>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Start Date</label>
                <div class="col-xs-7">
                  <input  name="from_date" id="edateFrom" class="form-control datepicker validate[required]" autocomplete="off" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">End Date</label>
                <div class="col-xs-7">
                  <input  name="to_date" id="edateTo" class="form-control datepicker validate[required]" autocomplete="off" type="text">
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-offset-5 col-xs-7">
                  <input class="btn orange hvr-float-shadow" value="Save" type="submit">
                </div>
              </div>
            </form>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.add-travel-Model -->

    <div class="modal fade" id="add-education" tabindex="-1" role="dialog" aria-labelledby="add-educationModel" aria-hidden="true">
      <div class="modal-dialog" style="max-height:800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Add your Education</h4>
          </div>
          <div class="modal-body" style="max-height:700px">
            <form method="post" action="" class="form-horizontal req-frm">
              <input name="action" value="add-education" type="hidden">
              <div class="form-group">
                <label class="col-xs-5 control-label">Degree Name</label>
                <div class="col-xs-7">
                  <input name="degree" class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Institute Name</label>
                <div class="col-xs-7">
                  <input name="institute" class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Passing Year</label>
                <div class="col-xs-7">
                  <input name="passing_year" class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-offset-5 col-xs-7">
                  <input class="btn orange hvr-float-shadow" value="Save" type="submit">
                </div>
              </div>
            </form>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.add-education-Model -->


	<div class="modal fade" id="edit-education" tabindex="-1" role="dialog" aria-labelledby="edit-educationModel" aria-hidden="true">
      <div class="modal-dialog" style="max-height:800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Update your Education</h4>
          </div>
          <div class="modal-body" style="max-height:700px">
            <form method="post" action="" class="form-horizontal req-frm">
              <input type="hidden" name="action" value="edit-education">
				<input type="hidden" name="recordId" id="recordId" value="">
              <div class="form-group">
                <label class="col-xs-5 control-label">Degree Name</label>
                <div class="col-xs-7">
                  <input name="degree" id="degree" class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Institute Name</label>
                <div class="col-xs-7">
                  <input name="institute" id="institute" class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Passing Year</label>
                <div class="col-xs-7">
                  <input name="passing_year" id="passing_year" class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-offset-5 col-xs-7">
                  <input class="btn orange hvr-float-shadow" value="Save" type="submit">
                </div>
              </div>
            </form>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.add-education-Model -->

    <div class="modal fade" id="add-language" tabindex="-1" role="dialog" aria-labelledby="add-languageModel" aria-hidden="true">
      <div class="modal-dialog" style="max-height:800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Add your Language</h4>
          </div>
          <div class="modal-body" style="max-height:700px">
            <form method="post" action="" class="form-horizontal req-frm">
              <input name="action" value="add-language" type="hidden">
              <div class="form-group">
                <label class="col-xs-5 control-label">Language</label>
                <div class="col-xs-7">
                  <input name="langauge" class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Language Skills</label>
                <div class="col-xs-7">
                  <p><input name="speaking" type="checkbox"> Speaking </p>
                  <p><input name="reading" type="checkbox"> Reading </p>
                  <p> <input name="writing" type="checkbox"> Writing </p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Preferred Order </label>
                <div class="col-xs-7">
                  <input name="sort" class="form-control validate[required]" type="text">
                  <span class="help-block">1 is Highest Ranked</span>
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-offset-5 col-xs-7">
                  <input class="btn orange hvr-float-shadow" value="Save" type="submit">
                </div>
              </div>
            </form>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.add-language-Model -->

	<div class="modal fade" id="edit-language" tabindex="-1" role="dialog" aria-labelledby="edit-languageModel" aria-hidden="true">
      <div class="modal-dialog" style="max-height:800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Update your Language</h4>
          </div>
          <div class="modal-body" style="max-height:700px">
            <form method="post" action="" class="form-horizontal req-frm">
              <input type="hidden" name="action" value="edit-language">
				<input type="hidden" name="recordId" id="recordId" value="">
              <div class="form-group">
                <label class="col-xs-5 control-label">Language</label>
                <div class="col-xs-7">
                  <input name="langauge" id="langauge" class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Language Skills</label>
                <div class="col-xs-7">
                  <p><input name="speaking" id="speaking" type="checkbox"> Speaking </p>
                  <p><input name="reading" id="reading" type="checkbox"> Reading </p>
                  <p> <input name="writing" id="writing" type="checkbox"> Writing </p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Preferred Order </label>
                <div class="col-xs-7">
                  <input name="sort" id="sort" class="form-control validate[required]" type="text">
                  <span class="help-block">1 is Highest Ranked</span>
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-offset-5 col-xs-7">
                  <input class="btn orange hvr-float-shadow" value="Save" type="submit">
                </div>
              </div>
            </form>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.add-language-Model -->

    <div class="modal fade" id="add-cricketer" tabindex="-1" role="dialog" aria-labelledby="add-cricketerModel" aria-hidden="true">
      <div class="modal-dialog" style="max-height:800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Add your Favorite Cricketer</h4>
          </div>
          <div class="modal-body" style="max-height:700px">
            <form method="post" action="" class="form-horizontal req-frm">
              <input name="action" value="add-cricketer" type="hidden">
              <div class="form-group">
                <label class="col-xs-5 control-label">Favorite Cricketer</label>
                <div class="col-xs-7">
                  <input name="name" class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Preferred Order </label>
                <div class="col-xs-7">
                  <input name="sort" class="form-control validate[required]" type="text">
                  <span class="help-block">1 is Highest Ranked</span>
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-offset-5 col-xs-7">
                  <input class="btn orange hvr-float-shadow" value="Save" type="submit">
                </div>
              </div>
            </form>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.add-cricketer-Model -->

	<div class="modal fade" id="edit-cricketer" tabindex="-1" role="dialog" aria-labelledby="edit-cricketerModel" aria-hidden="true">
      <div class="modal-dialog" style="max-height:800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Update your Favorite Cricketer</h4>
          </div>
          <div class="modal-body" style="max-height:700px">
            <form method="post" action="" class="form-horizontal req-frm">
				<input type="hidden" name="action" value="edit-cricketer">
				<input type="hidden" name="recordId" id="recordId" value="">
              <div class="form-group">
                <label class="col-xs-5 control-label">Favorite Cricketer</label>
                <div class="col-xs-7">
                  <input name="name" id="name" class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Preferred Order </label>
                <div class="col-xs-7">
                  <input name="sort" id="sort" class="form-control validate[required]" type="text">
                  <span class="help-block">1 is Highest Ranked</span>
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-offset-5 col-xs-7">
                  <input class="btn orange hvr-float-shadow" value="Save" type="submit">
                </div>
              </div>
            </form>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.add-cricketer-Model -->

    <div class="modal fade" id="add-brand" tabindex="-1" role="dialog" aria-labelledby="add-brandModel" aria-hidden="true">
      <div class="modal-dialog" style="max-height:800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Add your Favorite Cricket Merchandise Brand</h4>
          </div>
          <div class="modal-body" style="max-height:700px">
            <form method="post" action="" class="form-horizontal req-frm">
              <input name="action" value="add-brand" type="hidden">
              <div class="form-group">
                <label class="col-xs-5 control-label">Favorite Merchandise Brand</label>
                <div class="col-xs-7">
                  <input name="name" id="name" class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Preferred Order </label>
                <div class="col-xs-7">
                  <input name="sort" id="sort" class="form-control validate[required]" type="text">
                  <span class="help-block">1 is Highest Ranked</span>
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-offset-5 col-xs-7">
                  <input class="btn orange hvr-float-shadow" value="Save" type="submit">
                </div>
              </div>
            </form>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.add-brand-Model -->

	<div class="modal fade" id="edit-brand" tabindex="-1" role="dialog" aria-labelledby="edit-brandModel" aria-hidden="true">
      <div class="modal-dialog" style="max-height:800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Update your Favorite Cricket Merchandise Brand</h4>
          </div>
          <div class="modal-body" style="max-height:700px">
            <form method="post" action="" class="form-horizontal req-frm">
            <input type="hidden" name="action" value="edit-brand">
			<input type="hidden" name="recordId" id="recordId" value="">
              <div class="form-group">
                <label class="col-xs-5 control-label">Favorite Merchandise Brand</label>
                <div class="col-xs-7">
                  <input name="name"  id="name" class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Preferred Order </label>
                <div class="col-xs-7">
                  <input name="sort" id="sort" class="form-control validate[required]" type="text">
                  <span class="help-block">1 is Highest Ranked</span>
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-offset-5 col-xs-7">
                  <input class="btn orange hvr-float-shadow" value="Save" type="submit">
                </div>
              </div>
            </form>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.add-brand-Model -->




	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

	<script type="text/javascript">
	var WWW = "<?=WWW?>";
	$(document).ready(function() {

		$("#step-9-form").validate();

		$("#profile-photo").validate();

		$("#menu-individual-profile a").click(function(event){
			event.preventDefault();
			event.stopPropagation();
			var id = $(this).attr('id').replace('step','').replace('-tab','');
			for(var i=1;i<=10;i++){
				$("#step"+i).hide();
				$("#step"+i+"-tab").removeClass("active");
			}
			$("#step"+id).show();
			$("#step"+id+"-tab").addClass("active");
		});

		<?php if(isset($_POST) && !empty($_POST['update_profile_video'])){ ?>
			$("#step2").show();$("#step2-tab").addClass("active");
		<?php }elseif(isset($_POST) && !empty($_POST['update_profile_photo'])){ ?>
			$("#step1").show();$("#step1-tab").addClass("active");
		<?php }if(isset($_POST) && !empty($_POST['update_location'])){ ?>
			$("#step3").show();$("#step3-tab").addClass("active");
		<?php }elseif(isset($_POST) && !empty($_POST['update_individual_traits'])){ ?>
			$("#step4").show();$("#step4-tab").addClass("active");
		<?php }elseif(isset($_POST['action']) && in_array($_POST['action'],array('add-club','club-update','add-league','league-update','add-agent','agent-update') )){ ?>
			$("#step5").show();$("#step5-tab").addClass("active");
		<?php }elseif(isset($_POST['action']) && in_array($_POST['action'],array('update_overall_exp','add-exp','edit-exp') )){ ?>
			$("#step6").show();$("#step6-tab").addClass("active");
		<?php }elseif(isset($_POST['action']) && in_array($_POST['action'],array('add-domestic','edit-domestic','add-international','edit-international') )){ ?>
			$("#step7").show();$("#step7-tab").addClass("active");
		<?php }elseif(isset($_POST['action']) && in_array($_POST['action'],array('add-travel') )){ ?>
			$("#step8").show();$("#step8-tab").addClass("active");
		<?php }else if(isset($_GET['step']) && !empty($_GET['step'])){  ?>
			$("#step<?php echo $_GET['step']; ?>").show();$("#step<?php echo $_GET['step']; ?>-tab").addClass("active");
		<?php }else{ ?>
			$("#step1").show();$("#step1-tab").addClass("active");
		<?php } ?>

		$('input[name=video]').click(function(){
			if(this.value == 'embed'){
				$('#upload-video').addClass('hide');
				$('#embed-video').removeClass('hide');

			}else{
				$('#embed-video').addClass('hide');
				$('#upload-video').removeClass('hide');
			}
		});

		$('.datepicker').datepicker({'format':'yyyy-mm-dd'});
		$("#club_name_add,#club_name_edit").autocomplete({
			source: "affiliation_data.php?type=clubs",
		});
		$("#league_name_add,#league_name_edit").autocomplete({
			source: "affiliation_data.php?type=leagues",
		});
		$("#company_name_add,#company_name_edit").autocomplete({
			source: "affiliation_data.php?type=companies",
		});

		$('a.edit-affiliation').click(function(){
		var link = $(this).attr('href');
		if(link == '#edit-club'){
			var id = this.id;
			$.ajax({type	: 'POST',
				url		: WWW + 'includes/get-record.php',
				data	: ({table:'clubs',field:'title',id:id}),
				success	: function(msg){
					var msg1 = msg.split('___');
					$('#club_name_edit').val(msg1[1]);
					$('#club_sort_edit').val(msg1[0]);
					$('#club_affiliation_time_edit').val(msg1[2]);
					$('#recordId').val(id);
				}
			});
		}
		if(link == '#edit-league'){
			var id = this.id;
			$.ajax({type	: 'POST',
				url		: WWW + 'includes/get-record.php',
				data	: ({table:'leagues',field:'title',id:id}),
				success	: function(msg){
					var msg1 = msg.split('___');
					$('#league_name_edit').val(msg1[1]);
					$('#league_sort_edit').val(msg1[0]);
					$('#league_affiliation_time_edit').val(msg1[2]);
					$('#edit_league_id').val(id);
				}
			});
		}
		if(link == '#edit-agent'){
			var id = this.id;

			var idd= id.split('___');

			$.ajax({type	: 'POST',
				url		: WWW + 'includes/get-record.php',
				data	: ({table:'agents',field:'title',id:id}),
				success	: function(msg){
					var msg1 = msg.split('___');
					$('#company_name_edit').val(msg1[1]);
					$('#company_sort_edit').val(msg1[0]);
					$('#company_affiliation_time_edit').val(msg1[2]);
					$('#edit_agent_id').val(idd[0]);
				}
			});
		}

	});

	$('.edit-experience').click(function(){
		var link = $(this).attr('href');
		if(link == '#edit-exp'){
			var id = this.id;
			$.ajax({type	: 'POST',
				url		: WWW + 'includes/get-record.php',
				data	: ({table:'experiences',field:'*',id:id}),
				success	: function(msg){
					msg1 = msg.split('___');

					$("#editExp").find('#country').html(msg1[0]);
					$("#editExp").find('#organization').val(msg1[1]);
					$("#editExp").find('#job_title').val(msg1[2]);
					$("#editExp").find('#start_date_edit').val(msg1[3]);
					$("#editExp").find('#end_date_edit').val(msg1[4]);
					$("#editExp").find('#recordId').val(id);
				}
			});
		}
	});

	$(".req-frm").validationEngine();

	$('input[name=domestic]').click(function(){

		if($(this).is(':checked') == true){
			$('.domestic-detail').removeClass('hide');
		}else{
			$('.domestic-detail').addClass('hide');
		}
	});
	$('input[name=international]').click(function(){
		if($(this).is(':checked') == true){
			$('.international-detail').removeClass('hide');
		}else{
			$('.international-detail').addClass('hide');
		}
	});

	$('a.edit-opportunities').click(function(){
		var link = $(this).attr('href');
		if(link == '#edit-domestic'){
			var id = this.id;

			$.ajax({type	: 'POST',
				url		: WWW + 'includes/get_domestic_state.php',
				data	: ({id:id}),
				success	: function(msg){
					$("#user_state_domestic").html(msg);
					$(link+' #recordId').val(id);
				}
			});

			$.ajax({type	: 'POST',
				url		: WWW + 'includes/get-record.php',
				data	: ({table:'opportunities',field:'*',id:id}),
				success	: function(msg){
					msg1 = msg.split('___');
					$("#edit-domestic").find('#city_name').val(msg1[0]);
					$("#edit-domestic").find('#domestic_start_date_edit').val(msg1[1]);
					$("#edit-domestic").find('#domestic_end_date_edit').val(msg1[2]);
					$("#edit-domestic").find('#recordId').val(id);
				}
			});
		}
		if(link == '#edit-international'){
			var id = this.id;
			$('#country').html();
			$('#state_edit').html();
			$.ajax({type	: 'POST',
				url		: WWW + 'includes/get-country.php',
				data	: ({id:id}),
				success	: function(msg){
					$("#edit-international").find('#country').html(msg);
				}
			});
			$.ajax({type	: 'POST',
				url		: WWW + 'includes/get-state.php',
				data	: ({id:id}),
				success	: function(msg){
					$("#edit-international").find('#state_edit').html(msg);
				}
			});

			$.ajax({type	: 'POST',
				url		: WWW + 'includes/get-record.php',
				data	: ({table:'opportunities',field:'*',id:id}),
				success	: function(msg){
					msg1 = msg.split('___');
					$("#edit-international").find('#city_name').val(msg1[0]);
					$("#edit-international").find('#from_date_internation_edit').val(msg1[1]);
					$("#edit-international").find('#to_date_internation_edit').val(msg1[2]);
					$("#edit-international").find('#recordId').val(id);
				}
			});
		}

	});

	states = function(){
		var c_id 		= document.getElementById("c_id").value;
		var id = "id="+c_id;
		var url    		= 'states.php';
		$.ajax({type	: 'POST',
				url		: WWW + 'states.php',
				data	: ({id:c_id}),
				success	: function(msg){
					document.getElementById("state_data").innerHTML = msg;
				}
			});
	}

	get_states = function(){
		var c_id 		= document.getElementById("get_country").value;
		var id = "id="+c_id;
		var url    		= 'states.php';
		$.ajax({type	: 'POST',
				url		: WWW + 'states.php',
				data	: ({id:c_id}),
				success	: function(msg){
					document.getElementById("states_data").innerHTML = msg;
				}
			});
	}

	$('a.edit-travel-details').click(function(){
		var link = $(this).attr('href');
		if(link == '#edit-travel'){
			var id = this.id;
			$.ajax({type	: 'POST',
				url		: WWW + 'includes/get-record.php',
				data	: ({table:'travel_visas',field:'*',id:id}),
				success	: function(msg){
					msg1 = msg.split('___');
					$("#edit-travel").find('#name').html(msg1[0]);
					$("#edit-travel").find('#edateFrom').val(msg1[1]);
					$("#edit-travel").find('#edateTo').val(msg1[2]);
					$("#edit-travel").find('#recordId').val(id);
				}
			});
		}
	});


	$('a.personality-education').click(function(){
		var link = $(this).attr('href');
		if(link == '#edit-education'){
			var id = this.id;
			$.ajax({type	: 'POST',
				url		: WWW + 'includes/get-record.php',
				data	: ({table:'educations',field:'*',id:id}),
				success	: function(msg){
					msg1 = msg.split('___');alert(msg);
					$("#edit-education").find('#degree').val(msg1[0]);
					$("#edit-education").find('#institute').val(msg1[1]);
					$("#edit-education").find('#passing_year').val(msg1[2]);
					$("#edit-education").find('#recordId').val(id);
				}
			});
		}
		if(link == '#edit-language'){
			var id = this.id;
			$.ajax({type	: 'POST',
				url		: WWW + 'includes/get-record.php',
				data	: ({table:'languages',field:'*',id:id}),
				success	: function(msg){
					msg1 = msg.split('___');
					$("#edit-language").find('#langauge').val(msg1[0]);
					if(msg1[1] == '1'){
						$("#edit-language").find("#speaking").attr('checked','checked');
					}
					if(msg1[2] == '1'){
						$("#edit-language").find("#reading").attr('checked','checked');
					}
					if(msg1[3] == '1'){
						$("#edit-language").find("#writing").attr('checked','checked');
					}
					$("#edit-language").find('#sort').val(msg1[4]);
					$("#edit-language").find('#recordId').val(id);
				}
			});
		}
		if(link == '#edit-cricketer'){
			var id = this.id;
			$.ajax({type	: 'POST',
				url		: WWW + 'includes/get-record.php',
				data	: ({table:'favorite_cricketers',field:'name',id:id}),
				success	: function(msg){
					var msg1 = msg.split('___');
					$("#edit-cricketer").find('#name').val(msg1[0]);
					$("#edit-cricketer").find('#sort').val(msg1[1]);
					$("#edit-cricketer").find('#recordId').val(id);
				}
			});
		}
		if(link == '#edit-brand'){
			var id = this.id;
			$.ajax({type	: 'POST',
				url		: WWW + 'includes/get-record.php',
				data	: ({table:'favorite_brands',field:'name',id:id}),
				success	: function(msg){
					var msg1 = msg.split('___');
					$("#edit-brand").find('#name').val(msg1[0]);
					$("#edit-brand").find('#sort').val(msg1[1]);
					$("#edit-brand").find('#recordId').val(id);
				}
			});
		}

	});

	updateCountdown=function() {
		var remaining = 1000 - $('#msg').val().length;
		$('.countdown').text(remaining + ' characters remaining.');
	}

	updateCountdown();
    $('#msg').change(updateCountdown);
	$('#msg').keyup(updateCountdown);

	});
	</script>
	<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>
<div class="modal fade" id="phoneNumberView" tabindex="-1" role="dialog" aria-labelledby="phoneNumberViewModel" aria-hidden="true">
  <div class="modal-dialog" data-mcs-theme="minimal" style="max-height:800px">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="settingLabel">Why do we need your Phone Number </h4>
      </div>
      <div class="modal-body mCustomScrollbar" data-mcs-theme="minimal" style="max-height:700px">
        <div class="row">
          <div class="col-sm-12">
						<?php $row = get_record_on_id('cms',57);?>
		        <p><?php echo $row['content']; ?></p>
          </div>
        </div>
      </div><!-- /.modal-body -->
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.birthModel-Model -->
<?php include('common/footer.php'); ?>
