<?php include_once('includes/configuration.php');

$league_info = array();
$matches = array();
$user_tournaments = array();
$error = '';
$start_record = 0;
$records_per_page = 200;
$groups = array();
$tournament_teams = array();

$tournament_id = trim($_GET['t_id']);
$tournament_info = get_record_on_id('tournaments', $tournament_id);	
$abandon_array = array('N/A','Forfeit by One Team','Late Arrival of One Team','Umpire(s) Did not Show Up');
$week_days = array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
$months = array('January','February','March','April','May','June','July','August','September','October','November','December');

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	//$user_info = get_record_on_id('users', $user_id);	
}

if(isset($user_id) && !empty($user_id)){
	$sql = "select * from tournament_permissions where tournament_id = $tournament_id and member_id = $user_id LIMIT 1";
	$rs_permission = mysqli_query($conn,$sql);
	if($tournament_info['user_id'] == $user_id || mysqli_num_rows($rs_permission)>0 ){
		$tournament_permission = 1;
	}else{
		$tournament_permission = 0;
	}
}else{
	$tournament_permission = 0;
}

$sql = "SELECT member_id FROM tournament_permissions WHERE tournament_id = $tournament_id ";
$rs_permission = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_permission)){
	$permission_umpires[] = $row['member_id'];
}

$where = '';//var_dump($_GET);
if(isset($_POST['start_day']) && !empty($_POST['start_day'])){
	$where.=" AND tm.start_day = '".trim($_POST['start_day'])."'";
}
if(isset($_POST['start_month']) && !empty($_POST['start_month'])){
	$where.=" AND MONTH(tm.start_time) = '".trim($_POST['start_month'])."'";
}
if(isset($_POST['team_group']) && !empty($_POST['team_group'])){
	$where.=" AND (tt1.group_name = '".trim($_POST['team_group'])."' OR tt2.group_name = '".trim($_POST['team_group'])."') ";
}
if(isset($_POST['tournament_team']) && !empty($_POST['tournament_team'])){
	$where.=" AND (tm.team1 = '".trim($_POST['tournament_team'])."' OR tm.team2 = '".trim($_POST['tournament_team'])."') ";
}
if(isset($_POST['tournament_venue']) && !empty($_POST['tournament_venue'])){
	$where.=" AND tm.venue_id = '".trim($_POST['tournament_venue'])."'";
}

/*$sql = "SELECT count(*) as record_count FROM tournament_matches as tm 
inner join companies as c1 on tm.team1=c1.id  
inner join companies as c2 on tm.team2=c2.id
inner join tournament_teams as tt1 on tm.team1 = tt1.team_id
inner join tournament_teams as tt2 on tm.team2 = tt2.team_id
left join users as u1 on tm.umpire1_id = u1.id
 WHERE tm.tournament_id = $tournament_id and tt1.tournament_id = $tournament_id and tt2.tournament_id = $tournament_id and tm.status = 1 $where";
$rs_total = mysqli_query($conn,$sql);
$row_total = mysqli_fetch_assoc($rs_total);
$records_count = $row_total['record_count'];*/

$sql = "SELECT tm.*,c1.company_name as team1,c2.company_name as team2,u1.f_name as umpire1_fname, u1.last_name as umpire1_lname,tt1.group_name as team1_group,
tt2.group_name as team2_group,lv.venue,c3.company_name as umpire1_from,u2.f_name as umpire2_fname, u2.last_name as umpire2_lname,c4.company_name as umpire2_from,
u3.f_name as umpire3_fname, u3.last_name as umpire3_lname,c5.company_name as umpire3_from,u4.f_name as scorer1_fname, u4.last_name as scorer1_lname,
u5.f_name as scorer2_fname, u5.last_name as scorer2_lname,u6.f_name as referee_fname, u6.last_name as referee_lname, l1.umpire_phone as umpire1_phone,
l2.umpire_phone as umpire2_phone, l3.umpire_phone as umpire3_phone, l4.umpire_phone as scorer_1_phone, l5.umpire_phone as scorer_2_phone, l6.umpire_phone as referee_phone
FROM tournament_matches as tm 
inner join companies as c1 on tm.team1=c1.id  
inner join companies as c2 on tm.team2=c2.id 
inner join tournament_teams as tt1 on tm.team1 = tt1.team_id
inner join tournament_teams as tt2 on tm.team2 = tt2.team_id
left join users as u1 on tm.umpire1_id = u1.id
left join companies as c3 on tm.umpire1_from = c3.id
left join users as u2 on tm.umpire2_id = u2.id
left join companies as c4 on tm.umpire2_from = c4.id
left join users as u3 on tm.umpire3_id = u3.id
left join companies as c5 on tm.umpire3_from = c5.id
left join users as u4 on tm.scorer_1 = u4.id
left join users as u5 on tm.scorer_2 = u5.id
left join users as u6 on tm.referee_id = u6.id 
left join league_venues as lv on tm.venue_id = lv.id 
left join league_umpires as l1 on l1.umpire_id = tm.umpire1_id
left join league_umpires as l2 on l2.umpire_id = tm.umpire2_id
left join league_umpires as l3 on l3.umpire_id = tm.umpire3_id
left join league_umpires as l4 on l4.umpire_id = tm.scorer_1
left join league_umpires as l5 on l5.umpire_id = tm.scorer_2
left join league_umpires as l6 on l6.umpire_id = tm.referee_id 
 WHERE tm.tournament_id = $tournament_id and tt1.tournament_id = $tournament_id and tt2.tournament_id = $tournament_id
 and tm.status = 1 $where 
 ORDER BY tm.start_time asc
 LIMIT $start_record,$records_per_page";
$rs_matches = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_matches)){
	$matches[] = $row;
}

$sql = "SELECT tt.group_name,c.id,c.company_name as team_name FROM tournament_teams as tt inner join companies as c on tt.team_id=c.id  
WHERE tt.tournament_id = $tournament_id and c.status = '1' ORDER BY c.company_name";
$rs_teams = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_teams)){
	$tournament_teams[] = $row;
	if(!in_array($row['group_name'],$groups)){
		$groups[] = $row['group_name'];
	}
}

$sql = "SELECT id,venue FROM league_venues WHERE league_id = ".$tournament_info['league_id']." ORDER BY venue";
$rs_teams = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_teams)){
	$venues[] = $row;
}

?>

	
		<h1> Tournament Schedule </h1>
		<h2> Tournament: <?php echo $tournament_info['title']; ?>  </h2>
			<?php if(!empty($error)): ?>
				<?php echo $error; ?>
			<?php endif; ?>
			
			<div class="panel panel-default">
              <div class="panel-body">
                <div class="form-group">
                  <div class="col-sm-12">
                    <h3>  </h3>
                  </div>
                </div>
                <form class="form-horizontal" id="schedule-filters"  action="">
                  <div class="form-group">
                    
                    <div class="col-sm-2">
                      <select name="start_day" id="start_day" class="form-control">
                        <option value="0">All Days</option>
						<?php for($i=0;$i<count($week_days);$i++): ?>
							<?php if(isset($_POST['start_day']) && $_POST['start_day'] == $week_days[$i]) $sel = 'selected';else $sel = ''; ?>
							<option <?php echo $sel; ?> value="<?php echo $week_days[$i]; ?>"><?php echo $week_days[$i]; ?></option>
						<?php endfor; ?>
                      </select>
                    </div>
					<div class="col-sm-2">
                      <select name="start_month" id="start_month" class="form-control">
                        <option value="0">All Months</option>
						<?php for($i=0;$i<count($months);$i++): ?>
							<?php if(isset($_POST['start_month']) && $_POST['start_month'] == $i+1) $sel = 'selected';else $sel = ''; ?>
							<option <?php echo $sel; ?> value="<?php echo $i+1; ?>"><?php echo $months[$i]; ?></option>
						<?php endfor; ?>
                      </select>
                    </div>
					<div class="col-sm-2">
                      <select name="team_group" id="team_group" class="form-control">
                       <option value="0">All Groups</option>
						<?php for($i=0;$i<count($groups);$i++): ?>
							<?php if(isset($_POST['team_group']) && $_POST['team_group'] == $groups[$i]) $sel = 'selected';else $sel = ''; ?>
							<option <?php echo $sel; ?> value="<?php echo $groups[$i]; ?>"><?php echo $groups[$i]; ?></option>
						<?php endfor; ?>
                      </select>
                    </div>
					<div class="col-sm-2">
                      <select name="tournament_team" id="tournament_team" class="form-control">
						<option value="0">All Teams</option>
						<?php for($i=0;$i<count($tournament_teams);$i++): ?>
							<?php if(isset($_POST['tournament_team']) && $_POST['tournament_team'] == $tournament_teams[$i]['id']) $sel = 'selected';else $sel = ''; ?>
							<option <?php echo $sel; ?> value="<?php echo $tournament_teams[$i]['id']; ?>"><?php echo $tournament_teams[$i]['team_name']; ?></option>
						<?php endfor; ?>
                      </select>
                    </div>
					<div class="col-sm-2">
                      <select name="tournament_venue" id="tournament_venue" class="form-control">
                        <option value="0">All Venues</option>
						<?php for($i=0;$i<count($venues);$i++): ?>
							<?php if(isset($_POST['tournament_venue']) && $_POST['tournament_venue'] == $venues[$i]['id']) $sel = 'selected';else $sel = ''; ?>
							<option <?php echo $sel; ?> value="<?php echo $venues[$i]['id']; ?>"><?php echo $venues[$i]['venue']; ?></option>
						<?php endfor; ?>
                      </select>
                    </div>
					<div class="col-sm-2">
                      <input name="schedule_search" id="schedule_search" value="Search" class="btn orange hvr-float-shadow" type="button">
                    </div>
                  </div>
                 
                  
                </form>
              </div>
            </div>
		
			<div class="row">
              <div class="col-sm-12">
			  
                <div class="table-responsive">
                  <table class="table table-bordered table-condensed table-hover table-striped table-sm">
                    <thead>
                      <tr>
                        <th>Game #</th>
                        <th>Date/Time</th>
                        <th>Day</th>
                        <th>Host Team</th>
                        <th>Group</th>
                        <th>Guest Team</th>
                        <th>Group</th>
                        <th>Venue</th>
                        <th>1st Umpire</th>
                        <th>Phone No</th>
                        <th>Umpire1 From</th>
                        <th>2nd Umpire</th>
                        <th>Phone No</th>
                        <th>Umpire2 From</th>
                        <th>3rd Umpire</th>
                        <th>Phone No</th>
                        <th>Umpire3 From</th>
                        <th>1st Scorer</th>
                        <th>Phone No</th>
                        <th>2nd Scorer</th>
                        <th>Phone No</th>
                        <th>Match Referee</th>
                        <th>Phone No</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                    
					<?php for($i=0;$i<count($matches);$i++): ?>
					<tr>
						<td><?php echo $matches[$i]['game_number']; ?></td>
						<td><?php echo date('d M Y',strtotime($matches[$i]['start_time'])); ?><br/><?php echo date('H:i:s',strtotime($matches[$i]['start_time'])); ?> </td>
						<td><?php if(!empty($matches[$i]['start_day'])) echo substr($matches[$i]['start_day'],0,3);else echo date('D',strtotime($matches[$i]['start_time'])); ?> </td>
						<td><?php echo $matches[$i]['team1']; ?></td>
						<td><?php echo $matches[$i]['team1_group']; ?></td>
						<td><?php echo $matches[$i]['team2']; ?></td>
						<td><?php echo $matches[$i]['team2_group']; ?></td>
						<td><?php echo $matches[$i]['venue']; ?></td>
						<td><?php echo $matches[$i]['umpire1_fname']; ?> <?php echo $matches[$i]['umpire1_lname']; ?></td>
						<td><?php if(in_array($matches[$i]['umpire1_id'],$permission_umpires)) echo $matches[$i]['umpire1_phone']; ?> </td>
						<td><?php if($matches[$i]['umpire1_from'] == -1) echo 'Independent'; else echo $matches[$i]['umpire1_from_name']; ?></td>
						<td><?php echo $matches[$i]['umpire2_fname']; ?> <?php echo $matches[$i]['umpire2_lname']; ?></td>
						<td><?php if(in_array($matches[$i]['umpire2_id'],$permission_umpires)) echo $matches[$i]['umpire2_phone']; ?> </td>
						<td><?php if($matches[$i]['umpire2_from'] == -1) echo 'Independent'; else echo $matches[$i]['umpire2_from_name']; ?></td>
						<td><?php echo $matches[$i]['umpire3_fname']; ?> <?php echo $matches[$i]['umpire3_lname']; ?></td>
						<td><?php if(in_array($matches[$i]['umpire3_id'],$permission_umpires)) echo $matches[$i]['umpire3_phone']; ?> </td>
						<td><?php if($matches[$i]['umpire3_from'] == -1) echo 'Independent'; else echo $matches[$i]['umpire3_from_name']; ?></td>
						<td><?php echo $matches[$i]['scorer1_fname']; ?> <?php echo $matches[$i]['scorer1_lname']; ?></td>
						<td><?php if(in_array($matches[$i]['scorer_1'],$permission_umpires)) echo $matches[$i]['scorer_1_phone']; ?> </td>
						<td><?php echo $matches[$i]['scorer2_fname']; ?> <?php echo $matches[$i]['scorer2_lname']; ?></td>
						<td><?php if(in_array($matches[$i]['scorer_2'],$permission_umpires)) echo $matches[$i]['scorer_2_phone']; ?> </td>
						<td><?php echo $matches[$i]['referee_fname']; ?> <?php echo $matches[$i]['referee_lname']; ?></td>
						<td><?php if(in_array($matches[$i]['referee_id'],$permission_umpires)) echo $matches[$i]['referee_phone']; ?> </td>
						<td>
						<?php if($tournament_permission): ?>
							<a class="text-info" href="<?php echo WWW; ?>tournament/matches/match-header/<?php echo $matches[$i]['tournament_id']; ?>/<?php echo $matches[$i]['id']; ?>" title="Match Header"> <i class="fa fa-tv"></i></a>
                          <?php if(empty($matches[$i]['completely_abandon_due_to']) || in_array($matches[$i]['completely_abandon_due_to'],$abandon_array)): ?>	
							<a class="text-info" href="<?php echo WWW; ?>tournament/matches/batting/<?php echo $matches[$i]['tournament_id']; ?>/<?php echo $matches[$i]['id']; ?>/1" title="Scoreboard Innings 1"><i class="fa fa-bar-chart"></i></a>
							<a class="text-info" href="<?php echo WWW; ?>tournament/matches/batting/<?php echo $matches[$i]['tournament_id']; ?>/<?php echo $matches[$i]['id']; ?>/2" title="Scoreboard Innings 2" ><i class="fa fa-bar-chart-o"></i></a>
						<?php else: ?>	
							<a href="javascript:;" class="text-info" onclick="alert('Match Abandoned due to <?php echo $matches[$i]['completely_abandon_due_to']; ?>');" title="Scoreboard Innings 1"><i class="fa fa-bar-chart"></i></a>
							<a href="javascript:;" class="text-info" onclick="alert('Match Abandoned due to <?php echo $matches[$i]['completely_abandon_due_to']; ?>');" title="Scoreboard Innings 2" ><i class="fa fa-bar-chart-o"></i></a>
						<?php endif; ?>
						<a class="text-info" href="<?php echo WWW; ?>tournament/matches/edit/<?php echo $matches[$i]['tournament_id']; ?>/<?php echo $matches[$i]['id']; ?>" title="Match Highlights"><i class="fa fa-youtube-play"></i> </a>
						<a class="text-info" href="<?php echo WWW; ?>tournament/matches/scoresheet/<?php echo $matches[$i]['id']; ?>" title="View Scoresheet"><i class="fa fa-file-excel-o"></i></a>
						<br/>
						<?php if(empty($matches[$i]['completely_abandon_due_to']) || in_array($matches[$i]['completely_abandon_due_to'],$abandon_array)): ?>	
							<a class="text-info" href="<?php echo WWW; ?>tournament/matches/commentary/<?php echo $matches[$i]['id']; ?>/<?php echo $matches[$i]['batting_team1']; ?>"  title="Commentary Innings 1"> <i class="fa fa-microphone"></i></a>
							<a class="text-info" href="<?php echo WWW; ?>tournament/matches/commentary/<?php echo $matches[$i]['id']; ?>/<?php echo $matches[$i]['batting_team2']; ?>" title="Commentary Innings 2"> <i class="fa fa-microphone"></i></a>
						<?php else: ?>
							<a href="javascript:;" class="text-info" onclick="alert('Match Abandoned due to <?php echo $matches[$i]['completely_abandon_due_to']; ?>');"  title="Commentary Innings 1"> <i class="fa fa-microphone"></i></a>
							<a href="javascript:;" class="text-info" onclick="alert('Match Abandoned due to <?php echo $matches[$i]['completely_abandon_due_to']; ?>');" title="Commentary Innings 2"> <i class="fa fa-microphone"></i></a>
						<?php endif; ?>
						<a class="text-info" href="<?php echo WWW; ?>tournament/matches/gallery/<?php echo $matches[$i]['id']; ?>" title="Photo Gallery"><i class="fa fa-camera"></i></a>
						<a  href="<?php echo WWW; ?>tournament/matches/videos/list/<?php echo $matches[$i]['id']; ?>" title="Videos"><i class="fa fa-video-camera"></i></a>
					<?php else: ?>	
						<a class="text-info" href="<?php echo WWW; ?>tournament/matches/scoresheet/<?php echo $matches[$i]['id']; ?>" title="View Scoresheet"><i class="fa fa-file-excel-o"></i></a>
					<?php endif; ?>
                        </td>
					</tr>
					
					<?php if($i && ($i+1)%9 ==0): ?>
						<thead><tr><th>Game<br>Nbr</th><th>Date/Time</th><th>Day</th><th>Host Team</th><th>Group</th><th>Guest Team</th><th>Group</th><th>Venue</th><th>1st Umpire</th><th>Phone No</th><th>Umpire1 From</th>
						<th>2nd Umpire</th><th>Phone No</th><th>Umpire2 From</th><th>3rd Umpire</th><th>Phone No</th><th>Umpire3 From</th><th>1st Scorer</th><th>Phone No</th><th>2nd Scorer</th><th>Phone No</th><th>Match Referee</th><th>Phone No</th><th>Actions</th></tr></thead>
					<?php endif; ?>
					
					<?php endfor; ?>
                 
                   <?php if(empty($matches)): ?>
						<tr><td colspan="20">No Records</td></tr>
					<?php endif; ?>
					
					</tbody>
                  </table>
                </div>
				
              </div>
				
            </div>
				
	
