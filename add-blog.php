<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
<?php 
	$row = mysqli_fetch_assoc(mysqli_query($conn,"select * from blog_articles where id = '".$_GET['id']."'"));
?>
	
		<? 	if(isset($_POST) && !empty($_POST)){ 
				$title 	= $_POST['title'];
				if(isset($_GET['id'])){
					$title .= ' '.$row['title'];
				}
				$content= $_POST['content'];
				$category_id = (isset($_POST['category_id']))?$_POST['category_id']:$row['category_id'];
				$series_match_id= $_POST['series_match_id'];
				$date 		= date('Y-m-d H:i:s');
				$user_id 	= $_SESSION['ycdc_dbuid'];
				$isGlobal = ($_POST['is_global'] == 'on')?'1':'0';
				$espnLink = $_POST['espn_link'];
				if(empty($series_match_id)){
					$series_match_id = 0;
				}
				if(empty($_POST['record_count'])){
					$record_count = 0;
				}
				if(empty($_POST['is_featured'])){
					$is_featured = '';
				}
				if(!isset($user_id) || empty($user_id)){
					echo '<div id="error" class="alert alert-danger">Please login first ... !</div>';
				}else{
					if(!empty($title) && !empty($content)){
						
						$rs_blog = mysqli_query($conn,"select * from blog_articles where category_id = '$category_id' and title = '$title'");
						if(mysqli_num_rows($rs_blog) == 0){
							$query = "insert into blog_articles (title, content, status, create_date, user_id, category_id, is_global, espn_link, series_match_id,record_count,is_featured) 
							values ('$title','$content','1','$date','$user_id','$category_id','$isGlobal','$espnLink', '$series_match_id','$record_count','$is_featured')";

							if(mysqli_query($conn,$query)){
								$blog_id = mysqli_insert_id($conn);

								if(!is_dir('blog/'.$blog_id)){
									mkdir('blog/'.$blog_id,0777);
								}
								chmod('blog/'.$blog_id,0777);

								if(!empty($_FILES['photo']['name'])){
									$filename 	= friendlyURL($title).'.jpg';
									$image 		= new SimpleImage();
									$image->load($_FILES["photo"]["tmp_name"]);
									$image->save('blog/'.$blog_id.'/'.$filename);
									chmod('blog/'.$blog_id.'/'.$filename,0777);
								
									mysqli_query($conn,"insert into photos (file_name, title, entity_id, entity_type) values ('$filename','$title','$blog_id','blog')");
								}
							}

			                echo '<div id="success" class="alert alert-success">Blog Information added successfully ... !</div>';
			                ?>
			                <script>
			                window.location = '<?=WWW?>my-blog.html';
			                </script>
			                <?
						}else{
							echo '<div id="error" class="alert alert-danger">Information already exists ... !</div>';
						}
					}else{
						echo '<div id="error" class="alert alert-danger">Please fill all fields ... !</div>';
					}
				}
				
			}
			
		?>
		


<script src="<?=WWW;?>assets/js/ckeditor/ckeditor.js"></script>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1> Add Blog <?php echo (isset($_GET['id']))?' - '.$row['title']:'';?> </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <form method="post" action="" enctype="multipart/form-data" class="form-horizontal" id="form-add-blog">
            <input type="hidden" name="series_match_id" value="<?php echo $row['series_match_id']?>">
            <div class="white-box">
              <div class="form-group">
                <div class="col-sm-12 text-center">
                  <h2>Blog Information</h2>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Blog Category</label>
                <div class="col-sm-9">
					<?php if($row){ ?>
						<label><?=get_combo('blog_categories','name',$row['category_id'],'', 'text')?></label>
						<?php }else{ ?>
						<?=get_combo('blog_categories','name',$row['category_id'],'category_id')?>
					<?php } ?>	
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Is Global</label>
                <div class="col-sm-9">
                  <input type="checkbox" name="is_global" <?php echo ($row['is_global'] == '1')?'checked="checked"':'';?>>
                  <span class="help-block">Blog will be Regional Blog If checked</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">ESPN Link</label>
                <div class="col-sm-9">
                  <input name="espn_link" value="<?php echo $row['espn_link']?>" class="form-control" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Title</label>
                <div class="col-sm-9">
                  <input name="title" value="" class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Content</label>
                <div class="col-sm-9">
                  <div id="editor">
					<textarea name="content" id="content"  class="ckeditor" class="validate[required]"></textarea>
				  </div>
                </div>
              </div>
              
              <div class="form-group">
                <label class="col-sm-3 control-label">Blog Photo</label>
                <div class="col-sm-9">
                  <input accept="image/*" name="photo" type="file">
                  <span class="help-block">Max Photo size : 2MB</span>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12 text-center">
                  <input value=" Save " class="btn orange hvr-float-shadow" type="submit">
                  <a id="cancel" class="btn blue hvr-float-shadow" href="<?=WWW?>blogs.html">Cancel</a>
                </div>
              </div>
            </div>
		  </form>
        </div>
        
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
              <?php include('common/right-panel.php');?>
            </div>
          </div>
          
        </div>
      </div>
    </div><!-- /.container -->
	
<script type="text/javascript">
$(document).ready(function(){
	$('#form-add-blog').validationEngine();
});
</script>	
<?php include('common/footer.php'); ?>