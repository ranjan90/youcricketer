<?php include('common/header.php'); ?>

	
	<?php 
		$row = get_record_on_id('events',$_GET['id']);
		$row_img= mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'events' and entity_id = '".$row['id']."' order by id desc limit 1"));
	?>
	<div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1>
            Event Detail - <?=get_combo('events','title',$_GET['id'],'','text')?> <br>
            <span> <strong>Created By :</strong> <?=get_combo('users','f_name',$row['user_id'],'','text')?>&nbsp;<?=get_combo('users','last_name',$row['user_id'],'','text')?>
			<strong>Created On :</strong> <?=date_converter($row['create_date'])?>
			<strong>Start Date :</strong> <?=date_converter($row['start_date'])?> <strong>End Date :</strong> <?=date_converter($row['end_date'])?> </span>
          </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <div class="white-box">
            <div class="row">
              <div class="col-sm-3">
                
				<img src="<?=($row_img)?WWW.'events/'.$row['id'].'/'.$row_img['file_name']:WWW.'images/community-groups.png';?>" alt="<?=$row['title']?>" title="<?=$row['title']?>" class="img-responsive" />
              </div>
              <div class="col-sm-9">
                <?=($rpw['id']!='2')?$row['content']:'';?>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
              <?php include('common/right-panel.php');?>
            </div>
          </div>
          
        </div>
      </div>
    </div><!-- /.container -->
	
<?php include('common/footer.php'); ?>