<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>

<? } ?>
	<div class="middle">
		<h1>Add Press Release </h1>
		<? 	if(isset($_POST) && !empty($_POST)){ 
				$title 	= $_POST['title'];
				$content= $_POST['content'];
				$summary= $_POST['summary'];
				$date 		= date('Y-m-d H:i:s');
				$user_id 	= $_SESSION['ycdc_dbuid'];

				if(!isset($user_id) || empty($user_id)){
					echo '<div id="error">Please login first ... !</div>';
				}else{
					if(!empty($title) && !empty($content)){
						
						$rs_blog = mysqli_query($conn,"select * from press_releases where title = '$title'");
						if(mysqli_num_rows($rs_blog) == 0){
							$query = "insert into press_releases (title, summary, content, status, release_date) 
							values ('$title','$summary','$content','1','$date')";

							mysqli_query($conn,$query);
			                echo '<div id="success">PressRelease Information added successfully ... !</div>';
			                ?>
			                <script>
			                window.location = '<?=WWW?>press-releases.html';
			                </script>
			                <?
						}else{
							echo '<div id="error">Information already exists ... !</div>';
						}
					}else{
						echo '<div id="error">Please fill all fields ... !</div>';
					}
				}
				
			}
		?>
				<div class="white-box content">
			<form method="post">
				<fieldset>
					<h2>Press Release Information</h2>
					<div class="form-box">
						<label>Title</label>
						<input type="text" name="title" style="width: 253px;"class="input-login validate[required]">
					</div>
					<div class="clear"></div>
					<label>Summary</label>
					<div style="width: 600px; display: block; margin-left:130px;">
						<textarea maxlength="500" name="summary" style="width: 200px;" cols="0" rows="10" class="ckeditor validate[required]"></textarea>
					</div>
					<div class="clear"></div>
					<label>Content</label>
					<div style="width: 600px; display: block; margin-left:130px;">
						<textarea maxlength="500" name="content" style="width: 200px;" cols="0" rows="10" class="ckeditor validate[required]"></textarea>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<input type="submit" value=" Save " class="submit-login">
					</div>
					<div class="form-box">
						<a id="cancel" class="submit-login" style="background: none !important;position:relative; left:-150px; top:10px;" href="<?=WWW?>press-releases.html">Cancel</a>
					</div>
				</fieldset>
			</form>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>
<?php include('common/footer.php'); ?>