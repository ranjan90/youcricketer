<?php include_once('includes/configuration.php');
$page = 'market-place.html';
$selected_country = getGeoLocationCountry();
$is_user_a_company = 0;

$square_msg='';

///// PAyment Code
if(isset($_POST['submit'])){
    if(!empty($_SESSION['ycdc_dbuid'])){

        $rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select users.*,countries.name as country from users JOIN countries ON users.country_id = countries.id where users.id = '".$_SESSION['ycdc_dbuid']."'"));
        $id = (int) $_POST['item'];
        $rowItem = mysqli_fetch_assoc(mysqli_query($conn,"select * from marke_place_items where item_id = $id"));

    }
    if(!empty($_POST['quantity'])) {
        $quantity = (int)$_POST['quantity'];
    } else {
        $quantity = 1;
    }

    if($_POST['payment_method'] == 'square') {

      /* Stor data into session */
      $_SESSION['square']['item']=$_POST['item'];
      $_SESSION['square']['price']=$_POST['price'];
      $_SESSION['square']['quantity']=$quantity;

      /* client location id*/
      $location="CBASECUjDAZuTF_vJ0UVtWTJtJIgAQ";

      /* Post Url*/
      $url="https://connect.squareup.com/v2/locations/".$location."/checkouts";

      /* Amount that user need to pay*/
      $amount=$rowItem['item_price']*100;

      /* Authorization headers */
      $header[]="Authorization : Bearer sandbox-sq0atb-EUScP628qDebNAxU0oYJIg";
      $header[]="Content-Type : application/json";

       /* post data*/
       $var='{"redirect_url":"http://dev1.youcricketer.com/market-place.html?square_callback=yes","idempotency_key":"'.uniqid().'","ask_for_shipping_address":true,"merchant_support_email":"anujsetia@1wayit.com","order":{"reference_id":"Test","line_items":[{"name":"Anuj","quantity":"'.$quantity.'","base_price_money":{"amount":'.$amount.',"currency":"USD"}}]}}';

        /* curl */
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$var);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        /* response */
        $response =json_decode(curl_exec ($ch));
        curl_close ($ch);

        /* if return checkout url then it redirect to on that url else show error msg*/
        if(isset($response->checkout->checkout_page_url)){

            echo "<script>window.location.href='".$response->checkout->checkout_page_url."'</script>";

        }else{
          $square_msg="The Amount must be greater than 100.";
        }

    }else if($_POST['payment_method'] == 'paypal') { ?>

        <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" id="paypal-sub">
            <input type="hidden" name="cmd" value="_xclick">
            <input type="hidden" name="business" value="shujakhan2000-facilitator@hotmail.com"> <!--  sales@youcricketer.com-->
            <input type="hidden" name="item_name" value="<?= $rowItem['item_title'] ?>">
            <input type="hidden" name="item_number" value="<?= $rowItem['item_id'] ?>">
            <input type="hidden" name="custom" value="<?= $rowUser['id'].'-'.$rowItem['is_sponsor'] ?>">
            <input type="hidden" name="amount" value="<?= $rowItem['item_price'] ?>">
            <input type="hidden" name="quantity" value="<?=$quantity?>">
            <input type="hidden" name="currency_code" value="USD">
            <input type='hidden' name='email' value='<?= $rowUser['email'] ?>'/>
            <!-- Enable override of buyers's address stored with PayPal . -->

            <!-- Set variables that override the address stored with PayPal. -->
            <input type="hidden" name="first_name" value="<?= $rowUser['first_name']?>">
            <input type="hidden" name="last_name" value="<?= $rowUser['last_name'] ?>">
            <input type="hidden" name="address1" value="">
            <input type="hidden" name="city" value="">
            <input type="hidden" name="state" value="">
            <input type="hidden" name="zip" value="">
            <input type="hidden" name="country" value="<?= $rowUser['country'] ?>">
            <input type="hidden" name="return" value="<?=WWW."market-place.html?payment_status=success"?>">
            <input type="hidden" name="cancel_return" value="<?=WWW."market-place.html?payment_status=cancel"?>">

            <input type="hidden" name="notify_url" value="<?=WWW."notify_paypal_url.php"?>">

        </form>
        <script>
            document.getElementById("paypal-sub").submit();
        </script>
    <?php }

    /*if($_POST['payment_method'] == 'twocheckout') {
        ?>


        <form action='https://sandbox.2checkout.com/checkout/purchase' method='post'
              id="2checkout-sub">
            <input type='hidden' name='sid' value='901354914'/>
            <input type='hidden' name='uid' value='<?= $rowUser['id'].'-'.$rowItem['is_sponsor'] ?>'/>
            <input type='hidden' name='mode' value='2CO'/>
            <input type='hidden' name='li_0_type' value='<?= $rowItem['item_id'] ?>'/>
            <input type='hidden' name='itd' value='<?= $rowItem['item_id'] ?>'/>
            <input type='hidden' name='li_0_name' value='<?= $rowItem['item_title'] ?>'/>
            <input type='hidden' name='li_0_price' value='<?= $rowItem['item_price'] ?>'/>
            <input type='hidden' name='li_0_quantity' value='<?= $quantity  ?>'/>
            <input type='hidden' name='card_holder_name' value='<?= $rowUser['first_name'] . ' ' . $rowUser['last_name'] ?>'/>
            <input type='hidden' name='street_address' value='<?= $rowUser['street_address'] ?>'/>
            <input type='hidden' name='street_address2' value=''/>
            <input type='hidden' name='city' value=''/>
            <input type='hidden' name='state' value='OH'/>
            <input type='hidden' name='zip' value=''/>
            <input type='hidden' name='country' value='<?= $rowUser['country'] ?>'/>
            <input type='hidden' name='email' value='<?= $rowUser['email'] ?>'/>
            <input type='hidden' name='phone' value='<?= $rowUser['phone'] ?>'/>
            <input type='hidden' name='x_receipt_link_url' value='<?= WWW ?>success_2checkout_or.php'/>


        </form>
        <script>
            document.getElementById("2checkout-sub").submit();
        </script>

        <?php
    }*/


}
  /* square callback url */
  if(isset($_GET['square_callback']) && $_GET['square_callback']=='yes'){

      if(isset($_SESSION['square']['item']) && $_SESSION['square']['item']!=''){

          mysqli_query($conn,"insert into market_place_transaction(item_id,quantity,price,transaction_id,type)values('".$_SESSION['square']['item']."','".$_SESSION['square']['quantity']."','".$_SESSION['square']['price']."','".$_GET['transactionId']."','square')");

          unset($_SESSION['square']);

          echo "<script>window.location.href='http://dev1.youcricketer.com/market-place.html?payment_status=success'</script>";

      }else{
        $square_msg="Not Record Found";

      }

  }

$league_info = array();
$error = '';

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
    $rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
    $_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
    $user_id = $_SESSION['ycdc_dbuid'];
    $user_info = get_record_on_id('users', $user_id);
    if($user_info['is_company'] == 1){
        $is_user_a_company = 1;
        $sql = "select * from companies where user_id = $user_id and company_type_id IN(1,2,3,35) and status=1";
        $rs_company = mysqli_query($conn,$sql);
        if(mysqli_num_rows($rs_company)){
            $league_info = mysqli_fetch_assoc($rs_company);
        }
    }
}

if(isset($_GET['action']) && $_GET['action'] == 'delete'){
    $id = $_GET['id'];
    $arr1 = explode(',',$league_info['by_laws_file']);$arr2 = explode(',',$league_info['by_laws_file_orig']);
    for($i=0;$i<count($arr1);$i++){
        if($i == $id){
            unlink('by-laws-uploads/'.$arr1[$i]);
            continue;
        }
        $files1.=$arr1[$i].',';
        $files2.=$arr2[$i].',';
    }

    $files1 = rtrim($files1,',');
    $files2 = rtrim($files2,',');
    $sql = " UPDATE companies SET by_laws_file='$files1',by_laws_file_orig='$files2' WHERE user_id='".$user_id."'";
    mysqli_query($conn,$sql);
    $_SESSION['file_deleted'] = 1;
    header("Location:".WWW."league-by-laws.html");
    exit();
}

if(isset($_POST['submit_btn']) && !empty($_POST['submit_btn'])){

    if(empty($error)){

        for($i=0;$i<count($_FILES['by_laws_file']['name']);$i++){
            $targetFolder = 'by-laws-uploads';
            $tempFile = $_FILES['by_laws_file']['tmp_name'][$i];
            $targetPath = $targetFolder;
            $fileParts = pathinfo($_FILES['by_laws_file']['name'][$i]);
            $fileName = $fileParts['filename'].'_'.rand(99,9999).time().'.'.$fileParts['extension'];
            $targetFile = $targetPath . '/' .$fileName ;
            $file_orig = $fileParts['filename'].'.'.$fileParts['extension'];
            if(!move_uploaded_file($tempFile,$targetFile)){
                $error= 'Error in uploading File';
            }else{
                $file1.=$fileName.',';
                $file2.=$file_orig.',';
            }
        }

        $file1 = rtrim($file1,',');
        $file2 = rtrim($file2,',');
        if(!empty($league_info['by_laws_file'])){
            $file1 = $league_info['by_laws_file'].','.$file1;
        }
        if(!empty($league_info['by_laws_file_orig'])){
            $file2 = $league_info['by_laws_file_orig'].','.$file2;
        }

        $sql = " UPDATE companies SET by_laws_file =  '$file1',by_laws_file_orig = '$file2' ,tournament_data_added = 'yes' WHERE user_id='".$user_id."'";



        if(empty($error) && mysqli_query($conn,$sql)){
            $_SESSION['file_uploaded'] = 1;
        }else{
            $error = '<p id="error" class="alert alert-danger">Error in adding File. Try again later</p>';
        }

        if(empty($error)){
            $_SESSION['file_uploaded'] = 1;
            header("Location:".WWW."league-by-laws.html");
            exit();
        }else{
            $error = '<p id="error" class="alert alert-danger">Error in adding File. Try again later</p>';
        }
    }
}

if(isset($_SESSION['file_uploaded']) && $_SESSION['file_uploaded'] ==1){
    $file_uploaded = 1;
    unset($_SESSION['file_uploaded']);
}
if(isset($_SESSION['file_deleted']) && $_SESSION['file_deleted'] ==1){
    $file_deleted = 1;
    unset($_SESSION['file_deleted']);
}

?>
<?php include('common/header.php'); ?>

    <div class="page-container">
        <?php include('common/user-left-panel.php');?>
        <!-- END SIDEBAR -->

        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">


                <div class="white-box" style="overflow: hidden !important;">
                    <?php if( $is_user_a_company == 1 ){ ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <h2> Youcricketer Market </h2>

                            <div class="col-sm-12">

                                <?php if(isset($_GET['payment_status']) && $_GET['payment_status'] == 'success'): ?>
                                    <div id="information" class="alert alert-success">Your Payment has been done... !</div>
                                <?php  endif; ?>

                                <?php if(isset($square_msg) && $square_msg != ''): ?>
                                    <div id="information" class="alert alert-danger"><?php echo $square_msg;?></div>
                                <?php  endif; ?>

                                <?php if(isset($_GET['payment_status']) && $_GET['payment_status'] == 'cancel'): ?>
                                    <div id="information" class="alert alert-danger">Your Payment has been cancelled... !</div>
                                <?php  endif; ?>

                                <?php if(isset($_SESSION['my-sponosr-payment']) && $_SESSION['my-sponosr-payment'] == 1): ?>
                                    <div id="information" class="alert alert-success">Your Payment has been done... !</div>
                                    <?php unset($_SESSION['my-sponosr-payment']); endif; ?>

                                <?php if(isset($_SESSION['my-sponosr-payment']) && $_SESSION['my-sponosr-payment'] == 0): ?>
                                    <div id="information" class="alert alert-danger">Your payment is failed.... !</div>
                                    <?php unset($_SESSION['my-sponosr-payment']); endif; ?>
                            </div>

                        </div>
                    </div>
                    <?php
                    $query   = "SELECT *,if(is_sponsor = 1 ,10,0) as sponserOrder FROM marke_place_items ORDER BY sponserOrder DESC";
                    $result  = mysqli_query($conn,$query);
                    if(!empty($result)){
                    ?>

                    <?php $i=1;
                    while( $row = mysqli_fetch_assoc($result)){
                        if($i == 1){
                            ?>
                            <div class="row" style="border:1px;" ><!--Row Start-->
                        <?php } ?>
                            <div class="col-xs-12 col-lg-6 col-md-6">
                                <div class="mrkt_plc_outter">
                                    <div class="pull-left mrkt_plc_name">
                                        <h5><?php echo $row['item_title']?></h5>
                                        <span><?php echo "<b>Price</b> $".$row['item_price']?></span>
                                    </div>
                                    <div class="pull-right">
                                      <form method="POST" action="<?=$_SERVER['PHP_SELF']?>" id="2checkout-<?=$row['item_id']?>-id" class="payment_form_submit" style="display: none">
                                          <input type="hidden" name="submit">
                                          <select name="quantity" class="update-on-price" style="width:40%">
                                              <?php for($val=1; $val<100;$val++){
                                                  echo "<option value='$val'>$val</option>";
                                              }
                                              ?>
                                          </select>
                                          <input type="hidden" name="title" class="title" value="square">
                                          <input type="hidden" name="price" class="price" value="<?=$row['item_price']?>">
                                          <input type="hidden" name="item" value="<?=$row['item_id']?>">
                                          <input type="hidden" name="type" value="Square">
                                          <input type="hidden" name="payment_method" value="square">
                                          <input type="submit" class="btn btn-sm btn-success" value="Pay">
                                          <input type="button" class="btn btn-sm btn-danger dismiss" data-dismiss="modal" value="Cancel">
                                          <!-- <a href="#" id="submit-payment-form"> <img width="250" height="80" src="<?/*=WWW*/?>assets/img/2checkout.png"> </a> -->
                                          <!-- <input type="submit" value="2checkout" class="btn btn-sm btn-danger payment_modal " >-->
                                      </form>
                                        <button class="btn btn-sm btn-danger add_payment" id="2checkout-<?=$row['item_id']?>" data-toggle="mddodal" data-target="#myModal">Square</button>

                                        <form method="POST" action="<?=$_SERVER['PHP_SELF']?>" id="paypal-<?=$row['item_id']?>-id" class="payment_form_submit"  style="display:none">
                                            <select name="quantity" class="update-on-price" style="width:40%">
                                                <?php for($val=1; $val<100;$val++){
                                                    echo "<option value='$val'>$val</option>";
                                                }
                                                ?>
                                            </select>
                                            <input type="hidden" name="submit">
                                            <input type="hidden" name="title" class="title" value="PayPal">
                                            <input type="hidden" name="price" class="price" value="<?=$row['item_price']?>">
                                            <input type="hidden" name="type" value="paypal">
                                            <input type="hidden" name="payment_method" value="paypal">
                                            <input type="hidden" name="item" value="<?=$row['item_id']?>">
                                            <input type="submit" class="btn btn-sm btn-success" value="Pay">
                                            <input type="button" class="btn btn-sm btn-danger dismiss" data-dismiss="modal" value="Cancel">
                                            <!-- <a href="#" id="submit-payment-form"> <img width="250" height="80" src="<?/*=WWW*/?>assets/img/2checkout.png"> </a>-->

                                        </form>
                                        <button class="btn btn-sm btn-primary add_payment" id="paypal-<?=$row['item_id']?>" data-toggle="mddodal" data-target="#myModal">Paypal</button>
                                    </div>
                                    <div class="clearfix"></div>
                                    <p style="font-size: 13px;width:98%; line-height:16px;"> <?=$row['item_detail']?> </p>
                                </div>
                        </div>
                        <?php $i++;
                        if($i > 2){?>
                            </div> <!--Row End-->

                            <?php $i = 1; }
                    } ?>

                    <?php } else {
                        echo "<p> No Item available.</p>";
                    } } else {
                        echo "<p> Coming Soon! </p>";

                    }?>
                </div>
            </div>
        </div>


        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="sd"></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-1"> Quantity : </div>
                            <div class="col-lg-4 form-here"> </div>
                            <div class="col-lg-1"> Price : </div>
                            <div class="col-lg-4 price-here"></div>
                            <div class="col-lg-4 price-orgnial" style="display: none"></div>
                        </div>
                    </div>
                   <!-- <div class="modal-footer">
                        <button type="button"  class="btn btn-success click-to-submit">Pay Now</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>-->
                </div>

            </div>
        </div>


        <script>
            $(document).ready(function(){
                $("#submit-payment-form").on('click', function(e){
                    $("form#payment_form_submit").submit(function(){

                        return true;
                    });$("form#payment_form_submit").submit();
                    e.preventDefault();

                });
                $(".click-to-submit").on('click',function(){
                   // alert( $("#myModal").find('form').attr('id') ); //$(this).parent('div').parent('div').find('form#submit_payment');//find('form').submit();
                   // form.submit();
                    alert($("#submit_payment").serialize());//.submit();
                });

                $(".dismiss").bind('click', function(e){
                    e.preventDefault();
                });
                $(".add_payment").bind('click',function(){
                    var formId = "";
                        formId = this.id;
                    var divObj = $("#myModal");
                    var Form   = $(this).closest('div').find('#'+formId+"-id");
                   // Form.attr('id','submit_payment');
                    var title  = Form.find('.title').val();
                    var price  = Form.find('.price').val();
                    divObj.find('.modal-title').html("You've selected '<b>"+title+" </b>' payment method.");
                    divObj.find('.price-here').text('$'+price);
                    divObj.find('.price-orgnial').text(price);
                    divObj.find('.form-here').html('');

                    Form.clone(true,true).appendTo(divObj.find('.form-here'));
                    divObj.find('form').show();


                    divObj.modal('show');

                });

                $(".update-on-price").change(function(){
                    var priceObj = $(this).closest('div').parent('div').find('.price-here');
                    var orgPrice = $(this).closest('div').parent('div').find('.price-orgnial');
                    var price = orgPrice.text();
                    priceObj.html('$'+$(this).val() * price);
                });



            });

        </script>
<?php include('common/footer.php'); ?>
