<? include('common/header.php'); ?>
<?php
 if(isset($_GET['id'])){
		$id 	= $_GET['id'];
		$row 	= get_record_on_id('users', $id);
		$user_data = $row;
	}else if(isset($_GET['permalink'])){
    $per=explode('/',$_SERVER['REQUEST_URI']);

    $permalink 	= $per[2];
		$row = mysqli_fetch_assoc(mysqli_query($conn,"select * from users where permalink = '$permalink' and is_company='0'"));
		$user_data = $row;
		$id=$user_data['id'];
	}
	if($row=='' or count($row)<=0){

		my_redirect(WWW.'404.php');
	}

  $user_id = $id;
  $sqlSponsors= mysqli_query($conn,"select * from my_sponsors where user_id = '$user_id' ORDER BY sort ASC ");

  $payment = mysqli_fetch_assoc(mysqli_query($conn,"select * from user_sponsor_payment WHERE user_id = $user_id  AND status = 1 ORDER BY user_sponsor_id DESC"));


?>

<? $row_i= mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_id = '$id' and entity_type = 'users' and is_default = '1'"));?>
<? $row_v= mysqli_fetch_assoc(mysqli_query($conn,"select * from videos where entity_id = '$id' and entity_type = 'users' and is_default = '1'"));?>
<?


$row_city = get_record_on_id('cities', $row['city_id']); ?>
<? $row_states = get_record_on_id('states', $row['state_id']); ?>
<? $row_c= get_record_on_id('countries',$row['country_id']);?>
<? $fields_privacy = explode(',', $row['privacy_settings']); ?>

<?		$location   =  $row['city_name'].' > '.$row_states['name'].' > '.$row_c['name'];

?>
		<? 	$fromUserId = $_SESSION['ycdc_dbuid'];
			$toUserId 	= $id;
			if(isset($_POST) && $_POST['action'] == 'invite'){
				$date 		= date('Y-m-d H:i:s');

				$rsChk  = mysqli_query($conn,"select * from friendship where (from_user_id = '$toUserId' and to_user_id = '$fromUserId') or (from_user_id = '$fromUserId' and to_user_id = '$toUserId');");
				if(mysqli_num_rows($rsChk) == 0){
					$query 	= "insert into friendship (from_user_id, to_user_id, create_date, status)
								values ('$fromUserId','$toUserId','$date','0');";
					if(mysqli_query($conn,$query)){
						echo '<div id="success" class="alert alert-success">Invitation sent ... !</div>';
					}else{
						echo '<div id="error" class="alert alert-danger">Invitation cannot be sent ... !</div>';
					}
				}
			}
			if(isset($_POST) && $_POST['action'] == 'message'){
				$subject 	= $_POST['subject'];
				$message 	= $_POST['comments'];
				$from_user  = $_SESSION['ycdc_dbuid'];
				$to_user 	= $id;
				$date 		= date('Y-m-d H:i:s');

				if($from_user == $to_user){
					echo '<div id="error" class="alert alert-danger">You are not allowed to send message to your self ... !</div>';
				}else{
					if(!empty($from_user)){
						$rs_chk = mysqli_query($conn,"select * from messages where from_user_id = '$from_user' and to_user_id = '$to_user' and subject = '$subject' and message = '$message'");
						if(mysqli_num_rows($rs_chk) == 0){
							$query = "insert into messages (from_user_id, to_user_id, subject, message, status, message_date)
							values ('$from_user','$to_user','$subject','$message','0','$date')";
							mysqli_query($conn,$query);
							echo '<div id="success" class="alert alert-success">Message sent ... !</div>';
						}
					}
				}
			}
?>
<?php if($fields_privacy){ $fields = $fields_privacy; } else { $fields[] = '';}?>


    <div class="container-fluid">
      <div class="profile">
        <div class="row">
			<div class="col-md-12">
				<ul id="<?php if($row['user_type_id'] == 2){ ?>individual-menu<?php }else{ ?>individual-other-menu<?php } ?>" class="menu individual-menu" style="display:block; height: 30px;">
				<?php /* ?>  <li><a href="#" id="activity-tab" class="active">Activity</a></li>
				  <li class="indv-photo-li"><a href="#" id="images-tab">Photos</a></li>
				  <li class="indv-photo-li"><a href="#" id="videos-tab">Videos</a></li>
				  <li class="indv-photo-li"><a href="#" id="traits-tab">Traits</a></li>
				  <li><a href="#" id="affiliations-tab">Affiliations</a></li>
				  <li><a href="#" id="experiences-tab">Experience</a></li>
				  <?php  if($row['user_type_id'] == 2){ ?>
					<li><a href="#" id="statistics-tab">Statistics</a></li>
				  <?php } ?>
				  <li><a href="#" id="testimonials-tab">Testimonials</a></li>
				  <li><a href="#" id="sponsors-tab">Sponsors</a></li>
				  <li class="seek-opp-li"><a href="#" id="opportunities-tab">Seeking Opportunities</a></li>
				  <li class="travel-details-li"><a href="#" id="travel-requirements-tab">Travel Details</a></li>
				  <li><a href="#" id="personal-data-tab">Personality</a></li>
				  <li><a href="#" id="contact-tab">Contact</a></li><?php */ ?>
				  <li class="profile-top-nav" style="width:100%;">&nbsp;</li>
				</ul>
			</div>

			<div class="clear" style="height:6px;">&nbsp;</div>

          <div class="col-md-2 profile-img">
            <img style="max-height:330px;" src="<?=WWW?><?=($row_i)?'users/'.$id.'/photos/'.$row_i['file_name']:'images/no-photo.png'?>" alt="<?=$row['f_name'].' '.$row['m_name'].' '.$row['last_name']?>">
          </div>
          <div class="col-md-5 profile-info">
            <div class="row">
              <div class="col-sm-12">
                <img src="<?=WWW?>countries/<?=$row_c['flag']?>" title="<?=$row_c['name']?>" class="country">
                <h2> <div class="star_panel">
				<? $row_user = mysqli_fetch_assoc(mysqli_query($conn,"select * from users where id = '$id'")); ?>
					<? if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){ $user_id = $_SESSION['ycdc_dbuid'];?>
					<? 	$row_fav = mysqli_fetch_assoc(mysqli_query($conn,"select * from favorite_users where player_id = '$id' AND user_id = '$user_id'"));
						if($row_fav){
					?><a class="right star" style="cursor:pointer" id="<?=$_SESSION['ycdc_dbuid'];?>-<?=$id?>-<?php echo "remove";?>"  title="Remove Favorite"><img src="<?=WWW?>images/golden-star.png"></a><?
						}else{
					?><a class="right star" style="cursor:pointer" id="<?=$_SESSION['ycdc_dbuid'];?>-<?=$id?>-<?php echo "add";?>" title="Add to Favorite"><img src="<?=WWW?>images/silver-star.png"></a><?
					}
				} ?>
			</div>

					<?=$row['f_name']?>&nbsp;<?=$row['last_name'];?>

				</h2>
                <p><strong>Location:</strong> <?=$location?></p>
                <p><strong>Timezone: </strong><?=get_combo('timezones','title',$row['timezone'],'','text')?></p>
                <p><strong>Gender: </strong><?=($row['gender'] == '1')?'Male':'Female';?></p>
				<?
				$dob = date_converter($row['date_of_birth'],'M')." ".date_converter($row['date_of_birth'],'d');
				if(in_array('year_of_birth', $fields)){

					$dob.=", ".	date_converter($row['date_of_birth'],'Y');
				}?>
                <p><strong>Birthday:</strong> <?=$dob?></p>
                <p><strong><b>Favorited By: </b></strong> <?php
					$fav = mysqli_query($conn,"SELECT * FROM favorite_users WHERE player_id = '$id'");
					$count = mysqli_num_rows($fav);
					echo $count;
				?>
				</p>
				<?php $rol = mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM users WHERE id = '$id'"));
				$type_id  = $rol['user_type_id'];
				$user_rol = mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM user_types WHERE id = '$type_id'"));
				?>
                <p><strong>Individual Role: </strong> <?=$user_rol['name']?> </p>
				<? if($user_rol['name'] == 'Player' ){  ?>
					<? if($rol['type'] == 'Batsman'){
						   $usr_type = "Batsman";
					}?>
					<?  if($rol['type'] == 'Bowler'){
							$usr_type = "Bowler";
						}
						if($rol['type'] == 'All Rounder'){
							$usr_type = "All Rounder";
						}
						if($rol['type'] == 'Specialist Wicketkeeper'){
							$usr_type = "Specialist Wicketkeeper";
						}
					 ?>

				<p><strong>Type of Player:</strong><? echo $usr_type; ?></p>
					<? if($rol['type'] == 'Batsman'){ ?>
						<p> <strong>Batting Hand: </strong> <?=$rol['type_of_batsman']?></p>
						<p> <strong>Batting Order:</strong><?=$rol['batting_order']?></p>
					<? }else if($rol['type'] == 'Bowler'){ ?>
						<p><strong>Bowler Type: </strong> <?=$rol['type_of_bowler']?></p>
						<p><strong>Bowler Arm: </strong> <?=$rol['which_arm_bowler']?></p>
					<? } ?>

				<p><strong>Preferred Match Type:</strong> <?=$rol['match_types']?></p>
				<? } ?>

				<? if($rol['first_class_player'] == 1){ ?>
				<p ><strong>First Class Category:</strong> Yes</p>
				<? } else {  } ?>

				<? $opp = mysqli_query($conn,"SELECT * FROM opportunities WHERE user_id = '$id' AND type='domestic'");
				$rowO = mysqli_num_rows($opp);
				if($rowO > 0) {?>
					<p style="<?=$p3?>"><strong>Actively Seeking Domestic Opportunity:</strong> <? echo "Yes" ?> </p>
				<? } ?>

				<?php $opp = mysqli_query($conn,"SELECT * FROM opportunities WHERE user_id = '$id' AND type='international'");
				$rowO = mysqli_num_rows($opp);
				if($rowO > 0) {?>
				<p><strong>Actively Seeking International Opportunity:</strong> <? echo "Yes" ?></p>
				<? } ?>

              </div>
            </div>
            <div class="row">
              <div class="col-sm-5">
				<?php /*if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){ ?>
					<? if($fromUserId != $toUserId){ ?>
						<? 	$rsInvite = mysqli_query($conn,"select * from friendship where from_user_id = '$fromUserId' and to_user_id = '$toUserId'");
							if(mysqli_num_rows($rsInvite) == 0){ ?>
						<form method="post">
							<input type="hidden" name="action" value="invite">
							<input  type="submit" value="Invite" id="contact-btn" title="Invite to increase your network" class="submit-login btn orange full hvr-float-shadow">
						</form>
						<? }else{
							$rowInvite = mysqli_fetch_assoc($rsInvite);
							if($rowInvite['status'] = '1'){
								echo '<span><b>Already Friend</b></span>';
							}else{
								echo '<span><b>Invitation Sent</b></span>';
							}
						} ?>
					<? } ?>

				<?php }else{ ?>
					<button  id="contact-btn1" class="submit-login popup-link btn orange full hvr-float-shadow">Invite Friend</button>
				<?php }*/ ?>
			</div>



			<?php /* ?>
            <div class="col-sm-5">
                <? if(isset($_SESSION['ycdc_dbuid']) && $_SESSION['ycdc_dbuid'] != $_GET['id']){ ?>
					<a  id="contact-btn" href="javascript:;" onclick='$(".menu").find("a").removeClass("active");$("#contact-tab").addClass("active");$(".content1").hide();$("#contact").show();$(".white-box_listting").find(".menu").find("a").removeClass("active");$(".white-box_listting").find("#contact-tab").addClass("active");' class="submit-login popup-link btn orange full hvr-float-shadow">Contact</a>
					<? }else if(!isset($_SESSION['ycdc_dbuid'])){ ?>
					<button  id="contact-btn1" onclick='$(".menu").find("a").removeClass("active");$("#contact-tab").addClass("active");$("#contact-message").removeClass("hide");$(".white-box_listting").find(".menu").find("a").removeClass("active");$(".white-box_listting").find("#contact-tab").addClass("active");' class="submit-login popup-link btn orange full hvr-float-shadow">Contact</button>
				<? } ?>
            </div>
			<?php */ ?>

            </div>
          </div>
          <div class="col-md-3 profile-contact">
            <div id="last-login" style="display:none;"> </div>

			<? $video = $row_v['file_name'];
				if(!empty($video)){
					if(preg_match('/<iframe(.*)<\/iframe>/', $row_v['file_name'])){
						preg_match('/src="(.*?)"/',$row_v['file_name'] , $src);
						$src = $src[1];
						$video = "<div class='embed-responsive embed-responsive-16by9'><iframe width='365' height='320' allowfullscreen src='$src'></iframe></div>";
					}else{
						$video_path = WWW.'videos/'.$row_v['id'].'/'.$row_v['file_name'];
							$video = '<video width="365" height="320" controls>
									  <source src="'.$video_path.'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
									  <source src="'.$video_path.'" type=\'video/ogg; codecs="theora, vorbis"\'>
									  <source src="'.$video_path.'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
									  <source src="'.$video_path.'" type=\'video/webm; codecs="vp8, vorbis"\'>
									</video>';
					}
				}else{
					$video = '<img src="'.WWW.'assets/img/no-video.jpg" >';
				}
			echo $video;
			?>

          </div>

    		  <div class="col-md-2">
            <h3 class='spo-text text-center'> <?=$row['f_name']?>'s Sponsors</h3>
			<div class="vertical_panel">
            <?php
            $date1=date_create(date("d-m-Y"));
            $date2=date_create(date("d-m-Y",strtotime($payment['date_end'])));
            $diff=date_diff($date1,$date2);
            $diffdays = $diff->format("%R%a");
            if(mysqli_num_rows($sqlSponsors)>0 && $diffdays >= 0) {
              while($record=mysqli_fetch_assoc($sqlSponsors)){?>
                  <div class="v-img">
            				<a href="<?php echo $record['url']; ?>" target="_blank"><img src="<?=WWW?>my-sponsors/<?php echo $record['file_info']; ?>" alt="<?php echo $record['title']; ?>" title="<?php echo $record['title']; ?>"/></a>
            			</div>
                <?php }
              }else{
                ?>
                <div class="v-img">
                  <img src="<?=WWW?>images/sp2.jpg" alt="Dummy" title="Dummy"/>
                </div>
                <div class="v-img">
                  <img src="<?=WWW?>images/sp4.jpg" alt="Dummy" title="Dummy"/>
                </div>
                <div class="v-img">
                  <img src="<?=WWW?>images/sp3.jpg" alt="Dummy" title="Dummy"/>
                </div>

                <div class="v-img">
                  <img src="<?=WWW?>images/sp1.jpg" alt="Dummy" title="Dummy"/>
                </div>

              <?php }
              ?>

    		   </div>

			   </div>
        </div>
      </div>

      <?php
      $ulClas='';
      $uCquery=mysqli_query($conn,"select * from user_types where status='1'");

      while($rec=mysqli_fetch_assoc($uCquery)){
        if($row['user_type_id']==$rec['id']){
            $ulClas=strtolower(str_replace(" ","-",$rec['name'])).'-ul';
        }

      }

      ?>

      <div class="row">
        <div class="col-md-12">
          <div class="white-box_listting form-horizontal">
            <ul id="<?php if($row['user_type_id'] == 2){ ?>individual-menu<?php }else{ ?>individual-other-menu<?php } ?>" class="menu individual-menu <?php echo $ulClas;?>" style="display:block; height: 30px;">
              <li><a href="#" id="activity-tab" class="active my-show-add" data-show="1">Activity</a></li>
              <li class="indv-photo-li"><a href="#" class="my-show-add"  data-show="1" id="images-tab">Photos</a></li>
              <li class="indv-photo-li"><a href="#" class="my-show-add"  data-show="1" id="videos-tab">Videos</a></li>
              <li class="indv-photo-li"><a href="#" class="my-show-add"  data-show="1"  id="traits-tab">Traits</a></li>
              <li><a href="#" id="affiliations-tab" class="my-show-add"  data-show="1" >Affiliations</a></li>
              <li><a href="#" id="experiences-tab" class="my-show-add"  data-show="1" >Experience</a></li>
			  <?php  if($row['user_type_id'] == 2){ ?>
				<li><a href="#" id="statistics-tab" class="my-show-add"  <?php if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])) echo 'data-show="0"';else  echo 'data-show="1"'; ?> >Statistics</a></li>
			  <?php } ?>
              <li><a href="#" id="testimonials-tab" class="my-show-add"  data-show="1" >Testimonials</a></li>
              <!--<li><a href="#" id="sponsors-tab">Sponsors</a></li>-->
              <li class="seek-opp-li"><a href="#" id="opportunities-tab" class="my-show-add"  data-show="1" >Seeking Opportunities</a></li>
              <li class="travel-details-li"><a href="#" id="travel-requirements-tab" class="my-show-add"  data-show="1" >Travel Details</a></li>
              <li><a href="#" id="personal-data-tab" class="my-show-add"  data-show="1" >Personality</a></li>
              <li><a href="#" id="contact-tab" class="my-show-add"  data-show="1" >Contact</a></li>
			  <li><a href="#" id="invite-tab" class="my-show-add"  data-show="1" >Invite</a></li>
            </ul>
			<div id="profile_details" class="row">
				<div class="col-md-10 ads-10">
					<?php $rowMsg = get_record_on_id('cms', 21);  ?>
			<div class="clearfix" style="height:10px;"></div>
      <?php $ubo = mysqli_fetch_assoc(mysqli_query($conn, "select status from varifications where user_id=".$id)); ?>
			<? if(!isset($_SESSION['ycdc_user_name']) || empty($_SESSION['ycdc_user_name'])){ ?>
				<div class="clear"></div>
				<div id="contact-message" class="hide">
				<div id="success" class="alert alert-info"><b>Please Login or Register!</b></div>
				<p><?php $rowWhoopsMsg = get_record_on_id('cms', 23); echo $rowWhoopsMsg['content'];?></p>
				<? include('common/login-box.php');?>
				</div>
				<script>
        $("#contact-message").removeClass('hide');
				$('.menu li a,#menu6_1 li a').on('click', function(){
					$('#contact-message').removeClass('hide');
				});

				$('#contact-btn1').on('click', function(){
					$('#contact-message').removeClass('hide');
				});
				</script>

			<?php }elseif($ubo['status']==0){ ?>
        <div class="alert alert-danger"><b>This user is not activated yet...!</b></div>
      <?php }else{ ?>
            <div id="activity" class="content1">
              <h2 style="margin:10px 0px"> Activity </h2>
			   <? if(in_array('Status/Activity', $fields)){ ?>
			  <?php $rs_msg = mysqli_query($conn,"select * from activity where user_id =".$row['id']." and activity not like '%Logged in%' order by id desc limit 15");
					if(mysqli_num_rows($rs_msg) <= 0){
			        			?>
			        			<div id="error" class="information">No Activity</div>
			        			<?
					}else{
				        		while($row_msg = mysqli_fetch_assoc($rs_msg)){
				        			$rowUserActivity= get_record_on_id('users', $row_msg['user_id']);
				        			$row_img 		= mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$rowUserActivity['id']."' and is_default = '1' "));
				        			$rowStatusLike	= mysqli_query($conn,"select * from status_likes where activity_id = '".$row_msg['id']."' and user_id = '$userId'");
				        			$rsStatusLike 	= mysqli_query($conn,"select * from status_likes where activity_id = '".$row_msg['id']."'");
				        			$rsStatusShare 	= mysqli_query($conn,"select * from status_shares where activity_id = '".$row_msg['id']."'");
				        			$rsStatusComment= mysqli_query($conn,"select * from activity_comments where activity_id = '".$row_msg['id']."'");
				        			if($rowStatusLike){
				        				$imgTitle 	= 'Loved It';
				        			}else{
				        				$imgTitle 	= 'Love It';
				        			}
				        			$loversData 	= array();
				        			$sharersData 	= array();
				        			$commentData 	= array();
				        			if(mysqli_num_rows($rsStatusComment) > 0){
				        				while($rowStatusComment = mysqli_fetch_assoc($rsStatusComment)){
				        					$commentData[]    = $rowStatusComment['user_id'];
				        				}
				        				$commentCounter = '('.mysqli_num_rows($rsStatusComment).')';
				        			}else{
				        				$commentCounter = '';
				        			}
				        			if(mysqli_num_rows($rsStatusLike) > 0){
				        				while($rowStatusLike = mysqli_fetch_assoc($rsStatusLike)){
				        					$loversData[]    = $rowStatusLike['user_id'];
				        				}
				        				$loveCounter = '('.mysqli_num_rows($rsStatusLike).')';
				        			}else{
				        				$loveCounter = '';
				        			}
				        			if(mysqli_num_rows($rsStatusShare) > 0){
				        				while($rowStatusShare = mysqli_fetch_assoc($rsStatusShare)){
				        					$sharersData[]    = $rowStatusShare['user_id'];
				        				}
				        				$sharersCounter = '('.mysqli_num_rows($rsStatusShare).')';
				        			}else{
				        				$sharersCounter = '';
				    }
				    ?>

              <div class="panel panel-default" id="dashboard-activity">
                <div class="panel-heading">
                  <div class="row">
                    <div class="col-sm-1">
                      <img style="max-width:40px;padding:0px;" src="<?php echo WWW;?><?=($row_img && !empty($row_img['file_name']))?'users/'.$rowUserActivity['id'].'/photos/'.$row_img['file_name']:'images/no-photo.jpg'?>" class="img-responsive">
                    </div>
                    <div class="col-sm-10">
                      <h3><?php echo $rowUserActivity['f_name'].' '.$rowUserActivity['last_name'];?></h3>
                      <span id="dashboard-datetime"><?=date_converter($row_msg['activity_date'])." ".date("H:i:s",strtotime($row_msg['activity_date']))?></span>
                    </div>
                  </div>
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="col-sm-8">
                     <?=$row_msg['activity']?>
                    </div>
                    <div class="col-sm-4" id="gallery">
						<? 	$rsPhotos = mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_msg['user_id']."' and title = '".$row_msg['activity']."'");
				        						$rsVideos = mysqli_query($conn,"select * from videos where entity_type = 'users' and entity_id = '".$row_msg['user_id']."' and title = '".$row_msg['activity']."'");
				        						if(mysqli_num_rows($rsPhotos) > 0){
				        							$rowPhoto = mysqli_fetch_assoc($rsPhotos);
				        						?>
				        						<a href="<?=WWW?>users/<?=$row_msg['user_id']?>/photos/<?=$rowPhoto['file_name']?>" class="photo">
				        							<img  src="<?=WWW?>users/<?=$row_msg['user_id']?>/photos/<?=$rowPhoto['file_name']?>" class="img-responsive">
				        						</a><?
				        						}
				        						if(mysqli_num_rows($rsVideos) > 0){
				        							$rowVideo = mysqli_fetch_assoc($rsVideos);
				        							if(!empty($rowVideo['file_name'])){
				        								$filename = explode('.',$rowVideo['file_name']);
														$filename1= $filename[0];
														$video = '<video width="220" height="130" controls>
																  <source src="'.WWW.'videos/'.$rowVideo['id'].'/'.$filename1.'.mp4" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
																  <source src="'.WWW.'videos/'.$rowVideo['id'].'/'.$filename1.'.ogg" type=\'video/ogg; codecs="theora, vorbis"\'>
																  <source src="'.WWW.'videos/'.$rowVideo['id'].'/'.$filename1.'.mov" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
																  <source src="'.WWW.'videos/'.$rowVideo['id'].'/'.$filename1.'.webm" type=\'video/webm; codecs="vp8, vorbis"\'>
																</video>';
														echo $video;
				        							}
				        						}
				        ?>
                    </div>
                  </div>
                </div>

				</div>
			  <?  } } ?>

			<? }else{ ?>
					<? 	echo '<div id="information" class="alert alert-info">'.$rowMsg['content'].'</div>';?>
			<? } ?>

            </div> <!--./activity-->
            <div id="images" class="content1" style="display: none;">
              <h2 style="margin:10px 0px"> Photos </h2>
			  <? if(in_array('photos_Albums', $fields)){ ?>
              <div id="gallery">
                <div class="row text-center">
				<? 	$rs_imgs = mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '$id' and is_default = ''");
						while($row_img = mysqli_fetch_assoc($rs_imgs)){ ?>
					<div class="col-sm-3">
						<a class="match-gallery-link cboxElement" href="<?=WWW?>users/<?=$id?>/photos/<?=$row_img['file_name']?>">
						<img src="<?=WWW?>users/<?=$id?>/photos/<?=$row_img['file_name']?>" alt="<?=$_SESSION['ycdc_user_name']?>" class="img-responsive"></a><br>
                    </div>
				<?  } ?>

				</div>
              </div>
			<? }else{ ?>
					<? 	echo '<div id="information" class="alert alert-info">'.$rowMsg['content'].'</div>';?>
			<? } ?>
            </div><!--./photos-->
            <div id="videos" class="content1" style="display: none;">
              <h2 style="margin:10px 0px"> Videos </h2>
			<? if(in_array('videos_Albums', $fields)){ ?>
              <div id="gallery">
			  <div class="row">
				<? 	$rs_imgs = mysqli_query($conn,"select * from videos where entity_type = 'users' and entity_id = '$id' and is_default = '0' ");
						if(mysqli_num_rows($rs_imgs) > 0){
							while($row_img = mysqli_fetch_assoc($rs_imgs)){
								$video = $row_img['file_name'];

					if(!empty($video)){
							if(preg_match('/<iframe(.*)<\/iframe>/', $row_img['file_name'])){

											preg_match('/src="(.*?)"/',$row_img['file_name'] , $src);
											$src = $src[1];
											$video = "<iframe width='240' height='200' allowfullscreen src='$src'></iframe>";


							}else{
								$video_path = WWW.'videos/'.$row_img['id'].'/'.$row_img['file_name'];
								$video = '<video width="240" height="200" controls>
									  <source src="'.$video_path.'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
									  <source src="'.$video_path.'" type=\'video/ogg; codecs="theora, vorbis"\'>
									  <source src="'.$video_path.'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
									  <source src="'.$video_path.'" type=\'video/webm; codecs="vp8, vorbis"\'>
									</video>';
							}
					}else{
						$video = '<img src="'.WWW.'images/no-video.jpg" width="365" height="340">';
					}

								?>
								<div class="col-sm-4">
									<div class="embed-responsive embed-responsive-4by3">
										<? echo $video;?>
									</div>
								</div>
					<?php } ?>
					<? }else{ ?>
					<p>No Video .... !</p>
					<? } ?>

                </div>
              </div>

				<? }else{ ?>
					<? 	echo '<div id="information" class="alert alert-info">'.$rowMsg['content'].'</div>';?>
				<? } ?>
            </div><!--./videos-->

			<div id="traits" style="display: none;" class="content1">
              <h2 style="margin:10px 0px"> Traits </h2>
				<? if($rol['first_class_player'] == 1){ ?>
					<p><strong>First Class</strong> (<a target="_blank" href="<?=$row['espncrickinfo']?>">ESPN Profile</a>) </p>
					<? } ?>
					<? if($row['type'] == 'Bowler'){ ?>

					<p><strong>Bowling Preference in One Day Games:</strong> <?=$row['bowl_between_overs']?></p>
					<? } ?>
					<?php if($row['game_types']!=''){?>
						<p><strong>Games Types:</strong> <?=$row['game_types']?></p>
					<?php } ?>

					<?php if($rol['first_class_player']!='1' && $row['type'] != 'Bowler' && $row['game_types']==''){?>
						<div class="alert alert-info" id="information">No Traits Found</div>
					<?php } ?>

            </div><!--./traits-->


            <div style="display: none;" id="affiliations" class="content1">
              <h2 style="margin:10px 0px"> Affiliations </h2>
              <h3>Clubs</h3>
			  <? $rsClubs = mysqli_query($conn,"select c.title,utc.* from users_to_clubs utc join clubs c on c.id = utc.club_id where utc.user_id = '".$row['id']."' order by utc.sort asc");?>
              <div class="table-responsive">
                <table class="table table-bordered table-condensed table-striped">
                  <thead>
                    <tr>
                      <th width="25%">Name</th>
                      <th width="25%">Preference</th>
                      <th width="25%">Affiliation Date</th>
                      <th width="25%">Youcricketer Date &amp; Time</th>
                    </tr>
                  </thead>
                  <tbody>
                   <?
				   if(mysqli_num_rows($rsClubs)>0){
				   while($rowClub = mysqli_fetch_assoc($rsClubs)){ ?>
							<tr><td ><a href="advance-search.html?type=All&country=All&city=&companyname=<?=$rowClub['title']?>&yearofservice=All&company=Search#companies-tab"><?=$rowClub['title']?></a> </td>
							<td><?=$rowClub['sort']?> </td>
							<td><?=$rowClub['affiliation_date']?> </td>
							<td><?=$rowClub['date_added']?> </td></tr>
						<? }
					   }else{
						   echo "<tr><td colspan='4' align='center'>No Record Found</td></tr>";
					   }
						?>
                  </tbody>
                </table>
              </div>
              <h3>Leagues</h3>
              <div class="table-responsive">
				<? $rsClubs = mysqli_query($conn,"select c.title, utc.* from users_to_leagues utc join leagues c on c.id = utc.league_id where utc.user_id = '".$row['id']."' order by utc.sort asc");?>
                <table class="table table-bordered table-condensed table-striped">
                  <thead>
                    <tr>
                      <th width="25%">Name</th>
                      <th width="25%">Preference</th>
                      <th width="25%">Affiliation Date</th>
                      <th width="25%">Youcricketer Date &amp; Time</th>
                    </tr>
                  </thead>
                  <tbody>
						<?
						if(mysqli_num_rows($rowClub)>0){
						while($rowClub = mysqli_fetch_assoc($rsClubs)){ ?>
							<tr><td ><a href="advance-search.html?type=All&country=All&city=&companyname=<?=$rowClub['title']?>&yearofservice=All&company=Search#companies-tab"><?=$rowClub['title']?></a> </td>
							<td><?=$rowClub['sort']?> </td>
							<td><?=$rowClub['affiliation_date']?> </td>
							<td><?=$rowClub['date_added']?> </td></tr>
						<? }
						}else{
						   echo "<tr><td colspan='4' align='center'>No Record Found</td></tr>";
					   } ?>
                  </tbody>
                </table>
              </div>

              <h3>Organizations</h3>
              <div class="table-responsive">
				<? $rsAgent = mysqli_query($conn,"select * from agents  where user_id = '".$row['id']."' order by sort asc");?>
                <table class="table table-bordered table-condensed table-striped">
                  <thead>
                    <tr>
                      <th width="25%">Name</th>
                      <th width="25%">Preference</th>
                      <th width="25%">Affiliation Date</th>
                      <th width="25%">Youcricketer Date &amp; Time</th>
                    </tr>
                  </thead>
                  <tbody>
					<?
					if(mysqli_num_rows($rsClubs)>0){
					while($rowClub = mysqli_fetch_assoc($rsAgent)){ ?>
						<tr><td><a href="advance-search.html?type=All&country=All&city=&companyname=<?=$rowClub['name']?>&yearofservice=All&company=Search#companies-tab"><?=$rowClub['name']?></a> </td>
						<td><?=$rowClub['sort']?> </td>
						<td><?=$rowClub['affiliation_date']?> </td>
						<td><?=$rowClub['date_added']?> </td></tr>
					<? }
					}else{
						echo "<tr><td colspan='4' align='center'>No Record Found</td></tr>";
					}
					?>
                  </tbody>
                </table>
              </div>
            </div><!--./affiliations-->

			<div id="experiences" class="content1" style="display: none;">
              <h2 style="margin:10px 0px"> Experiences </h2>
			  <? if(in_array('experiences', $fields)){ ?>
              <div class="form-group">
                <label class="col-sm-5 control-label">Currently Working:</label>
                <div class="col-sm-7">
                  <p class="form-control-static"><?=($row['currently_playing'] == '1')?'Yes':'No';?></p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-5 control-label">Years of Experience:</label>
                <div class="col-sm-7">
                  <p class="form-control-static"><?=$row['no_of_playing'];?></p>
                </div>
              </div>
			   <div class="form-group">
                <label class="col-sm-5 control-label">Overall experience:</label>
                <div class="col-sm-7">
                  <p class="form-control-static"><?=$row['other_information']?></p>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12">
                  <div class="table-responsive">
                    <table class="table table-bordered table-condensed table-hover table-striped">
                      <thead>
                        <tr>
                          <th>Country</th>
                          <th>Organization</th>
                          <th>Job Title</th>
                          <th>From Date</th>
                          <th>To Date</th>
                        </tr>
                      </thead>
                      <tbody>
						<? 	$rs_msg = mysqli_query($conn,"select * from experiences where user_id = '".$row_user['id']."' order by to_date desc ");
				        	if(mysqli_num_rows($rs_msg) == 0){
				        		?>
				        		<tr>
				        			<td colspan="3"><td>
				        		</tr>
				        		<?
				        	}else{
					       		while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
					       		<tr title="Click for detail">
					       			<td><?=get_combo('countries','name',$row_msg['country_id'],'','text')?></td>
					       			<td><?=$row_msg['organization']?></td>
					       			<td><?=$row_msg['job_title']?></td>
					       			<td><?=date_converter($row_msg['from_date'])?></td>
					       			<td><?=date_converter($row_msg['to_date'])?></td>
					       		</tr>
					    <?  } } ?>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
				<? }else{ ?>
					<? 	echo '<div id="information" class="alert alert-info">'.$rowMsg['content'].'</div>';?>
				<? } ?>
            </div><!--./experiences-->

			<?php  if($row['user_type_id'] == 2){ ?>
            <div id="statistics" class="content1" style="display:none;">
              <h2 style="margin:10px 0px"> Statistics </h2>
				<?php $game_type = array('Ten10'=>'Ten10','Fifteen15'=>'Fifteen15','Twenty20'=>'Twenty20','Thirty30'=>'Thirty30','One Day'=>'One Day');
					$overs_type = array('10'=>'10','15'=>'15','20'=>'20','30'=>'30','35'=>'35','40'=>'40','45'=>'45','50'=>'50','55'=>'55','60'=>'60');
					$ball_type = array('hard_ball'=>'Hard Ball','rubber_ball'=>'Rubber Ball','tape_ball'=>'Tape Ball','tennis_ball'=>'Tennis Ball');
				?>
              <div class="statistics-filters">
                <div class="form-group">
                  <div class="col-md-2">
					<?php $rs_leagues = mysqli_query($conn,'Select DISTINCT c.id,c.company_name from companies as c inner join tournaments as t on c.id=t.league_id ORDER BY c.company_name'); ?>
                    <select name="league_select_stats" id="league_select_stats" class="form-control">
						<option value="0">All Leagues</option>
						<?php while($row1 = mysqli_fetch_assoc($rs_leagues)): ?>
							<option value="<?php echo $row1['id']; ?>"><?php echo $row1['company_name']; ?></option>
						<?php endwhile; ?>
                    </select>
                  </div>
                  <div class="col-md-2">
                   <?php //$rs_teams = mysqli_query($conn,"Select max(year) as max_year, min(year) as min_year FROM tournaments as t inner join tournament_teams as tt on t.id=tt.tournament_id inner join tournament_bat_scorecard as tbs on tbs.team_id=tt.team_id WHERE tbs.batsman_id = ".$row['id']);
						$rs_teams = mysqli_query($conn,"Select max(year) as max_year, min(year) as min_year FROM tournaments as t inner join tournament_teams as tt on t.id=tt.tournament_id "); // for player who have not played any tournament
						$year_row = mysqli_fetch_assoc($rs_teams);?>
						<select name="year_select_stats" id="year_select_stats" class="form-control">
							<option value="0">All Years</option>
							<?php for($i=$year_row['min_year'];$i<=$year_row['max_year'];$i++): ?>
								<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
							<?php endfor; ?>
						</select>
                  </div>
                  <div class="col-md-2">
                    <?php /*$rs_tournaments = mysqli_query($conn,"Select t.id,t.title from tournaments as t inner join tournament_teams as tt on t.id=tt.tournament_id
						inner join tournament_bat_scorecard as tbs on tbs.team_id=tt.team_id
						WHERE tbs.batsman_id = ".$row['id'].' ORDER BY title');*/
						$rs_tournaments = mysqli_query($conn,"Select distinct t.id,t.title from tournaments as t inner join tournament_teams as tt on t.id=tt.tournament_id ORDER BY t.title");
						?>
						<select name="tournament_select_stats" id="tournament_select_stats" class="form-control">
						<option value="0">All Tournaments</option>
						<?php while($row1 = mysqli_fetch_assoc($rs_tournaments)): ?>
							<option value="<?php echo $row1['id']; ?>"><?php echo $row1['title']; ?></option>
						<?php endwhile; ?>
						</select>
                  </div>
                  <div class="col-md-2">
                    <select name="game_types_select_stats" id="game_types_select_stats" class="form-control">
							<option value="0">Game Type</option>
							<?php foreach($game_type as $key=>$val):  ?>
								<option <?php echo $sel; ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
							<?php endforeach; ?>
					</select>
                  </div>
                  <div class="col-md-2">
                    <select name="overs_type_select_stats" id="overs_type_select_stats" class="form-control">
							<option value="0">Overs</option>
							<?php foreach($overs_type as $key=>$val):  ?>
								<option <?php echo $sel; ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
							<?php endforeach; ?>
					</select>
                  </div>
                  <div class="col-md-2">
                    <select name="ball_type_select_stats" id="ball_type_select_stats" class="form-control">
							<option value="0">Ball Type</option>
							<?php foreach($ball_type as $key=>$val):  ?>
								<option <?php echo $sel; ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
							<?php endforeach; ?>
					</select>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12 text-center">
                    <input name="player_stats_search" id="player_stats_search" class="btn orange hvr-float-shadow" value="Search" type="button">
                  </div>
                </div>
              </div>

			  <div id="player-statistics-data"></div>
            </div><!--./statistics-->
			<?php } ?>

            <!--<div id="sponsors" class="content1" style="display: none;">
              <h2 style="margin:10px 0px"> Sponsors </h2>
              <!--<div class="alert alert-info">Page Under Construction</div>
              <?php

              $sqlSponsors= mysqli_query($conn,"select * from my_sponsors where user_id = '$user_id' ORDER BY sort ASC ") ;
              if(mysqli_num_rows($sqlSponsors)>0){
              while($record=mysqli_fetch_assoc($sqlSponsors)){
                ?>

                  <div class="col-sm-3">
      							<a href="<?php echo $record['url']; ?>"  target="_blank"><span class="drag_img_bx">
    		              <img src="<?=WWW?>my-sponsors/<?php echo $record['file_info']; ?>" alt="<?php echo $record['title']; ?>" title="<?php echo $record['title']; ?>"/></a>
                      <h4><a href="<?php echo $record['url']; ?>" target="_blank"><?php echo $record['title']; ?></a></h4>
                  </span>
                  </div>
              <?php }
            }else{
              echo "<div class='information'>No Sponsors</div>";
            } ?>

            </div><!--./sponsors-->

            <div id="testimonials" class="content1" style="display: none;">
              <h2 style="margin:10px 0px"> Testimonials </h2>
              <div class="row">
                <div class="col-sm-12">
                  <div class="list testimonials">
                    <ul>
					<?  $rpp = PRODUCT_LIMIT_FRONT; // results per page
            			$ppage = intval($_GET["page"]);
      					if($ppage<=0) $ppage = 1;
      					$query = "select * from testimonials where status=1 and entity_id =".$row_user['id'];
				      	$query .= " order by id desc ";

				      $rs   = mysqli_query($conn,$query);
				      $tcount = mysqli_num_rows($rs);
				      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
				      $count = 0;
				      $i = ($ppage-1)* $rpp;
          			  $x = 0;
          			  //=======================================
						while(($count<$rpp) && ($i<$tcount)){
                		mysqli_data_seek($rs,$i);
                		$row_t 	= mysqli_fetch_array($rs);
                		$row_i  = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_t['user_id']."' and is_default = '1'"));
                	?>
						<li>
							<div class="row">
							  <div class="col-sm-9 sep-right">
								<h3>
								  <?php if(!empty($row_i['file_name']) && file_exists('users/'.$row_t['user_id'].'/photos/'.$row_i['file_name'])){?>
							<img  src="<?=WWW?><?='users/'.$row_t['user_id'].'/photos/'.$row_i['file_name']?>" alt="<?=get_combo('users','concat(f_name," ",last_name)',$row_t['user_id'],'','text')?>" >
							<?php }else{ ?>
								<img  src="<?=WWW?>images/no-photo.png" >
							<?php } ?> <?=get_combo('users','concat(f_name," ",last_name)',$row_t['user_id'],'','text')?>
							for <?php $row_i  = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_t['entity_id']."' and is_default = '1'")); ?>
							 <?php if($row_i['file_name'] && file_exists('users/'.$row_t['entity_id'].'/photos/'.$row_i['file_name'])){?>
							<img  src="<?=WWW?><?='users/'.$row_t['entity_id'].'/photos/'.$row_i['file_name']?>" alt="<?=get_combo('users','concat(f_name," ",last_name)',$row_t['entity_id'],'','text')?>" >
							<?php } else { ?>
								<img  src="<?=WWW?>images/no-photo.png" >
							<?php } ?>
							<?=get_combo('users','concat(f_name," ",last_name)',$row_t['entity_id'],'','text')?> on <?=date_converter($row_t['testimonial_date'],'M d,Y H:i')?>
								</h3>
								<p><?=$row_t['content']?></p>
							  </div>
							  <div class="col-sm-3 text-center sep-top">
								<? 	$rating = $row_t['rating']; ?>
								 <input name="input-3" value="<?php echo $rating; ?>" class="rating-loading input-3">
							  </div>
							</div>
						</li>
					<?
					      $i++;
					      $count++;
					      $x++;
					  }
					?>
                    </ul>

					<?php if(mysqli_num_rows($rs)<=0){?>
						<div class="alert alert-info" id="information">No Testimonials Found</div>
					<?php } ?>
                  </div>
                </div>
              </div>
            </div><!--./testimonials-->

            <div style="display: none;" id="opportunities" class="content1">
              <h2> Opportunities </h2>

              <div class="table-responsive">
				<? $rs_msg = mysqli_query($conn,"select * from opportunities where user_id = '".$row_user['id']."' and type='domestic' order by to_date desc ");  ?>
				<? if(mysqli_num_rows($rs_msg) > 0){ ?>
				<p><strong>Domestic Opportunities:</strong> Yes</p>
                <table class="table table-bordered table-condensed table-striped">
                  <thead>
                    <tr>
                      <th width="25%">State</th>
                      <th width="25%">City</th>
                      <th width="25%">From Date</th>
                      <th width="25%">To Date</th>
                    </tr>
                  </thead>
                  <tbody>
					<? 	if(mysqli_num_rows($rs_msg) > 0){
					    while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
						    <tr title="Click for detail">
								<td><?=get_combo('states','name',$row_msg['state_id'],'','text')?></td>
						       	<td><?=$row_msg['city_name']?></td>
						       	<td><?=date_converter($row_msg['from_date'])?></td>
						       	<td><?=date_converter($row_msg['to_date'])?></td>
						    </tr>
						<?  }
					} ?>
                  </tbody>
                </table>
				<? } ?>

				<?php if(mysqli_num_rows($rs_msg)<=0){?>
						<div class="alert alert-info" id="information">No Opportunities Found</div>
					<?php } ?>
              </div>

			  <div class="table-responsive">
				<? $rs_msg = mysqli_query($conn,"select * from opportunities where user_id = '".$row_user['id']."' and type='international' order by to_date desc ");  ?>
				<? if(mysqli_num_rows($rs_msg) > 0){ ?>
				<p><strong>International Opportunities:</strong> Yes</p>
                <table class="table table-bordered table-condensed table-striped">
                  <thead>
                    <tr>
					  <th>State</th>
                      <th>State</th>
                      <th>City</th>
                      <th>From Date</th>
                      <th>To Date</th>
                    </tr>
                  </thead>
                  <tbody>
					<? 	if(mysqli_num_rows($rs_msg) > 0){
					    while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
						    <tr title="Click for detail">
								<td><?=get_combo('countries','name',$row_msg['country_id'],'','text')?></td>
						       	<td><?=get_combo('states','name',$row_msg['state_id'],'','text')?></td>
						       	<td><?=$row_msg['city_name']?></td>
						       	<td><?=date_converter($row_msg['from_date'])?></td>
						       	<td><?=date_converter($row_msg['to_date'])?></td>
						    </tr>
						<?  }
					} ?>
                  </tbody>
                </table>
				<? } ?>
              </div>

			</div><!--./opportunities-->

            <div style="display: none;" id="travel-requirements" class="content1">
              <h2 style="margin:10px 0px"> Travel Requirements </h2>
			  <div class="row">
				<div class="col-md-12">
					<div class="form-group">
                <label class="col-sm-4 control-label">Citizenship:</label>
                <div class="col-sm-8">
                  <p class="form-control-static"><? 	$countries 	= explode(',',$row['citizen']);
							$cName 		= array();
							foreach($countries as $c){
								$rowCountry = get_record_on_id('countries', $c);
								$cName[] = $rowCountry['name'];
							}
							echo implode(', ', $cName);
					?></p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">International Driving License:</label>
                <div class="col-sm-8">
                  <p class="form-control-static"><?=($row_user['driving_license'] == '1')?'Yes':'No';?></p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Valid Passport:</label>
                <div class="col-sm-8">
                  <p class="form-control-static"><?=($row_user['passport'] == '1')?'Yes':'No';?></p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Passport Expiration Date:</label>
                <div class="col-sm-8">
                  <p class="form-control-static"><?=$row_user['passport_expiry']?></p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Passport Regular Processing time in my Country:</label>
                <div class="col-sm-8">
                  <p class="form-control-static"><?=$row_user['passport_weeks']?> Weeks</p>
                </div>
              </div>
			   <div class="form-group">
                <label class="col-sm-4 control-label">Travel Visa Available For:</label>
                <div class="col-sm-8">
                  <div class="table-responsive">
                    <table class="table table-bordered table-condensed table-hover table-striped">
                      <thead>
                        <tr>
                          <th width="34%">Country</th>
                          <th width="33%">From Date</th>
                          <th width="33%">To Date</th>
                        </tr>
                      </thead>
                      <tbody>
                        <? 	$rs_msg = mysqli_query($conn,"select * from travel_visas where user_id = '".$row_user['id']."' order by to_date desc");
				        	if(mysqli_num_rows($rs_msg) == 0){

								echo "<tr><td colspan='4' align='center'>No Record Found</td></tr>";

							}else{
					       		while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
					       		<tr title="Click for detail">
					       			<td><?=get_combo('countries','name',$row_msg['country_id'],'','text')?></td>
					       			<td><?=date_converter($row_msg['from_date'])?></td>
					       			<td><?=date_converter($row_msg['to_date'])?></td>
					       		</tr>
					       	<?  }
					    } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
				</div>
			  </div>
            </div><!--./travel-requirements-->

            <div style="display: none;" id="personal-data" class="content1">
              <h2 style="margin:10px 0px"> Personal Data </h2>
              <div class="row">
				<? if(in_array('about_myself', $fields)){ ?>
					<p style="margin-left:12px;"><strong>About Myself:</strong><?=$row['education']?></p>
				<? } ?>
				<div class="row">
					<div class="col-md-6">
						<label class="col-sm-12">Education:</label>
                <div class="col-sm-12">
                  <div class="table-responsive">
                    <table class="table table-bordered table-condensed table-hover table-striped">
                      <thead>
                        <tr>
                          <th width="34%">Degree</th>
                          <th width="33%">Institute</th>
                          <th width="33%">Passing Year</th>
                        </tr>
                      </thead>
                      <tbody>
                        <? 	$rs_msg = mysqli_query($conn,"select e.* from educations e where e.user_id = '".$row_user['id']."' order by passing_year desc ");
			        	if(mysqli_num_rows($rs_msg) == 0){
							echo "<tr><td colspan='3' align='center'>No Record Found</td></tr>";
			        	}else{
				       		while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
				       		<tr title="Click for detail">
				       			<td><?=$row_msg['degree']?></td>
				       			<td><?=$row_msg['institute']?></td>
				       			<td><?=$row_msg['passing_year']?></td>
				       		</tr>
				       	<?  } } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
					</div>
					<div class="col-md-6">
						<label class="col-sm-12">Languages:</label>
                <div class="col-sm-12">
                  <div class="table-responsive">
                    <table class="table table-bordered table-condensed table-hover table-striped">
                      <thead>
                        <tr>
                          <th width="20%">Language</th>
                          <th width="20%">Speaking</th>
                          <th width="20%">Reading</th>
                          <th width="20%">Writing</th>
                          <th width="20%">Preference</th>
                        </tr>
                      </thead>
                      <tbody>
                        <? 	$rs_msg = mysqli_query($conn,"select e.* from languages e where e.user_id = '".$row_user['id']."' order by sort desc");
			        	if(mysqli_num_rows($rs_msg) == 0){
							echo "<tr><td colspan='5' align='center'>No Record Found</td></tr>";
			        	}else{
				       		while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
				       		<tr title="Click for detail">
				       			<td><?=$row_msg['language']?></td>
				       			<td><?=($row_msg['speaking'] == '1')?'Yes':'No';?></td>
				       			<td><?=($row_msg['reading'] == '1')?'Yes':'No';?></td>
				       			<td><?=($row_msg['writing'] == '1')?'Yes':'No';?></td>
								<td><?=$row_msg['sort']?></td>
				       		</tr>
				       	<?  }
				       	} ?>
                      </tbody>
                    </table>
                  </div>
                </div>
					</div>
				</div>
                <div class="row">
					<div class="col-md-6">
						<label class="col-sm-12">Favorite Cricketers:</label>
                <div class="col-sm-12">
                  <div class="table-responsive">
                    <table class="table table-bordered table-condensed table-hover table-striped">
                      <thead>
                        <tr>
                          <th >Name</th>
                          <th>Preference</th>
                        </tr>
                      </thead>
                      <tbody>
                        <? 	$rs_msg = mysqli_query($conn,"select e.* from favorite_cricketers e where e.user_id = '".$row_user['id']."' order by sort desc");
			        	if(mysqli_num_rows($rs_msg) == 0){
							echo "<tr><td colspan='3' align='center'>No Record Found</td></tr>";
			        	}else{
				       		while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
				       		<tr title="Click for detail">
				       			<td><?=$row_msg['name']?></td>
				       			<td><?=$row_msg['sort'];?></td>
				       		</tr>
				       	<?  } } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
					</div>
					<div class="col-md-6">
						 <label class="col-sm-12">Favorite Cricket Merchandise Brand(s):</label>
                <div class="col-sm-12">
                  <div class="table-responsive">
                    <table class="table table-bordered table-condensed table-hover table-striped">
                      <thead>
                        <tr>
                          <th >Name</th>
                          <th>Preference</th>
                        </tr>
                      </thead>
                      <tbody>
                        <? 	$rs_msg = mysqli_query($conn,"select e.* from favorite_brands e where e.user_id = '".$row_user['id']."' order by sort desc");
			        	if(mysqli_num_rows($rs_msg) == 0){
							echo "<tr><td colspan='3' align='center'>No Record Found</td></tr>";
			        	}else{
				       		while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
				       		<tr title="Click for detail">
				       			<td><?=$row_msg['name']?></td>
				       			<td><?=$row_msg['sort'];?></td>
				       		</tr>
				       	<?  } } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
					</div>
				</div>
              </div>


            </div><!--./personal-data-->

            <div id="contact" class="content1" style="display:none;"> <br>
              <div class="form-group">
                <div class="col-sm-12">
                  <h2 style="margin:10px 0px"> Contact </h2>
                  <h3><? include('common/login-box.php');?></h3>
                </div>
              </div>

			  <? if(isset($_SESSION['ycdc_user_name']) && !empty($_SESSION['ycdc_user_name'])){ ?>
				<?php $isContact = 1; ?>
				<?php if(in_array('only_friends_can_contact', $fields_privacy)){ ?>
					<?php $friends = mysqli_fetch_assoc(mysqli_query($conn,"select * from friendship where (from_user_id = '".$_SESSION['ycdc_dbuid']."' or to_user_id = '".$_SESSION['ycdc_dbuid']."') and (from_user_id = '".$row['id']."' or to_user_id = '".$row['id']."')  and status = '1'")); ?>
					<?php if(!$friends){ ?>
						<?php $isContact = 0; ?>
					<?php } ?>
				<?php } ?>

			  <?php if($isContact == 1){ ?>
			  <div id="form-div">
			  <div class="col-md-6 col-xs-12">
              <form method="post" action="" class="req-frm" id="req-frm">
                <input name="action" value="message" type="hidden">
                <div class="form-group">
                  <label class="col-sm-3 control-label">To:</label>
                  <div class="col-sm-9">
                    <p class="form-control-static"><?=get_combo('users','f_name',$_GET['id'],'','text')." "?><?=get_combo('users','last_name',$_GET['id'],'','text')?></p>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Subject:</label>
                  <div class="col-sm-9">
                    <input name="subject" class="form-control <?=(!isset($_SESSION['ycdc_user_name']) || empty($_SESSION['ycdc_user_name']))?'':'validate[required]'?>" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Message here:</label>
                  <div class="col-sm-9">
                    <textarea name="comments" rows="5" class="form-control <?=(!isset($_SESSION['ycdc_user_name']) || empty($_SESSION['ycdc_user_name']))?'':'validate[required]'?>"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-3 col-sm-9">
                    <input value="Send" class="btn orange hvr-float-shadow" type="submit">
                  </div>
                </div>
              </form>
			  </div>
			  </div>
			<?php }else{ ?>
				<div id="error" class="alert alert-danger">This user have privacy setting on contact ...!</div>
				<?php } ?>
			<? }else{ ?>
				<div id="error" class="alert alert-danger">Please login first for post your reply ... !</div>
			<? } ?>

            </div><!--./contact-->


			<div id="invite" class="content1" style="display:none;"> <br>
				<div class="form-group">
					<div class="col-sm-12">
						<h2 style="margin:10px 0px"> Invite </h2>
					</div>
				</div>

				<div class="row">
				<div class="col-sm-2">
				<?php if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){ ?>
					<? if($fromUserId != $toUserId){ ?>
						<? 	$rsInvite = mysqli_query($conn,"select * from friendship where from_user_id = '$fromUserId' and to_user_id = '$toUserId'");
							if(mysqli_num_rows($rsInvite) == 0){ ?>
						<form method="post">
							<input type="hidden" name="action" value="invite">
							<input  type="submit" value="Invite" id="contact-btn" title="Invite to increase your network" class="submit-login btn orange full hvr-float-shadow">
						</form>
						<? }else{
							$rowInvite = mysqli_fetch_assoc($rsInvite);
							if($rowInvite['status'] = '1'){
								echo '<span><b>Already Friend</b></span>';
							}else{
								echo '<span><b>Invitation Sent</b></span>';
							}
						} ?>
					<? } ?>

				<?php }else{ ?>
					<button  id="contact-btn1" class="submit-login popup-link btn orange full hvr-float-shadow">Invite Friend</button>
				<?php } ?>
				</div>
				</div>

				<br>
            </div><!--./contact-->

			<?php } ?>
			<div class="clearfix"></div>
				</div>


        <div class="col-md-2">
		<div  id="global_banner">
            <div class="row">
              <div class="col-sm-12">

				<?php $latest=get_record_on_id('cms',54);	?>

				 <div class="row"><div class="col-sm-12">
					<div class="right_updates"><h3 style="padding:4px;"><?php echo $latest['title'];?></h3>
						<ul class="right_updates_li"><li><?php echo $latest['content'];?></li>
						</ul>
						&nbsp;
					</div>
					</div>
				</div>

                    <?php
                    $adverSql= mysqli_query($conn,"select * from advertisement where status='1' order by sort asc") ;
                    while($record=mysqli_fetch_assoc($adverSql)){
                      if(date("Ymd",strtotime($record['expiry_date']))>=date("Ymd")){

                    ?>
                      <div class="row">
                        <div class="col-sm-12">
                          <a title="HV Immigration" href="<?php echo $record['ad_link']?>" target="_BLANK" id="4" class="add-stat">
                            <img src="<?php echo WWW;?>ads/right_side/<?php echo $record['ad_image']?>" alt="<?php echo $record['ad_name']?>" style="display:block;width:100%" id="4" class="img-app">
                          </a>
                         </div>
                       </div>
                     <?php }
                    }
                  ?>

              </div>
            </div>
          </div>
		  <!-- you cricketer 160 600 -->
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- you cricketer 160 600 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:100%;height:600px"
     data-ad-client="ca-pub-5768719230543743"
     data-ad-slot="6717429711"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<div class="clearfix"></div>
		  </div>


				<div class="clearfix"></div>
			</div>


          </div>
        </div>
      </div>
    </div><!-- /.container -->

<script src="<?php echo WWW; ?>assets/js/star-rating.js"></script>
<script src="<?=WWW?>js/jquery.lightbox-0.5.min.js" type="text/javascript"></script>
<link href="<?=WWW?>css/jquery.lightbox-0.5.css" type="text/css" rel="stylesheet">
<script type="text/javascript">
var WWW = '<?php echo WWW; ?>';
$(document).ready(function(){
	$(".menu a").click(function(event){
			event.preventDefault();
			event.stopPropagation();
			var id = $(this).attr('id').replace('-tab','');
			$(".menu").find("a").removeClass("active");
			$(".content1").hide();
			$("#"+id).show();
			$("#"+id+"-tab").addClass("active");

			$(".white-box_listting").find(".menu").find("a").removeClass("active");
			$(".white-box_listting").find("#"+id+"-tab").addClass("active");
	});

	$('.input-3').rating({displayOnly: true, step: 0.5});
	$('#gallery a.match-gallery-link').lightBox();
	$("#contact").find('#form-div').find("form").validationEngine();

	<?php if($user_data['user_type_id'] == 2){ ?>
	displayPlayerStatistics = function(){
		var data = "player_id=<?php echo $user_data['id'] ?>&league_id="+$("#league_select_stats").val()+"&year="+$("#year_select_stats").val()+"&game_type="+$("#game_types_select_stats").val()+"&overs_type="+$("#overs_type_select_stats").val()+"&ball_type="+$("#ball_type_select_stats").val()+"&tour_id="+$("#tournament_select_stats").val();;
		$.ajax({
			url: "<?php echo WWW; ?>player-statistics-ajax.php",
			type:"POST",
			data:data,
			success: function(data) {
				$("#player-statistics-data").html(data);
			}
		});
	}

	$("#player_stats_search").bind('click',function(){
		displayPlayerStatistics();
	});
	$(".player-stats-link").bind('click',function(){
		displayPlayerStatistics();
	});

	displayPlayerStatistics();
	<?php } ?>

	$(".star").click(function(){
	   var id  = $(this).attr('id');
	   var uid = id.split('-');

		$(".star").html("<img src='images/loader.gif'>");
		$.ajax({type	: 'GET',
				url		: WWW + 'favorite.php',
				data	: ({user_id:uid[0],id:uid[1],action:uid[2]}),
				success	: function(msg){
				if(msg){
				 window.location.reload();
				}
				}
			});

	});

	<?php if(isset($_POST['action']) && $_POST['action'] == 'invite' ): ?>
		$("#invite-tab").addClass('active');
		$("#activity-tab").removeClass('active');
		$("#activity").hide();
		$("#invite").show();
	<?php endif; ?>

});
</script>

<? include('common/footer.php'); ?>
