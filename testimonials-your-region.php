<?php include('common/header.php'); 
$selected_country = getGeoLocationCountry();
	
?>
	
                	<?php  $rpp = PRODUCT_LIMIT_FRONT; // results per page
            			$ppage = intval($_GET["page"]);
      					if($ppage<=0) $ppage = 1;
      					$query = "select * from testimonials as t  ";
						$row_country = mysqli_fetch_assoc(mysqli_query($conn,"select * from countries where name = '$selected_country'"));
							$query .= " join users u on u.id = t.user_id and (u.country_id = '".$row_country['id']."'  ) ";
				      	//=======================================
						$query  .= " where t.status=1";
					    if(isset($_GET['keywords']) && $_GET['keywords'] != 'Search Here' && $_GET['keywords'] != 'all'){
					        $query  .= "and content like '%".str_replace('-','%',$_GET['keywords'])."%'";
							$keywords = trim($_GET['keywords']);
					    }else{
							$keywords = '';
						}
					    if(isset($_GET['type']) && $_GET['type'] == 'my'){
					    	$query  .= "and user_id = '".$_SESSION['ycdc_dbuid']."'";	
					    }
					    $query .= " order by t.id desc ";
				      //echo $query;
				      //=======================================
				      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
				        echo '<div id="information" class="alert alert-danger">No record ...!</div>';
				      }
				      $rs   = mysqli_query($conn,$query);
				      $tcount = mysqli_num_rows($rs);
				      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
				      $count = 0;
				      $i = ($ppage-1)* $rpp;
          			  $x = 0;?>

    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1> YouCricketer Testimonials In Your Region - <?php echo $selected_country;?> </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <div class="white-box">
            <div class="row">
              <div class="col-sm-5">
                <? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_FRONT){ ?>
		      	<?php
		        	//$reload = 'testimonials.html?';
					if(!empty($keywords)){
							$reload = "testimonials-{$keywords}.html?";
						}else{
							$reload = "your-region-testimonials.html?";
					}
		        	echo paginate_one($reload, $ppage, $tpages);
		      	?>
				<input type="hidden" name="pagination-page" value="testimonials.html">
		        <? } ?>   
              </div>
              <div class="col-sm-4" id="search-div-1">
                <form id="list-search" method="post" action="">
                  <div class="input-group">
                    <input type="text" class="form-control validate[required] input-login" name="txtsearch" placeholder="Search Here">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit">Go!</button>
                    </span>
                  </div>
                </form>
              </div>
              <div class="col-sm-3">
                <a href="<?=(!isset($_SESSION['ycdc_user_name']) || empty($_SESSION['ycdc_user_name']))?'#login':WWW.'add-testimonial.html';?>" class="btn orange full hvr-float-shadow">Create Testimonial</a>
              </div>
            </div>
            <div id="pagination-top"> </div>
            <div class="row">
              <div class="col-sm-12">
                <div class="list testimonials">
                  <ul>
				  <?php 
				    while(($count<$rpp) && ($i<$tcount)){
                		mysqli_data_seek($rs,$i);
                		$row_t 	= mysqli_fetch_array($rs);
                		$row_i  = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_t['user_id']."' and is_default = '1'"));
                	?>
                    <li>
                      <div class="row">
                      	<div class="col-sm-9 sep-right">
						<h3>
						<?php if(!empty($row_i['file_name']) && file_exists('users/'.$row_t['user_id'].'/photos/'.$row_i['file_name'])){?>
							<img style="margin:4px" src="<?=WWW?><?='users/'.$row_t['user_id'].'/photos/'.$row_i['file_name']?>" alt="<?=get_combo('users','concat(f_name," ",last_name)',$row_t['user_id'],'','text')?>" width="40">
							<?php }else{ ?>
								<img  src="<?=WWW?>images/no-photo.png" width="70" height="65">
							<?php } ?></td>
							<?=get_combo('users','concat(f_name," ",last_name)',$row_t['user_id'],'','text')?> for 
							
							<?php $row_i  = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row_t['entity_id']."' and is_default = '1'")); ?>
							 <?php if($row_i['file_name'] && file_exists('users/'.$row_t['entity_id'].'/photos/'.$row_i['file_name'])){?>
							<img style="margin:4px" src="<?=WWW?><?='users/'.$row_t['entity_id'].'/photos/'.$row_i['file_name']?>" alt="<?=get_combo('users','concat(f_name," ",last_name)',$row_t['entity_id'],'','text')?>" width="40">
							<?php } else { ?>
								<img  src="<?=WWW?>images/no-photo.png" width="70" height="65">
							<?php } ?>
							
							<?=get_combo('users','concat(f_name," ",last_name)',$row_t['entity_id'],'','text')?> on <?=date_converter($row_t['testimonial_date'],'M d,Y H:i')?>
						</h3>	
                          
                          <p><?=$row_t['content']?></p>
                        </div>
                        <div class="col-sm-3 text-center sep-top">
                           <? 	$rating = $row_t['rating'];
								for($x = 0; $x < 5; $x++){
									if($x < $rating){
									?><img src="<?=WWW?>images/star-fill-gray.jpg" title="<?=$rating?>/5"><?
									}else{
									?><img src="<?=WWW?>images/star-gray.jpg" title="<?=$rating?>/5"><?
									}
								}
							?>
                        </div>
                      </div>
                    </li>
                    <?
					      $i++;
					      $count++;
					      $x++;
					  } 
					  ?>
                    
                  </ul>
                </div>
              </div>
            </div>
            <div id="pagination-bottom"> </div>
            <div class="row">
              <div class="col-sm-5">
                <? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_FRONT){ ?>
		      	<?php
		        	//$reload = 'testimonials.html?';
					if(!empty($keywords)){
							$reload = "testimonials-{$keywords}.html?";
						}else{
							$reload = "your-region-testimonials.html?";
					}
		        	echo paginate_one($reload, $ppage, $tpages);
		      	?>
		      	<input type="hidden" name="pagination-page" value="testimonials.html">
		        <? } ?>    
              </div>
              <div class="col-sm-4" id="search-div-2">
                <form id="list-search" method="post" action="">
                  <div class="input-group">
                    <input type="text" class="form-control validate[required] input-login" name="txtsearch" placeholder="Search Here">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit">Go!</button>
                    </span>
                  </div>
                </form>
              </div>
              <div class="col-sm-3">
                <a href="<?=(!isset($_SESSION['ycdc_user_name']) || empty($_SESSION['ycdc_user_name']))?'#login':WWW.'add-testimonial.html';?>" class="btn orange full hvr-float-shadow">Create Testimonial</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
				<?php include('common/right-panel.php');?>
            </div>
          </div>
          
        </div>
      </div>
    </div><!-- /.container -->
	
	<script type="text/javascript">
		$('form#list-search').submit(function(e){
			var parentId = $(this).parent().attr('id');
			var string = $('#'+parentId +' form input[name=txtsearch]').val();
			if(string != '' && string != 'Search Here'){
				string = string.replace(/[^a-zA-Z0-9]+/g,'-').toLowerCase();
				if(string.length > 0){
					$('form#list-search').attr('action','<?=WWW;?>testimonials-' + string + '.html');
				}
			}
		});
				</script> 
<?php include('common/footer.php'); ?>