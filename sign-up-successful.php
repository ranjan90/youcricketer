<?php include('common/header.php'); ?>

<div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1> Sign Up Completed  </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <div class="white-box">
            
                <?php if(isset($_GET['status'])){ ?>
				<?php if($_GET['status'] == 'success_individual'){ ?>
				<div class="alert alert-success" >You Registration is successful, please check your email from YouCricketer and verify the links in it to complete your registration. If you do not see then check your Spam/Junk folder and click it to be safe!.</div>
				<?php } ?>
				<?php if($_GET['status'] == 'success_company'){ ?>
				<div class="alert alert-success" >You Registration is successful, please check your email from YouCricketer and verify the links in it to complete your registration.</div>
				<?php } ?>
				<?php } ?>
             
		  </div>
        </div>
        
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
				<?php include('common/right-panel.php');?>
            </div>
          </div>
          
        </div>
      </div>
</div><!-- /.container -->	
	
<?php include('common/footer.php'); ?>