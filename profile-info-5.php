<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	<div class="middle">
		<h1>Profile Information</h1>

		<? 	$user_id = $row_user['id'];
			//clubs
			if(isset($_GET) && $_GET['action'] == 'delete' && $_GET['type'] == 'clubs'){
				mysqli_query($conn,"delete from users_to_clubs where id = '".$_GET['id']."'");
				echo '<div id="success"><b>Success : </b>Information updated ... !</div><br><br>';
				
			}
			if(isset($_POST) && $_POST['action'] == 'add-club'){
				$club 		= $_POST['club_name'];
				$sort 		= $_POST['sort'];
				$affiliation_date = trim($_POST['club_affiliation_time']);
				if(!empty($club)){
					$sql = "select * from users_to_clubs where user_id = '$user_id' and sort = '$sort'";
					if(mysqli_num_rows(mysqli_query($conn,$sql)) == 0){
						$date 		= date('Y-m-d H:i:s');
						$rs_clubs 	= mysqli_query($conn,"select * from clubs where title = '$club'");
						if(mysqli_num_rows($rs_clubs) == 0){
							mysqli_query($conn,"insert into clubs (title, status, create_date) values ('$club','1','$date');");
							$club_id = mysqli_insert_id($conn);
						}else{
							$row 	= mysqli_fetch_assoc($rs_clubs);
							$club_id= $row['id'];
						}
						$rs_chk = mysqli_query($conn,"select * from users_to_clubs where user_id = '$user_id' and club_id = '$club_id'");
						if(mysqli_num_rows($rs_chk) == 0){ 
							
							mysqli_query($conn,"insert into users_to_clubs (sort, user_id, club_id,affiliation_date,date_added) values ('$sort','$user_id','$club_id','$affiliation_date',now()); ");					
							echo '<div id="success"><b>Success : </b>Information updated ... !</div><br><br>';
							
						}
					}else{
						echo '<div id="error"><b> Error : </b>This Preference already exists for clubs. Please chose a unique sort ... !</div><br><br>';
					}
				}
			}
			if(isset($_POST) && $_POST['action'] == 'club-update'){
				$club 		= $_POST['club_name'];
				$sort 		= $_POST['sort'];
				$affiliation_date = trim($_POST['club_affiliation_time']);
				if(!empty($club)){
					$usertoclub = $_POST['recordId'];
					$date 		= date('Y-m-d H:i:s');
					$sql = "select * from users_to_clubs where user_id = '$user_id' and sort = '$sort' and id!='$usertoclub' ";
					if(mysqli_num_rows(mysqli_query($conn,$sql)) == 0) {
						$rs_clubs 	= mysqli_query($conn,"select * from clubs where title = '$club'");
						if(mysqli_num_rows($rs_clubs) == 0){
							mysqli_query($conn,"insert into clubs (title, status, create_date) values ('$club','1','$date');");
							$club_id = mysqli_insert_id($conn);
						}else{
							$row 	= mysqli_fetch_assoc($rs_clubs);
							$club_id= $row['id'];
						}
						mysqli_query($conn,"update users_to_clubs set sort = '$sort', club_id = '$club_id',affiliation_date='$affiliation_date' where id = '$usertoclub'");
						echo '<div id="success"><b>Success : </b>Information updated ... !</div><br><br>';
						
					}else{
						echo '<div id="error"><b> Error : </b>This Preference already exists for clubs. Please chose a unique sort ... !</div><br><br>';
					}
				}
			}
			//leagues
			if(isset($_GET) && $_GET['action'] == 'delete' && $_GET['type'] == 'leagues'){
				mysqli_query($conn,"delete from users_to_leagues where id = '".$_GET['id']."'");
				echo '<div id="success"><b>Success : </b>Information updated ... !</div><br><br>';
				
			}
			if(isset($_POST) && $_POST['action'] == 'add-league'){
				$club 		= $_POST['league_name'];
				$sort 		= $_POST['sort'];
				$affiliation_date = trim($_POST['league_affiliation_time']);
				if(!empty($club)){
					$sql = "select * from users_to_leagues where user_id = '$user_id' and sort = '$sort'";
					if(mysqli_num_rows(mysqli_query($conn,$sql)) == 0){
						$date 		= date('Y-m-d H:i:s');
						$rs_clubs 	= mysqli_query($conn,"select * from leagues where title = '$club'");
						if(mysqli_num_rows($rs_clubs) == 0){
							mysqli_query($conn,"insert into leagues (title, status, create_date) values ('$club','1','$date');");
							$club_id = mysqli_insert_id($conn);
						}else{
							$row 	= mysqli_fetch_assoc($rs_clubs);
							$club_id= $row['id'];
						}
						$rs_chk = mysqli_query($conn,"select * from users_to_leagues where user_id = '$user_id' and league_id = '$club_id'");
						if(mysqli_num_rows($rs_chk) == 0){ 
							mysqli_query($conn,"insert into users_to_leagues (sort, user_id, league_id,affiliation_date,date_added) values ('$sort','$user_id','$club_id','$affiliation_date',now()); ");					
							echo '<div id="success"><b>Success : </b>Information updated ... !</div><br><br>';
							
						}
					}else{
						echo '<div id="error"><b> Error : </b>This Preference already exists for Leagues. Please chose a unique sort ... !</div><br><br>';
					}
				}
			}
			if(isset($_POST) && $_POST['action'] == 'league-update'){
				$club 		= $_POST['league_name'];
				$sort 		= $_POST['sort'];
				$affiliation_date = trim($_POST['league_affiliation_time']);
				if(!empty($club)){
					$usertoclub = $_POST['recordId'];
					$date 		= date('Y-m-d H:i:s');
					$sql = "select * from users_to_leagues where user_id = '$user_id' and sort = '$sort' and id!='$usertoclub' ";
					if(mysqli_num_rows(mysqli_query($conn,$sql)) == 0) {
						$rs_clubs 	= mysqli_query($conn,"select * from leagues where title = '$club'");
						if(mysqli_num_rows($rs_clubs) == 0){
							mysqli_query($conn,"insert into leagues (title, status, create_date) values ('$club','1','$date');");
							$club_id = mysqli_insert_id($conn);
						}else{
							$row 	= mysqli_fetch_assoc($rs_clubs);
							$club_id= $row['id'];
						}
						mysqli_query($conn,"update users_to_leagues set sort = '$sort', league_id = '$club_id',affiliation_date='$affiliation_date' where id = '$usertoclub'");
						echo '<div id="success"><b>Success : </b>Information updated ... !</div><br><br>';
					}else{
						echo '<div id="error"><b> Error : </b>This Preference already exists for Leagues. Please chose a unique sort ... !</div><br><br>';
					}	
				}
			}
			//agent
			if(isset($_GET) && $_GET['action'] == 'delete' && $_GET['type'] == 'agents'){
				$agent_id = $_GET['id'];
				
				mysqli_query($conn,"DELETE FROM agents WHERE id = '$agent_id'");
				echo '<div id="success"><b>Success : </b>Information updated ... !</div><br><br>';
				
			}
			if(isset($_POST) && $_POST['action'] == 'add-agent'){
				$club 		= $_POST['agent_name'];
				$sort 		= $_POST['sort'];
				$affiliation_date = trim($_POST['company_affiliation_time']);
			/*	if(!empty($club)){
					$agents = explode(',',$row_user['agent']);
					$agents[] = $club.'___'.$sort;
				}
				$agents = implode(',', $agents);
			*/
			//	mysqli_query($conn,"update users set agent = '$agents' where id = '".$row_user['id']."'");
				$check_sort = mysqli_query($conn,"SELECT * FROM agents WHERE sort = '$sort' AND user_id='".$row_user['id']."'");
				$sort_rows = mysqli_num_rows($check_sort);
				if($sort_rows > 0){
					echo '<div id="error"><b> Error : </b>This Sort is already exists Please chose a unique sort ... !</div><br><br>';
				}
				else  {
				mysqli_query($conn,"insert into agents (name, sort, user_id,affiliation_date,date_added) values ('$club',$sort,'".$row_user['id']."','$affiliation_date',now());");
				echo '<div id="success"><b>Success : </b>Information updated ... !</div><br><br>';
				
			}
			}
			if(isset($_POST) && $_POST['action'] == 'agent-update'){
				$agent 		= $_POST['agent_name'];
				$sort 		= $_POST['sort'];
				$agent_id   = $_POST['agent_id']; 
				$affiliation_date = trim($_POST['company_affiliation_time']);
				$sql = "select * from agents where user_id = '$user_id' and sort = '$sort' and id!='$agent_id' ";
				if(mysqli_num_rows(mysqli_query($conn,$sql)) == 0) {
					$query = "update agents set `name` = '$agent', `sort`='$sort',affiliation_date='$affiliation_date' where id = '$agent_id' AND user_id = '".$row_user['id']."'";
					mysqli_query($conn,$query);
					echo '<div id="success"><b>Success : </b>Information updated ... !</div><br><br>';
				}else{
					echo '<div id="error"><b> Error : </b>This Preference already exists for Company. Please chose a unique sort ... !</div><br><br>';
				}
			}
			
			?>

		<div class="white-box content" id="dashboard">
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
			<? include('common/profile-navigation.php');?>
			<form method="post" action="" enctype="multipart/form-data">
				<input type="hidden" name="action" value="submit">
				<fieldset>
					<legend style="font-size:20px;">Current Affiliations</legend>
					<div class="form-box">
						<legend>Your Club(s) </legend>
						</div>
					<div class="form-box"  style="margin-left: 1px; width:90%">
							<a style="color:#fff;" class="right no-bg popup-link"  href="#add-club"><img src="<?=WWW?>images/icons/add.png"> Add</a>
			
			        <table width="90%" border="1" class="data-tbl" style="border-collapse:collapse;margin-left: -20px" id="inbox" >
			        <? 	$rs_msg = mysqli_query($conn,"select utc.*, c.title from users_to_clubs utc join clubs c on c.id = utc.club_id where utc.user_id = '".$row_user['id']."' order by utc.sort asc "); 
			        	if(mysqli_num_rows($rs_msg) == 0){
			        		?>
			        		<tr>
			        			<td colspan="3">	<td>
			        		</tr>
			        		<?
			        	}else{ ?>
							<tr>
							<td > </td> 
							<td ><b> Name </b> </td>
							<td ><b>Preference  </b></td> 
							<td ><b>Affiliation Date  </b></td> 
							<td ><b>Youcricketer Date & Time  </b></td> </tr>
							<?php
				       		while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
				       		<tr title="Click for detail">
				       			<td> 
				       				<a class="popup-link no-bg no-padding" href="#edit-club" id="<?=$row_msg['id']?>"><img src="<?=WWW?>images/icons/edit.png"></a>
				       				<a class="no-bg no-padding" href="<?=WWW?>profile-information-step-5.html?action=delete&type=clubs&id=<?=$row_msg['id']?>"><img src="<?=WWW?>images/icons/delete.png"></a>
				       			</td>
				       			<td><?=$row_msg['title']?></td>
				       			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$row_msg['sort']?></td>
								<td><?=$row_msg['affiliation_date']?></td>
								<td><?=$row_msg['date_added']?></td>
				       		</tr>
				       	<?  }
				       	} ?>
			        </table>
			    	</div>
			        <div class="clear space10"></div>
			        <div class="form-box">
			        	<legend>Your League(s) </legend>
			        </div>

					<div class="form-box"  style="margin-left: 1px; width:90%">
					<a style="color:#fff;" class="right popup-link no-bg" href="#add-league"><img src="<?=WWW?>images/icons/add.png"> Add</a>
			        
			        <table width="90%" class="data-tbl" border="1" style="border-collapse:collapse;margin-left: -20px" id="inbox" >
			         <? 	$rs_msg = mysqli_query($conn,"select utc.*, c.title from users_to_leagues utc join leagues c on c.id = utc.league_id where utc.user_id = '".$row_user['id']."' order by utc.sort asc "); 
			        	if(mysqli_num_rows($rs_msg) == 0){
			        		?>
			        		<tr>
			        			<td colspan="3">	<td>
			        		</tr>
			        		<?
			        	}else{ ?>
			        	<tr>
							<td > </td> 
							<td ><b> Name </b> </td>
							<td ><b>Preference  </b></td> 
							<td ><b>Affiliation Date  </b></td> 
							<td ><b>Youcricketer Date & Time  </b></td> </tr>
							<?php
				       		while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
				       		<tr title="Click for detail">
				       			<td>
				       				<a class="no-bg no-padding popup-link" href="#edit-league" id="<?=$row_msg['id']?>"><img src="<?=WWW?>images/icons/edit.png"></a>
				       			<a class="no-bg no-padding" href="<?=WWW?>profile-information-step-5.html?action=delete&type=leagues&id=<?=$row_msg['id']?>"><img src="<?=WWW?>images/icons/delete.png"></a>
				       			</td>
				       			<td><?=$row_msg['title']?></td>
				       			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$row_msg['sort']?></td>
								<td><?=$row_msg['affiliation_date']?></td>
								<td><?=$row_msg['date_added']?></td>
				       		</tr>
				       	<?  }
				       	} ?>
			        </table>
			        </div>
					<div class="clear space10"></div>
			       
					<div class="clear"></div>


 <div class="form-box">
			        	<legend>Your Company(s)</legend>
			        </div>

					<div class="form-box"  style="margin-left: 1px; width:90%">
					<a style="color:#fff;" class="right popup-link no-bg"style="margin-right:-25px;" href="#add-agent"><img src="<?=WWW?>images/icons/add.png"> Add</a>
			        
			        <table width="90%" border="1" class="data-tbl" style="border-collapse:collapse;margin-left: -20px" id="inbox" >
			         <? 	$rs_msg = mysqli_query($conn,"SELECT * FROM agents WHERE user_id = '$user_id' ORDER by sort ASC "); 
			        	if(mysqli_num_rows($rs_msg) == 0){
			        		?>
			        		<tr>
			        			<td colspan="3">	<td>
			        		</tr>
			        		<?
			        	}else{ ?>
							<tr>
							<td > </td> 
							<td ><b> Name </b> </td>
							<td ><b>Preference  </b></td> 
							<td ><b>Affiliation Date  </b></td> 
							<td ><b>Youcricketer Date & Time  </b></td> </tr>
							<?php
				       		while($row_agent  = mysqli_fetch_assoc($rs_msg)){ ?>
				       		<tr title="Click for detail">
				       			<td>
				       			<a class="no-bg no-padding popup-link no-bg" href="#edit-agent" id="<?php echo $row_agent['id']."___".$row_agent['name']."___".$row_agent['sort'];?>"><img src="<?=WWW?>images/icons/edit.png" ></a>
				       			<a class="no-bg" href="<?=WWW?>profile-information-step-5.html?action=delete&type=agents&id=<?=$row_agent['id'] ?>"><img src="<?=WWW?>images/icons/delete.png" style="margin-left: -20px;"></a>
				       			</td>
				       			<td><?=$row_agent['name']?></td>
				       			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$row_agent['sort']?></td>
								<td><?=$row_agent['affiliation_date']?></td>
								<td><?=$row_agent['date_added']?></td>
				       		</tr>
				       	<?  }
				       	} ?>
			        </table>
			        </div>
					<div class="clear space10"></div>

					</fieldset>
					<table width="100%" class="profile-action">
						<tr>
							<td width="33%">
								<a id="cancel" class="submit-login" style="position:relative; left:-30px; top:10px;" href="<?=WWW?>dashboard.html">Cancel</a>
							</td>
							<td width="33%">
								
							</td>
							<td width="33%">
								<a class="submit-login" style="position:relative;left:0px;  top:10px;" href="<?=WWW?>profile-information-step-6.html">Skip</a>
							</td>
						</tr>
					</table>
				
			</form>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<style>
#city select{float:left; margin-left:20px;}
.data-tbl td{width:20%;}  .data-tbl td:first-child{width:10%;} .data-tbl td:nth-child(3){width:12%;} 
.data-tbl td:nth-child(2){width:25%;} .data-tbl td:nth-child(4){width:17%;}
/*.form-box{color: #2a8bbe;font-size: 18px;font-weight: bold;}*/
</style>
<script src="<?php echo WWW?>js/jquery-1.8.0.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

	$('#dashboard .black-box h2').click(function(){
		$('#dashboard .black-box ul').css('display','none');
		var id = this.id;
		$('#'+id+'-box ul').css('display','block');
	});
	
	$('a.popup-link').click(function(){
		var link = $(this).attr('href');
		if(link == '#edit-club'){
			var id = this.id;
			$.ajax({type	: 'POST', 
				url		: WWW + 'includes/get-record.php', 
				data	: ({table:'clubs',field:'title',id:id}),
				success	: function(msg){
					var msg1 = msg.split('___');
					$(link+' input[name=club_name]').val(msg1[1]);
					$(link+' input[name=sort]').val(msg1[0]);
					$(link+' input[name=club_affiliation_time]').val(msg1[2]);
					$(link+' #recordId').val(id);
				}
			});
		}
		if(link == '#edit-league'){
			var id = this.id;
			$.ajax({type	: 'POST', 
				url		: WWW + 'includes/get-record.php', 
				data	: ({table:'leagues',field:'title',id:id}),
				success	: function(msg){
					var msg1 = msg.split('___');
					$(link+' input[name=league_name]').val(msg1[1]);
					$(link+' input[name=sort]').val(msg1[0]);
					$(link+' input[name=league_affiliation_time]').val(msg1[2]);
					$(link+' #recordId').val(id);
				}
			});
		}
		if(link == '#edit-agent'){
			var id = this.id;
			
			var idd= id.split('___');
			
			$.ajax({type	: 'POST', 
				url		: WWW + 'includes/get-record.php', 
				data	: ({table:'agents',field:'title',id:id}),
				success	: function(msg){
					var msg1 = msg.split('___');
					$(link+' input[name=agent_name]').val(msg1[1]);
					$(link+' input[name=sort]').val(msg1[0]);
					$(link+' input[name=company_affiliation_time]').val(msg1[2]);
					$(link+' input[name=agent_id]').val(idd[0]);
				}
			});
		}
		$(link).removeClass('hide');
	});
	$('.close-popup').click(function(){
		$('.popup').addClass('hide');
	});
});
</script>

<div id="add-club" class="popup hide" style="margin-top: -500px;">
	<h1>
		Add your Club
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h1>
	<form method="post" action="">
		<input type="hidden" name="action" value="add-club">
		<fieldset>
			<div class="form-box">
				<input type="text" name="club_name" id="club_name_add" maxlength="54" class="validate[required] input-login" placeholder="Type Club Name">
				<br>
				<input type="text" name="sort" class="validate[required,[custom[integer]] input-login" placeholder="Type  Preferred Order 1 is Highest Ranked">
				<br>
				<input type="text" name="club_affiliation_time" id="club_affiliation_time_add" class="validate[required] input-login datetime"  autocomplete="off" placeholder="Affiliation Date">
				<br><br> 
				<input type="submit" class="submit-login margin-top-5" value="Save">
			</div>
		</fieldset>
	</form>
</div>
<div id="edit-club" class="popup hide"style="margin-top: -500px;">
	<h1>
		Update your Club
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h1>
	<form method="post" action="">
		<input type="hidden" name="action" value="club-update">
		<input type="hidden" name="recordId" id="recordId" value="">
		<fieldset>
			<div class="form-box">
				<input type="text" name="club_name" id="club_name_edit" maxlength="54" class="validate[required] input-login" placeholder="Type Club Name">
				<br>
				<input type="text" name="sort" class="validate[required,[custom[integer]] input-login" placeholder="Type  Preferred Order 1 is Highest Ranked">
				<input type="text" name="club_affiliation_time" id="club_affiliation_time" class="validate[required] input-login datetime" autocomplete="off" placeholder="Affiliation Date">
				<br><br> 
				<input type="submit" class="submit-login margin-top-5" value="Save">
			</div>
		</fieldset>
	</form>
</div>
<div id="add-league" class="popup hide" style="margin-top: -500px;">
	<h1>
		Add your League
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h1>
	<form method="post" action="">
		<input type="hidden" name="action" value="add-league">
		<fieldset>
			<div class="form-box">
				<input type="text" name="league_name" id="league_name_add" maxlength="54" class="validate[required] input-login" placeholder="Type league Name">
				<br>
				<input type="text" name="sort" class="validate[required,[custom[integer]] input-login" placeholder="Type  Preferred Order 1 is Highest Ranked">
				<input type="text" name="league_affiliation_time" id="league_affiliation_time" class="validate[required] input-login datetime" autocomplete="off" placeholder="Affiliation Date">
				<br><br> 
				<input type="submit" class="submit-login margin-top-5" value="Save">
			</div>
		</fieldset>
	</form>
</div>
<div id="edit-league" class="popup hide"style="margin-top: -500px;">
	<h1>
		Update your League
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h1>
	<form method="post" action="">
		<input type="hidden" name="action" value="league-update">
		<input type="hidden" name="recordId" id="recordId" value="">
		<fieldset>
			<div class="form-box">
				<input type="text" name="league_name" id="league_name_edit" maxlength="54" class="validate[required] input-login" placeholder="Type league Name">
				<br>
				<input type="text" name="sort" class="validate[required,[custom[integer]] input-login" placeholder="Type  Preferred Order 1 is Highest Ranked">
				<input type="text" name="league_affiliation_time" id="league_affiliation_time_edit" class="validate[required] input-login " autocomplete="off" placeholder="Affiliation Date">
				<br><br> 
				<input type="submit" class="submit-login margin-top-5" value="Save">
			</div>
		</fieldset>
	</form>
</div>
<div id="add-agent" class="popup hide" style="margin-top: -500px;">
	<h1>
		Add your Company
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h1>
	<form method="post" action="">
		<input type="hidden" name="action" value="add-agent">
		<fieldset>
			<div class="form-box">
				<input type="text" name="agent_name" id="company_name_add" maxlength="54" class="validate[required] input-login" placeholder="Type Company Name">
				<br>
				<input type="text" name="sort" class="validate[required,[custom[integer]] input-login" placeholder="Type  Preferred Order 1 is Highest Ranked">
				<input type="text" name="company_affiliation_time" id="company_affiliation_time" class="validate[required] input-login datetime" autocomplete="off" placeholder="Affiliation Date">
				<br><br> 
				<input type="submit" class="submit-login margin-top-5" value="Save">
			</div>
		</fieldset>
	</form>
</div>
<div id="edit-agent" class="popup hide" style="margin-top: -500px;">
	<h1>
		Update your Company
		<img src="<?=WWW?>images/icons/delete.png" class="close-popup">
	</h1>
	<form method="post" action="">
		<input type="hidden" name="action" value="agent-update">
		<input type="hidden" name="recordId" id="recordId" value="">
		<fieldset>
			<div class="form-box">
				<input type="text" name="agent_name" id="company_name_edit" maxlength="54" class="validate[required] input-login" placeholder="Type Comapany Name">
				<br>
				<input type="text" name="sort" class="validate[required,[custom[integer]] input-login" placeholder="Type  Preferred Order  1 is Highest Ranked">
				<br>
				<input type="hidden" name="agent_id" class="validate[required] input-login" placeholder="Type Sort Order 1 is Highest Ranked">
				<input type="text" name="company_affiliation_time" id="company_affiliation_time_edit" class="validate[required] input-login" autocomplete="off" placeholder="Affiliation Date">
				<br><br>
				<input type="submit" class="submit-login margin-top-5" value="Save">
			</div>
		</fieldset>
	</form>
</div>

<script src="<?php echo WWW; ?>js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo WWW; ?>css/jquery.datetimepicker.css"/>


<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script type="text/javascript" src="<?=WWW?>/js/jquery.validationengine.js"></script>
<script type="text/javascript" src="<?=WWW?>js/validation-engine.js"></script>
  
<script>
$('.datetime').datepicker({ maxDate: "+0D",dateFormat:"yy/mm/dd" });
$("#league_affiliation_time_edit,#company_affiliation_time_edit").datepicker({ maxDate: "+0D",dateFormat:"yy/mm/dd" });

$(document).ready(function(){
	$("#club_name_add,#club_name_edit").autocomplete({
      source: "affiliation_data.php?type=clubs",
    });
	$("#league_name_add,#league_name_edit").autocomplete({
      source: "affiliation_data.php?type=leagues",
    });
	$("#company_name_add,#company_name_edit").autocomplete({
      source: "affiliation_data.php?type=companies",
    });
	
	$('form').validationEngine();
});
    
</script>



<?php include('common/footer.php'); ?>