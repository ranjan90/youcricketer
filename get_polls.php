<?php include('includes/configuration.php'); 
extract($_POST);

$tournament_info = array();
$error = '';
$poll_str = '';
//$tournament_info = get_record_on_id('tournaments', $t_id);	

if(isset($_POST['action']) && $_POST['action'] == 'get' ){
	$sql = "SELECT * FROM tournament_polls WHERE tournament_id=$t_id and is_active=1 ORDER BY id DESC ";
	$rs_polls = mysqli_query($conn,$sql);
	while($row = mysqli_fetch_assoc($rs_polls)){
		$poll_str.='<div class="poll-div" id="poll_div_'.$row['id'].'"><div class="poll_question">'.$row['poll_question'].'</div><div class="clear"></div>';	
		for($i=1;$i<=6;$i++){
			if(!empty($row['poll_option_'.$i])){
				$poll_str.='<div class="poll_option"><input type="radio" value="'.$i.'" name="poll_option_'.$row['id'].'" id="poll_option_'.$row['id'].'_'.$i.'">'.$row['poll_option_'.$i].'</div><div class="clear"></div>';
			}	
		}
		
		$poll_str.='<div class="clear" style="height:5px;"></div><input type="button" onClick="submitPoll('.$row['id'].')" value="Submit" class="poll-submit" name="poll_submit_'.$row['id'].'" id="poll_submit_'.$row['id'].'">';
		$poll_str.='<div class="clear"></div> </div>';
		$poll_str.='<div class="clear"><br/></div>';
	}

	$arr = array('str'=>$poll_str,'error'=>$error);
	echo json_encode($arr);
	exit();
}

if(isset($_POST['action']) && $_POST['action'] == 'submit' ){
	if(isset($_POST['poll_id']) && isset($_POST['option_id'])){
		$sql = "UPDATE tournament_polls SET votes_{$option_id} = votes_{$option_id}+1 WHERE id=$poll_id ";
		mysqli_query($conn,$sql);
	}
	
	$sql = "SELECT * FROM tournament_polls WHERE id = $poll_id";
	$rs_polls = mysqli_query($conn,$sql);
	$poll_data = mysqli_fetch_assoc($rs_polls);
	$total_votes = 0; 
	for($i=1;$i<=6;$i++){
		$total_votes+=$poll_data['votes_'.$i];
	}
	
	$poll_str = '<div class="poll_question">'.$poll_data['poll_question'].'</div><div class="clear"></div>';
	$colors_arr = array('#ED7765','#93C636','#34ADC2','#E6CE1A','#F5AF47','#2CD494');
	
	for($i=1;$i<=6;$i++){
		if(!empty($poll_data['poll_option_'.$i])){
			$votes = $poll_data['votes_'.$i];
			$color = $colors_arr[$i-1];
			$votes_percent = round(($votes/$total_votes)*100);
			$poll_str.='<div class="poll_option">
			<div class="option_text">'.$poll_data['poll_option_'.$i].'</div>
			<div class="option_bar"><div class="option_percent" style="border:1px outset '.$color.';background-color:'.$color.';width:'.$votes_percent.'%">&nbsp;</div> &nbsp;'.$votes_percent.'%</div>
			</div><div class="clear"></div>';
		}	
	}
	
	$arr = array('str'=>$poll_str,'error'=>$error);
	echo json_encode($arr);
	exit();
}
