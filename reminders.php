<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>

<?  $rpp = PRODUCT_LIMIT_FRONT; // results per page
	            			$ppage = intval($_GET["page"]);
	      					if($ppage<=0) $ppage = 1;
	      					$query 	= "select i.id, i.to_emails from invitations i where i.from_user_id ='".$_SESSION['ycdc_dbuid']."' and i.to_emails not in (select email from users) order by i.id";
	      					//echo $query;
					      //=======================================
					      
					      $rs   = mysqli_query($conn,$query);
					      $tcount = mysqli_num_rows($rs);
					      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
					      $count = 0;
					      $i = ($ppage-1)* $rpp;
	          			  $x = 0; ?>

<div class="page-container"> 
		<?php  include('common/user-left-panel.php');?>
      <!-- END SIDEBAR -->
      
      <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
		<div class="page-content">
		
		
		<div class="white-box">
            <div class="row">
              <div class="col-sm-8">
                <h2>Reminders to already sent invitations</h2>
              </div>
              <div class="col-sm-4 text-right">
                <a href="#" class="text-success checked" id="checkAll" style="display:inline-block; margin:15px 5px;"><i class="fa fa-check-square"></i> Check All</a>
                <a href="#" class="text-danger checked" id="uncheckAll" style="display:inline-block; margin:15px 5px;"><i class="fa fa-check-square-o"></i> Uncheck All</a>
              </div>
            </div>
			
			<?php
					if(isset($_POST) && count($_POST['invitations']) > 0){ 
						$from_name	= $_SESSION['ycdc_user_name'];
						foreach($_POST['invitations'] as $invites){
							$rowInvite 	= get_record_on_id('invitations', $invites);
							$email 		= $rowInvite['to_emails'];

							if(!empty($email)){
								$email_template = get_record_on_id('cms', 12);							
								$mail_title		= $email_template['title'].' from '.$from_name;
					            $mail_content	= $email_template['content'];
					            $mail_content 	= str_replace('{to_name}',$name, $mail_content);	
					            $mail_content 	= str_replace('{from_name}',$from_name, $mail_content);	
					            $mail_content 	= str_replace('{site_link}','<a href="'.WWW.'" title="Visit The Site">YouCricketer</a>', $mail_content);
					            $mail_content 	= str_replace('{register_link}','<a href="'.WWW.'sign-up.html?ref='.$invites.'" title="Sign Up">HERE</a>', $mail_content);
					            $mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);
								$headers 		= "Content-type:text/html;charset=UTF-8" . "\r\n";
								$headers 	   .= "From: no-reply@youcricketer.com \r\n";
					            mail($email,$mail_title,$mail_content,$headers);
							}
						}
						echo '<div id="success" class="alert alert-success">Invitation Sent Successfuly... !</div>';
					}
				?>
					
            <form method="post" id="multiple" class="form-horizontal">
              <input name="action" value="resend" type="hidden">
			  
			  <?php if(mysqli_num_rows(mysqli_query($conn,$query)) > 0){ ?>
              <div class="form-group">
                <label class="col-sm-5 control-label">Remind Now</label>
                <div class="col-sm-7">
				<?  
	          			  //=======================================
	      				  while(($count<$rpp) && ($i<$tcount)){
	                		mysqli_data_seek($rs,$i);
	                		$row_g 	= mysqli_fetch_assoc($rs);
	      					?>
								<p><input name="invitations[]" value="<?php echo $row_g['id']?>" type="checkbox"> <?php echo $row_g['to_emails']?></p>
						    <?
						      $i++;
						      $count++;
						      $x++;
						  } 
						  ?>	
					
					<? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_FRONT){ ?>
			      	<?php
			        	$reload = 'reminders.html?';
			        	echo paginate_one($reload, $ppage, $tpages);
			      	?>
			      	<input type="hidden" name="pagination-page" value="reminders.html">
			        <? } ?>    
					
                </div>
              </div>
			  
              <div class="form-group">
                <div class="col-sm-offset-5 col-sm-7">
					<input type="hidden" name="action" value="resend">
                  <input value="Resend" class="btn orange hvr-float-shadow" type="submit">
                </div>
              </div>
			  <?php } else{ ?>
				<div id="error" class="alert alert-danger">Your search returned no results</div>
			  <?php } ?>
            </form>
        </div>
		
		</div>
	</div>
</div>		

<script>
$(document).ready(function(){
	
	$('a#checkAll').click(function(){
		$('form#multiple input[type=checkbox]').attr('checked', 'checked').prop('checked', true);
	});
	$('a#uncheckAll').click(function(){
		$('form#multiple input[type=checkbox]').removeAttr('checked', 'checked').prop('checked', false);
	});
});
</script>

<?php include('common/footer.php'); ?>