<?php include('common/header.php'); ?>

<?php 
	$row = get_record_on_id('forum_topics',$_GET['id']);
	$row_img= mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'groups' and entity_id = '".$row_g['id']."'"));
	if(!$row){ ?>
		<script>
		window.location = '<?php echo WWW;?>forums.html';
		</script>
	<?php } ?>

<?
			if(isset($_POST['action']) && $_POST['action'] == 'comment' && isset($_SESSION['ycdc_user_name']) && !empty($_SESSION['ycdc_user_name'])){
					$comments 	= $_POST['comments'];
					$user_id 	= $_SESSION['ycdc_dbuid'];
					$date 		= date('Y-m-d H:i:s');
					$topic_id 	= $_GET['id'];

					$rs_chk 	= mysqli_query($conn,"select * from forum_replies where user_id = '$user_id' and topic_id = '$topic_id' and comment = '$comments'");
					if(mysqli_num_rows($rs_chk) == 0){
						$query 		= "insert into forum_replies (comment, topic_id, user_id, post_date, status) 
									values ('$comments','$topic_id','$user_id','$date','1')";	
						if(mysqli_query($conn,$query)){
							?><div id="success" class="alert alert-success">Comments saved successfully ... !</div><?
						}else{
							?><div id="success" class="alert alert-danger">Comments already submitted ... !</div><?
						}
					}
					
				}
?>	

<script src="<?php echo WWW;?>assets/js/ckeditor/ckeditor.js"></script>
<div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1>
			Forum Detail - <?php echo get_combo('forum_topics','title',$_GET['id'],'','text')?> <br>
            <span> <strong>Created By :</strong> <?php echo get_combo('users','concat(f_name, last_name)',$row['user_id'],'','text')?>
			<strong>Created On :</strong> <?php echo date_converter($row['create_date'])?>
			<strong>Category :</strong> <?php echo get_combo('forum_categories','name',$row['category_id'],'','text')?> </span>
          </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <div class="white-box">
           <div class="clear"><br/></div>
			<div class="row">
				<div class="col-sm-12">
					<?php echo $row['content']; ?>
				</div>
			</div>
			<div class="clear"><br/></div>
			
			
			
			
            <div class="row">
              <div class="col-sm-12">
                <h2>Posted Replies</h2>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
				<?php  $query = "select * from forum_replies where status=1 and topic_id = '".$row['id']."' order by id desc ";
				    //echo $query;
				    //=======================================
				    $rs   = mysqli_query($conn,$query);
				    //=======================================
      				while($rowC = mysqli_fetch_assoc($rs)){ ?>
      					
      						
							<span class="list-text" >
								
	                            <h3><?php echo get_combo('users','concat(f_name, last_name)',$rowC['user_id'],'','text')?> on <?php echo date_converter($rowC['post_date'])?></h3>
	                            <p><?php echo $rowC['comment'];?></p>	
								<?php if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid']) && $_SESSION['ycdc_dbuid'] == $rowC['user_id']){ ?>
								
									<a href="<?php echo WWW?>delete-forum-replies-<?php echo $rowC['id']?>.html" onClick="return confirm('Are you sure to delete comment');"><img src="<?php echo WWW?>images/icons/delete.png"></a>
								<?php } ?>								
	                        </span>
							<hr style="margin:6px;">
					    <?php } ?>
					
					
				</div>
            </div>
			
			<div class="row">
				<div class="col-sm-12">
					<h2>Post Your Reply</h2>
					<?php include('common/login-box.php');?>
                </div>
            </div>
			
			<?php if(isset($_SESSION['ycdc_user_name']) && !empty($_SESSION['ycdc_user_name'])){ ?>
				<form method="post" action="">
				<div class="row">
				  <div class="col-sm-12">
					<div id="editor">
						<textarea name="comments"  class="ckeditor <?php echo (!isset($_SESSION['ycdc_user_name']) || empty($_SESSION['ycdc_user_name']))?'':'validate[required]'?>"></textarea>
					</div>
				  </div>
				</div>
				<div class="row">
				  <div class="col-sm-12 text-center">
					<br/>
					<input value=" Save " class="btn orange hvr-float-shadow" type="submit">
					<a id="cancel" class="btn blue hvr-float-shadow" href="<?php echo WWW?>forums.html">Cancel</a>
				  </div>
				</div>
				<input type="hidden" name="action" value="comment">
				</form>
			<?php }else{ ?>
				<div class="row">
				  <div class="col-sm-12 text-center">
					<div class="alert alert-danger">Please login first to post your reply ... !</div>
				  </div>
				</div>
			<?php } ?>
			
            
          </div>
        </div>
        
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
              <?php include('common/right-panel.php');?>
            </div>
          </div>
          
        </div>
      </div>
    </div><!-- /.container -->

<?php include('common/footer.php'); ?>