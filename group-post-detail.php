<?php include('common/header.php'); ?>
<?	$row_post   = get_record_on_id('group_posts', $_GET['id']);
	$row_group  = get_record_on_id('groups',$row_post['group_id']);
	$rs_members = mysqli_query($conn,"select * from users_to_groups where group_id = '".$row_post['group_id']."'");
	$members 	= array();
	while($row_m = mysqli_fetch_assoc($rs_members)){
		$members[] = $row_m['invitation_to'];
	}
?>
	<div class="middle">
		<h1>
			Group Post Detail - <?=get_combo('group_posts','title',$_GET['id'],'','text')?> 
		<?	if(isset($_SESSION['ycdc_user_name'])  && !empty($_SESSION['ycdc_user_name']) && !in_array($row_user['id'], $members) ){ ?>
			<a href="<?=WWW?>join-group-detail-<?=$row_group['id']?>-<?=friendlyURL($row_group['title'])?>.html" class="popup-link right submit-login no-shaddow" >Join Group</a>
			<? } ?>
		</h1>
		<div class="white-box content detail">
			<? 
				$row = get_record_on_id('group_posts',$_GET['id']);
			?>
			<?
				if(isset($_POST['action']) && $_POST['action'] == 'comment' && isset($_SESSION['ycdc_user_name']) && !empty($_SESSION['ycdc_user_name'])){
					$comments 	= $_POST['comments'];
					$user_id 	= $_SESSION['ycdc_dbuid'];
					$date 		= date('Y-m-d H:i:s');
					$topic_id 	= $_GET['id'];

					$rs_chk 	= mysqli_query($conn,"select * from group_replies where user_id = '$user_id' and topic_id = '$topic_id' and comment = '$comments'");
					if(mysqli_num_rows($rs_chk) == 0){
						$query 		= "insert into group_replies (comment, topic_id, user_id, post_date, status) 
									values ('$comments','$topic_id','$user_id','$date','1')";	
						if(mysqli_query($conn,$query)){
							?><div id="success">Comments saved successfully ... !</div><?
						}else{
							?><div id="success">Comments already submitted ... !</div><?
						}
					}
					
				}
			?>
			<p>
				<div>
					<!-- AddThis Button BEGIN -->
					<div class="addthis_toolbox addthis_default_style ">
					<a class="addthis_button_preferred_1"></a>
					<a class="addthis_button_preferred_2"></a>
					<a class="addthis_button_preferred_3"></a>
					<a class="addthis_button_preferred_4"></a>
					<a class="addthis_button_compact"></a>
					<a class="addthis_counter addthis_bubble_style"></a>
					</div>
					<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
					<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5073efa669740139"></script>
					<!-- AddThis Button END -->
				</div>
				<div><b>Created By : </b><?=get_combo('users','f_name',$row['user_id'],'','text')?>&nbsp;<?=get_combo('users','last_name',$row['user_id'],'','text')?></div>
				<div><b>Created On : </b><?=date_converter($row['post_date'])?></div>
				<div><b>Group : </b><?=get_combo('groups','title',$row['group_id'],'','text')?></div>
				<div class="space10"></div>
				<?=$row['description']?>
			</p>
		    <div class="clear"></div>

		    <h2>Add Your Reply</h2>
			<? include('common/login-box.php');?>
			<? if(isset($_SESSION['ycdc_user_name']) && !empty($_SESSION['ycdc_user_name'])){ ?>
				<? if(in_array($_SESSION['ycdc_dbuid'], $members)){ ?>
			    <form method="post" action="">
			    	<input type="hidden" name="action" value="comment">
					<fieldset>
						<div class="form-box">
							<textarea name="comments" style="margin-left: -5px; width:400px;" class="<?=(!isset($_SESSION['ycdc_user_name']) || empty($_SESSION['ycdc_user_name']))?'':'validate[required]'?>"></textarea>
						</div>
						<div class="clear"></div>
						<div class="form-box">
							<input type="submit" value="Save"  class="submit-login">
						</div>
					</fieldset>
				</form>
				<? }else{ ?>
					<div id="success">Please join this group first ... !</div>
				<? } ?>
			<? }else{ ?>
				<div id="success">Please login first to post your reply ... !</div>
			<? } ?>
		    <div class="clear"></div>
		    <h2>Posted Replies</h2>
		    <div id="pagination-top"></div>
			<div class="list">
                <ul>
                	<?  $rpp = PRODUCT_LIMIT_FRONT; // results per page
            			$ppage = intval($_GET["page"]);
      					if($ppage<=0) $ppage = 1;
      					$query = "select * from group_replies where status=1 and topic_id = '".$row['id']."'";
				      	//=======================================
				      	if(isset($_GET['keywords']) && $_GET['keywords'] != 'Search Here'){
					        $query  .= "and title like '%".$_GET['keywords']."%'";
					    }
					    $query .= " order by id desc ";
				      //echo $query;
				      //=======================================
				      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
				        echo '<div id="information">No record found ... !</div>';
				      }
				      $rs   = mysqli_query($conn,$query);
				      $tcount = mysqli_num_rows($rs);
				      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
				      $count = 0;
				      $i = ($ppage-1)* $rpp;
          			  $x = 0;
          			  //=======================================
      				  while(($count<$rpp) && ($i<$tcount)){
                		mysqli_data_seek($rs,$i);
                		$row_g 	= mysqli_fetch_assoc($rs);
                		?>
      					<li>
      						<? if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	                            if($row_group['user_id'] == $_SESSION['ycdc_dbuid'] || $row_group['admin_id'] == $_SESSION['ycdc_dbuid']){ ?>
	                            <a href="<?=WWW?>delete-group-post-comment-<?=$row_g['id']?>.html"><img src="<?=WWW?>images/icons/delete.png"></a>
	                         <? } ?>
	                      	<? } ?>
							<span class="list-text" style="margin-left: -10px;">
	                            <h3><?=get_combo('users','concat(f_name, last_name)',$row_g['user_id'],'','text')?> on <?=date_converter($row_g['post_date'])?></h3>
	                            <p><?=$row_g['comment'];?></p>	                            
	                        </span>
						</li>
					    <?
					      $i++;
					      $count++;
					      $x++;
					  } 
					  ?>
			</ul>
			</div>
			<div id="pagination-bottom">
				<? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_FRONT){ ?>
		      	<?php
		        	$reload = 'group-post-detail-'.$_GET['id'].'-'.friendlyURL($row['title']).'.html?';
		        	echo paginate_one($reload, $ppage, $tpages);
		      	?>
		      	<input type="hidden" name="pagination-page" value="group-post-detail-<?=$_GET['id']?>-<?=friendlyURL($row['title'])?>.html">
		        <? } ?>    
		        <form id="list-search" method="post" action="">
		        	<input type="text" name="txtsearch" placeholder="Search Here" class="validate[required] input-login">
		        </form>
		        <script type="text/javascript">
		        $('form#list-search').submit(function(e){
					var string = $('input[name=txtsearch]').val();
					if(string != '' && string != 'Search Here'){
						string = string.replace(/\s+/g,'_').toLowerCase();
						$('form#list-search').attr('action','<?=WWW;?>group-post-detail-<?=$_GET['id']?>-<?=friendlyURL($row['title'])?>-' + string + '.html');
					}
				});
		        </script>
		        <div class="clear"></div>
		    </div> 
		    <div class="clear"></div>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>
<?php include('common/footer.php'); ?>