<?php 
	include('includes/configuration.php');
	$redirectPath = $_SERVER['HTTP_REFERER'];
	if(isset($_POST) && !empty($_POST)){
		$f_name 	= $_POST['f_name'];
		$email 		= $_POST['email'];
		$country_id = $_POST['country_id'];
		$name 		= $f_name;	
		$verification_code 	= getRandomWord(12);
		$date 				= date('Y-m-d H:i:s');
		$status 	= '2';
		$is_company = '0';
		$user_type_id= 18;

		$rs_user	= mysqli_query($conn,"select * from users where email = '$username'");
		
		if(mysqli_num_rows($rs_user) == 0){
			$query = "insert into users (f_name, email, verification_code, create_date, status, is_company, user_type_id, country_id) 
			values ('$f_name','$email','$verification_code','$date','$status','$is_company','$user_type_id','$country_id')";

			$row_u  = get_record_on_id('user_types',$user_type_id);
			$row_c  = get_record_on_id('countries', $country_id);

			if(mysqli_query($conn,$query)){
				$user_id = mysqli_insert_id($conn);
					
				if(!is_dir('users/'.$user_id)){
					mkdir('users/'.$user_id,0777);
				}

				//sending to user
				$email_template = get_record_on_id('cms', 1);
				$mail_title		= $email_template['title'];
	            $mail_content	= $email_template['content'];
	            $mail_content 	= str_replace('{name}',$name, $mail_content);
	            $mail_content 	= str_replace('{verification_code}',$verification_code, $mail_content);
	            $mail_content 	= str_replace('{verification_link}','<a href="'.WWW.'verify-account.html?id='.$user_id.'" title="Verify your account">HERE</a>', $mail_content);
	            $mail_content 	= str_replace('{email}',$_POST['email'], $mail_content);
	            $mail_content 	= str_replace('{contact_link}','<a href="'.WWW.'contact-us.html" title="Contact Us">HERE</a>', $mail_content);
	            $mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);

	            mail($_POST['email'],$mail_title,$mail_content,'Content-type: text/html; From: '.FROM_EMAIL);

	            //sending to admin
				$email_template = get_record_on_id('cms', 8);
				$mail_title		= $email_template['title'];
	            $mail_content	= $email_template['content'];
	            $mail_content 	= str_replace('{name}',$name, $mail_content);
	            $mail_content 	= str_replace('{user_type_id}',$row_u['name'], $mail_content);
	            $mail_content 	= str_replace('{country_id}',$row_c['name'], $mail_content);
	            $mail_content 	= str_replace('{user_id}',$user_id, $mail_content);
	            $mail_content 	= str_replace('{email}',$_POST['email'], $mail_content);
	            $mail_content   = str_replace('{create_date}',$date, $mail_content);
	            $mail_content   = str_replace('{admin_panel}','<a href="'.WWW.'admin/">ADMIN PANEL</a>', $mail_content);
	            $mail_content 	= str_replace('{contact_link}','<a href="'.WWW.'contact-us.html" title="Contact Us">HERE</a>', $mail_content);
	            $mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);

	            mail(ADMIN_EMAIL,$mail_title,$mail_content,'Content-type: text/html; From: '.FROM_EMAIL);

	            $_SESSION['ycdc_dbuid']		= $user_id;
				$_SESSION['ycdc_user_email']= $email;
				$_SESSION['ycdc_user_name'] = $f_name;
				$_SESSION['ycdc_desig_id'] 	= $user_type_id;
				?>
				<script>
				   	window.location = '<?php echo $redirectPath;?>';
				</script>
				<?
			}
		}else{
			?>
			<script>
			   	window.location = '<?php echo $redirectPath;?>';
			</script>
			<?
		}
	}
?>