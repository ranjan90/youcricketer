<?php
require_once("lib/Twocheckout.php");

Twocheckout::privateKey('B25110FE-E279-45A4-ACC4-93AF53BF714A');
Twocheckout::sellerId('901353291');

// If you want to turn off SSL verification (Please don't do this in your production environment)
Twocheckout::verifySSL(false);  // this is set to true by default

// To use your sandbox account set sandbox to true
Twocheckout::sandbox(true);

// All methods return an Array by default or you can set the format to 'json' to get a JSON response.
Twocheckout::format('json');
print_R($_POST); echo "<br>";
try {
	$charge = Twocheckout_Charge::auth(array(
			"merchantOrderId" => "123",
			"token"      => $_POST['token'],
			"currency"   => 'USD',
			"total"      => '10.00',
			"billingAddr" => array(
					"name" => 'Testing Tester',
					"addrLine1" => '123 Test St',
					"city" => 'Columbus',
					"state" => 'OH',
					"zipCode" => '43123',
					"country" => 'USA',
					"email" => 'example@2co.com',
					"phoneNumber" => '555-555-5555'
			)
	));

	if ($charge['response']['responseCode'] == 'APPROVED') {
		echo "Thanks for your Order!";
		echo "<h3>Return Parameters:</h3>";
		echo "<pre>";
		print_r($charge);
		echo "</pre>";

	}
} catch (Twocheckout_Error $e) {
	print_r($e->getMessage().' dfds');
}

?>