<?php include('common/header.php'); ?>

<div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1> How-To Articles </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <div class="white-box">
            <div id="pagination-top"> </div>
            
            <div class="row">
              <div class="col-sm-12">
              	<div class="list">
                  <ul>
                    
					<?php  $rpp = PRODUCT_LIMIT_FRONT; // results per page
            			$ppage = intval($_GET["page"]);
      					if($ppage<=0) $ppage = 1;
      					$select = "select * from how_tos as h";
      					$where  = " where h.status=1 ";
      					//=======================================
      					//$cat_id = '6';
      					//$where .= " and category_id = '$cat_id'";
      					//=======================================
					    $orderBy = ' order by h.id desc';
					    $query = $select.$join.$where.$orderBy;
				      // echo $query;
				      //=======================================
				      $rs = mysqli_query($conn,$query);
      				  while($row_g = mysqli_fetch_assoc($rs)){ ?>
					
					<li>
                      <div class="row">
                      	<div class="col-sm-3 col-md-2">
							<a href="<?php echo WWW?>how-to/<?php echo $row_g['slug']; ?>" title="Read more" />
								<img src="<?php echo WWW.'images/community-groups.png';?>" alt="<?php echo $row_g['title']?>" title="<?php echo $row_g['title']?>" width="114" />
							</a>
                        </div>
                        <div class="col-sm-6 col-md-8">
							<h3 onclick="window.location='<?php echo WWW?>how-to/<?php echo $row_g['slug']; ?>'"><?php echo $row_g['title']?></h3>
	                        <p><?php echo truncate_string(strip_tags($row_g['content']), 180)?></p>
							<a href="<?php echo WWW?>how-to/<?php echo $row_g['slug']; ?>" title="Read more" class="btn btn-default pull-right" />Read More</a>
                        </div>
                        <div class="col-sm-3 col-md-2 text-center">
							<!-- AddThis Button BEGIN -->
						        <div class="addthis_toolbox addthis_default_style ">
						        <a class="addthis_button_preferred_1"></a>
						        <a class="addthis_button_preferred_2"></a>
						        <a class="addthis_button_preferred_3"></a>
						        <a class="addthis_button_preferred_4"></a>
						        <a class="addthis_button_compact"></a>
						        <a class="addthis_counter addthis_bubble_style"></a>
						        </div>
						        <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
						        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5073efa669740139"></script>
						    <!-- AddThis Button END -->
                          <p><?php echo date('d M, Y',strtotime($row_g['date_added'])); ?></p>
                        </div>
                      </div>
                    </li>
					
					<?php
					    $i++;
					    $count++;
					    $x++;
					} ?>
                    </ul>
                </div>
              </div>
            </div>
            
		  </div>
        </div>
        
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
				<?php include('common/right-panel.php');?>
            </div>
          </div>
          
        </div>
      </div>
</div><!-- /.container -->
<?php include('common/footer.php'); ?>