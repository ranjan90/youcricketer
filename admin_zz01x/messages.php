<? include('common/header.php');?>	
<!--**********************************-->
<div class="box1">
<h1>Messages (<?=get_combo('users','f_name',$_GET['id'],'','text')?>)</h1>
<table width="99%"  class="grid" border="0">
<tr>
<td width="751">
<div id="options">
     <div id="box" style="width:500px;">
        <form method="get" action="<?=ADMIN?>messages.php">
            <input type="hidden" name="action" value="search" />
            <input type="text" name="searchtxt" class=" txt-feild-small"  placeholder="Type here"  />
            <?=get_status_combo()?>
            <input type="submit" value="Go" />
        </form>
    </div>
    </td>
    <td width="326"><a class="btn2" id="send-message-btn" href="#">Send Message</a></td>
    
</tr>
</table>
<div class="clear"></div>
<?php
  if(isset($_POST['action']) && $_POST['action'] == 'send_message'){
    $to_user_id   = $_GET['id'];
    $from_user_id = $_SESSION['ycdc_admin_dbuid'];
    $message      = $_POST['message'];
    $subject      = $_POST['subject'];
    $datetime     = date('Y-m-d H:i:s');
    $rowUser      = get_record_on_id('users', $to_user_id);
    if(!empty($to_user_id)){
      $query = "insert into messages (from_user_id, to_user_id, message_date, subject, message, status, thread_id) 
              values ('$from_user_id','$to_user_id','$date','$subject','$message','0','0'); ";

      $email_template = get_record_on_id('cms', 16);
      $mail_title     = $email_template['title'];
      $mail_content   = $email_template['content'];
      $mail_content   = str_replace('{to_name}',$rowUser['f_name'], $mail_content);
      $mail_content   = str_replace('{login_link}','<a href="'.WWW.'inbox.html" title="Login to your account">HERE</a>', $mail_content);
      $mail_content   = str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);
                    
      $headers = "Content-type:text/html;charset=UTF-8" . "\r\n";
      $headers .= "From: no-reply@youcricketer.com" . "\r\n";
              
      if(mysqli_query($conn,$query) && mail($rowUser['email'],$mail_title,$mail_content,$headers)){
        $counter++;
      }
      ?>
      <script>
      window.location = "<?=ADMIN?>messages.php?id=<?php echo $_GET['id']?>";
      </script>
      <?
    }
  }
?>
<script type="text/javascript" src="<?=WWW?>js/jquery-1.8.0.js"></script>
<script>
$(document).ready(function(){
  $('#send-message-btn').click(function(){
    if($('#send-message').css('display') == 'none' || $('#send-message').css('display') == ''){
      $('#send-message').css('display', 'block');
    }else{
      $('#send-message').css('display', 'none');
    }
  });
});
</script>
<div id="send-message" style="display:none; border:1px solid #ccc; border-radius:4px; padding:4px;" >
  <h2>Send Message to <?=get_combo('users', 'concat(f_name, last_name)', $_GET['id'],'','text')?></h2>
  <form method="post">
    <input type="hidden" name="action" value="send_message">
    <input type="text" name="subject" class="txt-feild-small" placeholder="Type here Subject"><br>
    <textarea name="message" class="txt-feild-large" style="width:400px;height:100px;" placeholder="Type here Message"></textarea>
    <input type="submit" value="Send" class="btn1">
  </form>
</div>
<div class="clear"></div>
  <?      if(isset($_GET['action']) && ($_GET['action'] == 'change' || $_GET['action'] == 'delete')){
    
                $id         = $_GET['id'];
                $row        = get_record_on_id('messages',$id,'','status');
                $status     = $row['status'];
    
                $status     = ($status == '1')?'0':'1';
            
                if($_GET['action'] == 'change'){
                    $query  = "update messages set status = '$status' where id = '$id'";
                }else if($_GET['action'] == 'delete'){
                    $query  = "update messages set status = '".DELETED_STATUS."' where id = '$id'";
                }                             
                if(mysqli_query($conn,$query)){
                    echo '<div id="success"> <b> Success: </b> Information Updated ... ! </div>';
                }else{
                    echo '<div id="error"> <b> Failure: </b> Information cannot be updated ... ! </div>';
                }
            }
             if(isset($_POST['action']) && $_POST['action'] == 'multiple'){
              $ids = array_keys($_POST['ids']);
              $counter = 0;
              foreach($ids as $idr){
                mysqli_query($conn,"update messages set status = '".$_POST['dd_status']."' where id = '$idr'");
                $counter++;
              }
              echo '<div id="success">'.$counter.' records updated ... !</div>';
            }
    ?>
  <div class="clear"></div>
  <form method="post" id="multiple">
    <input type="hidden" name="action" value="multiple">
    <table class="grid" cellspacing="0" cellpadding="0" width="100%">
      <tr>
        <th width="10%">Action</th>
        <th width="20%">From</th>
        <th width="20%">To</th>
        <th width="20%">Subject</th>
        <th width="35%">Message</th>
      </tr>      
      
      <?  $rpp = PRODUCT_LIMIT_ADMIN; // results per page
            $ppage = intval($_GET["page"]);
      if($ppage<=0) $ppage = 1;
      $query = "select * from messages where (from_user_id = '".$_GET['id']."' or to_user_id = '".$_GET['id']."' )";
      
      //=======================================
      if(isset($_GET['action']) && $_GET['action'] == 'search'){
        if(!empty($_GET['searchtxt'])){
          $query  .= "and subject = '%".$_GET['searchtxt']."%'";
        }
        if(!empty($_GET['status']) && $_GET['status'] != ''){
          $query  .= "and status = '".$_GET['status']."'";
        }
      }
     
     // echo $query;
      //=======================================
      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
                echo '<tr><td colspan="5">Your search returned no results</td></tr>';
            }
      $rs   = mysqli_query($conn,$query);
      $tcount = mysqli_num_rows($rs);
      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
      $count = 0;
      $i = ($ppage-1)* $rpp;
          $x = 0;
      while(($count<$rpp) && ($i<$tcount)){
                mysqli_data_seek($rs,$i);
                $row = mysqli_fetch_array($rs);
      ?>
      <tr <?=($x%2 == 0)?' bgcolor="#f3f3f3"':''?>>
        <td>
          <input type="checkbox" name="ids[<?=$row['id']?>]">
          <a href="<?=ADMIN?>messages.php?action=change&id=<?=$row['id']?>" title="Change Status">
                        <?
                          if($row['status'] == '1'){
                            $img = 'status.png';
                            $alt = 'Active Record';
                          }else if($row['status'] == '0'){
                            $img = 'in-status.png';
                            $alt = 'Inactive Record';
                          }else if($row['status'] == DELETED_STATUS){
                            $img = 'restore.png';
                            $alt = 'Deleted Record';
                          }
                        ?>
                        <img src="<?=WWW?>images/icons/<?=$img?>" alt="<?=$alt?>" width="16" />
                      </a>
          <a href="<?=ADMIN?>messages.php?action=delete&id=<?=$row['id']?>" title="Delete Record">
                <img src="<?=WWW?>images/icons/delete.png" alt="Delete" />
            </a>
          <a href="<?=ADMIN?>view-message.php?id=<?=$row['id']?>" title="Record Details">
            <img src="<?=WWW?>images/icons/edit.png" alt="Record Details">
          </a>
        </td>
        <td><?=get_combo('users','f_name',$row['from_user_id'],'','text')?></td>
        <td><?=get_combo('users','f_name',$row['to_user_id'],'','text')?></td>
        <td><?=$row['subject']?></td>
        <td><?=$row['message']?></td>
      </tr>
      <?
        $i++;
        $count++;
        $x++;
      } 
    ?>
    <tr>
      <td>
        <input type="radio" name="checked" value="checked_all"> &nbsp; Checked All
      </td>
      <td>
        <input type="radio" name="checked" value="unchecked_all"> &nbsp; Unchecked All
      </td>
      <td>
        <select name="dd_status" class="dd1">
          <option value="" selected="selected">Select One</option>
          <option value="1">Activate Records</option>
          <option value="0">Deactivate Records</option>
          <option value="<?=DELETED_STATUS?>">Delete Records</option>
        </select>
      </td>
      <td>
        <input type="submit" value="Update">
      </td>
    </tr>
    </table>
<? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_ADMIN){ ?>
    <div id="pagination-bottom">
      <?php
        $keywords = array();
        foreach($_GET as $key=>$value){
          $keywords[] = $key.'='.$value;
        }
        $reload = 'messages.php?'.implode('&',$keywords).'&';
        echo paginate_one($reload, $ppage, $tpages);
      ?>
      <input type="hidden" name="pagination-page" value="messages">
        </div>  
        <? } ?>    
    <div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>home.php" title="Go to Home">Back</a></div>
             
    <div class="clear"></div>
</div>
<!--*****************************-->
<? include('common/footer.php') ?>