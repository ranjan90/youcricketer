<? include('common/header.php');?>	
<!--**********************************-->
<div class="box1">
	<h1>Cities (<?=get_combo('states','name',$_GET['sid'],'','text')?> - <?=get_combo('countries','name',$_GET['id'],'','text')?>)</h1>
	 <table width="99%"  class="grid" border="0">
<tr>
            	

<td width="751">
<div id="options">
     <div id="box" style="width:500px;">
        <form method="post" action="<?=ADMIN?>cities.php?id=<?=$_GET['id']?>&sid=<?=$_GET['sid']?>">
            <input type="hidden" name="action" value="search" />
            <input type="text" name="searchtxt" class=" txt-feild-small"  value="Type here" onclick="if(this.value=='Type here'){this.value=''}" onblur="if(this.value==''){this.value='Type here'}" />
            <?=get_status_combo()?>
            <input type="submit" value="Go" />
        </form>
    </div>
    </td>
   <td width="326"></td>
    <div id="box" class="text" style="width:280px;">
      <td width="117"><a class="btn1" href="<?=ADMIN?>cities.php?id=<?=$_GET['id']?>&sid=<?=$_GET['sid']?>">View All Records</a></td>
      <td width="116"><a class="btn2" href="<?=ADMIN?>add-city.php?id=<?=$_GET['id']?>&sid=<?=$_GET['sid']?>" >Add Record</a></td>
      <td><a  href="<?=ADMIN?>cities.php?id=<?=$_GET['id']?>&sid=<?=$_GET['sid']?>&show_deleted=1" title="Show Deleted" class="btn1">Show Deleted</a></td>
      </div>
                   
</tr>
</table>
                    
                   <div class="clear"></div>
                    <?		$id			= $_GET['cid'];
                            $row 		= get_record_on_id('cities',$id,'status');
                            $status		= $row['status'];

                            $status		= ($status == '1')?'0':'1';

                            if(isset($_GET['action']) && ($_GET['action'] == 'change' || $_GET['action'] == 'delete')){
                                if($_GET['action'] == 'change'){
                                    $query	= "update cities set status = '$status' where id = '$id'";
                                }else if($_GET['action'] == 'delete'){
                                    $query  = "update cities set status = '".DELETED_STATUS."' where id = '$id'";
                                }                             
								
                                if(mysqli_query($conn,$query)){
                                    echo '<div id="success">Information Updated ... !</div>';
                                }else{
                                    echo '<div id="error">Information cannot be updated ... !</div>';
                                }
                            }
                        ?>
                    <div class="clear"></div>
                    <div id="pagination-top"></div>
         	 <table width="1218" class="grid">

		
            <tr>
                <th width="10%">Action</th>
            	  <th width="90%">City</th>
            </tr>

    	<?	$rpp = PRODUCT_LIMIT_ADMIN; // results per page
            $page = intval($_GET["page"]);

			if($page<=0) $page = 1;
			$date  = date('Y-m-d h:m:s');
			$query = "select * from cities where 1=1 ";
      if(isset($_GET['sid'])){
        $query .= " and state_id = '".$_GET['sid']."'";
      }else{
        $query .= " and state_id in (select id from states where country_id = '".$_GET['id']."')";
      }

			//=======================================
			if(isset($_POST['action']) && $_POST['action'] == 'search'){
                if($_POST['searchtxt'] != 'Type Here'){
                    $query  .= "and name like '%".$_POST['searchtxt']."%'";    
                }
                if($_POST['status'] != 'Select One'){
                    $query .= " AND status LIKE '".$_POST['status']."%'";
                }
			}
			if(isset($_GET['show_deleted'])){
                $query .= " and status = '".DELETED_STATUS."'";
              }else{
                $query .= " and status <> '".DELETED_STATUS."'";
              }
      //echo $query;
			//=======================================
			if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
                echo '<tr><td colspan="5">Your search returned no results</td></tr>';
            }
			$rs 	= mysqli_query($conn,$query);
			$tcount = mysqli_num_rows($rs);
			$tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
			$count = 0;
			$i = ($page-1)* $rpp;
        	$x = 0;

			while(($count<$rpp) && ($i<$tcount)){
                mysqli_data_seek($rs,$i);
                $row = mysqli_fetch_array($rs);
			?>
				<tr <?=($x%2 == 0)?' bgcolor="#f3f3f3"':''?>>
                    <td>
                        <a href="<?=ADMIN?>view-city.php?id=<?=$_GET['id']?>&cid=<?=$row['id']?>&sid=<?=$_GET['sid']?>"><img src="<?=WWW?>images/icons/edit.png"></a>
                        <a href="<?=ADMIN?>cities.php?id=<?=$_GET['id']?>&sid=<?=$_GET['sid']?>&cid=<?=$row['id']?>&action=change" title="Change Status">
                        <?
                          if($row['status'] == '1'){
                            $img = 'status.png';
                            $alt = 'Active Record';
                          }else if($row['status'] == '0'){
                            $img = 'in-status.png';
                            $alt = 'Inactive Record';
                          }else if($row['status'] == DELETED_STATUS){
                            $img = 'restore.png';
                            $alt = 'Deleted Record';
                          }
                        ?>
                        <img src="<?=WWW?>images/icons/<?=$img?>" alt="<?=$alt?>" width="16" />
                      </a>
                      <a href="<?=ADMIN?>cities.php?id=<?=$_GET['id']?>&sid=<?=$_GET['sid']?>&cid=<?=$row['id']?>&action=delete" title="Delete Record">
                        <img src="<?=WWW?>images/icons/delete.png" alt="Delete" />
                      </a>
                      </td>
                    <td><?=$row['name']?></td>
                </tr>
			<?
				$i++;
				$count++;
				$x++;
			}	
		?>
</table>
<? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_ADMIN){ ?>
    <div id="pagination-bottom">
      <?php
        $reload = 'cities.php?sid='.$_GET['sid'].'&id='.$_GET['id'].'&';
        echo paginate_one($reload, $ppage, $tpages);
      ?>
      <input type="hidden" name="pagination-page" value="states">
        </div>  
        <? } ?>     
    <div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>countries.php">Back</a></div>
    <div class="clear"></div>
</div>
<!--**********************************-->
<? include('common/footer.php');?>