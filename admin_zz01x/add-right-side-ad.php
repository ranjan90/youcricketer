<? include('common/header.php');?>	
<script type="text/javascript" src="<?php echo WWW;?>js/jquery-ui.min.js"></script>
<link type="text/css" rel="stylesheet" href="<?php echo WWW;?>css/jquery-ui.css">
<script>
$(document).ready(function(){
	$("#start_date, #expiry_date").datepicker({changeYear:"true",yearRange: "1980:2020"});
    $("#start_date, #expiry_date").datepicker({changeYear:"true",minDate:0});
});
</script>
<!--**********************************-->
<div class="box1">
	<h1>Add New Add</h1>
    	<?php	//==========================
			if(isset($_POST['action']) && $_POST['action'] == 'submit'){

				$title		= $_POST['ad_name'];
				$admin_note	= $_POST['admin_note'];
				$sort		= $_POST['sort'];
				$add_info	= $_POST['ad_information'];
				$start_date	= date('Y-m-d',strtotime($_POST['start_date']));//date(strtotime($_POST['start_date']);
				$expiry_date= date('Y-m-d',strtotime($_POST['expiry_date']));//date_format_sql($_POST['expiry_date']);
				$url		= $_POST['ad_link'];

				if(strtotime($_POST['expiry_date']) < strtotime($start_date)){
					echo '<div id="error"><b>Failure : </b>Invalid Expiry date... !</div>';
				}else{
					$rs_chk = mysqli_query($conn,"select * from advertisement where ad_name = '$title' and start_date = '$start_date' and expiry_date = '$expiry_date'");
					if(mysqli_num_rows($rs_chk) == 0){
						$query		= "insert into
												advertisement
												(ad_name,ad_link,sort,ad_information,admin_note,start_date,expiry_date,status)
												values
												('$title','$url','$sort','$add_info','$admin_note','$start_date','$expiry_date','1')";
						
						if(mysqli_query($conn,$query)){
							$id	= mysqli_insert_id($conn);

							if(!empty($_FILES['banner']['name'])){
								if(!is_dir('../ads/right_side/')){
									mkdir('../ads/right_side/',0777);
								}

								if(!empty($_FILES['banner']['name'])){
									$filename 	= trim(str_replace(' ','-',$_POST['ad_name'])).'-'.substr(sha1(rand()), 0, 5).'-'.$id.'.png';
									$image 		= new SimpleImage();
									$image->load($_FILES["banner"]["tmp_name"]);
									$image->save('../ads/right_side/'.$filename);
									mysqli_query($conn,"update advertisement set ad_image = '$filename' where advertise_id= '$id'");
								}
							}
							
							echo '<div id="success"><b>Success : </b>Information Successfully Added ... !</div>';
						}else{
							echo '<div id="error"><b>Failure : </b>Invalid information ... !</div>';
						}
					}else{
						echo '<div id="error"><b>Failure : </b>Information Already Exists ... !</div>';
					}
				}
			}
			//==========================

		?>
        <div style="clear:both; height:30px;"></div>
	<form id="frm-add" method="post" action="add-right-side-ad.php" enctype="multipart/form-data" >
    <input type="hidden" name="action" value="submit" />
	<table id="detail">
		
        <tr>
			<td width="25%">Title : </td>
			<td><input type="text" name="ad_name" class="validate[required] txt-feild-small" value="" placeholder="Please Enter Ad Title Should be unique" /></td>
		</tr>
        <tr>
			<td width="25%">Sort: </td>
			<td><input type="text" name="sort" class="validate[required] txt-feild-small" value="" placeholder="Please Enter Sort Order of ad" </td>
		</tr>
        
        <tr>
			<td width="25%">Ad Information : </td>
			<td><textarea cols="" rows="" name="ad_information" class="txt-feild-large validate[required]" style="height:150px;"></textarea></td>
		</tr>

        <tr class="bannerurl">
			<td width="25%">Ad Image : </td>
			<td><input type="file" name="banner" class="validate[required]"  />     </td>
		</tr>
        <tr class="bannerurl">
			<td width="25%">URL : </td>
			<td><input type="text" name="ad_link" class="txt-feild-small" placeholder="Enter Ad Url" />     </td>
		</tr>
        <tr class="code" style="display:none;">
			<td width="25%">Banner Code : </td>
			<td><textarea name="banner" class="txt-feild-large" style="height:130px;"/></textarea></td>
		</tr>
        <tr>
        	<td>Start Date : </td>
            <td><input id="start_date" readonly="readonly" class="txt-feild-small validate[required]" name="start_date"></td>
        </tr>
        <tr>
        	<td>Expiry Date : </td>
            <td><input id="expiry_date" readonly="readonly" class="txt-feild-small validate[required]" name="expiry_date"></td>
        </tr>
         <tr>
			<td width="25%">Admin Note : </td>
			<td>
            	<textarea cols="100" rows="10" class="txt-feild-large" style="height:150px;" name="admin_note"></textarea>
            </td>
		</tr>
        <tr>
        	<td colspan="2" align="center"><input type="submit" value=" Save " class="btn1" /></td>
        </tr>
	</table>

	</form>

	<div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>right-side-advertisement.php" title="Go to banner">Back</a></div>
	<div class="clear"></div>
<script>
$(document).ready(function(){
	$('input[name=type]').click(function(){
		if(this.value == 'img'){
			$('tr.bannerurl').fadeIn();
			$('tr.bannerurl input').addClass('validate[required]');
			$('tr.bannerurl input').addClass('validate[url]');
			
			$('tr.code').fadeOut();
			$('tr.bannerurl input').removeClass('validate[required]');
		}else{
			$('tr.code').fadeIn();
			$('tr.bannerurl input').addClass('validate[required]');
			
			$('tr.bannerurl').fadeOut();
			$('tr.bannerurl input').removeClass('validate[required]');
			$('tr.bannerurl input').removeClass('validate[url]');
		}
	});
});
</script>
<!--***********************************-->
<? include('common/footer.php')?>