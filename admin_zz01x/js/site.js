$(document).ready(function(){
	$('#pagination-top').html($('#pagination-bottom').html());

	$('#options select[name=status]').removeClass('validate[required]');

	$('select#pagination-combo').change(function(){
		var x = this.value;
		var page = $('input[name=pagination-page]').val();
		var param = $('input[name=pagination-param]').val();
		if(param===undefined){
				window.location = WWW + 'admin_zz01x/'+page+'.php?page='+x;
		}else{
				window.location = WWW + 'admin_zz01x/'+page+'.php?'+param+'page='+x;
		}
	});

	$('form').validationEngine();

	setTimeout(function() {
		$('#success,#error').fadeOut();
	}, 3000);

	$('input[type=file]').change(function(){
		var val = this.value;

		var filesize = Math.round((this.files[0].size)/2000,2);

		if(filesize > 1000){
			$(this).val('');
                        alert("Image is too large ... !");
		}
	});

	$('input[name=checked]').click(function(){
		if(this.value == 'checked_all'){
			$('form#multiple input[type=checkbox]').attr('checked', 'checked');
		}else{
			$('form#multiple input[type=checkbox]').removeAttr('checked', 'checked');
		}
	});

	$(".calender").datepicker({changeYear:"true",yearRange: "1980:2020"});

});
