<? include('common/header.php');?>	
<!--**********************************-->
<div class="box1">
	<h1>Add Team Member</h1>
	<?	
		if($_POST['action'] == 'submit'){
			$picture = false;
			if(!empty($_FILES["banner"]["name"])){
				$file	= strtolower(str_replace(' ','-',($_FILES["banner"]["name"])));
				$file	= strtolower(str_replace('_','-',($file)));
				$title  = $_POST['title'];

				$image 	= new SimpleImage();
				$image->load($_FILES["banner"]["tmp_name"]);

				$image->save('../team/'.$file);
				$picture	= true;
				$designation= $_POST['designation'];
				$sort_order	= $_POST['sort_order'];

				$rs_chk = mysqli_query($conn,"select * from team where name = '$name'");
				if(mysqli_num_rows($rs_chk) == 0){
					$picture = true;
					mysqli_query($conn,"insert into team (name, designation, photo, sort_order, status) 
						values ('$title','$designation','$file','$sort_order','1')");
					chmod('../team/'.$file, 0777);
				}
			}
			
			if($picture === true){
				echo '<div id="success"><b>Success : </b>Information added ... !</div>';
				?>
				<script>
					window.location = '<?=ADMIN?>team.php';
				</script>
				<?
			}else{
				echo '<div id="error"><b>Failure : </b>Information cannot be added ... !</div>';
			}
		}	
	?>
	<div style="clear:both; height:30px;"></div>
	<form id="frm-add" method="post" action="<?=ADMIN?>add-team.php" enctype="multipart/form-data">
    <input type="hidden" name="action" value="submit" />
	<table width="41%"  id="detail">
		<tr>
			<td width="25%">Title : </td>
			<td><input type="text" name="title" class="validate[required] txt-feild-small" /></td>
		</tr>
        <tr>
			<td width="25%">Photo : </td>
			<td><input name="banner" class="validate[required] txt-feild-small" type="file"></td>
		</tr>
		<tr>
			<td width="25%">Designation : </td>
			<td><input name="designation" class="validate[required] txt-feild-small" type="text"></td>
		</tr>
		<tr>
			<td width="25%">Sort Order : </td>
			<td><input name="sort_order" class="validate[required, custom[integer]] txt-feild-small" type="text"></td>
		</tr>
        <tr>
        	<td colspan="2" align="center"><input type="submit" value="Save"class="btn1" /></td>
        </tr>
	</table>

</form>

	<div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>team.php">Back</a></div>

<!--***********************************-->
<? include('common/footer.php')?>