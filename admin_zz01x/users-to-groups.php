<? include('common/header.php');?>	
<!--**********************************-->
<div class="box1">
<h1>Users to Groups (<?=get_combo('groups','title',$_GET['id'],'','text')?>) </h1>
<div class="clear"></div>
    <div id="pagination-top"></div>
    <table class="grid" cellspacing="0" cellpadding="0" width="100%">
      <tr>
        <th width="25%">From</th>
        <th width="25%">To</th>
        <th width="25%">Invitation Date</th>
        <th width="25%">Status</th>
      </tr>      
      
      <?  $rpp = PRODUCT_LIMIT_ADMIN; // results per page
            $ppage = intval($_GET["page"]);
      if($ppage<=0) $ppage = 1;
      $query = "select * from users_to_groups where group_id = ".$_GET['id'];
      
      //=======================================
      if(isset($_POST['action']) && $_POST['action'] == 'search'){
        if(!empty($_POST['searchtxt'])){
          $query  .= "and title = '%".$_POST['searchtxt']."%'";
        }
        if(!empty($_POST['status']) && $_POST['status'] != 'Select One'){
          $query  .= "and status = '".$_POST['status']."'";
        }
      }
      if(isset($_GET['show_deleted'])){
                $query .= " and status = '".DELETED_STATUS."'";
              }else{
                $query .= " and status <> '".DELETED_STATUS."'";
              }
      //echo $query;
      //=======================================
      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
                echo '<tr><td colspan="5">Your search returned no results</td></tr>';
            }
      $rs   = mysqli_query($conn,$query);
      $tcount = mysqli_num_rows($rs);
      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
      $count = 0;
      $i = ($ppage-1)* $rpp;
          $x = 0;
      while(($count<$rpp) && ($i<$tcount)){
                mysqli_data_seek($rs,$i);
                $row = mysqli_fetch_array($rs);
      ?>
      <tr <?=($x%2 == 0)?' bgcolor="#f3f3f3"':''?>>
        <<td><?=get_combo('users','f_name',$row['user_from_id'],'','text')?></td>
        <td><?=get_combo('users','f_name',$row['invitation_to'],'','text')?></td>
        <td><?=date_converter($row['invitation_date'])?></td>
        <td><?=($row['status'] == '1')?'Accepted':'Pending'?></td>
      </tr>
      <?
        $i++;
        $count++;
        $x++;
      } 
    ?>
    </table>
<? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_ADMIN){ ?>
    <div id="pagination-bottom">
      <?php
        $reload = 'users-to-groups.php?';
        echo paginate_one($reload, $ppage, $tpages);
      ?>
      <input type="hidden" name="pagination-page" value="users-to-groups">
        </div>  
        <? } ?>    
    <div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>groups.php" title="Go to Back">Back</a></div>
             
    <div class="clear"></div>
</div>
<!--*****************************-->
<? include('common/footer.php') ?>