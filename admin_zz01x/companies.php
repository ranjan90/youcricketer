<? include('common/header.php');?>
<!--**********************************-->
<div class="box1">
<h1>Organizations </h1>
<table width="99%"  class="grid" border="0">
<tr>

<td width="751">
<div id="options">
     <div id="box" style="width:500px;">
        <form method="get" action="<?=ADMIN?>companies.php">
            <input type="hidden" name="action" value="search" />
            <input type="text" name="searchtxt" class=" txt-feild-small" value="<?=$_GET['searchtxt'];?>" placeholder="Type here" />
            <?//=get_status_combo()?>
			<select class="dd1" id="status" name="status"><option value="" >Select One</option>
				<option value="1" <?php if(isset($_GET['status']) && $_GET['status'] == 1) echo 'selected'; ?> >Active</option>
				<option value="2" <?php if(isset($_GET['status']) && $_GET['status'] == 2) echo 'selected'; ?> >Inactive</option>
			</select>
            <input type="submit" value="Go" />
        </form>
    </div>
    </td>
   <td width="326"></td>
    <div id="box" class="text" style="width:280px;">
      <td width="117"><a class="btn1" href="<?=ADMIN?>companies.php">View All Records</a></td>
      <td width="116">
      </td>
      <td><a  href="<?=ADMIN?>companies.php?show_deleted=1" title="Show Deleted" class="btn1">Show Deleted</a></td>
      </div>

</tr>
</table>
  <?      if(isset($_GET['action']) && ($_GET['action'] == 'change' || $_GET['action'] == 'delete' || $_GET['action'] == 'feature')){

                $id         = $_GET['id'];
                $row        = get_record_on_id('companies',$id,'','status');
                $status     = $row['status'];

                $status     = ($status == '1')?'2':'1';

                if($_GET['action'] == 'change'){
                    $query  = "update companies set status = '$status' where id = '$id'";
                }else if($_GET['action'] == 'delete'){
                    if($row['status'] == '1'){
                    $query  = "update companies set status = '".DELETED_STATUS."' where id = '$id'";
                  }else{
                    $query  = "delete from companies where id = '$id';";
                  }


                }else if($_GET['action'] == 'feature'){
                     $feature  = $row['is_featured'];

                  $feature     = ($feature == '1')?'0':'1';
                  $query    = "update companies set is_featured = '$feature' where id = '$id'";
                }
                if(mysqli_query($conn,$query)){
					if($_GET['action'] == 'change'){
						$sql  = "update users set status = '$status' where id = ".$_GET['uid'];
						mysqli_query($conn,$sql);
					}
					if($_GET['action'] == 'delete'){
						$sql  = "update users set status = '".DELETED_STATUS."' where id = ".$_GET['uid'];
						mysqli_query($conn,$sql);
					}
                    echo '<div id="success"> <b> Success: </b> Information Updated ... ! </div>';
                }else{
                    echo '<div id="error"> <b> Failure: </b> Information cannot be updated ... ! </div>';
                }

				echo '<script>window.location.href="'.ADMIN.'companies.php"</script>';
            }
             if(isset($_POST['action']) && $_POST['action'] == 'multiple'){
              $ids = array_keys($_POST['ids']);
              $counter = 0;
              foreach($ids as $idr){
                $rowRecord = get_record_on_id('companies', $idr);
                if($_POST['dd_status'] == DELETED_STATUS && $rowRecord['status'] == DELETED_STATUS){
                  mysqli_query($conn,"delete from companies where id = '$idr'");
				  mysqli_query($conn,"delete from users where id =".$rowRecord['user_id']);
                }else{
                  mysqli_query($conn,"update companies set status = '".$_POST['dd_status']."' where id = '$idr'");
                }
                $counter++;
              }
              echo '<div id="success">'.$counter.' records updated ... !</div>';
            }
    ?>
  <div class="clear"></div>
  <div id="pagination-top"></div>
  <form method="post" id="multiple">
    <input type="hidden" name="action" value="multiple">
    <table class="grid" cellspacing="0" cellpadding="0" width="100%">
      <tr>
        <th width="10%">Action</th>
		<th width="10%">Status</th>
        <th width="15%">Created By/Contact Name</th>
        <th width="10%">Organization Name</th>
        <th width="10%">Organization Type</th>
		<th width="10%">Country</th>
        <th width="7%">State</th>
		<th width="7%">City</th>
		<!--<th width="14%">Years In Business</th>
        <th width="10%">Contact Name</th>-->

        <th width="5%">Office Phone</th>
        <th width="13%">Registered On</th>
        <th width="10%">Email</th>

      </tr>

      <?  $rpp = PRODUCT_LIMIT_ADMIN; // results per page
            $ppage = intval($_GET["page"]);
      if($ppage<=0) $ppage = 1;

      $query = "select c.*,u.email as user_email, u.create_date as create_date from companies c left join users as u on c.user_id=u.id where 1=1 ";
      //=======================================
      if(isset($_GET['action']) && $_GET['action'] == 'search'){
        if(!empty($_GET['searchtxt'])){
          $query  .= "and company_name like '%".$_GET['searchtxt']."%'";
        }
        if(!empty($_GET['status']) && $_GET['status'] != ''){
          $query  .= "and c.status = '".$_GET['status']."'";
        }
      }
      if(isset($_GET['show_deleted'])){
        $query .= " and c.status = '".DELETED_STATUS."'";
      }else{
        $query .= " and c.status <> '".DELETED_STATUS."'";
      }
      $query .= " order by c.id desc";
      //echo $query;
      //=======================================
      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
                echo '<tr><td colspan="5">Your search returned no results</td></tr>';
            }
      $rs   = mysqli_query($conn,$query);
      $tcount = mysqli_num_rows($rs);
      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
      $count = 0;
      $i = ($ppage-1)* $rpp;
          $x = 0;
      while(($count<$rpp) && ($i<$tcount)){
                mysqli_data_seek($rs,$i);
                $row = mysqli_fetch_array($rs);
				$userDetail=get_record_on_id('users',$row['user_id']);
				$country=get_record_on_id('countries',$userDetail['country_id']);
				$state=get_record_on_id('states',$userDetail['state_id']);

				$city=get_record_on_id('cities',$userDetail['city_id']);

      ?>
      <tr <?=($x%2 == 0)?' bgcolor="#f3f3f3"':''?>>
        <td>
          <input type="checkbox" name="ids[<?=$row['id']?>]">
          <a href="<?=ADMIN?>companies.php?action=change&id=<?=$row['id']?>&uid=<?php echo $row['user_id']; ?>" title="Change Status">
                        <?
                          if($row['status'] == '1'){
                            $img = 'status.png';
                            $alt = 'Active Record';
                          }else if($row['status'] == '2'){
                            $img = 'in-status.png';
                            $alt = 'Inactive Record';
                          }else if($row['status'] == DELETED_STATUS){
                            $img = 'restore.png';
                            $alt = 'Deleted Record';
                          }
                        ?>
                        <img src="<?=WWW?>images/icons/<?=$img?>" alt="<?=$alt?>" width="16" />
                      </a>
          <? if($row['status'] != DELETED_STATUS){ ?>
            <a href="<?=ADMIN?>companies.php?action=delete&id=<?=$row['id']?>&uid=<?php echo $row['user_id']; ?>" title="Delete Record">
                <img src="<?=WWW?>images/icons/delete.png" alt="Delete" />
            </a>
            <? } ?>
 <a href="<?=ADMIN?>view-company.php?action=delete&id=<?=$row['id']?>&uid=<?php echo $row['user_id']; ?>" title="Edit">
                <img src="<?=WWW?>images/icons/edit.png" alt="Delete" />
            </a>

          <a href="<?=WWW?>company-detail-<?=$row['id']?>-<?=friendlyURL($row['company_name'])?>.html" target="_blank" title="View Profile">
              <img src="<?=WWW?>images/icons/view.png" alt="View" />
          </a>
          <a href="<?=ADMIN?>make-card.php?id=<?=$row['user_id']?>" title="Make Card">
              <img src="<?=WWW?>images/icons/card.png" alt="card" />
          </a>
        </td>
		<td><?php if($row['status'] ==1) echo 'Active';elseif($row['status'] ==0 || $row['status'] ==2) echo 'Inactive';elseif($row['status'] ==8) echo 'Deleted'; ?></td>
        <td><?=ucwords($userDetail['f_name']." ".$userDetail['last_name'])?></td>
        <td><?=$row['company_name']?></td>
        <td><?=get_combo('company_types','name',$row['company_type_id'],'','text')?></td>
		<td><?php echo $country['name']; ?></td>
		<td><?php echo $state['name']; ?></td>
		<td><?php echo $userDetail['city_name']; ?></td>
        <!--<td><?=$row['years_in_business']?></td>
        <td><?=$row['contact_name']?></td>-->
        <td><?=$row['office_phone']?></td>
        <td><?=date("M d, Y",strtotime($row['create_date']));?></td>
        <td><?=$row['user_email']?></td>
      </tr>
      <?
        $i++;
        $count++;
        $x++;
      }
    ?><tr>
      <td>
        <input type="radio" name="checked" value="checked_all"> &nbsp; Checked All
      </td>
      <td>
        <input type="radio" name="checked" value="unchecked_all"> &nbsp; Unchecked All
      </td>
      <td>
        <select name="dd_status" class="dd1">
          <option value="" selected="selected">Select One</option>
          <option value="1">Activate Records</option>
          <option value="2">Deactivate Records</option>
          <option value="<?=DELETED_STATUS?>">Delete Records</option>
        </select>
      </td>
      <td>
        <input type="submit" value="Update">
      </td>
    </tr>
    </table>
<? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_ADMIN){ ?>
    <div id="pagination-bottom">
      <?php
        $keywords = array();
        foreach($_GET as $key=>$value){
          $keywords[] = $key.'='.$value;
        }
        $reload = 'companies.php?action=search&searchtxt='.$_GET['searchtxt'].'&status='.$_GET['status'].'&';
        echo paginate_one($reload, $ppage, $tpages);
      ?>
      <input type="hidden" name="pagination-page" value="companies">
        </div>
        <? } ?>
    <div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>home.php" title="Go to Home">Back</a></div>

    <div class="clear"></div>
</div>
<!--*****************************-->
<? include('common/footer.php') ?>
