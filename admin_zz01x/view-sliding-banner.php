<? include('common/header.php');?>	
<!--**********************************-->
<div class="box1">
	<h1>Sliding Banner</h1>
	<?	
		if($_POST['action'] == 'submit'){
			if(!empty($_FILES["banner"]["tmp_name"])){
				$file	= strtolower(str_replace(' ','-',($_FILES["banner"]["name"])));
				$file	= strtolower(str_replace('_','-',($file)));
						
				$image 	= new SimpleImage();
				$image->load($_FILES["banner"]["tmp_name"]);
				if($image->getWidth() >= 650){
					$image->resizeToWidth(650);
				}else{
					echo '<div id="error"><b>Failure : </b>Banner size is small... !</div>';
				}
				
				if($image->save('../banners/'.$file) == true){
					$picture	= true;
					$web_url 	= $_POST['web_url'];
					$sort_order	= $_POST['sort_order'];

					mysqli_query($conn,"update sliding_banners set content = '$file' where id = '".$_GET['id']."'");
				}else{
					$picture	= false;
				}
			}
			
			
				mysqli_query($conn,"update sliding_banners set sort_order = '$sort_order' where id = '".$_GET['id']."'");
				echo '<div id="success"><b>Success : </b>Information added ... !</div>';
				?>
				<script>
					window.location = '<?=ADMIN?>sliding-banners.php';
				</script>
				<?
			
		}

		$id = $_GET['id'];
		$row= get_record_on_id('sliding_banners', $id);


	?>
	<div style="clear:both; height:30px;"></div>
	<form id="frm-add" method="post" action="<?=ADMIN?>view-sliding-banner.php?id=<?=$_GET['id']?>" enctype="multipart/form-data">
    <input type="hidden" name="action" value="submit" />
	<table width="41%"  id="detail">
		<tr>
			<td width="25%">Title : </td>
			<td><input type="text" name="title" class="validate[required] txt-feild-small" value="<?=$row['title']?>" /></td>
		</tr>
        <tr>
			<td width="25%">Banner File : </td>
			<td>
				<input name="banner" class="txt-feild-small" type="file">
				<br>
				<img src="<?=WWW?>banners/<?=$row['content']?>" />
			</td>
		</tr>
		<tr>
			<td width="25%">Website URL : </td>
			<td><input name="web_url" class="validate[required, custom[url]] txt-feild-small" type="text" value="<?=$row['web_url']?>"></td>
		</tr>
		<tr>
			<td width="25%">Sort Order : </td>
			<td><input name="sort_order" class="validate[required, custom[integer]] txt-feild-small" type="text" value="<?=$row['sort_order']?>"></td>
		</tr>
        <tr>
        	<td colspan="2" align="center"><input type="submit" value="Save"class="btn1" /></td>
        </tr>
	</table>

</form>

	<div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>sliding-banners.php">Back</a></div>

<!--***********************************-->
<? include('common/footer.php')?>