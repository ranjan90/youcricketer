<? include('common/header.php');?>	
<!--**********************************-->
<script type="text/javascript" src="<?php echo WWW;?>js/jquery-ui.min.js"></script>
<link type="text/css" rel="stylesheet" href="<?php echo WWW;?>css/jquery-ui.css">
<script>
$(document).ready(function(){
	$("#start_date, #expiry_date").datepicker({changeYear:"true",yearRange: "1980:2020"});
    $("#start_date, #expiry_date").datepicker({changeYear:"true",minDate:0});
});
</script>
<!--**********************************-->
<div class="box1">
	<h1>Update Banner</h1>
    	<?	//==========================
			if(isset($_POST['action']) && $_POST['action'] == 'update'){
				
				$title		= $_POST['title'];
				$admin_note	= $_POST['admin_note'];
				$placement	= $_POST['banner_placement_id'];
				$user_info	= $_POST['user_info'];
				$start_date	= date_format_sql($_POST['start_date']);
				$expiry_date= date_format_sql($_POST['expiry_date']);
				$date		= date('Y-m-d');
				$url	= $_POST['url'];
				$status		= $_POST['status'];
				$id	= $_POST['id'];
				
				if(strtotime($expiry_date) < strtotime($start_date)){
					echo '<div id="error"><b>Failure : </b>Invalid Expiry date... !</div>';
				}else{
					$query		= "update banners set admin_note = '$admin_note', title = '$title', banner_placement_id = '$placement', user_info = '$user_info', start_date = '$start_date', expiry_date = '$expiry_date',status = '$status' where id = '$id'";
					//echo $query;exit;
					
					if($_POST['type'] == 'img'){
								if(!is_dir('../ads/'.$id)){
									mkdir('../ads/'.$id,0777);
								}
								
								if(!empty($_FILES['banner']['name'])){
									$filename 	= urlencode($_POST['title']).'.png';
									$image 		= new SimpleImage();
									$image->load($_FILES["banner"]["tmp_name"]);
									$image->save('../ads/'.$id.'/'.$filename);
									$content = 'img_______'.$filename.'_______'.$url;
									mysqli_query($conn,"update banners set content = '$content' where id= '$id'");
								}
							}else{
								$content = 'code_______'.$_POST['banner'];
								mysqli_query($conn,"update banners set content = '$content' where id= '$id'");
							}
					
					/*
					
					if(!is_dir('../ads/'.$id)){
								mkdir('../ads/'.$id,0777);
							}
					if(!empty($_FILES['banner']['name'])){
								$filename 	= urlencodeStocuri($_POST['title']).'.png';
								$image 		= new SimpleImage();
								$image->load($_FILES["banner"]["tmp_name"]);
								$image->save('../ads/'.$id.'/'.$filename);
								$content = $filename.'_______'.$url;
								mysqli_query($conn,"update banners set content = '$content' where id= '$id'");
							}*/
					
					if(mysqli_query($conn,$query)){
						echo '<div id="success"><b>Success : </b>Information Updated Successfully ... !</div>';
					}else{
						echo '<div id="error"><b>Failure : </b>Information cannot be updated ... !</div>';
					}
				}
			}
			//==========================
			if(isset($_GET['id'])){
				$id = $_GET['id'];
			}else{
				$id = $_POST['id'];
			}
			$row	= get_record_on_id('banners',$id);
			$content= explode('_______',$row['content']);
		?>
        <div style="clear:both; height:30px;"></div>
	<form id="frm-add" method="post" action="view-banner.php" enctype="multipart/form-data" >
    <input type="hidden" name="action" value="update" />
    <input type="hidden" name="id" value="<?=$id?>" />
    <input type="hidden" name="type" value="<?=$content[0]?>" />
	<table id="detail">
		
        <tr>
			<td width="25%">Title : </td>
			<td><input type="text" name="title" class="validate[required] txt-feild-small" value="<?=$row['title']?>" /></td>
		</tr>
        <tr>
			<td width="25%">Placement: </td>
			<td><?=get_combo('banner_placements','name',$row['banner_placement_id'], 'banner_placement_id','','','','validate[required]')?></td>
		</tr>
        <tr>
			<td width="25%">User Information : </td>
			<td><textarea cols="" rows="" name="user_info" class="txt-feild-large validate[required]" style="height:150px;"><?php echo $row['user_info']?></textarea></td>
		</tr>
        <? if($content[0] == 'code'){ ?>
        <tr>
			<td width="25%">Banner Code : </td>
			<td><textarea name="banner" class="txt-feild-large" style="height:130px;"/><?=$content[1]?></textarea></td>
		</tr>
        <? }else{  ?>
        <tr>
			<td width="25%">Banner Image: </td>
			<td>
				<input type="file" name="banner" /><br /><img src="<?=WWW?>ads/<?=$row['id']?>/<?=$content[1]?>" />
			</td>
		</tr>
        <tr>
			<td width="25%">Banner URL : </td>
			<td><input type="text" name="url" value="<?=$content[2]?>" class="validate[required,url] txt-feild-small" />     </td>
		</tr>
        <? } ?>
        <tr>
        	<td>Start Date : </td>
            <td><input id="start_date" readonly="readonly" class="date-pick dp-applied txt-feild-small validate[required]" name="start_date" value="<?=date_format_calender($row['start_date'])?>">
               	<p class="dp-choose-date" title="Choose date"></p>
            </td>
        </tr>
        <tr>
        	<td>Expiry Date : </td>
            <td><input id="expiry_date" readonly="readonly" class="date-pick dp-applied txt-feild-small validate[required]" name="expiry_date" value="<?=date_format_calender($row['expiry_date'])?>">
               	<p class="dp-choose-date" title="Choose date"></p>
            </td>
        </tr>
        <tr>
			<td width="25%">Admin Note  : </td>
			<td>
				<textarea cols="100" rows="10" class="txt-feild-large" style="height:150px;" name="admin_note"><?=stripslashes($row['admin_note'])?></textarea>
            </td>
		</tr>
        <tr>
			<td width="25%">Status : </td>
			<td><?=get_status_combo($row['status'])?></td>
		</tr>
        <tr>
        	<td colspan="2" align="center"><input type="submit" value=" Save " class="btn1" /></td>
        </tr>
	</table>

	</form>

	<div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>banners.php">Back</a></div>
	<div class="clear"></div>
<!--***********************************-->
<? include('common/footer.php')?>