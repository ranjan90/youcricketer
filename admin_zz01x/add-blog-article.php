<? include('common/header.php');?>	
<!--**********************************-->
<div class="box1">
	<h1>Add Blog Article</h1>
    	<?	//==========================
			if(isset($_POST['action']) && $_POST['action'] == 'submit'){
				
				$name		= $_POST['name'];
				$category_id= $_POST['category_id'];
				$content 	= $_POST['content'];
				$date 		= date('Y-m-d');

				$query		= "insert into blog_articles (category_id, title, content, create_date, status) 
				values ('$category_id','$title','$content','$date','1')";
				
				if(mysqli_query($conn,$query)){
					echo '<div id="success"><b>Success : </b>Information Added ... !</div>';
					?>
						<script>
						window.location = '<?=ADMIN?>blog-articles.php';
						</script>
					<?
				}else{
					echo '<div id="error"><b>Failure : </b>Information already exists ... !</div>';
				}
			}
			//==========================
		?>
        <div style="clear:both; height:30px;"></div>
	<form id="frm-add" method="post" action="<?php echo ADMIN?>add-blog-article.php" >
    <input type="hidden" name="action" value="submit" />
	<table id="detail">
		
        <tr>
			<td width="25%">Blog Category : </td>
			<td><?=get_combo('blog_categories','name','','category_id')?></td>
		</tr>
		<tr>
			<td width="25%">Title : </td>
			<td><input type="text" name="title" class="txt-feild-small validate[required]"></td>
		</tr>
		<tr>
			<td>Content</td>
			<td><textarea class="validate[required] txt-feild-large" style="height:200px;" name="content"></textarea></td>
		</tr>
		
        <tr>
        	<td colspan="2" align="center"><input type="submit" value=" Save" class="btn1" /></td>
        </tr>
	</table>
	</form>
	<div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>blog-articles.php" >Back</a></div>
    <div class="clear"></div>

<!--***********************************-->
<? include('common/footer.php')?>