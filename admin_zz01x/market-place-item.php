<? include('common/header.php');?>
    <!--**********************************-->
<div class="box1">
    <h1>Market Place Items</h1>
    <table width="99%"  class="grid" border="0">
<tr>
<td width="751">
<div id="options">
     <div id="box" style="width:500px;">
        <form method="get" action="<?=ADMIN?>market-place-item.php">
            <input type="hidden" name="action" value="search" />
            <input type="text" name="searchtxt" class=" txt-feild-small" value="<? echo ((isset($_GET['searchtxt']))?$_GET['searchtxt']:'')?>"  placeholder="Item Title" />

            <?=get_status_combo_selected($_GET['status']);?>

                <select name="item_type" id="item_type" class="dd1 form-control">
                    <option selected="selected" value="">Item Type</option>
                    <option value="1" <?php if($_GET['item_type']==1) echo 'selected'; ?>>Global</option>
                    <option value="2" <?php if($_GET['item_type']==2) echo 'selected'; ?>>Regional</option>
                </select>

            <input type="submit" value="Go" />
        </form>
    </div>
    </td>
   <td width="326"></td>
    <div id="box" class="text" style="width:280px;">
      <td width="117"><a class="btn1" href="<?=ADMIN?>market-place-item.php">View All Records</a></td>
      <td width="116"><a class="btn2" href="<?=ADMIN?>add-market-place-item.php" >Add Record</a></td>
      <td><a  href="<?=ADMIN?>market-place-item.php?show_deleted=1" title="Show Deleted" class="btn1">Show Deleted</a></td>
      <!--<td><a  href="<?/*=ADMIN*/?>banners.php?show_deleted=1" title="Show Deleted" class="btn1">Show Deleted</a></td>
      --></div>

</tr>
</table>
    <div class="clear"></div>
    <?

    if(isset($_GET['update_sponsor']) && !empty($_GET['update_sponsor']))
    {
        $itemID = (int) $_GET['update_sponsor'];
        $query	= "update marke_place_items set is_sponsor = 0";
        mysqli_query($conn,$query);
        $query	= "update marke_place_items set is_sponsor = 1 WHERE item_id = $itemID";
        mysqli_query($conn,$query);
        echo '<div id="success">'.$counter.' records updated ... !</div>';
    }
    if(isset($_GET['action']) && ($_GET['action'] == 'change' || $_GET['action'] == 'delete')){

				$id			= $_GET['id'];
                $query = 'select * from marke_place_items where item_id = '.$id;
                $rs = mysqli_query($conn,$query);
                $row = mysqli_fetch_assoc($rs);
				$status		= $row['status'];
                $status		= ($status == '1')?'0':'1';

				if($_GET['action'] == 'change'){
					$query	= "update marke_place_items set status = '$status' where item_id = '$id'";
                    mysqli_query($conn,$query);

				}elseif($_GET['action'] == 'delete'){
          $rc = mysqli_num_rows(mysqli_query($conn, "select item_id from marke_place_items where item_id=".$id." and status=".DELETED_STATUS));
          if($rc > 0){
            $query = "DELETE FROM `marke_place_items` WHERE item_id=".$id;
          }else{
            $query  = "update marke_place_items set status = '".DELETED_STATUS."' where item_id = '$id'";
          }
          if(mysqli_query($conn,$query)){
              echo '<div id="success"> <b> Success: </b> Information Updated ... ! </div>';
          }else{
              echo '<div id="error"> <b> Failure: </b> Information cannot be updated ... ! </div>';
          }
				}

			}
      if(isset($_POST['action']) && $_POST['action'] == 'multiple'){
        if(count($_POST['ids'])>0){
              $ids = array_keys($_POST['ids']);
              if($_POST['dd_status']==8){
                $counter = 0;
                foreach($ids as $idr){
                  $rc = mysqli_num_rows(mysqli_query($conn, "select item_id from marke_place_items where item_id=".$idr." and status=".DELETED_STATUS));
                  if($rc > 0){
                    $query = "DELETE FROM `marke_place_items` WHERE item_id=".$idr;
                  }else{
                    $query  = "update marke_place_items set status = '".DELETED_STATUS."' where item_id = '$idr'";
                  }
                  mysqli_query($conn,$query);
                  $counter++;
                }
              }else{
                $counter = 0;
                foreach($ids as $idr){
                    $query  = "update marke_place_items set status = '".$_POST['dd_status']."' where item_id = '$idr'";
                    mysqli_query($conn,$query);
                    $counter++;
                }
              }
              echo '<div id="success">'.$counter.' records updated ... !</div>';
            }else{
              echo '<div id="error">No Record Selected...</div>';
            }
          }

    if(isset($_GET['noRecord'])){
        echo '<div id="error"> <b> Failure: </b> Information Not Exists ... ! </div>';
    }
	?>
    <div class="clear"></div>
    <div id="pagination-top"></div>
    <form method="post" id="multiple">
    <input type="hidden" name="action" value="multiple">
    <table class="grid" cellspacing="0" cellpadding="0" width="100%">
      <tr>
        <th width="10%">Action</th>
        <th width="14%">Item Title</th>
        <th width="7%">Item Price</th>
        <th width="10%">Item Type</th>
        <th width="10%">Item Detail</th>
        <th width="10%">Country</th>
        <th width="10%">State</th>
        <th width="10%">City</th>
        <th width="15%">Admin Note</th>
        <!--<th width="20%">Is Sponsor</th>-->
        <th width="7%">Status</th>

      </tr>

      <?  $rpp = PRODUCT_LIMIT_ADMIN; // results per page
            $ppage = intval($_GET["page"]);
      if($ppage<=0) $ppage = 1;
      $query = "select * from marke_place_items where 1=1 ";

      //=======================================
      if(isset($_GET['action']) && $_GET['action'] == 'search'){
            $search = (string) $_GET['searchtxt'];
            $query  .= " and item_title LIKE '%$search%'";
          if(isset($_GET['status']) && $_GET['status'] != ''){
              $status = $_GET['status'];
              $query  .= " and status = '$status'";
          }

          if(isset($_GET['item_type']) && $_GET['item_type'] != ''){
              $item_type = $_GET['item_type'];
              $query  .= " and item_type = '$item_type'";
          }
      }
      if(isset($_GET['show_deleted'])){
        $query .= " and status = '".DELETED_STATUS."'";
      }else{
        $query .= " and status <> '".DELETED_STATUS."'";
      }
      //echo $query;
      //=======================================
      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
                echo '<tr><td colspan="5">Your search returned no results</td></tr>';
            }
      $rs   = mysqli_query($conn,$query);
      $tcount = mysqli_num_rows($rs);
      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
      $count = 0;
      $i = ($ppage-1)* $rpp;
          $x = 0;
      while(($count<$rpp) && ($i<$tcount)){
                mysqli_data_seek($rs,$i);
                $row = mysqli_fetch_array($rs);
      ?>
      <tr <?=($x%2 == 0)?' bgcolor="#f3f3f3"':''?>>
        <td align="center">
          <input type="checkbox" name="ids[<?=$row['item_id']?>]">
          <a href="<?=ADMIN?>add-market-place-item.php?id=<?=$row['item_id']?>" title="Update Record">
              <img src="<?=WWW?>images/icons/edit.png" alt="Edit" />
            </a>
            <a href="<?=ADMIN?>market-place-item.php?action=change&id=<?=$row['item_id']?>" title="Change Status">
                        <?
                          if($row['status'] == '1'){
                            $img = 'status.png';
                            $alt = 'Active Record';
                          }else if($row['status'] == '0'){
                            $img = 'in-status.png';
                            $alt = 'Inactive Record';
                          }else if($row['status'] == DELETED_STATUS){
                            $img = 'restore.png';
                            $alt = 'Deleted Record';
                          }
                        ?>
                        <img src="<?=WWW?>images/icons/<?=$img?>" alt="<?=$alt?>" width="16" />
                      </a>
            <a href="<?=ADMIN?>market-place-item.php?action=delete&id=<?=$row['item_id']?>" title="Delete Record">
              <img src="<?=WWW?>images/icons/delete.png" alt="Delete" />
            </a>

        </td>
        <td><?=$row['item_title']?> </td>
        <td><?=$row['item_price']?> </td>
        <td><?php if($row['item_type']=='1') echo "Global";elseif($row['item_type']=='2') echo "Regional"; ?> </td>
        <td><?=substr($row['item_detail'],0,100).((strlen($row['item_detail']) >= 100)?' ...':'')?></td>
        <td><?=$row['item_country']?> </td>
        <td><?=$row['item_state']?> </td>
        <td><?=$row['item_city']?> </td>
        <td>
        <? echo $row['admin_note']?>
        </td>

             <!--<td align="center">
              <?php if( $row['is_sponsor'] == 0 )
              {
                  echo "<a href=".ADMIN."market-place-item.php?update_sponsor=".$row['item_id']."> Make </a>";
              } else {
                  echo 'Yes';
              }

              ?></td>-->

          <td align="center">
              <?php if( $row['status'] == 0 )
              {
                  echo "Deactive";
              } else {
                  echo 'Active';
              }

              ?></td>



      </tr>
      <?
        $i++;
        $count++;
        $x++;
      }
    ?>
    <tr>
      <td>
        <input type="radio" name="checked" value="checked_all"> &nbsp; Checked All
      </td>
      <td>
        <input type="radio" name="checked" value="unchecked_all"> &nbsp; Unchecked All
      </td>
      <td>
        <select name="dd_status" class="dd1">
          <option value="" selected="selected">Select One</option>
          <option value="1">Activate Records</option>
          <option value="0">Deactivate Records</option>
          <option value="<?=DELETED_STATUS?>">Delete Records</option>
        </select>
      </td>
      <td>
        <input type="submit" value="Update">
      </td>
    </tr>
    </table>
<? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_ADMIN){ ?>
    <div id="pagination-bottom">
      <?php
       $keywords = array();
        foreach($_GET as $key=>$value){
          $keywords[] = $key.'='.$value;
        }
        $param = 'action=search&searchtxt='.$_GET['searchtxt'].'&status='.$_GET['status'].'&item_type='.$_GET['item_type'].'&';
        $reload = 'market-place-item.php?'.$param;
        echo paginate_one($reload, $ppage, $tpages);
      ?>
      <input type="hidden" name="pagination-page" value="market-place-item">
      <input type="hidden" name="pagination-param" value="<?=$param?>">
        </div>
        <? } ?>
    <div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>home.php" title="Go to Home">Back</a></div>
    <div class="clear"></div>
  </div>
  <!--**********************************-->
  <? include('common/footer.php');?>
