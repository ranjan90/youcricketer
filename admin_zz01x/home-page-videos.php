<? include('common/header.php');?>
    <!--**********************************-->
<div class="box1">
    <h1>Home Page Videos</h1>
    <table width="99%"  class="grid" border="0">
<tr>


<td width="751">
<div id="options">
     <div id="box" style="width:500px;">
        <form method="get" action="<?=ADMIN?>home-page-videos.php">
            <input type="hidden" name="action" value="search" />
            <input type="text" name="searchtxt" class=" txt-feild-small" value="<?=$_GET['searchtxt'];?>" placeholder="Type here" />
            <?=get_status_combo_selected($_GET['status'])?>
            <input type="submit" value="Go" />
        </form>
    </div>
    </td>
   <td width="326"></td>
    <div id="box" class="text" style="width:280px;">
      <td width="117"><a class="btn1" href="<?=ADMIN?>home-page-videos.php">View All Records</a></td>
      <td width="116"><a class="btn2" href="<?=ADMIN?>add-home-page-video.php" >Add Record</a></td>
      <td><a  href="<?=ADMIN?>home-page-videos.php?show_deleted=1" title="Show Deleted" class="btn1">Show Deleted</a></td>
      </div>

</tr>
</table>
    <div class="clear"></div>
    <?		$id			= $_GET['id'];
			$row 		= get_record_on_id('home_page_videos',$id,'status');
            $status		= $row['status'];

            $status		= ($status == '1')?'0':'1';

            if(isset($_GET['action']) && ($_GET['action'] == 'change' || $_GET['action'] == 'delete')){
            	if($_GET['action'] == 'change'){
                	$query	= "update home_page_videos set status = '$status' where id = '$id'";
                }else if($_GET['action'] == 'delete'){
                  $rc = mysqli_num_rows(mysqli_query($conn, "select id from home_page_videos where id=".$id." and status=".DELETED_STATUS));
                  if($rc > 0){
                    $query = "DELETE FROM `home_page_videos` WHERE id=".$id;
                  }else{
                    $query  = "update home_page_videos set status = '".DELETED_STATUS."' where id = '$id'";
                  }
	            }

                if(mysqli_query($conn,$query)){
                	echo '<div id="success">Information Updated ... !</div>';
                }else{
                	echo '<div id="error">Information cannot be updated ... !</div>';
                }
            }
            if(isset($_POST['action']) && $_POST['action'] == 'multiple'){
              $ids = array_keys($_POST['ids']);
              if($_POST['dd_status']==8){
                $counter = 0;
                foreach($ids as $idr){
                  $rc = mysqli_num_rows(mysqli_query($conn, "select id from home_page_videos where id=".$idr." and status=".DELETED_STATUS));
                  if($rc > 0){
                    $query = "DELETE FROM `home_page_videos` WHERE id=".$idr;
                  }else{
                    $query  = "update home_page_videos set status = '".DELETED_STATUS."' where id = '$idr'";
                  }
                  mysqli_query($conn,$query);
                  $counter++;
                }
              }else{
                $counter = 0;
                foreach($ids as $idr){
                    $query  = "update home_page_videos set status = '".$_POST['dd_status']."' where id = '$idr'";
                    mysqli_query($conn,$query);
                  $counter++;
                }
              }
              echo '<div id="success">'.$counter.' records updated ... !</div>';
            }
        ?>
    <div class="clear"></div>
    <div id="pagination-top"></div>
    <form method="post" id="multiple">
<input type="hidden" name="action" value="multiple">
    <table class="detail" cellspacing="0" cellpadding="0">
      <tr>
      	<th width="10%">Action</th>
        <th width="90%">Video</th>
      </tr>
      <? $rpp = PRODUCT_LIMIT_ADMIN; // results per page
      $ppage = intval($_GET["page"]);

if($ppage<=0){
  $ppage = 1;
  $start=0;
  $limit=PRODUCT_LIMIT_ADMIN;
}else{
  $start=($ppage-1)*PRODUCT_LIMIT_ADMIN;
  $limit=$ppage*PRODUCT_LIMIT_ADMIN;
}
      $date  = date('Y-m-d h:m:s');
      $query = "select * from home_page_videos where 1=1 ";
      //=======================================
      if(isset($_GET['action']) && $_GET['action'] == 'search'){
        if(!empty($_GET['searchtxt'])){
          $query  .= "and title like '%".$_GET['searchtxt']."%'";
        }
        if($_GET['status'] != ''){
          $query  .= "and status = '".$_GET['status']."'";
        }
      }
      if(isset($_GET['show_deleted'])){
                $query .= " and status = '".DELETED_STATUS."'";
              }else{
                $query .= " and status <> '".DELETED_STATUS."'";
              }

      //=======================================
      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
                echo '<tr><td colspan="5">There is no record in this selection ... !</td></tr>';
            }
      $rs = mysqli_query($conn,$query);
      $tcount = mysqli_num_rows($rs);
      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
      $count = 0;
      $i = ($ppage-1)* $rpp;
      $x = 0;
      while(($count<$rpp) && ($i<$tcount)){
                mysqli_data_seek($rs,$i);
                $row = mysqli_fetch_array($rs);
      ?>
      <tr <?=($x%2 == 0)?' bgcolor="#f3f3f3"':''?>>
        <td>
          <input type="checkbox" name="ids[<?=$row['id']?>]">
          <a href="<?=ADMIN?>view-home-page-video.php?id=<?=$row['id']?>"><img src="<?=WWW?>images/icons/edit.png"></a>
          <a href="<?=ADMIN?>home-page-videos.php?action=change&id=<?=$row['id']?>" title="Change Status">
          <?
            if($row['status'] == '1'){
              $img = 'status.png';
              $alt = 'Active Record';
            }else if($row['status'] == '0'){
              $img = 'in-status.png';
              $alt = 'Inactive Record';
            }else if($row['status'] == DELETED_STATUS){
              $img = 'restore.png';
              $alt = 'Deleted Record';
            }
          ?>
          <img src="<?=WWW?>images/icons/<?=$img?>" alt="<?=$alt?>" width="16" />
          </a>
          <a href="<?=ADMIN?>home-page-videos.php?action=delete&id=<?=$row['id']?>" title="Delete Record">
            <img src="<?=WWW?>images/icons/delete.png" alt="Delete" />
          </a>
        </td>
        <td><?=$row['content']?></td>
      <tr>
      <?
        $x++;
        $i++;
        $count++;
      }
    ?>
    <tr>
      <td>
        <input type="radio" name="checked" value="checked_all"> &nbsp; Checked All
      </td>
      <td>
        <input type="radio" name="checked" value="unchecked_all"> &nbsp; Unchecked All
        <select name="dd_status" class="dd1">
          <option value="" selected="selected">Select One</option>
          <option value="1">Activate Records</option>
          <option value="0">Deactivate Records</option>
          <option value="<?=DELETED_STATUS?>">Delete Records</option>
        </select>
        <input type="submit" value="Update">
      </td>
    </tr>
     </table>
<? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_ADMIN){ ?>
    <div id="pagination-bottom">
      <?php
        $keywords = array();
        foreach($_GET as $key=>$value){
          $keywords[] = $key.'='.$value;
        }
        $param = "action=search&searchtxt=".$_GET['searchtxt']."&status=".$_GET['status']."&";
        $reload = 'home-page-videos.php?'.$param;
        echo paginate_one($reload, $ppage, $tpages);
      ?>
      <input type="hidden" name="pagination-page" value="home-page-videos">
      <input type="hidden" name="pagination-param" value="<?=$param?>">
        </div>
        <? } ?>
    <div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>home.php" title="Go to Home">Back</a></div>
    <div class="clear"></div>
  </div>
  <!--**********************************-->
  <? include('common/footer.php');?>
