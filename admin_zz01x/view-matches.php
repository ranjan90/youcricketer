<? include('common/header.php');?>
<!--**********************************-->
<div class="box1">
<h1>
  Matches (<?=get_combo('series', 'title', $_GET['id'],'','text')?>)
  <a class="right" href="<?php echo ADMIN;?>add-match.php?id=<?php echo $_GET['id'];?>"><img src="<?php echo WWW;?>images/icons/add.png"></a>
</h1>

  <?      if(isset($_GET['action']) && ($_GET['action'] == 'change' || $_GET['action'] == 'delete' || $_GET['action'] == 'feature')){

                $id         = $_GET['id'];
                $row        = get_record_on_id('clubs',$id,'','status');
                $status     = $row['status'];

                $status     = ($status == '1')?'0':'1';

                if($_GET['action'] == 'change'){
                    $query  = "update clubs set status = '$status' where id = '$id'";
                }else if($_GET['action'] == 'delete'){
                    $query  = "update clubs set status = '".DELETED_STATUS."' where id = '$id'";
                }else if($_GET['action'] == 'feature'){
                  $feature  = $row['is_featured'];

                  $feature     = ($feature == '1')?'0':'1';
                  $query    = "update clubs set is_featured = '$feature' where id = '$id'";
                }
                if(mysqli_query($conn,$query)){
                    echo '<div id="success"> <b> Success: </b> Information Updated ... ! </div>';
                }else{
                    echo '<div id="error"> <b> Failure: </b> Information cannot be updated ... ! </div>';
                }
            }
            if(isset($_POST['action']) && $_POST['action'] == 'multiple'){
              $ids = array_keys($_POST['ids']);
              $counter = 0;
              foreach($ids as $idr){
                mysqli_query($conn,"update clubs set status = '".$_POST['dd_status']."' where id = '$idr'");
                $counter++;
              }
              echo '<div id="success">'.$counter.' records updated ... !</div>';
            }
    ?>
  <div class="clear"></div>
  <div id="pagination-top"></div>
  <form method="post" id="multiple">
    <input type="hidden" name="action" value="multiple">
    <table class="grid" cellspacing="0" cellpadding="0" width="100%">
      <tr>
        <th width="10%">Title</th>
        <th width="5%">Country 1</th>
        <th width="5%">Country 2</th>
        <th width="3%">Pool</th>
        <th width="8%">Match Date</th>
        <th width="7%">GMT Time</th>
        <th width="7%">IST Time</th>
        <th width="15%">Venue</th>
      </tr>

      <?  $rpp = PRODUCT_LIMIT_ADMIN; // results per page
            $ppage = intval($_GET["page"]);
      if($ppage<=0) $ppage = 1;
      $query = "select * from series_matches where series_id = ".$_GET['id'];

      //=======================================

      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
                echo '<tr><td colspan="5">Your search returned no results</td></tr>';
            }
      $rs   = mysqli_query($conn,$query);
      $tcount = mysqli_num_rows($rs);
      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
      $count = 0;
      $i = ($ppage-1)* $rpp;
          $x = 0;
      while(($count<$rpp) && ($i<$tcount)){
                mysqli_data_seek($rs,$i);
                $row = mysqli_fetch_array($rs);
      ?>
      <tr <?=($x%2 == 0)?' bgcolor="#f3f3f3"':''?>>
        <td><?php echo $row['title'];?></td>
        <td><?=get_combo('countries', 'name', $row['country_1'], '', 'text')?></td>
        <td><?=get_combo('countries', 'name', $row['country_2'], '', 'text')?></td>
        <td><?=$row['pool']?></td>
        <td><?=date('d-M-Y', strtotime($row['match_date']))?></td>
        <td><?=$row['gmt_time']?></td>
        <td><?=$row['ist_time']?></td>
        <td>
          <?php
          $venue = $row['match_ground'].', '.$row['match_city'].', ';
          $row_country= get_record_on_id('countries', $row['match_country_id']);
          echo $venue.$row_country['name'];
          ?>
        </td>
      </tr>
      <?
        $i++;
        $count++;
        $x++;
      }
    ?>

    </table>
<? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_ADMIN){ ?>
    <div id="pagination-bottom">
      <?php
        $keywords = array();
        foreach($_GET as $key=>$value){
          $keywords[] = $key.'='.$value;
        }
        $param = "id=".$_GET['id']."&";
        $reload = 'view-matches.php?'.$param;
        echo paginate_one($reload, $ppage, $tpages);
      ?>
      <input type="hidden" name="pagination-page" value="view-matches">
      <input type="hidden" name="pagination-param" value="<?=$param?>">
        </div>
        <? } ?>
    <div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>series.php" title="Go to Home">Back</a></div>

    <div class="clear"></div>
</div>
<!--*****************************-->
<? include('common/footer.php') ?>
