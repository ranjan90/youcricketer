<? include('common/header.php');?>	
<!--**********************************-->
<div class="box1">
	<h1>Add Blog Category</h1>
    	<?	//==========================
			if(isset($_POST['action']) && $_POST['action'] == 'submit'){
				
				$name		= $_POST['name'];
				$parent 	= $_POST['parent_id'];
				$description= $_POST['description'];
				$mkeyword 	= $_POST['meta_keywords'];
				$mdescription= $_POST['meta_description'];

				$query		= "insert into blog_categories (name, description, meta_keywords, meta_description, parent_id, status) 
				values ('$name','$description','$mkeyword','$mdescription','$parent','1')";
					
				if(mysqli_query($conn,$query)){
					echo '<div id="success"><b>Success : </b>Information Added ... !</div>';
					?>
						<script>
						window.location = '<?=ADMIN?>blog-categories.php';
						</script>
					<?
				}else{
					echo '<div id="error"><b>Failure : </b>Information already exists ... !</div>';
				}
			}
			//==========================
		?>
        <div style="clear:both; height:30px;"></div>
	<form id="frm-add" method="post" action="<?php echo ADMIN?>add-blog-category.php" >
    <input type="hidden" name="action" value="submit" />
	<table id="detail">
		
        <tr>
			<td width="25%">Blog Category : </td>
			<td><input type="text" name="name" class="validate[required] txt-feild-small" /></td>
		</tr>
		<tr>
			<td width="25%">Parent : </td>
			<td><?=get_combo('blog_categories','name','','parent_id')?></td>
		</tr>
		<tr>
			<td>Description</td>
			<td><textarea class="validate[required] txt-feild-large" style="height:200px;" name="description"></textarea></td>
		</tr>
		<tr>
			<td>Meta Keywords</td>
			<td><textarea class="validate[required] txt-feild-large" style="height:200px;" name="meta_keywords"></textarea></td>
		</tr>
		<tr>
			<td>Meta Description</td>
			<td><textarea class="validate[required] txt-feild-large" style="height:200px;" name="meta_description"></textarea></td>
		</tr>
        <tr>
        	<td colspan="2" align="center"><input type="submit" value=" Save" class="btn1" /></td>
        </tr>
	</table>
	</form>
	<div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>blog-categories.php" >Back</a></div>
    <div class="clear"></div>

<!--***********************************-->
<? include('common/footer.php')?>