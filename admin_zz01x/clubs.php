<? include('common/header.php');?>
<!--**********************************-->
<div class="box1">
<h1>Clubs </h1>
<table width="99%"  class="grid" border="0">
<tr>


<td width="751">
<div id="options">
     <div id="box" style="width:500px;">
        <form method="get" action="<?=ADMIN?>clubs.php">
            <input type="hidden" name="action" value="search" />
            <input type="text" name="searchtxt" class=" txt-feild-small" value="<?=$_GET['searchtxt']?>" placeholder="Type here" />
            <?=get_status_combo_selected($_GET['status']);?>
            <input type="submit" value="Go" />
        </form>
    </div>
    </td>
   <td width="326"></td>
    <div id="box" class="text" style="width:280px;">
      <td width="117"><a class="btn1" href="<?=ADMIN?>clubs.php">View All Records</a></td>
      <td width="116"><a class="btn2" href="<?=ADMIN?>add-club.php" >Add Record</a></td>
      <td><a  href="<?=ADMIN?>clubs.php?show_deleted=1" title="Show Deleted" class="btn1">Show Deleted</a></td>
      </div>

</tr>
</table>
  <?      if(isset($_GET['action']) && ($_GET['action'] == 'change' || $_GET['action'] == 'delete' || $_GET['action'] == 'feature')){

                $id         = $_GET['id'];
                $row        = get_record_on_id('clubs',$id,'','status');
                $status     = $row['status'];

                $status     = ($status == '1')?'0':'1';

                if($_GET['action'] == 'change'){
                    $query  = "update clubs set status = '$status' where id = '$id'";
                }else if($_GET['action'] == 'delete'){
                    $rc = mysqli_num_rows(mysqli_query($conn, "select id from clubs where id=".$id." and status=".DELETED_STATUS));
                    if($rc > 0){
                      $query = "DELETE FROM `clubs` WHERE id=".$id;
                    }else{
                      $query  = "update clubs set status = '".DELETED_STATUS."' where id = '$id'";
                    }
                }else if($_GET['action'] == 'feature'){
                  $feature  = $row['is_featured'];

                  $feature     = ($feature == '1')?'0':'1';
                  $query    = "update clubs set is_featured = '$feature' where id = '$id'";
                }
                if(mysqli_query($conn,$query)){
                    echo '<div id="success"> <b> Success: </b> Information Updated ... ! </div>';
                }else{
                    echo '<div id="error"> <b> Failure: </b> Information cannot be updated ... ! </div>';
                }
            }
            if(isset($_POST['action']) && $_POST['action'] == 'multiple'){
              $ids = array_keys($_POST['ids']);
              if($_POST['dd_status']==8){
                $counter = 0;
                foreach($ids as $idr){
                  $rc = mysqli_num_rows(mysqli_query($conn, "select id from clubs where id=".$idr." and status=".DELETED_STATUS));
                  if($rc > 0){
                    $query = "DELETE FROM `clubs` WHERE id=".$idr;
                  }else{
                    $query  = "update clubs set status = '".DELETED_STATUS."' where id = '$idr'";
                  }
                  mysqli_query($conn,$query);
                  $counter++;
                }
              }else{
                $counter = 0;
                foreach($ids as $idr){
                    $query  = "update clubs set status = '".$_POST['dd_status']."' where id = '$idr'";
                    mysqli_query($conn,$query);
                  $counter++;
                }
              }
              echo '<div id="success">'.$counter.' records updated ... !</div>';
            }
    ?>
  <div class="clear"></div>
  <div id="pagination-top"></div>
  <form method="post" id="multiple">
    <input type="hidden" name="action" value="multiple">
    <table class="grid" cellspacing="0" cellpadding="0" width="100%">
      <tr>
        <th width="10%">Action</th>
        <th width="20%">Title</th>
        <th width="31%">Description</th>
        <th width="10%">Email</th>
        <th width="12%">Location</th>
        <th width="15%">Year in Service</th>
      </tr>

      <?  $rpp = PRODUCT_LIMIT_ADMIN; // results per page
            $ppage = intval($_GET["page"]);
      if($ppage<=0) $ppage = 1;
      $query = "select * from clubs where 1=1 ";

      //=======================================
      if(isset($_GET['action']) && $_GET['action'] == 'search'){
        if(!empty($_GET['searchtxt'])){
          $query  .= "and title like '%".$_GET['searchtxt']."%'";
        }
        if(!empty($_GET['status'])){
          $query  .= " and status = '".$_GET['status']."'";
        }
      }
      if(isset($_GET['show_deleted'])){
        $query .= " and status = '".DELETED_STATUS."'";
      }else{
        $query .= " and status <> '".DELETED_STATUS."'";
      }
      //echo $query;
      //=======================================
      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
                echo '<tr><td colspan="5">Your search returned no results</td></tr>';
            }
      $rs   = mysqli_query($conn,$query);
      $tcount = mysqli_num_rows($rs);
      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
      $count = 0;
      $i = ($ppage-1)* $rpp;
          $x = 0;
      while(($count<$rpp) && ($i<$tcount)){
                mysqli_data_seek($rs,$i);
                $row = mysqli_fetch_array($rs);
      ?>
      <tr <?=($x%2 == 0)?' bgcolor="#f3f3f3"':''?>>
        <td>
          <input type="checkbox" name="ids[<?=$row['id']?>]">
          <a href="<?=ADMIN?>clubs.php?action=change&id=<?=$row['id']?>" title="Change Status">
                        <?
                          if($row['status'] == '1'){
                            $img = 'status.png';
                            $alt = 'Active Record';
                          }else if($row['status'] == '0'){
                            $img = 'in-status.png';
                            $alt = 'Inactive Record';
                          }else if($row['status'] == DELETED_STATUS){
                            $img = 'restore.png';
                            $alt = 'Deleted Record';
                          }
                        ?>
                        <img src="<?=WWW?>images/icons/<?=$img?>" alt="<?=$alt?>" width="16" />
                      </a>
          <a href="<?=ADMIN?>clubs.php?action=delete&id=<?=$row['id']?>" title="Delete Record">
                <img src="<?=WWW?>images/icons/delete.png" alt="Delete" />
            </a>
            <a href="<?=ADMIN?>clubs.php?action=feature&id=<?=$row['id']?>" title="Make Featured">
              <img src="<?=WWW?>images/icons/<?=($row['is_featured'] == '1')?'featured-on':'featured-off'?>.png" alt="Featured" />
            </a>
          <a href="<?=ADMIN?>view-club.php?id=<?=$row['id']?>" title="Record Details">
            <img src="<?=WWW?>images/icons/edit.png" alt="Record Details">
          </a>
        </td>
        <td><?=$row['title']?></td>
        <td><?=$row['description']?></td>
        <td><?=$row['email']?></td>
        <td>
          <?php
          $row_city   = get_record_on_id('cities', $row['city_id']);
          $row_states = get_record_on_id('states', $row_city['state_id']);
          $row_country= get_record_on_id('countries', $row_states['country_id']);
          echo $row_city['name'].' > '.$row_states['name'].' > '.$row_country['name'];
          ?>
        </td>
        <td><?=$row['year_in_service']?></td>
      </tr>
      <?
        $i++;
        $count++;
        $x++;
      }
    ?>
    <tr>
      <td>
        <input type="radio" name="checked" value="checked_all"> &nbsp; Checked All
      </td>
      <td>
        <input type="radio" name="checked" value="unchecked_all"> &nbsp; Unchecked All
      </td>
      <td>
        <select name="dd_status" class="dd1">
          <option value="" selected="selected">Select One</option>
          <option value="1">Activate Records</option>
          <option value="0">Deactivate Records</option>
          <option value="<?=DELETED_STATUS?>">Delete Records</option>
        </select>
      </td>
      <td>
        <input type="submit" value="Update">
      </td>
    </tr>
    </table>
<? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_ADMIN){ ?>
    <div id="pagination-bottom">
      <?php
        $keywords = array();
        foreach($_GET as $key=>$value){
          $keywords[] = $key.'='.$value;
        }
        $param = 'action=search&searchtxt='.$_GET['searchtxt'].'&status='.$_GET['status'].'&';
        $reload = 'clubs.php?'.$param;
        echo paginate_one($reload, $ppage, $tpages);
      ?>
      <input type="hidden" name="pagination-page" value="clubs">
      <input type="hidden" name="pagination-param" value="<?=$param?>">
        </div>
        <? } ?>
    <div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>home.php" title="Go to Home">Back</a></div>

    <div class="clear"></div>
</div>
<!--*****************************-->
<? include('common/footer.php') ?>
