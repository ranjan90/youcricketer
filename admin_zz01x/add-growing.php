<? include('common/header.php');?>	
<!--**********************************-->
<div class="box1">
	<h1>Add Growing Story</h1>
	<?	
		if($_POST['action'] == 'submit'){
			$picture = false;
			if(!empty($_FILES["banner"]["name"])){
				$file	= strtolower(str_replace(' ','-',($_FILES["banner"]["name"])));
				$file	= strtolower(str_replace('_','-',($file)));
				$title  = $_POST['title'];

				$image 	= new SimpleImage();
				$image->load($_FILES["banner"]["tmp_name"]);

				$image->save('../growing/'.$file);
				$picture	= true;
				$description= $_POST['description'];
				$tag	= $_POST['tag'];
				$sort_order	= $_POST['sort_order'];

				$rs_chk = mysqli_query($conn,"select * from growing where title = '$title'");
				if(mysqli_num_rows($rs_chk) == 0){
					$picture = true;
					mysqli_query($conn,"insert into growing (title, description, photo, tag, status, sort_order) 
						values ('$title','$description','$file','$tag','1', '$sort_order')");
					chmod('../growing/'.$file, 0777);
				}
			}
			
			if($picture === true){
				echo '<div id="success"><b>Success : </b>Information added ... !</div>';
				?>
				<script>
					window.location = '<?=ADMIN?>growing.php';
				</script>
				<?
			}else{
				echo '<div id="error"><b>Failure : </b>Information cannot be added ... !</div>';
			}
		}	
	?>
	<div style="clear:both; height:30px;"></div>
	<form id="frm-add" method="post" action="<?=ADMIN?>add-growing.php" enctype="multipart/form-data">
    <input type="hidden" name="action" value="submit" />
	<table width="41%"  id="detail">
		<tr>
			<td width="25%">Title : </td>
			<td><input type="text" name="title" class="validate[required] txt-feild-small" /></td>
		</tr>
        <tr>
			<td width="25%">Photo : </td>
			<td><input name="banner" class="validate[required] txt-feild-small" type="file"></td>
		</tr>
		<tr>
			<td width="25%">Description : </td>
			<td><textarea name="description" class="validate[required] txt-feild-small" cols="400" rows="5"></textarea></td>
		</tr>
		<tr>
			<td width="25%">Tag / Year: </td>
			<td><input name="tag" class="validate[required] txt-feild-small" type="text"></td>
		</tr>
		<tr>
			<td width="25%">Sort Order : </td>
			<td><input name="sort_order" class="validate[required] txt-feild-small" type="text"></td>
		</tr>
        <tr>
        	<td colspan="2" align="center"><input type="submit" value="Save"class="btn1" /></td>
        </tr>
	</table>

</form>

	<div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>team.php">Back</a></div>

<!--***********************************-->
<? include('common/footer.php')?>