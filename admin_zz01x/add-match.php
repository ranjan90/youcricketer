<? include('common/header.php');?>	
<!--**********************************-->
<div class="box1">
	<h1>Add Match (<?=get_combo('series', 'title', $_GET['id'],'','text');?>)</h1>
    	<?	//==========================
			if(isset($_POST['action']) && $_POST['action'] == 'submit'){
				
				$country_1  = $_POST['country_1'];
				$country_2  = $_POST['country_2'];
				$pool 	    = $_POST['pool'];
				$gmt_time   = $_POST['gmt_time'];
				$ist_time   = $_POST['ist_time'];
				$match_ground= $_POST['match_ground'];
				$match_city = $_POST['match_city'];
				$match_country_id= $_POST['match_country_id'];
				$match_date = date('Y-m-d', strtotime($_POST['match_date']));
				$date 		= date('Y-m-d');
				$user_id 	= $_SESSION['ycdc_admin_dbuid'];
				$series_id 	= $_GET['id'];
				$query		= "insert into series_matches (country_1, country_2, pool, match_date, gmt_time, ist_time, match_ground, match_city, match_country_id, series_id, created_by, create_date, status)
				values ('$country_1','$country_2','$pool','$match_date','$gmt_time','$ist_time','$match_ground','$match_city','$match_country_id','$series_id','$user_id','$date','1')";
				// echo $query;exit;
				if(mysqli_query($conn,$query)){
					echo '<div id="success"><b>Success : </b>Information Added ... !</div>';
					?>
						<script>
						window.location = '<?=ADMIN?>view-matches.php?id=<?php echo $series_id;?>';
						</script>
					<?
				}else{
					echo '<div id="error"><b>Failure : </b>Information already exists ... !</div>';
				}
			}
			//==========================
		?>
        <div style="clear:both; height:30px;"></div>
	<form id="frm-add" method="post" action="<?php echo ADMIN?>add-match.php?id=<?php echo $_GET['id']?>" >
    <input type="hidden" name="action" value="submit" />
	<table id="detail">
		<tr>
			<td width="25%">Country 1 : </td>
			<td><?php echo get_combo('countries', 'name', '', 'country_1');?></td>
		</tr>
		<tr>
			<td width="25%">Country 2 : </td>
			<td><?php echo get_combo('countries', 'name', '', 'country_2');?></td>
		</tr>
		<tr>
			<td width="25%">Pool : </td>
			<td>
				<select name="pool" class="validate[required] txt-feild-small">
					<option value="A">A</option>
					<option value="B">B</option>
					<option value="C">C</option>					
				</select>
			</td>
		</tr>
		<tr>
			<td width="25%">GMT Time : </td>
			<td><input type="text" name="gmt_time" class="validate[required] txt-feild-small"></td>
		</tr>
		<tr>
			<td width="25%">IST Time : </td>
			<td><input type="text" name="ist_time" class="validate[required] txt-feild-small"></td>
		</tr>
		<tr>
			<td width="25%">Match Ground : </td>
			<td><input type="text" name="match_ground" class="validate[required] txt-feild-small"></td>
		</tr>
		<tr>
			<td width="25%">Match City : </td>
			<td><input type="text" name="match_city" class="validate[required] txt-feild-small"></td>
		</tr>
		<tr>
			<td width="25%">Match Country : </td>
			<td><?php echo get_combo('countries', 'name', '', 'match_country_id');?></td>
		</tr>
		<tr>
			<td width="25%">Match Date : </td>
			<td><input type="text" name="match_date" class="txt-feild-small validate[required] calender"></td>
		</tr>
		<tr>
        	<td colspan="2" align="center"><input type="submit" value=" Save" class="btn1" /></td>
        </tr>
	</table>
	</form>
	<div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>series.php" >Back</a></div>
    <div class="clear"></div>
<script type="text/javascript" src="<?=WWW?>js/jquery-ui.min.js"></script>
<link href="<?=WWW?>css/jquery-ui.css" type="text/css" rel="stylesheet">
<!--***********************************-->
<? include('common/footer.php')?>