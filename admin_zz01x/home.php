<?php include('common/header.php'); ?>
<!--**********************************-->
<?
    if(isset($_GET['error']) && $_GET['error'] == 'rights'){
    ?><div id="error">Requested page has no permission for you ... !</div><?
    }
?>
<div class="box">
	<table border="1" style=" width:300px; border-radius:5px; border-collapse:collapse;">
		<tr>
			<th colspan="2">We have in database</th>
		</tr>
		<tr><td>Total Individuals : </td><td><?php echo mysqli_num_rows(mysqli_query($conn,"select * from users where is_company = '0'"))?></td></tr>
		<tr><td>Total Organizations : </td><td><?php echo mysqli_num_rows(mysqli_query($conn,"select * from users where is_company = '1'"))?></td></tr>
		<tr><td>Total Blog Articles : </td><td><?php echo mysqli_num_rows(mysqli_query($conn,"select * from blog_articles"))?></td></tr>
		<tr><td>Total Forum Topics : </td><td><?php echo mysqli_num_rows(mysqli_query($conn,"select * from forum_topics"))?></td></tr>
		<tr><td>Total Press Releases : </td><td><?php echo mysqli_num_rows(mysqli_query($conn,"select * from press_releases"))?></td></tr>
		<tr><td>Total Clubs : </td><td><?php echo mysqli_num_rows(mysqli_query($conn,"select * from clubs"))?></td></tr>
		<tr><td>Total Leagues : </td><td><?php echo mysqli_num_rows(mysqli_query($conn,"select * from leagues"))?></td></tr>
		<tr><td>Total FAQs : </td><td><?php echo mysqli_num_rows(mysqli_query($conn,"select * from faqs"))?></td></tr>
		<tr><td>Total Banners : </td><td><?php echo mysqli_num_rows(mysqli_query($conn,"select * from banners"))?></td></tr>
		<tr><td>Total News : </td><td><?php echo mysqli_num_rows(mysqli_query($conn,"select * from news"))?></td></tr>
		<tr><td>Total Events : </td><td><?php echo mysqli_num_rows(mysqli_query($conn,"select * from events"))?></td></tr>
		<tr><td>Total Groups : </td><td><?php echo mysqli_num_rows(mysqli_query($conn,"select * from groups"))?></td></tr>
		<tr><td>Total Testimonials : </td><td><?php echo mysqli_num_rows(mysqli_query($conn,"select * from testimonials"))?></td></tr>
		<tr><td>Total Contact Requests : </td><td><?php echo mysqli_num_rows(mysqli_query($conn,"select * from contact_requests"))?></td></tr>
	</table>
</div>
<style>
	td, th{padding:7px;}
</style>
<!--**********************************-->
<?php include('common/footer.php');?>