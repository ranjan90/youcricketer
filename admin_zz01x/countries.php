<? include('common/header.php');?>
<!--**********************************-->
<div class="box1">
<h1>Countries </h1>
<table width="99%"  class="grid" border="0">
<tr>

<td width="751">
<div id="options">
     <div id="box" style="width:500px;">
        <form method="get" action="<?=ADMIN?>countries.php">
            <input type="hidden" name="action" value="search" />
            <input type="text" name="searchtxt" class=" txt-feild-small"  placeholder="Type here" value="<?=$_GET['searchtxt'];?>" />
            <?=get_status_combo_selected($_GET['status']);?>
            <input type="submit" value="Go" />
        </form>
    </div>
    </td>
   <td width="326"></td>
      <td width="117"><a class="btn1" href="<?=ADMIN?>countries.php">View All Records</a></td>
      <td width="116"><a class="btn2" href="<?=ADMIN?>add-country.php" >Add Record</a></td>
      <td><a  href="<?=ADMIN?>countries.php?show_deleted=1" title="Show Deleted" class="btn1">Show Deleted</a></td>

</tr>
</table>
<div class="clear"></div>
        <?    if(isset($_GET['action']) && ($_GET['action'] == 'change' || $_GET['action'] == 'delete')){
        $id     = $_GET['id'];
        $row    = get_record_on_id('countries',$id,'', 'status');
        $status   = $row['status'];
        $status   = ($status == '1')?'0':'1';

                if($_GET['action'] == 'change'){
                    $query  = "update countries set status = '$status' where id = '$id'";
                }else if($_GET['action'] == 'delete'){
                    $rc = mysqli_num_rows(mysqli_query($conn, "select id from countries where id=".$id." and status=".DELETED_STATUS));
                    if($rc > 0){
                      $query = "DELETE FROM `countries` WHERE id=".$id;
                    }else{
                      $query  = "update countries set status = '".DELETED_STATUS."' where id = '$id'";
                    }
                }
                if(mysqli_query($conn,$query)){
                    echo '<div id="success"> <b> Success: </b> Information Updated ... ! </div>';
                }else{
                    echo '<div id="error"> <b> Failure: </b> Information cannot be updated ... ! </div>';
                }
            }

    ?>
              <div class="clear"></div>
    <div id="pagination-top"></div>
    <table width="1155" class="grid">
      <tr> </tr>
      <tr>
        <th width="10%">Action</th>
        <th width="40%">Country Name</th>
        <th width="10%">Country Code</th>
        <th width="10%">Flag</th>
        <th width="10%">Phone Code</th>
      </tr>
      <?	$rpp = PRODUCT_LIMIT_ADMIN; // results per page
      $ppage = intval($_GET["page"]);
			if($ppage<=0){
        $ppage = 1;
      }
			$date  = date('Y-m-d h:m:s');
			$query = "select * from countries where 1=1 ";
			//=======================================
			if(isset($_GET['action']) && $_GET['action'] == 'search'){
        if(!empty($_GET['searchtxt'])){
          $query  .= "and name like '%".$_GET['searchtxt']."%'";
        }
        if($_GET['status'] != ''){
          $query  .= " and status = '".$_GET['status']."'";
        }
      }
      if(isset($_GET['show_deleted'])){
                $query .= " and status = '".DELETED_STATUS."'";
              }else{
                $query .= " and status <> '".DELETED_STATUS."'";
              }
			//=======================================
			if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
                echo '<tr><td colspan="5">There is no record in this selection ... !</td></tr>';
            }
			$rs = mysqli_query($conn,$query);
			$tcount = mysqli_num_rows($rs);

			$tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
			$count = 0;
			$i = ($ppage-1)* $rpp;
			$x = 0;
			while(($count<$rpp) && ($i<$tcount)){
                mysqli_data_seek($rs,$i);
                $row = mysqli_fetch_array($rs);
			?>
      <tr <?=($x%2 == 0)?' bgcolor="#f3f3f3"':''?>>
        <td>
          <a href="<?=ADMIN?>view-country.php?id=<?=$row['id']?>"><img src="<?=WWW?>images/icons/edit.png"></a>
          <a href="<?=ADMIN?>countries.php?action=change&id=<?=$row['id']?>" title="Restore">
          <?
            if($row['status'] == '1'){
              $img = 'status.png';
              $alt = 'Active Record';
            }else if($row['status'] == '0'){
              $img = 'in-status.png';
              $alt = 'Inactive Record';
            }else if($row['status'] == DELETED_STATUS){
              $img = 'restore.png';
              $alt = 'Deleted Record';
            }
          ?>
          <img src="<?=WWW?>images/icons/<?=$img?>" alt="<?=$alt?>" width="16" />
          </a>
          <a href="<?=ADMIN?>countries.php?action=delete&id=<?=$row['id']?>" title="Delete Record">
            <img src="<?=WWW?>images/icons/delete.png" alt="Delete" />
          </a>
          <a href="<?=ADMIN?>states.php?id=<?=$row['id']?>" title="View States">
            <img src="<?=WWW?>images/icons/states.png">
          </a>
          <a href="<?=ADMIN?>cities.php?id=<?=$row['id']?>" title="View Cities">
            <img src="<?=WWW?>images/icons/cities.png">
          </a>
        </td>
        <td><?=$row['name']?></td>
        <td><?=$row['3_code']?></td>
        <td><img src="<?=WWW?>countries/<?=$row['flag']?>" width="20"></td>
        <td><?=$row['phone_code']?></td>
      </tr>
      <?
				$x++;
				$i++;
				$count++;
			}
		?>
     </table>
<? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_ADMIN){ ?>
    <div id="pagination-bottom">
      <?php
        $keywords = array();
        foreach($_GET as $key=>$value){
          $keywords[] = $key.'='.$value;
        }
        $reload = 'countries.php?action=search&searchtxt='.$_GET['searchtxt'].'&status='.$_GET['status'].'&';
        echo paginate_one($reload, $ppage, $tpages);
      ?>
      <input type="hidden" name="pagination-page" value="countries">
        </div>
        <? } ?>
    <div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>home.php">Back</a></div>
    <div class="clear"></div>
</div>
<div class="clear"></div>
<!--**********************************-->

<!--**********************************-->
<? include('common/footer.php');?>
