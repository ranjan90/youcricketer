<? include('common/header.php');?>	
<!--**********************************-->
<div class="box1">
	<h1>Add Sliding Banner</h1>
	<?	
		if($_POST['action'] == 'submit'){
			if(!empty($_FILES["banner"]["name"])){

				$file	= strtolower(str_replace(' ','-',($_FILES["banner"]["name"])));
				$file	= strtolower(str_replace('_','-',($file)));
				$title  = $_POST['title'];

				$image 	= new SimpleImage();
				$image->load($_FILES["banner"]["tmp_name"]);
				if($image->getWidth() >= 650){
					$image->resizeToWidth(650);
				}else{
					echo '<div id="error"><b>Failure : </b>Banner size is small... !</div>';
				}
				
				if($image->save('../banners/'.$file) == true){
					$picture	= true;
					$web_url 	= $_POST['web_url'];
					$sort_order	= $_POST['sort_order'];

					$rs_chk = mysqli_query($conn,"select * from sliding_banners where content = '$file' and web_url = '$web_url'");
					if(mysqli_num_rows($rs_chk) == 0){
						mysqli_query($conn,"insert into sliding_banners (content, title, web_url, sort_order, status) 
							values ('$file','$title','$web_url','$sort_order','1')");
						chmod('../banners/'.$file, 0777);
					}
				}else{
					$picture	= false;
				}
			}
			
			if($picture){
				echo '<div id="success"><b>Success : </b>Information added ... !</div>';
				?>
				<script>
					window.location = '<?=ADMIN?>sliding-banners.php';
				</script>
				<?
			}else{
				echo '<div id="error"><b>Failure : </b>Information cannot be added ... !</div>';
			}
		}	
	?>
	<div style="clear:both; height:30px;"></div>
	<form id="frm-add" method="post" action="<?=ADMIN?>add-sliding-banner.php" enctype="multipart/form-data">
    <input type="hidden" name="action" value="submit" />
	<table width="41%"  id="detail">
		<tr>
			<td width="25%">Title : </td>
			<td><input type="text" name="title" class="validate[required] txt-feild-small" /></td>
		</tr>
        <tr>
			<td width="25%">Banner File : </td>
			<td><input name="banner" class="validate[required] txt-feild-small" type="file"></td>
		</tr>
		<tr>
			<td width="25%">Website URL : </td>
			<td><input name="web_url" class="validate[required, custom[url]] txt-feild-small" type="text"></td>
		</tr>
		<tr>
			<td width="25%">Sort Order : </td>
			<td><input name="sort_order" class="validate[required, custom[integer]] txt-feild-small" type="text"></td>
		</tr>
        <tr>
        	<td colspan="2" align="center"><input type="submit" value="Save"class="btn1" /></td>
        </tr>
	</table>

</form>

	<div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>sliding-banners.php">Back</a></div>

<!--***********************************-->
<? include('common/footer.php')?>