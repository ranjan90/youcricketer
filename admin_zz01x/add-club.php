<? include('common/header.php');?>	
<!--**********************************-->
<div class="box1">
	<h1>Add Club</h1>
    	<?	//==========================
			if(isset($_POST['action']) && $_POST['action'] == 'submit'){
				
				$name		= $_POST['name'];
				$description= $_POST['description'];
				$email 		= $_POST['email'];
				$year_in_service= $_POST['year_in_service'];
				$city_id 	= (isset($_POST['city_id']) && !empty($_POST['city_id']))?$_POST['city_id']:'';

				if(empty($city_id)){
					echo '<div id="error">Please select Location ... !</div>';
				}else{
					$query		= "insert into clubs (title, description, email,year_in_service, status, city_id) 
					values ('$name','$description','$email','$year_in_service','1','$city_id')";
					if(mysqli_query($conn,$query)){
						echo '<div id="success"><b>Success : </b>Information Added ... !</div>';
						?>
							<script>
							window.location = '<?=ADMIN?>clubs.php';
							</script>
						<?
					}else{
						echo '<div id="error"><b>Failure : </b>Information already exists ... !</div>';
					}
				}
			}
			//==========================
		?>
        <div style="clear:both; height:30px;"></div>
	<form id="frm-add" method="post" action="<?php echo ADMIN?>add-club.php" >
    <input type="hidden" name="action" value="submit" />
	<table id="detail">
		
        <tr>
			<td width="25%">Title : </td>
			<td><input type="text" name="name" class="validate[required] txt-feild-small" /></td>
		</tr>
		<tr>
			<td>Description</td>
			<td><textarea class="validate[required] txt-feild-large" style="height:200px;" name="description"></textarea></td>
		</tr>
		<tr>
			<td>Club Email</td>
			<td><input type="text" name="email" class="validate[required] txt-feild-small" /></td>
		</tr>
		<tr>
			<td>Year In Service</td>
			<td><input type="text" name="year_in_service" class="validate[required] txt-feild-small" /></td>
		</tr>
		<tr>
			<td>Location</td>
			<td>
				<?=get_combo('countries','name','','country_id')?>
				<div id="state"></div>
				<div id="city"></div>
			</td>
		</tr>
        <tr>
        	<td colspan="2" align="center"><input type="submit" value=" Save" class="btn1" /></td>
        </tr>
	</table>
	</form>
	<div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>clubs.php" >Back</a></div>
    <div class="clear"></div>
<script src="<?php echo WWW?>js/jquery-1.8.0.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('select[name=country_id]').change(function(){
		var id = $('select[name=country_id]').val();
		$.ajax({type	: 'POST', 
				url		: WWW + 'includes/get-states.php', 
				data	: ({id:id}),
				success	: function(msg){
					$("#state").html(msg);
				}
		});
	});
	$('select[name=state_id]').live('change',function(){
		var id = $('select[name=state_id]').val();
		$.ajax({type	: 'POST', 
				url		: WWW + 'includes/get-cities.php', 
				data	: ({id:id}),
				success	: function(msg){
					$("#city").html(msg);
				}
		});
	});
});
</script>
<!--***********************************-->
<? include('common/footer.php')?>