<? include('common/header.php');?>
    <!--**********************************-->
<div class="box1">
    <h1>Manage Team</h1>
    <table width="99%"  class="grid" border="0">
<tr>
            	

<td width="751">
<div id="options">
     <div id="box" style="width:500px;">
        <form method="get" action="<?=ADMIN?>team.php">
            <input type="hidden" name="action" value="search" />
            <input type="text" name="searchtxt" class=" txt-feild-small"  placeholder="Type here" />
            <?=get_status_combo()?>
            <input type="submit" value="Go" />
        </form>
    </div>
    </td>
   <td width="326"></td>
    <div id="box" class="text" style="width:280px;">
      <td width="117"><a class="btn1" href="<?=ADMIN?>team.php">View All Records</a></td>
      <td width="116"><a class="btn2" href="<?=ADMIN?>add-team.php" >Add Record</a></td>
      <td><a  href="<?=ADMIN?>team.php?show_deleted=1" title="Show Deleted" class="btn1">Show Deleted</a></td>
      </div>
                   
</tr>
</table>
    <div class="clear"></div>
    <?		$id			= $_GET['id'];
			$row 		= get_record_on_id('team',$id,'status');
            $status		= $row['status'];

            $status		= ($status == '1')?'0':'1';

            if(isset($_GET['action']) && ($_GET['action'] == 'change' || $_GET['action'] == 'delete')){
            	if($_GET['action'] == 'change'){
                	$query	= "update team set status = '$status' where id = '$id'";
                }else if($_GET['action'] == 'delete'){
                	$query  = "update team set status = '".DELETED_STATUS."' where id = '$id'";
	            }                             
								
                if(mysqli_query($conn,$query)){
                	echo '<div id="success">Information Updated ... !</div>';
                }else{
                	echo '<div id="error">Information cannot be updated ... !</div>';
                }
            }
        ?>

    <div class="clear"></div>
    <div id="pagination-top"></div>
    <table class="detail" cellspacing="0" cellpadding="0">
      <tr>
      	<th width="10%">Action</th>
        <th width="10%">Name</th>
        <th width="20%">Designation</th>
        <th width="20%">Photo</th>
        <th width="10%">Sort Order</th>
      </tr>      
      <? $rpp = PRODUCT_LIMIT_ADMIN; // results per page
      $ppage = intval($_GET["page"]);
      if($ppage<=0){
        $ppage = 1;
      }
      $date  = date('Y-m-d h:m:s');
      $query = "select * from team where 1=1 ";
      //=======================================
      if(isset($_GET['action']) && $_GET['action'] == 'search'){
        if(!empty($_GET['searchtxt'])){
          $query  .= "and name like '%".$_GET['searchtxt']."%'";
        }
        if($_GET['status'] != ''){
          $query  .= "and status = '".$_GET['status']."'";
        }
      }
      if(isset($_GET['show_deleted'])){
                $query .= " and status = '".DELETED_STATUS."'";
              }else{
                $query .= " and status <> '".DELETED_STATUS."'";
              }
      //=======================================
      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
                echo '<tr><td colspan="5">There is no record in this selection ... !</td></tr>';
            }
      $rs = mysqli_query($conn,$query);
      $tcount = mysqli_num_rows($rs);
      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
      $count = 0;
      $i = ($ppage-1)* $rpp;
      $x = 0;
      while(($count<$rpp) && ($i<$tcount)){
                mysqli_data_seek($rs,$i);
                $row = mysqli_fetch_array($rs);
      ?>
      <tr <?=($x%2 == 0)?' bgcolor="#f3f3f3"':''?>>
        <td>
          <a href="<?=ADMIN?>view-team.php?id=<?=$row['id']?>"><img src="<?=WWW?>images/icons/edit.png"></a>
          <a href="<?=ADMIN?>team.php?action=change&id=<?=$row['id']?>" title="Change Status">
          <?
            if($row['status'] == '1'){
              $img = 'status.png';
              $alt = 'Active Record';
            }else if($row['status'] == '0'){
              $img = 'in-status.png';
              $alt = 'Inactive Record';
            }else if($row['status'] == DELETED_STATUS){
              $img = 'restore.png';
              $alt = 'Deleted Record';
            }
          ?>
          <img src="<?=WWW?>images/icons/<?=$img?>" alt="<?=$alt?>" width="16" />
          </a>
          <a href="<?=ADMIN?>team.php?action=delete&id=<?=$row['id']?>" title="Delete Record">
            <img src="<?=WWW?>images/icons/delete.png" alt="Delete" />
          </a>
        </td>
        <td><?=$row['name']?></td>
        <td><?=$row['designation']?></td>
        <td><img src="<?=WWW?>team/<?=$row['photo']?>" width="150"></td>
        <td><?=$row['sort_order']?></td>
      </tr>
      <?
        $x++;
        $i++;
        $count++;
      } 
    ?>
     </table>
<? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_ADMIN){ ?>
    <div id="pagination-bottom">
      <?php
        $keywords = array();
        foreach($_GET as $key=>$value){
          $keywords[] = $key.'='.$value;
        }
        $reload = 'team.php?'.implode('&',$keywords).'&';
        echo paginate_one($reload, $ppage, $tpages);
      ?>
      <input type="hidden" name="pagination-page" value="sliding-banners">
        </div>  
        <? } ?> 
    <div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>home.php" title="Go to Home">Back</a></div>
    <div class="clear"></div>
  </div>
  <!--**********************************-->
  <? include('common/footer.php');?>
