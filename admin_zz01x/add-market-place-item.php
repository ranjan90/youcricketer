<? include('common/header.php');

?>
<script type="text/javascript" src="<?php echo WWW;?>js/jquery-ui.min.js"></script>
<link type="text/css" rel="stylesheet" href="<?php echo WWW;?>css/jquery-ui.css">

<!--**********************************-->
<div class="box1">
	<h1>Add New Ad</h1>
    	<?php	//==========================
			if(isset($_POST['action']) && $_POST['action'] == 'submit'){

				$title		= $_POST['item_title'];
				$admin_note	= $_POST['admin_note'];
				$item_detail= $_POST['item_detail'];
				$item_price = $_POST['item_price'];
				$item_type = $_POST['item_type'];

				if($item_type=='1'){
					$item_country = '';
					$item_state = '';
					$item_city = '';
				}else{
					$item_country = $_POST['item_country'];
					$item_state = $_POST['item_state'];
					$item_city = $_POST['item_city'];
				}
				$status = $_POST['status'];

					//$rs_chk = mysqli_query($conn,"select * from marke_place_items where item_title = '$title' ");
					//if(mysqli_num_rows($rs_chk) == 0){

                        if(isset($_GET['id']) && !empty($_GET['id']))
                        {
                            $item_id = (int) $_GET['id'];
                            $query = "UPDATE marke_place_items  SET  item_title = '$title', item_price = '$item_price', item_type = '$item_type', item_country = '$item_country', item_state = '$item_state', item_city = '$item_city', item_detail = '$item_detail', admin_note = '$admin_note', status = '$status' WHERE item_id = '$item_id' ";
                        }else {
                           $query = "insert into
												marke_place_items
												(item_title,item_price,item_type,item_country,item_state,item_city,item_detail,admin_note,status)
												values
												('$item_title','$item_price','$item_type','$item_country','$item_state','$item_city','$item_detail','$admin_note','$status')";

                        }

						if(mysqli_query($conn,$query)){
							$id	= mysqli_insert_id($conn);

							if(isset($_GET['id']) && !empty($_GET['id']))
								echo '<div id="success"><b>Success : </b>Information Successfully Updated ... !</div>';
							else
								echo '<div id="success"><b>Success : </b>Information Successfully Added ... !</div>';

						}else{
							echo '<div id="error"><b>Failure : </b>Invalid information ... !</div>';
						}
					/*}else{
						echo '<div id="error"><b>Failure : </b>Information Already Exists ... !</div>';
					}*/

			}
			//==========================

		?>
        <div style="clear:both; height:30px;"></div>
	<form id="frm-add" method="post" action="add-market-place-item.php<?php if(isset($_GET['id']) && !empty($_GET['id'])){echo "?id=".$_GET['id'];}?>" enctype="multipart/form-data" >

        <?php

        if(isset($_GET['id']) && !empty($_GET['id']))
        {
            $id = (int) $_GET['id'];
            $query = 'select * from marke_place_items where item_id = '.$id;
            $rs = mysqli_query($conn,$query);
            $row = mysqli_fetch_assoc($rs);

        }
        ?>


    <input type="hidden" name="action" value="submit" />
	<table id="detail">

        <tr>
			<td width="25%">Title *: </td>
			<td><input type="text" name="item_title" class="validate[required] txt-feild-small" value="<?php if(isset($row['item_title'])){echo $row['item_title'];} ?>" placeholder="Please Enter unique Item Title" /></td>
		</tr>

        <tr>
            <td width="25%">Item Price *: </td>
            <td><input type="text" name="item_price" class="validate[required] txt-feild-small" value="<?php if(isset($row['item_title'])){echo $row['item_price'];} ?>" placeholder="Please Enter Item Price"	 /></td>
        </tr>

				<tr>
            <td width="25%">Item Type *: </td>
            <td>
							<select name="item_type" class="validate[required] item_type">
								<option value="">Choose Item Type</option>
								<option value="1" <?php if(isset($row['item_type']) && $row['item_type']=='1') echo "selected='selected'";?>>Global</option>
								<option value="2" <?php if(isset($row['item_type']) && $row['item_type']=='2') echo "selected='selected'";?>>Regional</option>
							</select>
						</td>
        </tr>

				<tr class='normal' style="<?php if(!isset($row['item_type']) || $row['item_type']!='2') echo 'display:none';?>">
            <td width="25%">Item Country * : </td>
            <td>
							<?php
							$query=mysqli_query($conn,"select * from countries");
							?>
							<select name="item_country" class="validate[required] countries">
								<option value="">Choose Item Country</option>
								<?php while($record=mysqli_fetch_assoc($query)){?>
									<option value="<?php echo $record['name'];?>" data-id="<?php echo $record['id'];?>" <?php if($row['item_country']==$record['name']) echo "selected='selected'";?>><?php echo $record['name'];?></option>
								<?php } ?>
							</select>
						</td>
        </tr>

				<tr class='normal' style="<?php if(!isset($row['item_type']) || $row['item_type']!='2') echo 'display:none';?>">
            <td width="25%">Item State *: </td>
            <td>
							<?php
							$query=mysqli_query($conn,"select * from states");
							?>
							<select name="item_state" class="validate[required] states">
								<option value="">Choose Item State</option>
								<?php while($record=mysqli_fetch_assoc($query)){?>
									<option value="<?php echo $record['name'];?>" data-id="<?php echo $record['id'];?>" <?php if($row['item_state']==$record['name']) echo "selected='selected'";?>><?php echo $record['name'];?></option>
								<?php } ?>
							</select>
						</td>
        </tr>

				<tr class='normal' style="<?php if(!isset($row['item_type']) || $row['item_type']!='2') echo 'display:none';?>">
            <td width="25%">Item City *: </td>
            <td>
							<input type="text" name="item_city" class="validate[required] txt-feild-small" value="<?php if(isset($row['item_city'])){echo $row['item_city'];} ?>" placeholder="Please Enter Item City" />
						</td>
        </tr>

        <tr>
					<td width="25%">Item Detail *: </td>
					<td><textarea cols="" rows="" name="item_detail" class="txt-feild-large validate[required]" style="height:150px;"><?php if(isset($row['item_title'])){echo $row['item_detail'];} ?></textarea></td>
				</tr>

        <tr>
			<td width="25%">Admin Note : </td>
			<td>
            	<textarea cols="100" rows="10" class="txt-feild-large" style="height:150px;" name="admin_note"><?php if(isset($row['item_title'])){echo $row['admin_note'];} ?></textarea>
            </td>
		</tr>

		<tr>
				<td width="25%"> Status: </td>
				<td><input type="radio" name="status" value="1" <?php if(isset($row['status']) && $row['status']=='1') echo "checked='checked'"; ?>> Active &nbsp;&nbsp;<input type="radio" name="status" value="0" <?php if((isset($row['status']) && $row['status']=='0') || !isset($row['status'])) echo "checked='checked'"; ?> > Inactive </td>
		</tr>

        <tr>
        	<td colspan="2" align="center"><input type="submit" value=" Save " class="btn1" /></td>
        </tr>
	</table>

	</form>

	<div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>market-place-item.php" title="Go to banner">Back</a></div>
	<div class="clear"></div>

    <script>
		$(document).ready(function(){

			$(".item_type").change(function(){

					var val=$(this).val();

					if(val=='2'){
						$(".normal").show();
					}else{
						$(".normal").hide();
					}

			});

			$(".countries").change(function(){

				var country=$(".countries > option:selected").attr('data-id');

				$.ajax({
					url: "ajax/states.php",
					type: "post",
					data: {country:country},
					success: function(result){
							$(".states").html(result);
						}
				});

			});

		});
</script>
<!--***********************************-->
<? include('common/footer.php')?>
