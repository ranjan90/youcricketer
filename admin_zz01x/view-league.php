<? include('common/header.php');?>
<!--**********************************-->
<div class="box1">
	<h1>View League</h1>
    	<?	//==========================
			if(isset($_POST['action']) && $_POST['action'] == 'submit'){

				$row_city_old = get_record_on_id('leagues', $_GET['id']);

				$name		= $_POST['name'];
				$description= $_POST['description'];
				$email 		= $_POST['email'];
				$year_in_service= $_POST['year_in_service'];
				$city_id 	= (isset($_POST['city_id']) && !empty($_POST['city_id']))?$_POST['city_id']:$row_city_old['city_id'];
				$id 		= $_GET['id'];

				if(empty($city_id)){
					echo '<div id="error">Please select Location ... !</div>';
				}else{
					$query		= "update leagues set title = '$name', description = '$description', email = '$email', year_in_service = '$year_in_service', city_id = '$city_id' where id = '$id'";

					if(mysqli_query($conn,$query)){
						echo '<div id="success"><b>Success : </b>Information Added ... !</div>';
						?>
							<script>
							window.location = '<?=ADMIN?>leagues.php';
							</script>
						<?
					}else{
						echo '<div id="error"><b>Failure : </b>Information already exists ... !</div>';
					}
				}
			}
			//==========================
			$id = $_GET['id'];
			$row = get_record_on_id('leagues', $id);
			$title = str_replace('"','',$row['title']);
		?>
        <div style="clear:both; height:30px;"></div>
	<form id="frm-add" method="post" action="<?php echo ADMIN?>view-league.php?id=<?=$id?>" >
    <input type="hidden" name="action" value="submit" />
	<table id="detail">

        <tr>
			<td width="25%">Title : </td>
			<td><input type="text" name="name" value="<?=$title?>" class="validate[required] txt-feild-small" /></td>
		</tr>
		<tr>
			<td>Description</td>
			<td><textarea class="validate[required] txt-feild-large" style="height:200px;" name="description"><?=$row['description']?></textarea></td>
		</tr>
		<tr>
			<td>Club Email</td>
			<td><input type="text" name="email" class="validate[required] txt-feild-small" value="<?=$row['email']?>" /></td>
		</tr>
		<tr>
			<td>Year In Service</td>
			<td><input type="text" name="year_in_service" class="validate[required] txt-feild-small" value="<?=$row['year_in_service']?>" /></td>
		</tr>
		<tr>
			<td>Location</td>
			<td><?php
		          $row_city   = get_record_on_id('cities', $row['city_id']);
		          $row_states = get_record_on_id('states', $row_city['state_id']);
		          $row_country= get_record_on_id('countries', $row_states['country_id']);
		          echo $row_city['name'].' > '.$row_states['name'].' > '.$row_country['name'];
		          ?>
				<?=get_combo('countries','name','','country_id')?>
				<div id="state"></div>
				<div id="city"></div>
			</td>
		</tr>
        <tr>
        	<td colspan="2" align="center"><input type="submit" value=" Save" class="btn1" /></td>
        </tr>
	</table>
	</form>
	<div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>leagues.php" >Back</a></div>
    <div class="clear"></div>
<script src="<?php echo WWW?>js/jquery-1.8.0.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('select[name=country_id]').change(function(){
		var id = $('select[name=country_id]').val();
		$.ajax({type	: 'POST',
				url		: WWW + 'includes/get-states.php',
				data	: ({id:id}),
				success	: function(msg){
					$("#state").html(msg);
				}
		});
	});
	$('select[name=state_id]').live('change',function(){
		var id = $('select[name=state_id]').val();
		$.ajax({type	: 'POST',
				url		: WWW + 'includes/get-cities.php',
				data	: ({id:id}),
				success	: function(msg){
					$("#city").html(msg);
				}
		});
	});
});
</script>
<!--***********************************-->
<? include('common/footer.php')?>
