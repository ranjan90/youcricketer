<?php include('common/header.php');?>	
<!--**********************************-->

<div class="box1">
	<h1>Create Newsletter</h1>
	<?
				
		if($_POST['action'] == 'submit'){
			
			extract($_POST);
			
			$content = str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $content);

			$query = "insert into newsletters (title,content, date_added, status)
				values('$title','".addslashes($content)."','".date('Y-m-d h:i:s')."','1')";
				
				if(mysqli_query($conn,$query)){
					$id = mysqli_insert_id($conn);
					$emailsf = explode(',', $emails);
					foreach($emailsf as $emailId){
						$email = trim($emailId);
						$rs_chk = mysqli_query($conn,"select * from users where email = '$email'");
						if(mysqli_num_rows($rs_chk) > 0){
							$row_u = mysqli_fetch_assoc($rs_chk);
							$user_id = $row_u['id'];
						}else{
							mysqli_query($conn,"insert into users (email, user_type_id, status) values ('$email','18','1');");
							$user_id = mysqli_insert_id($conn);
						}
						mysqli_query($conn,"INSERT INTO send_newsletter_users (newsletter_id,user_id, status)
							    VALUES(".$id.",".$user_id.",'1')");
						$headers 		= "Content-type:text/html;charset=UTF-8" . "\r\n";
						$headers 	   .= "From: no-reply@youcricketer.com \r\n";
			            mail($email,$title,$content,$headers);
					}
					?>
						<script>
						window.location = '<?php echo ADMIN?>already-sent.php';
						</script>
						<?
					
				}else{				
					echo '<div id="error">Information cannot be updated ... !</div>';				
				}
		}			
		
		if(isset($_GET['id'])){
			$row = get_record_on_id('email_templates', $_GET['id']);
		}
	?>
	<div style="clear:both; height:30px;"></div>
	<form id="frm-add" method="post" action="<?php echo ADMIN?>add-newsletter.php">
    <input type="hidden" name="action" value="submit" />
	<table width="80%"  id="detail">
		<tr>
			<td width="25%" class="xtright">Newsletter Template : </td>
			<td><?php echo get_combo('email_templates','title',$_GET['id'],'newsletter_template_id');?></td>
		</tr>
		<tr>
			<td width="25%" class="xtright">Title : </td>
			<td><input type="text" name="title" id="title_nl" value="<?php echo (isset($_GET['id']))?$row['title']:''?>" class="txt-feild-small" /></td>
		</tr>
		<tr>
			<td width="25%" class="xtright">Content : </td>
			<td><textarea name="content" cols="100" rows="10"  class="txt-feild-small" style="width:720px; height:200px;"><?php echo (isset($_GET['id']))?$row['content']:''?></textarea></td>
		</tr>
		<tr>
			<td>Emails : </td>
			<td>
				<textarea cols="100" rows="10" name="emails"></textarea>
				<br>
				E.g. email1@domain.com, email2@domain.com
			</td>
		</tr>
		<tr>
        	<td align="center"><input type="submit" value="Save" class="btn1" onclick="submitPromo()" /></td>
        </tr>
	</table>

</form>

	<div class="clear"></div>
    <div id="back"><a href="<?php echo ADMIN?>home.php">Back</a></div>
<script src="<?=WWW?>js/jquery-1.8.0.js" type="text/javascript"></script>
<script>
$(document).ready(function(){
	$('select#newsletter_template_id').change(function(){
		var id = this.value;
		window.location = '<?=ADMIN?>add-newsletter.php?id='+id;
	});
});
</script>
<!--***********************************-->
<?php include('common/footer.php')?>