<? include('common/header.php');?>	
<!--**********************************-->
<div class="box1">
	<h1>Add Press Release</h1>
    	<?	//==========================
			if(isset($_POST['action']) && $_POST['action'] == 'submit'){
				
				$title	= addslashes($_POST['title']);
				$content= addslashes($_POST['content']);
				$date 	= date_converter($_POST['release_date'], 'Y-m-d');
				
				$query		= "insert into press_releases (title, content, release_date, status) 
				values ('$title','$content','$date','1')";
				
				if(mysqli_query($conn,$query)){
					echo '<div id="success"><b>Success : </b>Information Added ... !</div>';
					?>
						<script>
						window.location = '<?=ADMIN?>press-releases.php';
						</script>
					<?
				}else{
					echo '<div id="error"><b>Failure : </b>Information already exists ... !</div>';
				}
			}
			//==========================
		?>
        <div style="clear:both; height:30px;"></div>
	<form id="frm-add" method="post" action="<?php echo ADMIN?>add-press-release.php" >
    <input type="hidden" name="action" value="submit" />
	<table id="detail">
		
        <tr>
			<td width="25%">Title : </td>
			<td><input type="text" name="title" class="validate[required] txt-feild-small"></td>
		</tr>
		<tr>
			<td width="25%">Content : </td>
			<td><textarea class="validate[required] txt-feild-large" style="height:200px;" name="content"></textarea></td>
		</tr>
		<tr>
			<td>Release Date : </td>
			<td><input type="text" name="release_date" readonly="readonly" class="validate[required] txt-feild-small calender"></td>
		</tr>
		<tr>
        	<td colspan="2" align="center"><input type="submit" value=" Save" class="btn1" /></td>
        </tr>
	</table>
	</form>
	<div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>press-releases.php" >Back</a></div>
    <div class="clear"></div>
<script type="text/javascript" src="<?=WWW?>js/jquery-ui.min.js"></script>
<link href="<?=WWW?>css/jquery-ui.css" type="text/css" rel="stylesheet">
<!--***********************************-->
<? include('common/footer.php')?>