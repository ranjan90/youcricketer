<? include('common/header.php');?>	
<!--**********************************-->
<div class="box1">
	<h1>View Blog Article</h1>
    	<?	//==========================
			if(isset($_POST['action']) && $_POST['action'] == 'submit'){
				
				$name		= $_POST['title'];
				$category_id= $_POST['category_id'];
				$content 	= $_POST['content'];
				$status 	= $_POST['status'];
				$id 		= $_GET['id'];

				$query		= "update blog_articles set title = '$name', content = '$content', category_id = '$category_id', status = '$status' where id = '$id'";
					
				if(mysqli_query($conn,$query)){
					echo '<div id="success"><b>Success : </b>Information Added ... !</div>';
					?>
						<script>
						window.location = '<?=ADMIN?>blog-articles.php';
						</script>
					<?
				}else{
					echo '<div id="error"><b>Failure : </b>Information already exists ... !</div>';
				}
			}
			if(isset($_GET['action']) && $_GET['action'] == 'delete'){
				$cid = $_GET['cid'];
				$query = "update blog_comments set status = '".DELETED_STATUS."' where id = '$cid'";
				mysqli_query($conn,$query);
			}
			//==========================
			$id 	= $_GET['id'];
			$row 	= get_record_on_id('blog_articles', $id);
		?>
        <div style="clear:both; height:30px;"></div>
	<form id="frm-add" method="post" action="<?php echo ADMIN?>view-blog-article.php?id=<?=$id?>" >
    <input type="hidden" name="action" value="submit" />
	<table id="detail">
		
        <tr>
			<td width="25%">Blog Category : </td>
			<td><?=get_combo('blog_categories','name',$row['category_id'],'category_id')?></td>
		</tr>
		<tr>
			<td width="25%">Title : </td>
			<td><input type="text" name="title" class="txt-feild-small validate[required]" value="<?=$row['title']?>"></td>
		</tr>
		<tr>
			<td>Content</td>
			<td><textarea class="validate[required] txt-feild-large" style="height:200px;" name="content"><?=$row['content']?></textarea></td>
		</tr>
		<tr>
			<td>Date Created</td>
			<td><?=date_converter($row['create_date'])?></td>
		</tr>
		<tr>
			<td>Status</td>
			<td><?=get_status_combo($row['status'])?></td>
		</tr>
        <tr>
        	<td colspan="2" align="center"><input type="submit" value=" Save" class="btn1" /></td>
        </tr>
	</table>
	</form>
	<div class="clear"></div>
	<div class="box1">
	<h1><a name="comments">Comments</a></h1>
	<table width="100%" class="grid">
		<tr>
			<th>Action</th>
			<th>Commented By</th>
			<th>Comment Date</th>
			<th>Comment Text</th>
		</tr>
		<? 	$rs_c = mysqli_query($conn,"select * from blog_comments where blog_article_id = '$id'");
			if(mysqli_num_rows($rs_c) > 0){
				while($row_c = mysqli_fetch_assoc($rs_c)){ ?>
				<tr>
					<td>
						<a href="<?=ADMIN?>view-blog-article.php?id=<?=$id?>&cid=<?=$row_c['id']?>&action=delete">
							<img src="<?=WWW?>images/icons/delete.png">
						</a>
					</td>
					<td><?=get_combo('users', 'name', $row_c['user_id'],'','text')?></td>
					<td><?=date_converter($row_c['comment_date'])?></td>
					<td><?=$row_c['comment']?></td>
				</tr>
			<?  } ?>
		<?  }else{ ?>	
		<tr><td colspan="4">No record found ... !</td></tr>
		<?	} ?>
	</table>
	</div>
	<div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>blog-articles.php" >Back</a></div>
    <div class="clear"></div>

<!--***********************************-->
<? include('common/footer.php')?>