<? include('common/header.php');?>
<script type="text/javascript" src="<?php echo WWW;?>js/jquery-ui.min.js"></script>
<link type="text/css" rel="stylesheet" href="<?php echo WWW;?>css/jquery-ui.css">
<script>
$(document).ready(function(){
	$("#start_date, #expiry_date").datepicker({changeYear:"true",yearRange: "1980:2020"});
    $("#start_date, #expiry_date").datepicker({changeYear:"true",minDate:0});
});
</script>
<!--**********************************-->
    <div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>payments.php" title="Go to banner">Back</a></div>
    <div class="clear"></div>
<div class="box1">
	<h1>Payment Detail</h1>

        <div style="clear:both; height:30px;"></div>
	<input type="hidden" name="action" value="submit" />
	<table id="detail">
		<?php
        $id  = (int) $_GET['id'];
        $sql = "select user_payments.*, user_payments.date_created as date,users.f_name,users.last_name,marke_place_items.item_price,marke_place_items.* from user_payments INNER JOIN users ON users.id = user_payments.user_id
	                INNER JOIN marke_place_items ON marke_place_items.item_id = user_payments.item_id where user_payments.payment_id='".$id."'";

        $rs  = mysqli_query($conn,$sql);
        $row = mysqli_fetch_assoc($rs);


        ?>

				<tr>
					<td width="25%">User Name </td>
					<td><?php echo $row['f_name']." ".$row['last_name']?></td>
				</tr>

				<tr>
					<td width="25%">Item Name </td>
					<td><?php echo $row['item_title']?></td>
				</tr>

				<tr>
					<td width="25%">Item Price </td>
					<td><?php echo $row['item_price']?></td>
				</tr>

				<tr>
					<td width="25%">Item detail </td>
					<td><?php echo $row['item_detail']?></td>
				</tr>

				<tr>
					<td width="25%">Item Type </td>
					<td><?php if($row['item_type']=='1') echo "Global";elseif($row['item_type']=='2') echo "Normal";?></td>
				</tr>

				<?php if($row['item_type']=='2') {?>

					<tr>
						<td width="25%">Item Country </td>
						<td><?php echo $row['item_country']?></td>
					</tr>

					<tr>
						<td width="25%">Item State </td>
						<td><?php echo $row['item_state']?></td>
					</tr>

					<tr>
						<td width="25%">Item City </td>
						<td><?php echo $row['item_city']?></td>
					</tr>

				<?php } ?>

				<tr>
					<td width="25%">Payment Status </td>
					<td><?php echo $row['payment_status']?></td>
				</tr>

				<tr>
					<td width="25%">Transaction Id </td>
					<td><?php echo $row['transaction_id']?></td>
				</tr>

				<tr>
					<td width="25%">Date </td>
					<td><?php echo $row['date']?></td>
				</tr>

	</table>



	<div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>payments.php" title="Go to banner">Back</a></div>
	<div class="clear"></div>
<script>
$(document).ready(function(){
	$('input[name=type]').click(function(){
		if(this.value == 'img'){
			$('tr.bannerurl').fadeIn();
			$('tr.bannerurl input').addClass('validate[required]');
			$('tr.bannerurl input').addClass('validate[url]');

			$('tr.code').fadeOut();
			$('tr.bannerurl input').removeClass('validate[required]');
		}else{
			$('tr.code').fadeIn();
			$('tr.bannerurl input').addClass('validate[required]');

			$('tr.bannerurl').fadeOut();
			$('tr.bannerurl input').removeClass('validate[required]');
			$('tr.bannerurl input').removeClass('validate[url]');
		}
	});
});
</script>
<!--***********************************-->
<? include('common/footer.php')?>
