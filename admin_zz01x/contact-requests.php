<?php include('common/header.php');?>
<!--**********************************-->
<div class="box1">
  <h1>Contact Requests</h1>
   <table width="99%"  class="grid" border="0">
<tr>


<td width="751">
<div id="options">
     <div id="box" style="width:500px;">
        <form method="get" action="<?=ADMIN?>contact-requests.php">
            <input type="hidden" name="action" value="search" />
            <input type="text" name="searchtxt" class=" txt-feild-small" value="<?=$_GET['searchtxt'];?>" placeholder="Type here" />
            <?=get_status_combo_selected($_GET['status'])?>
            <input type="submit" value="Go" />
        </form>
    </div>
    </td>
   <td width="326"></td>
    <div id="box" class="text" style="width:280px;">
      <td width="117"><a class="btn1" href="<?=ADMIN?>contact-requests.php">View All Records</a></td>
      <td><a  href="<?=ADMIN?>contact-requests.php?show_deleted=1" title="Show Deleted" class="btn1">Show Deleted</a></td>
      </div>

</tr>
</table>
<div class="clear"></div>
<?php	$id	= $_GET['id'];
        $row 	= mysqli_fetch_assoc(mysqli_query($conn,"select * from contact_requests where id = '$id' "));
        $status	= $row['status'];
        $status	= ($status == '1')?'0':'1';

	if(isset($_GET['action']) && ($_GET['action'] == 'change' || $_GET['action'] == 'delete')){
          if($_GET['action'] == 'change'){
            $query	= "update contact_requests set status = '$status' where id = '$id'";
          }else if($_GET['action'] == 'delete'){
            $rc = mysqli_num_rows(mysqli_query($conn, "select id from contact_requests where id=".$id." and status=".DELETED_STATUS));
            if($rc > 0){
              $query = "DELETE FROM `contact_requests` WHERE id=".$id;
            }else{
              $query  = "update contact_requests set status = '".DELETED_STATUS."' where id = '$id'";
            }
            // if($row['status'] == '1'){
            //        $query = "update contact_requests set status = '".DELETED_STATUS."' where id = '$id'";
            //       }else{
            //         $query  = "delete from contact_requests where id = '$id';";
            //       }

          }

          if(mysqli_query($conn,$query)){
            echo '<div id="success"> <b> '.SUCCESS.': </b> Information updated ... ! </div>';
          }else{
            echo '<div id="error"> <b> '.FAILURE.': </b> Information cannot be updated... ! </div>';
          }
        }
        if(isset($_POST['action']) && $_POST['action'] == 'multiple'){
              $ids = array_keys($_POST['ids']);
              $counter = 0;
              foreach($ids as $idr){
                $rowRecord = get_record_on_id('blog_articles', $idr);
                if($_POST['dd_status'] == DELETED_STATUS && $rowRecord['status'] == DELETED_STATUS){
                  mysqli_query($conn,"delete from contact_requests where id = '$idr'");
                }else{
                  mysqli_query($conn,"update contact_requests set status = '".$_POST['dd_status']."' where id = '$idr'");
                }
                $counter++;
              }
              echo '<div id="success">'.$counter.' records updated ... !</div>';
            }
?>
<div class="clear"></div>
<div id="pagination-top"></div>
<form method="post" id="multiple">
    <input type="hidden" name="action" value="multiple">
<table width="1218" class="grid">


            <tr>
                <th width="10%">Action</th>
                <th width="15%">Department</th>
              <th width="10%">Name</th>
              <th width="10%">Phone</th>
              <th width="10%">Email</th>
              <th width="50%">Message</th>
            </tr>

      <?  $rpp = PRODUCT_LIMIT_ADMIN; // results per page
            // $page = intval($_GET["page"]);
            $ppage = intval($_GET["page"]);

            if($ppage<=0){
            $ppage = 1;
            $start=0;
            $limit=PRODUCT_LIMIT_ADMIN;
            }else{
            $start=($ppage-1)*PRODUCT_LIMIT_ADMIN;
            $limit=$ppage*PRODUCT_LIMIT_ADMIN;
            }
      $date  = date('Y-m-d h:m:s');
      $query = "select * from contact_requests where 1=1 ";
      //=======================================
      if(isset($_GET['action']) && $_GET['action'] == 'search'){
                if($_GET['searchtxt'] != ''){
                    $query  .= "and name like '%".$_GET['searchtxt']."%'";
                }
                if($_GET['status'] != ''){
                    $query .= " AND status LIKE '".$_GET['status']."%'";
                }
      }
      if(isset($_GET['show_deleted'])){
                $query .= " and status = '".DELETED_STATUS."'";
              }else{
                $query .= " and status <> '".DELETED_STATUS."'";
              }
              //echo $query;
      //=======================================
      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
                echo '<tr><td colspan="5">Your search returned no results</td></tr>';
            }
      $rs   = mysqli_query($conn,$query);
      $tcount = mysqli_num_rows($rs);
      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
      $count = 0;
      $i = ($ppage-1)* $rpp;
          $x = 0;

      while(($count<$rpp) && ($i<$tcount)){
                mysqli_data_seek($rs,$i);
                $row = mysqli_fetch_array($rs);
      ?>
        <tr <?=($x%2 == 0)?' bgcolor="#f3f3f3"':''?>>
                    <td>
                      <input type="checkbox" name="ids[<?=$row['id']?>]">
                        <a href="<?=ADMIN?>contact-requests.php?action=change&id=<?=$row['id']?>" title="Change Status">
                        <?
                          if($row['status'] == '1'){
                            $img = 'status.png';
                            $alt = 'Active Record';
                          }else if($row['status'] == '0'){
                            $img = 'in-status.png';
                            $alt = 'Inactive Record';
                          }else if($row['status'] == DELETED_STATUS){
                            $img = 'restore.png';
                            $alt = 'Deleted Record';
                          }
                        ?>
                        <img src="<?=WWW?>images/icons/<?=$img?>" alt="<?=$alt?>" width="16" />
                      </a>
                      <a href="<?=ADMIN?>contact-requests.php?action=delete&id=<?=$row['id']?>" title="Delete Record">
                        <img src="<?=WWW?>images/icons/delete.png" alt="Delete" />
                        </a>
                      </td>
                      <td><?=get_combo('admin_emails','concat(title," ",email)',$row['department_id'],'','text')?></td>
                    <td><?=$row['name']?></td>
                    <td><?=$row['phone']?></td>
                    <td><?=$row['email']?></td>
                    <td><?=$row['content']?></td>
                </tr>
      <?
        $i++;
        $count++;
        $x++;
      }
    ?>
    <tr>
      <td>
        <input type="radio" name="checked" value="checked_all"> &nbsp; Checked All
      </td>
      <td>
        <input type="radio" name="checked" value="unchecked_all"> &nbsp; Unchecked All
      </td>
      <td>
        <select name="dd_status" class="dd1">
          <option value="" selected="selected">Select One</option>
          <option value="1">Activate Records</option>
          <option value="0">Deactivate Records</option>
          <option value="<?=DELETED_STATUS?>">Delete Records</option>
        </select>
      </td>
      <td>
        <input type="submit" value="Update">
      </td>
    </tr>
</table>
<? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_ADMIN){ ?>
    <div id="pagination-bottom">
      <?php
        $keywords = array();
        foreach($_GET as $key=>$value){
          $keywords[] = $key.'='.$value;
        }
        $param = "action=search&searchtxt=".$_GET['searchtxt']."&status=".$_GET['status']."&";
        $reload = 'contact-requests.php?'.implode('&',$keywords).'&';
        echo paginate_one($reload, $ppage, $tpages);
      ?>
      <input type="hidden" name="pagination-page" value="contact-requests">
      <input type="hidden" name="pagination-param" value="<?=$param?>">
        </div>
        <? } ?>
        <div class="clear"></div>
<!--**********************************-->
<?php include('common/footer.php');?>
