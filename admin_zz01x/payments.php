<? include('common/header.php');?>
    <!--**********************************-->
<div class="box1">
    <h1>Market Payments</h1>
    <table width="99%"  class="grid" border="0">
<tr>

<td width="751">
<div id="options">
     <div id="box" style="width:500px;">
        <form method="get" action="<?=ADMIN?>payments.php">
            <input type="hidden" name="action" value="search" />
            <input type="text" name="searchtxt" class=" txt-feild-small" value="<? echo ((isset($_GET['searchtxt']))?$_GET['searchtxt']:'')?>"  placeholder="Type here" />
            <?=get_status_combo_selected($_GET['status']);?>
            <select name="payment_type" id="payment_type" class="dd1 form-control">
                <option selected="selected" value="">Select One</option>
                <option value="paypal" <?php if(isset($_GET['payment_type']) && $_GET['payment_type'] === 'paypal'){echo "selected";} ?>>Paypal</option>
                <option value="square" <?php if(isset($_GET['payment_type']) && $_GET['payment_type'] === 'square'){echo "selected";} ?>>Square</option>

                <!--<option value="2checkout" <?php if(isset($_GET['payment_type']) && $_GET['payment_type'] === '2checkout'){echo "selected";} ?>>2checkout</option></select>-->

            <input type="submit" value="Go" />
        </form>
    </div>
    </td>
   <td width="326"></td>
    <div id="box" class="text" style="width:280px;">
      <td width="117"><a class="btn1" href="<?=ADMIN?>payments.php">View All Records</a></td>
      <td><a  href="<?=ADMIN?>payments.php?show_deleted=1" title="Show Deleted" class="btn1">Show Deleted</a></td>
    </div>

</tr>
</table>
    <div class="clear"></div>
    <?		if(isset($_GET['action']) && ($_GET['action'] == 'change' || $_GET['action'] == 'delete')){

				        $id			= $_GET['id'];
                $query = 'select * from user_payments where payment_id = '.$id;
                $rs = mysqli_query($conn,$query);
                $row = mysqli_fetch_assoc($rs);
				        $status		= $row['status'];
                $status		= ($status == '1')?'0':'1';

				if($_GET['action'] == 'change'){
					$query	= "UPDATE `user_payments` SET `status`= '".$status."' WHERE payment_id=".$id;
                 mysqli_query($conn,$query);

				}else if($_GET['action'] == 'delete'){
          $rc = mysqli_num_rows(mysqli_query($conn, "select payment_id from user_payments where payment_id=".$id." and status=".DELETED_STATUS));
          if($rc > 0){
            $query = "DELETE FROM `user_payments` WHERE payment_id=".$id;
          }else{
            $query  = "update user_payments set status = '".DELETED_STATUS."' where payment_id = '$id'";
          }
          if(mysqli_query($conn,$query)){
              echo '<div id="success"> <b> Success: </b> Information Updated ... ! </div>';
          }else{
              echo '<div id="error"> <b> Failure: </b> Information cannot be updated ... ! </div>';
          }
				}

			}
      if(isset($_POST['action']) && $_POST['action'] == 'multiple'){
              $ids = $_POST['ids'];
              if($_POST['dd_status']==8){
                $counter = 0;
                foreach($ids as $idr){
                  $rc = mysqli_num_rows(mysqli_query($conn, "select payment_id from user_payments where payment_id=".$idr." and status=".DELETED_STATUS));
                  if($rc > 0){
                    $query = "DELETE FROM `user_payments` WHERE payment_id=".$idr;
                  }else{
                    $query  = "update user_payments set status = '".DELETED_STATUS."' where payment_id = '$idr'";
                  }
                  mysqli_query($conn,$query);
                  $counter++;
                }
              }else{
                $counter = 0;
                foreach($ids as $idr){
                    $query  = "update user_payments set status = '".$_POST['dd_status']."' where payment_id = '$idr'";
                    mysqli_query($conn,$query);
                  $counter++;
                }
              }
              echo '<div id="success">'.$counter.' records updated ... !</div>';
            }
    if(isset($_GET['noRecord'])){
        echo '<div id="error"> <b> Failure: </b> Information Not Exists ... ! </div>';
    }
	?>
    <div class="clear"></div>
    <div id="pagination-top"></div>
    <form method="post" id="multiple">
    <input type="hidden" name="action" value="multiple">
    <table class="grid" cellspacing="0" cellpadding="0" width="100%">
      <tr>
        <th width="10%">Action</th>
        <th width="15%">User Name</th>
        <th width="25%">Item Title</th>
        <th width="10%">Item Price</th>
        <th width="10%">Item Type</th>
        <th width="10%">Payment Method</th>
        <th width="10%">Payment Status</th>
        <th width="15%">Date</th>
      </tr>
      <?  $rpp = PRODUCT_LIMIT_ADMIN; // results per page
            $ppage = intval($_GET["page"]);
      if($ppage<=0) $ppage = 1;
      $query = "select user_payments.*, user_payments.date_created as date, users.f_name, users.last_name, marke_place_items.item_price, marke_place_items.item_title, marke_place_items.item_type, marke_place_items.item_country, marke_place_items.item_state, marke_place_items.item_city from user_payments INNER JOIN users ON users.id = user_payments.user_id
                INNER JOIN marke_place_items ON marke_place_items.item_id = user_payments.item_id ";

      //=======================================
      if(isset($_GET['action']) && $_GET['action'] == 'search'){
            $search = (string) $_GET['searchtxt'];
            $search = strtolower($search);
            $query .= "and ((lower(users.f_name) like '%$search%') or (lower(users.last_name) like '%$search%') or (lower(marke_place_items.item_title) like '%$search%'))";
           // $query  .= " and item_title LIKE '%$search%'";
          if(isset($_GET['status']) && trim($_GET['status']) != ''){
              $status = $_GET['status'];
              $query  .= " and user_payments.status = '$status'";
          }
         if(isset($_GET['payment_type']) && $_GET['payment_type'] != ''){
                      $payment_type = $_GET['payment_type'];
                      $query  .= " and user_payments.payment_type = '$payment_type' ";
         }
      }
      if(isset($_GET['show_deleted'])){
        $query .= " and user_payments.status = '".DELETED_STATUS."'";
      }else{
        $query .= " and user_payments.status <> '".DELETED_STATUS."'";
      }
      $query .= "ORDER BY `date_created` DESC";
      //echo $query;
      //=======================================
      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
                echo '<tr><td colspan="5">Your search returned no results</td></tr>';
            }
      $rs   = mysqli_query($conn,$query);
      $tcount = mysqli_num_rows($rs);
      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
      $count = 0;
      $i = ($ppage-1)* $rpp;
          $x = 0;
      while(($count<$rpp) && ($i<$tcount)){
                mysqli_data_seek($rs,$i);
                $row = mysqli_fetch_assoc($rs);


      ?>
      <tr <?=($x%2 == 0)?' bgcolor="#f3f3f3"':''?>>
        <td align="center">
          <input type="checkbox" name="ids[<?=$row['advertise_id']?>]" value="<?php echo $row['payment_id']; ?>">

            <!--<a href="<?=ADMIN?>add-market-place-item.php?id=<?=$row['payment_id']?>" title="Update Record">
              <img src="<?=WWW?>images/icons/view.png" alt="View" />
            </a>

          <a href="<?=ADMIN?>add-market-place-item.php?id=<?=$row['payment_id']?>" title="Update Record">
              <img src="<?=WWW?>images/icons/edit.png" alt="Edit" />
            </a>-->

            <a href="<?php echo ADMIN?>payment_detail.php?id=<?=$row['payment_id']?>" title="View Detail">
                <img src="<?=WWW?>images/icons/view.png" alt="View" />
              </a>
            <a href="<?=ADMIN?>payments.php?action=change&id=<?=$row['payment_id']?>" title="Change Status">
                        <?
                          if($row['status'] == '1'){
                            $img = 'status.png';
                            $alt = 'Active Record';
                          }else if($row['status'] == '0'){
                            $img = 'in-status.png';
                            $alt = 'Inactive Record';
                          }else if($row['status'] == DELETED_STATUS){
                            $img = 'restore.png';
                            $alt = 'Deleted Record';
                          }
                        ?>
                        <img src="<?=WWW?>images/icons/<?=$img?>" alt="<?=$alt?>" width="16" />
                      </a>
            <a href="<?=ADMIN?>payments.php?action=delete&id=<?=$row['payment_id']?>" title="Delete Record">
              <img src="<?=WWW?>images/icons/delete.png" alt="Delete" />
            </a>

        </td>
        <td><?=$row['f_name'].' '.$row['last_name']?> </td>
        <td><?=$row['item_title']?> </td>
        <td><?=$row['item_price']?> </td>
        <td><?php if($row['item_type']=='1') echo "Global";elseif($row['item_type']=='2') echo "Normal";?> </td>
        <td><?=$row['payment_type']?></td>
        <td><?=$row['payment_status']?></td>
        <td><?=$row['date']?></td>
      </tr>
      <?
        $i++;
        $count++;
        $x++;
      }
    ?>
    <tr>
      <td>
        <input type="radio" name="checked" value="checked_all"> &nbsp; Checked All
      </td>
      <td>
        <input type="radio" name="checked" value="unchecked_all"> &nbsp; Unchecked All
      </td>
      <td>
        <select name="dd_status" class="dd1">
          <option value="" selected="selected">Select One</option>
          <option value="1">Activate Records</option>
          <option value="0">Deactivate Records</option>
          <option value="<?=DELETED_STATUS?>">Delete Records</option>
        </select>
      </td>
      <td>
        <input type="submit" value="Update">
      </td>
    </tr>
    </table>
<? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_ADMIN){ ?>
    <div id="pagination-bottom">
      <?php
       $keywords = array();
        foreach($_GET as $key=>$value){
          $keywords[] = $key.'='.$value;
        }
        $param = 'action=search&searchtxt='.$_GET['searchtxt'].'&status='.$_GET['status'].'&payment_type='.$_GET['payment_type'].'&';
        $reload = 'payments.php?'.$param;
        echo paginate_one($reload, $ppage, $tpages);
      ?>
      <input type="hidden" name="pagination-page" value="payments">
      <input type="hidden" name="pagination-param" value="<?=$param?>">
        </div>
        <? } ?>
    <div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>home.php" title="Go to Home">Back</a></div>
    <div class="clear"></div>
  </div>
  <!--**********************************-->
  <? include('common/footer.php');?>
