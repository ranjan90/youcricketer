<? include('common/header.php');?>	
<!--**********************************-->
<div class="box1">
	<h1>View Event</h1>
    	<?	//==========================
			if(isset($_POST['action']) && $_POST['action'] == 'submit'){
				
				$title		= mysqli_real_escape_string($conn,$_POST['title']);
				$content 	= mysqli_real_escape_string($conn,$_POST['content']);
				$start_date = date_converter($_POST['start_date'],'Y-m-d');
				$end_date 	= date_converter($_POST['end_date'],'Y-m-d');
				$meta_keywords = mysqli_real_escape_string($conn,$_POST['meta_keywords']);
				$meta_description = mysqli_real_escape_string($conn,$_POST['meta_description']);
				$date 		= date('Y-m-d');
				$user_id 	= $_SESSION['ycdc_admin_dbuid'];
				$is_announcement = ($_POST['is_announcement'] == 'on')?'1':'0';
				$status 	= $_POST['status'];
				$id 		= $_GET['id'];


				$query		= "update events set is_announcement = '$is_announcement', title = '".mysqli_real_escape_string($conn,$title)."', content = '".mysqli_real_escape_string($conn,$content)."', start_date = '$start_date', end_date = '$end_date', meta_keywords = '$meta_keywords', meta_description = '$meta_description', status = '$status' where id = '$id'";
				//echo $query;exit;
				if(mysqli_query($conn,$query)){
					echo '<div id="success"><b>Success : </b>Information Added ... !</div>';
					?>
						<script>
						window.location = '<?=ADMIN?>events.php';
						</script>
					<?
				}else{
					echo '<div id="error"><b>Failure : </b>Information cannot be updated ... !</div>';
				}
			}
			//==========================
			$id = $_GET['id'];
			$row= get_record_on_id('events', $id);
		?>
        <div style="clear:both; height:30px;"></div>
	<form id="frm-add" method="post" action="<?php echo ADMIN?>view-event.php?id=<?=$id?>" >
    <input type="hidden" name="action" value="submit" />
	<table id="detail">
		<tr>
			<td width="25%">Is Announcement / Live Streaming : </td>
			<td><input type="checkbox" name="is_announcement" <?=($row['is_announcement'] == '1')?'checked="checked"':'';?>></td>
		</tr>
		<tr>
			<td width="25%">Title : </td>
			<td><input type="text" name="title" value="<?=$row['title']?>" class="txt-feild-small validate[required]"></td>
		</tr>
		<tr>
			<td>Content</td>
			<td><textarea class="validate[required] txt-feild-large" style="height:200px;" name="content"><?=$row['content']?></textarea></td>
		</tr>
		<tr>
			<td>Start date</td>
			<td><input type="text" name="start_date" value="<?=date_converter($row['start_date'],'m/d/Y')?>" class="txt-feild-small validate[required] calender"></td>
		</tr>
		<tr>
			<td>End date</td>
			<td><input type="text" name="end_date" value="<?=date_converter($row['end_date'],'m/d/Y')?>" class="txt-feild-small validate[required] calender"></td>
		</tr>
		<tr>
			<td>Meta Keywords</td>
			<td><textarea class="validate[required] txt-feild-large" style="height:200px;" name="meta_keywords"><?=$row['meta_keywords']?></textarea></td>
		</tr>
		<tr>
			<td>Meta Description</td>
			<td><textarea class="validate[required] txt-feild-large" style="height:200px;" name="meta_description"><?=$row['meta_description']?></textarea></td>
		</tr>
		<tr>
			<td>Status</td>
			<td><?=get_status_combo($row['status'])?></td>
		</tr>
        <tr>
        	<td colspan="2" align="center"><input type="submit" value=" Save" class="btn1" /></td>
        </tr>
	</table>
	</form>
	<div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>events.php" >Back</a></div>
    <div class="clear"></div>
<script type="text/javascript" src="<?=WWW?>js/jquery-ui.min.js"></script>
<link href="<?=WWW?>css/jquery-ui.css" type="text/css" rel="stylesheet">
<!--***********************************-->
<? include('common/footer.php')?>