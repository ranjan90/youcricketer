<? include('common/header.php');?>	
<!--**********************************-->
<div class="box1">
	<h1>Add How To </h1>
	<?	
		if($_POST['action'] == 'submit'){

			$title	= $_POST['title'];
			$value	= $_POST['value'];
			$mkeywords= $_POST['mkeywords'];
			$mdescription=$_POST['mdescription'];

			if(!empty($title) && !empty($value)){
				$slug = getSlug($title);
				$query = "insert into how_tos (title,slug,content,meta_keyword, meta_description,status,date_added) 
				values('".($title)."','$slug','".($value)."','$mkeywords','".($mdescription)."','1',now())";
				//echo $query;exit;

				if(mysqli_query($conn,$query)){
					echo '<div id="success">Information Updated ... !</div>';
					?>
						<script>
						window.location = '<?=ADMIN?>how-tos.php';
						</script>
						<?
				}else{//echo $query.mysqli_error($conn);exit;
					echo '<div id="error">Information already exists ... !</div>';
				}
			}else{
				echo '<div id="error">Please fill all fileds... !</div>';
			}
		}	
	?>
	<div style="clear:both; height:30px;"></div>
	<form id="frm-add" method="post" >
    <input type="hidden" name="action" value="submit" />
	<table  id="detail" cellpadding="4" cellspacing="4">
		<tr>
			<td  valign="top">Title : </td>
			<td><input type="text" name="title" class="validate[required] txt-feild-small " style="width:700px;"/></td>
		</tr>
        <tr>
			<td  valign="top">Description : </td>
			<td><textarea name="value" cols="500" rows="10" class="validate[required] txt-feild-large editor" ></textarea></td>
		</tr>
		<tr>
			<td valign="top">Meta Keywords : </td>
			<td><textarea name="mkeywords" cols="50" rows="10" class="txt-feild-large" style="width:700px;height:200px;"></textarea></td>
		</tr>
		<tr>
			<td valign="top">Meta Description : </td>
			<td><textarea name="mdescription" cols="50" rows="10" class="txt-feild-large" style="width:700px;height:200px;"></textarea></td>
		</tr>
        <tr>
        	<td colspan="2" align="center"><input type="submit" value="Save"class="btn1" /></td>
        </tr>
	</table>

</form>

	<div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>how-tos.php">Back</a></div>

<!--***********************************-->
<script type="text/javascript" src="<?php echo WWW; ?>/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: ".editor",
	width:700,
	height:400,
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste jbimages"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages"
});
</script>
<? include('common/footer.php')?>