<? include('common/header.php');?>	
<!--**********************************-->
<div class="box1">
<h1>Group Replies (<?=get_combo('group_posts', 'title',$_GET['id'],'','text')?>) </h1>
  <?      if(isset($_GET['action']) && ($_GET['action'] == 'change' || $_GET['action'] == 'delete')){
    
                $id         = $_GET['id'];
                $row        = get_record_on_id('group_replies',$id,'','status');
                $status     = $row['status'];
    
                $status     = ($status == '1')?'0':'1';
            
                if($_GET['action'] == 'change'){
                    $query  = "update group_replies set status = '$status' where id = '$id'";
                }else if($_GET['action'] == 'delete'){
                    $query  = "update group_replies set status = '".DELETED_STATUS."' where id = '$id'";
                }                             
                if(mysqli_query($conn,$query)){
                    echo '<div id="success"> <b> Success: </b> Information Updated ... ! </div>';
                }else{
                    echo '<div id="error"> <b> Failure: </b> Information cannot be updated ... ! </div>';
                }
            }
    ?>
  <div class="clear"></div>
    <table class="grid" cellspacing="0" cellpadding="0" width="100%">
      <tr>
        <th width="5%">Action</th>
        <th width="20%">Comment</th>
        <th width="7%">Posted By</th>
        <th width="7%">Posted Date</th>
      </tr>      
      
      <?  $rpp = PRODUCT_LIMIT_ADMIN; // results per page
            $ppage = intval($_GET["page"]);
      if($ppage<=0) $ppage = 1;
      $query = "select * from group_replies where topic_id = ".$_GET['id'];
      
      //=======================================
      if(isset($_POST['action']) && $_POST['action'] == 'search'){
        if(!empty($_POST['searchtxt'])){
          $query  .= "and comments = '%".$_POST['searchtxt']."%'";
        }
        if(!empty($_POST['status']) && $_POST['status'] != 'Select One'){
          $query  .= "and status = '".$_POST['status']."'";
        }
      }
      if(isset($_GET['show_deleted'])){
                $query .= " and status = '".DELETED_STATUS."'";
              }else{
                $query .= " and status <> '".DELETED_STATUS."'";
              }
      //echo $query;
      //=======================================
      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
                echo '<tr><td colspan="5">Your search returned no results</td></tr>';
            }
      $rs   = mysqli_query($conn,$query);
      $tcount = mysqli_num_rows($rs);
      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
      $count = 0;
      $i = ($ppage-1)* $rpp;
          $x = 0;
      while(($count<$rpp) && ($i<$tcount)){
                mysqli_data_seek($rs,$i);
                $row = mysqli_fetch_array($rs);
      ?>
      <tr <?=($x%2 == 0)?' bgcolor="#f3f3f3"':''?>>
        <td>
          <a href="<?=ADMIN?>group-replies.php?action=change&id=<?=$row['id']?>" title="Change Status">
                        <?
                          if($row['status'] == '1'){
                            $img = 'status.png';
                            $alt = 'Active Record';
                          }else if($row['status'] == '0'){
                            $img = 'in-status.png';
                            $alt = 'Inactive Record';
                          }else if($row['status'] == DELETED_STATUS){
                            $img = 'restore.png';
                            $alt = 'Deleted Record';
                          }
                        ?>
                        <img src="<?=WWW?>images/icons/<?=$img?>" alt="<?=$alt?>" width="16" />
                      </a>
          <a href="<?=ADMIN?>group-replies.php?action=delete&id=<?=$row['id']?>" title="Delete Record">
                <img src="<?=WWW?>images/icons/delete.png" alt="Delete" />
            </a>
        </td>
        <td><?=$row['comment']?></td>
        <td><?=get_combo('users','f_name',$row['user_id'],'','text')?></td>
        <td><?=date_converter($row['post_date'])?></td>
      </tr>
      <?
        $i++;
        $count++;
        $x++;
      } 
    ?>
    </table>

    <div id="back"><a href="<?=ADMIN?>group-posts.php?id=<?=$_GET['id']?>" title="Go to Back">Back</a></div>
             
    <div class="clear"></div>
</div>
<!--*****************************-->
<? include('common/footer.php') ?>