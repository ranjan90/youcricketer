<? include('common/header.php');?>	
<!--**********************************-->
<div class="box1">
	<h1>Packages</h1>
	 <table width="99%"  class="grid" border="0">
<tr>
            	

<td width="751">
<div id="options">
     <div id="box" style="width:500px;">
        <form method="get" action="<?=ADMIN?>packages.php">
            <input type="hidden" name="action" value="search" />
            <input type="text" name="searchtxt" class=" txt-feild-small"  placeholder="Type here"/>
            <?=get_status_combo()?>
            <input type="submit" value="Go" />
        </form>
    </div>
    </td>
   <td width="326"></td>
    <div id="box" class="text" style="width:280px;">
      <td width="117"><a class="btn1" href="<?=ADMIN?>packages.php">View All Records</a></td>
      <td width="116"><a class="btn2" href="<?=ADMIN?>add-package.php" >Add Record</a></td>
      <td><a  href="<?=ADMIN?>packages.php?show_deleted=1" title="Show Deleted" class="btn1">Show Deleted</a></td>
      </div>
                   
</tr>
</table>
                    
                   <div class="clear"></div>
                    <?		$id			= $_GET['id'];
                            $row 		= get_record_on_id('packages',$id,'status');
                            $status		= $row['status'];

                            $status		= ($status == '1')?'0':'1';

                            if(isset($_GET['action']) && ($_GET['action'] == 'change' || $_GET['action'] == 'delete')){
                                if($_GET['action'] == 'change'){
                                    $query	= "update packages set status = '$status' where id = '$id'";
                                }else if($_GET['action'] == 'delete'){
                                    $query  = "update packages set status = '".DELETED_STATUS."' where id = '$id'";
                                }                             
								
                                if(mysqli_query($conn,$query)){
                                    echo '<div id="success">Information Updated ... !</div>';
                                }else{
                                    echo '<div id="error">Information cannot be updated ... !</div>';
                                }
                            }
                        ?>
                    <div class="clear"></div>
                    <div id="pagination-top"></div>
         	 <table width="1218" class="grid">

		
            <tr>
                <th width="10%">Action</th>
            	  <th width="10%">Title</th>
                <th width="10%">Duration</th>
                <th width="10%">Price</th>
            </tr>

    	<?	$rpp = PRODUCT_LIMIT_ADMIN; // results per page
            $page = intval($_GET["page"]);

			if($page<=0) $page = 1;
			$date  = date('Y-m-d h:m:s');
			$query = "select * from packages where 1=1 ";
			//=======================================
			if(isset($_GET['action']) && $_GET['action'] == 'search'){
                if($_GET['searchtxt'] != ''){
                    $query  .= "and title like '%".$_GET['searchtxt']."%'";    
                }
                if($_GET['status'] != ''){
                    $query .= " AND status LIKE '".$_GET['status']."%'";
                }
			}
			if(isset($_GET['show_deleted'])){
                $query .= " and status = '".DELETED_STATUS."'";
              }else{
                $query .= " and status <> '".DELETED_STATUS."'";
              }
              //echo $query;
			//=======================================
			if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
                echo '<tr><td colspan="5">Your search returned no results</td></tr>';
            }
			$rs 	= mysqli_query($conn,$query);
			$tcount = mysqli_num_rows($rs);
			$tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
			$count = 0;
			$i = ($page-1)* $rpp;
        	$x = 0;

			while(($count<$rpp) && ($i<$tcount)){
                mysqli_data_seek($rs,$i);
                $row = mysqli_fetch_array($rs);
			?>
				<tr <?=($x%2 == 0)?' bgcolor="#f3f3f3"':''?>>
                    <td>
                        <a href="<?=ADMIN?>view-package.php?id=<?=$row['id']?>"><img src="<?=WWW?>images/icons/edit.png"></a>
                        <a href="<?=ADMIN?>packages.php?action=change&id=<?=$row['id']?>" title="Change Status">
                        <?
                          if($row['status'] == '1'){
                            $img = 'status.png';
                            $alt = 'Active Record';
                          }else if($row['status'] == '0'){
                            $img = 'in-status.png';
                            $alt = 'Inactive Record';
                          }else if($row['status'] == DELETED_STATUS){
                            $img = 'restore.png';
                            $alt = 'Deleted Record';
                          }
                        ?>
                        <img src="<?=WWW?>images/icons/<?=$img?>" alt="<?=$alt?>" width="16" />
                      </a>
                      <a href="<?=ADMIN?>packages.php?action=delete&id=<?=$row['id']?>" title="Delete Record">
                        <img src="<?=WWW?>images/icons/delete.png" alt="Delete" />
                      </a>
                      <a href="<?=ADMIN?>package-rules.php?id=<?=$row['id']?>" title="Package Rules">
                        <img src="<?=WWW?>images/icons/options.png" alt="Package Rules" width="16" />
                      </a>
                      </td>
                    <td><?=$row['title']?></td>
                    <td><?=$row['duration']?></td>
                    <td><?=$row['price']?></td>
                </tr>
			<?
				$i++;
				$count++;
				$x++;
			}	
		?>
</table>
<? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_ADMIN){ ?>
    <div id="pagination-bottom">
      <?php
        $keywords = array();
        foreach($_GET as $key=>$value){
          $keywords[] = $key.'='.$value;
        }
        $reload = 'packages.php?'.implode('&',$keywords).'&';
        echo paginate_one($reload, $ppage, $tpages);
      ?>
      <input type="hidden" name="pagination-page" value="packages">
        </div>  
        <? } ?>       
    <div class="clear"></div>
</div>
<!--**********************************-->
<? include('common/footer.php');?>