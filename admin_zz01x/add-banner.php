<? include('common/header.php');?>	
<script type="text/javascript" src="<?php echo WWW;?>js/jquery-ui.min.js"></script>
<link type="text/css" rel="stylesheet" href="<?php echo WWW;?>css/jquery-ui.css">
<script>
$(document).ready(function(){
	$("#start_date, #expiry_date").datepicker({changeYear:"true",yearRange: "1980:2020"});
    $("#start_date, #expiry_date").datepicker({changeYear:"true",minDate:0});
});
</script>
<!--**********************************-->
<div class="box1">
	<h1>Add Banner</h1>
    	<?	//==========================
			if(isset($_POST['action']) && $_POST['action'] == 'submit'){
				
				$title		= $_POST['title'];
				$admin_note	= $_POST['admin_note'];
				$placement	= $_POST['banner_placement_id'];
				$user_info	= $_POST['user_info'];
				$start_date	= date_format_sql($_POST['start_date']);
				$expiry_date= date_format_sql($_POST['expiry_date']);
				$date		= date('Y-m-d');
				$url	= $_POST['url'];

				if(strtotime($_POST['expiry_date']) < strtotime($start_date)){
					echo '<div id="error"><b>Failure : </b>Invalid Expiry date... !</div>';
				}else{
					$rs_chk = mysqli_query($conn,"select * from banners where title = '$title' and banner_placement_id = '$placement' and start_date = '$start_date' and expiry_date = '$expiry_date'");
					if(mysqli_num_rows($rs_chk) == 0){
						$query		= "insert into banners (title,admin_note,banner_placement_id,user_info,start_date,expiry_date,create_date,status) 
									values ('$title','$admin_note','$placement','$user_info','$start_date','$expiry_date','$date','1')";
						
						if(mysqli_query($conn,$query)){
							$id	= mysqli_insert_id($conn);
							
							if($_POST['type'] == 'img'){
								if(!is_dir('../ads/'.$id)){
									mkdir('../ads/'.$id,0777);
								}
								
								if(!empty($_FILES['banner']['name'])){
									$filename 	= urlencode($_POST['title']).'.png';
									$image 		= new SimpleImage();
									$image->load($_FILES["banner"]["tmp_name"]);
									$image->save('../ads/'.$id.'/'.$filename);
									$content = 'img_______'.$filename.'_______'.$url;
									mysqli_query($conn,"update banners set content = '$content' where id= '$id'");
								}
							}else{
								$content = 'code_______'.$_POST['banner'];
								mysqli_query($conn,"update banners set content = '$content' where id= '$id'");
							}
							
							
							echo '<div id="success"><b>Success : </b>Information Successfully Added ... !</div>';
						}else{
							echo '<div id="error"><b>Failure : </b>Invalid information ... !</div>';
						}
					}else{
						echo '<div id="error"><b>Failure : </b>Information Already Exists ... !</div>';
					}
				}
			}
			//==========================
		?>
        <div style="clear:both; height:30px;"></div>
	<form id="frm-add" method="post" action="add-banner.php" enctype="multipart/form-data" >
    <input type="hidden" name="action" value="submit" />
	<table id="detail">
		
        <tr>
			<td width="25%">Title : </td>
			<td><input type="text" name="title" class="validate[required] txt-feild-small" value="<?=$title?>" /></td>
		</tr>
        <tr>
			<td width="25%">Placement: </td>
			<td><?=get_combo('banner_placements','name','', 'banner_placement_id','','','','validate[required]')?></td>
		</tr>
        
        <tr>
			<td width="25%">User Information : </td>
			<td><textarea cols="" rows="" name="user_info" class="txt-feild-large validate[required]" style="height:150px;"></textarea></td>
		</tr>
        <tr>
			<td width="25%">Type : </td>
			<td><input type="radio" name="type" value="img" checked="checked" /> Banner + URL<br />
            	<input type="radio" name="type" value="code" /> AdSense / Flash Code
            </td>
		</tr>
        <tr class="bannerurl">
			<td width="25%">Banner Image : </td>
			<td><input type="file" name="banner" />     </td>
		</tr>
        <tr class="bannerurl">
			<td width="25%">URL : </td>
			<td><input type="text" name="url" class="txt-feild-small" />     </td>
		</tr>
        <tr class="code" style="display:none;">
			<td width="25%">Banner Code : </td>
			<td><textarea name="banner" class="txt-feild-large" style="height:130px;"/></textarea></td>
		</tr>
        <tr>
        	<td>Start Date : </td>
            <td><input id="start_date" readonly="readonly" class="txt-feild-small validate[required]" name="start_date"></td>
        </tr>
        <tr>
        	<td>Expiry Date : </td>
            <td><input id="expiry_date" readonly="readonly" class="txt-feild-small validate[required]" name="expiry_date"></td>
        </tr>
         <tr>
			<td width="25%">Admin Note : </td>
			<td>
            	<textarea cols="100" rows="10" class="txt-feild-large" style="height:150px;" name="admin_note"></textarea>
            </td>
		</tr>
        <tr>
        	<td colspan="2" align="center"><input type="submit" value=" Save " class="btn1" /></td>
        </tr>
	</table>

	</form>

	<div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>banners.php" title="Go to banner">Back</a></div>
	<div class="clear"></div>
<script>
$(document).ready(function(){
	$('input[name=type]').click(function(){
		if(this.value == 'img'){
			$('tr.bannerurl').fadeIn();
			$('tr.bannerurl input').addClass('validate[required]');
			$('tr.bannerurl input').addClass('validate[url]');
			
			$('tr.code').fadeOut();
			$('tr.bannerurl input').removeClass('validate[required]');
		}else{
			$('tr.code').fadeIn();
			$('tr.bannerurl input').addClass('validate[required]');
			
			$('tr.bannerurl').fadeOut();
			$('tr.bannerurl input').removeClass('validate[required]');
			$('tr.bannerurl input').removeClass('validate[url]');
		}
	});
});
</script>
<!--***********************************-->
<? include('common/footer.php')?>