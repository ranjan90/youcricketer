<? include('common/header.php');?>
<!--**********************************-->
    <h1>User Permissions</h1>
    <?
      if(isset($_POST['action']) && $_POST['action'] == 'submit'){

        mysqli_query($conn,"truncate modules_to_users");

        foreach($_POST['rights'] as $key=>$r){
          $user_type_id = $key;
          foreach($r as $mr){
            $module_right_id = $mr;
            mysqli_query($conn,"insert into modules_to_users (module_right_id, user_type_id) values ('$mr','$user_type_id');");
          }
        }
        echo '<div id="success">User Permissions updated ... !</div>';
      }
    ?>

    <form method="post" action="<?=ADMIN?>user-permissions.php" id="user_permissions">
      <input type="hidden" name="action" value="submit">
    <table width="100%" class="grid" border="1" style="border-collapse:collapse;">
      <?
         $rs_ut = mysqli_query($conn,"select * from user_types");
         $count_ut = mysqli_num_rows($rs_ut);
      ?>
      <tr>
        <th width="10%"></th>
        <? $utypes = array();
           while($row_ut = mysqli_fetch_assoc($rs_ut)){ ?>
            <th style="font-size:12px; padding:5px;text-align:center;"><?=$row_ut['name']?></th>
        <?  $utypes[] = $row_ut['id'];
            } ?>
      </tr>
      <? $rs_m = mysqli_query($conn,"select * from modules");
         while($row_m = mysqli_fetch_assoc($rs_m)){ ?>
         <tr>
          <td colspan="<?=$count_ut+=1?>" class="heading"><?=ucwords($row_m['name'])?></td>
         </tr>
         <? $rs_rights = mysqli_query($conn,"select * from module_rights where module_id = '".$row_m['id']."'");
            while($row_rights = mysqli_fetch_assoc($rs_rights)){ ?>
            <tr>
              <td>&nbsp; -> <?=ucwords($row_rights['name'])?> Record</td>
              <? foreach($utypes as $uts){
                  $row_mtr = mysqli_fetch_assoc(mysqli_query($conn,"select * from modules_to_users where user_type_id = '$uts' and module_right_id = '".$row_rights['id']."'"));
                  ?>
              <td>
                <center>
                  <input type="checkbox" <?=($row_m['id'] == '46')?'readonly="readonly"':'';?> <?=($row_mtr)?'checked="checked"':'';?> name="rights[<?=$uts?>][]" value="<?=$row_rights['id']?>">
                </center>
              </td>
              <? } ?>
            </tr>
         <? } ?>
      <? } ?>
    </table>
    <br>
    <input type="submit" value="Update" class="btn1" style="float:right;">
    </form>
    <div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>home.php" title="Go to Home">Back</a></div>
	<div class="clear"></div>
  </div>
  <style>
    .grid th{height:25px !important;}
    .grid td.heading{font-size: 15px; background:#efefef; color:#000;}
  </style>
<!--**********************************-->
<? include('common/footer.php');?>
