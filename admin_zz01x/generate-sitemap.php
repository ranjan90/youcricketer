<? include('common/header.php'); ?>
<!--**********************************-->
<div class="box1">
	<h1>Generate Sitemap</h1>
    	<?	ini_set('max_execution_time', -1);
    		//==========================
    		$records_per_page_front		= get_record_on_id('settings',4);
    		define('PRODUCT_LIMIT_FRONT',$records_per_page_front['value'],true);
    		function writeSitemap($filename, $urls = array()){
    			$y = 0;
    			
    			$noOfFiles = round(ceil(count($urls)/49900));
    			if($noOfFiles > 1){
	    			for($x = 1; $x <= $noOfFiles; $x++){
	    				if($x != 1){
	    					$startLimit = ($x-1)*49900 + 1;
	    				}else{
	    					$startLimit = ($x-1)*49900;	
	    				}
	    				$endLimit 	= ($x)*49900;
	    				
		    			$output[$x] = '<?xml version="1.0" encoding="UTF-8"?>
							<urlset  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
			    		for($y = $startLimit; $y <= $endLimit; $y++){
			    			if(!empty($urls[$y])){
			    				$output[$x] .= '	<url><loc>'.$urls[$y].'</loc></url>
		';
							}
			    		}
						$output[$x] .= '</urlset>';

						$myFile = "../sitemaps/".$filename."-".$x.".xml";
						$fh = fopen($myFile, 'w') or die("Can't open file");
						if(fwrite($fh, $output[$x])){
							echo '<div id="success">File Generated with '.count($urls).' links ... !</div>';
						}
						fclose($fh);
						chmod($myFile, 0777);
					}
				}else{
					$output = '<?xml version="1.0" encoding="UTF-8"?>
							<urlset  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
			    		foreach($urls as $url){
			    				$output .= '	<url><loc>'.$url.'</loc></url>
		';
			    		}
						$output .= '</urlset>';

						$myFile = "../sitemaps/".$filename.".xml";
						$fh = fopen($myFile, 'w') or die("Can't open file");
						if(fwrite($fh, $output)){
							echo '<div id="success">File Generated with '.count($urls).' links ... !</div>';
						}
						fclose($fh);
						chmod($myFile, 0777);
				}
			}
    		//==========================
    		if($_GET['type'] == 'index'){
    			$urls 	= array();
    			$dir 	= "../sitemaps/";
				$dh 	= opendir($dir);
				$output = '<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
				$x = 0;
				while(($file = readdir($dh)) !== false) {
					if (is_file($dir.'/'.$file) && $file != "." && $file != "..") {
						$output .= '<sitemap>
      <loc>'.WWW.'sitemaps/'.$file.'</loc>
      <lastmod>'.date('Y-m-d').'</lastmod>
   </sitemap>';
					}
					$x++;
				}
				$output .= '</sitemapindex>';
				$myFile = "../sitemap_index.xml";
				$fh = fopen($myFile, 'w') or die("Can't open file");
				if(fwrite($fh, $output)){
					echo '<div id="success">File Generated with '.$x.' links ... !</div>';
				}
				fclose($fh);
				chmod($myFile, 0777);
    		}
    		if($_GET['type'] == 'news'){
    			$urls 	= array();
    			$urls[] 	= WWW.'news.html';
    			$rsTotalRecords 	= mysqli_query($conn,"select * from news where status = '1' ");
				$NumRowsRecords 	= ceil(mysqli_num_rows(mysqli_query($conn,"select * from news where status = '1' "))/PRODUCT_LIMIT_FRONT);
				for($x = 2; $x <= $NumRowsRecords; $x++){
					$urls[] 	= WWW.'news.html?page='.$x;
				}
				while($rowMaxRecords = mysqli_fetch_assoc($rsTotalRecords)){
					$urls[] 	= WWW.'news-detail-'.$rowMaxRecords['id'].'-'.friendlyURL($rowMaxRecords['title']).'.html';	
				}
				writeSitemap($_GET['type'], $urls);
    		}
    		if($_GET['type'] == 'press-releases'){
    			$urls 	= array();
    			$urls[] 	= WWW.'press-releases.html';
    			$rsTotalRecords 	= mysqli_query($conn,"select * from press_releases where status = '1' ");
				$NumRowsRecords 	= ceil(mysqli_num_rows(mysqli_query($conn,"select * from press_releases where status = '1' "))/PRODUCT_LIMIT_FRONT);
				for($x = 2; $x <= $NumRowsRecords; $x++){
					$urls[] 	= WWW.'press-releases.html?page='.$x;
				}
				while($rowMaxRecords = mysqli_fetch_assoc($rsTotalRecords)){
					$urls[] 	= WWW.'press-release-detail-'.$rowMaxRecords['id'].'-'.friendlyURL($rowMaxRecords['title']).'.html';	
				}
				writeSitemap($_GET['type'], $urls);
    		}
    		if($_GET['type'] == 'blog'){
    			$urls 	= array();
    			$urls[] 	= WWW.'blogs.html';
    			$rsTotalRecords 	= mysqli_query($conn,"select * from blog_articles where status = '1' ");
				$NumRowsRecords 	= ceil(mysqli_num_rows(mysqli_query($conn,"select * from blog_articles where status = '1' "))/PRODUCT_LIMIT_FRONT);
				for($x = 2; $x <= $NumRowsRecords; $x++){
					$urls[] 	= WWW.'blogs.html?page='.$x;
				}
				while($rowMaxRecords = mysqli_fetch_assoc($rsTotalRecords)){
					$urls[] 	= WWW.'blog-detail-'.$rowMaxRecords['id'].'-'.friendlyURL($rowMaxRecords['title']).'.html';	
				}
				writeSitemap($_GET['type'], $urls);
    		}
    		if($_GET['type'] == 'forum'){
    			$urls 	= array();
    			$urls[] 	= WWW.'forums.html';
    			$rsTotalRecords 	= mysqli_query($conn,"select * from forum_topics where status = '1' ");
				$NumRowsRecords 	= ceil(mysqli_num_rows(mysqli_query($conn,"select * from forum_topics where status = '1' "))/PRODUCT_LIMIT_FRONT);
				for($x = 2; $x <= $NumRowsRecords; $x++){
					$urls[] 	= WWW.'forums.html?page='.$x;
				}
				while($rowMaxRecords = mysqli_fetch_assoc($rsTotalRecords)){
					$urls[] 	= WWW.'forum-detail-'.$rowMaxRecords['id'].'-'.friendlyURL($rowMaxRecords['title']).'.html';	
				}
				writeSitemap($_GET['type'], $urls);
    		}
    		if($_GET['type'] == 'groups'){
    			$urls 	= array();
    			$urls[] 	= WWW.'groups.html';
    			$rsTotalRecords 	= mysqli_query($conn,"select * from forum_topics where status = '1' ");
				$NumRowsRecords 	= ceil(mysqli_num_rows(mysqli_query($conn,"select * from forum_topics where status = '1' "))/PRODUCT_LIMIT_FRONT);
				for($x = 2; $x <= $NumRowsRecords; $x++){
					$urls[] 	= WWW.'groups.html?page='.$x;
				}
				while($rowMaxRecords = mysqli_fetch_assoc($rsTotalRecords)){
					$urls[] 	= WWW.'group-detail-'.$rowMaxRecords['id'].'-'.friendlyURL($rowMaxRecords['title']).'.html';	
				}
				writeSitemap($_GET['type'], $urls);
    		}
    		if($_GET['type'] == 'groups'){
    			$urls 	= array();
    			$urls[] 	= WWW.'groups.html';
    			$rsTotalRecords 	= mysqli_query($conn,"select * from forum_topics where status = '1' ");
				$NumRowsRecords 	= ceil(mysqli_num_rows(mysqli_query($conn,"select * from forum_topics where status = '1' "))/PRODUCT_LIMIT_FRONT);
				for($x = 2; $x <= $NumRowsRecords; $x++){
					$urls[] 	= WWW.'groups.html?page='.$x;
				}
				while($rowMaxRecords = mysqli_fetch_assoc($rsTotalRecords)){
					$urls[] 	= WWW.'group-detail-'.$rowMaxRecords['id'].'-'.friendlyURL($rowMaxRecords['title']).'.html';	
				}
				writeSitemap($_GET['type'], $urls);
    		}
    		if($_GET['type'] == 'testimonials'){
    			$urls 	= array();
    			$urls[] 	= WWW.'testimonials.html';
    			$rsTotalRecords 	= mysqli_query($conn,"select * from testimonials where status = '1' ");
				$NumRowsRecords 	= ceil(mysqli_num_rows(mysqli_query($conn,"select * from testimonials where status = '1' "))/PRODUCT_LIMIT_FRONT);
				for($x = 2; $x <= $NumRowsRecords; $x++){
					$urls[] 	= WWW.'testimonials.html?page='.$x;
				}
				writeSitemap($_GET['type'], $urls);
    		}
    		if($_GET['type'] == 'clubs'){
    			$urls 	= array();
    			$urls[] 	= WWW.'clubs.html';
    			$rsTotalRecords 	= mysqli_query($conn,"select * from clubs where status = '1' ");
				$NumRowsRecords 	= ceil(mysqli_num_rows(mysqli_query($conn,"select * from clubs where status = '1' "))/PRODUCT_LIMIT_FRONT);
				for($x = 2; $x <= $NumRowsRecords; $x++){
					$urls[] 	= WWW.'groups.html?page='.$x;
				}
				while($rowMaxRecords = mysqli_fetch_assoc($rsTotalRecords)){
					$urls[] 	= WWW.'club-detail-'.$rowMaxRecords['id'].'-'.friendlyURL($rowMaxRecords['title']).'.html';	
				}
				writeSitemap($_GET['type'], $urls);
    		}
    		if($_GET['type'] == 'leagues'){
    			$urls 	= array();
    			$urls[] 	= WWW.'leagues.html';
    			$rsTotalRecords 	= mysqli_query($conn,"select * from leagues where status = '1' ");
				$NumRowsRecords 	= ceil(mysqli_num_rows(mysqli_query($conn,"select * from leagues where status = '1' "))/PRODUCT_LIMIT_FRONT);
				for($x = 2; $x <= $NumRowsRecords; $x++){
					$urls[] 	= WWW.'leagues.html?page='.$x;
				}
				while($rowMaxRecords = mysqli_fetch_assoc($rsTotalRecords)){
					$urls[] 	= WWW.'league-detail-'.$rowMaxRecords['id'].'-'.friendlyURL($rowMaxRecords['title']).'.html';	
				}
				writeSitemap($_GET['type'], $urls);
    		}
    		if($_GET['type'] == 'events'){
    			$urls 	= array();
    			$urls[] 	= WWW.'events.html';
    			$rsTotalRecords 	= mysqli_query($conn,"select * from events where status = '1' ");
				$NumRowsRecords 	= ceil(mysqli_num_rows(mysqli_query($conn,"select * from events where status = '1' "))/PRODUCT_LIMIT_FRONT);
				for($x = 2; $x <= $NumRowsRecords; $x++){
					$urls[] 	= WWW.'events.html?page='.$x;
				}
				while($rowMaxRecords = mysqli_fetch_assoc($rsTotalRecords)){
					$urls[] 	= WWW.'event-detail-'.$rowMaxRecords['id'].'-'.friendlyURL($rowMaxRecords['title']).'.html';	
				}
				writeSitemap($_GET['type'], $urls);
    		}
    		if($_GET['type'] == 'individuals'){
    			$urls 	= array();
    			$urls[] 	= WWW.'individuals.html';
    			$rsTotalRecords 	= mysqli_query($conn,"select * from users where is_company = '0' and status = '1' ");
				$NumRowsRecords 	= ceil(mysqli_num_rows(mysqli_query($conn,"select * from events where is_company = '0' and status = '1' "))/PRODUCT_LIMIT_FRONT);
				for($x = 2; $x <= $NumRowsRecords; $x++){
					$urls[] 	= WWW.'individuals.html?page='.$x;
				}
				while($rowMaxRecords = mysqli_fetch_assoc($rsTotalRecords)){
					$urls[] 	= WWW.'individual-detail-'.$rowMaxRecords['id'].'-'.friendlyURL($rowMaxRecords['f_name'].' '.$rowMaxRecords['last_name']).'.html';	
				}
				writeSitemap($_GET['type'], $urls);
    		}
    		if($_GET['type'] == 'companies'){
    			$urls 	= array();
    			$urls[] 	= WWW.'companies.html';
    			$rsTotalRecords 	= mysqli_query($conn,"select * from users where is_company = '1' and status = '1' ");
				$NumRowsRecords 	= ceil(mysqli_num_rows(mysqli_query($conn,"select * from events where is_company = '1' and status = '1' "))/PRODUCT_LIMIT_FRONT);
				for($x = 2; $x <= $NumRowsRecords; $x++){
					$urls[] 	= WWW.'companies.html?page='.$x;
				}
				while($rowMaxRecords = mysqli_fetch_assoc($rsTotalRecords)){
					$rowCompany = mysqli_fetch_assoc(mysqli_query($conn,"select * from companies where user_id = '".$rowMaxRecords['id']."'"));
					if($rowCompany){
						$urls[] 	= WWW.'company-detail-'.$rowCompany['id'].'-'.friendlyURL($rowCompany['company_name']).'.html';		
					}					
				}
				writeSitemap($_GET['type'], $urls);
    		}
    		
			
			//==========================
		?>
        <div style="clear:both; height:30px;"></div>
	<?php 	$entities = array('individuals', 'companies', 'news', 'press-releases', 'blog', 'forum', 'groups', 'testimonials', 
	'clubs', 'leagues','events');?>
	<table width="40%">
		<tr>
			<th>Action</th>
			<th>Entities</th>
			<th>File Name(s)</th>
			<th>Last Generated</th>
			
		</tr>
		<?php foreach($entities as $e){  ?>		
			<tr>
				<td><a href="generate-sitemap.php?type=<?php echo $e;?>">Generate</a></td>
				<td><?php echo ucwords($e);?></td>
				<td>
					<?php
					
					?>
				</td>
				<td>
					<?php 	if(file_exists('../sitemaps/'.$e.'.xml')){
								echo date('Y-m-d H:i:s', filemtime('../sitemaps/'.$e.'.xml'));
							}else if(file_exists('../sitemaps/'.$e.'-1.xml')){
								echo date('Y-m-d H:i:s', filemtime('../sitemaps/'.$e.'-1.xml'));
							}else{
								echo 'Not Generated';
							}
					?>
				</td>
			</tr>
		<?php } ?>
		<tr>
			<td colspan="3" align="right"><a href="generate-sitemap.php?type=index">Generate Index File</a></td>
		</tr>		
	</table>

	<div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>home.php">Back</a></div>
<style>
	th{background: #555; color:#fff;}
	th, td{font-size:15px; line-height: 25px;}
	td a{color: blue; text-decoration: underline;}
</style>
<!--***********************************-->
<? include('common/footer.php')?></urlset>