<? include('common/header.php');?>	
<!--**********************************-->
<div class="box1">
	<h1>Add User</h1>
	<?	
		if($_POST['action'] == 'submit'){

			$f_name 	= $_POST['f_name'];
			$s_name 	= $_POST['s_name'];
			$l_name 	= $_POST['l_name'];
			$gender 	= $_POST['gender'];
			$user_type_id=$_POST['user_type_id'];
			$email 		= $_POST['email'];
			$status 	= $_POST['status'];
			$password 	= $_POST['password'];
			$epassword  = md5($password);
			
			$query = "insert into users (f_name, m_name, last_name, gender, user_type_id, email, password, status)
					values ('$f_name','$m_name','$l_name','$gender','$user_type_id','$email','$epassword','1');";
			//echo $query;exit;

			if(mysqli_query($conn,$query)){
				echo '<div id="success">Information Updated ... !</div>';
				?>
					<script>
					window.location = '<?=ADMIN?>users.php?type=i';
					</script>
					<?
			}else{
				echo '<div id="error">Information already exists ... !</div>';
			}
		}	
	?>
	<div style="clear:both; height:30px;"></div>
	<form id="frm-add" method="post" action="<?=ADMIN?>add-user.php">
    <input type="hidden" name="action" value="submit" />
	<table width="41%"  id="detail">
		<tr>
			<td width="25%">First Name : </td>
			<td><input type="text" name="f_name" class="validate[required] txt-feild-small" /></td>
		</tr>
		<tr>
			<td width="25%">Middle Name : </td>
			<td><input type="text" name="m_name" class="txt-feild-small" /></td>
		</tr>
		<tr>
			<td width="25%">Last Name : </td>
			<td><input type="text" name="l_name" class="txt-feild-small" /></td>
		</tr>
        <tr>
			<td width="25%">Gender : </td>
			<td><?=get_gender_combo();?></td>
		</tr>
		<tr>
			<td width="25%">User Type : </td>
			<td><?=get_combo('user_types','name','','user_type_id');?></td>
		</tr>
		<tr>
			<td width="25%">Email Address: </td>
			<td><input type="text" name="email" class="txt-feild-small validate[required, custom[email]]"></td>
		</tr>
		<tr>
			<td width="25%">Password: </td>
			<td><input type="password" name="password" class="txt-feild-small validate[required]"></td>
		</tr>
		<tr>
			<td width="25%">Status : </td>
			<td><?=get_status_combo();?></td>
		</tr>
        <tr>
        	<td colspan="2" align="center"><input type="submit" value="Save"class="btn1" /></td>
        </tr>
	</table>

</form>

	<div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>users.php">Back</a></div>

<!--***********************************-->
<? include('common/footer.php')?>