<?
    $url	= $_SERVER['REQUEST_URI'];
	$url	= preg_split('/',$url);
	$url	= $url[count($url)-1];

	if(preg_split('.php',$url)){
		$url = preg_split('.php',$url);
		$url = $url[0];
	}
?>
<div class="menu2">
<ul>
	<li><a href="<?php echo ADMIN?>home.php" <?php echo ($url == 'home' || empty($url))?'class="selected"':''?>>Home</a></li>
    <li><a href="#" <?php echo ($url == 'settings' || empty($url))?'class="selected"':''?>>Configuration</a>
        <ul>
            <li><a href="<?php echo ADMIN?>countries.php">Locations</a></li>
            <li><a href="<?php echo ADMIN?>settings.php">Settings</a></li>
            <li><a href="<?php echo ADMIN?>cms.php">CMS Pages</a></li>
			<li><a href="<?php echo ADMIN?>how-tos.php">How To Pages</a></li>
            <li><a href="<?php echo ADMIN?>user-types.php">User Types</a></li>
            <li><a href="<?php echo ADMIN?>banner-placements.php">Banner Placements</a></li>
            <li><a href="<?php echo ADMIN?>generics.php">Generics</a></li>
            <li><a href="<?php echo ADMIN?>market-place-item.php">Market Place Items</a></li>
            <li><a href="<?php echo ADMIN?>payments.php">Payments</a></li>
            <li><a href="<?php echo ADMIN?>packages.php">Packages</a></li>
            <li><a href="<?php echo ADMIN?>banners.php">Advertisment Banners</a></li>
            <li><a href="<?php echo ADMIN?>right-side-advertisement.php">Right Side Advertisement Banners</a></li>
            <li><a href="<?php echo ADMIN?>small-sliding-banners.php">Small Sliding Banners</a></li>
            <li><a href="<?php echo ADMIN?>sliding-banners.php">Sliding Banners</a></li>
            <li><a href="<?php echo ADMIN?>home-page-videos.php">Home Page Videos</a></li>
            <li><a href="<?php echo ADMIN?>company-types.php">Organization Types</a></li>
            <li><a href="<?php echo ADMIN?>admin-emails.php">Admin Emails</a></li>
            <li><a href="<?php echo ADMIN?>front-end-stats.php">Frontend Stats</a></li>
            <li><a href="<?php echo ADMIN?>bottom-boxes-photos.php">Bottom Boxes Photos</a></li>
            <li><a href="<?php echo ADMIN?>growings.php">Manage Growth</a></li>
        </ul>
    </li>
    <li><a href="#">Community</a>
        <ul>
            <li><a href="<?php echo ADMIN?>blog-articles.php">Blog Articles</a></li>
            <li><a href="<?php echo ADMIN?>forum-topics.php">Forum Topics</a></li>
            <li><a href="<?php echo ADMIN?>news.php">News</a></li>
            <li><a href="<?php echo ADMIN?>faqs.php">FAQs</a></li>
            <li><a href="<?php echo ADMIN?>events.php">Events</a></li>
            <li><hr></li>
            <li><a href="<?php echo ADMIN?>blog-categories.php">Blog Categories</a></li>
            <li><a href="<?php echo ADMIN?>forum-categories.php">Forum Categories</a></li>
            <li><a href="<?php echo ADMIN?>news-categories.php">News Categories</a></li>
            <li><a href="<?php echo ADMIN?>faqs-categories.php">FAQs Categories</a></li>
        </ul>
    </li>
    <li><a href="#">Discussions</a>
        <ul>
            <li><a href="<?php echo ADMIN?>groups.php">Groups</a></li>
            <li><a href="<?php echo ADMIN?>clubs.php">Clubs</a></li>
            <li><a href="<?php echo ADMIN?>leagues.php">Leagues</a></li>
            <li><a href="<?php echo ADMIN?>press-releases.php">Press Releases</a></li>
        </ul>
    </li>
    <li><a href="<?php echo ADMIN?>users.php">Users</a>
        <ul>
            <li><a href="<?php echo ADMIN?>users.php">Individuals</a></li>
            <li><a href="<?php echo ADMIN?>companies.php">Organizations</a></li>            
            <li><a href="<?php echo ADMIN?>newsletter-users.php">Newsletter Users</a></li>
            <li><a href="<?php echo ADMIN?>users.php?status=5">Closed Accounts</a></li>
            <li><a href="<?php echo ADMIN?>messages.php">Messages</a></li>
            <li><a href="<?php echo ADMIN?>team.php">Our Team</a></li>
        </ul>
    </li>
    <li><a href="<?php echo ADMIN?>email-templates.php">Newsletters</a>
        <ul>
            <li><a href="<?php echo ADMIN?>email-templates.php">Newsletter Template</a></li>
            <li><a href="<?php echo ADMIN?>add-newsletter.php">Create Newsletter</a></li>
            <li><a href="<?php echo ADMIN?>already-sent.php">Already Sent</a></li>
        </ul>
    </li>
    <li><a href="<?=ADMIN?>testimonials.php">Testimonials</a></li>
    <li><a href="<?=ADMIN?>contact-requests.php">Contact Requests</a></li>
    <li><a href="<?=ADMIN?>searches.php">Searches</a></li>
    <li><a href="#">Reports</a></li>
     <li><a href="<?php echo ADMIN;?>series.php">Series</a></li>
    <li style="float:right; background:#515151; "><a href="#">Super Admin</a>
        <ul style="right:0px;">
			<li><a href="<?php echo ADMIN?>generate-rss.php">Generate RSS</a></li>
            <li><a href="<?php echo ADMIN?>generate-sitemap.php">Generate Sitemap</a></li>
            <li><a href="<?php echo ADMIN?>user-permissions.php">User Permission</a></li>
        </ul>
    </li>
    <div class="clear"></div>
</ul>
</div> <!--menu2 -->
