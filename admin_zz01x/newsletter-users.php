<? include('common/header.php');?>
<!--**********************************-->
<div class="box1">
<h1>Newsletter Users </h1>
<table width="99%"  class="grid" border="0">
<tr>
<td width="751">
<div id="options">
     <div id="box" style="width:500px;">
        <form method="get" action="<?=ADMIN?>newsletter-users.php">
            <input type="hidden" name="action" value="search" />
            <input type="text" name="searchtxt" class=" txt-feild-small"  placeholder="Type here" />
            <?=get_status_combo()?>
            <input type="submit" value="Go" />
        </form>
    </div>
    </td>
   <td width="326"></td>
</tr>
</table>
  <div class="clear"></div>
  <form method="post" id="multiple">
    <input type="hidden" name="action" value="multiple">
    <table class="grid" cellspacing="0" cellpadding="0" width="100%">
      <tr>
        <th width="15%">Action</th>
        <th width="15%">Name</th>
        <th width="7%">Email</th>
        <th width="7%">Gender</th>
        <th width="7%">UserRole</th>
        <th width="15%">Location</th>
        <th width="15%">Email</th>
        <th width="18%">Date of Birth</th>
      </tr>

      <?  $rpp = PRODUCT_LIMIT_ADMIN; // results per page
            $ppage = intval($_GET["page"]);
      if($ppage<=0) $ppage = 1;

      $select = "select u.* from users u";
      //$where  = " where f_name = '' ";
      $where  = " where is_newsletter = '1' ";
      //=======================================
      if(isset($_GET['action']) && $_GET['action'] == 'search'){
        if(!empty($_GET['searchtxt'])){
          $where  .= "and email like '%".$_GET['searchtxt']."%'";
        }
        if(!empty($_GET['status']) && $_GET['status'] != ''){
          $where  .= "and u.status = '".$_GET['status']."'";
        }
      }
      $where .= " and u.status <> '".DELETED_STATUS."'";

      $query = $select.$join.$where." order by u.id desc";
      //echo $query;
      //=======================================
      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
                echo '<tr><td colspan="5">Your search returned no results</td></tr>';
            }
      $rs   = mysqli_query($conn,$query);
      $tcount = mysqli_num_rows($rs);
      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
      $count = 0;
      $i = ($ppage-1)* $rpp;
          $x = 0;
      while(($count<$rpp) && ($i<$tcount)){
                mysqli_data_seek($rs,$i);
                $row = mysqli_fetch_array($rs);
      ?>
      <tr <?=($x%2 == 0)?' bgcolor="#f3f3f3"':''?>>
        <td>
          <input type="checkbox" name="ids[<?=$row['id']?>]">
          <a href="<?=ADMIN?>users.php?<?=($row['status'] == DELETED_STATUS)?'show_deleted=1&':'';?>action=change&id=<?=$row['id']?>" title="Change Status">
                        <?
                          if($row['status'] == '1'){
                            $img = 'status.png';
                            $alt = 'Active Record';
                          }else if($row['status'] == '0'){
                            $img = 'in-status.png';
                            $alt = 'Inactive Record';
                          }else if($row['status'] == DELETED_STATUS){
                            $img = 'restore.png';
                            $alt = 'Deleted Record';
                          }
                        ?>
                        <img src="<?=WWW?>images/icons/<?=$img?>" alt="<?=$alt?>" width="16" />
                      </a>
          <a href="<?=ADMIN?>users.php?action=delete&id=<?=$row['id']?>" title="Delete Record">
                <img src="<?=WWW?>images/icons/delete.png" alt="Delete" />
            </a>
          <a href="<?=ADMIN?>view-user.php?id=<?=$row['id']?>" title="Record Details">
            <img src="<?=WWW?>images/icons/edit.png" alt="Record Details">
          </a>
          <a href="<?=ADMIN?>users.php?type=<?=$_GET['type']?>&action=feature&id=<?=$row['id']?>" title="Make Featured">
              <img src="<?=WWW?>images/icons/<?=($row['is_featured'] == '1')?'featured-on':'featured-off'?>.png" alt="Featured" />
          </a>
          <a href="<?=WWW?>individual-detail-<?=$row['id']?>-<?=friendlyURL($row['f_name'].' '.$row['last_name'])?>.html" target="_blank" title="View Profile">
              <img src="<?=WWW?>images/icons/view.png" alt="View" />
          </a>
          <a href="<?=ADMIN?>messages.php?id=<?=$row['id']?>" title="Messages">
            <img src="<?=WWW?>images/icons/mail.png" alt="Messages">
          </a>
          <a href="<?=ADMIN?>make-card.php?id=<?=$row['id']?>" title="Make Card">
              <img src="<?=WWW?>images/icons/card.png" alt="card" />
          </a>
        </td>
        <td><?=$row['f_name'].' '.$row['m_name'].' '.$row['last_name']?></td>
        <td><?=$row['email'];?></td>
        <td><?=get_gender_combo($row['gender'],'text')?></td>
        <td><?=get_combo('user_types','name',$row['user_type_id'],'','text')?></td>
        <td>
          <?php
          $row_city   = get_record_on_id('cities', $row['state_id']);
          $row_states = get_record_on_id('states', $row['state_id']);
          $row_country= get_record_on_id('countries', $row['country_id']);
          echo $row['city_name'].' > '.$row_states['name'].' > '.$row_country['name'];
          ?>
        </td>
        <td><?=$row['email']?></td>
        <td><?=($row['date_of_birth'] != '0000-00-00')?date_converter($row['date_of_birth']):'';?></td>
      </tr>
      <?
        $i++;
        $count++;
        $x++;
      }
    ?>
    <tr>
      <td>
        <input type="radio" name="checked" value="checked_all"> &nbsp; Checked All
      </td>
      <td>
        <input type="radio" name="checked" value="unchecked_all"> &nbsp; Unchecked All
      </td>
      <td>
        <select name="dd_status" class="dd1">
          <option value="" selected="selected">Select One</option>
          <option value="1">Activate Records</option>
          <option value="0">Deactivate Records</option>
          <option value="<?=DELETED_STATUS?>">Delete Records</option>
        </select>
      </td>
      <td>
        <input type="submit" value="Update">
      </td>
    </tr>
    </table>
<? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_ADMIN){ ?>
    <div id="pagination-bottom">
      <?php
        $keywords = array();
        foreach($_GET as $key=>$value){
          $keywords[] = $key.'='.$value;
        }
        $reload = 'newsletter-users.php?';
        echo paginate_one($reload, $ppage, $tpages);
      ?>
      <input type="hidden" name="pagination-page" value="users">
        </div>
        <? } ?>
    <div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>home.php" title="Go to Home">Back</a></div>

    <div class="clear"></div>
</div>
<!--*****************************-->
<? include('common/footer.php') ?>
