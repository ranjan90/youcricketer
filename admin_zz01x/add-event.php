<? include('common/header.php');?>	
<!--**********************************-->
<div class="box1">
	<h1>Add Event</h1>
    	<?	//==========================
			if(isset($_POST['action']) && $_POST['action'] == 'submit'){
				
				$title		= $_POST['title'];
				$content 	= $_POST['content'];
				$start_date = date_converter($_POST['start_date'],'Y-m-d');
				$end_date 	= date_converter($_POST['end_date'],'Y-m-d');
				$meta_keywords = $_POST['meta_keywords'];
				$meta_description = $_POST['meta_description'];
				$is_announcement = ($_POST['is_announcement'] == 'on')?'1':'0';
				$date 		= date('Y-m-d');
				$user_id 	= $_SESSION['ycdc_admin_dbuid'];

				$query		= "insert into events (is_announcement, title, content, start_date, end_date, meta_keywords, meta_description, user_id, create_date, is_featured, status)
				values ('$is_announcement', '$title','$content','$start_date','$end_date','$meta_keywords','$meta_description','$user_id','$date','0','1')";
				//echo $query;exit;
				if(mysqli_query($conn,$query)){
					echo '<div id="success"><b>Success : </b>Information Added ... !</div>';
					?>
						<script>
						window.location = '<?=ADMIN?>events.php';
						</script>
					<?
				}else{
					echo '<div id="error"><b>Failure : </b>Information already exists ... !</div>';
				}
			}
			//==========================
		?>
        <div style="clear:both; height:30px;"></div>
	<form id="frm-add" method="post" action="<?php echo ADMIN?>add-event.php" >
    <input type="hidden" name="action" value="submit" />
	<table id="detail">
		<tr>
			<td width="25%">Is Announcement / Live Streaming : </td>
			<td><input type="checkbox" name="is_announcement"></td>
		</tr>
		<tr>
			<td width="25%">Title : </td>
			<td><input type="text" name="title" class="txt-feild-small validate[required]"></td>
		</tr>
		<tr>
			<td>Content</td>
			<td><textarea class="validate[required] txt-feild-large" style="height:200px;" name="content"></textarea></td>
		</tr>
		<tr>
			<td>Start date</td>
			<td><input type="text" name="start_date" class="txt-feild-small validate[required] calender"></td>
		</tr>
		<tr>
			<td>End date</td>
			<td><input type="text" name="end_date" class="txt-feild-small validate[required] calender"></td>
		</tr>
		<tr>
			<td>Meta Keywords</td>
			<td><textarea class="validate[required] txt-feild-large" style="height:200px;" name="meta_keywords"></textarea></td>
		</tr>
		<tr>
			<td>Meta Description</td>
			<td><textarea class="validate[required] txt-feild-large" style="height:200px;" name="meta_description"></textarea></td>
		</tr>
        <tr>
        	<td colspan="2" align="center"><input type="submit" value=" Save" class="btn1" /></td>
        </tr>
	</table>
	</form>
	<div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>events.php" >Back</a></div>
    <div class="clear"></div>
<script type="text/javascript" src="<?=WWW?>js/jquery-ui.min.js"></script>
<link href="<?=WWW?>css/jquery-ui.css" type="text/css" rel="stylesheet">
<!--***********************************-->
<? include('common/footer.php')?>