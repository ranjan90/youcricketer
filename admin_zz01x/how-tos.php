<? include('common/header.php');?>
<!--**********************************-->
<div class="box1">
	<h1>How to Pages</h1>
	 <table width="99%"  class="grid" border="0">
<tr>


<td width="751">
<div id="options">
     <div id="box" style="width:500px;">
        <form method="get" action="<?=ADMIN?>how-tos.php">
            <input type="hidden" name="action" value="search" />
            <input type="text" name="searchtxt" class=" txt-feild-small"  placeholder="Type here" value="<?=$_GET['searchtxt']?>" />
            <?=get_status_combo_selected($_GET['status'])?>
            <input type="submit" value="Go" />
        </form>
    </div>
    </td>
   <td width="326"></td>
    <div id="box" class="text" style="width:280px;">
      <td width="117"><a class="btn1" href="<?=ADMIN?>how-tos.php">View All Pages</a></td>
      <td width="116"><a class="btn2" href="<?=ADMIN?>add-how-to.php" >Add Page</a></td>
      <td><a  href="<?=ADMIN?>how-tos.php?show_deleted=1" title="Show Deleted" class="btn1">Show Deleted</a></td>
      </div>

</tr>
</table>

                   <div class="clear"></div>
                    <?		$id			= $_GET['id'];
                            $row 		= get_record_on_id('how_tos',$id,'status');
                            $status		= $row['status'];

                            $status		= ($status == '1')?'0':'1';

                            if(isset($_GET['action']) && ($_GET['action'] == 'change' || $_GET['action'] == 'delete')){
                                if($_GET['action'] == 'change'){
                                    $query	= "update how_tos set status = '$status' where id = '$id'";
                                }else if($_GET['action'] == 'delete'){
																	$rc = mysqli_num_rows(mysqli_query($conn, "select id from how_tos where id=".$id." and status=".DELETED_STATUS));
																	if($rc > 0){
																		$query = "DELETE FROM `how_tos` WHERE id=".$id;
																	}else{
																		$query  = "update how_tos set status = '".DELETED_STATUS."' where id = '$id'";
																	}
                                }

                                if(mysqli_query($conn,$query)){
                                    echo '<div id="success">Information Updated ... !</div>';
                                }else{
                                    echo '<div id="error">Information cannot be updated ... !</div>';
                                }
                            }
														if(isset($_POST['action']) && $_POST['action'] == 'multiple'){
								              $ids = array_keys($_POST['ids']);
															if($_POST['dd_status']==8){
															  $counter = 0;
															  foreach($ids as $idr){
															    $rc = mysqli_num_rows(mysqli_query($conn, "select id from how_tos where id=".$idr." and status=".DELETED_STATUS));
															    if($rc > 0){
															      $query = "DELETE FROM `how_tos` WHERE id=".$idr;
															    }else{
															      $query  = "update how_tos set status = '".DELETED_STATUS."' where id = '$idr'";
															    }
															    mysqli_query($conn,$query);
															    $counter++;
															  }
															}else{
															  $counter = 0;
															  foreach($ids as $idr){
															      $query  = "update how_tos set status = '".$_POST['dd_status']."' where id = '$idr'";
															      mysqli_query($conn,$query);
															    $counter++;
															  }
															}
								              echo '<div id="success">'.$counter.' records updated ... !</div>';
								            }
                        ?>
                    <div class="clear"></div>
                    <div id="pagination-top"></div>
                    <form method="post" id="multiple">
    <input type="hidden" name="action" value="multiple">
         	 <table width="1218" class="grid">


            <tr>
                <th width="10%">Action</th>
            	<th width="10%">Title</th>
                <th width="30%">Description</th>
            </tr>

    	<?	$rpp = PRODUCT_LIMIT_ADMIN; // results per page
            $ppage = intval($_GET["page"]);

						$ppage = intval($_GET["page"]);

			if($ppage<=0){
				$ppage = 1;
				$start=0;
				$limit=PRODUCT_LIMIT_ADMIN;
			}else{
				$start=($ppage-1)*PRODUCT_LIMIT_ADMIN;
				$limit=$ppage*PRODUCT_LIMIT_ADMIN;
			}
			$date  = date('Y-m-d h:m:s');
			$query = "select * from how_tos where 1=1 ";
			//=======================================
			if(isset($_GET['action']) && $_GET['action'] == 'search'){
                if($_GET['searchtxt'] != ''){
                    $query  .= "and title like '%".$_GET['searchtxt']."%'";
                }
                if($_GET['status'] != ''){
                    $query .= " AND status LIKE '".$_GET['status']."%'";
                }
			}
			if(isset($_GET['show_deleted'])){
                $query .= " and status = '".DELETED_STATUS."'";
              }else{
                $query .= " and status <> '".DELETED_STATUS."'";
              }
              $query .= " order by id desc ";
              //echo $query;
			//=======================================
			if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
                echo '<tr><td colspan="5">Your search returned no results</td></tr>';
            }
			$rs 	= mysqli_query($conn,$query);
			$tcount = mysqli_num_rows($rs);
			$tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
			$count = 0;
			$i = ($ppage-1)* $rpp;
        	$x = 0;

			while(($count<$rpp) && ($i<$tcount)){
                mysqli_data_seek($rs,$i);
                $row = mysqli_fetch_array($rs);
			?>
				<tr <?=($x%2 == 0)?' bgcolor="#f3f3f3"':''?>>
                    <td>
                      <input type="checkbox" name="ids[<?=$row['id']?>]">
                        <a href="<?=ADMIN?>view-how-to.php?id=<?=$row['id']?>"><img src="<?=WWW?>images/icons/edit.png"></a>
                        <a href="<?=ADMIN?>how-tos.php?action=change&id=<?=$row['id']?>" title="Change Status">
                        <?
                          if($row['status'] == '1'){
                            $img = 'status.png';
                            $alt = 'Active Record';
                          }else if($row['status'] == '0'){
                            $img = 'in-status.png';
                            $alt = 'Inactive Record';
                          }else if($row['status'] == DELETED_STATUS){
                            $img = 'restore.png';
                            $alt = 'Deleted Record';
                          }
                        ?>
                        <img src="<?=WWW?>images/icons/<?=$img?>" alt="<?=$alt?>" width="16" />
                      </a>
											<a href="<?=ADMIN?>how-tos.php?action=delete&id=<?=$row['id'];?>"><img src="<?=WWW?>images/icons/delete.png" alt="Delete Record" width="16" /></a>
                      </td>
                    <td><?=$row['title']?></td>
                    <td><?php echo substr(strip_tags($row['content']),0,200);?></td>
                </tr>
			<?
				$i++;
				$count++;
				$x++;
			}
		?>
    <tr>
      <td>
        <input type="radio" name="checked" value="checked_all"> &nbsp; Checked All
      </td>
      <td>
        <input type="radio" name="checked" value="unchecked_all"> &nbsp; Unchecked All
      </td>
      <td>
        <select name="dd_status" class="dd1">
          <option value="" selected="selected">Select One</option>
          <option value="1">Activate Records</option>
          <option value="0">Deactivate Records</option>
          <option value="<?=DELETED_STATUS?>">Delete Records</option>
        </select>
        <input type="submit" value="Update">
      </td>
    </tr>
</table>
<? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_ADMIN){ ?>
    <div id="pagination-bottom">
      <?php
       $keywords = array();
        foreach($_GET as $key=>$value){
          $keywords[] = $key.'='.$value;
        }
				$param = "action=search&searchtxt=".$_GET['searchtxt']."&status=".$_GET['status']."&";
        $reload = 'how-tos.php?'.$param;
        echo paginate_one($reload, $ppage, $tpages);
      ?>
      <input type="hidden" name="pagination-page" value="how-tos">
			<input type="hidden" name="pagination-param" value="<?=$param?>">
        </div>
        <? } ?>
    <div class="clear"></div>
</div>
<!--**********************************-->


<? include('common/footer.php');?>
