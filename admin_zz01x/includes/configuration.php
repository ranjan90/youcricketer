<?	session_start();

	$user_ip= $_SERVER['REMOTE_ADDR'];

	include('../includes/path.php');
	include('../includes/connection.php');
	include('../includes/DX_Mailer.php');
	include('../includes/functions.php');
	include('../includes/timezone.php');
	include('../includes/SimpleImage.php');
	include('../includes/email-setting.php');
	include('pagination.php');

	extract($_POST);
	error_reporting(0);

	define('PAGE_TOKEN_ADMIN', PAGE_TOKEN + 1, true);
	define('ADMIN',WWW.'admin_zz01x/',true);

	$row_site_title				= get_record_on_id('settings',1);
	$records_per_page_admin		= get_record_on_id('settings',5);

	$reload = '';
	//=====================
	$url	= explode('/',$_SERVER['REQUEST_URI']);
	$url1	= explode('.', $url[PAGE_TOKEN_ADMIN]);
	$page   = $url1[0];

	$p_name	= '';
	for($x = 0; $x < strlen($page)-4;$x++){
		$p_name = $p_name . $page[$x];
	}
	//=====================
	$page_title		= $row_site_title['value'];
	define('PRODUCT_LIMIT_ADMIN',$records_per_page_admin['value'],true);
	define('DELETED_STATUS', '8', true);
	//=====================
?>
<!DOCTYPE HTML>
<html>
<head>
<title><?php echo $page_title?> (Admin Panel)</title>
<link href="<?php echo ADMIN?>css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo WWW?>css/validation-engine.css" rel="stylesheet" type="text/css" />
<script src="<?php echo WWW?>js/jquery-1.8.0.js" type="text/javascript"></script>
<script src="<?php echo WWW?>js/jquery.validationengine.js" type="text/javascript"></script>
<script src="<?php echo WWW?>js/validation-engine.js" type="text/javascript"></script>
<script src="<?php echo ADMIN?>js/site.js" type="text/javascript"></script>
<script type="text/javascript">
var WWW = '<?php echo WWW?>';
</script>
</head>
