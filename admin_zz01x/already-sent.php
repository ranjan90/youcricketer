<?php include('common/header.php');?>	
<!--**********************************-->
<div class="box1">
<h1>Already Sent Newsletters </h1>
    <table class="grid" cellspacing="0" cellpadding="0" width="100%">
      <tr>
        <th width="20%">Newsletter Title</th>
        <th width="25%">Sent Date</th>
        <th width="25%">Sent To</th>
      </tr>      
      
      <?  $rpp = PRODUCT_LIMIT_ADMIN; // results per page
            $ppage = intval($_GET["page"]);
      if($ppage<=0) $ppage = 1;
      $query = 'select n.title as title, n.date_added as sent_date, concat(u.f_name," (", u.email,")") as send_to from send_newsletter_users snu join users u on u.id = snu.user_id
join newsletters n on snu.newsletter_id = n.id where 1 = 1';
      
      //=======================================
      if(isset($_POST['action']) && $_POST['action'] == 'search'){
        if(!empty($_POST['searchtxt'])){
          $query  .= "and n.title = '%".$_POST['searchtxt']."%'";
        }
        if(!empty($_POST['status']) && $_POST['status'] != 'Select One'){
          $query  .= "and snu.status = '".$_POST['status']."'";
        }
      }
      if(isset($_GET['show_deleted'])){
                $query .= " and snu.status = '".DELETED_STATUS."'";
              }else{
                $query .= " and snu.status <> '".DELETED_STATUS."'";
              }
              $query .= " order by id desc ";
      //echo $query;
      //=======================================
      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
                echo '<tr><td colspan="5">Your search returned no results</td></tr>';
            }
      $rs   = mysqli_query($conn,$query);
      $tcount = mysqli_num_rows($rs);
      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
      $count = 0;
      $i = ($ppage-1)* $rpp;
          $x = 0;
      while(($count<$rpp) && ($i<$tcount)){
                mysqli_data_seek($rs,$i);
                $row = mysqli_fetch_array($rs);
      ?>
      <tr <?=($x%2 == 0)?' bgcolor="#f3f3f3"':''?>>
        
        <td><?=$row['title']?></td>
        <td><?=date_converter($row['sent_date'])?></td>
        <td><?=$row['send_to']?></td>
      </tr>
      <?
        $i++;
        $count++;
        $x++;
      } 
    ?>
    </table>
<? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_ADMIN){ ?>
    <div id="pagination-bottom">
      <?php
        $reload = 'email-templates.php?';
        echo paginate_one($reload, $ppage, $tpages);
      ?>
      <input type="hidden" name="pagination-page" value="email-templates">
    </div>  
   <? } ?>    
    <div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>home.php" title="Go to Home">Back</a></div>
             
    <div class="clear"></div>
<!--**********************************-->
<?php include('common/footer.php');?>