<?php include('includes/configuration.php');
$site_key = '6LdcVRETAAAAAD91vVdDOQLo_bhqq-3MUyHBi2nS';

?>
<script src='https://www.google.com/recaptcha/api.js'></script>
<body>
	<style>
		body { background: #FFFFFF none; }
		.new-wrapper { margin: 12em auto; width: 360px; }
		form { background: none repeat scroll 0 0 #FFFFFF; border: 1px solid #E5E5E5; border-radius: 3px 3px 3px 3px; box-shadow: 0 4px 10px -1px rgba(200, 200, 200, 0.7); font-weight: normal; margin-left: 8px; padding: 26px 24px 46px; }
		label { color: #777777; font-size: 14px; }
		form .input { background: none repeat scroll 0 0 #FBFBFB; border: 1px solid #E5E5E5; box-shadow: 1px 1px 2px rgba(200, 200, 200, 0.2) inset; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", sans-serif; font-size: 24px; font-weight: 200; margin-bottom: 16px; margin-right: 6px; margin-top: 2px; outline: medium none; padding: 3px; width: 97%; }
		textarea.error, input.error { border:1px solid red; }
    a span{color:#cc0000; font-size:35px;}
    </style>
	  <?php if(isset($_GET['error'])){ 
      if($_GET['error'] == 'invalid_id'){
          $message = "Please enter correct Email ... !";
      }else if($_GET['error'] == 'wrong_password'){
          $message = "Please enter correct password ... !";
      }else if($_GET['error'] == 'inactive'){
          $message = "Inactive User ... !";
      }else if($_GET['error'] == 'login_required'){
          $message = "Login Required ... !";
      }else if($_GET['error'] == 'fill_all_fields'){
          $message = "Please fill all fields ... !";
      }else if($_GET['error'] == 'invalid_human_verif'){
          $message = "Invalid Human Verification ... !";
      }
	  
  ?>
      <div id="error" style="margin:auto; top:100px; position:relative; width:340px;">
          <span> ERROR! </span><?php echo $message?>.
      </div><!-- information -->
  <?php } ?>

<div class="new-wrapper">
	<form method="post" action="<?php echo ADMIN?>login.php">
        <p>
           <center><a style="font-size:13px;" href="<?php echo ADMIN?>"><span>Y</span>ou<span>C</span>ricketer</a></center>
        </p>
        <br />
        <p>
           <label>Email :</label>
           <input type="text" name="username" class="input validate[required]" id="username" />
        </p>
        <br />
        <p>
           <label>Password:</label>
           <input type="password" name="password" class="input validate[required]" id="password" />
        </p>
        <br />
		 <p>
           <label>Human Verification:</label>
           <div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
        </p>
		<br/>
        <p class="right">
            <input type="submit" class="btn1" value="Login Me" />
        </p>
    </form>
</div><!--new wrapper -->

</body>
</html>