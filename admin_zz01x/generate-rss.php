<? include('common/header.php'); ?>
<!--**********************************-->
<div class="box1">
	<h1>Generate RSS</h1>
    	<?	ini_set('max_execution_time', -1);
    		//==========================
    		$records_per_page_front		= get_record_on_id('settings',4);
    		define('PRODUCT_LIMIT_FRONT',$records_per_page_front['value'],true);
    		function writeSitemap($filename, $urls = array()){
    			$y = 0;
    			
    			$noOfFiles = round(ceil(count($urls)/49900));
    			if($noOfFiles > 1){
	    			for($x = 1; $x <= $noOfFiles; $x++){
	    				if($x != 1){
	    					$startLimit = ($x-1)*49900 + 1;
	    				}else{
	    					$startLimit = ($x-1)*49900;	
	    				}
	    				$endLimit 	= ($x)*49900;
	    				
		    			$output[$x] = '<?xml version="1.0" encoding="UTF-8"?><urlset  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
			    		for($y = $startLimit; $y <= $endLimit; $y++){
			    			if(!empty($urls[$y])){
			    				$output[$x] .= '<url><loc>'.$urls[$y].'</loc></url>';
							}
			    		}
						$output[$x] .= '</urlset>';

						$myFile = "../sitemaps/".$filename."-".$x.".xml";
						$fh = fopen($myFile, 'w') or die("Can't open file");
						if(fwrite($fh, $output[$x])){
							echo '<div id="success">File Generated with '.count($urls).' links ... !</div>';
						}
						fclose($fh);
						chmod($myFile, 0777);
					}
				}else{
					$output = '<?xml version="1.0" encoding="UTF-8"?>
							<urlset  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
			    		foreach($urls as $url){
			    				$output .= '<url><loc>'.$url.'</loc></url>';
			    		}
						$output .= '</urlset>';

						$myFile = "../sitemaps/".$filename.".xml";
						$fh = fopen($myFile, 'w') or die("Can't open file");
						if(fwrite($fh, $output)){
							echo '<div id="success">File Generated with '.count($urls).' links ... !</div>';
						}
						fclose($fh);
						chmod($myFile, 0777);
				}
			}
			
			function writeToFile($filename, $data,$url_count){
				$myFile = "../rss/".$filename;
				$fh = fopen($myFile, 'w') or die("Can't open file");
				if(fwrite($fh, $data)){
					echo '<div id="success">File Generated with '.$url_count.' links ... !</div>';
				}
				fclose($fh);
				chmod($myFile, 0777);
			}
    		//==========================
    		if($_GET['type'] == 'index'){
    			$urls 	= array();
    			$dir 	= "../sitemaps/";
				$dh 	= opendir($dir);
				$output = '<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
				$x = 0;
				while(($file = readdir($dh)) !== false) {
					if (is_file($dir.'/'.$file) && $file != "." && $file != "..") {
						$output .= '<sitemap>
      <loc>'.WWW.'sitemaps/'.$file.'</loc>
      <lastmod>'.date('Y-m-d').'</lastmod>
   </sitemap>';
					}
					$x++;
				}
				$output .= '</sitemapindex>';
				$myFile = "../sitemap_index.xml";
				$fh = fopen($myFile, 'w') or die("Can't open file");
				if(fwrite($fh, $output)){
					echo '<div id="success">File Generated with '.$x.' links ... !</div>';
				}
				fclose($fh);
				chmod($myFile, 0777);
    		}
    		if($_GET['type'] == 'news'){
    			
				$url_count = 0;
    			$rsRecords 	= mysqli_query($conn,"select * from news where status = '1' order by news_date DESC");
				$str = '<?xml version="1.0" encoding="UTF-8" ?><rss version="2.0"><channel><title>Youcricketer News</title><link>'.WWW.'news.html</link>
							<description>Youcricketer News</description>';
				while($row = mysqli_fetch_assoc($rsRecords)){
					$str.='<item>
					<title>'.$row['title'].'</title>
					<link>'.WWW.'news-detail-'.$row['id'].'-'.friendlyURL($row['title']).'.html</link>
					<description><![CDATA['.substr(strip_tags($row['content']),0,200).'...]]></description>
					<pubDate>'.$row['news_date'].'</pubDate>
					</item>';
					$url_count++;
				}
				
				$str.='</channel></rss>';
				writeToFile('news.xml',$str,$url_count);
			}
    		if($_GET['type'] == 'press-releases'){
    			
				$url_count = 0;
    			$rsRecords 	= mysqli_query($conn,"select * from press_releases where status = '1' ORDER BY release_date DESC LIMIT ".PRODUCT_LIMIT_FRONT);
				$str = '<?xml version="1.0" encoding="UTF-8" ?><rss version="2.0"><channel><title>Youcricketer Press Releases</title><link>'.WWW.'press-releases.html</link>
							<description>Youcricketer Press Releases</description>';
				while($row = mysqli_fetch_assoc($rsRecords)){
					$str.='<item>
					<title>'.$row['title'].'</title>
					<link>'.WWW.'press-release-detail-'.$row['id'].'-'.friendlyURL($row['title']).'.html</link>
					<description><![CDATA['.substr(strip_tags($row['content']),0,200).'...]]></description>
					<pubDate>'.$row['release_date'].'</pubDate>
					</item>';
					$url_count++;
				}
				
				$str.='</channel></rss>';
				writeToFile('press-releases.xml',$str,$url_count);
			}
			
    		if($_GET['type'] == 'blogs'){
    			$url_count = 0;
    			$rsRecords 	= mysqli_query($conn,"select * from blog_articles where status = '1' order by create_date DESC");
				$str = '<?xml version="1.0" encoding="UTF-8" ?><rss version="2.0"><channel><title>Youcricketer Blogs</title><link>'.WWW.'blogs.html</link>
							<description>Youcricketer Blogs</description>';
				while($row = mysqli_fetch_assoc($rsRecords)){
					$str.='<item>
					<title>'.$row['title'].'</title>
					<link>'.WWW.'blog-detail-'.$row['id'].'-'.friendlyURL($row['title']).'.html</link>
					<description><![CDATA['.substr(strip_tags($row['content']),0,200).'...]]></description>
					<pubDate>'.$row['create_date'].'</pubDate>
					</item>';
					$url_count++;
				}
				
				$str.='</channel></rss>';
				writeToFile('blogs.xml',$str,$url_count);

    		}
    		if($_GET['type'] == 'forums'){
    			$url_count = 0;
    			$rsRecords 	= mysqli_query($conn,"select * from forum_topics where status = '1' order by create_date DESC");
				$str = '<?xml version="1.0" encoding="UTF-8" ?><rss version="2.0"><channel><title>Youcricketer Forums</title><link>'.WWW.'forums.html</link>
							<description>Youcricketer Forums</description>';
				while($row = mysqli_fetch_assoc($rsRecords)){
					$str.='<item>
					<title>'.$row['title'].'</title>
					<link>'.WWW.'forum-detail-'.$row['id'].'-'.friendlyURL($row['title']).'.html</link>
					<description><![CDATA['.substr(strip_tags($row['content']),0,200).'...]]></description>
					<pubDate>'.$row['create_date'].'</pubDate>
					</item>';
					$url_count++;
				}
				
				$str.='</channel></rss>';
				writeToFile('forums.xml',$str,$url_count);
    		}
    		
    		if($_GET['type'] == 'testimonials'){
    			
				$url_count = 0;
    			$rsRecords 	= mysqli_query($conn,"select t.*,u.* from testimonials as t inner join users as u on t.user_id=u.id where t.status = '1' order by testimonial_date DESC");
				$str = '<?xml version="1.0" encoding="UTF-8" ?><rss version="2.0"><channel><title>Youcricketer Testimonials</title><link>'.WWW.'testimonials.html</link>
							<description>Youcricketer Testimonials</description>';
				while($row = mysqli_fetch_assoc($rsRecords)){
					$str.='<item>
					<title>'.$row['f_name'].' '.$row['last_name'].'</title>
					<link>'.WWW.'testimonials.html</link>
					<description><![CDATA['.substr(strip_tags($row['content']),0,200).']]></description>
					<pubDate>'.$row['create_date'].'</pubDate>
					</item>';
					$url_count++;
				}
				
				$str.='</channel></rss>';
				writeToFile('testimonials.xml',$str,$url_count);
    		}
    		if($_GET['type'] == 'clubs'){
    			$url_count = 0;
    			$rsRecords 	= mysqli_query($conn,"select * from companies where company_type_id = '4' AND status = '1' order by company_name ASC");
				$str = '<?xml version="1.0" encoding="UTF-8" ?><rss version="2.0"><channel><title>Youcricketer Clubs</title><link>'.WWW.'</link>
							<description>Youcricketer Clubs</description>';
				while($row = mysqli_fetch_assoc($rsRecords)){
					$str.='<item>
					<title>'.$row['company_name'].'</title>
					<link>'.WWW.'company-detail-'.$row['id'].'-'.friendlyURL($row['company_name']).'.html</link>
					<description><![CDATA['.$row['company_name'].']]></description>
					
					</item>';
					$url_count++;
				}
				
				$str.='</channel></rss>';
				writeToFile('clubs.xml',$str,$url_count);
    		}
    		
    		if($_GET['type'] == 'leagues'){
    			$url_count = 0;
    			$rsRecords 	= mysqli_query($conn,"select * from companies where company_type_id = '2' AND status = '1' order by company_name ASC");
				$str = '<?xml version="1.0" encoding="UTF-8" ?><rss version="2.0"><channel><title>Youcricketer Leagues</title><link>'.WWW.'</link>
							<description>Youcricketer Leagues</description>';
				while($row = mysqli_fetch_assoc($rsRecords)){
					$str.='<item>
					<title>'.$row['company_name'].'</title>
					<link>'.WWW.'company-detail-'.$row['id'].'-'.friendlyURL($row['company_name']).'.html</link>
					<description><![CDATA['.$row['company_name'].']]></description>
					
					</item>';
					$url_count++;
				}
				
				$str.='</channel></rss>';
				writeToFile('leagues.xml',$str,$url_count);
    		}
    		if($_GET['type'] == 'events'){
    			$urls 	= array();
    			$urls[] 	= WWW.'events.html';
    			$rsTotalRecords 	= mysqli_query($conn,"select * from events where status = '1' ORDER BY start_date DESC");
				
				$url_count = 0;
    			$rsRecords 	= mysqli_query($conn,"select * from events where status = '1' ");
				$str = '<?xml version="1.0" encoding="UTF-8" ?><rss version="2.0"><channel><title>Youcricketer Events</title><link>'.WWW.'events.html</link>
							<description>Youcricketer Events</description>';
				while($row = mysqli_fetch_assoc($rsRecords)){
					$str.='<item>
					<title>'.$row['title'].'</title>
					<link>'.WWW.'event-detail-'.$row['id'].'-'.friendlyURL($row['title']).'.html</link>
					<description><![CDATA['.substr(strip_tags($row['content']),0,200).'...]]></description>
					<pubDate>'.$row['start_date'].'</pubDate>
					</item>';
					$url_count++;
				}
				
				$str.='</channel></rss>';
				writeToFile('events.xml',$str,$url_count);
				
    		}
    		
    		
			
			//==========================
		?>
        <div style="clear:both; height:30px;"></div>
	<?php 	$entities = array( 'news', 'press-releases', 'blogs', 'forums', 'testimonials', 'clubs', 'leagues','events');?>
	<table width="40%">
		<tr>
			<th>Action</th>
			<th>Entities</th>
			<th>File Name(s)</th>
			<th>Last Generated</th>
			
		</tr>
		<?php foreach($entities as $e){  ?>		
			<tr>
				<td><a href="generate-rss.php?type=<?php echo $e;?>">Generate</a></td>
				<td><?php echo ucwords($e);?></td>
				<td>
					<?php
					
					?>
				</td>
				<td>
					<?php 	if(file_exists('../rss/'.$e.'.xml')){
								echo date('Y-m-d H:i:s', filemtime('../rss/'.$e.'.xml'));
							}else if(file_exists('../rss/'.$e.'-1.xml')){
								echo date('Y-m-d H:i:s', filemtime('../rss/'.$e.'-1.xml'));
							}else{
								echo 'Not Generated';
							}
					?>
				</td>
			</tr>
		<?php } ?>
		<tr>
			<td colspan="3" align="right"></td>
		</tr>		
	</table>

	<div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>home.php">Back</a></div>
<style>
	th{background: #555; color:#fff;}
	th, td{font-size:15px; line-height: 25px;}
	td a{color: blue; text-decoration: underline;}
</style>
<!--***********************************-->
<? include('common/footer.php')?>