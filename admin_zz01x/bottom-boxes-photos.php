<? include('common/header.php');?>  
<!--**********************************-->
<div class="box1">
<h1>Bottom Boxes Photos </h1>
    <table class="grid" cellspacing="0" cellpadding="0" width="100%">
      <tr>
        <th width="5%">Action</th>
        <th width="31%">URL</th>
        <th width="31%">Image</th>
      </tr>      
      
      <?  
      $query = "select * from photos where file_name != '' and entity_type = 'index'";
      $rs    = mysqli_query($conn,$query);
      //=======================================
      while($row = mysqli_fetch_assoc($rs)){  ?>
      <tr <?=($x%2 == 0)?' bgcolor="#f3f3f3"':''?>>
          <td>
            <a href="<?=ADMIN?>view-bottom-boxes-photo.php?id=<?=$row['id']?>">
              <img src="<?=WWW?>images/icons/edit.png">
            </a>
          </td>
          <td><?=$row['title']?></td>
          <td>
            <img src="<?=WWW?>bottom/<?=$row['id']?>/<?=$row['file_name']?>">
          </td>
      </tr>
      <? } ?>
    </table>
    <div class="clear"></div>
    <div id="back"><a href="<?=ADMIN?>home.php" title="Go to Home">Back</a></div>           
    <div class="clear"></div>
</div>
<!--*****************************-->
<? include('common/footer.php') ?>