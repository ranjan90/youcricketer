<?php include('common/header.php');?>	
<!--**********************************-->
<h1>Manage Searches</h1>
<table  border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td>
      <form method="get" action="<?=ADMIN?>searches.php" />
        <input type="hidden" name="action" value="search" />
    <input type="text" name="txtsearch" placeholder="Search Here" class="txt-feild-small"  />
        <input type="submit" value="Go" />
        </form>
    </td>
    <td></td>
    <td style="padding-left:30px;">
        
    </td>
    <td>
      <a  href="<?=ADMIN?>searches.php" title="View All Records" class="btn1">All Records </a>
    </td>
    <td>
        <a  href="<?=ADMIN?>searches.php?show_deleted=1" title="View All Cities" class="btn1">Show Deleted</a>
    </td>
  </tr>
</table>
<div class="clear"></div>
<?php	$id	= $_GET['id'];
        $row 	= mysqli_fetch_assoc(mysqli_query($conn,"select * from searches where id = '$id' "));
        $status	= $row['status'];
        $status	= ($status == '1')?'0':'1';

	if(isset($_GET['action']) && ($_GET['action'] == 'change' || $_GET['action'] == 'delete')){
          if($_GET['action'] == 'change'){
            $query	= "update searches set status = '$status' where id = '$id'";
          }else if($_GET['action'] == 'delete'){
            $query	= "update searches set status = '".DELETED_STATUS."' where id = '$id'";
          }                             

          if(mysqli_query($conn,$query)){
            echo '<div id="success"> <b> '.SUCCESS.': </b> Information updated ... ! </div>';
          }else{
            echo '<div id="error"> <b> '.FAILURE.': </b> Information cannot be updated... ! </div>';
          }
        }
?>
<div class="clear"></div>	
<div id="pagination-top"></div>
<table class="grid">
  <tr>
    <th width="3%">Action</th>
    <th width="30%">Keywords</th>
    <th width="10%">IPs</th>
  </tr>
  <?  
      $rpp = PRODUCT_LIMIT_ADMIN; // results per page
      $ppage = intval($_GET["page"]);
      if($ppage<=0) $ppage = 1;
      $date  = date('Y-m-d h:m:s');
      $query = "select * from searches where 1=1 ";
      //=======================================
      if(isset($_GET['action']) && $_GET['action'] == 'search'){
        if($_GET['txtsearch'] != ''){
          $txtsearch = $_GET['txtsearch'];
          $query  .= " and keywords like '%".$txtsearch."%'";
        }
      }
      if(isset($_GET['show_deleted'])){
        $query .= " and status = '".DELETED_STATUS."'";
      }else{
        $query .= " and status <> '".DELETED_STATUS."'";
      }
      //echo $query;exit;
      //=======================================
      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
        echo '<tr><td colspan="5">No record found ... !</td></tr>';
      }
      $rs   = mysqli_query($conn,$query);
      $tcount = mysqli_num_rows($rs);
      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
      $count = 0;
      $i = ($ppage-1)* $rpp;
      $x = 0;
      while(($count<$rpp) && ($i<$tcount)){
        mysqli_data_seek($rs,$i);
        $row = mysqli_fetch_array($rs);
        ?>
        <tr <?=($x%2 == 0)?' bgcolor="#f3f3f3"':''?>>
          <td>
            <a href="<?=ADMIN?>searches.php?action=change&id=<?=$row['id']?>" title="Change Status">
            <?
              if($row['status'] == '1'){
                $img = 'status.png';
                $alt = 'Active Record';
              }else if($row['status'] == '0'){
                $img = 'in-status.png';
                $alt = 'Inactive Record';
              }else if($row['status'] == DELETED_STATUS){
                $img = 'restore.png';
                $alt = 'Deleted Record';
              }
            ?>
            <img src="<?=WWW?>images/icons/<?=$img?>" alt="<?=$alt?>" width="16" />
          </a>
          <a href="<?=ADMIN?>searches.php?action=delete&id=<?=$row['id']?>" title="Delete Record">
            <img src="<?=WWW?>images/icons/delete.png" alt="Delete" />
            </a>
          </td>
          <td><?php echo $row['keywords']?></td>
          <td><?=$row['ip']?></td>
        </tr>
      <?
        $i++;
        $count++;
        $x++;
      } 
    ?>
</table>
<? if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_ADMIN){ ?>
    <div id="pagination-bottom">
      <?php
        $keywords = array();
        foreach($_GET as $key=>$value){
          $keywords[] = $key.'='.$value;
        }
        $reload = 'searches.php?'.implode('&',$keywords).'&';
        echo paginate_one($reload, $ppage, $tpages);
      ?>
      <input type="hidden" name="pagination-page" value="searches">
        </div>  
        <? } ?>
        <div class="clear"></div>    
<!--**********************************-->
<?php include('common/footer.php');?>