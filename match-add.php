<?php include_once('includes/configuration.php');
$page = 'match-add.html';
$selected_country = getGeoLocationCountry(); 

$error = '';
$umpires = array();
$venues = array();
$tournament_permission = 0;
$tournament_id = trim($_GET['t_id']);
$tournament_info = get_record_on_id('tournaments', $tournament_id);	

$page_title = 'Add New Match - '.ucwords($tournament_info['title']);

/*$sql = "Select * from tournament_matches where tournament_id = ".$tournament_id.' order by start_time';
$rs = mysqli_query($conn,$sql);
$i = 1;
while($row = mysqli_fetch_assoc($rs)){
	$sql = "Update tournament_matches SET game_number=$i WHERE id = ".$row['id'];mysqli_query($conn,$sql);
	$i++;
}*/

$sql = "Select max(game_number) as max_game_number from tournament_matches where tournament_id = ".$tournament_id;
$row_game_number = mysqli_fetch_assoc(mysqli_query($conn,$sql));
$game_number = $row_game_number['max_game_number']+1;

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
	
	if($user_id){
		$sql = 'select c.*,t.group_name from tournament_teams as t inner join companies as c on t.team_id=c.id 
		inner join league_clubs as lc on lc.club_id=t.team_id 
		where t.tournament_id = '.$tournament_id;
		$rs_club = mysqli_query($conn,$sql);
		while($row = mysqli_fetch_assoc($rs_club)){
			$tournament_clubs[] = $row;
		}
		
		$sql = 'select * from companies where user_id = '.$tournament_info['user_id'];
		$rs_league = mysqli_query($conn,$sql);
		$league_info = mysqli_fetch_assoc($rs_league);
		
		$sql = 'select c.* from league_clubs as t inner join companies as c on t.club_id=c.id where t.league_id = '.$league_info['id'];
		$rs_club = mysqli_query($conn,$sql);
		while($row = mysqli_fetch_assoc($rs_club)){
			$league_clubs[] = $row;
		}
	}
}

if(isset($user_id) && !empty($user_id)){
	$sql = "select * from tournament_permissions where tournament_id = $tournament_id and member_id = $user_id LIMIT 1";
	$rs_permission = mysqli_query($conn,$sql);
	if($tournament_info['user_id'] == $user_id || mysqli_num_rows($rs_permission)>0 ){
		$tournament_permission = 1;
	}else{
		$tournament_permission = 0;
	}
}else{
	$tournament_permission = 0;
}

//$sql = "SELECT id,umpire_name FROM tournament_umpires WHERE tournament_id = $tournament_id ORDER BY id ";
$sql = "select u.f_name,u.m_name,u.last_name,u.id as user_id,l.umpire_phone from users as u inner join tournament_permissions as tm on u.id=tm.member_id
 inner join league_umpires as l on u.id=l.umpire_id 
 where tm.tournament_id = $tournament_id ";

$rs_umpires = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_umpires)){
	$umpires[] = $row;
}

$sql = "SELECT id,venue FROM league_venues WHERE league_id = ".$league_info['id']." ORDER BY id ";
$rs_venues = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_venues)){
	$venues[] = $row;
}

if(isset($_POST['submit_btn']) && !empty($_POST['submit_btn'])){
	validate();
	if(empty($error)){
		$date = date('Y/m/d H:i:s')	;
		if(empty($_POST['umpire_id'])) $_POST['umpire_id'] = 0;
		if(empty($_POST['venue_id'])) $_POST['venue_id'] = 0;
		if(empty($_POST['toss_won_team_id'])) $_POST['toss_won_team_id'] = 0;
		$day = date('l',strtotime(trim($_POST['start_time'])));
			
		$sql = " INSERT INTO tournament_matches SET game_number = $game_number, tournament_id='".trim($tournament_id)."',team1='".trim($_POST['team1'])."',
		team2='".trim($_POST['team2'])."',start_time='".trim($_POST['start_time'])."',start_day='$day',umpire1_id='".trim($_POST['umpire1_id'])."',
		umpire2_id='".trim($_POST['umpire2_id'])."',umpire3_id='".trim($_POST['umpire3_id'])."',umpire1_from='".trim($_POST['umpire1_from'])."',
		umpire2_from='".trim($_POST['umpire2_from'])."',umpire3_from='".trim($_POST['umpire3_from'])."',referee_id='".trim($_POST['referee_id'])."',
		added_on='".$date."',venue_id='".trim($_POST['venue_id'])."',scorer_1='".trim($_POST['scorer_1'])."',
		scorer_2='".trim($_POST['scorer_2'])."',last_updated_by = $user_id,updated_on='".$date."',maximum_overs=".$tournament_info['overs_count'].",status=1, type='".trim($_POST['type'])."'";
		// echo $sql;exit;
		if(mysqli_query($conn,$sql)){
			$match_id = mysqli_insert_id($conn);
			
			$_SESSION['match_added'] = 1;
			header("Location:".WWW."tournament/matches/list/".$tournament_id);
		}else{
			$error = '<p class="alert alert-danger">Error in adding Match. Try again later</p>';
		}
	}
}

function validate(){
	global $error;
	
	if(empty($_POST['start_time'])){
		$error.= '<p class="alert alert-danger">Date/Time is required field</p>';
	}
	/*if(!empty($_POST['group_1']) && !ctype_alnum($_POST['group_1'])){
		$error.= '<p class="alert alert-danger">Group should have Alphanumeric characters</p>';
	}*/
	if(empty($_POST['team1']) && $_POST['team1'] != '0'){
		$error.= '<p class="alert alert-danger">Host Team is required field</p>';
	}
	if(empty($_POST['team2']) && $_POST['team2'] != '0'){
		$error.= '<p class="alert alert-danger">Guest Team is required field</p>';
	}
	if(!empty($_POST['team1']) && !empty($_POST['team2']) && $_POST['team1'] == $_POST['team2']){
		$error.= '<p class="alert alert-danger">Host Team is same as Guest Team </p>';
	}
	if(empty($_POST['venue_id'])){
		$error.= '<p class="alert alert-danger">Venue is required field</p>';
	}
}

?>
<?php include('common/header.php'); ?>


<div class="page-container"> 
		<?php  include('common/user-left-panel.php');?>
      <!-- END SIDEBAR -->
      
      <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
		<div class="page-content">
		<?php  include('common/breadcrumbs.php');?>
		
		    <div class="white-box">
            <div class="row">
              <div class="col-sm-12">
                <h2> Add New Match Schedule </h2>
                <h3> Tournament: <?php echo $tournament_info['title'] ?> </h3>
              </div>
            </div>
            
            <div class="row">
              <div class="col-sm-12">
                <h3> Match Schedule </h3>
                <p>Fields with * are Required</p>
				<?php if(empty($user_info)): ?>
					<div id="error" class="alert alert-danger">You are not logged... !</div>
				<?php endif; ?>
				
				<?php if(!empty($error)): ?>
					<?php echo $error; ?>
				<?php endif; ?>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <form method="post" enctype="multipart/form-data" class="form-horizontal">
                  <input name="action" value="submit" type="hidden">
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Date/Time*</label>
                    <div class="col-sm-7">
                      <input name="start_time" autocomplete="off" id="start_time" class="form-control" value="<?php if(!empty($_POST['start_time'])) echo $_POST['start_time']; ?>" type="text">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Game Number</label>
                    <div class="col-sm-7">
                      <input name="game_number" id="game_number" class="form-control" value="94" value="<?php echo $game_number; ?>" disabled type="text">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Host Team*</label>
                    <div class="col-sm-7 text">
                      <select name="team1" id="team1" class="form-control">
                        <option value="">Select One</option>
                        <option value="0">To Be Decided</option>
							<?php for($i=0;$i<count($tournament_clubs);$i++): if(isset($_POST['team1']) && $_POST['team1'] == $tournament_clubs[$i]['id']) $sel =  'selected';else $sel = '';  ?>
								<option <?php echo $sel; ?> value="<?php echo $tournament_clubs[$i]['id']; ?>"><?php echo $tournament_clubs[$i]['company_name']; ?></option>
							<?php endfor; ?>
                      </select>
					  <div style="float:left;margin-left:10px;font-weight:normal;margin-top:6px;" class="group_name"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Guest Team*</label>
                    <div class="col-sm-7 text">
                      <select name="team2" id="team2" class="form-control">
                        <option value="">Select One</option>
                        <option value="0">To Be Decided</option>
							<?php for($i=0;$i<count($tournament_clubs);$i++): if(isset($_POST['team2']) && $_POST['team2'] == $tournament_clubs[$i]['id']) $sel =  'selected';else $sel = '';  ?>
								<option <?php echo $sel; ?> value="<?php echo $tournament_clubs[$i]['id']; ?>"><?php echo $tournament_clubs[$i]['company_name']; ?></option>
							<?php endfor; ?>
                      </select>
					  <div style="float:left;margin-left:10px;font-weight:normal;margin-top:6px;" class="group_name"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Venue*</label>
                    <div class="col-sm-7">
                      <select name="venue_id" id="venue_id" class="form-control">
                        <option value="0">Select One</option>
							<?php for($i=0;$i<count($venues);$i++): if(isset($_POST['venue_id']) && $_POST['venue_id'] == $venues[$i]['id']) $sel =  'selected';else $sel = '';  ?>
								<option <?php echo $sel; ?> value="<?php echo $venues[$i]['id']; ?>"><?php echo $venues[$i]['venue']; ?></option>
							<?php endfor; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">1st Umpire</label>
                    <div class="col-sm-7">
                      <select name="umpire1_id" id="umpire1_id" class="form-control display-phone">
                        <option value="0">N/A</option>
						<?php for($i=0;$i<count($umpires);$i++): if(isset($_POST['umpire1_id']) && $_POST['umpire1_id'] == $umpires[$i]['user_id']) $sel =  'selected';else $sel = '';  ?>
							<option <?php echo $sel; ?> value="<?php echo $umpires[$i]['user_id']; ?>"><?php echo $umpires[$i]['f_name']; ?> <?php echo $umpires[$i]['m_name']; ?> <?php echo $umpires[$i]['last_name']; ?></option>
						<?php endfor; ?>
                      </select>
					  <div id="umpire1_id_phone" class="umpire-phone"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">1st Umpire From</label>
                    <div class="col-sm-7">
                      <select name="umpire1_from" id="umpire1_from" class="form-control">
                       <option value="0">N/A</option>
							<option value="-1">Independent</option>
							<?php for($i=0;$i<count($league_clubs);$i++): if(isset($_POST['umpire1_from']) && $_POST['umpire1_from'] == $league_clubs[$i]['id']) $sel =  'selected';else $sel = '';  ?>
								<option <?php echo $sel; ?> value="<?php echo $league_clubs[$i]['id']; ?>"><?php echo $league_clubs[$i]['company_name']; ?></option>
							<?php endfor; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">2nd Umpire</label>
                    <div class="col-sm-7">
                      <select name="umpire2_id" id="umpire2_id" class="form-control display-phone">
                        <option value="0">N/A</option>
						<?php for($i=0;$i<count($umpires);$i++): if(isset($_POST['umpire2_id']) && $_POST['umpire2_id'] == $umpires[$i]['user_id']) $sel =  'selected';else $sel = '';  ?>
							<option <?php echo $sel; ?> value="<?php echo $umpires[$i]['user_id']; ?>"><?php echo $umpires[$i]['f_name']; ?> <?php echo $umpires[$i]['m_name']; ?> <?php echo $umpires[$i]['last_name']; ?></option>
						<?php endfor; ?>
                      </select>
					  <div id="umpire2_id_phone" class="umpire-phone"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">2nd Umpire From</label>
                    <div class="col-sm-7">
                      <select name="umpire2_from" id="umpire2_from" class="form-control">
                        <option value="0">N/A</option>
							<option value="-1">Independent</option>
							<?php for($i=0;$i<count($league_clubs);$i++): if(isset($_POST['umpire2_from']) && $_POST['umpire2_from'] == $league_clubs[$i]['id']) $sel =  'selected';else $sel = '';  ?>
								<option <?php echo $sel; ?> value="<?php echo $league_clubs[$i]['id']; ?>"><?php echo $league_clubs[$i]['company_name']; ?></option>
							<?php endfor; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Ist Scorer</label>
                    <div class="col-sm-7">
                      <select name="scorer_1" id="scorer_1" class="form-control display-phone">
                        <option value="0">N/A</option>
							<?php for($i=0;$i<count($umpires);$i++): if(isset($_POST['scorer_1']) && $_POST['scorer_1'] == $umpires[$i]['user_id']) $sel =  'selected';else $sel = '';  ?>
								<option <?php echo $sel; ?> value="<?php echo $umpires[$i]['user_id']; ?>"><?php echo $umpires[$i]['f_name']; ?> <?php echo $umpires[$i]['m_name']; ?> <?php echo $umpires[$i]['last_name']; ?></option>
							<?php endfor; ?>
						</select>
						<div id="scorer_1_phone" class="umpire-phone"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">2nd Scorer</label>
                    <div class="col-sm-7">
                      <select name="scorer_2" id="scorer_2" class="form-control display-phone">
                        <option value="0">N/A</option>
							<?php for($i=0;$i<count($umpires);$i++): if(isset($_POST['scorer_2']) && $_POST['scorer_2'] == $umpires[$i]['user_id']) $sel =  'selected';else $sel = '';  ?>
								<option <?php echo $sel; ?> value="<?php echo $umpires[$i]['user_id']; ?>"><?php echo $umpires[$i]['f_name']; ?> <?php echo $umpires[$i]['m_name']; ?> <?php echo $umpires[$i]['last_name']; ?></option>
							<?php endfor; ?>
						</select>
						<div id="scorer_2_phone" class="umpire-phone"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">3rd Umpire</label>
                    <div class="col-sm-7">
                      <select name="umpire3_id" id="umpire3_id" class="form-control display-phone">
                        <option value="0">N/A</option>
							<?php for($i=0;$i<count($umpires);$i++): if(isset($_POST['umpire3_id']) && $_POST['umpire3_id'] == $umpires[$i]['user_id']) $sel =  'selected';else $sel = '';  ?>
								<option <?php echo $sel; ?> value="<?php echo $umpires[$i]['user_id']; ?>"><?php echo $umpires[$i]['f_name']; ?> <?php echo $umpires[$i]['m_name']; ?> <?php echo $umpires[$i]['last_name']; ?></option>
							<?php endfor; ?>
						</select>
						<div id="umpire3_id_phone" class="umpire-phone"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">3rd Umpire From</label>
                    <div class="col-sm-7">
                      <select name="umpire3_from" id="umpire3_from" class="form-control">
                        <option value="0">N/A</option>
							<option value="-1">Independent</option>
						<?php for($i=0;$i<count($league_clubs);$i++): if(isset($_POST['umpire3_from']) && $_POST['umpire3_from'] == $league_clubs[$i]['id']) $sel =  'selected';else $sel = '';  ?>
							<option <?php echo $sel; ?> value="<?php echo $league_clubs[$i]['id']; ?>"><?php echo $league_clubs[$i]['company_name']; ?></option>
						<?php endfor; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Match Referee</label>
                    <div class="col-sm-7">
                      <select name="referee_id" id="referee_id" class="form-control display-phone">
                        <option value="0">N/A</option>
							<?php for($i=0;$i<count($umpires);$i++): if(isset($_POST['referee_id']) && $_POST['referee_id'] == $umpires[$i]['user_id']) $sel =  'selected';else $sel = '';  ?>
								<option <?php echo $sel; ?> value="<?php echo $umpires[$i]['user_id']; ?>"><?php echo $umpires[$i]['f_name']; ?> <?php echo $umpires[$i]['m_name']; ?> <?php echo $umpires[$i]['last_name']; ?></option>
							<?php endfor; ?>
						</select>
						<div id="referee_id_phone" class="umpire-phone"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Type *</label>
                    <div class="col-sm-7">
	                    <select name="type" id="type" class="form-control display-phone" required>
                        	<option value="" selected="selected">Select One</option>
                        	<option value="Regular">Regular</option>
                        	<option value="R Robin">R Robin</option>
                        	<option value="Qtr-Fnl">Qtr-Fnl</option>
                        	<option value="Semis">Semis</option>
                        	<option value="Final">Final</option>
						</select>
						<div id="referee_id_phone" class="umpire-phone"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-5 col-sm-7">
                      <input name="submit_btn" value=" Submit " class="btn orange hvr-float-shadow" type="submit">
                      <input name="cancel_btn" value=" Cancel " class="btn blue hvr-float-shadow" onclick="window.location.href='<?php echo WWW; ?>tournament/matches/list/<?php echo $tournament_id; ?>';" type="button">
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
		
		
		</div>
	</div>
</div>		


<script src="<?php echo WWW; ?>assets/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo WWW; ?>assets/css/jquery.datetimepicker.css"/>
<script type="text/javascript">
	$('#start_time').datetimepicker();
	var teams = new Array();
	<?php for($i=0;$i<count($tournament_clubs);$i++){ ?>
		teams[<?php echo $tournament_clubs[$i]['id']; ?>] = "<?php echo $tournament_clubs[$i]['group_name']; ?>";
	<?php } ?>
	
	$("#team1,#team2").bind('change',function(){
		var team_id = $(this).val();
		if(team_id != ''){
			var group_name = teams[team_id];
			$(this).parents('.text').find('.group_name').html(group_name);
		}else{
			$(this).parents('.text').find('.group_name').html('');
		}
	});
	
	var phones = new Array();
	<?php for($i=0;$i<count($umpires);$i++){ ?>
		phones[<?php echo $umpires[$i]['user_id']; ?>] = "<?php echo $umpires[$i]['umpire_phone']; ?>";
	<?php } ?>
	
	$(".display-phone").bind('change',function(){
		var umpire_id = $(this).val();
		var id = $(this).attr('id');
		if(umpire_id != 0){
			var phone_no = phones[umpire_id];
			$("#"+id+"_phone").html("Phone No: "+phone_no);
		}else{
			$("#"+id+"_phone").html("Phone No: ");
		}
	});
	
</script>
<?php include('common/footer.php'); ?>