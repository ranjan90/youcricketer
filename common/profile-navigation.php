<ul id="steps">
	<li class="<?=(preg_match('/profile-information-step-1.html(.*)/',$page))?'current':'';?>" onclick="window.location='<?=WWW?>profile-information-step-1.html'">
		Step 1<span>Profile Photo</span>
	</li>
	<li class="<?=(preg_match('/profile-information-step-2.html(.*)/',$page))?'current':'';?>" onclick="window.location='<?=WWW?>profile-information-step-2.html'">
		Step 2<span>Profile Video</span>
	</li>
	<li class="<?=(preg_match('/profile-information-step-3.html(.*)/',$page))?'current':'';?>" onclick="window.location='<?=WWW?>profile-information-step-3.html'">
		Step 3<span>Location Details</span>
	</li>
	<li class="<?=(preg_match('/profile-information-step-4.html(.*)/',$page))?'current':'';?>" onclick="window.location='<?=WWW?>profile-information-step-4.html'">
		Step 4<span>Individual Traits</span>
	</li>
	<li class="<?=(preg_match('/profile-information-step-5.html(.*)/',$page))?'current':'';?>" onclick="window.location='<?=WWW?>profile-information-step-5.html'">
		Step 5<span>Current Affiliations</span>
	</li>
	
	<li class="<?=(preg_match('/profile-information-step-6.html(.*)/',$page))?'current':'';?>" onclick="window.location='<?=WWW?>profile-information-step-6.html'">
		Step 6<span>Overall Experience</span>
	</li>
	<li class="<?=(preg_match('/profile-information-step-7.html(.*)/',$page))?'current':'';?>" onclick="window.location='<?=WWW?>profile-information-step-7.html'">
		Step 7<span>Seeking Opportunities</span>
	</li>
	<li class="<?=(preg_match('/profile-information-step-8.html(.*)/',$page))?'current':'';?>" onclick="window.location='<?=WWW?>profile-information-step-8.html'">
		Step 8<span>Travel Details</span>
	</li>
	<li class="<?=(preg_match('/profile-information-step-9.html(.*)/',$page))?'current':'';?>" onclick="window.location='<?=WWW?>profile-information-step-9.html'">
		Step 9<span>Personality</span>
	</li>
	<li class="<?=(preg_match('/profile-information-step-10.html(.*)/',$page))?'current':'';?>" onclick="window.location='<?=WWW?>profile-information-step-10.html'">
		Step 10<span>Social Media Details</span>
	</li>
</ul>