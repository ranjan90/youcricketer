<div itemscope="itemscope" itemtype="http://schema.org/WebPage">
	<?php if($page != '' && $page != 'index.html' && $page != 'login.html?status=verified' && $page != '?status=success_individual' && $page != '?status=success_company'){ 
			$community = array('blogs.html', 'forums.html','groups.html','testimonials.html','news.html','events.html','leagues.html','clubs.html', 'other-companies.html');
			
		?>
	<div id="breadcrumbs" itemprop="breadcrumb">
		<a href="<?=WWW?>" title="Home - <?=$site_title?>">Home</a>
		<span></span>
		<? if(in_array($page, $community)){ ?>
		<a itemprop="item" href="<?=WWW?>community.html" title="Community - <?=$site_title?>">Community</a>
		<span></span>
		<? } ?>
		<?  
			if(preg_match('/my-(.*).html/',$page, $match)){
				if($match[0] == 'my-news.html'
				|| $match[0] == 'my-groups.html'
				|| $match[0] == 'my-blog.html'
				|| $match[0] == 'my-forum.html'
				|| $match[0] == 'my-events.html'
				|| $match[0] == 'my-testimonials.html'){
					echo '<a itemprop="item" href="'.WWW.'my-community.html">My Community</a>';
					echo '<span></span>';	
				}
			}
		?>
		<? if(preg_match('/(.*)-detail-/',$page, $match)){

				$br_name 	= $_GET['name'];
				
				if($match[1] == 'blog'){
					$br_page = 'blogs';
				}else if($match[1] == 'forum'){
					$br_page = 'forums';
				}else if($match[1] == 'group-post'){
					$br_page = 'groups';
				}else if($match[1] == 'company'){
					$br_page = 'companies';
				}else{
					if($match[1] != 'news'){
						$br_page = $match[1].'s';	
					}else{
						$br_page = $match[1];	
					}	
				}
				if($match[1] != 'individual' && $match[1] != 'company'){
				if($match[1] == 'press-release') {
	                       // do not put comunity here if press-release found
											}
				else {
					echo '<a itemprop="item" href="'.WWW.'community.html">Community</a>';
					echo '<span></span>';	
					}
				}
				echo '<a itemprop="item" href="'.WWW.$br_page.'.html">'.ucwords($match[1]).'</a>';
				echo '<span></span>';
				echo '<p class="inline">'.ucwords($br_name).'</p>';
		 ?>
		<? }else{
			if(isset($_GET['url'])){
				echo '<a itemprop="item" href="'.WWW.$_GET['type'].'.html">'.ucwords($_GET['type']).'</a>';
				echo '<span></span>';
				echo '<p class="inline">ESPN '.ucwords($_GET['type']).'</p>';
			}else if(isset($_GET['keywords']) && !empty($_GET['keywords'])){
				$pageTokens = explode('-', $page);
				$pageLink   = array();
				for($x=0; $x < count($pageTokens)-1; $x++){
					$pageLink[] = $pageTokens[$x];
				}
				echo '<a itemprop="item" href="'.WWW.'community.html">Community</a>';
				echo '<span></span>';
				echo '<a itemprop="item" href="'.WWW.implode('-',$pageLink).'.html">'.ucwords(implode(' ',$pageLink)).'</a>';
				echo '<span></span>';
				echo '<p class="inline">Searched : '.ucwords($_GET['keywords']).'</p>';
				?>
		 <? }else if(preg_match('/(.*)-category-/', $page, $pageType)){
		 		switch($pageType[1]){
		 			case 'blog':
		 				$pageLinkCat = 'blogs';
		 				$titleLinkCat= 'Blog';
		 				break;
		 			case 'forum':
		 				$pageLinkCat = 'forum';
		 				$titleLinkCat= 'Forum';
		 				break;	
		 		}
		 		echo '<a itemprop="item" href="'.WWW.$pageLinkCat.'.html">'.$titleLinkCat.'</a>';
				echo '<span></span>';
				echo '<a itemprop="item" href="'.WWW.$pageLinkCat.'.html">Categories</a>';
				echo '<span></span>';
				echo cleanForBreadcrumb($_GET['name']);

		 	}else if(isset($_GET['page'])){
		 		$pppage = explode('?', $page);
		 		$phtml  = explode('.', $pppage[0]);
		 		
		 		echo '<a itemprop="item" href="'.WWW.'community.html">Community</a>';
				echo '<span></span>';
				echo '<a itemprop="item" href="'.WWW.$pppage[0].'">'.ucwords($phtml[0]).'</a>';
				echo '<span></span>';
				echo 'Page # '.$_GET['page'];
		 		
		 	}else{ ?>
				<p class="inline">
				<?php 
			
				if(strpos(cleanForBreadcrumb($page), "Profile Information Step 5") === 0)
				{
					echo "Profile Information Step 5";
				}
				
				else if(strpos(cleanForBreadcrumb($page), "Profile Information Step 9") === 0){
					echo "Profile Information Step 9";
				}
				else if(strpos(cleanForBreadcrumb($page), "Profile Information Step 6") === 0){
					echo "Profile Information Step 6";
				}
					else if(strpos(cleanForBreadcrumb($page), "Profile Information Step 7") === 0){
					echo "Profile Information Step 7";
				}
					else if(strpos(cleanForBreadcrumb($page), "Profile Information Step 8") === 0){
					echo "Profile Information Step 8";
				}
			else if(strpos(cleanForBreadcrumb($page), "Inbox Message") === 0){
					echo "Inbox Message";
				}
			else if(strpos(cleanForBreadcrumb($page), "Verify Account") === 0){
					echo "Verify Account";
				}
			else if(strpos(cleanForBreadcrumb($page), "Outbox Message") === 0){
					echo "Outbox Message";
				}
			else if(strpos(cleanForBreadcrumb($page), "Deleted Message") === 0){
					echo "Deleted Message";
				}
		else if(strpos(cleanForBreadcrumb($page), "Sign Up?ref") === 0){
					echo "Sign Up";
				}
				else if(strpos(cleanForBreadcrumb($page), "Searches") === 0){
					$search = explode('-',$page);
					echo "Search result for ".$search[1];
				}

				else if(strpos(cleanForBreadcrumb($page), "Forward Outbox Message") === 0){
					$search = explode('-',$page);
					echo ' Forward Outbox  Message ';
				}


				else if(strpos(cleanForBreadcrumb($page), "Advance Search") === 0){
					
					echo "Advance Search";
				}
				else if(strpos(cleanForBreadcrumb($page), "Update Company Profile 5") === 0){
							
							echo "Company Profile 5";
						}
				else if(strpos(cleanForBreadcrumb($page), "Update Company Profile 4") === 0){
							
							echo "Company Profile 4";
						}
				else if(strpos(cleanForBreadcrumb($page), "Update Company Profile 3") === 0){
							
							echo "Company Profile 3";
						}
				else if(strpos(cleanForBreadcrumb($page), "Update Company Profile 2") === 0){
							
							echo "Company Profile 2";
						}
				else if(strpos(cleanForBreadcrumb($page), "Update Company Profile 1") === 0){
					
					echo "Company Profile 1";
				}
				else {
					echo cleanForBreadcrumb($page);
				}
				?></p>	
		 <? } // if keywords		 
		 } ?>
	</div>
</div>
<?php $check = stripos($page, 'world-cup-cricket-2015-');
	if($check !== false){ ?>
<div id="social" class="left">
<a style="background-image:url('<?php echo WWW;?>images/bread-crum-arrow.png') no-repeat !important;" href="<?php echo WWW;?>world-cup-cricket-2015.html" title="World Cup Cricket 2015">Back to World Cup 2015</a>
</div>
<?php } ?>
<div id="social" class="right">
	<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <span class='st_facebook_hcount' displayText='Facebook'></span>
    <span class='st_twitter_hcount' displayText='Tweet'></span>
	<span class='st_linkedin_hcount' displayText='LinkedIn'></span>
</div>
<div class="clear"></div>
<?php } ?>  