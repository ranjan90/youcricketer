<? //error_reporting(E_ALL ^ E_STRICT); ?>
</div></div>
<div id="footer">
	<div class="footer-container">
        <div class="end">
        	<div class="end-links">
        		<a title="Home - <?=$site_title?>" href="<?=WWW?>">Home</a> - 
                <a title="Quick Tour - <?=$site_title?>" href="<?=WWW?>quick-tour.html">Quick Tour</a> - 
        		<a title="About Us - <?=$site_title?>" href="<?=WWW?>about-us.html">About Us</a> - 
        		<a title="Multimedia Policies - <?=$site_title?>" href="<?=WWW?>multimedia-policy.html">Multimedia Policies</a> - 
	        	<a title="Privacy Policy and Cookie Use - <?=$site_title?>" href="<?=WWW?>cookie-use.html">Privacy Policy and Cookie Use</a> - 
	        	<a title="Terms of Use - <?=$site_title?>" href="<?=WWW?>terms-of-use.html">Terms of Use</a> - 
	        	<a title="Advertising - <?=$site_title?>" href="<?=WWW?>advertising.html">Advertising</a> - 
                <a title="Careers - <?=$site_title?>" href="<?=WWW?>careers.html">Careers</a> - 
	        	<a title="Contact Us- <?=$site_title?>" href="<?=WWW?>contact-us.html">Contact Us</a>
        	</div>
			<div class="copyright">Copyright &copy; <?php echo date('Y');?> - All Rights Reserved By <span class="orange"><strong><?=$row_site_title['value'];?></strong></span></div>
        </div>
    </div>
</div>
<?php mysqli_close($conn);?>
</body>
</html>