 <footer class="footer">
      <div class="container">
        <div class="end-links">
          <a href="<?php echo WWW ?>">Home</a> -
          <a href="<?php echo WWW ?>quick-tour.html">Quick Tour</a> -
          <a href="<?php echo WWW ?>about-us.html">About Us</a> -
          <a href="<?php echo WWW ?>multimedia-policy.html">Multimedia Policies</a> -
          <a href="<?php echo WWW ?>cookie-use.html">Privacy Policy and Cookie Use</a> -
          <a href="<?php echo WWW ?>terms-of-use.html">Terms of Use</a> -
          <a href="<?php echo WWW ?>advertising.html">Advertising</a> -
          <a href="<?php echo WWW ?>careers.html">Careers</a> -
          <a href="<?php echo WWW ?>contact-us.html">Contact Us</a>
        </div>
		<div class="copyright">Copyright &copy; <?php echo date("Y");?>- All Rights Reserved By <span class="orange"><strong>YouCricketer Inc.</strong></span></div>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="<?php echo WWW ?>assets/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo WWW ?>assets/js/ie10-viewport-bug-workaround.js"></script>
    <!--<script src="<?php echo WWW ?>assets/js/jquery.tabify.js" type="text/javascript" charset="utf-8"></script>-->
	<!--<script src="<?php echo WWW ?>assets/js/bootstrap-datepicker.js"></script>-->
	  <!-- <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
	<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <!-- <script src="<?php echo WWW ?>assets/js/jquery.ui.touch-punch.js"></script> -->
  <script src="http://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
  <script src="http://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
  <!-- <script src="<?php echo WWW ?>js/jquery.sortable.js" type="text/javascript" charset="utf-8"></script> -->
	<script src="<?php echo WWW ?>assets/js/app.js" type="text/javascript" charset="utf-8"></script>

    <script type="text/javascript">
		$('ul.nav li.dropdown').hover(function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});

		$('[data-toggle="tooltip"]').tooltip();

    $(".my-show-add").click(function(){
        var check=$(this).attr('data-show');
        if(check==0){
          $("#global_banner").parent('div').hide();
          $(".ads-10").removeClass('col-md-10');
          $(".ads-10").addClass('col-md-12');

        }else{
          $("#global_banner").parent('div').show();
          $(".ads-10").removeClass('col-md-12');
          $(".ads-10").addClass('col-md-10');
        }

    });

		$(document).ready(function() {
			App.init();
			$("#companies-tab").click(function(event){
				event.preventDefault();
				event.stopPropagation();
				$("#individual").hide();
				$("#companies").show();
				$("#companies-tab").addClass("active");
				$("#individual-tab").removeClass("active");
			});
			$("#individual-tab").click(function(event){
				event.preventDefault();
				event.stopPropagation();
				$("#companies").hide();
				$("#individual").show();
				$("#individual-tab").addClass("active");
				$("#companies-tab").removeClass("active");
			});

			$("#news-tab").click(function(event){
				event.preventDefault();
				event.stopPropagation();
				$("#scoreboard").hide();
				$("#news").show();
				$("#news-tab").addClass("active");
				$("#scoreboard-tab").removeClass("active");
			});
			$("#scoreboard-tab").click(function(event){
				event.preventDefault();
				event.stopPropagation();
				$("#news").hide();
				$("#scoreboard").show();
				$("#scoreboard-tab").addClass("active");
				$("#news-tab").removeClass("active");
			});

			$("#result-tab").click(function(event){
				event.preventDefault();
				event.stopPropagation();
				$("#blog").hide();
				$("#result").show();
				$("#result-tab").addClass("active");
				$("#blog-tab").removeClass("active");
			});
			$("#blog-tab").click(function(event){
				event.preventDefault();
				event.stopPropagation();
				$("#result").hide();
				$("#blog").show();
				$("#blog-tab").addClass("active");
				$("#result-tab").removeClass("active");
			});

			$("#facebook-tab").click(function(event){
				event.preventDefault();
				event.stopPropagation();
				$("#twitter").hide();
				$("#facebook").show();
				$("#facebook-tab").addClass("active");
				$("#twitter-tab").removeClass("active");
			});
			$("#twitter-tab").click(function(event){
				event.preventDefault();
				event.stopPropagation();
				$("#facebook").hide();
				$("#twitter").show();
				$("#twitter-tab").addClass("active");
				$("#facebook-tab").removeClass("active");
			});

      setInterval(function(){
        $(".alert-success").fadeOut();
        $(".alert-danger").fadeOut();
      }, 5000);
		});
    </script>
  </body>
</html>
