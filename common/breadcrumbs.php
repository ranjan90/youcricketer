<div class="row">
<div class="col-md-12">
<div class="breadcrumb" style="border:1px solid #ccc;float:left;width:100%;">
<div class="breadcrumb-div1" style="float:left;">
	<?php if($page != '' && $page != 'index.html' && $page != 'login.html?status=verified' && $page != '?status=success_individual' && $page != '?status=success_company'){ 
			$community = array('blogs.html', 'forums.html','groups.html','testimonials.html','news.html','events.html','leagues.html','clubs.html', 'other-companies.html');
			
		?>
	
		<a href="<?=WWW?>" title="Home - <?=$site_title?>">Home</a>
		<span></span>
		<? if(in_array($page, $community)){ ?>
		<a itemprop="item" href="<?=WWW?>community.html" title="Community - <?=$site_title?>">Community</a>
		<span></span>
		<? } ?>
		<?  
			if(preg_match('/my-(.*).html/',$page, $match)){
				if($match[0] == 'my-news.html'
				|| $match[0] == 'my-groups.html'
				|| $match[0] == 'my-blog.html'
				|| $match[0] == 'my-forum.html'
				|| $match[0] == 'my-events.html'
				|| $match[0] == 'my-testimonials.html'){
					echo '<a itemprop="item" href="'.WWW.'my-community.html">My Community</a>';
					echo '<span></span>';	
				}
			}
		?>
		<? if(preg_match('/(.*)-detail-/',$page, $match)){

				$br_name 	= $_GET['name'];
				
				if($match[1] == 'blog'){
					$br_page = 'blogs';
				}else if($match[1] == 'forum'){
					$br_page = 'forums';
				}else if($match[1] == 'group-post'){
					$br_page = 'groups';
				}else if($match[1] == 'company'){
					$br_page = 'companies';
				}else{
					if($match[1] != 'news'){
						$br_page = $match[1].'s';	
					}else{
						$br_page = $match[1];	
					}	
				}
				if($match[1] != 'individual' && $match[1] != 'company'){
				if($match[1] == 'press-release') {
	                       // do not put comunity here if press-release found
											}
				else {
					echo '<a itemprop="item" href="'.WWW.'community.html">Community</a>';
					echo '<span></span>';	
					}
				}
				echo '<a itemprop="item" href="'.WWW.$br_page.'.html">'.ucwords($match[1]).'</a>';
				echo '<span></span>';
				echo '<p class="inline">'.ucwords($br_name).'</p>';
		 ?>
		<? }else{
			if(isset($_GET['url'])){
				echo '<a itemprop="item" href="'.WWW.$_GET['type'].'.html">'.ucwords($_GET['type']).'</a>';
				echo '<span></span>';
				echo '<p class="inline">ESPN '.ucwords($_GET['type']).'</p>';
			}elseif($page == 'club-members.html' || $page == 'club-members-search.html'){
				if($page == 'club-members.html'){
					echo '<p class="inline">League/Assoc. Clubs</p>';
				}else{
					echo '<p class="inline">Add League/Assoc. Clubs</p>';
				}
			}elseif($page == 'league-umpires.html' || $page == 'league-umpires-search.html'){
				if($page == 'league-umpires.html'){
					echo '<p class="inline">League Umpires</p>';
				}else{
					echo '<p class="inline">Add Umpires</p>';
				}
			}elseif($page == 'venues/list' || $page == 'venues/add' || $page == 'venues/edit'){
				if($page == 'venues/list'){
					echo '<p class="inline">Venues List</p>';
				}elseif($page == 'venues/add'){
					echo '<p class="inline">Add Venue</p>';
				}else{
					echo '<p class="inline">Edit Venue</p>';
				}
			}elseif(in_array($page,array('league-follow.html','club-follow.html','company-follow.html','company-follow-search.html')) ){
				if($page == 'league-follow.html'){
					echo '<p class="inline">Leagues I Follow</p>';
				}elseif($page == 'club-follow.html'){
					echo '<p class="inline">Clubs I Follow</p>';
				}elseif($page == 'company-follow.html'){
					echo '<p class="inline">Other Companies I Follow</p>';
				}else{
					echo '<p class="inline">Add Clubs/Leagues/Companies I Follow</p>';
				}
			}
			elseif(strpos($_SERVER['REQUEST_URI'],'tournament/') !==false ){
				$url = str_replace('/youcricketer.com1','',$_SERVER['REQUEST_URI']);
				$url = ltrim($url,'/');
				$url_arr = explode('/',$url);;
				//var_dump($url_arr);echo $url;
				if(count($url_arr) == 2 && $url_arr[1] == 'list' ){
					echo 'Tournaments';
				}else{
					if(isset($_SESSION['is_company']) && $_SESSION['is_company'] ==1 && in_array($_SESSION['company_type_id'],array(1,2,3,35))){
						echo '<a itemprop="item" href="'.WWW.'tournament/list">Tournaments</a>';	
					}else{
						$sql = 'Select comp.id as company_id, comp.company_name, comp.company_type_id,co.name as country_name,comp.company_permalink from companies as comp
						left join countries as co on comp.country_id=co.id inner join tournaments as t on t.league_id = comp.id
						WHERE t.id='.$tournament_id;
						$row_company_top = mysqli_fetch_assoc(mysqli_query($conn,$sql));
						echo '<a href="'.WWW.$row_company_top['company_permalink'].'" >'.$row_company_top['country_name'].' '.$row_company_top['company_name'].'</a>';
					} 
				}
				if(in_array($url_arr[1],array('venues','umpires','polls'))){
					echo '<span></span>';
					if($url_arr[2] == 'add' || $url_arr[2] == 'edit'){
						if($url_arr[2] == 'add') $t_id = $url_arr[3];else $t_id = $tournament_id;
						echo '<a itemprop="item" href="'.WWW.'tournament/'.$url_arr[1].'/list/'.$t_id.'">'.ucwords($url_arr[1]).'</a>';
						echo '<span></span>';
						echo '<p class="inline">'.ucwords($url_arr[2]).'</p>';
					}else{
						echo '<p class="inline">'.ucwords($url_arr[1]).'</p>';
					}
				}elseif(in_array($url_arr[1],array('standings','team-standings','clubs','add','edit'))){
					echo '<span></span>';
					if($url_arr[1] == 'add' || $url_arr[1] == 'edit'){
						echo '<p class="inline">'.ucwords($url_arr[1]).' Tournament</p>';
					}elseif($url_arr[1] == 'standings'){
						echo '<p class="inline">'.ucwords($url_arr[1]).'</p>';	
					}elseif($url_arr[1] == 'team-standings'){
						echo '<a itemprop="item" href="'.WWW.'tournament/standings/'.$url_arr[2].'">Standings</a>';	
						echo '<span></span>';
						echo '<p class="inline">'.$team_info['company_name'].' Standings</p>';	
					}
				}elseif(in_array($url_arr[1],array('matches'))){
					echo '<span></span>';
					if(in_array($url_arr[2],array('add','edit','match-header','match-edit-basic'))){
						echo '<a itemprop="item" href="'.WWW.'tournament/'.$url_arr[1].'/list/'.$url_arr[3].'">Schedule & Score Book</a>';
						echo '<span></span>';
						if($url_arr[2] == 'add'){
							echo '<p class="inline">Add New Match Schedule</p>';
						}elseif($url_arr[2] == 'edit'){
							echo '<p class="inline">Match Highlights</p>';
						}elseif($url_arr[2] == 'match-edit-basic'){
							echo '<p class="inline">Edit Match Schedule</p>';
						}else{
							echo '<p class="inline">Match Header</p>';
						}
					}elseif(in_array($url_arr[2],array('list','scoresheet'))){
						if($url_arr[2] == 'list'){
							echo '<p class="inline">Schedule & Score Book</p>';
						}
						if($url_arr[2] == 'scoresheet'){
							$score_link = 0;	
							if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
								if($tournament_owner_id == $_SESSION['ycdc_dbuid']){
									$score_link = 1;	
								}else{
									$sql = "select id from tournament_permissions where tournament_id = $tournament_id and member_id = ".$_SESSION['ycdc_dbuid']." LIMIT 1";
									$rs_permission = mysqli_query($conn,$sql);
									if(mysqli_num_rows($rs_permission)){
										$score_link = 1;
									}
								}
							}
							if($score_link){
								echo '<a itemprop="item" href="'.WWW.'tournament/'.$url_arr[1].'/list/'.$tournament_id.'">Schedule & Score Book</a>';
								echo '<span></span>';
							}
							echo '<p class="inline">'.ucwords($url_arr[2]).'</p>';
						}
					}elseif(in_array($url_arr[2],array('batting'))){
						echo '<a itemprop="item" href="'.WWW.'tournament/'.$url_arr[1].'/list/'.$url_arr[3].'">Schedule & Score Book</a>';
						echo '<span></span>';
						echo '<p class="inline">Scoreboard Innings '.$url_arr[5].'</p>';
					}elseif(in_array($url_arr[2],array('commentary','gallery'))){
						echo '<a itemprop="item" href="'.WWW.'tournament/'.$url_arr[1].'/list/'.$tournament_id.'">Schedule & Score Book</a>';
						echo '<span></span>';
						echo '<p class="inline">'.ucwords($url_arr[2]).'</p>';
					}elseif(in_array($url_arr[2],array('videos'))){
						if(in_array($url_arr[3],array('list'))){
							echo '<a itemprop="item" href="'.WWW.'tournament/'.$url_arr[1].'/list/'.$tournament_id.'">Schedule & Score Book</a>';
							echo '<span></span>';
							echo '<p class="inline">Videos</p>';
						}elseif(in_array($url_arr[3],array('add','edit'))){
							if($url_arr[3] == 'add') $m_id = $url_arr[4];else $m_id = $url_arr[5];	
							echo '<a itemprop="item" href="'.WWW.'tournament/Schedule & Score Book/'.$url_arr[2].'/list/'.$m_id.'">Videos</a>';
							echo '<span></span>';
							echo '<p class="inline">'.ucwords($url_arr[3]).'</p>';
						}
					}
				}elseif(strpos($url_arr[1],'tournament-permissions') !== false){
					echo '<span></span>';
					echo '<p class="inline">Umpires/Referees/Scorers Permission</p>';
				}elseif(strpos($url_arr[1],'public-polls') !== false){
					echo '<span></span>';
					echo '<p class="inline">Polls</p>';
				}
			}else if(isset($_GET['keywords']) && !empty($_GET['keywords'])){
				$pageTokens = explode('-', $page);
				$pageLink   = array();
				for($x=0; $x < count($pageTokens)-1; $x++){
					$pageLink[] = $pageTokens[$x];
				}
				echo '<a itemprop="item" href="'.WWW.'community.html">Community</a>';
				echo '<span></span>';
				echo '<a itemprop="item" href="'.WWW.implode('-',$pageLink).'.html">'.ucwords(implode(' ',$pageLink)).'</a>';
				echo '<span></span>';
				echo '<p class="inline">Searched : '.ucwords($_GET['keywords']).'</p>';
				?>
		 <? }else if(preg_match('/(.*)-category-/', $page, $pageType)){
		 		switch($pageType[1]){
		 			case 'blog':
		 				$pageLinkCat = 'blogs';
		 				$titleLinkCat= 'Blog';
		 				break;
		 			case 'forum':
		 				$pageLinkCat = 'forum';
		 				$titleLinkCat= 'Forum';
		 				break;	
		 		}
		 		echo '<a itemprop="item" href="'.WWW.$pageLinkCat.'.html">'.$titleLinkCat.'</a>';
				echo '<span></span>';
				echo '<a itemprop="item" href="'.WWW.$pageLinkCat.'.html">Categories</a>';
				echo '<span></span>';
				echo cleanForBreadcrumb($_GET['name']);

		 	}else if(isset($_GET['page'])){
		 		$pppage = explode('?', $page);
		 		$phtml  = explode('.', $pppage[0]);
		 		
		 		echo '<a itemprop="item" href="'.WWW.'community.html">Community</a>';
				echo '<span></span>';
				echo '<a itemprop="item" href="'.WWW.$pppage[0].'">'.ucwords($phtml[0]).'</a>';
				echo '<span></span>';
				echo 'Page # '.$_GET['page'];
		 		
		 	}else{ ?>
				<p class="inline">
				<?php 
			
				if(strpos(cleanForBreadcrumb($page), "Profile Information Step 5") === 0)
				{
					echo "Profile Information Step 5";
				}
				
				else if(strpos(cleanForBreadcrumb($page), "Profile Information Step 9") === 0){
					echo "Profile Information Step 9";
				}
				else if(strpos(cleanForBreadcrumb($page), "Profile Information Step 6") === 0){
					echo "Profile Information Step 6";
				}
					else if(strpos(cleanForBreadcrumb($page), "Profile Information Step 7") === 0){
					echo "Profile Information Step 7";
				}
					else if(strpos(cleanForBreadcrumb($page), "Profile Information Step 8") === 0){
					echo "Profile Information Step 8";
				}
			else if(strpos(cleanForBreadcrumb($page), "Inbox Message") === 0){
					echo "Inbox Message";
				}
			else if(strpos(cleanForBreadcrumb($page), "Verify Account") === 0){
					echo "Verify Account";
				}
			else if(strpos(cleanForBreadcrumb($page), "Outbox Message") === 0){
					echo "Outbox Message";
				}
			else if(strpos(cleanForBreadcrumb($page), "Deleted Message") === 0){
					echo "Deleted Message";
				}
		else if(strpos(cleanForBreadcrumb($page), "Sign Up?ref") === 0){
					echo "Sign Up";
				}
				else if(strpos(cleanForBreadcrumb($page), "Searches") === 0){
					$search = explode('-',$page);
					echo "Search result for ".$search[1];
				}

				else if(strpos(cleanForBreadcrumb($page), "Forward Outbox Message") === 0){
					$search = explode('-',$page);
					echo ' Forward Outbox  Message ';
				}


				else if(strpos(cleanForBreadcrumb($page), "Advance Search") === 0){
					
					echo "Advance Search";
				}
				else if(strpos(cleanForBreadcrumb($page), "Update Company Profile 5") === 0){
							
							echo "Company Profile 5";
						}
				else if(strpos(cleanForBreadcrumb($page), "Update Company Profile 4") === 0){
							
							echo "Company Profile 4";
						}
				else if(strpos(cleanForBreadcrumb($page), "Update Company Profile 3") === 0){
							
							echo "Company Profile 3";
						}
				else if(strpos(cleanForBreadcrumb($page), "Update Company Profile 2") === 0){
							
							echo "Company Profile 2";
						}
				else if(strpos(cleanForBreadcrumb($page), "Update Company Profile 1") === 0){
					
					echo "Company Profile 1";
				}
				else {
					echo cleanForBreadcrumb($page);
				}
				?></p>	
		 <? } // if keywords		 
		 } ?>
	
<?php $check = stripos($page, 'world-cup-cricket-2015-');
	if($check !== false){ ?>
<div id="social" class="left">
<a style="background-image:url('<?php echo WWW;?>images/bread-crum-arrow.png') no-repeat !important;" href="<?php echo WWW;?>world-cup-cricket-2015.html" title="World Cup Cricket 2015">Back to World Cup 2015</a>
</div>
<?php } ?>
</div>
<div id="social" class="right" style="float:right;">
	<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <span class='st_facebook_hcount' displayText='Facebook'></span>
    <span class='st_twitter_hcount' displayText='Tweet'></span>
	<span class='st_linkedin_hcount' displayText='LinkedIn'></span>
</div>
</div></div></div>
<div class="clear"></div>
<style>.stButton.stFb, .stButton .stTwbutton, .stButton .stMainServices{height:auto;}
.breadcrumb-div1 span {background: rgba(0, 0, 0, 0) url("<?php echo WWW;?>images/bread-crum-arrow.png") no-repeat scroll 0 2px;color: #666;margin: 0 5px;padding-right: 12px;}
.inline {display: inline;}
</style>
<?php } ?>  