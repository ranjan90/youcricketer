<div class="modal fade" id="loginView" tabindex="-1" role="dialog" aria-labelledby="loginViewModel" aria-hidden="true">
      <div class="modal-dialog" data-mcs-theme="minimal" style="max-height:800px">
        <div class="modal-content">
          <div class="modal-body mCustomScrollbar" data-mcs-theme="minimal" style="max-height:700px">
            <!--div class="row">
				<?php //include('common/social-login.php');?> 
            </div-->
            <div class="row">
			<div class="login_panel clearfix">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>   
              <div class="col-sm-12">
                <h2>Login</h2>
				<a href="javascript: void(0);" onclick="javascript: popup('<?=WWW?>social/api/facebook.php');"><button class="loginBtn loginBtn--facebook">Login with Facebook</button></a>
              </div>
            <form class="form-horizontal" method="post" action="<?php echo WWW; ?>login-box-submit.php">
              <input name="action" value="login" type="hidden">
              <div class="form-group">
                <label class="col-sm-3 control-label">Username/Email: </label>
                <div class="col-sm-9">
                  <input name="email" class="form-control validate[required, custom[email]]" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Password: </label>
                <div class="col-sm-9">
                  <input name="password" class="form-control validate[required]" type="password">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9 text-left">
                  <input value=" Login " class="btn orange full hvr-float-shadow" type="submit">
                </div>
			 </div>
			 <div class="form-group">
                <div class="col-sm-12">
                  <a href="<?php echo WWW; ?>sign-up-individual.html">Register as Individual</a><br>
                  <a href="<?php echo WWW; ?>sign-up-company.html">Register as Club/League/Company</a>
                </div>
              </div>
            </form>
			</div>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.searchView-Model -->
	 </div>