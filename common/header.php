<?php include_once('includes/configuration.php');
	if(isset($_SESSION['ycdc_dbuid']) && $_SESSION['ycdc_dbuid']!=''){
		$userDetail=get_record_on_id('users',$_SESSION['ycdc_dbuid']);
	}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php echo strip_tags($meta_description)?>">
    <meta name="author" content="">
    <link rel="shortcut icon" type="image/ico" href="<?php echo WWW ?>assets/img/favicon.png">
	<meta name="keywords" content="<?php echo $meta_keywords?>">
    <title><?php if(isset($page_title)) echo $page_title.' - Youcricketer Overseas Cricket Agency';else echo $site_title;?></title>

    <!-- Bootstrap core CSS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
    <link href="<?php echo WWW ?>assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo WWW; ?>assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo WWW; ?>assets/css/datepicker.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo WWW; ?>assets/css/style.css" rel="stylesheet">
		<link href="<?php echo WWW; ?>css/jquery-ui.css" rel="stylesheet">
	 <script src="<?php echo WWW; ?>assets/js/jquery.min.js"></script>
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="<?php echo WWW; ?>assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?php echo WWW; ?>assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<?php echo WWW; ?>assets/js/site.js?v=1.1"></script>

	<script src="<?php echo WWW; ?>assets/js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?php echo WWW; ?>assets/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
	<link rel="stylesheet" href="<?php echo WWW; ?>assets/css/validationEngine.jquery.css" type="text/css"/>
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo WWW ?>" title="Home Page - "><img alt="Logo" src="<?php echo WWW; ?>assets/img/logo.png" /></a>
		  <p>Connect  & Discover Cricket Opportunities Globally </p>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <div class="header-top">
            <div class="row">
              <div class="col-sm-6">
                <form method="post"  id="frm-search">
                  <div class="input-group">

                    <input id='ja' name="keywords" type="text" class="form-control" placeholder="Search Individuals/Clubs/Leagues/Organizations" value="<?=$_GET['keyword']?>">
                    <div class="input-group-addon"><button type="button" id="search-btn"><i class="fa fa-search"></i></button></div>
                  </div>
                </form>
				<script type="text/javascript">
					$('#search-btn').click(function(){
			    	var string = $('form#frm-search input[name=keywords]').val();
	        	if(string != ''){
							string = string.replace(/\s+/g,'_').toLowerCase();
							if(string.length > 0){
							window.location = '<?php echo WWW;?>searches-' + string + '.html';
							}
						}else{
							window.location = '<?php echo WWW;?>';
						}
					});

					$('form#frm-search').submit(function(){
			        	var string = $('form#frm-search input[name=keywords]').val();
						if(string != ''){
							string = string.replace(/\s+/g,'_').toLowerCase();
							if(string.length > 0){
								$('form#frm-search').attr("action",'<?php echo WWW;?>searches-' + string + '.html');
							}
						}
					});
			    </script>
				<style type="text/css">.flag img{padding:0px;} @media only screen and (min-width:1280px) {dt img{max-height:170px;}
				#logged-nav > li > a{font-size:15px; padding:0 19px;}  }</style>
              </div>
              <div class="col-sm-3 col-xs-6">
                <a href="<?php echo WWW?>advance-search.html"> Advanced Search </a>
              </div>

              <div class="col-sm-3 col-xs-6">

                <ul class="social"><!--<?php $social_title = 'Youcricketer Sports Social Media' ?>-->
            <?php
            $query=mysqli_query($conn,"select * from user_notifications where user_id='".$_SESSION['ycdc_dbuid']."' and read_status='0' order by id desc ");
            $total=mysqli_num_rows($query);

            ?>
          <!-- <?php if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){ ?>

  				  <li class="dropdown">
  					  <span class="dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-bell"></i>
  						<b><?php echo $total;?></b>
  					  </span>
              <ul class="dropdown-menu">
                <?php
                $query=mysqli_query($conn,"select * from user_notifications where user_id='".$_SESSION['ycdc_dbuid']."' and read_status='0' order by id desc limit 0, 3 ");

                while($rec=mysqli_fetch_assoc($query)){?>

                  <li><a href="notifications.html#<?php echo $rec['id'];?>"><?php echo $rec['message'];?></a></li>

                    <?php } ?>
      						   <li><a href="notifications.html">View All +</a></li>
      					  </ul>
      					</li>
              <?php } ?> -->
                  <?php
                  if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid']) && $_SESSION['ycdc_user_email']=='admin@youcricketer.com'){ ?>

                    <li><a href="<?php echo WWW;?>rss.html"><img alt="RSS" height="25" width="30" src="<?php echo WWW; ?>assets/img/rss.png" /></a></li>

                  <?php } ?>

                  <li><a href="<?php echo $twitter_page_link['value']?>" title="Twitter Page - <?php echo $social_title?>" target="_blank"><img alt="Twitter Page - <?php echo $social_title?>" height="25" width="30" src="<?php echo WWW; ?>assets/img/twitter.png" /></a></li>
                  <li><a href="<?php echo $facebook_page_link['value']?>" title="Facebook Page - <?php echo $social_title?>" target="_blank"><img alt="Facebook Page - <?php echo $social_title?>" height="25" width="30" src="<?php echo WWW; ?>assets/img/facebook.png" /></a></li>
                  <!-- <?php /* ?><li><a href="<?php echo $google_plus_page_link['value']?>" title="Google Plus Page - <?php echo $social_title?>" target="_blank"><img alt="Google Plus Page - <?php echo $social_title?>" height="25" width="30" src="<?php echo WWW;?>images/google.png" /></a></li>
                  <li><a href="<?php echo $pinterest_page_link['value']?>" title="Pinterest Plus Page - <?php echo $social_title?>" target="_blank"><img alt="Pinterest Plus Page - <?php echo $social_title?>" height="25" width="30" src="<?php echo WWW;?>images/pinterest.png" /></a></li>
                  <li><a href="<?php echo $tumblr_page_link['value']?>" title="Tumblr Plus Page - <?php echo $social_title?>" target="_blank"><img alt="Tumblr Plus Page - <?php echo $social_title?>" height="25" width="30" src="<?php echo WWW;?>images/tumblr.png" /></a></li><?php */ ?>
                  <li><a href="#"><img height="25" width="20" src="<?php echo WWW;?>images/youtube.png" /></a></li> -->
                </ul>
              </div>
            </div>
          </div>
		  <?php if(isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
				$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select * from users where email = '".$_SESSION['ycdc_user_email']."'"));
				$_SESSION['ycdc_dbuid'] = $rowUser['id'];
			} ?>
          <div class="signup-buttons">
		  <?php if(!isset($_SESSION['ycdc_user_name']) || empty($_SESSION['ycdc_user_name'])){ ?>
            <div class="row">
              <div class="col-sm-4 col-xs-12">
                <a title="Login - <?php echo $site_title?>" href="<?php echo WWW ?>login.html" class="btn blue full hvr-float-shadow">Login</a>
              </div>
              <div class="col-sm-4 col-xs-12">
                <a title="Individual Sign up - <?php echo $site_title?>" href="<?php echo WWW ?>sign-up-individual.html" class="btn orange full hvr-float-shadow">Sign up as Individual</a>
              </div>
              <div class="col-sm-4 col-xs-12">
                <a title="Company Sign up - <?php echo $site_title?>" href="<?php echo WWW ?>sign-up-company.html" class="btn orange full hvr-float-shadow">Sign up as Club/League/Organization</a>
              </div>
            </div>
		  <?php } else{ ?>
		  <?php $row_user = get_record_on_id('users', $_SESSION['ycdc_dbuid']);
			$row_comp = mysqli_fetch_assoc(mysqli_query($conn,"select * from companies where user_id = '".$row_user['id']."'")); ?>
			<div class="row">
              <div class="col-sm-4 col-xs-12" style="margin-top:10px;">
                <h3 style="display:inline;margin-right:7px;">Welcome </h3> <strong><?php echo $row_user['f_name']?></strong>
              </div>
              <div class="col-sm-4 col-xs-12">
                <a title="My Dashboard - <?php echo $site_title?>" href="<?php echo WWW?>dashboard.html" class="btn orange full hvr-float-shadow">My Dashboard</a>
              </div>
              <div class="col-sm-4 col-xs-12">
                <a title="Logout - <?php echo $site_title?>" href="<?php echo WWW?>logout.html" class="btn orange full hvr-float-shadow">Logout</a>
              </div>
            </div>
		  <?php } ?>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <ul class="nav navbar-nav" <?php if(isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])): ?> id="logged-nav" <?php else: ?> id="top-nav" <?php endif; ?>>
                <li class="active"><a href="<?php echo WWW ?>" title="Home - <?php echo $site_title?>" >Home</a></li>
                <li class="dropdown"><a href="#" title="About Us - <?php echo $site_title?>" class="dropdown-toggle" >About Us</a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<?php echo WWW ?>about-us.html">About Us</a></li>
                        <li><a href="<?php echo WWW ?>who-we-are.html">Who We Are</a></li>
                        <li><a href="<?php echo WWW ?>our-mission.html">Our Mission</a></li>
                        <li><a href="<?php echo WWW ?>our-philosphy.html">Our Philosphy</a></li>
                        <?php if($show_our_team['status'] == '1'){ ?>
                        <li><a href="<?php echo WWW ?>our-team.html">Our Team</a></li>
                        <?php } ?>
                        <?php if($show_how_we_growing['status'] == '1'){ ?>
                        <li><a href="<?php echo WWW ?>how-we-growing.html">How We Are Growing</a></li>
                        <?php } ?>
                    </ul>
                </li>
                <li><a href="<?php echo WWW ?>press-releases.html" title="Press Releases - <?php echo $site_title?>">Press Releases</a></li>
                <li class="dropdown"><a href="#" title="Community - <?php echo $site_title?>" class="dropdown-toggle" >Community</a>
                    <ul class="dropdown-menu" role="menu">

                      <li><a href="<?php echo WWW ?>community.html">Community</a></li>

                      <?php if(isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])): ?>
							<li><a href="<?php echo WWW?>blogs.html" title="Blog">Blogs</a></li>
							<li><a href="<?php echo WWW?>forums.html" title="Forum">Forums</a></li>
							<li><a href="<?php echo WWW?>groups.html" title="Groups">Groups</a></li>
							<li><a href="<?php echo WWW?>testimonials.html" title="Testimonials">Testimonials</a></li>
							<li><a href="<?php echo WWW?>news.html" title="News">Whats Happening</a></li>
							<li><a href="<?php echo WWW?>events.html" title="Events">Events</a></li>
						<?php else: ?>
							<li><a href="<?php echo WWW?>sign-up-community.html?page=blogs" title="Blog">Blogs</a></li>
							<li><a href="<?php echo WWW?>sign-up-community.html?page=forums" title="Forum">Forums</a></li>
							<li><a href="<?php echo WWW?>sign-up-community.html?page=groups" title="Groups">Groups</a></li>
							<li><a href="<?php echo WWW?>sign-up-community.html?page=testimonials" title="Testimonials">Testimonials</a></li>
							<li><a href="<?php echo WWW?>sign-up-community.html?page=member-news" title="News">Whats Happening</a></li>
							<li><a href="<?php echo WWW?>sign-up-community.html?page=events" title="Events">Events</a></li>
						<?php endif; ?>
                    </ul>
                </li>
                <li class="dropdown"><a href="#" class="dropdown-toggle" >Your Region</a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<?php echo WWW ?>in-your-region.html" title="Your Region Blog">Your Region</a></li>

                        <?php if(isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])): ?>
								<li><a href="<?php echo WWW?>your-region-blogs.html" title="Your Region Blog">Blogs</a></li>
								<li><a href="<?php echo WWW?>your-region-forums.html" title="Your Region Forum">Forums</a></li>
								<li><a href="<?php echo WWW?>your-region-groups.html" title="Your Region Groups">Groups</a></li>
								<li><a href="<?php echo WWW?>your-region-testimonials.html" title="Testimonials">Testimonials</a></li>
								<li><a href="<?php echo WWW?>your-region-news.html" title="Your Region News">Whats Happening</a></li>
								<li><a href="<?php echo WWW?>your-region-events.html" title="Your Region Events">Events</a></li>
						<?php else: ?>
								<li><a href="<?php echo WWW?>sign-up-community.html?page=blogs&region=true" title="Your Region Blog">Blogs</a></li>
								<li><a href="<?php echo WWW?>sign-up-community.html?page=forums&region=true" title="Your Region Forum">Forums</a></li>
								<li><a href="<?php echo WWW?>sign-up-community.html?page=groups&region=true" title="Your Region Groups">Groups</a></li>
								<li><a href="<?php echo WWW?>sign-up-community.html?page=testimonials&region=true" title="Testimonials">Testimonials</a></li>
								<li><a href="<?php echo WWW?>sign-up-community.html?page=member-news&region=true" title="Your Region News">Whats Happening</a></li>
								<li><a href="<?php echo WWW?>sign-up-community.html?page=events&region=true" title="Your Region Events">Events</a></li>
						<?php endif; ?>
                    </ul>
                </li>
                <li><a title="Most Favorite - <?php echo $site_title?>" href="<?php echo WWW ?>most-favorite.html">Most Favorite</a></li>
                <li class="dropdown"><a href="<?php echo WWW ?>#" title="FAQs - <?php echo $site_title?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">FAQs</a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<?php echo WWW ?>faqs.html">FAQs</a></li>
                        <li><a href="<?php echo WWW ?>how-to.html">How To</a></li>
                    </ul>
                </li>
                <li><a title="Contact Us - <?php echo $site_title?>" href="<?php echo WWW ?>contact-us.html">Contact Us</a></li>
				<?php if(isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])): ?>
					<li class="dropdown"><a href="#" class="dropdown-toggle"  title="My Account">My Account</a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="<?php echo WWW ?>dashboard.html" title="My Dashboard">My Dashboard</a></li>

							<li><a href="<?php echo WWW ?>my-network.html" title="My Network">My Network</a></li>
							<li><a href="<?php echo WWW ?>inbox.html" title="My Messages">My Messages</a></li>
							<li><a href="<?php echo WWW ?>my-community.html" title="My Community">My Community</a></li>
							<li><a href="<?php echo WWW ?>my-photos.html" title="My Albums" >My Albums</a></li>
							<li><a href="<?php echo WWW ?>league-follow.html" title="My Following">My Following</a></li>
              <li><a href="<?php echo WWW ?>required-person.html" title="Required a Person">Requirement</a></li>

							<?php if($_SESSION['is_company'] == 0):

								if($userDetail['permalink']!=''){
									$pLink=WWW."yci/".$userDetail['permalink'];
								}else{
									$pLink=WWW."individual-detail-".$_SESSION['ycdc_dbuid']."-".friendlyURL($_SESSION['ycdc_user_name']).".html";
								}

							?>
								<li><a href="<?php echo $pLink;?>" title="My Profile">My Profile View</a></li>
							<?php else: ?>
								<li><a href="<?php echo WWW?><?php echo $_SESSION['company_permalink']?>" title="My Profile">My Profile View</a></li>
							<?php endif; ?>
						</ul>
					</li>
				<?php endif; ?>
              </ul>
            </div>
          </div>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
