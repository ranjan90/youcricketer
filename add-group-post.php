<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	<div class="middle">
		<h1>Add Group Post </h1>
		<? 	if(isset($_POST) && !empty($_POST)){ 
				$title 	= $_POST['title'];
				$content= $_POST['content'];
				$date 		= date('Y-m-d H:i:s');
				$user_id 	= $_SESSION['ycdc_dbuid'];
				$group_id 	= $_GET['id'];


				if(!isset($user_id) || empty($user_id)){
					echo '<div id="error">Please login first ... !</div>';
				}else{
					if(!empty($title) && !empty($content)){
						$rs_check = mysqli_query($conn,"select * from group_posts where user_id = '$user_id' and group_id = '$group_id' and title = '$title'");
						if(mysqli_num_rows($rs_check) == 0){
							$query = "insert into group_posts (title, description, user_id, group_id, status, post_date) 
							values ('$title','$content','$user_id','$group_id','1','$date')";

							if(mysqli_query($conn,$query)){
								echo '<div id="success">Groups Information added successfully ... !</div>';	
								?>
				                <script>
				                window.location = "<?=WWW?>group-detail-<?=$group_id?>-<?=friendlyURL($_GET['name'])?>.html";
				                </script>
				                <?
							}			                
			                
						}else{
							echo '<div id="error">Information already exists ... !</div>';
						}
					}else{
						echo '<div id="error">Please fill all fields ... !</div>';
					}
				}
				
			}
		?>
		<div class="white-box content">
			<form method="post" enctype="multipart/form-data">
				<fieldset>
					<h2>Group Post Information</h2>
					<div class="form-box">
						<label>Title</label>
						<input type="text" name="title" class="input-login validate[required]">
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Content</label>
						<textarea maxlength="500" name="content" style="width:600px; margin-left:110px; height:100px;" class="validate[required]"></textarea>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<input type="submit" value=" Save " class="submit-login">
					</div>
					
				</fieldset>
			</form>
		</div>
		<div class="rightbar"><?php include('common/right-panel.php');?></div>
		<div class="clear"></div>
	</div>
<?php include('common/footer.php'); ?>