<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
<?php 
if(isset($_POST['delete_noti_submit']) && !empty($_POST['delete_noti_submit'])){
	$ids_str = implode(',',$_POST['del_ids']);
	$sql = "Update friendship SET notification_status = 'read' WHERE id IN($ids_str) and to_user_id = ".$_SESSION['ycdc_dbuid'];
	mysqli_query($conn,$sql);
	echo '<script>window.location = "'.WWW.'network-notifications.html";</script>';
}
?>

<div class="page-container"> 
		<?php  include('common/user-left-panel.php');?>
      <!-- END SIDEBAR -->
      
      <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
		<div class="page-content">
		
		
		<div class="white-box">
            <div class="row">
              <div class="col-sm-12">
                <h2> Network Invitation Notifications </h2>
              </div>
            </div>
			
			<form method="post" class="form-horizontal" >
			<div class="form-group">
				<div class="col-sm-12">
				
				<? 	$rsFriends = mysqli_query($conn,"select * from friendship where to_user_id = '".$_SESSION['ycdc_dbuid']."' and status = '1' and notification_status = 'unread' order by update_date desc");
					while($rowF = mysqli_fetch_assoc($rsFriends)){ 
						$rowUser= get_record_on_id('users', $rowF['from_user_id']);
						?>
					<p><input type="checkbox" name="del_ids[]" value="<?php echo $rowF['id']; ?>"> <a title="Click for more detail <?=$rowUser['f_name'].' '.$rowUser['last_name'];?>" href="<?=WWW?>individual-detail-<?=$rowUser['id'];?>-<?=friendlyURL($rowUser['f_name'].' '.$rowUser['last_name']);?>.html#activity-tab"><?=$rowUser['f_name'].' '.$rowUser['last_name']?></a> is now your friend. </p>
				<? 	} ?>
			    
				<?php if(mysqli_num_rows($rsFriends)): ?>
					<input type="submit" name="delete_noti_submit" value="Delete Notifications" class="btn orange hvr-float-shadow">
				<?php else: ?>	
					<div id="information" class="alert alert-info"><i class="fa fa-exclamation-triangle"> </i> No Notifications</div>
				<?php endif; ?>
				
				</div>
			</div>
			</form>
            
        </div>
		
		</div>
	</div>
</div>

<?php include('common/footer.php'); ?>