<?php include_once('includes/configuration.php');
$page = 'tournament-standings.html';
$selected_country = getGeoLocationCountry(); 

$match_info = array();
$match_id = trim($_GET['m_id']);
$match_info = get_record_on_id('tournament_matches', $match_id);	
$tournament_id = $match_info['tournament_id'];
$tournament_info = get_record_on_id('tournaments', $tournament_id);	

if(empty($match_info['toss_won_team_id']) || empty($match_info['toss_won_decision']) || empty($match_info['venue_id'])){
	$_SESSION['basic_info_error'] = 1;
	header("Location:".WWW."tournament/matches/match-header/$tournament_id/".$match_id);
}

$page_title = 'Commentary - '.ucwords($tournament_info['title']);

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
}

$tournament_permission = 0;

if(isset($user_id) && !empty($user_id)){
	$sql = "select * from tournament_permissions where tournament_id = $tournament_id and member_id = $user_id LIMIT 1";
	$rs_permission = mysqli_query($conn,$sql);
	if($tournament_info['user_id'] == $user_id || mysqli_num_rows($rs_permission)>0 ){
		$tournament_permission = 1;
	}else{
		$tournament_permission = 0;
	}
}else{
	$tournament_permission = 0;
}

$commentary_data = array();
$batting_team_data = array();
$bowling_team_data = array();
$error = '';
$how_out_arr = array('Bowled','Caught','Caught & Bowled','Caught Behind','DNB','Handled Ball','Hit Wicket','Hit the ball twice','LBW','Not Out',
'Obstructed Fielder','Retired Hurt','Retired Out','Run Out','Stumped');

$team_id = trim($_GET['t_id']);
//$team_info = get_record_on_id('companies', $team_id);	

$team_batting = $team_id;
if($match_info['team1'] == $team_batting){
	$team_bowling = $match_info['team2'];
}else{
	$team_bowling = $match_info['team1'];
}

$batting_team_info = get_record_on_id('companies', $team_batting);
$bowling_team_info = get_record_on_id('companies', $team_bowling);

$sql = "SELECT u.* FROM club_members as c inner join users as u on c.member_id=u.id WHERE c.club_id=$team_batting ORDER BY c.id ASC";
$rs_team_batting = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_team_batting)){
	$batting_team_data[] = $row;
}

$sql = "SELECT u.* FROM club_members as c inner join users as u on c.member_id=u.id WHERE c.club_id=$team_bowling ORDER BY c.id ASC";
$rs_team_bowling = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_team_bowling)){
	$bowling_team_data[] = $row;
}

$sql  = "Select * from tournament_match_commentary WHERE match_id=$match_id and team_id=$team_id ORDER BY id DESC LIMIT 30";
$rs_commentary = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_commentary)){
	$commentary_data[] = $row;
}

?>
<?php include('common/header.php'); ?>

<div class="page-container"> 
		<?php  include('common/user-left-panel.php');?>
      <!-- END SIDEBAR -->
      
      <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
		<div class="page-content">
		<?php  include('common/breadcrumbs.php');?>
		
		<div class="white-box">
            <div class="row">
              <div class="col-sm-12">
                <h2> Commentary - <?php echo $batting_team_info['company_name']; ?> vs <?php echo $bowling_team_info['company_name']; ?> - <?php echo date('d M Y',strtotime($match_info['start_time'])); ?> </h2>
                <h3> <?php echo $batting_team_info['company_name']; ?> Innings </h3>
                <h3><a href="<?php echo WWW; ?>tournament/matches/list/<?php echo $tournament_info['id']; ?>"><h2><?php echo ucwords($tournament_info['title']); ?></h2></a> </h3>
				<?php if(!empty($error)): ?>
					<?php echo $error; ?>
				<?php endif; ?>
              </div>
            </div>
            
			<?php if($tournament_permission){ ?>
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="form-group">
                  <div class="col-sm-12">
                    <h3> Add Commentary </h3>
					<div id="error_input"  class="alert alert-danger error" ></div><div class="clear"></div>
                  </div>
                </div>
                <form class="form-horizontal" id="commentary_form" method="post" action="">
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Over</label>
                    <div class="col-sm-4">
                      <select name="overs" id="overs" class="form-control">
                        <option value="">Over</option>
						<?php for($i=0;$i<$match_info['maximum_overs'];$i++): ?>
							<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
						<?php endfor; ?>
                      </select>
                    </div>
                    <label class="col-sm-2 control-label">Ball</label>
                    <div class="col-sm-4">
                      <select name="balls" id="balls" class="form-control">
                        <option value="">Ball</option>
						<?php for($i=1;$i<=6;$i++): ?>
							<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
						<?php endfor; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Bowler</label>
                    <div class="col-sm-4">
                      <select name="bowler_id" id="bowler_id" class="form-control">
                        <option value="">Bowler</option>
						<?php for($k=0;$k<count($bowling_team_data);$k++): ?>
							<option value="<?php echo $bowling_team_data[$k]['id']; ?>" <?php echo $sel; ?>><?php echo $bowling_team_data[$k]['f_name']; ?> <?php echo $bowling_team_data[$k]['last_name']; ?></option>
						<?php endfor; ?>
                      </select>
                    </div>
                    <label class="col-sm-2 control-label">Batsman</label>
                    <div class="col-sm-4">
                      <select name="batsman_id" id="batsman_id" class="form-control">
                       <?php for($k=0;$k<count($batting_team_data);$k++): ?>
							<option value="<?php echo $batting_team_data[$k]['id']; ?>" <?php echo $sel; ?>><?php echo $batting_team_data[$k]['f_name']; ?> <?php echo $batting_team_data[$k]['last_name']; ?></option>
						<?php endfor; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Ball Type</label>
                    <div class="col-sm-4">
                      <select name="ball_type" id="ball_type" class="form-control">
                        <option value="">Ball Type</option>
                        <option value="legal_ball">Legal Ball</option>
                        <option value="wide_ball">Wide Ball</option>
                        <option value="no_ball">No Ball</option>
                      </select>
                    </div>
                    <label class="col-sm-2 control-label">Runs Scored</label>
                    <div class="col-sm-4">
                      <select name="runs" id="runs" class="form-control">
                       <option value="">Runs Scored</option>
						<?php for($i=0;$i<=6;$i++): ?>
							<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
						<?php endfor; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Runs Type</label>
                    <div class="col-sm-4">
                      <select name="runs_type" id="runs_type" class="form-control">
                        <option value="">Runs Type</option>
						<option value="batsman_runs">Batsman Runs</option>
						<option value="byes">Byes</option>
						<option value="leg_byes">Leg Byes</option>
                      </select>
                    </div>
                    <label class="col-sm-2 control-label">Batsman Out</label>
                    <div class="col-sm-4">
                      <select name="batsman_out" id="batsman_out" class="form-control">
                        <option value="">Batsman Out</option>
						<option value="yes">Yes</option>
						<option value="no">No</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Fielder</label>
                    <div class="col-sm-4">
                      <select name="fielder_id" id="fielder_id" class="form-control">
                       <option value="">Fielder</option>
						<?php for($k=0;$k<count($bowling_team_data);$k++): ?>
							<option value="<?php echo $bowling_team_data[$k]['id']; ?>" <?php echo $sel; ?>><?php echo $bowling_team_data[$k]['f_name']; ?> <?php echo $bowling_team_data[$k]['last_name']; ?></option>
						<?php endfor; ?>
                      </select>
                    </div>
                    <label class="col-sm-2 control-label">How Out</label>
                    <div class="col-sm-4">
                      <select name="how_out" id="how_out" class="form-control">
                        <option value="">How Out</option>
						<?php for($k=0;$k<count($how_out_arr);$k++): ?>
							<option value="<?php echo $how_out_arr[$k]; ?>" <?php echo $sel; ?>><?php echo $how_out_arr[$k]; ?></option>
						<?php endfor; ?>
                      </select>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <div class="col-sm-offset-5 col-sm-7">
                      <input name="update_commentary_btn" id="update_commentary_btn" value="Update" onClick="updateCommentary();" class="btn orange hvr-float-shadow" type="button">
                    </div>
                  </div>
                </form>
              </div>
            </div>
            
				 <div id="loading" >Loading...</div>
			 
				<div id="commentary_div"></div>
			
			<?php }else{ ?><br>
				<div id="error" class="alert alert-danger">You do not have Permission for this Tournament... !</div>
			<?php } ?>	
            
          </div>
		
		
		</div>
	</div>
</div>		 

<?php if($tournament_permission){ ?>	
<script type="text/javascript">
$("document").ready(function(){
	
	updateCommentary = function(){
		var error = '';
		if($("#overs").val() == '' || $("#balls").val() == '' || $("#batsman_id").val() == '' || $("#bowler_id").val() == ''){
			error = 'These fields are required:<br>Over, Ball, Batsman, Bowler';
		}
		if(error == '' && $("#ball_type").val() != 'legal_ball' && $("#batsman_out").val() == 'yes' && ($("#how_out").val() != 'DNB' && $("#how_out").val() != 'Obstructed Fielder' && $("#how_out").val() != 'Retired Hurt' && $("#how_out").val() != 'Retired Out' && $("#how_out").val() != 'Run Out' && $("#how_out").val() != 'Stumped')){
			error = 'Batsman can be Out only on Legal Ball';
		}
		if(error == '' && ($("#runs").val() != '' && $("#runs").val()>0) && $("#batsman_out").val() == 'yes' && ( $("#how_out").val() != 'Obstructed Fielder' && $("#how_out").val() != 'Retired Hurt' && $("#how_out").val() != 'Retired Out' && $("#how_out").val() != 'Run Out' )  ){
			error = 'Runs can not be added with Batsman out';
		}
		if(error == '' && $("#how_out").val() == '' && $("#batsman_out").val() == 'yes'){
			error = 'How out is required with Batsman out';
		}
		if(error!=''){
			$("#error_input").html("Error: "+error).show();
			return false;
		}else{
			$("#error_input").hide();
			$("#loading").show();
		}
		
		if($("#batsman_out").val() != 'yes'){
			$("#how_out").val('');
			$("#fielder_id").val('');
		}
		
		var overs = $("#overs").val()+"."+$("#balls").val();
		$.ajax({
			url: "<?php echo WWW; ?>update_commentary.php?t=<?php echo time(); ?>",
			type:"POST",
			data:"match_id=<?php echo $match_id; ?>&team_id=<?php echo $team_id; ?>&batsman_id="+$("#batsman_id").val()+"&bowler_id="+$("#bowler_id").val()+"&ball_score="+$("#runs").val()+"&team_overs="+overs+"&ball_type="+$("#ball_type").val()+"&fielder_id="+$("#fielder_id").val()+"&batsman_out="+$("#batsman_out").val()+"&how_out="+$("#how_out").val()+"&runs_type="+$("#runs_type").val(),
			success: function(data) {
				var data = $.parseJSON(data);
				if(data.error == 'no'){
					getCommentary(1);
					$("#loading").hide();
				}else{
					$("#error_input").html("Error: "+data.error_msg).show();
					$("#loading").hide();
				}
			}
		});
	}
	
	getCommentary = function(p){
		$.ajax({
			url: "<?php echo WWW; ?>get_commentary.php?t=<?php echo time(); ?>",
			type:"POST",
			data:"match_id=<?php echo $match_id; ?>&team_id=<?php echo $team_id; ?>&p="+p,
			success: function(data) {
				$("#commentary_div").html(data);
			}
		});
	}
	getCommentary(1);
	
	deleteCommentary = function(id){
		
		$.ajax({
			url: "<?php echo WWW; ?>update_commentary.php?t=<?php echo time(); ?>",
			type:"POST",
			data:"id="+id+"&action=delete&match_id=<?php echo $match_id; ?>",
			success: function(data) {
				getCommentary(1);
			}
		});
	}
});
</script>
<?php } ?>

<style type="text/css">
#commentary_div {clear:both;}
.over_end{background-color:#FB8E64;float:left;padding:4px 2px 2px 1px;margin:5px 0px 5px 0px;width:100%;font-size:12px;}
.ball_desc{float:left;font-size:12px;padding:2px;}
.no_ball{color:#E59B1B;font-weight:bold;}
.four_six {color:#000099;font-weight:bold;}
.batsman_out{color:#ff0000;font-weight:bold;}
#error_input{display:none;}
#loading{float:left;display:none;font-weight:bold;color:#94BF1D}
.delete_row{color:#ff0000;}
</style>

<?php include('common/footer.php'); ?>