<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>

<?php $msg = '';
if(isset($_POST['action']) && $_POST['action'] == 'send'){ 
						
						if(!empty($_POST['subject']) && !empty($_POST['message']) && !empty($_POST['to'])){
							$message 	= $_POST['message'];
							$subject 	= $_POST['subject'];
							$from_user_id = $_SESSION['ycdc_dbuid'];
							$date 		= date('Y-m-d H:i:s');
							$toEmails 	= explode('),', $_POST['to']);
							
							$counter 	= 0;
							foreach($toEmails as $te){
								$complete = explode('(', $te);
								$info 	  = explode(',', $complete[1]);
								$role  	  = $info[0];
								$country  = str_replace(')','',$info[1]);
								$rowRole = mysqli_fetch_assoc(mysqli_query($conn,"select * from user_types where name = '".trim($role)."'"));
								$rowcountry = mysqli_fetch_assoc(mysqli_query($conn,"select * from countries where name = '".trim($country)."'"));
								$name = explode(' ', $complete[0]);
								$rowUser= mysqli_fetch_assoc(mysqli_query($conn,"select * from users where f_name = '".$name[0]."' and last_name = '".$name[1]."' 
																		and user_type_id = '".$rowRole['id']."' and country_id = '".$rowcountry['id']."'"));
								/********************************/
								$to_user_id = $rowUser['id'];
								if(!empty($to_user_id)){
										$query = "insert into messages (from_user_id, to_user_id, message_date, subject, message, status, thread_id) 
										values ('$from_user_id','$to_user_id','$date','$subject','$message','0','0'); ";

										$email_template = get_record_on_id('cms', 16);
										$mail_title		= $email_template['title'];
							            $mail_content	= $email_template['content'];
							            $mail_content 	= str_replace('{to_name}',$rowUser['f_name'], $mail_content);
						                $mail_content 	= str_replace('{login_link}','<a href="'.WWW.'inbox.html" title="Login to your account">HERE</a>', $mail_content);
						                $mail_content 	= str_replace('{site_logo}','<a href="'.WWW.'" title="'.$site_title.'"><img src="'.WWW.'images/logo.png" alt="'.$site_title.'"></a>', $mail_content);
										
											$headers = "Content-type:text/html;charset=UTF-8" . "\r\n";
											$headers .= "From: no-reply@youcricketer.com" . "\r\n";
							
										if(mysqli_query($conn,$query) && mail($rowUser['email'],$mail_title,$mail_content,$headers)){
											$counter++;
										}
									
								}
							}
							if($counter > 0){
								$msg =  '<div id="success" class="alert alert-success"> Message sent successfully</div>';
							}
						}else{
								$msg =  '<div id="error" class="alert alert-danger"> Please fill all fields ... !</div>';
						}
}
?>


<div class="page-container"> 
		<?php  include('common/user-left-panel.php');?>
      <!-- END SIDEBAR -->
      
      <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
		<div class="page-content">
		
		<div class="white-box">
            <div class="row">
              <div class="col-sm-12">
                <h2> Compose Message </h2>
				<?php echo $msg; ?>
              </div>
            </div>
			
			<form method="post" action="" enctype="multipart/form-data" class="form-horizontal req-frm">
              <input name="action" value="send" type="hidden">
              <div class="alert alert-info">Fill all fields</div>
              <div class="form-group">
                <label class="col-sm-4 control-label"> To : </label>
                <div class="col-sm-8">
					<textarea class="" id="to" name="to" style="height:30px; width:100%;box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;"></textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label"> Subject : </label>
                <div class="col-sm-8">
                  <input name="subject" class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label"> Message : </label>
                <div class="col-sm-8">
                  <textarea class="form-control validate[required]" name="message" rows="5"></textarea>
                </div>
              </div>
              
              <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
					<input type="hidden" name="action" value="send">
                  <input class="btn orange hvr-float-shadow" value="Send Message" type="submit">
                </div>
              </div>
            </form>
          </div>
		
		</div>
	</div>
</div>		

<script type="text/javascript" src="<?=WWW?>js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?=WWW?>js/select2.js"></script>
<link href="<?=WWW?>css/jquery-ui.css" type="text/css" rel="stylesheet">
<link href="<?=WWW?>css/select2.css" type="text/css" rel="stylesheet">

<script type="text/javascript">
var WWW = "<?=WWW;?>";
$(function(){
	function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }
	
	 $.ajax({type	: 'POST', 
		   	url		: WWW + 'includes/get-users.php', 
			dataType: 'json',
			success	: function(msg){
				$("#to").select2({tags:msg});
			}
    });
	/*****************************************************/
	
	$('.req-frm').validationEngine();
});
</script>
<?php include('common/footer.php'); ?>