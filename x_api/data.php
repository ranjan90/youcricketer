<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
include_once("api.php");

$api	=	new Api();

$api->domainName	=	"http://dev1.youcricketer.com/";
//$api->getHomeUsers(5);
if(sizeof($_GET)>0){
	$request =	$api->getParam('request');
	
	//get users list to display at home page
	if($request=='homeUsers'){
			$offset =	$api->getParam('offset');
			$api->getHomeUsers($offset);
	}
		
	//Load companies list to display at home page
	if($request=='homeCompanies'){
			$offset =	$api->getParam('offset');
			$api->getHomeCompanies($offset);
	}
	if($request=='registerUser'){
			
			$api->registerUser($_POST);
			
	}
	if($request=='userLogin'){
			
			$api->userLogin($_POST);
			
	}
}
?>