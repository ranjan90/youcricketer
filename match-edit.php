<?php include_once('includes/configuration.php');
$page = 'match-edit.html';
$selected_country = getGeoLocationCountry(); 

$error = '';
$umpires = array();
$venues = array();
$tournament_permission = 0;
$tournament_id = trim($_GET['t_id']);
$tournament_info = get_record_on_id('tournaments', $tournament_id);	
$match_id = trim($_GET['m_id']);
$match_info = get_record_on_id('tournament_matches', $match_id);	
$abandon_conditions = array('N/A','Forfeit by One Team','Forfeit by Both Teams','Rain',  'Late Arrival of One Team', 'Late Arrival of Both Teams',
'Unplayable Pitch conditions', 'Unplayable Ground Conditions', 'Unplayable Pitch and Ground', 'Umpire(s) Did not Show Up');
$abandon_array = array('Forfeit by One Team','Late Arrival of One Team','Umpire(s) Did not Show Up') ; 

$page_title = 'Match Highlights - '.ucwords($tournament_info['title']);

if((empty($match_info['completely_abandon_due_to']) || $match_info['completely_abandon_due_to'] == 'N/A') && (empty($match_info['toss_won_team_id']) || empty($match_info['toss_won_decision'])  || empty($match_info['venue_id']))){
	$_SESSION['basic_info_error'] = 1;
	header("Location:".WWW."tournament/matches/match-header/$tournament_id/".$match_id);
	exit();
}

/*if(in_array($match_info['completely_abandon_due_to'],$abandon_array) && (empty($match_info['toss_won_team_id']) || empty($match_info['toss_won_decision'])  || empty($match_info['venue_id']))){
	$_SESSION['basic_info_error'] = 1;
	header("Location:".WWW."tournament/matches/match-header/$tournament_id/".$match_id);
	exit();
}*/

$sql = "SELECT tm.*,c1.id as batting_team1_id,c2.id as batting_team2_id,c1.company_name as batting_team1_name,c2.company_name as batting_team2_name,c3.company_name as toss_won_team_name FROM tournament_matches as tm inner join companies as c1 on tm.batting_team1=c1.id  inner join companies as c2 on tm.batting_team2=c2.id
left join companies as c3 on tm.toss_won_team_id=c3.id WHERE tm.id = $match_id ";
$rs_match = mysqli_query($conn,$sql);
$match_teams = mysqli_fetch_assoc($rs_match);

$sql = "SELECT u.id,u.f_name,u.m_name,u.last_name FROM club_members as c inner join users as u on c.member_id=u.id WHERE c.club_id=".$match_teams['batting_team1_id']." ORDER BY u.f_name ASC";
$rs_team_batting = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_team_batting)){
	$team1_players[] = $row;
}

$sql = "SELECT u.id,u.f_name,u.m_name,u.last_name FROM club_members as c inner join users as u on c.member_id=u.id WHERE c.club_id=".$match_teams['batting_team2_id']." ORDER BY u.f_name ASC";
$rs_team_batting = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_team_batting)){
	$team2_players[] = $row;
}

$match_team_ids = array($match_info['team1'],$match_info['team2']);
	
if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);	
	if($user_id){
		$sql = 'select c.* from tournament_teams as t inner join companies as c on t.team_id=c.id where t.tournament_id = '.$tournament_id;
		$rs_club = mysqli_query($conn,$sql);
		while($row = mysqli_fetch_assoc($rs_club)){
			$tournament_clubs[] = $row;
		}
		
		$sql = 'select * from companies where user_id = '.$tournament_info['user_id'];
		$rs_league = mysqli_query($conn,$sql);
		$league_info = mysqli_fetch_assoc($rs_league);
		
		$sql = 'select c.* from league_clubs as t inner join companies as c on t.club_id=c.id where t.league_id = '.$league_info['id'];
		$rs_club = mysqli_query($conn,$sql);
		while($row = mysqli_fetch_assoc($rs_club)){
			$league_clubs[] = $row;
		}
	}
}

if(isset($user_id) && !empty($user_id)){
	$sql = "select * from tournament_permissions where tournament_id = $tournament_id and member_id = $user_id LIMIT 1";
	$rs_permission = mysqli_query($conn,$sql);
	if($tournament_info['user_id'] == $user_id || mysqli_num_rows($rs_permission)>0 ){
		$tournament_permission = 1;
	}else{
		$tournament_permission = 0;
	}
}else{
	$tournament_permission = 0;
}

$sql = "select u.f_name,u.m_name,u.last_name,u.id as user_id from users as u inner join tournament_permissions as tm on u.id=tm.member_id
 where tm.tournament_id = $tournament_id ";
$rs_umpires = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_umpires)){
	$umpires[] = $row;
}

$sql = "SELECT id,venue FROM league_venues WHERE league_id = ".$league_info['id']." ORDER BY id ";
$rs_venues = mysqli_query($conn,$sql);
while($row = mysqli_fetch_assoc($rs_venues)){
	$venues[] = $row;
}

if(isset($_SESSION['toss_added']) && $_SESSION['toss_added']==1) {
	$toss_added = 1;
	unset($_SESSION['toss_added']);
}

if(isset($_SESSION['basic_info_error']) && $_SESSION['basic_info_error']==1) {
	$basic_info_error = 1;
	unset($_SESSION['basic_info_error']);
}

if(isset($_POST['submit_btn']) && !empty($_POST['submit_btn'])){
	validate2();
	if(empty($error)){
		$date = date('Y/m/d H:i:s')	;
		if(empty($_POST['team1_score'])) $_POST['team1_score'] = 0;
		if(empty($_POST['team2_score'])) $_POST['team2_score'] = 0;
		if(empty($_POST['team1_overs'])) $_POST['team1_overs'] = 0;
		if(empty($_POST['team2_overs'])) $_POST['team2_overs'] = 0;
		if(empty($_POST['winning_team_id'])) $_POST['winning_team_id'] = 0;
		if(empty($_POST['disciplinary_issues'])) $_POST['disciplinary_issues'] = 0;
		
		if(isset($_POST['start_time'])){
			//$start_time = trim($_POST['start_time']);
		}else{
			//$start_time = date('Y/m/d',strtotime($match_info['start_time'])).' '.$_POST['start_hours'].':'.$_POST['start_minutes'];
		}
		
		if(isset($_POST['man_of_match1']) && !empty($_POST['man_of_match1'])){
			$man_of_match = $_POST['man_of_match1'];
		}elseif(isset($_POST['man_of_match2']) && !empty($_POST['man_of_match2'])){
			$man_of_match = $_POST['man_of_match2'];
		}else{
			$man_of_match = 0;
		}
			
		$start_time = $match_info['start_time'];
		
		$sql = " UPDATE tournament_matches SET start_time='".trim($start_time)."',
		team1_score='".trim($_POST['team1_score'])."',team1_wickets='".trim($_POST['team1_wickets'])."',
		team1_overs='".trim($_POST['team1_overs'])."',team2_score='".trim($_POST['team2_score'])."',team2_wickets='".trim($_POST['team2_wickets'])."',
		team2_overs='".trim($_POST['team2_overs'])."',winning_team_id='".trim($_POST['winning_team_id'])."',match_result_text='".trim($_POST['match_result_text'])."',
		highlights_text='".trim($_POST['highlights_text'])."',disciplinary_issues='".trim($_POST['disciplinary_issues'])."',man_of_match_player_id=$man_of_match,
		disciplinary_issues_text='".trim($_POST['disciplinary_issues_text'])."',updated_on='".$date."',last_updated_by = $user_id
		WHERE id=".$match_id;
		
		if(mysqli_query($conn,$sql)){
			$_SESSION['match_highlight_updated'] = 1;
			header("Location:".WWW."tournament/matches/list/".$tournament_id);
		}else{
			$error = '<p id="error" class="alert alert-danger">Error in updating Match. Try again later</p>';
		}
	}
}

function validate2(){
	global $error,$match_info;
	
	if(isset($_POST['start_time']) && empty($_POST['start_time'])){
		$error.= '<p id="error" class="alert alert-danger">Start Date/Time is required field</p>';
	}
	
	if(isset($_POST['winning_team_id']) && empty($_POST['winning_team_id'])){
		$error.= '<p id="error" class="alert alert-danger">Match Result is required field</p>';
	}
	
	// if match won by first team, second team or tied, then man of match is required field
	if(($_POST['winning_team_id']>0 && (empty($match_info['completely_abandon_due_to']) || $match_info['completely_abandon_due_to'] == 'N/A')) || $_POST['winning_team_id'] == -1 ){
		if(empty($_POST['man_of_match1']) && empty($_POST['man_of_match2'])){
			$error.= '<p id="error" class="alert alert-danger">Man of the Match is required field</p>';
		}
	}
}

if(isset($_POST['submit_toss_btn']) && !empty($_POST['submit_toss_btn'])){
	validate1();
	if(empty($error)){
		$date = date('Y/m/d H:i:s')	;
		
		if($_POST['toss_won_decision'] == 'bat'){
			$batting_team1 = $_POST['toss_won_team_id'];
			if($_POST['toss_won_team_id'] == $match_info['team1']){
				$batting_team2 = $match_info['team2'];
			}else{
				$batting_team2 = $match_info['team1'];
			}
		}else{
			$batting_team2 = $_POST['toss_won_team_id'];
			if($_POST['toss_won_team_id'] == $match_info['team1']){
				$batting_team1 = $match_info['team2'];
			}else{
				$batting_team1 = $match_info['team1'];
			}
		}
		
		if(isset($_POST['start_time'])){
			//$start_time = trim($_POST['start_time']);
		}else{
			//$start_time = date('Y/m/d',strtotime($match_info['start_time'])).' '.$_POST['start_hours'].':'.$_POST['start_minutes'];
		}
		$start_time = $match_info['start_time'];
		
		$sql = " UPDATE tournament_matches SET start_time='".trim($start_time)."', toss_won_team_id=".$_POST['toss_won_team_id'].",
		batting_team1 = $batting_team1,batting_team2 = $batting_team2,toss_won_decision='".trim($_POST['toss_won_decision'])."',
		maximum_overs='".trim($_POST['maximum_overs'])."'
		WHERE id=".$match_id;
		
		if(mysqli_query($conn,$sql)){
			$_SESSION['toss_added'] = 1;
			header("Location:".WWW."tournament/matches/edit/$tournament_id/".$match_id);
		}else{
			$error = '<p id="error" class="alert alert-danger">Error in updating Match. Try again later</p>';
		}
	}
}

function validate1(){
	global $error;
	
	if(isset($_POST['start_time']) && empty($_POST['start_time'])){
		$error.= '<p id="error" class="alert alert-danger">Start Date/Time is required field</p>';
	}
	
	if(empty($_POST['toss_won_team_id'])){
		$error.= '<p id="error" class="alert alert-danger">Toss Won By is required field</p>';
	}
	if(empty($_POST['toss_won_decision'])){
		$error.= '<p id="error" class="alert alert-danger">Elected To is required field</p>';
	}
}

?>
<?php include('common/header.php'); ?>

<div class="page-container"> 
		<?php  include('common/user-left-panel.php');?>
      <!-- END SIDEBAR -->
      
      <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
		<div class="page-content">
		<?php  include('common/breadcrumbs.php');?>
		
		    <div class="white-box">
            <div class="row">
              <div class="col-sm-12">
                <h2> Match Highlights </h2>
                <h3> Tournament: <?php echo $tournament_info['title'] ?> </h3>
              </div>
            </div>
            
            <div class="row">
              <div class="col-sm-12">
                <h3> Match Highlights </h3>
				<?php if(empty($user_info)): ?>
					<div id="error" class="alert alert-danger">You are not logged... !</div>
				<?php endif; ?>
				
				<?php if(!empty($error)): ?>
					<?php echo $error; ?>
				<?php endif; ?>
				
				<?php if(isset($toss_added) && $toss_added == 1): ?>
					<div id="information" class="alert alert-danger">Basic information updated Successfully. You can now add detailed information about Match.</div>
				<?php endif; ?>
				
				<?php if(isset($basic_info_error) && $basic_info_error == 1): ?>
					<div id="error" class="alert alert-danger">Fill the basic information before updating Scoreboard or Commentary.</div>
				<?php endif; ?>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
			  <?php if($tournament_permission){ ?>
			  <?php if($match_info['completely_abandon_due_to'] != 'N/A' || (!empty($match_info['toss_won_team_id']) && !empty($match_info['toss_won_decision'])) ): ?>
                <form method="post" enctype="multipart/form-data" class="form-horizontal">
                  <input name="action" value="submit" type="hidden">
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Start Date/Time*</label>
                    <div class="col-sm-7">
                      <p class="form-control-static"><?php echo date('d F, Y H:i:s',strtotime($match_info['start_time'])); ?></p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Overs*</label>
                    <div class="col-sm-7">
                      <p class="form-control-static"><?php echo $match_info['maximum_overs']; ?></p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Completely Abandoned Due To</label>
                    <div class="col-sm-7">
                      <select name="completely_abandon_due_to" id="completely_abandon_due_to" disabled="" class="form-control">
                        <?php for($i=0;$i<count($abandon_conditions);$i++): ?>
							<?php if($match_info['completely_abandon_due_to'] == $abandon_conditions[$i]) $sel = 'selected';else $sel = ''; ?>
							<option <?php echo $sel; ?> value="<?php echo $abandon_conditions[$i]; ?>"><?php echo $abandon_conditions[$i]; ?></option>
						<?php endfor; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Toss Won By*</label>
                    <div class="col-sm-7">
						<p class="form-control-static">
						<?php if(!empty($match_info['toss_won_team_id'])): ?>
							<?php echo $match_teams['toss_won_team_name'];  ?> and Elected to <?php echo $match_info['toss_won_decision']; ?>
						<?php endif; ?>
						</p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Batting Team1*</label>
                    <div class="col-sm-7">
                      <p class="form-control-static"><?php echo $match_teams['batting_team1_name'];  ?> </p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Batting Team1 Score</label>
                    <div class="col-sm-7">
                      <input name="team1_score" id="team1_score" class="form-control" value="<?php if(isset($_POST['team1_score'])) echo $_POST['team1_score'];else echo $match_info['team1_score']; ?>" type="text">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Batting Team1 Wickets</label>
                    <div class="col-sm-7">
                      <select name="team1_wickets" id="team1_wickets" class="form-control">
                       <option value="">Select One</option>
						<?php for($i=0;$i<=9;$i++): ?>
							<?php if(isset($_POST['team1_wickets']) && $_POST['team1_wickets'] == $i) $sel = 'selected'; elseif($match_info['team1_wickets'] == $i) $sel = 'selected';else $sel = ''; ?>
								<option value="<?php echo $i; ?>" <?php echo $sel; ?>><?php echo $i; ?></option>
						<?php endfor; ?>
						<?php if(isset($_POST['team1_wickets']) && $_POST['team1_wickets'] == 10) $sel = 'selected'; elseif($match_info['team1_wickets'] == 10) $sel = 'selected';else $sel = ''; ?>
						<option value="10" <?php echo $sel; ?>>All Out</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Batting Team1 Overs</label>
                    <div class="col-sm-7">
                      <input name="team1_overs" id="team1_overs" class="form-control" value="<?php if(isset($_POST['team1_overs'])) echo $_POST['team1_overs'];else echo $match_info['team1_overs']; ?>" type="text">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Batting Team2</label>
                    <div class="col-sm-7">
                      <p class="form-control-static"><?php echo $match_teams['batting_team2_name'];  ?></p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Batting Team2 Score</label>
                    <div class="col-sm-7">
                      <input name="team2_score" id="team2_score" class="form-control" value="<?php if(isset($_POST['team2_score'])) echo $_POST['team2_score'];else echo $match_info['team2_score']; ?>" type="text">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Batting Team2 Wickets</label>
                    <div class="col-sm-7">
                      <select name="team2_wickets" id="team2_wickets" class="form-control">
                        <option value="">Select One</option>
							<?php for($i=0;$i<=9;$i++): ?>
							<?php if(isset($_POST['team2_wickets']) && $_POST['team2_wickets'] == $i) $sel = 'selected'; elseif($match_info['team2_wickets'] == $i) $sel = 'selected';else $sel = ''; ?>
								<option value="<?php echo $i; ?>" <?php echo $sel; ?>><?php echo $i; ?></option>
							<?php endfor; ?>
							<?php if(isset($_POST['team2_wickets']) && $_POST['team2_wickets'] == 10) $sel = 'selected'; elseif($match_info['team2_wickets'] == 10) $sel = 'selected';else $sel = ''; ?>
						<option value="10" <?php echo $sel; ?>>All Out</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Batting Team2 Overs</label>
                    <div class="col-sm-7">
                      <input name="team2_overs" id="team2_overs" class="form-control" value="<?php if(isset($_POST['team2_overs'])) echo $_POST['team2_overs'];else echo $match_info['team2_overs']; ?>" type="text">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Match Result*</label>
                    <div class="col-sm-7">
                      <select name="winning_team_id" id="winning_team_id" class="form-control" > 
								<option value="">Select One</option>
								<option <?php if(!empty($_POST['winning_team_id']) && $_POST['winning_team_id'] == $match_info['batting_team1']) echo 'selected'; elseif(!isset($_POST['winning_team_id']) && $match_info['batting_team1'] == $match_info['winning_team_id'] && (empty($match_info['completely_abandon_due_to']) || $match_info['completely_abandon_due_to'] == 'N/A')) echo 'selected'; ?> value="<?php echo $match_teams['batting_team1'];  ?>"><?php echo $match_teams['batting_team1_name'];  ?> Wins</option>
								<option <?php if(!empty($_POST['winning_team_id']) && $_POST['winning_team_id'] == $match_info['batting_team2']) echo 'selected'; elseif(!isset($_POST['winning_team_id']) && $match_info['batting_team2'] == $match_info['winning_team_id'] && (empty($match_info['completely_abandon_due_to']) || $match_info['completely_abandon_due_to'] == 'N/A')) echo 'selected'; ?> value="<?php echo $match_teams['batting_team2'];  ?>"><?php echo $match_teams['batting_team2_name'];  ?> Wins</option>
								<option <?php if($match_info['batting_team1'] == $match_info['winning_team_id'] && $match_info['completely_abandon_due_to'] == 'Forfeit by One Team') echo 'selected'; ?> value="<?php echo $match_teams['batting_team1'];  ?>"><?php echo $match_teams['batting_team1_name'];  ?> Wins by Forfeit</option>
								<option <?php if($match_info['batting_team2'] == $match_info['winning_team_id'] && $match_info['completely_abandon_due_to'] == 'Forfeit by One Team') echo 'selected'; ?> value="<?php echo $match_teams['batting_team2'];  ?>"><?php echo $match_teams['batting_team2_name'];  ?> Wins by Forfeit</option>
								<option <?php if(!empty($_POST['winning_team_id']) && $_POST['winning_team_id'] == -1) echo 'selected';elseif(!isset($_POST['winning_team_id']) && $match_info['winning_team_id'] == -1) echo 'selected'; ?> value="-1">Match Tied</option>
								<option <?php if(!empty($_POST['winning_team_id']) && $_POST['winning_team_id'] == -2) echo 'selected';elseif(!isset($_POST['winning_team_id']) && $match_info['winning_team_id'] == -2 ) echo 'selected'; ?> value="-2">Match Abandoned</option>
								<option <?php if(!empty($_POST['winning_team_id']) && $_POST['winning_team_id'] == -3) echo 'selected';elseif(!isset($_POST['winning_team_id']) && $match_info['winning_team_id'] == -3 ) echo 'selected'; ?> value="-3">Decision Pending Review</option>
						</select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Match Result Text</label>
                    <div class="col-sm-7">
                      <input name="match_result_text" id="match_result_text" class="form-control" value="<?php if(isset($_POST['match_result_text'])) echo $_POST['match_result_text'];else echo $match_info['match_result_text']; ?>" type="text">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Man of the Match</label>
                    <div class="col-sm-7">
                      <select name="man_of_match1" id="man_of_match1" class="form-control">
                       <option value="">--<?php echo $match_teams['batting_team1_name']; ?>--</option>
						<?php for($i=0;$i<count($team1_players);$i++): if(isset($_POST['man_of_match1']) && $_POST['man_of_match1'] == $team1_players[$i]['id']) $sel =  'selected';elseif(!isset($_POST['man_of_match1']) && $match_info['man_of_match_player_id'] == $team1_players[$i]['id']) $sel = 'selected';else $sel = ''; ?>
							<option <?php echo $sel; ?> value="<?php echo $team1_players[$i]['id']; ?>"><?php echo $team1_players[$i]['f_name']; ?> <?php echo $team1_players[$i]['m_name']; ?> <?php echo $team1_players[$i]['last_name']; ?></option>
						<?php endfor; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Man of the Match</label>
                    <div class="col-sm-7">
                      <select name="man_of_match2" id="man_of_match2" class="form-control"> 
							<option value="">--<?php echo $match_teams['batting_team2_name']; ?>--</option>
							<?php for($i=0;$i<count($team2_players);$i++): if(isset($_POST['man_of_match2']) && $_POST['man_of_match2'] == $team2_players[$i]['id']) $sel =  'selected';elseif(!isset($_POST['man_of_match2']) && $match_info['man_of_match_player_id'] == $team2_players[$i]['id']) $sel = 'selected';else $sel = ''; ?>
								<option <?php echo $sel; ?> value="<?php echo $team2_players[$i]['id']; ?>"><?php echo $team2_players[$i]['f_name']; ?> <?php echo $team2_players[$i]['m_name']; ?> <?php echo $team2_players[$i]['last_name']; ?></option>
							<?php endfor; ?>
						</select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Highlights</label>
                    <div class="col-sm-7">
                      <textarea name="highlights_text" rows="3" class="form-control" id="highlights_text"><?php if(isset($_POST['highlights_text'])) echo $_POST['highlights_text'];else echo $match_info['highlights_text']; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Disciplinary Issues</label>
                    <div class="col-sm-7">
                      <input value="1" name="disciplinary_issues" id="disciplinary_issues" type="checkbox" <?php if(!empty($_POST['disciplinary_issues']) && $_POST['disciplinary_issues'] == 1) echo 'checked';else if($match_info['disciplinary_issues'] == 1) echo 'checked'; ?>>
                      Check for disciplinary issue
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Issues Comment</label>
                    <div class="col-sm-7">
                      <textarea name="disciplinary_issues_text" rows="3" class="form-control" id="disciplinary_issues_text"><?php if(isset($_POST['disciplinary_issues_text'])) echo $_POST['disciplinary_issues_text'];else echo $match_info['disciplinary_issues_text']; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Last Updated</label>
                    <div class="col-sm-7">
                      <p class="form-control-static"><?php echo date('d F, Y H:i:s', strtotime($match_info['updated_on']));  ?> </p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Last Updated by</label>
                    <div class="col-sm-7">
						<?php $last_updated_by = get_record_on_id('users', $match_info['last_updated_by']);	 ?>
                      <p class="form-control-static"><?php echo $last_updated_by['f_name'].' '.$last_updated_by['m_name'].' '.$last_updated_by['last_name'] ;  ?> </p>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <div class="col-sm-offset-5 col-sm-7">
                      <input name="submit_btn" value=" Update " class="btn orange hvr-float-shadow" type="submit">
                      <input name="cancel_btn" value=" Cancel " class="btn blue hvr-float-shadow" onclick="window.location.href='<?php echo WWW; ?>tournament/matches/list/<?php echo $tournament_id; ?>';" type="button">
                    </div>
                  </div>
                </form>
				<?php else: ?>
					<form method="post" enctype="multipart/form-data" class="form-horizontal">
						<div class="form-group">
						<label class="col-sm-5 control-label">Start Date/Time*</label>
						<div class="col-sm-7">
						  <p class="form-control-static"><?php echo date('d F, Y H:i:s',strtotime($match_info['start_time'])); ?></p>
						</div>
						</div>
						
						<div class="form-group">
						<label class="col-sm-5 control-label">Overs*</label>
						<div class="col-sm-7">
						  <p class="form-control-static"><?php echo $match_info['maximum_overs']; ?></p>
						</div>
						</div>
						
						<div class="form-group">
						<label class="col-sm-5 control-label">Toss Won By*</label>
						<div class="col-sm-7">
							<select name="toss_won_team_id" id="toss_won_team_id" class="form-control"> 
							<option value="">Select One</option>
							<?php for($i=0;$i<count($tournament_clubs);$i++): 
								if(!in_array($tournament_clubs[$i]['id'],$match_team_ids)) continue;
								if(isset($_POST['toss_won_team_id']) && $_POST['toss_won_team_id'] == $tournament_clubs[$i]['id']) $sel =  'selected';else $sel = '';  ?>
								<option <?php echo $sel; ?> value="<?php echo $tournament_clubs[$i]['id']; ?>"><?php echo $tournament_clubs[$i]['company_name']; ?></option>
							<?php endfor; ?>
							</select>
							<span style="font-weight:normal;float:right;">Make sure to select the right team, once saved you will not be able to update</span>

						</div>
						</div>
						
						<div class="form-group">
						<label class="col-sm-5 control-label">Elected To*</label>
						<div class="col-sm-7">
							<select name="toss_won_decision" id="toss_won_decision" class="form-control"> 
								<option value="">Select One</option>
								<option value="bat" <?php if(isset($_POST['toss_won_decision']) && $_POST['toss_won_decision'] == 'bat') echo 'selected'; ?>>Bat</option>
								<option value="bowl" <?php if(isset($_POST['toss_won_decision']) && $_POST['toss_won_decision'] == 'bowl') echo 'selected'; ?>>Bowl</option>
							</select>
							<span style="font-weight:normal;float:right;">Make sure to select the right value, once saved you will not be able to update</span>
						</div>
						</div>
						
						
					<div class="form-group">
                    <div class="col-sm-offset-5 col-sm-7">
                      <input name="submit_toss_btn" value=" Update " class="btn orange hvr-float-shadow" type="submit">
                      <input name="cancel_btn" value=" Cancel " class="btn blue hvr-float-shadow" onclick="window.location.href='<?php echo WWW; ?>tournament/matches/list/<?php echo $tournament_id; ?>';" type="button">
                    </div>
					</div>
				  
					</form>
				<?php endif; ?>
				
				<?php }else{ ?>
					<div id="error" class="alert alert-danger">You do not have Permission for this Tournament... !</div>
				<?php } ?>
              </div>
            </div>
          </div>
		
		
		</div>
	</div>
</div>		

<script src="<?php echo WWW; ?>assets/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo WWW; ?>assets/css/jquery.datetimepicker.css"/>
<script>
$('#start_time').datetimepicker();
$(document).ready(function(){
	$("#man_of_match1").bind('change',function(){
		if($(this).val() != ''){
			$("#man_of_match2").attr('disabled','disabled');
		}else{
			$("#man_of_match2").removeAttr('disabled');
		}
	});
	
	$("#man_of_match2").bind('change',function(){
		if($(this).val() != ''){
			$("#man_of_match1").attr('disabled','disabled');
		}else{
			$("#man_of_match1").removeAttr('disabled');
		}
	});
	
	if($("#man_of_match1").val() != ''){
		$("#man_of_match2").attr('disabled','disabled');
	}
	if($("#man_of_match2").val() != ''){
		$("#man_of_match1").attr('disabled','disabled');
	}
	
});
</script>
<?php include('common/footer.php'); ?>