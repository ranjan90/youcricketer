<?php include('includes/configuration.php'); 

if(isset($_POST['action']) && $_POST['action'] == 'delete'){
	$id = $_POST['id'];
	$sql = "DELETE from tournament_match_commentary WHERE id = $id ";
	mysqli_query($conn,$sql);
	exit();
}

if(empty($_POST['batsman_out'])){
	$_POST['batsman_out'] = 'no';
}
if(empty($_POST['fielder_id'])){
	$_POST['fielder_id'] = 0;
}
if(empty($_POST['runs_type'])){
	$_POST['runs_type'] = 'batsman_runs';
}
if(empty($_POST['ball_type'])){
	$_POST['ball_type'] = 'legal_ball';
}
if(empty($_POST['ball_score'])){
	$_POST['ball_score'] = 0;
}

extract($_POST);
$error = '';

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
}

$tournament_permission = 0;
$match_info = get_record_on_id('tournament_matches', $match_id);
$tournament_id = $match_info['tournament_id'];

if(isset($user_id) && !empty($user_id)){
	$tournament_permission = 1;
}

if($tournament_permission == 0){
	$error = 'You do not have Permission for this Tournament';
}

if(empty($error)){
	$sql = "SELECT * from tournament_match_commentary WHERE match_id = $match_id and team_id = $team_id  and team_overs = $team_overs and ball_type='legal_ball'";
	$rs_commentary = mysqli_query($conn,$sql);
	if(mysqli_num_rows($rs_commentary)){
		$error = 'This ball already exists';
	}
}

if(empty($error)){

	$sql = "SELECT * from tournament_match_commentary WHERE match_id = $match_id and team_id = $team_id ORDER BY id DESC LIMIT 1";
	$rs_commentary = mysqli_query($conn,$sql);
	if(mysqli_num_rows($rs_commentary)>0){
		$row_prev = mysqli_fetch_assoc($rs_commentary);
		foreach($row_prev as $key=>$value){
			if(empty($row_prev["$key"])){
				$row_prev["$key"] = 0;
			}
		}
	}else{
		$sql = "show columns FROM `tournament_match_commentary` ";
		$rs_commentary = mysqli_query($conn,$sql);
		while($row = mysqli_fetch_assoc($rs_commentary)){
			$key = $row['Field'];
			$row_prev["$key"] = 0;
		}
	}



	$sql = "INSERT INTO tournament_match_commentary SET ";

	foreach($_POST as $key=>$value){
		$sql.= " $key = '$value', ";
	}
	$sql = rtrim($sql,', ');

	if( !empty($row_prev['batsman_id']) && $batsman_id != $row_prev['batsman_id']){
		$sql_batsman = "SELECT batsman_balls,batsman_score,batsman_fours,batsman_sixes from tournament_match_commentary 
		WHERE match_id = $match_id and team_id = $team_id and batsman_id=$batsman_id ORDER BY id DESC LIMIT 1";
		$rs_commentary = mysqli_query($conn,$sql_batsman);
		if(mysqli_num_rows($rs_commentary )>0){
			$row_prev_batsman = mysqli_fetch_assoc($rs_commentary);
			$row_prev['batsman_balls'] = $row_prev_batsman['batsman_balls'];
			$row_prev['batsman_score'] = $row_prev_batsman['batsman_score'];
			$row_prev['batsman_fours'] = $row_prev_batsman['batsman_fours'];
			$row_prev['batsman_sixes'] = $row_prev_batsman['batsman_sixes'];
		}else{
			$row_prev['batsman_balls'] = 0;
			$row_prev['batsman_score'] = 0;
			$row_prev['batsman_fours'] = 0;
			$row_prev['batsman_sixes'] = 0;
		}
	}

	$over_ball_arr = explode('.',$team_overs);
	$overs = $over_ball_arr[0];
	$balls = $over_ball_arr[1];

	if($ball_type == 'legal_ball'){
		$batsman_balls = 1+$row_prev['batsman_balls'];
		$sql.=", batsman_balls = $batsman_balls ";
		if($batsman_out == 'yes'){
			$team_wickets = 1+$row_prev['team_wickets'];
			$sql.=" , team_wickets = $team_wickets";
		}else{
			$team_wickets = $row_prev['team_wickets'];
			$sql.=" , team_wickets = $team_wickets";
		}
		
	}else{
		$batsman_balls = $row_prev['batsman_balls'];
		$sql.=", batsman_balls = $batsman_balls ";
		if($batsman_out == 'yes'){
			$team_wickets = 1+$row_prev['team_wickets'];
			$sql.=" , team_wickets = $team_wickets";
		}else{
			$team_wickets = $row_prev['team_wickets'];
			$sql.=" , team_wickets = $team_wickets";
		}
	}

	if($ball_type == 'legal_ball' ){
		if($runs_type == 'batsman_runs'){
			$batsman_score = $ball_score+$row_prev['batsman_score'];
			$sql.=", batsman_score = $batsman_score ";
			if($ball_score == 4){
				$batsman_fours = 1+$row_prev['batsman_fours'];
				$sql.=", batsman_fours = $batsman_fours";	
			}else{
				$batsman_fours = $row_prev['batsman_fours'];
				$sql.=", batsman_fours = $batsman_fours";	
			}
			if($ball_score == 6){
				$batsman_sixes = 1+$row_prev['batsman_sixes'];
				$sql.=", batsman_sixes = $batsman_sixes";	
			}else{
				$batsman_sixes = $row_prev['batsman_sixes'];
				$sql.=", batsman_sixes = $batsman_sixes";	
			}
			
			$total_extra_runs = $row_prev['total_extra_runs'];
			$sql.=", total_extra_runs = $total_extra_runs";	
		}elseif($runs_type == 'byes' || $runs_type == 'leg_byes'){
			$batsman_score = $row_prev['batsman_score'];
			$sql.=", batsman_score = $batsman_score ";
			$total_extra_runs = $ball_score+$row_prev['total_extra_runs'];
			$sql.=", total_extra_runs = $total_extra_runs";	
				
			$ball_extra_runs = $ball_score;
			$sql.=", ball_extra_runs = $ball_extra_runs";		
		}
		
		$team_score = $ball_score+$row_prev['team_score'];
		$sql.=", team_score = $team_score";
		if($balls == 1 && strpos($row_prev['team_overs'],'.6') !== false ){
			$sql.=" , score_in_over = $ball_score";
		}else{
			$over_score = $ball_score + $row_prev['score_in_over'];
			$sql.=" , score_in_over = $over_score";
		}
	}	
	elseif($ball_type == 'no_ball'){
		
		if($runs_type == 'batsman_runs'){
			$batsman_score = $ball_score+$row_prev['batsman_score'];
			$sql.=", batsman_score = $batsman_score ";
			if($ball_score == 4){
				$batsman_fours = 1+$row_prev['batsman_fours'];
				$sql.=", batsman_fours = $batsman_fours";	
			}else{
				$batsman_fours = $row_prev['batsman_fours'];
				$sql.=", batsman_fours = $batsman_fours";	
			}
			if($ball_score == 6){
				$batsman_sixes = 1+$row_prev['batsman_sixes'];
				$sql.=", batsman_sixes = $batsman_sixes";	
			}else{
				$batsman_sixes = $row_prev['batsman_sixes'];
				$sql.=", batsman_sixes = $batsman_sixes";	
			}
			
			$total_extra_runs = 1+$row_prev['total_extra_runs'];
			$sql.=", total_extra_runs = $total_extra_runs";	
				
			//$ball_extra_runs = $ball_score+1;
			$ball_extra_runs = 1;
			$sql.=", ball_extra_runs = $ball_extra_runs";		
			
		}elseif($runs_type == 'byes' || $runs_type == 'leg_byes'){
			$batsman_score = $row_prev['batsman_score'];
			$sql.=", batsman_score = $batsman_score ";
			$total_extra_runs = $ball_score+$row_prev['total_extra_runs']+1;
			$sql.=", total_extra_runs = $total_extra_runs";	
				
			$ball_extra_runs = $ball_score+1;
			$sql.=", ball_extra_runs = $ball_extra_runs";		
		}
		
		$team_score = $ball_score+$row_prev['team_score']+1;
		$sql.=", team_score = $team_score";
		
		/*$team_wickets = $row_prev['team_wickets'];
		$sql.=", team_wickets = $team_wickets";*/
		
		if($balls == 1 && strpos($row_prev['team_overs'],'.6') !== false ){
			$over_score = $ball_score +1;
			$sql.=" , score_in_over = $over_score";
		}else{
			$over_score = $ball_score + $row_prev['score_in_over']+1;
			$sql.=" , score_in_over = $over_score";
		}
		
	}elseif($ball_type == 'wide_ball'){
		$batsman_score = $row_prev['batsman_score'];
		$sql.=", batsman_score = $batsman_score ";
		$batsman_fours = $row_prev['batsman_fours'];
		$sql.=", batsman_fours = $batsman_fours";	
		$batsman_sixes = $row_prev['batsman_sixes'];
		$sql.=", batsman_sixes = $batsman_sixes";	
		
		$total_extra_runs = $ball_score+$row_prev['total_extra_runs']+1;
		$sql.=", total_extra_runs = $total_extra_runs";	
		$ball_extra_runs = $ball_score+1;
		$sql.=", ball_extra_runs = $ball_extra_runs";	
		
		$team_score = $ball_score+$row_prev['team_score']+1;
		$sql.=", team_score = $team_score";
		/*$team_wickets = $row_prev['team_wickets'];
		$sql.=", team_wickets = $team_wickets";*/
		
		if($balls == 1 && strpos($row_prev['team_overs'],'.6') !== false ){
			$over_score = $ball_score +1;
			$sql.=" , score_in_over = $over_score";
		}else{
			$over_score = $ball_score + $row_prev['score_in_over']+1;
			$sql.=" , score_in_over = $over_score";
		}
	}

	if(mysqli_query($conn,$sql)){
		$inserted_id = mysqli_insert_id($conn);
		$sql = "SELECT * FROM tournament_match_commentary WHERE id = $inserted_id ";
		$rs_commentary = mysqli_query($conn,$sql);
		$inserted_arr = mysqli_fetch_assoc($rs_commentary);
		$error = '';
	}else{
		$error = 'Error in adding data. Try again later.'.mysqli_error($conn).$sql;
	}

}

if(!empty($error)){
	$output_arr = array('error'=>'yes','error_msg'=>$error);
}else{
	$output_arr = array('error'=>'no','error_msg'=>'');
}

updateScoreBoard($_POST,$match_info,$inserted_arr);

echo json_encode($output_arr);
exit;
//echo mysqli_error($conn).$sql;exit;
//var_dump($_POST);

function updateScoreBoard($data,$match_info,$inserted_arr){
	$match_id = $match_info['id'];
	$team_id = $data['team_id'];
	
	$team_batting = $team_id;
	if($match_info['team1'] == $team_batting){
		$team_bowling = $match_info['team2'];
	}else{
		$team_bowling = $match_info['team1'];
	}
	
	$sql = "SELECT id FROM tournament_bat_scorecard WHERE match_id=$match_id LIMIT 1";
	$rs_scorecard = mysqli_query($conn,$sql);

	if(mysqli_num_rows($rs_scorecard) == 0){
		for($i=1;$i<=11;$i++){
			$sql = "INSERT INTO tournament_bat_scorecard SET match_id=$match_id, team_id=".$match_info['batting_team1'].",batsman_no=$i,batsman_id=0,
			how_out='',fielder_id=0,bowler_id=0,runs_scored=0,balls_played=0,fours_scored=0,sixes_scored=0,strike_rate=0";
			mysqli_query($conn,$sql);
		}
		
		for($i=1;$i<=11;$i++){
			$sql = "INSERT INTO tournament_bat_scorecard SET match_id=$match_id, team_id=".$match_info['batting_team2'].",batsman_no=$i,batsman_id=0,
			how_out='',fielder_id=0,bowler_id=0,runs_scored=0,balls_played=0,fours_scored=0,sixes_scored=0,strike_rate=0";
			mysqli_query($conn,$sql);
		}
	}

	$sql = "SELECT id FROM tournament_bowl_scorecard WHERE match_id=$match_id LIMIT 1";
	$rs_scorecard = mysqli_query($conn,$sql);

	if(mysqli_num_rows($rs_scorecard) == 0){
		for($i=1;$i<=11;$i++){
			$sql = "INSERT INTO tournament_bowl_scorecard SET match_id=$match_id, team_id=".$match_info['batting_team1'].",bowler_no=$i,bowler_id=0,
			overs_bowled=0,maidens_bowled=0,runs_conceded=0,wide_balls_bowled=0,no_balls_bowled=0,wickets_taken=0";
			mysqli_query($conn,$sql);
		}
		
		for($i=1;$i<=11;$i++){
			$sql = "INSERT INTO tournament_bowl_scorecard SET match_id=$match_id, team_id=".$match_info['batting_team2'].",bowler_no=$i,bowler_id=0,
			overs_bowled=0,maidens_bowled=0,runs_conceded=0,wide_balls_bowled=0,no_balls_bowled=0,wickets_taken=0";
			mysqli_query($conn,$sql);
		}
	}

	$sql = "SELECT id FROM tournament_fow_scorecard WHERE match_id=$match_id LIMIT 1";
	$rs_scorecard = mysqli_query($conn,$sql);

	if(mysqli_num_rows($rs_scorecard) == 0){
		for($i=1;$i<=10;$i++){
			$sql = "INSERT INTO tournament_fow_scorecard SET match_id=$match_id, team_id=".$match_info['batting_team1'].",wicket_no=$i,score=0,
			partnership=0,overs=0";
			mysqli_query($conn,$sql);
		}
		
		for($i=1;$i<=10;$i++){
			$sql = "INSERT INTO tournament_fow_scorecard SET match_id=$match_id, team_id=".$match_info['batting_team2'].",wicket_no=$i,score=0,
			partnership=0,overs=0";
			mysqli_query($conn,$sql);
		}
	}
	
	$sql = "SELECT batsman_no FROM tournament_bat_scorecard WHERE match_id = $match_id and team_id = $team_id and batsman_id = ".$inserted_arr['batsman_id'];
	$rs_bat = mysqli_query($conn,$sql);
	if(mysqli_num_rows($rs_bat)){
		$row_bat = mysqli_fetch_assoc($rs_bat);
		$batsman_no = $row_bat['batsman_no'];
	}else{
		$sql = "SELECT max(batsman_no) as batsman_no_max FROM tournament_bat_scorecard WHERE match_id = $match_id and team_id = $team_id and batsman_id>0";
		$rs_bat = mysqli_query($conn,$sql);
		$row_bat = mysqli_fetch_assoc($rs_bat);
		$batsman_no = $row_bat['batsman_no_max']+1;
		//$batsman_no = 1;
	}
	
	if(!empty($inserted_arr['how_out'])) $bowler_id = $inserted_arr['bowler_id'];else $bowler_id = 0;
	
	$sql = "UPDATE tournament_bat_scorecard SET batsman_no = $batsman_no,batsman_id=".$inserted_arr['batsman_id'].",
			how_out='".$inserted_arr['how_out']."',fielder_id=".$inserted_arr['fielder_id'].",bowler_id=".$bowler_id.",
			runs_scored=".$inserted_arr['batsman_score'].",balls_played=".$inserted_arr['batsman_balls'].",fours_scored=".$inserted_arr['batsman_fours'].",
			sixes_scored=".$inserted_arr['batsman_sixes'].",strike_rate=0 WHERE batsman_no = $batsman_no and match_id=$match_id and team_id= $team_id";
	mysqli_query($conn,$sql);
	
	$sql = "SELECT bowler_no FROM tournament_bowl_scorecard WHERE match_id = $match_id and team_id = $team_bowling and bowler_id = ".$inserted_arr['bowler_id'];
	$rs_bowl = mysqli_query($conn,$sql);
	if(mysqli_num_rows($rs_bowl)){
		$row_bowl = mysqli_fetch_assoc($rs_bowl);
		$bowler_no = $row_bowl['bowler_no'];
	}else{
		$sql = "SELECT max(bowler_no) as bowler_no_max FROM tournament_bowl_scorecard WHERE match_id = $match_id and team_id = $team_bowling and bowler_id>0";
		$rs_bowl = mysqli_query($conn,$sql);
		$row_bowl = mysqli_fetch_assoc($rs_bowl);
		$bowler_no = $row_bowl['bowler_no_max']+1;
		//$bowler_no = 1;
	}
	
	$sql = "SELECT sum(ball_score) as score_conceded,count(id) as balls_bowled FROM `tournament_match_commentary` where bowler_id=".$inserted_arr['bowler_id']." and match_id=$match_id and ball_type='legal_ball'";
	$rs_bowl = mysqli_query($conn,$sql);
	$row_bowl_1 = mysqli_fetch_assoc($rs_bowl);
	$overs_bowled = floor($row_bowl_1['balls_bowled']/6);
	$rest_balls = $row_bowl_1['balls_bowled']%6;
	$overs_bowled = $overs_bowled.'.'.$rest_balls;
	$row_bowl_1['overs_bowled'] = $overs_bowled;
	
	$sql = "SELECT ball_type,count(id) as ball_type_count  FROM `tournament_match_commentary` WHERE bowler_id=".$inserted_arr['bowler_id']." AND match_id=$match_id GROUP BY ball_type ";
	$rs_bowl = mysqli_query($conn,$sql);
	while($row = mysqli_fetch_assoc($rs_bowl)){
		$row_bowl_2[$row['ball_type']] = $row['ball_type_count'];
	}
	if(!isset($row_bowl_2['legal_ball'])) $row_bowl_2['legal_ball'] = 0;
	if(!isset($row_bowl_2['wide_ball'])) $row_bowl_2['wide_ball'] = 0;
	if(!isset($row_bowl_2['no_ball'])) $row_bowl_2['no_ball'] = 0;
	
	$sql = "SELECT count(id) as maidens_overs FROM `tournament_match_commentary` where bowler_id=".$inserted_arr['bowler_id']." and match_id=$match_id and ball_type='legal_ball' and score_in_over=0 and team_overs like '%.6%' ";
	$rs_bowl = mysqli_query($conn,$sql);
	$row_bowl_3 = mysqli_fetch_assoc($rs_bowl);
	
	$sql = "SELECT count(id) as wickets_taken FROM `tournament_match_commentary` where bowler_id=".$inserted_arr['bowler_id']." and match_id=$match_id and ball_type='legal_ball' and batsman_out = 'yes' ";
	$rs_bowl = mysqli_query($conn,$sql);
	$row_bowl_4 = mysqli_fetch_assoc($rs_bowl);
	
	$sql = "UPDATE tournament_bowl_scorecard SET bowler_id=".$inserted_arr['bowler_id'].",
	overs_bowled=".$row_bowl_1['overs_bowled'].",maidens_bowled=".$row_bowl_3['maidens_overs'].",runs_conceded=".$row_bowl_1['score_conceded'].",
	wide_balls_bowled=".$row_bowl_2['wide_ball'].",no_balls_bowled=".$row_bowl_2['no_ball'].",wickets_taken=".$row_bowl_4['wickets_taken']."  
	 WHERE bowler_no= $bowler_no and match_id=$match_id and team_id=$team_bowling ";
	mysqli_query($conn,$sql);
	//echo $sql.mysqli_error($conn);exit;
	
	$sql = "SELECT sum(ball_extra_runs) as extra_runs_count,count(id) as ball_count,ball_type FROM `tournament_match_commentary` 
	WHERE match_id=$match_id and team_id=$team_id GROUP BY ball_type";
	$rs_bowl = mysqli_query($conn,$sql);
	while($row = mysqli_fetch_assoc($rs_bowl)){
		if($row['ball_type'] == 'no_ball'){	//no ball does not add byes or leg byes in it. Wide ball does not have byes and leg byes.
			$row_bowl_5[$row['ball_type']] = $row['ball_count'];
		}else{
			$row_bowl_5[$row['ball_type']] = $row['extra_runs_count'];
		}
	}
	
	if(!isset($row_bowl_5['wide_ball'])) $row_bowl_5['wide_ball'] = 0;
	if(!isset($row_bowl_5['no_ball'])) $row_bowl_5['no_ball'] = 0;
	
	$sql = "SELECT sum(ball_score) as score_count,runs_type FROM `tournament_match_commentary` 
	WHERE match_id=$match_id and team_id=$team_id GROUP BY runs_type";
	$rs_bowl = mysqli_query($conn,$sql);
	while($row = mysqli_fetch_assoc($rs_bowl)){
		$row_bowl_6[$row['runs_type']] = $row['score_count'];
	}
	
	if(!isset($row_bowl_6['byes'])) $row_bowl_6['byes'] = 0;
	if(!isset($row_bowl_6['leg_byes'])) $row_bowl_6['leg_byes'] = 0;
	
	if($team_id == $match_info['batting_team1']){
		$sql = "UPDATE tournament_matches SET team1_score=".$inserted_arr['team_score'].",team1_wickets=".$inserted_arr['team_wickets'].",
		team1_overs=".$inserted_arr['team_overs'].",team1_leg_byes=".$row_bowl_6['leg_byes'].",team1_byes=".$row_bowl_6['byes'].",
		team1_wides=".$row_bowl_5['wide_ball'].",team1_no_balls=".$row_bowl_5['no_ball']." WHERE id = ".$match_info['id'];
	}else{
		$sql = "UPDATE tournament_matches SET team2_score=".$inserted_arr['team_score'].",team2_wickets=".$inserted_arr['team_wickets'].",
		team2_overs=".$inserted_arr['team_overs'].",team2_leg_byes=".$row_bowl_6['leg_byes'].",team2_byes=".$row_bowl_6['byes'].",
		team2_wides=".$row_bowl_5['wide_ball'].",team2_no_balls=".$row_bowl_5['no_ball']." WHERE id = ".$match_info['id'];
	}
	
	mysqli_query($conn,$sql);

}