<?php include('common/header.php'); ?>
<?php
	if(isset($_POST['action']) && $_POST['action'] == 'comment' && isset($_SESSION['ycdc_user_name']) && !empty($_SESSION['ycdc_user_name'])){
		$comments 	= addslashes($_POST['comments']);
		$user_id 	= $_SESSION['ycdc_dbuid'];
		$date 		= date('Y-m-d H:i:s');
		$topic_id 	= $_GET['id'];

		$rs_chk 	= mysqli_query($conn,"select * from blog_comments where user_id = '$user_id' and blog_article_id = '$topic_id' and comments = '$comments'");
		if(mysqli_num_rows($rs_chk) == 0){
			$query 		= "insert into blog_comments (comments, blog_article_id, user_id, comment_date, status) 
									values ('$comments','$topic_id','$user_id','$date','1')";	
			if(mysqli_query($conn,$query)){
			?><div id="success" class="alert alert-success">Comments saved successfully ... !</div><?
			}else{
				?><div id="success" class="alert alert-success">Comments already submitted ... !</div><?
			}
		}
					
	}
?>

<?php 
	$row = get_record_on_id('blog_articles',$_GET['id']);
	if(!$row){ ?>
		<script>
		window.location = '<?php echo WWW;?>blogs.html';
		</script>
	<? }
	$rowC= get_record_on_id('blog_categories',$row['category_id']);
	$rowU= get_record_on_id('users',$row['user_id']);
	$row_img= mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'groups' and entity_id = '".$row_g['id']."'"));
?>

<script src="<?=WWW;?>assets/js/ckeditor/ckeditor.js"></script>
<div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1>
			Blog Detail - <?=get_combo('blog_articles','title',$_GET['id'],'','text')?><br>
            <span> <strong>Created By :</strong> <?php echo (!isset($rowU['f_name']))?'Administrator':$rowU['f_name'];?>
			<strong>Created On :</strong> <?php echo date_converter($row['create_date'])." ".date("H:i:s",strtotime($row['create_date']))?>
			<strong>Category :</strong> <?php echo $rowC['name']?> </span>
          </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <div class="white-box">
            <?php if(!empty($row['series_match_id'])){ ?>
				<div class="row">
				  <div class="col-sm-12">
					<a href="#" onClick="javascript:void(0)" data-toggle="modal" data-target="#blogView">
					  <img src="<?php echo WWW;?>assets/img/world-cup-cricket-blogging-contest-100-doller.jpg" class="img-responsive" style="margin:10px auto;">
					</a>
				  </div>
				</div>
				<div class="row">
				  <div class="col-sm-12 text-center">
					<a class="btn orange hvr-float-shadow" href="<?php echo WWW;?>add-blog-<?php echo $row['id']?>-<?php echo friendlyURL($row['title'])?>.html">Submit Yours</a>
				  </div>
				</div>
			<?php } else { ?>
				<div class="row">
				  <div class="col-sm-12">
					<?php echo $row['content']; ?>
				  </div>
				</div>
			<?php } ?>
			
            <div class="row">
              <div class="col-sm-12">
                <?php if(!empty($row['series_match_id'])){ ?>
				<h2>Other Blogs for Same Match</h2>
				<ul style="list-style:square; font-size:13px; line-height:25px; margin-left:20px;">
				<?php
					$rsBlog = mysqli_query($conn,"select * from blog_articles where series_match_id = '".$row['series_match_id']."' and id <> '".$row['id']."' and status = '1'");
					while($rowBlog = mysqli_fetch_assoc($rsBlog)){
					?>
					<li><a href="<?php echo WWW;?>blog-detail-<?php echo $rowBlog['id']?>-<?php echo friendlyURL($rowBlog['title'])?>.html" title="<?php echo $rowBlog['title']?>"><?php echo $rowBlog['title']?></a></li>
					<?php
					}
				?>
				</ul>
				<?php } ?>
			
                <h2>Post Your Reply</h2>
				<? include('common/login-box.php');?>
                
              </div>
            </div>
			
			<?php if(isset($_SESSION['ycdc_user_name']) && !empty($_SESSION['ycdc_user_name'])){ ?>
				<form method="post" action="">
				<div class="row">
				  <div class="col-sm-12">
					<div id="editor">
						<textarea name="comments"  class="ckeditor <?=(!isset($_SESSION['ycdc_user_name']) || empty($_SESSION['ycdc_user_name']))?'':'validate[required]'?>"></textarea>
					</div>
				  </div>
				</div>
				<div class="row">
				  <div class="col-sm-12 text-center">
					<br/>
					<input value=" Save " class="btn orange hvr-float-shadow" type="submit">
					<a id="cancel" class="btn blue hvr-float-shadow" href="<?=WWW?>blogs.html">Cancel</a>
				  </div>
				</div>
				<input type="hidden" name="action" value="comment">
				</form>
			<?php }else{ ?>
				<div class="row">
				  <div class="col-sm-12 text-center">
					<div class="alert alert-danger">Please login first to post your reply ... !</div>
				  </div>
				</div>
			<?php } ?>
			
            <div class="row">
              <div class="col-sm-12">
                <h2>Posted Replies</h2>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
				 <?php  $rpp = PRODUCT_LIMIT_FRONT; // results per page
            			$ppage = intval($_GET["page"]);
      					if($ppage<=0) $ppage = 1;
      					$query = "select * from blog_comments where status=1 and blog_article_id = '".$row['id']."'";
				      	//=======================================
				      	if(isset($_GET['keywords']) && $_GET['keywords'] != 'Search Here'){
					        $query  .= "and title like '%".$_GET['keywords']."%'";
					    }
					    $query .= " order by id desc ";
				      //echo $query;
				      //=======================================
				      if(mysqli_num_rows(mysqli_query($conn,$query)) == 0){
				        echo '<div class="alert alert-info">No Record found...!</div>';
				      }
				      $rs   = mysqli_query($conn,$query);
				      $tcount = mysqli_num_rows($rs);
				      $tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
				      $count = 0;
				      $i = ($ppage-1)* $rpp;
          			  $x = 0;
          			  //=======================================
      				  while(($count<$rpp) && ($i<$tcount)){
                		mysqli_data_seek($rs,$i);
                		$row_g 	= mysqli_fetch_assoc($rs);
                		?>
      					
      						<? if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	                            if($row['user_id'] == $_SESSION['ycdc_dbuid']){ ?>
	                            <a href="<?=WWW?>delete-blog-comment-<?=$row_g['id']?>.html"><img src="<?=WWW?>images/icons/delete.png"></a>
	                         <? } ?>
	                      	<? } ?>
							<span class="list-text" >
								
	                            <h3><?=get_combo('users','concat(f_name, last_name)',$row_g['user_id'],'','text')?> on <?=date_converter($row_g['comment_date'])?></h3>
	                            <p><?=$row_g['comments'];?></p>	
								<?php if(isset($_SESSION['ycdc_dbuid']) 
									  && !empty($_SESSION['ycdc_dbuid']) 
									  && $_SESSION['ycdc_dbuid'] == $row_g['user_id']){ ?>
								<a href="<?=WWW?>edit-comments-<?=$row_g['id']?>.html"><img src="<?=WWW?>images/icons/edit.png"></a>
			                    <a href="<?=WWW?>delete-comments-<?=$row_g['id']?>.html" onClick="return confirm('Are you sure to delete comment');"><img src="<?=WWW?>images/icons/delete.png"></a>
								  
			                    &nbsp;&nbsp;&nbsp;
								<?php } ?>								
	                        </span>
							<hr style="margin:6px;">
					    <?
					      $i++;
					      $count++;
					      $x++;
					  } 
					  ?>
					
					<?php if(mysqli_num_rows($rs) != 0 && mysqli_num_rows($rs) > PRODUCT_LIMIT_FRONT){ ?>
					<?php
						$reload = 'blog-detail-'.$_GET['id'].'-'.urlencode($row['title']).'.html?';
						echo paginate_one($reload, $ppage, $tpages);
					?>
					<input type="hidden" name="pagination-page" value="blog-detail-<?=$_GET['id']?>-<?=urlencode($row['title'])?>.html">
					<? } ?>    
				</div>
            </div>
            <div class="row">
              <div class="col-sm-4 col-sm-offset-8">
                <form id="list-search" method="post" action="">
                  <div class="input-group">
                    <input type="text" class="form-control validate[required] input-login" name="txtsearch" placeholder="Search Here">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit">Go!</button>
                    </span>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
              <?php include('common/right-panel.php');?>
            </div>
          </div>
          
        </div>
      </div>
    </div><!-- /.container -->
	
<script type="text/javascript">
		$('form#list-search').submit(function(e){
			var string = $('input[name=txtsearch]').val();
			if(string != '' && string != 'Search Here'){
				string = string.replace(/\s+/g,'_').toLowerCase();
				$('form#list-search').attr('action','<?=WWW;?>blog-detail-<?=$_GET['id']?>-<?=urlencode($row['title'])?>-' + string + '.html');
			}
		});
</script>

<div class="modal fade" id="blogView" tabindex="-1" role="dialog" aria-labelledby="blogViewModel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="settingLabel">Blogging Contest Rules </h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-12">
                <ol class="list_ol">
                  <li>To participate in Youcricketer.com 2015 World Cup Blog contest you need to have a registered account with Youcricketer. If you do not then first register either as Individual or as company. Membership is Free. </li>
                  <li>Once your account has been created and verified you are good to participate in this free of cost Blogging contest.</li>
                  <li>There are 49 blogs Heading per each World Cup game already created that can be accessed from the World Cup Logo on the home page.</li>
                  <li>Click the the World Cup Blog Contest/Discussion Link per each game and then start blogging by posting your replies while logged in to your account.</li>
                  <li>You can write in every way possible expressing your views and can be critical and vocal. However, refrain from using foul language or religiously offending a fellow blogger.</li>
                  <li>You can begin blogging for any World Cup game from February 14th 2015 .However the blogging cut off time for each blog is 24 hours after the game. Any blogging after that will not be considered for contest.</li>
                  <li>Our team of SMO technical writer will be reviewing blogs per each match and will determine the best blogger of that particular blog specific to that game.</li>
                  <li>At the end of the world cup our team of SMO will review the names of winner of each World Cup game blog. The individual with most wins will be the final winner of the US $100 cash Prize. </li>
                  <li>Winner name and picture will be displayed on the home page of Youcricketer for entire 1 month and permanently on Secondary pages of the site.</li>
                  <li>Our Admin will contact you through email and phone if listed in your account to discuss the methodology of the delivery of your $100.</li>
                </ol>
                <p class="orange"><strong>Good Luck</strong></p>
              </div>
            </div>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
</div><!-- /.blogViewModel-Model -->	
	
<?php include('common/footer.php'); ?>