<?php include_once('includes/configuration.php');
$page = 'league-umpires-search.html';
$selected_country = getGeoLocationCountry();

$countries = array();
$rs_countries = mysqli_query($conn,"select id,name from countries where status='1'");
while($row = mysqli_fetch_assoc($rs_countries)){
	$countries[] = $row;
}

$clubs = array();
$rs_clubs = mysqli_query($conn,"select id,title from clubs where status='1' order by title");
while($row = mysqli_fetch_assoc($rs_clubs)){
	$clubs[] = $row;
}

$ppage = intval($_GET["page"]);
if($ppage<=0) $ppage = 1;

$league_info = array();
$user_info  = array();
$league_umpires = array();

if(empty($_SESSION['ycdc_dbuid']) &&  isset($_SESSION['ycdc_user_email']) && !empty($_SESSION['ycdc_user_email'])){
	$rowUser = mysqli_fetch_assoc(mysqli_query($conn,"select id from users where email = '".$_SESSION['ycdc_user_email']."'"));
	$_SESSION['ycdc_dbuid'] = $rowUser['id'];
}

if(isset($_SESSION['ycdc_dbuid']) && !empty($_SESSION['ycdc_dbuid'])){
	$user_id = $_SESSION['ycdc_dbuid'];
	$user_info = get_record_on_id('users', $user_id);
	if($user_info['is_company'] == 1){
		$sql = "select * from companies where user_id = $user_id and company_type_id IN(1,2,3,35) and status=1";
		$rs_company = mysqli_query($conn,$sql);
		if(mysqli_num_rows($rs_company)){
			$league_info = mysqli_fetch_assoc($rs_company);
			$sql = 'select * from league_umpires where league_id = '.$league_info['id'];
			$rs_club = mysqli_query($conn,$sql);
			while($row = mysqli_fetch_assoc($rs_club)){
				$league_umpires[] = $row['umpire_id'];
			}
		}
	}
}

if(isset($_POST['members_add_submit']) && !empty($_POST['members_add_submit'])){
	$members = $_POST['member_ids'];
	if(!empty($members)){
		for($i=0;$i<count($members);$i++){
			$sql = "INSERT into league_umpires SET league_id=".$league_info['id'].", umpire_id=".$members[$i];
			mysqli_query($conn,$sql);
		}
		if(!mysqli_error($conn)){
			$sql = "UPDATE companies SET tournament_data_added = 'yes' WHERE id = ".$league_info['id'];
			mysqli_query($conn,$sql);
		}
	}

	$_SESSION['league_umpire_added'] = 1;

	if(isset($_GET['keywords']) && !empty($_GET['keywords'])){
		$url_q.='-'.$_GET['keywords'];
	}else{
		$url_q.='-all';
	}
	if(isset($_GET['club_id']) && !empty($_GET['club_id']) ){
		$url_q.='-'.$_GET['club_id'];
	}else{
		$url_q.='-0';
	}
	if(isset($_GET['country_id']) && !empty($_GET['country_id']) ){
		$url_q.='-'.$_GET['country_id'];
	}else{
		$url_q.='-0';
	}

	header("Location:".WWW."league-umpires-search{$url_q}-".$ppage.'.html');
	exit;

}

if(isset($_SESSION['league_umpire_added']) && $_SESSION['league_umpire_added']==1) {
	$member_added = 1;
	unset($_SESSION['league_umpire_added']);
}

if(isset($_GET['club_id']) && !empty($_GET['club_id'])){
	$query = "select company_name from companies where company_type_id=4 and id =".$_GET['club_id'];
	$rs   = mysqli_query($conn,$query);
	if(mysqli_num_rows($rs)){
		$club_arr = mysqli_fetch_assoc($rs);
		$club_name = $club_arr['company_name'];
	}
}

$page_title = 'Add Umpires, Referees & Scorers';
?>
<?php include('common/header.php'); ?>

<?
						//$club_id = 0;

						$rpp = PRODUCT_LIMIT_FRONT; // results per page

                		$country_id = 0;

      					$query = "select distinct u.* from users u ";
						$query_count = "select count(*) as users_count from users u ";
				      	//=======================================
						$where = " where u.is_company = '0'  and u.status = 1";
						if(isset($_GET['club_id']) && !empty($_GET['club_id'])){
							$club_id = trim($_GET['club_id']);
							$join  = " INNER JOIN club_members as c ON u.id=c.member_id ";
							$query.=$join;
							$query_count.=$join;
							$where.=" AND c.club_id = ".$club_id;
						}

						if(isset($_GET['country_id']) && !empty($_GET['country_id'])){
							$country_id = trim($_GET['country_id']);
							$where.=" AND u.country_id = ".$country_id;
						}

					    if(isset($_GET['keywords']) && $_GET['keywords'] != 'Search Here' && $_GET['keywords'] != 'all' ){
							$keywords = trim($_GET['keywords']);
							if(strpos($keywords,'_') !== false){
								$keywords_arr = explode('_',$keywords);
								$where.= " and (u.f_name like '%".$keywords_arr[0]."%'  AND u.last_name like '%".$keywords_arr[1]."%') ";
							}else{
								$where  .= " and (u.f_name like '%{$keywords}%' OR u.m_name like '%{$keywords}%' OR u.last_name like '%{$keywords}%') ";
							}
						}else if(isset($_GET['member_name_srch']) && $_GET['member_name_srch'] != 'Member Name' && $_GET['member_name_srch'] != 'all' ){
							$keywords = trim($_GET['member_name_srch']);
							$where  .= " and (u.f_name like '%{$keywords}%' OR u.m_name like '%{$keywords}%' OR u.last_name like '%{$keywords}%') ";
						}else{
							$keywords = '';
						}

						$query.=$where;
						$query_count.=$where;
					    $query .= " order by u.id desc ";

						$rs_count   = mysqli_query($conn,$query_count);
						$row_count  = mysqli_fetch_assoc($rs_count);
						$tcount = $row_count['users_count'];

						$tpages = ($tcount) ? ceil($tcount/$rpp) : 1;
						$count = 0;
						$start = ($ppage-1)* $rpp;
						$x = 0;

						$query .= " LIMIT $start,$rpp "; ?>





<div class="page-container">
		<?php include('common/user-left-panel.php');?>
      <!-- END SIDEBAR -->

      <!-- BEGIN CONTENT -->
      <div class="page-content-wrapper">
        <div class="page-content">

          <div class="white-box">
            <div class="row">
              <div class="col-md-12">
                <h2>  Add Umpires, Referees & Scorers  </h2>

				<?php if(isset($member_added) && $member_added == 1): ?>
					<div id="information" class="alert alert-success">Umpires added Successfully... !</div>
				<?php  endif; ?>

				<?php if(empty($user_info)): ?>
					<div id="error" class="alert alert-danger">You are not logged... !</div>
				<?php endif; ?>

				<?php if(!empty($user_info) && empty($league_info) ): ?>
					<div id="error" class="alert alert-danger">You are not logged as League Owner.. !</div>
				<?php endif; ?>
              </div>
            </div>

			<div id="adv_search" class="panel panel-default">
              <div class="panel-body">
                <div class="form-group">
                  <div class="col-sm-12">
                    <h3> Advanced Search </h3>
                  </div>
                </div>
                <form class="form-horizontal" id="advance_search_form" method="post" action="">
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Club Name</label>
                    <div class="col-sm-7">
						<input class="form-control" id="search_box" name="club_name" <?php if(isset($club_name) && !empty($club_name)){?> value="<?php echo $club_name; ?>" <?php } else{ ?> placeholder="Club Name" <?php } ?> autocomplete="off" type="text">
						<div style="clear:both"></div>
						<div id="searchres" class="searchres"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Country</label>
                    <div class="col-sm-7">
                      <select name="country_id_srch" id="country_id_srch" class="form-control" >
						<option value="0">Country</option>
						<?php for($i=0;$i<count($countries);$i++): ?>
							<?php if(isset($_GET['country_id']) && $_GET['country_id'] == $countries[$i]['id']) $sel = 'selected';else $sel = ''; ?>
							<option <?php echo $sel; ?> value="<?php echo $countries[$i]['id']; ?>"><?php echo $countries[$i]['name']; ?></option>
						<?php endfor; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Member Name</label>
                    <div class="col-sm-7">
                      <input class="form-control" name="member_name_srch" id="member_name_srch" <?php if(isset($_GET['keywords']) && !empty($_GET['keywords'])): ?> value="<?php echo str_replace('_',' ',$_GET['keywords']); ?>" <?php else: ?>  placeholder="Member Name" <?php endif; ?>  type="text">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-5 col-sm-7">
                      <input type="hidden" name="club_id" id="club_id"  value="<?php if(isset($club_id) and $club_id>=0){ echo $club_id; }else{ echo '-1'; } ?>" >
                      <input name="submit_adv_search" id="submit_adv_search" value="Search" class="btn orange hvr-float-shadow" type="submit">
                    </div>
                  </div>
                </form>
              </div>
            </div>

			<?php if(!empty($_GET['keywords']) || !empty($_GET['league_id']) || !empty($_GET['country_id'])){ ?>
            <div id="pagination-top">
              <div class="row">
                <div class="col-sm-6">
                <? if($tcount != 0 && $tcount > PRODUCT_LIMIT_FRONT){ ?>
		      	<?php
		        	$q_url = '';
					if(isset($keywords) && !empty($keywords)){
						$q_url.="-{$keywords}";
					}else{
						$q_url.="-all";
					}
					if(!isset($club_id)){
						$club_id=0;
					}
					if(isset($club_id) ){
						if($club_id==-1){
							$club_id = 0;
						}
						$q_url.="-{$club_id}";
					}
					if(isset($country_id) ){
						$q_url.="-{$country_id}";
					}

					if(!empty($q_url)){
						$reload = "league-umpires-search{$q_url}.html?";
					}else{
						$reload = "league-umpires-search-all.html?";
					}
		        	echo paginate_one($reload, $ppage, $tpages);
		      	?>
		      	<input type="hidden" name="pagination-page" value="league-umpires-search.html">
		        <? } ?>
				</div>
                <div class="col-sm-3" id="search-div1">
					<!-- <form id="list-search" method="post" action="">
                  <div class="input-group" >
                    <input class="form-control validate[required] input-login" name="txtsearch" id="txtsearch" placeholder="Search Here" type="text">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit">Go!</button>
                    </span>
                  </div>
				  </form> -->
                </div>
              </div>
            </div>

			<form method="post">
            <div id="individual" class="content1">
				<?php
						$rs   = mysqli_query($conn,$query);
						$rcount = mysqli_num_rows(mysqli_query($conn,$query));
						if($rcount == 0){
							echo '<div id="information" class="alert alert-danger">No record found ... !</div>';
						}
						///echo $query;
						$i= 0 ;
						while($row 	= mysqli_fetch_assoc($rs)){
							if($row['permalink']!=''){
								$pLink=WWW."yci/".$row['permalink'];
							}else{
								$pLink=WWW."individual-detail-".$row['id']."-".friendlyURL($row['f_name']." ".$row['m_name']." ".$row['last_name']).".html";
							}

							$row_img = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$row['id']."' and is_default = '1' "));
			  				$row_country= get_record_on_id('countries', $row['country_id']);
			  				$location 	= $row_country['name'];
			  				$row_vid = mysqli_fetch_assoc(mysqli_query($conn,"select * from videos where entity_id = '".$row['id']."' and entity_type = 'users' and is_default = '1' and file_name != ''"));

		        ?>
              <dl>
                <dt> <a href="<?=$pLink?>" title="<?=$row['f_name'].' '.$row['m_name'].' '.$row['last_name']?>"><img src="<?php echo WWW;?><?=($row_img && !empty($row_img['file_name']))?'users/'.$row['id'].'/photos/'.$row_img['file_name']:'images/no-photo.jpg'?>"></a></dt>
                <dd>
                  <div class="details">
                    <h3><a href="<?=$pLink?>" title="<?=$row['f_name'].' '.$row['m_name'].' '.$row['last_name']?>"><?=truncate_string($row['f_name'],20).'<br>'.truncate_string($row['last_name'], 20)?></a></h3>
									<p><? echo $location;?></p>

							<?if($row['user_type_id'] != 1) {
								echo get_combo('user_types','name',$row['user_type_id'],'','text');
							?><br />
							<?   if($row['user_type_id'] == 2){ echo $row['type'];} }?>

							<?php if(!empty($league_info)) { ?>

										<?php if(!in_array($row['id'],$league_umpires)){ ?>
											<p><input type="checkbox" name="member_ids[]" id="member_id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>"> Add to League</p>
										<?php } else{ ?>
											<p>Already League Umpire</p>
										<?php } ?>
							<?php } ?>

								<a href="<?=$pLink?>" title="<?=$row['f_name'].' '.$row['m_name'].' '.$row['last_name'].' - '.$site_title?>">View Profile</a>
								<span class="flag"><img title="<?=$row_country['name']?>" alt="<?=$row_country['name']?>" src="<?php echo WWW;?>countries/<?=$row_country['flag']?>"></span>
						</div>
                  <div class="video">
						<?php
										$video = $row_vid['file_name'];
											if(!empty($video)){
											if(preg_match('/<iframe(.*)<\/iframe>/', $row_vid['file_name'])){

														preg_match('/src="(.*?)"/',$row_vid['file_name'] , $src);
														$src = $src[1];
														$video = "<iframe width='205' height='130' frameborder='0' allowfullscreen src='$src'></iframe>";


											}else{
												$filename = explode('.',$video);
												$filename1= $filename[0];
												$video = '<video width="220" height="130" controls>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mp4" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.ogg" type=\'video/ogg; codecs="theora, vorbis"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.mov" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_vid['id'].'/'.$filename1.'.webm" type=\'video/webm; codecs="vp8, vorbis"\'>
												</video>';
											}
										}else{
											$video = '<img src="'.WWW.'images/no-video.jpg" >';
										}
										echo $video;?>
					</div>
                </dd>
              </dl>
			  <?php

				$i++;
				$count++;
				$x++;
				}
				?>

              <div class="clearfix"></div>
            </div>
			<div class="clearfix"></div>
						<?php if($rcount > 0){?>
						<div class="row">
              <div class="col-sm-12">
                <input name="members_add_submit" id="members_add_submit" value="Add Umpires" type="submit" class="btn orange hvr-float-shadow" style="margin:15px 0">
              </div>
            </div>
					<?php } ?>
			</form>
            <div class="clearfix"></div>
            <div id="pagination-bottom">
              <div class="row">
                <div class="col-sm-6">
                <? if($tcount != 0 && $tcount > PRODUCT_LIMIT_FRONT){ ?>
		      	<?php
		        	$q_url = '';
					if(isset($keywords) && !empty($keywords)){
						$q_url.="-{$keywords}";
					}else{
						$q_url.="-all";
					}
					if(isset($club_id) ){
						$q_url.="-{$club_id}";
					}
					if(isset($country_id) ){
						$q_url.="-{$country_id}";
					}

					if(!empty($q_url)){
						$reload = "league-umpires-search{$q_url}.html?";
					}else{
						$reload = "league-umpires-search-all.html?";
					}
		        	echo paginate_one($reload, $ppage, $tpages);
		      	?>
		      	<input type="hidden" name="pagination-page" value="league-umpires-search.html">
		        <? } ?>
				</div>
                <div class="col-sm-3" id="search-div2">
					<!-- <form id="list-search" method="post" action="">
                  <div class="input-group">
                    <input class="form-control validate[required] input-login" name="txtsearch" id="txtsearch" placeholder="Search Here" type="text">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit">Go!</button>
                    </span>
                  </div>
				  </form> -->
                </div>
              </div>
            </div>
			<?php } ?>

          </div>
        </div>
      </div>
      <!-- END CONTENT-->
    </div><!-- /.container -->

	<script type="text/javascript">
		        $('form#list-search').submit(function(e){
					var parentId = $(this).parent().attr('id');
					var string = $('#'+parentId +' form input[name=txtsearch]').val();
					if(string != '' && string != 'Search Here'){
						string = string.replace(/[ ]+/g,'_');
						string = string.replace(/[^a-zA-Z0-9_]+/g,'').toLowerCase();
						if(string.length > 0){
							$('form#list-search').attr('action','<?=WWW;?>league-umpires-search-' + string + '.html');
						}
					}
				});

				$('#submit_adv_search').click(function(e){

					var string = $('#member_name_srch').val();
					if(string == ''){
						string = 'all';
					}
					if($("#search_box").val()!=''){
						var club_id = $('#club_id').val();
					}else{
						var club_id = 0;
					}
					var country_id = $('#country_id_srch').val();
					//if(string != '' && string != 'Member Name'){
						string = string.replace(/[ ]+/g,'_');
;						string = string.replace(/[^a-zA-Z0-9_]+/g,'').toLowerCase();
						//if(string.length > 0){
							$('form#advance_search_form').attr('action','<?=WWW;?>league-umpires-search-' + string +"-"+club_id+"-"+country_id+ '.html');
						//}
					//}
				});
		        </script>

				<script type="text/javascript">
				$(document).ready(function(){
				$("#search_box").keyup(function(){
				var search_string = $("#search_box").val();
				$(".searchres").css({"display":"block"});
				if(search_string == ''){$("#searchres").html('');}else{postdata = {'string' : search_string}
				$.post("get_clubs.php",postdata,function(data){
						if(data==0){
							var name = $("#search_box").val();
							fillme(name,-1);
							data = '<div class="no_data">No Result Found !</div>';
						}
						$("#searchres").html(data);
				});}});});
				function fillme(name,id){$("#search_box").val(name);$("#searchres").html(''); $("#club_id").val(id);}
				$(".searchres").click(function(){
					$(".searchres").hide('fast');
				});
				$("#search_box").focusout(function(){
					$(".searchres").hide('fast');
				});
				</script>

<style type="text/css">
.searchres{margin-left:2px;width:97%;border: #f0f0f0 1px solid;border-radius:5px;position:absolute;z-index:100;background-color:#fff; height: 285px;overflow-y: scroll;display: none;}
.img{float:left;margin: 5 5 5 5px;float:left;}
.user_div{clear:both;font-family: verdana;border-top: #f0f0f0 1px solid;color:black; font-size: 13px;height:20px;margin:1px;padding:13px;width:95%;border-radius:4px;vertical-align: top;  }
.user_div:hover{background:#F6F6F6;color:#fff;cursor:pointer}
.no_data{height: 40px;background-color: #F0F0F0;padding: 10 0 0 10px;width: 180px;border-radius: 5px;font-family: verdana;color:black; font-size: 15px;}
</style>
<?php include('common/footer.php'); ?>
