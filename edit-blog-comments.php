<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	
<?php 	
			$id 	= $_GET['id'];
			$row 	= get_record_on_id('blog_comments', $id);
			$rowBlog= mysqli_fetch_assoc(mysqli_query($conn,"select * from blog_articles where id = '".$row['blog_article_id']."'"));

			if(isset($_POST) && !empty($_POST)){ 
				$comments 	= addslashes($_POST['comments']);
				$id 		= $_GET['id'];
				$query = "update blog_comments set comments = '$comments' where id = '$id'";
				mysqli_query($conn,$query);
				?>
				<script>
				window.location = '<?php echo WWW;?>blog-detail-<?php echo $rowBlog['id']?>-<?php echo friendlyURL($rowBlog['title'])?>.html';
				</script>
				<?
			} ?>

<script src="<?=WWW;?>assets/js/ckeditor/ckeditor.js"></script>

	<div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1>Update Blog Comments </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <div class="white-box">
            <div class="row">
              <div class="col-md-12">
                <h2>Blog Comments Information</h2>
                <form method="post" action="" class="form-horizontal">
                  
                  <div class="form-group">
                   
                    <div class="col-sm-9">
                      <textarea maxlength="500" name="comments" class="ckeditor validate[required]"><?=$row['comments']?></textarea>
                    </div>
                  </div>
                  
                 
                  <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                      <input value="Submit" class="btn orange hvr-float-shadow" type="submit">
					  <a id="cancel" class="btn blue hvr-float-shadow" href="<?php echo WWW;?>blog-detail-<?php echo $rowBlog['id']?>-<?php echo friendlyURL($rowBlog['title'])?>.html">Cancel</a>
                    </div>
                  </div>
                </form>
              </div>
              
            </div>
		  </div>
        </div>
        
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
				<?php include('common/right-panel.php');?>
            </div>
          </div>
          
        </div>
      </div>
    </div><!-- /.container -->



<?php include('common/footer.php'); ?>