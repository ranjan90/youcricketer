 function  news() {
 alert("Parsing of News Start");
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  xml=new XMLHttpRequest();
	  }
	else
	  {// code for IE6, IE5
	  xml=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	 var url = 'espn/news.php';
	xml.open("POST",url,true);
	xml.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xml.onreadystatechange = function(){

	if(xml.readyState == 4 && xml.status == 200) {
    
		    var return_data = xml.responseText;
			document.getElementById("get_news").innerHTML = return_data;
			alert("parsing of news Ends");
			 document.getElementById("news_status").innerHTML = "";
			
	    

    } 
}
xml.send();
	  // document.getElementById("news_status").innerHTML = "<img src='images/loading.gif' />"; 
	}



///////////////////////////

 function  fixtures() {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  fixture =new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
			fix=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		 var url = 'espn/fixture.php';
		 fixture.open("POST",url,true);
		 fixture.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		 fixture.onreadystatechange = function(){

		if(fixture.readyState == 4 && fixture.status == 200) {
	    
			    var return_data = fixture.responseText;
				document.getElementById("get_fixtures").innerHTML = return_data;
				 document.getElementById("fixture_status").innerHTML = "";
				
		    

	    } 
	}
		 fixture.send();
		//   document.getElementById("fixture_status").innerHTML = "<img src='images/loading.gif' />"; 
		}
 
 
 ////////////////////////////
 
 function  scores() {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
			score =new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
			score=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		 var url = 'espn/score.php';
		 score.open("POST",url,true);
		 score.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		 score.onreadystatechange = function(){

		if(score.readyState == 4 && score.status == 200) {
	    
			    var return_data = score.responseText;
				document.getElementById("get_scores").innerHTML = return_data;
				 document.getElementById("score_status").innerHTML = "";
				
		    

	    } 
	}
		 score.send();
		//   document.getElementById("score_status").innerHTML = "<img src='images/loading.gif' />"; 
		}


////////////////////////////

 function  results() {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
			result =new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
			result=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		 var url = 'espn/results.php';
		 result.open("POST",url,true);
		 result.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		 result.onreadystatechange = function(){

		if(result.readyState == 4 && result.status == 200) {
	    
			    var return_data = result.responseText;
				document.getElementById("get_results").innerHTML = return_data;
				 document.getElementById("result_status").innerHTML = "";
				
		    

	    } 
	}
		 result.send();
		//   document.getElementById("result_status").innerHTML = "<img src='images/loading.gif' />"; 
		}


////////////////////////////
	function addLoadEvent(func) {
	  var oldonload = window.onload;
	  if (typeof window.onload != 'function') {
	    window.onload = func;
	  } else {
	    window.onload = function() {
	      if (oldonload) {
	        oldonload();
	      }
	      func();
	    }
	  }
	}
	addLoadEvent(news);
	addLoadEvent(fixtures);
	addLoadEvent(scores);
	addLoadEvent(results);