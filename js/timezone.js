$(document).ready(function(){
	if($('input[name=timezone]')){
		var timezone = jstz.determine();
		$('input[name=timezone]').val(timezone.name());	
	}
});