$(document).ready(function(){
	if($('.all-banners').length){
		$('#all-banners-navigation').html('');
			
		$('.all-banners').cycle({
			fx:'fade',
			timeout:5000,
			speed:800,
			pause:1,
			//pager: '#all-banners-navigation'
		});
					
		$('#all-banners').css('visibility','visible');
	}
});		
