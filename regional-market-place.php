<?php include_once('includes/configuration.php');
$page = 'regional-market-place.html';
 include('common/header.php');
 $square_msg='';


 /* Square Process */
 if(isset($_POST['square'])){

   $ttlQty=array();
   $title=array();
   $ttlPrice=array();
   $line_items='';

   for($i=0;$i<=count($_POST['qty']);$i++){

     if(isset($_POST['ids'][$i])){

       $id=$_POST['ids'][$i];

       $item[]=$_POST['ids'][$i];
       $ttlQty[]=$_POST['qty'][$id];
       $title[]=$_POST['title'][$id];
       $ttlPrice[]=$_POST['price'][$id];

       $line_items.='{"name":"'.$_POST['title'][$id].'","quantity":"'.$_POST['qty'][$id].'","base_price_money":{"amount":'.($_POST['price'][$id]*100).',"currency":"USD"}},';

     }

   }

   $line_items=rtrim($line_items,',');

   $_SESSION['square']['user_id']=$_SESSION['ycdc_dbuid'];
   $_SESSION['square']['item']=$item;
   $_SESSION['square']['title']=$title;
   $_SESSION['square']['price']=$ttlPrice;
   $_SESSION['square']['quantity']=$ttlQty;

   /* client location id*/
   $location="CBASECUjDAZuTF_vJ0UVtWTJtJIgAQ";

   /* Post Url*/
   $url="https://connect.squareup.com/v2/locations/".$location."/checkouts";

   /* Authorization headers */
   $header[]="Authorization : Bearer sandbox-sq0atb-EUScP628qDebNAxU0oYJIg";
   $header[]="Content-Type : application/json";

   $ur=WWW."regional-market-place.html?square_callback=yes";

   /* post data*/
   $var='{"redirect_url":"'.$ur.'","idempotency_key":"'.uniqid().'","ask_for_shipping_address":true,"merchant_support_email":"anujsetia@1wayit.com","order":{"reference_id":"Youcricketer","line_items":['.$line_items.']}}';

   /* curl */
   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL,$url);
   curl_setopt($ch, CURLOPT_POST, 1);
   curl_setopt($ch, CURLOPT_POSTFIELDS,$var);
   curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

   /* response */
   $response =json_decode(curl_exec ($ch));
   curl_close ($ch);

   /* if return checkout url then it redirect to on that url else show error msg*/
   if(isset($response->checkout->checkout_page_url)){

       echo "<script>window.location.href='".$response->checkout->checkout_page_url."'</script>";

   }else{
     createMsg("The Amount must be greater than 100.",'error');
   }

 }


 /* paypal Process */
 if(isset($_POST['paypal'])){

   $url='';

   $ttlQty=array();
   $title=array();
   $ttlPrice=array();
   $tl='';
   $amount=0;
   $qty=0;

   for($i=0;$i<=count($_POST['qty']);$i++){

     if(isset($_POST['ids'][$i])){

         $id=$_POST['ids'][$i];

         $item[]=$_POST['ids'][$i];
         $ttlQty[]=$_POST['qty'][$id];
         $qty+=$_POST['qty'][$id];
         $title[]=$_POST['title'][$id];
         $ttlPrice[]=$_POST['price'][$id];
         $tl.=$_POST['title'][$id].', ';
         $amount+=$_POST['price'][$id];

       }

   }

   $_SESSION['paypal']['user_id']=$_SESSION['ycdc_dbuid'];
   $_SESSION['paypal']['item']=$item;
   $_SESSION['paypal']['title']=$title;
   $_SESSION['paypal']['price']=$ttlPrice;
   $_SESSION['paypal']['quantity']=$ttlQty;

   ?>
   <form action="<?php echo PAYPAL_URL;?>" method="post" id="paypal-sub">
       <input type="hidden" name="cmd" value="_cart">
       <input type="hidden" name="business" value="<?php echo PAYPAL_EMAIL;?>">
       <input type="hidden" name="item_number" value="<?php echo rand(1,100000);?>">
       <input type="hidden" name="upload" value="1">
       <input type="hidden" name="email" value="<?php echo $_SESSION['ycdc_user_email']?>">

       <?php for($i =0;$i< count($_SESSION['paypal']['item']);$i++){ ?>
         <input type="hidden" name="item_name_<?php echo $i+1;?>" value="<?php echo $_SESSION['paypal']['title'][$i]; ?>">
         <input type="hidden" name="amount_<?php echo $i+1;?>" value="<?php echo $_SESSION['paypal']['price'][$i]; ?>">
         <input type="hidden" name="quantity_<?php echo $i+1;?>" value="<?php echo $_SESSION['paypal']['quantity'][$i];?>">
       <?php } ?>

       <input type="hidden" name="custom" value="<?php echo base64_encode(serialize($_SESSION['paypal']));?>" />
       <!--<input type="hidden" name="currency_code" value="USD">-->
       <input type="hidden" name="return" value="<?=WWW."global-market-place.html?paypal=success"?>">
       <input type="hidden" name="cancel_return" value="<?=WWW."global-market-place.html?paypal=cancel"?>">
       <input type="hidden" name="notify_url" value="<?=WWW."paypal_notify.php"?>">
   </form>
   <script>
       document.getElementById("paypal-sub").submit();
   </script>

   <?php

 }

 /* square callback url */
 if(isset($_GET['square_callback']) && $_GET['square_callback']=='yes'){

   if(isset($_SESSION['square']) && count($_SESSION['square']['item'])>0){

       for($i=0;$i<count($_SESSION['square']['item']);$i++){

            mysqli_query($conn,"insert into user_payments (payment_type,is_sponsor,user_id,item_id,quantity,price,transaction_id,status,payment_status)values('square','1','".$_SESSION['ycdc_dbuid']."','".$_SESSION['square']['item'][$i]."','".$_SESSION['square']['quantity'][$i]."','".$_SESSION['square']['price'][$i]."','".$_GET['transactionId']."','VERIFIED','Completed')");

       }
       unset($_SESSION['square']);

       createMsg("Your Payment has been done");

       my_redirect('regional-market-place.html');

       die();


   }else{

       my_redirect('regional-market-place.html');

   }

 }

 /* Paypal Callback*/
 if(isset($_GET['paypal'])){

   if($_GET['paypal']=='success'){

     createMsg("Your Payment has been completed.Please wait for verification we respond you shortly!...");

     my_redirect('regional-market-place.html');

     die();

   }elseif($_GET['paypal']=='cancel'){

     createMsg("Your Payment has been cancelled","error");

     my_redirect('regional-market-place.html');

     die();

   }

 }

 ?>
 <div class="page-container">
        <?php include('common/user-left-panel.php');?>
        <!-- END SIDEBAR -->

        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="white-box" style="overflow: hidden !important;">
                  <?php echo displayMsg();?>


      				<div class="tit_head">
      					<div class="row">

                    <div class="col-sm-12">
    							         <h2>Regional Products & Services</h2>
      						  </div>
                    <div style="clear:both;"></div><br/>

                    <div class="col-md-12">
                      <div class="alert alert-info">
                        <?php $rowMsg=get_record_on_id('cms', 53);  ?>
                        <?php echo $rowMsg['content'];?>
                      </div>
                    </div>

                    <div style="clear:both;"></div><br/>

                    <form action="" method="get" class="">

                    <div class="filter_panel">
                      <div class="col-md-3">
                        <?php
          							$cQuery=mysqli_query($conn,"select * from countries");
          							?>
                        <select name="country" class="countries form-control">
          								<option value="">Choose Country</option>
          								<?php while($record=mysqli_fetch_assoc($cQuery)){?>
          									<option value="<?php echo $record['name'];?>" data-id="<?php echo $record['id'];?>" <?php if(isset($_GET['country']) && $_GET['country']==$record['name']) echo "selected='selected'";?>><?php echo $record['name'];?></option>
          								<?php } ?>
          							</select>
                        </div>

                        <div class="col-md-3">
                          <?php
            							$cstates=mysqli_query($conn,"select * from states");
            							?>
                          <select name="state" class="states form-control">
            								<option value="">Choose State</option>
                            <?php while($record=mysqli_fetch_assoc($cstates)){?>
            									<option value="<?php echo $record['name'];?>" data-id="<?php echo $record['id'];?>" <?php if(isset($_GET['state']) && $_GET['state']==$record['name']) echo "selected='selected'";?>><?php echo $record['name'];?></option>
            								<?php } ?>
            							</select>
                        </div>
                        <div class="col-md-3">
                            <input type="text" name="city" class="form-control" value="<?php if(isset($_GET['city'])) echo $_GET['city'];?>" placeholder="City" />
                        </div>
                        <div class="col-md-3">
                            <input type="submit" name="search" class="btn btn-primary" value="Search" />&nbsp;&nbsp;
                            <a  class="btn btn-primary" href="<?php echo WWW;?>regional-market-place.html">Reset Filter</a>
                        </div>

                        <div style="clear:both;"></div><br/>

                      </div>
                     </form>

                    </div>

      						</div>


            <form action="" method="post" class="global-form">

              <div class="btns text-right">
				<p>Payment Methods : <a class="payment-btn" data-id="square"><img src="<?php echo WWW;?>images/square.png" alt="Square Payment" title="Square Payment"/></a>
				<a  class="payment-btn" data-id="paypal"><img src="<?php echo WWW;?>images/paypal.png" alt="Paypal Payment" title="Paypal Payment"/></a></p>

                <input type="submit" name="square" value="Square" class="btn square orange hvr-float-shadow" style="display:none;">
                <input type="submit" name="paypal" value="Paypal" class="btn paypal orange hvr-float-shadow"  style="display:none;">
              </div>
              <div style="clear:both;"></div><br/>

	            <div class="global_regional_panel">

                <?php

				$tquery=mysqli_query($conn,"select * from marke_place_items where item_type='2'  and status='1' order by item_id asc");
				$rpp = PRODUCT_LIMIT_FRONT;
				$ppage = intval($_GET["page"]);

				if($ppage<=0){

					$ppage = 1;
					$start=0;
					$limit=$rpp;

				}else{
					$start=($ppage-1)*$rpp;
					$limit=$rpp;
				}

        $sql="select * from marke_place_items where item_type='2' and status='1' ";

         if(isset($_GET['country']) && !empty($_GET['country'])){
           $sql.=" and item_country like '%".$_GET['country']."%'";
         }

         if(isset($_GET['state']) && !empty($_GET['state'])){
          $sql.=" and item_state like '%".$_GET['state']."%'";
         }

         if(isset($_GET['city']) && !empty($_GET['city'])){
          $sql.=" and item_city like '%".$_GET['city']."%'";
         }

         $sql.=" order by item_id asc limit $start,$limit";

        $query=mysqli_query($conn,$sql);

				$tcount = ceil(mysqli_num_rows($tquery)/$rpp);

                if(mysqli_num_rows($query)>0){
                      while($record=mysqli_fetch_assoc($query)){
                  ?>
                <input type="hidden" value="1">
                <input type="hidden" name="title[<?php echo $record['item_id']?>]" value="<?php echo $record['item_title'];?>">
                <input type="hidden" name="price[<?php echo $record['item_id']?>]" value="<?php echo $record['item_price'];?>">

      					<div class="global_regional">
      						<div class="row">
      							<div class="col-md-6">
      								<h4><input type="checkbox" class="chk-bx all_ids" name="ids[]" value="<?php echo $record['item_id'];?>"/> <?php echo $record['item_title'];?></h4>
      							</div>
      							<div class="col-md-3">
      								<h4>Price  <span>$ <?php echo number_format($record['item_price'],2);?></span></h4>
      							</div>
      							<div class="col-md-3">
      								<h4>Qty
                        <select class="form-control select_bx"  name="qty[<?php echo $record['item_id']?>]">
                        <?php for($i=1;$i<=20;$i++){?>
                          <option value="<?php echo $i;?>"><?php echo $i;?></option>
                        <?php } ?>
        							</select>
                      </h4>
      							</div>
      						</div>
      						<div class="row" id="detail_address">
      							<div class="col-md-12">
      								<p><?php echo $record['item_detail'];?></p>
                      <p><label><?php
                        $address='';

                        if($record['item_city']!='')
                          $address.=$record['item_city'].", ";

                        if($record['item_state']!='')
                          $address.=$record['item_state'].", ";

                        if($record['item_country']!='')
                          $address.=$record['item_country'];

                      echo ucwords($address);?></label></p>
      							</div>
      						</div>
      					</div>

              <?php }?>
				<?php if($tcount>=1){?>
				<div id="pagination-bottom">
				  <?php
					$reload = 'regional-market-place.html';
					echo paginate_one($reload, $ppage, $tcount);
				  ?>
				</div>
				<?php } ?>
            <?php }else{

                echo "<p>No Records found</p>";

            }
               ?>

      				  </div>
                </form>
      				</div>
					</div>
            </div>
        </div>


  </div>
  <script type="text/javascript">
    $(document).ready(function(){
      $(".payment-btn").click(function(){

          var id=$(this).attr('data-id');

          $("."+id).trigger('click');

      });
      $(".global-form").submit(function(){
          var check=$(".all_ids:checked").length;
          if(check>0)
            return true;
          else
            alert("Please Choose at least one item");

          return false;
      });

      $(".countries").change(function(){

				var country=$(".countries > option:selected").attr('data-id');

				$.ajax({
					url: "ajax/states.php",
					type: "post",
					data: {country:country},
					success: function(result){
							$(".states").html(result);
						}
				});

			});

      <?php if(isset($_GET['country']) && !empty($_GET['country'])) {?>

        var country=$(".countries > option:selected").attr('data-id');

        $.ajax({
          url: "ajax/states.php",
          type: "post",
          data: {country:country,selected:"<?php echo $_GET['state']?>"},
          success: function(result){
              $(".states").html(result);
            }
        });

      <?php } ?>

    });
  </script>
<?php include('common/footer.php'); ?>
