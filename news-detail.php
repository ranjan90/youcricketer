<?php include('common/header.php'); ?>
<?php 
	$row = get_record_on_id('news',$_GET['id']);
	$row_img= mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'news' and entity_id = '".$row['id']."' order by id desc limit 1"));
?>
	
	<div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h1>
            Whats Happening Detail - <?=get_combo('news','title',$_GET['id'],'','text')?> <br>
            <span> <strong>Created By :</strong> <?=get_combo('users','f_name',$row['user_id'],'','text')?>&nbsp;<?=get_combo('users','last_name',$row['user_id'],'','text')?> 
			<? $u_row = mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM users WHERE id = '".$row['user_id']."'")); ?>   
			<strong>Location :</strong> <?=get_combo('countries','name',$u_row['country_id'],'','text')?>> <?=get_combo('states','name',$u_row['state_id'],'','text')?> ><?=$u_row['city_name']?>
			<strong>Created On :</strong> <?=date_converter($row['news_date'])?> </span>
          </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <div class="white-box">
            <div class="row">
              <div class="col-sm-3">
			  <img src="<?=($row_img)?WWW.'news/'.$row['id'].'/'.$row_img['file_name']:WWW.'images/community-news.png';?>" alt="<?=$row['title']?>" title="<?=$row['title']?>" class="img-responsive" />
                
              </div>
              <div class="col-sm-9">
				<!-- AddThis Button BEGIN -->
					<div class="addthis_toolbox addthis_default_style ">
					<a class="addthis_button_preferred_1"></a>
					<a class="addthis_button_preferred_2"></a>
					<a class="addthis_button_preferred_3"></a>
					<a class="addthis_button_preferred_4"></a>
					<a class="addthis_button_compact"></a>
					<a class="addthis_counter addthis_bubble_style"></a>
					</div>
					<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
					<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5073efa669740139"></script>
				<!-- AddThis Button END -->
					
                <p><?=$row['content']?></p>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
              <?php include('common/right-panel.php');?>
            </div>
          </div>
          
        </div>
      </div>
    </div><!-- /.container -->
	
<?php include('common/footer.php'); ?>