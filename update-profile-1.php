<?php include('common/header.php'); ?>
<? 

if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>

<?php 	
			$user_id = $_SESSION['ycdc_dbuid'];
			
			if(isset($_POST) && !empty($_POST) && $_POST['action'] == 'close'){
				$salt = 'zxzxqb79jsMaEzXMvCO2iWtzU2gT7rBoRmQzlvj5yNVgP4aGOrZ524pT5KoTDJ7vNiINPOVD9Tzx';
				$token = sha1($salt . $_SESSION['time']);
				if($token != $_POST['token'])
				{
					exit();
				}
				$closingComments = $_POST['close_comments'];
				mysqli_query($conn,"update users set closing_note = '$closingComments', status = '5' where id = '$user_id' and is_company = '0' ");
				?>
				<script>
					alert('Your account has been closed ... !');
					window.location = '<?=WWW?>logout.php';
				</script>
				<?
			}
			
			if(isset($_POST) && !empty($_POST) && $_POST['action'] == 'update'){

				$set = array();
				
				if($_POST['is_newsletter'] == 'on'){
					$isNews = '1';
				}else{
					$isNews = '0';
				}

				$set[] = "f_name = '".$_POST['f_name']."'";
				//$set[] = "m_name = '".$_POST['m_name']."'";
				$set[] = "last_name = '".$_POST['last_name']."'";
				//$set[] = "generic_id = '".$_POST['generic_id']."'";
				$set[] = "is_newsletter = '".$isNews."'";
				$set[] = "country_id = '".$_POST['country_id']."'";
				//$set[] = "date_of_birth = '".$_POST['year'].'-'.$_POST['month'].'-'.$_POST['day']."'";
				$set[] = "date_of_birth = '".$_POST['birthday']."'";
				$set[] = "user_type_id = '".$_POST['user_type_id']."'";
				$set[] = "type 		= '".$_POST['player_type']."'";
				$set[] = "timezone = '".$_POST['timezone']."'";
				$set[] = "gender = '".$_POST['gender']."'";
				/*
				$rsCheck  = mysqli_fetch_assoc(mysqli_query($conn,"select * from users where yc_email = '".$_POST['yc_email']."'"));
				if(isset($_POST['yc_email']) && mysqli_num_rows($rsCheck) == 0){
					$set[] = "yc_email = '".$_POST['yc_email']."'";
				}else{
					echo '<div id="error">Email Letters already exists ... !</div>';
				}
				*/
				$rsCheck  = mysqli_query($conn,"select * from users where permalink = '".$_POST['permalink']."'");
				
				if(isset($_POST['permalink'])){
					if(mysqli_num_rows($rsCheck) <= 0){
						$set[] = "permalink = '".$_POST['permalink']."'";
					}
				}
				

				if($user_type_id == '17'){
					$set[] = "is_company = '1'";
				}else{
					$set[] = "is_company = '0'";
				}
				
				$query  = "update users set ".implode(',',$set)." where id = '".$row_user['id']."'";
				//echo $query;exit;
				$user_id= $row_user['id'];

				if(mysqli_query($conn,$query)){
					/*
					if(!empty($_FILES['photo']['name'])){
						
						if(!is_dir('users/'.$user_id)){
							mkdir('users/'.$user_id,0777);
						}
						chmod('users/'.$user_id,0777);
						if(!is_dir('users/'.$user_id.'/thumbnails')){
							mkdir('users/'.$user_id.'/thumbnails',0777);
						}
						chmod('users/'.$user_id.'/thumbnails',0777);

						$filename 	= friendlyURL($f_name).'.jpg';
						$image 		= new SimpleImage();
						$image->load($_FILES["photo"]["tmp_name"]);
						$image->save('users/'.$user_id.'/'.$filename);
						chmod('users/'.$user_id.'/'.$filename,0777);
						
						$rs_photos = mysqli_query($conn,"select * from photos where entity_id = '$user_id' and entity_type = 'users' and is_default = '1'");
						if(mysqli_num_rows($rs_photos) > 0){
							mysqli_query($conn,"update photos set file_name = '$filename' where entity_id = '$user_id' and entity_type = 'users' and is_default = '1'");
						}else{
							mysqli_query($conn,"insert into photos (file_name, title, entity_id, entity_type, is_default) values ('$filename','$f_name','$user_id','users','1')");
						}		
						
						$image->load($_FILES["photo"]["tmp_name"]);
						$image->resizeToWidth('128');
						$image->save('users/'.$user_id.'/thumbnails/'.$filename);
						chmod('users/'.$user_id.'/thumbnails/'.$filename,0777);
						
					}
					*/

	                echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
	                ?>
	                <script>
	                	window.location = '<?=WWW?>account-information.html';
					</script>
	                <?
				}else{
					
				}
			}
			$row_photo = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_id = '".$row_user['id']."' and entity_type = 'users' and is_default = '1'"));
		?>


<div class="page-container"> 
	<?php include('common/user-left-panel.php');?>
	
	<!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
          
          <div class="white-box">
			
			<div class="row">
              <div class="col-sm-12">
                <h2> Account Information </h2>
              </div>
            </div>
            <input type="hidden" class="check-submit" value="1">
			
            <form method="post" action="" enctype="multipart/form-data" id="edit-account-frm" class="form-horizontal">
              <input name="action" value="update" type="hidden">
              <div class="form-group">
                <label class="col-sm-3 control-label"> First Name: </label>
                <div class="col-sm-3">
                  <input value="<?=$row_user['f_name']?>" name="f_name" maxlength="30" class="form-control validate[required]" type="text">
                </div>
                <label class="col-sm-3 control-label"> Last Name: </label>
                <div class="col-sm-3">
                  <input value="<?=$row_user['last_name']?>" maxlength="30" name="last_name" class="form-control" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label"> Date of Birth : </label>
                <div class="col-sm-3">
                  <input name="birthday" value="<?php echo $row_user['date_of_birth']; ?>" autocomplete="off" class="form-control datepicker validate[required]" maxlength="30" type="text">
                  <span class="help-block"><a href="#" onClick="javascript:void(0)" data-toggle="modal" data-target="#birthView">Why do we need your birthday?</a></span>
                </div>
                <label class="col-sm-3 control-label"> Gender: </label>
                <div class="col-sm-3">
                  <?=get_gender_combo($row_user['gender'],'');?>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label"> Country of Residence: </label>
                <div class="col-sm-3">
                  <?=get_combo('countries','name', $row_user['country_id'],'country_id');?>
                </div>
                <label class="col-sm-3 control-label"> Your Role: </label>
                <div class="col-sm-3">
				<select name="user_type_id" id="user_type_id" class="form-control validate[required]">
					<option value="">Select One</option>
						<? 	$rs_uts = mysqli_query($conn,"select * from user_types where id  != '1' and status = '1' order by name;");
							while($row_ut = mysqli_fetch_assoc($rs_uts)){?>
							<option <?=($row_ut['id'] == $row_user['user_type_id'])?'selected="selected"':'';?> value="<?=$row_ut['id']?>"><?=$row_ut['name']?></option>
						<? } ?>
				</select>
                <span class="help-block">You can further define Your Role in your Pofile section</span>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label"> Email Address: </label>
                <div class="col-sm-3">
                  <p class="form-control-static"><?php echo $row_user['email']; ?></p>
                </div>
                <label class="col-sm-3 control-label"> Registered On: </label>
                <div class="col-sm-3">
                  <p class="form-control-static"><?=date_converter($row_user['create_date'])?></p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label"> Timezone: </label>
                <div class="col-sm-3">
                  <?php echo get_combo('timezones', 'title', $row_user['timezone'],'timezone')?>
                </div>
                <label class="col-sm-3 control-label"> YC Email: </label>
                <div class="col-sm-3">
					<? if(empty($row_user['yc_email'])){ ?>
						<input type="text" name="yc_email" maxlength="255" class="form-control validate[required]" value="<?=strtolower(str_replace(' ','',$row_user['f_name'].$row_user['last_name']))?>">
					<? }else{ ?>
						<div class="text"><?=$row_user['yc_email']?>@youcricketer.com</div>
					<? } ?>
                  
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label"> YC Profile Permalink: </label>
                <div class="col-sm-3">
					<? if(empty($row_user['permalink'])){ ?>
						<input type="text" name="permalink" value="<?=strtolower(str_replace(' ','',$row_user['f_name'].$row_user['last_name']))?>" maxlength="255" class="form-control validate[required] check_permalink"><span class="my-chk-span"></span>
					<? }else{ ?> 
						<?=WWW?>yci/<?=$row_user['permalink']?> 
					<? } ?>
                </div>
                <label class="col-sm-3 control-label"> Referral Acceptance Counter: </label>
                <div class="col-sm-3">
                  <p class="form-control-static">
					<? 	$query 	= "select * from invitations where from_user_id = '$user_id' and status = 'Invitation Accepted'";
						$rs_friends = mysqli_query($conn,$query);
						echo mysqli_num_rows($rs_friends);
					?></p>
                  <span class="help-block"><a href="#" onClick="javascript:void(0)" data-toggle="modal" data-target="#counterView">What is this Counter for?</a></span>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12 text-center">
                  <input class="btn orange hvr-float-shadow" value="Update Information" type="submit">
                  <a class="btn blue hvr-float-shadow" href="dashboard.html">Go Back</a>
                </div>
              </div>
            </form>
            <div class="row">
              <div class="col-sm-12">
                <div class="alert alert-warning" id="close-my-account-link" style="cursor:pointer;" onclick="$('#account-close-frm').toggle();"> I want to close my account </div>
              </div>
            </div>
            <form method="post" class="form-horizontal" style="display:none;" id="account-close-frm">
              <input name="action" value="close" type="hidden">
              <div class="form-group">
                <label class="col-sm-5 control-label">Why you want to close account ?</label>
                <div class="col-sm-7">
                  <textarea name="close_comments" class="form-control validate[required]" rows="5"></textarea>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-5 col-sm-7">
                  <input value="Close Account" class="btn orange hvr-float-shadow" onclick="return confirm('Are You sure you want to close your Account. If yes then please click OK');" type="submit">
                  <?php
									$time = time();
									$_SESSION['time'] = $time;
									$salt = 'zxzxqb79jsMaEzXMvCO2iWtzU2gT7rBoRmQzlvj5yNVgP4aGOrZ524pT5KoTDJ7vNiINPOVD9Tzx';
									$token = sha1($salt . $time);
					?>
					<input type="hidden" name="token" value="<?php echo $token; ?>" />
                </div>
              </div>
            </form>
			
            
          </div>
        </div>
      </div>
      <!-- END CONTENT-->
		
</div>


    <div class="modal fade" id="birthView" tabindex="-1" role="dialog" aria-labelledby="birthViewModel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="settingLabel">Why do we need your Birthday </h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-12">
				<?php $row = get_record_on_id('cms',7);?>
                <p><?php echo $row['content']; ?></p>
              </div>
            </div>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.birthModel-Model -->
    
    <div class="modal fade" id="counterView" tabindex="-1" role="dialog" aria-labelledby="counterViewModel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="settingLabel">What is this counter for ? </h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-12">
                <?php $row = get_record_on_id('cms',14);?>
                <p><?php echo $row['content']; ?></p>
              </div>
            </div>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.counterView-Model -->
	
<script type="text/javascript">
$(document).ready(function(){
	$('.datepicker').datepicker({'format':'yyyy-mm-dd'});
	$('#edit-account-frm').validationEngine();
	
	$("#edit-account-frm").submit(function(){
		
		var chk=$(".check-submit").val();
		if(chk=='1'){
			return true;
		}else{
			return false;
		}
		
		
	});
	
	<?php if(empty($row_user['permalink'])){?>
		
		$(".check_permalink").next("span").html('');
 		
		var val=$(".check_permalink").val();
		
			$.ajax({
			  url: "ajax/permalk.php",
			  cache: false,
			  data:{val:val},
			  success: function(data){
				$(".check-submit").val(data);
				if(data=='1'){
					$(".check_permalink").next('span').html('<i class="fa fa-check">');
					
				}else{
					$(".check_permalink").next('span').html('<i class="fa fa-close">');
					
				}
			  }
			});
		  
	<?php } ?>
	
	$(".check_permalink").keyup(function(){
		$(".check_permalink").next("span").html('');
		var val=$(this).val();
		$.ajax({
		  url: "ajax/permalk.php",
		  cache: false,
		  data:{val:val},
		  success: function(data){
			$(".check-submit").val(data);
			if(data=='1'){
				$(".check_permalink").next('span').html('<i class="fa fa-check">');
				
			}else{
				$(".check_permalink").next('span').html('<i class="fa fa-close">');
				
			}
		  }
		});
	});
});
</script>	

<?php include('common/footer.php'); ?>