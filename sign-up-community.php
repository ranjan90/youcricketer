<?php include('common/header.php');
$page_type = array('blogs'=>'Blogs','forums'=>'Forums','groups'=>'Groups','testimonials'=>'Testimonials','member-news'=>'Whats Happening','events'=>'Events');
$page = $_GET['page'];
if(isset($_GET['region']) && $_GET['region']=='true' ){
	$region = ' in  your region';
}else{
	$region = '';
}
?>

<div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
			<h1>YouCricketer <?php echo $page_type["$page"]; ?> <?php echo $region; ?> </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <div class="white-box">
			<?php if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
            <div class="alert alert-warning">Please Login or Register!</div>
            <p><?php $rowWhoopsMsg = get_record_on_id('cms', 43); echo str_replace('{type}',$page_type["$page"].$region,$rowWhoopsMsg['content']);?></p>
            <?php include('common/login-box.php');?>
			<?php } ?>
		  </div>
        </div>
        
        <div class="col-md-2">
          <div class="row">
            <div class="col-sm-12">
              <?php include('common/right-panel.php');?>
            </div>
          </div>
         
        </div>
      </div>
</div><!-- /.container -->	

<?php include('common/footer.php'); ?>