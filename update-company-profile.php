<?php include('common/header.php'); ?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? }
if($_SESSION['delete_photo'] == "Del"){

	echo '<div id="success" class="alert alert-success"><b>Success : </b>Photo Deleted ... !</div><br><br>';
	unset($_SESSION['delete_photo']);
}
 	/*	Profile Photo code			*/
		$user_id = $row_user['id'];
		$f_name  = $row_user['f_name'];

			if(isset($_POST) && !empty($_POST['update_profile_photo'])){
				$filename = $_FILES['photo']['name'];
				$ext = pathinfo($filename, PATHINFO_EXTENSION);
				if($ext=='jpg' || $ext=='png' || $ext=='jpeg'){
					if(!empty($_FILES['photo']['name'])){
						if(!is_dir('users/'.$user_id)){
							mkdir('users/'.$user_id,0777);
						}
						chmod('users/'.$user_id,0777);
						if(!is_dir('users/'.$user_id.'/photos')){
							mkdir('users/'.$user_id.'/photos',0777);
						}
						chmod('users/'.$user_id.'/photos',0777);

						$filename 	= friendlyURL($f_name).'.jpg';
						$image 		= new SimpleImage();
						$image->load($_FILES["photo"]["tmp_name"]);
						$image->save('users/'.$user_id.'/photos/'.$filename);
						chmod('users/'.$user_id.'/photos/'.$filename,0777);

						$rs_photos = mysqli_query($conn,"select * from photos where entity_id = '".$user_id."' and entity_type = 'users' and is_default = '1'");
						if(mysqli_num_rows($rs_photos) > 0){
							mysqli_query($conn,"update photos set file_name = '$filename' where entity_id = '$user_id' and entity_type = 'users' and is_default = '1'");
						}else{
							mysqli_query($conn,"insert into photos (file_name, title, entity_id, entity_type, is_default) values ('$filename','$f_name','$user_id','users','1')");
						}
					}else{
						$row_photo_user = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$user_id."' and is_default = '1'"));
						$row_photo_id   = $row_photo_user['id'];
						mysqli_query($conn,"update photos set is_default = '0' where id = '".$row_photo_id."'");
						mysqli_query($conn,"update photos set is_default = '1' where id = '".$_POST['album_photo']."'");
					}

					echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
					// echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=1"</script>';
				}else{
					echo '<div id="error" class="alert alert-danger"><b>Error : </b>Invalid file type...!</div><br><br>';
				}
			}
			$row_photo = mysqli_fetch_assoc(mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$user_id."' and is_default = '1' order by id desc limit 1"));

			?>


			<? /*	Profile Video code			*/
			$user_id = $row_user['id'];
			if($_POST['album_video'] && !empty($_POST['album_video'])){
				$vid_id = $_POST['album_video'];

				$sql = mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM videos WHERE entity_id = '$user_id'and entity_type = 'users' and is_default = '1' "));
				$current_id = $sql['id'];

				$profile_video = mysqli_query($conn,"update videos set is_default = '0',profile_video = '0' WHERE id = '$current_id' ");
					if($profile_video){
				mysqli_query($conn,"update videos set is_default = '1',profile_video = '1' WHERE id = '$vid_id' ");
					}

			}

			if(isset($_POST) && !empty($_POST['update_profile_video'])){
			  if($_POST['video'] == 'embed'){
			  if(!empty($_POST['video_code'])){
					$rs_check_code = mysqli_query($conn,"select * from videos where entity_type = 'users' and entity_id = '".$row_user['id']."' and is_default = '1'");
					if(mysqli_num_rows($rs_check_code) == 0){
						mysqli_query($conn,"insert into videos (file_name, status, entity_id, entity_type, is_default)
						values ('".$_POST['video_code']."','1','".$row_user['id']."','users','1');");
					}else{
						$row_video = mysqli_fetch_assoc($rs_check_code);
						mysqli_query($conn,"update videos set file_name = '".$_POST['video_code']."' where id = '".$row_video['id']."'");
						echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
					}
				}
			  }else{
			  	if(!empty($_FILES['video_file']['name'])){
			  		if($_FILES['video_file']['type'] == 'video/mp4'
			  		|| $_FILES['video_file']['type'] == 'video/webm'
			  		|| $_FILES['video_file']['type'] == 'video/ogg'
			  		|| $_FILES['video_file']['type'] == 'video/mov'
			  		|| $_FILES['video_file']['type'] == 'video/quicktime'){
			  			$exte = explode('.', $_FILES['video_file']['name']);
			  			$ext  = $exte[count($exte)-1];
			  			$filename 	= friendlyURL($_FILES['video_file']['name']).'.'.$ext;
						$rs_check   = mysqli_query($conn,"select * from videos where entity_type = 'users' and entity_id = '".$row_user['id']."' and file_name = '".$filename."'");
						mysqli_query($conn,"insert into videos (file_name, status, entity_id, entity_type, is_default)
						values ('".$filename."','1','".$row_user['id']."','users','1');");
						$video_id = mysqli_insert_id($conn);
						mysqli_query($conn,"update videos set is_default = '0' where entity_type = 'users' and entity_id = '".$row_user['id']."' and id <> '$video_id'");
						mkdir('videos/'.$video_id,0777);
						chmod('videos/'.$video_id,0777);
						move_uploaded_file($_FILES['video_file']['tmp_name'], 'videos/'.$video_id.'/'.$filename);
						chmod('videos/'.$video_id.'/'.$filename,0777);
						echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div><br><br>';
			  		}else{
			  			echo '<div id="error" class="alert alert-danger"><b>Failure : </b>Invalid format ... !</div><br><br>';
			  		}
				}
			  }
			  echo '<script>window.location.href="'.WWW.'profile-information-step-1.html?step=2"</script>';
			}
			$row_video = mysqli_fetch_assoc(mysqli_query($conn,"select * from videos where entity_id = '$user_id' and entity_type = 'users' and is_default = '1'"));
			if(preg_match('/<iframe(.*)<\/iframe>/', $row_video['file_name'])){
				$embed = 1;
			}else{
				$embed = 0;
			}

			if(isset($_POST['update_company_information']) && !empty($_POST['update_company_information'])){
				$state_id 		= $_POST['state_id'];
				$city_name		= $_POST['city_name'];
				$zipcode 		= $_POST['zipcode'];
				$user_id 		= $row_user['id'];
				$country_id= $_POST['country_id'];

				mysqli_query($conn,"update users set post_code = '$zipcode',country_id = '$country_id',state_id = '$state_id',city_name = '$city_name' where id = '$user_id'");

				$company_id 	= $row_comp['id'];
				$company_name 	= $_POST['company_name'];
				$company_type_id= $_POST['company_type_id'];
				$office_phone   = $_POST['office_phone'];
				$address 		= $_POST['address'];

				mysqli_query($conn,"update companies set address = '$address', office_phone = '$office_phone', company_name = '$company_name', company_type_id = '$company_type_id',country_id = '$country_id' where id = '$company_id'");

				echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div>';
				echo '<script>window.location.href="'.WWW.'update-company-profile-1.html?step=3"</script>';
			}

			if(isset($_POST['update_individual_traits']) && !empty($_POST['update_individual_traits'])){
				$years_in_business 	= $_POST['years_in_business'];
				$legal_status 		= $_POST['legal_status'];
				$status_level 		= $_POST['status_level'];
				$gameType 			= implode(',', $_POST['gameType']);
				$matchType 			= implode(',', $_POST['matchType']);
				$types_of_services 	= $_POST['types_of_services'];
				$id 				= $_SESSION['ycdc_dbuid'];

				mysqli_query($conn,"update users set game_types = '$gameType', match_types = '$matchType' where id = '$id'");
				mysqli_query($conn,"update companies set years_in_business = '$years_in_business', legal_status = '$legal_status', status_level = '$status_level', service_offering = '$types_of_services' where user_id = '$id'");
				echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div>';
				echo '<script>window.location.href="'.WWW.'update-company-profile-1.html?step=4"</script>';
			}

			if(isset($_POST) && $_POST['action'] == 'add-staff'){

				$title 	 		= $_POST['title'];
				$first_name 	= $_POST['first_name'];
				$last_name 		= $_POST['last_name'];
				$user_id 		= $_SESSION['ycdc_dbuid'];
				if(!empty($title) && !empty($first_name) && !empty($last_name)){
					$rs_chk = mysqli_query($conn,"select * from company_staff where user_id = '$user_id' and title = '$title' and first_name = '$first_name' and last_name = '$last_name'");
					if(mysqli_num_rows($rs_chk) == 0){
						mysqli_query($conn,"insert into company_staff (user_id, title, first_name, last_name, type)
							values ('$user_id','$title','$first_name','$last_name','management');");
						echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div>';
						echo '<script>window.location.href="'.WWW.'update-company-profile-1.html?step=5"</script>';
					}
				}
			}

			if(isset($_GET) && $_GET['action'] == 'delete' && $_GET['type'] == 'staff'){
				mysqli_query($conn,"delete from company_staff where id = '".$_GET['id']."'");
				echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div>';
				echo '<script>window.location.href="'.WWW.'update-company-profile-1.html?step=5"</script>';
			}

			if(isset($_POST) && $_POST['action'] == 'edit-staff'){

				$title 	 		= $_POST['title'];
				$first_name 	= $_POST['first_name'];
				$last_name 		= $_POST['last_name'];
				$recordId 		= $_POST['recordId'];

				mysqli_query($conn,"update company_staff set title = '$title', first_name = '$first_name', last_name = '$last_name' where id = '$recordId'");
				echo '<div id="success" class="alert alert-success"><b>Success : </b>Information updated ... !</div>';
				echo '<script>window.location.href="'.WWW.'update-company-profile-1.html?step=5"</script>';
			}
?>

<style>
	.menu li {width:20%;}
	.menu li a{padding:0 10px;}
	.hide{display:none;}
	.datepicker{z-index:1151 !important;}
	.ui-autocomplete{z-index:1152 !important;}
</style>

<div class="page-container">
	<?php include('common/user-left-panel.php');?>

	<!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">

        <div class="white-box_listting">

            <div class="row">
              <div class="col-sm-12">
                <h2 style="margin-top:0"> Profile Information </h2>
              </div>
            </div>
            <ul id="menu-company-profile" class="menu" style="display:block; height: 30px;">
              <li><a href="#" id="step1-tab" >Step 1</a></li>
              <li><a href="#" id="step2-tab">Step 2</a></li>
              <li><a href="#" id="step3-tab">Step 3</a></li>
              <li><a href="#" id="step4-tab">Step 4</a></li>
              <li><a href="#" id="step5-tab">Step 5</a></li>

            </ul>
            <div class="clearfix" style="height:10px;"></div>

            <div id="step1" class="content1" style="display:none;" >
              <h3> Profile Photo </h3>
              <form method="post" action="" enctype="multipart/form-data" class="form-horizontal">
                <input name="action" value="submit" type="hidden">
                <div class="form-group">
                  <label class="col-sm-4 control-label"> Current Photo </label>
                  <div class="col-sm-8">
                    <? if($row_photo){ ?>
						<div style="float:left;width:160px;">
						<a class="no-bg"  href="<?=WWW?>delete-user-profile-picture.php?id=<?php echo $row_photo['id'];?>">
						<img onclick="return confirm('Are you sure to delete Photo');" src="<?=WWW?>images/erase.png" border="0" style=" position:relative; left:170px;top:10px;z-index:1;" /></a>
						<img src="<?=WWW?>users/<?=$row_user['id']?>/photos/<?=$row_photo['file_name']?>" class="img-responsive">
						</div>
					<? }else{ ?>
						<img src="<?=WWW?>images/no-photo.png" class="img-responsive">
					<? } ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label"> Update Photo </label>
                  <div class="col-sm-4">
                    <input name="photo" accept="image/*" type="file">
                    <span class="help-block">
                      Max Photo size : 10MB &nbsp; &nbsp; &nbsp; <a target="_blank" href="multimedia-policy.html">Multimedia Policy</a>
                    </span>
                  </div>
                  <div class="col-sm-4">
                    <p class="form-control-static"> <a id="from-album" href="javascript:;" onclick="if($('#photos').hasClass('hide')) $('#photos').removeClass('hide'); else $('#photos').addClass('hide'); " class="btn orange full hvr-float-shadow"> From Album</a> </p>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-4 col-sm-8">
                    <input id="update" value="Update" name="update_profile_photo" class="btn orange hvr-float-shadow" type="submit">
                    <a id="cancel" class="btn blue hvr-float-shadow" href="dashboard.html">Cancel</a>
                  </div>
                </div>
              </form>

				<div id="photos" class="hide">
					<div class="row">
						<? 	$rs_photo = mysqli_query($conn,"select * from photos where entity_type = 'users' and entity_id = '".$user_id."' and is_default <> '1'");
							if(mysqli_num_rows($rs_photo) > 0){
								while($row_photos = mysqli_fetch_assoc($rs_photo)){
								?>
								<div id="photo" class="col-sm-3 text-center">
									<img src="<?=WWW?>users/<?=$user_id?>/photos/<?=$row_photos['file_name']?>" style="max-height:180px;">
									<br>
									<center>
										<input type="radio" name="album_photo" value="<?=$row_photos['id']?>">
									</center>
								</div>
								<?
								}
							}else{
								echo '<div class="alert alert-warning" style="margin:15px;"  id="information"> <i class="fa fa-exclamation-triangle"> </i>Photo album is empty</div>';
							}
						?>
					</div>
				</div>

            </div><!-- ./Step 1-->
            <div id="step2" class="content1" style="display:none;">
              <h3> Profile Video </h3>
              <form method="post" action="" enctype="multipart/form-data" class="form-horizontal">
                <input name="action" value="submit" type="hidden">
                <div class="form-group">
				 <label class="col-sm-2 control-label">Video Type</label>
                  <div class="col-sm-8">  <input type="radio" value="upload" name="video" <?=($embed == 0)?'checked="checked"':'';?>> Upload Video
                  <input type="radio" value="embed" name="video" <?=($embed == 1)?'checked="checked"':'';?>> Embed Video </div>

                </div>
				<div class="form-group">
                  <label class="col-sm-2 control-label"> Current Video <br/><br/>

				  <?php if($row_video['id']!=''){?>
					<a class="btn blue hvr-float-shadow" onclick='return confirm("Are you sure to delete Video");' href="<?=WWW?>delete-user-profile-video.php?id=<?php echo $row_video['id'];?>"><i class="fa fa-times"></i></a>
				  <?php } ?>

				  </label>
                  <div class="col-sm-8">
                    <?php $row_v = mysqli_fetch_assoc(mysqli_query($conn,"select * from videos where entity_type = 'users' and entity_id = '".$row_user['id']."' and is_default = '1'"));
						if($row_v){

								if(preg_match('/<iframe(.*)<\/iframe>/', $row_video['file_name'])){
									preg_match('/src="(.*?)"/',$row_video['file_name'] , $src);
									$src = $src[1];
									$video = "<iframe width='450' height='350' allowfullscreen src='$src'></iframe>";
								}else{
									$video_path = WWW.'videos/'.$row_v['id'].'/'.$row_v['file_name'];
									$video = '<video id="video1" width="450" height="350" controls preload="metadata">
										<source src="'.$video_path.'" type="video/mp4; codecs=\'avc1.42E01E, mp4a.40.2\'">
										<source src="'.$video_path.'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
									    <source src="'.$video_path.'" type=\'video/ogg; codecs="theora, vorbis"\'>
									    <source src="'.$video_path.'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
									    <source src="'.$video_path.'" type=\'video/webm; codecs="vp8, vorbis"\'>
										</video>';
								}
					?>

					<?php
					echo '<div class="embed-responsive embed-responsive-16by9">'.$video.'</div>';
					} else { ?>
						<img src="<?=WWW?>images/no-photo.png" class="img-responsive">
					<?php }?>

					</div>
                </div>
                <div class="form-group" id="upload-video">
                  <label class="col-sm-2 control-label"> Upload Video </label>
                  <div class="col-sm-4">
                    <input name="video_file" accept="video/*" type="file">
                    <span class="help-block">
                      Max Video size : 10MB &nbsp; &nbsp; &nbsp; <a target="_blank" href="<?=WWW?>multimedia-policy.html">Multimedia Policy</a>
                    </span>
                  </div>
                  <div class="col-sm-4">
                    <p class="form-control-static"> <a id="from-album" onclick="if($('#videos').hasClass('hide')) $('#videos').removeClass('hide'); else $('#videos').addClass('hide'); " class="btn orange full hvr-float-shadow"> From Album</a> </p>
                  </div>
                </div>
                <div class="form-group" id="embed-video">
                  <label class="col-sm-2 control-label"> Embed Video </label>
                  <div class="col-sm-8">
                    <textarea name="video_code" class="form-control" rows="5" placeholder="Please paste here YouTube/Vimeo/DailyMotion Embed Code"><?if(preg_match('/<iframe(.*)<\/iframe>/', $row_video['file_name'])){echo trim($row_video['file_name']);} ?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-2">
                    <input id="update" value="Update" name="update_profile_video" class="btn orange hvr-float-shadow" type="submit">
                    <a id="cancel" class="btn blue hvr-float-shadow" href="<?=WWW?>dashboard.html">Cancel</a>
                  </div>
                </div>


				<div id="videos" class="hide">
				<div class="row">
						<? 	$rs_photo = mysqli_query($conn,"select * from videos where entity_type = 'users' and entity_id = '".$user_id."' and is_default  = '0' and id != '".$row_v['id']."'");
							if(mysqli_num_rows($rs_photo) > 0){
								while($row_photos = mysqli_fetch_assoc($rs_photo)){
								?>
								<div id="photo" class="col-sm-3 text-center">
									<?
											$video = $row_photos['file_name'];
											if(!empty($video)){
						if(preg_match('/<iframe(.*)<\/iframe>/', $row_photos['file_name'])){

									preg_match('/src="(.*?)"/',$row_photos['file_name'] , $src);
									$src = $src[1];
									$video = "<iframe width='205' height='130' frameborder='0' allowfullscreen
												src='$src'>
																	</iframe>";

									if(!preg_match('/height="(.*)"/', $video)){
										$video = preg_replace('/height="(.*)"/','height="130"',$video);
									}else{
										$video = preg_replace('/width="(.*)"/','width="220" height="150"',$video);
									}
						}else{

												$filename = explode('.',$video);
												$filename1= $filename[0];
												$video = '<video width="220" height="130" controls>
												  <source src="'.WWW.'videos/'.$row_photos['id'].'/'.$filename1.'.mp4" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_photos['id'].'/'.$filename1.'.ogg" type=\'video/ogg; codecs="theora, vorbis"\'>
												  <source src="'.WWW.'videos/'.$row_photos['id'].'/'.$filename1.'.mov" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'>
												  <source src="'.WWW.'videos/'.$row_photos['id'].'/'.$filename1.'.webm" type=\'video/webm; codecs="vp8, vorbis"\'>
												</video>';
											}
										}else{
											$video = '<img src="'.WWW.'images/no-video.jpg" width="220" height="130">';
										}
										echo $video;?>
										<br/>
										<input type="radio" name="album_video" value="<?=$row_photos['id']?>">

								</div>
								<?
								}
							}else{
								echo '<div style="margin:15px;" class="alert alert-warning" id="information"> <i class="fa fa-exclamation-triangle"> </i>Video album is empty</div>';
							}
						?>
					</div>
				</div>

			</form>
            </div><!-- ./Step 2-->


			 <div id="step3" class="content1" style="display:none;">
              <h3>Organization Information</h3>
              <form method="post" action="" enctype="multipart/form-data" class="form-horizontal req-frm">
                <input name="action" value="submit" type="hidden">
                <div class="form-group">
                  <label class="col-sm-2 control-label"> Organization Name </label>
                  <div class="col-sm-4">
                    <input value="<?=$row_comp['company_name']?>" name="company_name" class="form-control validate[required]" type="text">
                  </div>
                  <label class="col-sm-2 control-label"> Organization Type </label>
                  <div class="col-sm-4">
                    <?=get_combo('company_types','name',$row_comp['company_type_id'],'company_type_id');?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"> Organization Country </label>
                  <div class="col-sm-4">
								  <select name="country_id" id="country_id" class="validate[required] dd1 form-control" onchange="loadstates(this.value);">
									<?php
									$query = mysqli_query($conn, "select id, name from countries order by name ASC");
									while($rw = mysqli_fetch_assoc($query)){
									?>
										<option value="<?php echo $rw['id']; ?>" <?php if($row_comp['country_id']==$rw['id']) echo 'selected'; ?>><?php echo $rw['name']; ?></option>
									<?php
									}
									?>
									</select>
                  </div>
                  <label class="col-sm-2 control-label"> State </label>
                  <div class="col-sm-4">
                    <select name="state_id" id="state_id" class="form-control">
                      <option selected="selected" value="">Select State</option>
						<? 	$rs_states = mysqli_query($conn,"select * from states where country_id = '".$row_comp['country_id']."'");
								while($row_state_l = mysqli_fetch_assoc($rs_states)){ ?>
								<option <?=($row_user['state_id'] == $row_state_l['id'])?'selected="selected"':'';?> value="<?=$row_state_l['id']?>"><?=$row_state_l['name']?></option>
						<?  } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"> City </label>
                  <div class="col-sm-4">
                    <input name="city_name" value="<?php echo $row_user['city_name']; ?>" class="form-control" type="text">
                  </div>
                  <label class="col-sm-2 control-label"> Zip/Postal Code </label>
                  <div class="col-sm-4">
                    <input name="zipcode" class="form-control" maxlength="10" value="<?=$row_user['post_code']?>" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"> Street Address </label>
                  <div class="col-sm-4">
                    <input value="<?=$row_comp['address']?>" name="address" class="form-control" type="text">
                  </div>
                  <label class="col-sm-2 control-label"> Phone Number </label>
                  <div class="col-sm-4">
                    <?=get_combo('countries','phone_code',$row_comp['country_id'],'','text')?>
					<input value="<?=$row_comp['office_phone']?>" name="office_phone" class="form-control" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <input id="update" value="Update" class="btn orange hvr-float-shadow" name="update_company_information" type="submit">
                    <a id="cancel" class="btn blue hvr-float-shadow" href="<?=WWW?>dashboard.html">Cancel</a>
                  </div>
                </div>
              </form>
            </div><!-- ./Step 3-->
            <div id="step4" class="content1" style="display:none;">
              <h3> Individual Traits </h3>
              <form method="post" action="" enctype="multipart/form-data" class="form-horizontal req-frm">
                <input name="action" value="submit" type="hidden">
                <div class="form-group">
                  <label class="col-sm-4 control-label"> Year(s) of Formation </label>
                  <div class="col-sm-8">
                    <select name="years_in_business" class="form-control validate[required]">
                    <? for($x = 0; $x <= 100; $x++){ ?>
							<option value="<?=$x?>" <?=($x == $row_comp['years_in_business'])?'selected="selected"':'';?>><?=$x?></option>
					<? } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-4 control-label"> Legal Status </label>
                  <div class="col-sm-8">
                    <select name="legal_status" class="form-control validate[required]">
                      <option selected="selected" value="">Select One</option>
						<? foreach($legalStatus as $ls){ ?>
						<option value="<?=$ls?>" <?=($ls == $row_comp['legal_status'])?'selected="selected"':'';?>><?=$ls?></option>
						<? } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label"> Status Level </label>
                  <div class="col-sm-8">
                    <select name="status_level" class="form-control validate[required]">
						<option selected="selected" value="">Select One</option>
						<? foreach($statusLevel as $ls){ ?>
							<option value="<?=$ls?>" <?=($ls == $row_comp['status_level'])?'selected="selected"':'';?>><?=$ls?></option>
						<? } ?>
                    </select>
                  </div>
                </div>
				<? if($row_comp['company_type_id'] == 1
					   || $row_comp['company_type_id'] == 2
					   || $row_comp['company_type_id'] == 4
					   || $row_comp['company_type_id'] == 35){
				?>
                <div class="form-group">
                  <label class="col-sm-4 control-label"> Preferred Game Type </label>
                  <div class="col-sm-8">

						<? $prefferedGameTypeData = explode(',',$row_user['game_types']);?>
							<? foreach($prefferedGameType as $ma){ ?>
							<p><input style="float:left;" type="checkbox" <?=(in_array($ma, $prefferedGameTypeData))?'checked="checked"':'';?> name="gameType[]" value="<?=$ma?>">&nbsp; <?=$ma?></p>
						<? } ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label"> Preferred Match Type </label>
                  <div class="col-sm-8">
						<? $matchArrayData = explode(',',$row_user['match_types']);?>
							<? foreach($matchArray as $ma){ ?>
							<p><input style="float:left;" type="checkbox" <?=(in_array($ma, $matchArrayData))?'checked="checked"':'';?> name="matchType[]" value="<?=$ma?>">&nbsp; <?=$ma?></p>
						<? } ?>

                  </div>
                </div>
				<? } ?>
                <div class="form-group">
                  <label class="col-sm-4 control-label"> Type of services: </label>
                  <div class="col-sm-8">
                    <textarea name="types_of_services" class="validate[required] form-control" rows="3" maxlength="1000"><?=$row_comp['service_offering']?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-4 col-sm-8">
                    <input id="update" value="Update" class="btn orange hvr-float-shadow" type="submit" name="update_individual_traits">
                    <a id="cancel" class="btn blue hvr-float-shadow" href="<?=WWW?>dashboard.html">Cancel</a>
                  </div>
                </div>
              </form>
            </div><!-- ./Step 4-->
            <div id="step5" class="content1" style="display:none;">
              <h3> Organization Management </h3>
              <form method="post" action="" enctype="multipart/form-data" class="form-horizontal">
                <input name="action" value="submit" type="hidden">
                <div class="row">
                  <div class="col-xs-10">
                    <h5>Organization Management (please list them in hierarchy)</h5>
                  </div>
                  <div class="col-xs-2">
                    <a href="#" class="btn orange full hvr-float-shadow margin-bottom-10" onClick="javascript:void(0)" data-toggle="modal" data-target="#add-staff"> <i class="fa fa-plus"></i> </a>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="table-responsive">
                      <table class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                          <tr>
                            <th>Title</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
						<? 	$rs_msg = mysqli_query($conn,"select cs.* from company_staff cs where cs.user_id = '".$row_user['id']."' and type='management' order by id ");
			        	if(mysqli_num_rows($rs_msg) > 0){
						while($row_msg = mysqli_fetch_assoc($rs_msg)){ ?>
                          <tr title="Click for detail">
                            <td><?=$row_msg['title']?></td>
				       		<td><?=$row_msg['first_name']?></td>
				       		<td><?=$row_msg['last_name']?></td>
                            <td>
                              <a class="text-warning edit-staff-info" href="#edit-staff" id="<?=$row_msg['id']?>" onClick="javascript:void(0)" data-toggle="modal" data-target="#edit-staff"><i class="fa fa-edit"></i></a>
                              <a class="text-danger" onclick="return confirm('Are you sure to delete record');"  href="<?=WWW?>update-company-profile-1.html?action=delete&type=staff&id=<?=$row_msg['id']?>"><i class="fa fa-times"></i></a>
                            </td>
                          </tr>
                        <? } } ?>

                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12 text-center">
                    <input value="Update" class="btn orange hvr-float-shadow" type="submit">
                    <a id="cancel" class="btn blue hvr-float-shadow" href="<?=WWW?>dashboard.html">Cancel</a>
                  </div>
                </div>
              </form>
            </div><!-- ./Step 5-->



          </div>


        </div>
      </div>
      <!-- END CONTENT-->

</div>

 <div class="modal fade" id="add-staff" tabindex="-1" role="dialog" aria-labelledby="add-staffModel" aria-hidden="true">
      <div class="modal-dialog" style="max-height:800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Add Organization Staff</h4>
          </div>
          <div class="modal-body" style="max-height:700px">
            <form method="post" action="" class="form-horizontal" id="myform">
              <input type="hidden" name="action" value="add-staff">
              <div class="form-group">
                <label class="col-xs-5 control-label">Title of employee</label>
                <div class="col-xs-7">
                  <input name="title" required class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">First Name</label>
                <div class="col-xs-7">
                  <input name="first_name" required class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Last Name</label>
                <div class="col-xs-7">
                  <input name="last_name" class="form-control validate[required]" type="text" required>
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-offset-5 col-xs-7">
                  <input class="btn orange hvr-float-shadow" value="Save" type="submit">
                </div>
              </div>
            </form>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.add-staff-Model -->

	 <div class="modal fade" id="edit-staff" tabindex="-1" role="dialog" aria-labelledby="edit-staffModel" aria-hidden="true">
      <div class="modal-dialog" style="max-height:800px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Update Company Staff</h4>
          </div>
          <div class="modal-body" style="max-height:700px">
            <form method="post" action="" class="form-horizontal update-staff" id="">
				<input type="hidden" name="action" value="edit-staff">
				<input type="hidden" name="recordId" id="recordId" value="">
              <div class="form-group">
                <label class="col-xs-5 control-label">Title of employee</label>
                <div class="col-xs-7">
                  <input name="title" id="title" required class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">First Name</label>
                <div class="col-xs-7">
                  <input name="first_name" required id="first_name" class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-5 control-label">Last Name</label>
                <div class="col-xs-7">
                  <input name="last_name" id="last_name" required class="form-control validate[required]" type="text">
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-offset-5 col-xs-7">
                  <input class="btn orange hvr-float-shadow" value="Save" type="submit">
                </div>
              </div>
            </form>
          </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.add-staff-Model -->

	<script type="text/javascript">
	var WWW = "<?=WWW?>";
	$(document).ready(function() {

		$("#myform").validate();

		$(".update-staff").validate();

		$("#menu-company-profile a").click(function(event){
			event.preventDefault();
			event.stopPropagation();
			var id = $(this).attr('id').replace('step','').replace('-tab','');
			for(var i=1;i<=10;i++){
				$("#step"+i).hide();
				$("#step"+i+"-tab").removeClass("active");
			}
			$("#step"+id).show();
			$("#step"+id+"-tab").addClass("active");
		});

		<?php if(isset($_POST) && !empty($_POST['update_profile_video'])){ ?>
			$("#step2").show();$("#step2-tab").addClass("active");
		<?php }elseif(isset($_POST) && !empty($_POST['update_profile_photo'])){ ?>
			$("#step1").show();$("#step1-tab").addClass("active");
		<?php }if(isset($_POST) && !empty($_POST['update_location'])){ ?>
			$("#step3").show();$("#step3-tab").addClass("active");
		<?php }elseif(isset($_POST) && !empty($_POST['update_individual_traits'])){ ?>
			$("#step4").show();$("#step4-tab").addClass("active");
		<?php }elseif(isset($_POST['action']) && in_array($_POST['action'],array('add-club','club-update','add-league','league-update','add-agent','agent-update') )){ ?>
			$("#step5").show();$("#step5-tab").addClass("active");
		<?php }else if(isset($_GET['step']) && !empty($_GET['step'])){  ?>
			$("#step<?php echo $_GET['step']; ?>").show();$("#step<?php echo $_GET['step']; ?>-tab").addClass("active");
		<?php }else{ ?>
			$("#step1").show();$("#step1-tab").addClass("active");
		<?php } ?>

		$('input[name=video]').click(function(){
			if(this.value == 'embed'){
				$('#upload-video').addClass('hide');
				$('#embed-video').removeClass('hide');

			}else{
				$('#embed-video').addClass('hide');
				$('#upload-video').removeClass('hide');
			}
		});

		$('.datepicker').datepicker({'format':'yyyy-mm-dd'});

	$('a.edit-staff-info').click(function(){
		var link = $(this).attr('href');
		if(link == '#edit-staff'){
			var id = this.id;
			$.ajax({type	: 'POST',
				url		: WWW + 'includes/get-record.php',
				data	: ({table:'company_staff',field:'*',id:id}),
				success	: function(msg){
					msg1 = msg.split('___');
					$("#edit-staff").find('#title').val(msg1[0]);
					$("#edit-staff").find('#first_name').val(msg1[1]);
					$("#edit-staff").find('#last_name').val(msg1[2]);
					$("#edit-staff").find('#recordId').val(id);
				}
			});
		}
	});

	$(".req-frm").validationEngine();

	});
	function loadstates(id){
		$.ajax({
	      type: "POST",
	      url: "ajax/loadstates.php",
				data: "cntid="+id,
	      success: function (result) {
					$("#state_id").html(result);
	      }
	  });
	  return false;
	}
	</script>

<?php include('common/footer.php'); ?>
