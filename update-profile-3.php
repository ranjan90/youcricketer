<?php include('common/header.php');?>
<? if(!isset($_SESSION['ycdc_dbuid']) || empty($_SESSION['ycdc_dbuid'])){ ?>
<script>
	window.location = '<?=WWW?>logout.php';
</script>
<? } ?>
	<div class="middle">
		<h1>Multimedia Information</h1>

		<? 	if(isset($_POST) && !empty($_POST)){
				$facebook 	= $_POST['facebook'];
				$twitter 	= $_POST['twitter'];
				$fieldsa 	= array();

				if(!empty($facebook)){
					$fieldsa[] = "facebook = '$facebook'";
				}
				if(!empty($twitter)){
					$fieldsa[] = "twitter = '$twitter'";
				}

				$fieldsa[] = "isdefaultvideo = " . $_POST['ddDefaultVideo'];
				mysqli_query($conn,"update users set ".implode(',',$fieldsa)." where id = '".$row_user['id']."'");

				$rs_club = mysqli_query($conn,"select * from clubs where name = '".$_POST['club']."'");
				if(mysqli_num_rows($rs_club) == 0){
					mysqli_query($conn,"insert into clubs (title, status) values ('".$_POST['club']."','1') ");
					$club_id = mysqli_insert_id($conn);
				}else{
					$row_club = mysqli_fetch_assoc($rs_club);
					$club_id  = $row_club['id'];
				}

				$rs_leagues = mysqli_query($conn,"select * from leagues where name = '".$_POST['league']."'");
				if(mysqli_num_rows($rs_leagues) == 0){
					mysqli_query($conn,"insert into leagues (title, status) values ('".$_POST['league']."','1') ");
					$league_id = mysqli_insert_id($conn);
				}else{
					$row_league = mysqli_fetch_assoc($rs_leagues);
					$league_id  = $row_league['id'];
				}

				$rs_chk_clubs = mysqli_query($conn,"select * from users_to_clubs where club_id = '$club_id' and user_id = '".$row_user['id']."'");
				if(mysqli_num_rows($rs_chk_clubs) == 0){
					mysqli_query($conn,"insert into users_to_clubs (club_id, user_id) values ('$club_id','".$row_user['id']."')");
				}
				$rs_chk_leagues = mysqli_query($conn,"select * from users_to_leagues where league_id = '$league_id' and user_id = '".$row_user['id']."'");
				if(mysqli_num_rows($rs_chk_leagues) == 0){
					mysqli_query($conn,"insert into users_to_leagues (league_id, user_id) values ('$league_id','".$row_user['id']."')");
				}

				$other_information = $_POST['other_information'];
				
/*
				$filename = "";

				if(!empty($_FILES['video']['name']))
				{
					$filename = friendlyURL($_FILES['video']['name'][0]).'.mpg';

					$image = new SimpleImage(FALSE);

					$image->load($_FILES["video"]["tmp_name"][$x]);
					$image->save('users/'.$user_id.'/videos/'.$filename);

					chmod('users/'.$user_id.'/videos/'.$filename,0777);

					$rs_check   = mysqli_query($conn,"select * from videos where entity_type = 'users' and entity_id = '".$row_user['id']."' and file_name = '".$filename."'");
					mysqli_query($conn,"insert into videos (file_name, status, entity_id, entity_type, is_default) values ('".$filename."','1','".$row_user['id']."','users','1');");
				}

				if( $_POST['ddDefaultVideo'] == "0" )
				{
					$rs_check_code = mysqli_query($conn,"select * from videos where entity_type = 'users' and entity_id = '".$row_user['id']."' and is_default = '1'");
					if(mysqli_num_rows($rs_check_code) == 0)
					{
						mysqli_query($conn,"insert into videos (file_name, status, entity_id, entity_type, is_default) values ('".$_POST['video_code']."','1','".$row_user['id']."','users','1');");
					}
					else
					{
						mysqli_query($conn,"update videos set file_name = '" . $filename . "', status, entity_id, entity_type, is_default) values ('".$_POST['video_code']."','1','".$row_user['id']."','users','1');");
					}
				}
				else
				{
						$row_video = mysqli_fetch_assoc($rs_check_code);
						mysqli_query($conn,"update videos set file_name = '".$_POST['video_code']."' where id = '".$row_video['id']."'");
				}
 */
				mysqli_query($conn,"update users set other_information = '$other_information' where id = '".$row_user['id']."'");
				echo '<div id="success"><b>Success : </b>Information updated ... !</div><br><br>';
                ?>
                <script>
                	window.location = '<?=WWW?>dashboard.html';
				</script>
                <?
			}
			$row_video = mysqli_fetch_assoc(mysqli_query($conn,"select * from videos where entity_id = '".$row_user['id']."' and entity_type = 'users' and is_default = '1' and file_name != ''"));
		?>

		<div class="white-box content" id="dashboard">
			<div class="small-column">
				<? include('common/user-left-panel.php');?>
			</div>
			<div class="large-column">
			<form method="post" action="" enctype="multipart/form-data">
				<fieldset>
					<h2>Playing Details</h2>
					<div class="form-box">
						<label>Your Facebook Page Link</label>
					</div>
					<div class="form-box">
						<input style="width:100%;" type="text" name="facebook" value="<?=$row_user['facebook']?>" class="input-login validate[custom[url]]">
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Your Twitter Page Link</label>
					</div>
					<div class="form-box">
						<input style="width:100%;" type="text" name="twitter" value="<?=$row_user['twitter']?>" class="input-login validate[custom[url]]">
					</div>
					<div class="clear"></div>
					<h2>Playing Details</h2>
					<div class="form-box">
						<label>Your Current Club Name</label>
					</div>
					<div class="form-box">
						<? $row_club = mysqli_fetch_assoc(mysqli_query($conn,"select * from users_to_clubs where user_id = '".$_SESSION['ycdc_dbuid']."' order by id desc limit 1")); ?>
						<input style="width:100%;" value="<?=get_combo('clubs','title',$row_club['club_id'],'','text')?>" type="text" name="club" class="input-login">
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Your Current League Name </label>
					</div>
					<div class="form-box">
						<? $row_league = mysqli_fetch_assoc(mysqli_query($conn,"select * from users_to_leagues where user_id = '".$_SESSION['ycdc_dbuid']."' order by id desc limit 1")); ?>
						<input style="width:100%;" value="<?=get_combo('leagues','title',$row_league['league_id'],'','text')?>" type="text" name="league" class="input-login ">
					</div>
					<div class="clear"></div>
					<div class="form-box" style="width:100%">
						<label>Anything Else That You Would Like to Share about Your Cricketing Experience? </label>
						<textarea style="width:92%;" name="other_information" class=""><?=$row_user['other_information']?></textarea>
					</div>
					<div class="clear"></div>
					<h2>Videos/Media</h2>
					<div class="form-box">
						<label>Default Profile Video </label>
					</div>
					<div class="form-box">
						<select style="width:100%;" name="ddDefaultVideo">
							<option value="1">Uploaded Video</option>
							<option value="0">Embedded Video Code</option>
						</select>
					</div>
					<div class="form-box" style="width:100%;">
						<script type="text/javascript">
							function ShowVideo(objTA)
							{
								var objContainerDIV = objTA.parentNode.nextSibling;
								var strValue = "";

								if( navigator.appName == "Netscape" )
								{
									strValue = objTA.value.trim();
								}
								else
								{
									strValue = trim(objTA.value);
								}

								objContainerDIV.innerHTML = strValue;
							}
						</script>
						
						<label>Attach Default Video </label>
						<div>
							<div style="float:left; width: 270px; height: 240px; padding-right: 20px;">
								<textarea style="width: 250px; height: 230px;" name="video_code" onblur="ShowVideo(this);"><?=($row_video)?$row_video['file_name']:'';?></textarea>
							</div><div style="border: solid 1px #C0C0C0; float:left; height: 240px; width: 320px; overflow: hidden;"><?=($row_video)?$row_video['file_name']:'';?></div>
						</div>
					</div>
					<div class="clear"></div>
					<div class="form-box">
						<label>Please paste here YouTube/DailyMotion Embed Code</label>
					</div>
					<div class="clear"></div>
					<p style="text-align:left; float:left; margin-left:10px; color:#cc0000; font-size:25px;">OR</p>
					<div class="clear"></div>
					<div class="form-box">
						<label>Upload Default Video </label>
						<input type="file" name="video">
					</div>
					<div class="form-box">
						<label>Max Video size : 10MB</label>
					</div>
					<div class="clear"></div>
					
					<div class="form-box">
						<a href="<?=WWW?>dashboard.html" class="submit-login left">Back</a>
					</div>
					<div class="form-box">
						<input type="submit" value=" Update " class="submit-login">
					</div>
				</fieldset>
			</form>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<?php include('common/footer.php'); ?>